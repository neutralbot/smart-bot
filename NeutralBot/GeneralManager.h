#pragma once
#include "Core\Neutral.h"
#include <map>
#include <vector>
#include <algorithm>

#define DEFAULT_ALARM_PATH ".\\sound\\police.wav"
#define DEFAULT_THEME "Windows7Theme"

#pragma pack(push, 1)
class GeneralManager{
	std::vector<std::string> recent_scripts;
	std::map<alert_t, std::string/*filename*/> map_sound;

	std::string soundPath;
	std::string themaName;
	std::string currentLang = "langbase";

	uint32_t auto_save_sec = 60000;

	bool practice_mode = false;
	bool dev_mode = false;
	bool interface_most_top = true;
	bool interface_auto_resize = false;
	bool interface_shortcut_panel = false;

public:
	GeneralManager();
	
	std::vector<std::string> vector_all_sound;
	
	void add_vector_all_sound(std::string filename){
		bool add = true;

		for (auto item : vector_all_sound)
			if (item == filename)
				add = false;

		if (!add)
			return;

		vector_all_sound.push_back(filename);
	}

	std::map<alert_t, std::string/*filename*/> get_map_sound(){
		return map_sound;
	}

	std::string get_sound_filename(alert_t key){
		return map_sound[key];
	}
	void set_sound_filename(alert_t key, std::string filename){
		map_sound[key] = filename;
	}

	void set_interface_most_top(bool state);
	void set_interface_auto_resize(bool state);
	void set_interface_shortcut(bool state);
	void set_practice_mode(bool state){
		practice_mode = state;
	}
	void set_auto_save_sec(uint32_t temp){
		auto_save_sec = temp;
	}
	uint32_t get_auto_save_sec(){
		return auto_save_sec;
	}
	bool get_practice_mode(){
		return practice_mode;
	}
	bool get_interface_shortcut();
	bool get_interface_auto_resize();
	bool get_interface_most_top();

	std::vector<std::string> get_recent_scripts();

	void add_recent_scripts(std::string recent);
	void remove_recent_scritps(std::string value);
	void set_soundPath(std::string pathin);
	void set_themaName(std::string themaNamein);
	void set_currentLang(std::string currentLangin);
	void set_dev_mode(bool dev_modein){
		dev_mode = dev_modein;
	}
	bool get_dev_mode(){
		return dev_mode;
	}
	std::string get_currentLang();
	std::string get_soundPath();
	std::string get_themaName();
	
	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);

	static GeneralManager* get();
};

#pragma pack(pop)