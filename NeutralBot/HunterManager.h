#pragma once
#include <string>
#include <map>
#include "Core\Util.h"

#define HUNTER_MAX_GROUPS 10

#pragma pack(push,1)
class TargetNonShared{
	uint32_t weapon_id = 0;
	uint32_t danger = 0;
	uint32_t order = 0;
	uint32_t min_hp = 0;
	uint32_t max_hp = 100;
	uint32_t distance = 1;
	uint32_t target_range_min = 0;
	uint32_t target_range_max = 10;
	uint32_t point = 0;
	lure_mode_t type_lure;
	move_mode_t move_mode_type = move_mode_t::move_mode_t_follow;

	bool diagonal;	
	bool shooter;
	bool pathable;
	bool shotable;
	bool loot;
	bool attack_if_trapped = true;
	bool attack_if_block_path = true;
public:
	TargetNonShared();

	DEFAULT_GET_SET(move_mode_t, move_mode_type);	
	DEFAULT_GET_SET(uint32_t, order);
	DEFAULT_GET_SET(uint32_t, min_hp);
	DEFAULT_GET_SET(uint32_t, max_hp);
	DEFAULT_GET_SET(uint32_t, point);
	DEFAULT_GET_SET(bool, diagonal);
	DEFAULT_GET_SET(uint32_t, target_range_min);
	DEFAULT_GET_SET(uint32_t, target_range_max);
	DEFAULT_GET_SET(uint32_t, distance);
	DEFAULT_GET_SET(bool, pathable);
	DEFAULT_GET_SET(bool, shotable);
	DEFAULT_GET_SET(bool, loot);
	DEFAULT_GET_SET(int, danger);
	DEFAULT_GET_SET(uint32_t, weapon_id);
	
	void set_shooter(bool in);

	bool get_shooter();

	bool in_distance_range(uint32_t current_distance);

	void set_type_lure(lure_mode_t in);

	lure_mode_t get_type_lure();

	Json::Value parse_class_to_json();

	void set_weapon(uint32_t id);

	uint32_t get_weapon();

	bool get_use_weapon();

	void set_use_weapon(bool use);

	void parse_json_to_class(Json::Value& attr);
};

typedef std::shared_ptr<TargetNonShared> TargetNonSharedPtr;
class Target{
	std::string name;
	bool attack_if_trapped = true;
	bool attack_if_block_path = true;

public:
	Target();

	bool get_attack_if_block_path(){
		return attack_if_block_path;
	}
	void set_attack_if_block_path(bool in){
		attack_if_block_path = in;
	}

	//DEFAULT_GET_SET(bool, attack_if_block_path);
	DEFAULT_GET_SET(bool, attack_if_trapped);
	DEFAULT_GET_SET(std::string, name);

	std::map<int32_t/*group*/,std::shared_ptr<TargetNonShared>> non_shared_info;
	std::shared_ptr<TargetNonShared> get_at_group(int group);
	TargetNonSharedPtr get_group_life_in_range(uint32_t life_percent);
	TargetNonSharedPtr get_group_match(uint32_t life_percent, uint32_t current_range, bool pathable = true);
	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value& json_target);
};

typedef std::shared_ptr<Target> TargetPtr;
typedef std::map <std::string, TargetPtr> TargetPtrMap;


class HunterManager{
	neutral_mutex mtx_access;
	TargetPtrMap target_rules;

	HunterManager();

	int min_points = 0;
	bool any_monster = false;
	bool wait_pulse = false;
	bool pulse_if_trapped = true;
	bool has_keep_distance_hook = false;

	bool anyMonster = false;
	uint32_t weapon_id = 0;
	std::string keep_distance_algorithm;

	bool reset_keep_distance_algorithm();
	
	uint32_t min_points_to_change = 0;
	uint32_t path_index = 0;
	uint32_t path_index_two = 0;

	attack_mode_t new_mode = attack_mode_no_change;
	attack_mode_t old_mode = attack_mode_no_change;
public:

	static HunterManager* get();

	void set_pulse_if_trapped(bool in){ pulse_if_trapped = in; }
	void set_min_points_to_change(uint32_t in){ min_points_to_change = in; }
	void set_new_mode(attack_mode_t in){ new_mode = in; }
	void set_old_mode(attack_mode_t in){ old_mode = in; }
	void set_path_index(uint32_t in){ path_index = in; }
	void set_path_index_two(uint32_t in){ path_index_two = in; }

	bool get_pulse_if_trapped(){ return pulse_if_trapped; }
	uint32_t get_path_index_two(){ return path_index_two; }
	uint32_t get_path_index(){ return path_index; }
	uint32_t get_min_points_to_change(){ return min_points_to_change; }
	attack_mode_t get_new_mode(){ return new_mode; }
	attack_mode_t get_old_mode(){ return old_mode; }

	void set_weapon(uint32_t id);

	uint32_t get_weapon();

	bool get_wait_pulse();
	
	std::pair<TargetPtr, TargetNonSharedPtr> get_match_creature(char* str, uint32_t life_percent, uint32_t dist, bool pathable);

	void set_wait_pulse(bool state);

	bool get_has_keep_distance_hook();

	void set_has_keek_distance_hook(bool state);

	int get_min_points();

	char* get_keep_distance_algorithm_data();

	void set_keep_distance_algorithm(std::string in);

	void Clear();
	void disable_group(uint32_t group_id);
	void enable_group(uint32_t group_id);
	void set_any_monster(bool state);
	void set_min_points(int temp);
	void reset_distance_algorithm();

	void set_group_state(uint32_t group_id, bool state);

	bool get_group_state(uint32_t group_id);
	bool creatures_need_rearchable;
	bool creatures_need_shootable;
	bool contains_creature_name(std::string name);
	bool get_any_monster();

	std::vector<std::string> get_duplicated_row_name();

	std::vector<bool> group_status;
	std::string request_new_creature();
	void delete_id(std::string id_);
	TargetPtr get_target_rule_by_id(std::string id);
	TargetPtrMap get_target_rules();
	
	std::pair<TargetPtr, TargetNonSharedPtr> get_target_by_values(std::string name, uint32_t life_percent, uint32_t current_distance = 0xffffffff);
	static std::pair<TargetPtr, TargetNonSharedPtr> get_target_by_values(TargetPtrMap& target_map,
		std::string& name,
		uint32_t life_percent, uint32_t current_distance = 0xffffffff);
	static std::pair<TargetPtr, TargetNonSharedPtr> get_target_by_values(TargetPtrMap& target_map, char* name, uint32_t life_percent,
		uint32_t current_distance = 0xffffffff);

	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value& hunterValue);
};
#pragma pack(pop)


