#pragma once
#include "Alerts.h"
#include "ConfigSound.h"
#include "Core\constants.h"

using namespace Neutral;
void Alerts::loadItemOnView(){
	ListAlerts->BeginUpdate();
	ListAlerts->Items->Clear();

	std::map<uint32_t, std::shared_ptr<AlertsStruct>> myMap = AlertsManager::get()->GetMap();
	for (auto i = myMap.rbegin(); i != myMap.rend(); i++){
		std::shared_ptr<AlertsStruct> row = AlertsManager::get()->GetRuleById(i->first);
		array<String^>^ columnArrayItems = { gcnew String(&row->to_string()[0]), Convert::ToString(row->sound), Convert::ToString(row->pause), Convert::ToString(row->logout), Convert::ToString(row->value) };
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(i->first), columnArrayItems);
		newItem->Value = gcnew String(Convert::ToString(i->first));
		ListAlerts->Items->Add(newItem);
	}
	ListAlerts->EndUpdate();
}

void Alerts::loadAll(){
	loadItemOnView();
}

System::Void Alerts::ListAlerts_EditorRequired(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEditorRequiredEventArgs^  e) {
	if (e->ListViewElement->CurrentColumn->HeaderText == "Type"){
		Telerik::WinControls::UI::ListViewDropDownListEditor^ editortype = gcnew Telerik::WinControls::UI::ListViewDropDownListEditor();

		for (int i = 1; i < alert_t::alert_total; i++){
			Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
			item->Text = gcnew String(&AlertsManager::getAlertAsString((alert_t)i)[0]);
			((Telerik::WinControls::UI::BaseDropDownListEditorElement^)editortype->EditorElement)->Items->Add(item);
		}
		editortype->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;

		e->Editor = editortype;
	}
	else if (e->ListViewElement->CurrentColumn->HeaderText == "Value"){
		Telerik::WinControls::UI::ListViewSpinEditor^ editorr = gcnew Telerik::WinControls::UI::ListViewSpinEditor();
		editorr->MaxValue = 9999;
		e->Editor = editorr;
	}
	else{
		array<String^>^ columnArrayItens = { "True", "False" };
		Telerik::WinControls::UI::ListViewDropDownListEditor^ editor = gcnew Telerik::WinControls::UI::ListViewDropDownListEditor();
		((Telerik::WinControls::UI::BaseDropDownListEditorElement^)editor->EditorElement)->Items->AddRange(columnArrayItens);

		editor->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;

		e->Editor = editor;
	}
}
System::Void Alerts::ListAlerts_DoubleClick(System::Object^  sender, System::EventArgs^  e) {
	Telerik::WinControls::UI::RadListView^ listView = (Telerik::WinControls::UI::RadListView^) sender;

	listView->BeginEdit();
}
System::Void Alerts::ListAlerts_ItemValueChanging(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangingEventArgs^  e) {
	int index = ListAlerts->Items->IndexOf(e->Item);
	if (e->NewValue == e->OldValue)
		return;

	if (e->ListViewElement->CurrentColumn->Name != "Column 0")
		return;

	if (index == -1)
		return;

	if (index != (ListAlerts->Items->Count - 1))
		return;
	
	array<String^>^ columnArrayItems = { "", "False", "False", "False", "0" };
	Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(-1), columnArrayItems);
	ListAlerts->Items->AddRange(gcnew array<Telerik::WinControls::UI::ListViewDataItem^>(1) { newItem });
}
System::Void Alerts::ListAlerts_ItemValueChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangedEventArgs^  e) {
	std::shared_ptr<AlertsStruct> item;
	if (e->Item->Value)
		item = AlertsManager::get()->GetRuleById(Convert::ToInt32(e->Item->Value));

	if (!item){
		uint32_t row_id = AlertsManager::get()->CreatNewMapId();
		item = AlertsManager::get()->GetRuleById(row_id);
		e->Item->Value = Convert::ToString(row_id);
	}

	if (e->ListViewElement->CurrentColumn->Name == "Column 0")
		item->type = AlertsManager::getStringAsAlert(managed_util::fromSS(e->ListViewElement->CurrentItem[0]->ToString()));
	else if (e->ListViewElement->CurrentColumn->Name == "Column 1")
		item->sound = (e->ListViewElement->CurrentItem[1]->ToString() == "True");
	else if (e->ListViewElement->CurrentColumn->Name == "Column 2")
		item->pause = (e->ListViewElement->CurrentItem[2]->ToString() == "True");
	else if (e->ListViewElement->CurrentColumn->Name == "Column 3")
		item->logout = (e->ListViewElement->CurrentItem[3]->ToString() == "True");
	else if (e->ListViewElement->CurrentColumn->Name == "Column 4")
		item->value = Convert::ToInt32(e->ListViewElement->CurrentItem[4]->ToString());
}

System::Void Alerts::Alerts_Load(System::Object^  sender, System::EventArgs^  e) {
	disable_update = true;

	this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");

	loadAll();
	update_idiom();

	ListAlerts->BeginUpdate();
	array<String^>^ columnArrayItems = { "", "False", "False", "False", "0" };
	Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(-1), columnArrayItems);
	ListAlerts->Items->AddRange(gcnew array<Telerik::WinControls::UI::ListViewDataItem^>(1) { newItem });
	ListAlerts->EndUpdate();

	disable_update = false;
}
System::Void Alerts::ListAlerts_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	std::shared_ptr<AlertsStruct> item = AlertsManager::get()->GetRuleById(Convert::ToInt32(e->Item->Value));

	if (!item)
		e->Cancel = true;
	else
		AlertsManager::get()->deleteMap(Convert::ToInt32(e->Item->Value));
}

bool Alerts::save_script(std::string file_name){
	Json::Value file;
	file["version"] = 100;

	file["AlertsManager"] = AlertsManager::get()->parse_class_to_json();

	return saveJsonFile(file_name, file);
}
bool Alerts::load_script(std::string file_name){
	Json::Value file = loadJsonFile(file_name);
	if (file.isNull())
		return false;

	AlertsManager::get()->parse_json_to_class(file["AlertsManager"]);
	return true;
}

System::Void Alerts::bt_load__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::OpenFileDialog fileDialog;
	fileDialog.Filter = "Alerts|*.alerts";
	fileDialog.Title = "Open Alerts";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	AlertsManager::get()->clear();
	ListAlerts->Items->Clear();

	if (!load_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded.", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded.", "Save/Load");

	Alerts_Load(nullptr, nullptr);
}
System::Void Alerts::bt_save__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::SaveFileDialog fileDialog;
	fileDialog.Filter = "Alerts|*.alerts";
	fileDialog.Title = "Save Alerts";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	if (!save_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Saved.", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved.", "Save/Load");
}

void Alerts::update_idiom(){
	radMenuItemSave->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItemSave->Text))[0]);
	radMenuItemLoad->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItemLoad->Text))[0]);
}

System::Void Alerts::MenuItemConfigSound_Click(System::Object^  sender, System::EventArgs^  e) {
	(gcnew NeutralBot::ConfigSound)->ShowUnique();
}