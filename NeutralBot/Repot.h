#pragma once
#include "RepoterManager.h"
#include "LanguageManager.h"
#include "ManagedUtil.h"
#include "Core\LooterCore.h"
#include "GeneralManager.h"
#include "ControlManager2.h"
#include "GifManager.h"

namespace Neutral {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class Repot : public Telerik::WinControls::UI::RadForm{
	public:
		Repot(void){
			InitializeComponent();
			this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}
		
	protected:
		~Repot(){
			unique = nullptr;
			if (components)
				delete components;
		}

	protected:



	public: static void HideUnique(){
				if (unique)unique->Hide();
	}
	 Telerik::WinControls::UI::RadPanel^  radPanel1;
	 Telerik::WinControls::UI::RadMenu^  radMenu1;
	 Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
	 Telerik::WinControls::UI::RadMenuItem^  bt_save_;
	 Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
	 Telerik::WinControls::UI::RadMenuItem^  bt_load_;
	 Telerik::WinControls::UI::RadPanel^  radPanel2;
	 System::Windows::Forms::PictureBox^  itemPictureBox;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListReorganize;
	public:
	private: Telerik::WinControls::UI::RadLabel^  radLabel5;
	private: Telerik::WinControls::UI::RadDropDownList^  radDropDownList1;
	private: Telerik::WinControls::UI::RadLabel^  radLabel6;

	protected:
	protected: static Repot^ unique;

	public: static void ReloadForm(){
				unique->Repot_Load(nullptr, nullptr);
	}
	public: static void ShowUnique(){
				if (unique == nullptr)
					unique = gcnew Repot();

				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Repot();
	}
	public:	static void CloseUnique(){
				if (unique)unique->Close();
			}
	 Telerik::WinControls::UI::RadPageView^  PageRepot;
	protected:
	 Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	 Telerik::WinControls::UI::RadLabel^  radLabel4;
	 Telerik::WinControls::UI::RadLabel^  radLabel3;
	 Telerik::WinControls::UI::RadLabel^  radLabel2;
	 Telerik::WinControls::UI::RadDropDownList^  tx_nameitem;
	 Telerik::WinControls::UI::RadLabel^  radLabel1;
	 Telerik::WinControls::UI::RadSpinEditor^  tx_gpeach;
	 Telerik::WinControls::UI::RadSpinEditor^  tx_min;
	 Telerik::WinControls::UI::RadSpinEditor^  tx_max;
	 Telerik::WinControls::UI::RadButton^  bt_add;
	 Telerik::WinControls::UI::RadButton^  bt_delete;
	 System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 this->PageRepot = (gcnew Telerik::WinControls::UI::RadPageView());
				 this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->tx_gpeach = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->tx_min = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->tx_max = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->tx_nameitem = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->bt_delete = (gcnew Telerik::WinControls::UI::RadButton());
				 this->bt_add = (gcnew Telerik::WinControls::UI::RadButton());
				 this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
				 this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				 this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->bt_save_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
				 this->bt_load_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
				 this->radDropDownList1 = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel6 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->DropDownListReorganize = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->itemPictureBox = (gcnew System::Windows::Forms::PictureBox());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageRepot))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				 this->radGroupBox1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_gpeach))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_min))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_max))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_nameitem))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
				 this->radPanel1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
				 this->radPanel2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownList1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListReorganize))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->itemPictureBox))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // PageRepot
				 // 
				 this->PageRepot->Location = System::Drawing::Point(3, 3);
				 this->PageRepot->Name = L"PageRepot";
				 this->PageRepot->Size = System::Drawing::Size(306, 237);
				 this->PageRepot->TabIndex = 0;
				 this->PageRepot->Text = L"radPageView1";
				 this->PageRepot->NewPageRequested += gcnew System::EventHandler(this, &Repot::PageRepot_NewPageRequested);
				 this->PageRepot->PageRemoved += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEventArgs^ >(this, &Repot::PageRepot_PageRemoved);
				 this->PageRepot->SelectedPageChanged += gcnew System::EventHandler(this, &Repot::PageRepot_SelectedPageChanged);
				 this->PageRepot->TextChanged += gcnew System::EventHandler(this, &Repot::PageRename_TextChanged);
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->NewItemVisibility = Telerik::WinControls::UI::StripViewNewItemVisibility::End;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->ItemFitMode = Telerik::WinControls::UI::StripViewItemFitMode::None;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
				 // 
				 // radGroupBox1
				 // 
				 this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox1->Controls->Add(this->tx_gpeach);
				 this->radGroupBox1->Controls->Add(this->tx_min);
				 this->radGroupBox1->Controls->Add(this->radLabel4);
				 this->radGroupBox1->Controls->Add(this->tx_max);
				 this->radGroupBox1->Controls->Add(this->tx_nameitem);
				 this->radGroupBox1->Controls->Add(this->radLabel2);
				 this->radGroupBox1->Controls->Add(this->radLabel3);
				 this->radGroupBox1->Controls->Add(this->radLabel1);
				 this->radGroupBox1->HeaderText = L"";
				 this->radGroupBox1->Location = System::Drawing::Point(309, 84);
				 this->radGroupBox1->Name = L"radGroupBox1";
				 this->radGroupBox1->Size = System::Drawing::Size(241, 96);
				 this->radGroupBox1->TabIndex = 1;
				 // 
				 // tx_gpeach
				 // 
				 this->tx_gpeach->Location = System::Drawing::Point(40, 65);
				 this->tx_gpeach->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 9999, 0, 0, 0 });
				 this->tx_gpeach->Name = L"tx_gpeach";
				 this->tx_gpeach->Size = System::Drawing::Size(57, 20);
				 this->tx_gpeach->TabIndex = 14;
				 this->tx_gpeach->TabStop = false;
				 this->tx_gpeach->ValueChanged += gcnew System::EventHandler(this, &Repot::tx_gpeach_ValueChanged);
				 // 
				 // tx_min
				 // 
				 this->tx_min->Location = System::Drawing::Point(40, 37);
				 this->tx_min->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 9999, 0, 0, 0 });
				 this->tx_min->Name = L"tx_min";
				 this->tx_min->Size = System::Drawing::Size(75, 20);
				 this->tx_min->TabIndex = 13;
				 this->tx_min->TabStop = false;
				 this->tx_min->ValueChanged += gcnew System::EventHandler(this, &Repot::tx_min_ValueChanged);
				 // 
				 // radLabel4
				 // 
				 this->radLabel4->Location = System::Drawing::Point(7, 65);
				 this->radLabel4->Name = L"radLabel4";
				 this->radLabel4->Size = System::Drawing::Size(30, 18);
				 this->radLabel4->TabIndex = 19;
				 this->radLabel4->Text = L"Price";
				 // 
				 // tx_max
				 // 
				 this->tx_max->Location = System::Drawing::Point(161, 37);
				 this->tx_max->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 9999, 0, 0, 0 });
				 this->tx_max->Name = L"tx_max";
				 this->tx_max->Size = System::Drawing::Size(75, 20);
				 this->tx_max->TabIndex = 12;
				 this->tx_max->TabStop = false;
				 this->tx_max->ValueChanged += gcnew System::EventHandler(this, &Repot::tx_max_ValueChanged);
				 // 
				 // tx_nameitem
				 // 
				 this->tx_nameitem->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->tx_nameitem->Location = System::Drawing::Point(40, 11);
				 this->tx_nameitem->Name = L"tx_nameitem";
				 this->tx_nameitem->Size = System::Drawing::Size(196, 20);
				 this->tx_nameitem->TabIndex = 17;
				 this->tx_nameitem->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Repot::tx_nameitem_SelectedIndexChanged);
				 this->tx_nameitem->TextChanged += gcnew System::EventHandler(this, &Repot::tx_nameitem_TextChanged);
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(127, 38);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(27, 18);
				 this->radLabel2->TabIndex = 17;
				 this->radLabel2->Text = L"Max";
				 // 
				 // radLabel3
				 // 
				 this->radLabel3->Location = System::Drawing::Point(9, 39);
				 this->radLabel3->Name = L"radLabel3";
				 this->radLabel3->Size = System::Drawing::Size(25, 18);
				 this->radLabel3->TabIndex = 18;
				 this->radLabel3->Text = L"Min";
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(5, 11);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(29, 18);
				 this->radLabel1->TabIndex = 16;
				 this->radLabel1->Text = L"Item";
				 // 
				 // bt_delete
				 // 
				 this->bt_delete->Location = System::Drawing::Point(371, 56);
				 this->bt_delete->Name = L"bt_delete";
				 this->bt_delete->Size = System::Drawing::Size(174, 24);
				 this->bt_delete->TabIndex = 3;
				 this->bt_delete->Text = L"Remove";
				 this->bt_delete->Click += gcnew System::EventHandler(this, &Repot::bt_delete_Click);
				 // 
				 // bt_add
				 // 
				 this->bt_add->Location = System::Drawing::Point(371, 29);
				 this->bt_add->Name = L"bt_add";
				 this->bt_add->Size = System::Drawing::Size(174, 24);
				 this->bt_add->TabIndex = 2;
				 this->bt_add->Text = L"New";
				 this->bt_add->Click += gcnew System::EventHandler(this, &Repot::bt_add_Click);
				 // 
				 // radPanel1
				 // 
				 this->radPanel1->Controls->Add(this->radMenu1);
				 this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
				 this->radPanel1->Location = System::Drawing::Point(0, 0);
				 this->radPanel1->Name = L"radPanel1";
				 this->radPanel1->Size = System::Drawing::Size(556, 18);
				 this->radPanel1->TabIndex = 4;
				 this->radPanel1->Text = L"radPanel1";
				 // 
				 // radMenu1
				 // 
				 this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->radMenuItem1 });
				 this->radMenu1->Location = System::Drawing::Point(0, 0);
				 this->radMenu1->Name = L"radMenu1";
				 this->radMenu1->Size = System::Drawing::Size(556, 20);
				 this->radMenu1->TabIndex = 0;
				 this->radMenu1->Text = L"radMenu1";
				 // 
				 // radMenuItem1
				 // 
				 this->radMenuItem1->AccessibleDescription = L"Menu";
				 this->radMenuItem1->AccessibleName = L"Menu";
				 this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
					 this->bt_save_, this->radMenuSeparatorItem1,
						 this->bt_load_
				 });
				 this->radMenuItem1->Name = L"radMenuItem1";
				 this->radMenuItem1->Text = L"Menu";
				 // 
				 // bt_save_
				 // 
				 this->bt_save_->AccessibleDescription = L"Save";
				 this->bt_save_->AccessibleName = L"Save";
				 this->bt_save_->Name = L"bt_save_";
				 this->bt_save_->Text = L"Save";
				 this->bt_save_->Click += gcnew System::EventHandler(this, &Repot::bt_save__Click);
				 // 
				 // radMenuSeparatorItem1
				 // 
				 this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
				 // 
				 // bt_load_
				 // 
				 this->bt_load_->AccessibleDescription = L"Load";
				 this->bt_load_->AccessibleName = L"Load";
				 this->bt_load_->Name = L"bt_load_";
				 this->bt_load_->Text = L"Load";
				 this->bt_load_->Click += gcnew System::EventHandler(this, &Repot::bt_load__Click);
				 // 
				 // radPanel2
				 // 
				 this->radPanel2->Controls->Add(this->radDropDownList1);
				 this->radPanel2->Controls->Add(this->radLabel6);
				 this->radPanel2->Controls->Add(this->DropDownListReorganize);
				 this->radPanel2->Controls->Add(this->radLabel5);
				 this->radPanel2->Controls->Add(this->itemPictureBox);
				 this->radPanel2->Controls->Add(this->PageRepot);
				 this->radPanel2->Controls->Add(this->radGroupBox1);
				 this->radPanel2->Controls->Add(this->bt_delete);
				 this->radPanel2->Controls->Add(this->bt_add);
				 this->radPanel2->Dock = System::Windows::Forms::DockStyle::Bottom;
				 this->radPanel2->Location = System::Drawing::Point(0, 24);
				 this->radPanel2->Name = L"radPanel2";
				 this->radPanel2->Size = System::Drawing::Size(556, 243);
				 this->radPanel2->TabIndex = 5;
				 this->radPanel2->Text = L"radPanel2";
				 // 
				 // radDropDownList1
				 // 
				 radListDataItem1->Text = L"True";
				 radListDataItem2->Text = L"False";
				 this->radDropDownList1->Items->Add(radListDataItem1);
				 this->radDropDownList1->Items->Add(radListDataItem2);
				 this->radDropDownList1->Location = System::Drawing::Point(436, 217);
				 this->radDropDownList1->Name = L"radDropDownList1";
				 this->radDropDownList1->Size = System::Drawing::Size(109, 20);
				 this->radDropDownList1->TabIndex = 8;
				 this->radDropDownList1->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Repot::radDropDownList1_SelectedIndexChanged);
				 // 
				 // radLabel6
				 // 
				 this->radLabel6->Location = System::Drawing::Point(310, 217);
				 this->radLabel6->Name = L"radLabel6";
				 this->radLabel6->Size = System::Drawing::Size(112, 18);
				 this->radLabel6->TabIndex = 7;
				 this->radLabel6->Text = L"minimize containers: ";
				 // 
				 // DropDownListReorganize
				 // 
				 radListDataItem3->Text = L"True";
				 radListDataItem4->Text = L"False";
				 this->DropDownListReorganize->Items->Add(radListDataItem3);
				 this->DropDownListReorganize->Items->Add(radListDataItem4);
				 this->DropDownListReorganize->Location = System::Drawing::Point(436, 191);
				 this->DropDownListReorganize->Name = L"DropDownListReorganize";
				 this->DropDownListReorganize->Size = System::Drawing::Size(109, 20);
				 this->DropDownListReorganize->TabIndex = 6;
				 this->DropDownListReorganize->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Repot::DropDownListReorganize_SelectedIndexChanged);
				 // 
				 // radLabel5
				 // 
				 this->radLabel5->Location = System::Drawing::Point(310, 191);
				 this->radLabel5->Name = L"radLabel5";
				 this->radLabel5->Size = System::Drawing::Size(120, 18);
				 this->radLabel5->TabIndex = 5;
				 this->radLabel5->Text = L"reorganize containers: ";
				 // 
				 // itemPictureBox
				 // 
				 this->itemPictureBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				 this->itemPictureBox->Location = System::Drawing::Point(310, 26);
				 this->itemPictureBox->Name = L"itemPictureBox";
				 this->itemPictureBox->Size = System::Drawing::Size(55, 55);
				 this->itemPictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				 this->itemPictureBox->TabIndex = 4;
				 this->itemPictureBox->TabStop = false;
				 // 
				 // Repot
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(556, 267);
				 this->Controls->Add(this->radPanel2);
				 this->Controls->Add(this->radPanel1);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->MaximizeBox = false;
				 this->Name = L"Repot";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"Repoter";
				 this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Repot::Repot_FormClosing);
				 this->Load += gcnew System::EventHandler(this, &Repot::Repot_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageRepot))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				 this->radGroupBox1->ResumeLayout(false);
				 this->radGroupBox1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_gpeach))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_min))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_max))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_nameitem))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
				 this->radPanel1->ResumeLayout(false);
				 this->radPanel1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
				 this->radPanel2->ResumeLayout(false);
				 this->radPanel2->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownList1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListReorganize))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->itemPictureBox))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion
			 System::String^ text;
			 bool disable_item_update;
			 void PageAddNewPage(std::string PageName);
			 void addItemOnView(String^ idBp, String^ Item, int min, int max, int gpeach);
			 void loadItemOnView(std::string id_);
			 void loadAll();
			 void updateSelectedData();

			 Telerik::WinControls::UI::RadListView^ getItemListView(){
				 auto selected_page = this->PageRepot->SelectedPage;

				 if (!selected_page)
					 return nullptr;

				 array<Control^>^ getRadListView = selected_page->Controls->Find("list_" + PageRepot->SelectedPage->Text, true);

				 if (!getRadListView->Length)
					 return nullptr;

				 return (Telerik::WinControls::UI::RadListView^) getRadListView[0];
			 }

	 System::Void ItemListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
	 System::Void ItemListViewItemChanged(System::Object^  sender, System::EventArgs^  e);
	 System::Void PageRepot_NewPageRequested(System::Object^  sender, System::EventArgs^  e);
	 System::Void PageRepot_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e);
	 System::Void Repot_Load(System::Object^  sender, System::EventArgs^  e);

	 System::Void bt_delete_Click(System::Object^  sender, System::EventArgs^  e);
	 System::Void bt_add_Click(System::Object^  sender, System::EventArgs^  e);

	 System::Void Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e);
	 System::Void PageRename_TextChanged(System::Object^  sender, System::EventArgs^ e);
	 
	 System::Void tx_nameitem_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_item_update)
					 return;

				 Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

				 if (!radListview)
					 return;

				 if (!radListview->SelectedItem)
					 return;

				 Telerik::WinControls::UI::ListViewDataItem^ radItem = (Telerik::WinControls::UI::ListViewDataItem^)radListview->SelectedItem;
				 radListview->SelectedItem[0] = tx_nameitem->Text;
				 radListview->SelectedItem->Image = managed_util::get_item_image(tx_nameitem->Text,20);
				 
				 std::shared_ptr<RepotId> srd_bp = RepoterManager::get()->getRepoIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
				 srd_bp->set_Item(managed_util::fromSS(radListview->SelectedItem->Text), managed_util::fromSS(tx_nameitem->Text));

				 updateSelectedData();

				 ControlManager::get()->changePropertyRepoter(managed_util::fromSS(tx_nameitem->Text), managed_util::fromSS(PageRepot->SelectedPage->Text + radListview->SelectedItem->Text + "combo"), "text");
	}
	 System::Void tx_max_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

				 if (!radListview)
					 return;

				 if (!radListview->SelectedItem)
					 return;

				 std::shared_ptr<RepotId> srd_bp = RepoterManager::get()->getRepoIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
				 srd_bp->set_Max(managed_util::fromSS(radListview->SelectedItem->Text), Convert::ToInt32(tx_max->Value));
				
				 ControlManager::get()->changePropertyRepoter(managed_util::fromSS(tx_max->Text), managed_util::fromSS(PageRepot->SelectedPage->Text + radListview->SelectedItem->Text + "spinmax"), "value");
	}
	 System::Void tx_min_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

				 if (!radListview)
					 return;

				 if (!radListview->SelectedItem)
					 return;

				 std::shared_ptr<RepotId> srd_bp = RepoterManager::get()->getRepoIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
				 srd_bp->set_Min(managed_util::fromSS(radListview->SelectedItem->Text), Convert::ToInt32(tx_min->Value));
				 
				 ControlManager::get()->changePropertyRepoter(managed_util::fromSS(tx_min->Text), managed_util::fromSS(PageRepot->SelectedPage->Text + radListview->SelectedItem->Text + "spinmin"), "value");
	}
	 System::Void tx_gpeach_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

				 if (!radListview)
					 return;

				 if (!radListview->SelectedItem)
					 return;

				 std::shared_ptr<RepotId> srd_bp = RepoterManager::get()->getRepoIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
				 std::string item_name = srd_bp->get_Item(managed_util::fromSS(radListview->SelectedItem->Text));
				 uint32_t item_id = ItemsManager::get()->getitem_idFromName(item_name);
				
				 std::shared_ptr<ItemsInfoLooted> itemsInfo = LooterCore::get()->getRuleItemsLooted(item_id);

				 if (itemsInfo)
					itemsInfo->set_buy_price(Convert::ToInt32(tx_gpeach->Value));				

				 srd_bp->set_GpEach(managed_util::fromSS(radListview->SelectedItem->Text), Convert::ToInt32(tx_gpeach->Value));
	}

			 bool save_script(std::string file_name){
				 Json::Value file;
				 file["version"] = 0100;

				 file["RepoterManager"] = RepoterManager::get()->parse_class_to_json();

				 return saveJsonFile(file_name, file);
			 }
			 bool load_script(std::string file_name){
				 Json::Value file = loadJsonFile(file_name);
				 if (file.isNull())
					 return false;

				 RepoterManager::get()->parse_json_to_class(file["RepoterManager"]);
				 return true;
			 }
			  
	 System::Void bt_save__Click(System::Object^  sender, System::EventArgs^  e) {
				 System::Windows::Forms::SaveFileDialog fileDialog;
				 fileDialog.Filter = "Repoter Script|*.Repot";
				 fileDialog.Title = "Save Script";

				 if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 if (!save_script(managed_util::fromSS(fileDialog.FileName)))
					  Telerik::WinControls::RadMessageBox::Show(this,"Error Saved", "Save/Load");
				 else
					  Telerik::WinControls::RadMessageBox::Show(this,"Sucess Saved", "Save/Load");
	}
	 System::Void bt_load__Click(System::Object^  sender, System::EventArgs^  e) {
				 System::Windows::Forms::OpenFileDialog fileDialog;
				 fileDialog.Filter = "Repoter Script|*.Repot";
				 fileDialog.Title = "Open Script";

				 if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 RepoterManager::get()->clear();
				 PageRepot->Pages->Clear();
				  
				 if (!load_script(managed_util::fromSS(fileDialog.FileName)))
					  Telerik::WinControls::RadMessageBox::Show(this,"Error Loaded", "Save/Load");
				 else
					  Telerik::WinControls::RadMessageBox::Show(this,"Sucess Loaded", "Save/Load");

				 loadAll();
	}

	 System::Void tx_nameitem_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		itemPictureBox->Image = managed_util::get_item_image(tx_nameitem->Text, itemPictureBox->Height);
		update_current_item_value();
	}

	 void close_button(){
		 if (PageRepot->Pages->Count <= 1)
			 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
		 else
			 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
	 }
	 void add_in_setup(std::string id);
private: System::Void PageRepot_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
			 auto listview = getItemListView();

			 if (!listview)
				 return;

			 if (listview->Items->Count <= 0)
				 radGroupBox1->Enabled = false;
			 else
				 radGroupBox1->Enabled = true;

			 updateSelectedData();
}

		 void update_idiom(){
			 radLabel4->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel4->Text))[0]);
			 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
			 radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
			 radLabel5->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel5->Text))[0]);
			 radLabel6->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel6->Text))[0]);
			 radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
			 bt_delete->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_delete->Text))[0]);
			 bt_add->Text	= gcnew String(&GET_TR(managed_util::fromSS(bt_add->Text))[0]);
			 bt_save_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save_->Text))[0]);
			 bt_load_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load_->Text))[0]);			 
		 }

private: System::Void Repot_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			 if (GeneralManager::get()->get_practice_mode()){
				 this->Hide();
				 e->Cancel = true;
				 return;
			 }
}

		 void update_current_item_value(){
			 return;
			 if (!PageRepot->SelectedPage)
				 return;

			 Telerik::WinControls::UI::RadListView^ radlist = getItemListView();

			 if (!radlist->SelectedItem)
				 return;

			 if (!radGroupBox1->Enabled)
				 radGroupBox1->Enabled = true;

			 std::shared_ptr<RepotId> mapId = RepoterManager::get()->getRepoIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
			 std::string idMap = managed_util::fromSS(radlist->SelectedItem->Text);
			 std::shared_ptr<RepotItens> mapItens = mapId->getRepotItensRule(idMap);
			 
			std::shared_ptr<ItemsInfoLooted> itemsInfo = LooterCore::get()->getRuleItemsLooted(mapItens->item_id);
			if (itemsInfo && itemsInfo->get_buy_price() != 0)
				tx_gpeach->Value = itemsInfo->get_buy_price();
			 
		 }

private: System::Void DropDownListReorganize_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
			 if (disable_item_update)
				 return;

			 bool temp = false;
			 if (DropDownListReorganize->Text->ToLower() == "true")
				 temp = true;

			 RepoterManager::get()->set_reorganize_containers(temp);
}
private: System::Void radDropDownList1_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
			 if (disable_item_update)
				 return;

			 bool temp = false;
			 if (radDropDownList1->Text->ToLower() == "true")
				 temp = true;

			 RepoterManager::get()->set_minimize_all_containers(temp);
}
};
}