#pragma once
#include "ManagedUtil.h"
#include "HotkeysManager.h"
#include "InputWait.h"
#include "LuaBackground.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class HotkeyManager :public Telerik::WinControls::UI::RadForm
	{
	public:
		HotkeyManager(void);


	protected: static HotkeyManager^ unique;

	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
		if (unique == nullptr)
			unique = gcnew HotkeyManager();
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
		unique->BringToFront();
		unique->Show();
	}
	public: static void ReloadForm(){
				unique->HotkeyManager_Load(nullptr, nullptr);
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew HotkeyManager();
	}
	static void CloseUnique(){
		if(unique)unique->Close();
	}
	protected:
		~HotkeyManager()
		{
			SaveAll(nullptr);
			if (components)
			{
				delete components;
			}
			unique = nullptr;
		}

	 bool notifications_enabled;
	 Telerik::WinControls::UI::RadButton^  radButtonNewHotkey;
	 Telerik::WinControls::UI::RadButton^  radButtonRemoveHotkey;
	 Telerik::WinControls::UI::RadGroupBox^  radGroupBoxEdit;
	 Telerik::WinControls::UI::RadLabel^  radLabel7;
	 Telerik::WinControls::UI::RadLabel^  radLabel6;
	 Telerik::WinControls::UI::RadLabel^  radLabel5;
	 Telerik::WinControls::UI::RadLabel^  radLabel4;
	 Telerik::WinControls::UI::RadLabel^  radLabel3;
	 Telerik::WinControls::UI::RadLabel^  radLabel2;
	 Telerik::WinControls::UI::RadLabel^  radLabel1;
	 Telerik::WinControls::UI::RadDropDownList^  radDropDownListCapsLock;
	 Telerik::WinControls::UI::RadDropDownList^  radDropDownListInsert;
	 Telerik::WinControls::UI::RadDropDownList^  radDropDownListScrollLock;
	 Telerik::WinControls::UI::RadDropDownList^  radDropDownListNumLock;
	 Telerik::WinControls::UI::RadDropDownList^  radDropDownListAlt;
	 Telerik::WinControls::UI::RadDropDownList^  radDropDownListShift;
	 Telerik::WinControls::UI::RadDropDownList^  radDropDownListCtrl;

	 Telerik::WinControls::UI::RadCheckBox^  radCheckBoxState;





	 Telerik::WinControls::UI::RadButton^  radButtonUpdateHotkey;
	 Telerik::WinControls::UI::RadPanel^  radPanelEdit;

	 Telerik::WinControls::UI::RadListView^  radListViewHotkeys;
	 Telerik::WinControls::UI::RadButton^  radButton1;
	 Telerik::WinControls::UI::RadTextBox^  radTextBoxKeys;
	 Telerik::WinControls::UI::RadTextBox^  radTextBoxName;
	 Telerik::WinControls::UI::RadMenu^  radMenu1;
	 Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
	 Telerik::WinControls::UI::RadMenuItem^  radMenuItemExport;
	 Telerik::WinControls::UI::RadMenuItem^  radMenuItemImport;
	 Telerik::WinControls::UI::RadPanel^  radPanel1;
	 Telerik::WinControls::UI::RadPanel^  radPanel2;

	protected:	System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Id",
				L"Id"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Hotkey",
				L"Hotkey"));
			this->radButtonNewHotkey = (gcnew Telerik::WinControls::UI::RadButton());
			this->radButtonRemoveHotkey = (gcnew Telerik::WinControls::UI::RadButton());
			this->radGroupBoxEdit = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->radTextBoxName = (gcnew Telerik::WinControls::UI::RadTextBox());
			this->radTextBoxKeys = (gcnew Telerik::WinControls::UI::RadTextBox());
			this->radCheckBoxState = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->radLabel7 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel6 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radDropDownListCapsLock = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radDropDownListInsert = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radDropDownListScrollLock = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radDropDownListNumLock = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radDropDownListAlt = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radDropDownListShift = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radDropDownListCtrl = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radButtonUpdateHotkey = (gcnew Telerik::WinControls::UI::RadButton());
			this->radPanelEdit = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radButton1 = (gcnew Telerik::WinControls::UI::RadButton());
			this->radListViewHotkeys = (gcnew Telerik::WinControls::UI::RadListView());
			this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
			this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItemExport = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItemImport = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonNewHotkey))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonRemoveHotkey))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBoxEdit))->BeginInit();
			this->radGroupBoxEdit->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTextBoxName))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTextBoxKeys))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckBoxState))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListCapsLock))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListInsert))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListScrollLock))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListNumLock))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListAlt))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListShift))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListCtrl))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonUpdateHotkey))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelEdit))->BeginInit();
			this->radPanelEdit->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListViewHotkeys))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// radButtonNewHotkey
			// 
			this->radButtonNewHotkey->Location = System::Drawing::Point(184, 5);
			this->radButtonNewHotkey->Name = L"radButtonNewHotkey";
			this->radButtonNewHotkey->Size = System::Drawing::Size(123, 24);
			this->radButtonNewHotkey->TabIndex = 2;
			this->radButtonNewHotkey->Text = L"New Hotkey";
			this->radButtonNewHotkey->Click += gcnew System::EventHandler(this, &HotkeyManager::radButtonNewHotkey_Click);
			// 
			// radButtonRemoveHotkey
			// 
			this->radButtonRemoveHotkey->Location = System::Drawing::Point(313, 5);
			this->radButtonRemoveHotkey->Name = L"radButtonRemoveHotkey";
			this->radButtonRemoveHotkey->Size = System::Drawing::Size(108, 24);
			this->radButtonRemoveHotkey->TabIndex = 3;
			this->radButtonRemoveHotkey->Text = L"Remove Hotkey";
			this->radButtonRemoveHotkey->Click += gcnew System::EventHandler(this, &HotkeyManager::radButtonRemoveHotkey_Click);
			// 
			// radGroupBoxEdit
			// 
			this->radGroupBoxEdit->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBoxEdit->Controls->Add(this->radTextBoxName);
			this->radGroupBoxEdit->Controls->Add(this->radTextBoxKeys);
			this->radGroupBoxEdit->Controls->Add(this->radCheckBoxState);
			this->radGroupBoxEdit->Controls->Add(this->radLabel7);
			this->radGroupBoxEdit->Controls->Add(this->radLabel6);
			this->radGroupBoxEdit->Controls->Add(this->radLabel5);
			this->radGroupBoxEdit->Controls->Add(this->radLabel4);
			this->radGroupBoxEdit->Controls->Add(this->radLabel3);
			this->radGroupBoxEdit->Controls->Add(this->radLabel2);
			this->radGroupBoxEdit->Controls->Add(this->radLabel1);
			this->radGroupBoxEdit->Controls->Add(this->radDropDownListCapsLock);
			this->radGroupBoxEdit->Controls->Add(this->radDropDownListInsert);
			this->radGroupBoxEdit->Controls->Add(this->radDropDownListScrollLock);
			this->radGroupBoxEdit->Controls->Add(this->radDropDownListNumLock);
			this->radGroupBoxEdit->Controls->Add(this->radDropDownListAlt);
			this->radGroupBoxEdit->Controls->Add(this->radDropDownListShift);
			this->radGroupBoxEdit->Controls->Add(this->radDropDownListCtrl);
			this->radGroupBoxEdit->HeaderText = L"Properties";
			this->radGroupBoxEdit->Location = System::Drawing::Point(184, 67);
			this->radGroupBoxEdit->Name = L"radGroupBoxEdit";
			this->radGroupBoxEdit->Size = System::Drawing::Size(237, 218);
			this->radGroupBoxEdit->TabIndex = 0;
			this->radGroupBoxEdit->Text = L"Properties";
			// 
			// radTextBoxName
			// 
			this->radTextBoxName->Location = System::Drawing::Point(84, 19);
			this->radTextBoxName->Name = L"radTextBoxName";
			this->radTextBoxName->Size = System::Drawing::Size(146, 20);
			this->radTextBoxName->TabIndex = 21;
			this->radTextBoxName->TextChanged += gcnew System::EventHandler(this, &HotkeyManager::radTextBoxName_TextChanged);
			// 
			// radTextBoxKeys
			// 
			this->radTextBoxKeys->Enabled = false;
			this->radTextBoxKeys->Location = System::Drawing::Point(126, 191);
			this->radTextBoxKeys->Name = L"radTextBoxKeys";
			this->radTextBoxKeys->Size = System::Drawing::Size(104, 20);
			this->radTextBoxKeys->TabIndex = 20;
			this->radTextBoxKeys->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// radCheckBoxState
			// 
			this->radCheckBoxState->Location = System::Drawing::Point(16, 21);
			this->radCheckBoxState->Name = L"radCheckBoxState";
			this->radCheckBoxState->Size = System::Drawing::Size(60, 18);
			this->radCheckBoxState->TabIndex = 19;
			this->radCheckBoxState->Text = L"Enabled";
			this->radCheckBoxState->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &HotkeyManager::radCheckBoxState_ToggleStateChanged);
			// 
			// radLabel7
			// 
			this->radLabel7->Location = System::Drawing::Point(128, 88);
			this->radLabel7->Name = L"radLabel7";
			this->radLabel7->Size = System::Drawing::Size(53, 18);
			this->radLabel7->TabIndex = 17;
			this->radLabel7->Text = L"CapsLock";
			// 
			// radLabel6
			// 
			this->radLabel6->Location = System::Drawing::Point(129, 130);
			this->radLabel6->Name = L"radLabel6";
			this->radLabel6->Size = System::Drawing::Size(34, 18);
			this->radLabel6->TabIndex = 16;
			this->radLabel6->Text = L"Insert";
			// 
			// radLabel5
			// 
			this->radLabel5->Location = System::Drawing::Point(128, 45);
			this->radLabel5->Name = L"radLabel5";
			this->radLabel5->Size = System::Drawing::Size(56, 18);
			this->radLabel5->TabIndex = 15;
			this->radLabel5->Text = L"ScrollLock";
			// 
			// radLabel4
			// 
			this->radLabel4->Location = System::Drawing::Point(15, 171);
			this->radLabel4->Name = L"radLabel4";
			this->radLabel4->Size = System::Drawing::Size(54, 18);
			this->radLabel4->TabIndex = 14;
			this->radLabel4->Text = L"NumLock";
			// 
			// radLabel3
			// 
			this->radLabel3->Location = System::Drawing::Point(16, 129);
			this->radLabel3->Name = L"radLabel3";
			this->radLabel3->Size = System::Drawing::Size(20, 18);
			this->radLabel3->TabIndex = 13;
			this->radLabel3->Text = L"Alt";
			// 
			// radLabel2
			// 
			this->radLabel2->Location = System::Drawing::Point(15, 87);
			this->radLabel2->Name = L"radLabel2";
			this->radLabel2->Size = System::Drawing::Size(29, 18);
			this->radLabel2->TabIndex = 12;
			this->radLabel2->Text = L"Shift";
			// 
			// radLabel1
			// 
			this->radLabel1->Location = System::Drawing::Point(16, 45);
			this->radLabel1->Name = L"radLabel1";
			this->radLabel1->Size = System::Drawing::Size(24, 18);
			this->radLabel1->TabIndex = 11;
			this->radLabel1->Text = L"Ctrl";
			// 
			// radDropDownListCapsLock
			// 
			this->radDropDownListCapsLock->Location = System::Drawing::Point(126, 108);
			this->radDropDownListCapsLock->Name = L"radDropDownListCapsLock";
			this->radDropDownListCapsLock->Size = System::Drawing::Size(104, 20);
			this->radDropDownListCapsLock->TabIndex = 10;
			this->radDropDownListCapsLock->Text = L"any";
			this->radDropDownListCapsLock->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &HotkeyManager::radDropDownListCapsLock_SelectedIndexChanged);
			// 
			// radDropDownListInsert
			// 
			this->radDropDownListInsert->Location = System::Drawing::Point(126, 150);
			this->radDropDownListInsert->Name = L"radDropDownListInsert";
			this->radDropDownListInsert->Size = System::Drawing::Size(104, 20);
			this->radDropDownListInsert->TabIndex = 9;
			this->radDropDownListInsert->Text = L"any";
			this->radDropDownListInsert->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &HotkeyManager::radDropDownListInsert_SelectedIndexChanged);
			// 
			// radDropDownListScrollLock
			// 
			this->radDropDownListScrollLock->Location = System::Drawing::Point(126, 65);
			this->radDropDownListScrollLock->Name = L"radDropDownListScrollLock";
			this->radDropDownListScrollLock->Size = System::Drawing::Size(104, 20);
			this->radDropDownListScrollLock->TabIndex = 8;
			this->radDropDownListScrollLock->Text = L"any";
			this->radDropDownListScrollLock->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &HotkeyManager::radDropDownListScrollLock_SelectedIndexChanged);
			// 
			// radDropDownListNumLock
			// 
			this->radDropDownListNumLock->Location = System::Drawing::Point(13, 191);
			this->radDropDownListNumLock->Name = L"radDropDownListNumLock";
			this->radDropDownListNumLock->Size = System::Drawing::Size(104, 20);
			this->radDropDownListNumLock->TabIndex = 7;
			this->radDropDownListNumLock->Text = L"any";
			this->radDropDownListNumLock->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &HotkeyManager::radDropDownListNumLock_SelectedIndexChanged);
			// 
			// radDropDownListAlt
			// 
			this->radDropDownListAlt->Location = System::Drawing::Point(13, 149);
			this->radDropDownListAlt->Name = L"radDropDownListAlt";
			this->radDropDownListAlt->Size = System::Drawing::Size(104, 20);
			this->radDropDownListAlt->TabIndex = 6;
			this->radDropDownListAlt->Text = L"any";
			this->radDropDownListAlt->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &HotkeyManager::radDropDownListAlt_SelectedIndexChanged);
			// 
			// radDropDownListShift
			// 
			this->radDropDownListShift->Location = System::Drawing::Point(14, 107);
			this->radDropDownListShift->Name = L"radDropDownListShift";
			this->radDropDownListShift->Size = System::Drawing::Size(104, 20);
			this->radDropDownListShift->TabIndex = 5;
			this->radDropDownListShift->Text = L"any";
			this->radDropDownListShift->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &HotkeyManager::radDropDownListShift_SelectedIndexChanged);
			// 
			// radDropDownListCtrl
			// 
			this->radDropDownListCtrl->Location = System::Drawing::Point(13, 65);
			this->radDropDownListCtrl->Name = L"radDropDownListCtrl";
			this->radDropDownListCtrl->Size = System::Drawing::Size(104, 20);
			this->radDropDownListCtrl->TabIndex = 4;
			this->radDropDownListCtrl->Text = L"any";
			this->radDropDownListCtrl->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &HotkeyManager::radDropDownListCtrl_SelectedIndexChanged);
			// 
			// radButtonUpdateHotkey
			// 
			this->radButtonUpdateHotkey->Location = System::Drawing::Point(184, 37);
			this->radButtonUpdateHotkey->Name = L"radButtonUpdateHotkey";
			this->radButtonUpdateHotkey->Size = System::Drawing::Size(123, 24);
			this->radButtonUpdateHotkey->TabIndex = 3;
			this->radButtonUpdateHotkey->Text = L"Update Properties";
			this->radButtonUpdateHotkey->Click += gcnew System::EventHandler(this, &HotkeyManager::radButtonUpdateHotkey_Click);
			// 
			// radPanelEdit
			// 
			this->radPanelEdit->Controls->Add(this->radButton1);
			this->radPanelEdit->Controls->Add(this->radListViewHotkeys);
			this->radPanelEdit->Controls->Add(this->radButtonUpdateHotkey);
			this->radPanelEdit->Controls->Add(this->radButtonNewHotkey);
			this->radPanelEdit->Controls->Add(this->radGroupBoxEdit);
			this->radPanelEdit->Controls->Add(this->radButtonRemoveHotkey);
			this->radPanelEdit->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanelEdit->Location = System::Drawing::Point(0, 0);
			this->radPanelEdit->Name = L"radPanelEdit";
			this->radPanelEdit->Size = System::Drawing::Size(427, 290);
			this->radPanelEdit->TabIndex = 4;
			this->radPanelEdit->Text = L"radPanel1";
			// 
			// radButton1
			// 
			this->radButton1->Location = System::Drawing::Point(313, 37);
			this->radButton1->Name = L"radButton1";
			this->radButton1->Size = System::Drawing::Size(108, 24);
			this->radButton1->TabIndex = 4;
			this->radButton1->Text = L"Edit Script";
			this->radButton1->Click += gcnew System::EventHandler(this, &HotkeyManager::radButton1_Click);
			// 
			// radListViewHotkeys
			// 
			listViewDetailColumn1->HeaderText = L"Id";
			listViewDetailColumn1->MaxWidth = 177;
			listViewDetailColumn1->MinWidth = 177;
			listViewDetailColumn1->Visible = false;
			listViewDetailColumn1->Width = 177;
			listViewDetailColumn2->HeaderText = L"Hotkey";
			listViewDetailColumn2->MaxWidth = 174;
			listViewDetailColumn2->MinWidth = 174;
			listViewDetailColumn2->Width = 174;
			this->radListViewHotkeys->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(2) {
				listViewDetailColumn1,
					listViewDetailColumn2
			});
			this->radListViewHotkeys->ItemSpacing = -1;
			this->radListViewHotkeys->Location = System::Drawing::Point(3, 3);
			this->radListViewHotkeys->Name = L"radListViewHotkeys";
			this->radListViewHotkeys->Size = System::Drawing::Size(175, 282);
			this->radListViewHotkeys->TabIndex = 20;
			this->radListViewHotkeys->Text = L"radListView1";
			this->radListViewHotkeys->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			this->radListViewHotkeys->SelectedItemChanged += gcnew System::EventHandler(this, &HotkeyManager::radListViewHotkeys_SelectedItemChanged);
			this->radListViewHotkeys->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &HotkeyManager::radListViewHotkeys_ItemRemoving);
			this->radListViewHotkeys->ItemRemoved += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &HotkeyManager::radListViewHotkeys_ItemRemoved);
			// 
			// radMenu1
			// 
			this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->radMenuItem1 });
			this->radMenu1->Location = System::Drawing::Point(0, 0);
			this->radMenu1->Name = L"radMenu1";
			this->radMenu1->Size = System::Drawing::Size(427, 20);
			this->radMenu1->TabIndex = 5;
			this->radMenu1->Text = L"radMenu1";
			// 
			// radMenuItem1
			// 
			this->radMenuItem1->AccessibleDescription = L"Menu";
			this->radMenuItem1->AccessibleName = L"Menu";
			this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->radMenuItemExport, this->radMenuItemImport });
			this->radMenuItem1->Name = L"radMenuItem1";
			this->radMenuItem1->Text = L"Menu";
			// 
			// radMenuItemExport
			// 
			this->radMenuItemExport->AccessibleDescription = L"radMenuItemExport";
			this->radMenuItemExport->AccessibleName = L"radMenuItemExport";
			this->radMenuItemExport->Name = L"radMenuItemExport";
			this->radMenuItemExport->Text = L"Export";
			this->radMenuItemExport->Click += gcnew System::EventHandler(this, &HotkeyManager::radMenuItemExport_Click);
			// 
			// radMenuItemImport
			// 
			this->radMenuItemImport->AccessibleDescription = L"radMenuItemImport";
			this->radMenuItemImport->AccessibleName = L"radMenuItemImport";
			this->radMenuItemImport->Name = L"radMenuItemImport";
			this->radMenuItemImport->Text = L"Import";
			this->radMenuItemImport->Click += gcnew System::EventHandler(this, &HotkeyManager::radMenuItemImport_Click);
			// 
			// radPanel1
			// 
			this->radPanel1->Controls->Add(this->radMenu1);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel1->Location = System::Drawing::Point(0, 0);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(427, 20);
			this->radPanel1->TabIndex = 6;
			this->radPanel1->Text = L"radPanel1";
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radPanelEdit);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel2->Location = System::Drawing::Point(0, 20);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(427, 290);
			this->radPanel2->TabIndex = 7;
			this->radPanel2->Text = L"radPanel2";
			// 
			// HotkeyManager
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(427, 310);
			this->Controls->Add(this->radPanel2);
			this->Controls->Add(this->radPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"HotkeyManager";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"Hotkeys";
			this->Load += gcnew System::EventHandler(this, &HotkeyManager::HotkeyManager_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonNewHotkey))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonRemoveHotkey))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBoxEdit))->EndInit();
			this->radGroupBoxEdit->ResumeLayout(false);
			this->radGroupBoxEdit->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTextBoxName))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTextBoxKeys))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckBoxState))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListCapsLock))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListInsert))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListScrollLock))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListNumLock))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListAlt))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListShift))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownListCtrl))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonUpdateHotkey))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelEdit))->EndInit();
			this->radPanelEdit->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListViewHotkeys))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			this->radPanel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
 System::Void radButtonNewHotkey_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void radButtonRemoveHotkey_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void radButtonUpdateHotkey_Click(System::Object^  sender, System::EventArgs^  e);

	void updateCurrentHotkey();
	void addInterfaceHotkey(Hotkey* hk);
	void setEditMode();
	void setInputMode();

 System::Void radListViewHotkeys_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e);
 std::shared_ptr<Hotkey> getSelectedHotkey();		 
 System::Void radDropDownListCtrl_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
 System::Void radDropDownListShift_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
 System::Void radDropDownListAlt_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
 System::Void radDropDownListNumLock_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
 System::Void radDropDownListInsert_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
 System::Void radDropDownListCapsLock_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
 System::Void radDropDownListScrollLock_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
 System::Void radCheckBoxState_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
 
 void loadHotkey(std::shared_ptr<Hotkey> hotkey);

 System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void radTextBoxName_TextChanged(System::Object^  sender, System::EventArgs^  e);

 void LoadAll(String^ path);
 void SaveAll(String^ path);
 void SetDefault();

 System::Void radMenuItemExport_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void radMenuItemImport_Click(System::Object^  sender, System::EventArgs^  e);
 System::Void radListViewHotkeys_ItemRemoved(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e);

 bool disable_update = false;
 void LoadInterfaceData();
		 
 System::Void radListViewHotkeys_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	if (!e->ListViewElement->SelectedItem)
		return;

	if (!e->ListViewElement->SelectedItem["Id"])
		return; 

	HotkeysManager::get()->remove_hotkey((uint32_t)e->ListViewElement->SelectedItem["Id"]);
}

 void update_idiom(){
	 radButtonNewHotkey->Text = gcnew String(&GET_TR(managed_util::fromSS(radButtonNewHotkey->Text))[0]);
	 radButtonRemoveHotkey->Text = gcnew String(&GET_TR(managed_util::fromSS(radButtonRemoveHotkey->Text))[0]);
	 radGroupBoxEdit->Text = gcnew String(&GET_TR(managed_util::fromSS(radGroupBoxEdit->Text))[0]);
	 radCheckBoxState->Text = gcnew String(&GET_TR(managed_util::fromSS(radCheckBoxState->Text))[0]);
	 radLabel7->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel7->Text))[0]);
	 radLabel6->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel6->Text))[0]);
	 radLabel5->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel5->Text))[0]);
	 radLabel4->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel4->Text))[0]);
	 radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
	 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
	 radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
	 radDropDownListCapsLock->Text = gcnew String(&GET_TR(managed_util::fromSS(radDropDownListCapsLock->Text))[0]);
	 radDropDownListInsert->Text = gcnew String(&GET_TR(managed_util::fromSS(radDropDownListInsert->Text))[0]);
	 radDropDownListScrollLock->Text = gcnew String(&GET_TR(managed_util::fromSS(radDropDownListScrollLock->Text))[0]);
	 radDropDownListNumLock->Text = gcnew String(&GET_TR(managed_util::fromSS(radDropDownListNumLock->Text))[0]);
	 radDropDownListAlt->Text = gcnew String(&GET_TR(managed_util::fromSS(radDropDownListAlt->Text))[0]);
	 radDropDownListShift->Text = gcnew String(&GET_TR(managed_util::fromSS(radDropDownListShift->Text))[0]);
	 radDropDownListCtrl->Text = gcnew String(&GET_TR(managed_util::fromSS(radDropDownListCtrl->Text))[0]);
	 radButtonUpdateHotkey->Text = gcnew String(&GET_TR(managed_util::fromSS(radButtonUpdateHotkey->Text))[0]);
	 radButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton1->Text))[0]);
	 radMenuItemExport->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItemExport->Text))[0]);
	 radMenuItemImport->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItemImport->Text))[0]);
 }

private: System::Void HotkeyManager_Load(System::Object^  sender, System::EventArgs^  e) {
			 update_idiom();
}

};
}
