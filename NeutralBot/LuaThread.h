#pragma once
#include "ThreadHandler.h"
#include "CoreBase.h"

#pragma pack(push,1)
class LuaThread : public CoreBase{
	std::vector<std::shared_ptr<ThreadHandler>> existent_threads;
	neutral_mutex mtx_access;
	LuaCore* luaHudCore;
	LuaBackgroundManager* lua_background_ptr;
	std::map<std::string, LuaBackgroundCodes*> hud_vector;

public:
	TimeChronometer timer;
	void process_input(InputBaseEvent* input);
	LuaThread();
	void clear_hud_vector(){ hud_vector.clear(); }
	std::shared_ptr<ThreadHandler> CreatThreadHandler();
	std::shared_ptr<ThreadHandler> get_free_thread_handled(bool create = true);
	void refresh();
	void simple_execute(std::string &script, std::string name);

	void run_wait_input();
	static LuaThread* get();
};

class LuaHandler{
	uint64_t print_id;
	std::vector<std::pair<uint64_t, std::string>> lua_error_log;
	neutral_mutex mtx_access;
public:
	LuaHandler();
	void add_lua_error_log(std::string error);
	std::vector<std::pair<uint64_t, std::string>> get_index_greater_than(uint64_t index, bool clear = false);
	static LuaHandler* get();
};
#pragma pack(pop)