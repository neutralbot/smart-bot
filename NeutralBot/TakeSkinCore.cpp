#pragma once
#include "Core\Map.h"
#include "DevelopmentManager.h"
#include "Core\constants.h"
#include "LooterManager.h"
#include "TakeSkinCore.h"
#include "Core\Actions.h"
#include <thread>
#include "Core\Pathfinder.h"
#include "Core\LooterCore.h"
#include "TakeSkinManager.h"


TakeSkinCore::TakeSkinCore(){
	std::thread([&](){ 
		MONITOR_THREAD("TakeSkinCore::run")
			run();
	}).detach();
}

bool TakeSkinCore::can_execute(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return false;

	if (!TakeSkinManager::get()->get_take_skin_state())
		return false;

	if (NeutralManager::get()->is_paused() || !NeutralManager::get()->get_bot_state())
		return false;

	if (NeutralManager::get()->get_core_states(CORE_LOOTER) == ENABLED
		&& LooterCore::get()->need_operate())
		return false;

	if (HunterCore::get()->need_operate())
		return false;

	return true;
}

void TakeSkinCore::run(){
	TimeChronometer time;
	while (true){
		Sleep(100);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;
				
		if (!gMapTilesPtr)
			continue;

		if (!can_execute())
			continue;

		execute_take_skin();		
	}
}

bool TakeSkinCore::need_operate(){
	bool retval;
	mtx_access.lock();
	retval = var_need_execute && (uint32_t)last_need_execute.elapsed_milliseconds() < 2000;
	mtx_access.unlock();
	return retval;
}

bool TakeSkinCore::update_need_execute(bool state){
	mtx_access.lock();
	if (state)
		last_need_execute.reset();
	
	var_need_execute = state;
	mtx_access.unlock();
	return state;
}

void TakeSkinCore::execute_take_skin(){
	auto mapTakeSkin = TakeSkinManager::get()->getMapTakeSkin();
	for (auto takeSkin : mapTakeSkin){
		if (takeSkin.second->item_id <= 0 || takeSkin.second->corpse_id <= 0)
			continue;
		
		auto tilesCorpses = gMapTilesPtr->get_tiles_with_top_id(takeSkin.second->corpse_id);
		if (!tilesCorpses.size())
			continue;

		if (ContainerManager::get()->get_item_count(takeSkin.second->item_id) <= 0)
			continue;

		for (auto tileCorpses : tilesCorpses){
			gMapTilesPtr->wait_refresh();

			if (!gMapMinimapPtr->get_walkability(tileCorpses->coordinate,map_type_t::map_front_mini_t))
				continue;

			if (!Pathfinder::get()->is_reachable(tileCorpses->coordinate, map_front_mini_t, false))
				continue;

			if (tileCorpses->contains_monster() || tileCorpses->contains_npc() ||
				tileCorpses->contains_player() || tileCorpses->contains_summon())
				continue;
			
			update_need_execute(true);
			switch ((take_skin_condition_t)takeSkin.second->condition_index){
				case take_skin_condition_t::take_skin_condition_t_after_6_sec:{
					if (can_reset){
						time.reset();
						can_reset = false;
					}

					if (time.elapsed_milliseconds() < 6000)
						continue;

					for (int count = 0; count < 3; count++)
						if (use_item_corporses(tileCorpses->coordinate, takeSkin.second->item_id, takeSkin.second->corpse_id))
							break;	
							
					can_reset = true;
					continue;
				}
				break;
				case take_skin_condition_t::take_skin_condition_t_none:{
					for (int count = 0; count < 3; count++){
						if (use_item_corporses(tileCorpses->coordinate, takeSkin.second->item_id, takeSkin.second->corpse_id))
							break;
					}
					continue;
				}
				break;
			}
		}
	}
	update_need_execute(false);
}

bool TakeSkinCore::use_item_corporses(Coordinate coord, uint32_t item_to_use, uint32_t corporses_id){
	TilePtr tile = gMapTilesPtr->get_tile_at(coord);
	if (!tile)
		return false;

	if (tile->get_top_item_id() != corporses_id)
		return true;

	Coordinate myPosition = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (myPosition.z != coord.z)
		return false;

	if (myPosition.get_axis_min_dist(coord) > 1){
		Coordinate coordTemp = Pathfinder::get()->get_nearest_coordinate_from_another(coord, map_front_mini_t, true, true, 1, false, true);

		Actions::goto_coord(coordTemp, 0);

		TibiaProcess::get_default()->character_info->wait_stop_waking();
	}

	gMapTilesPtr->wait_refresh();

	TibiaProcess::get_default()->wait_ping_delay(2.0);

	if (!Actions::use_crosshair_item_on_coordinate(coord, item_to_use))
		return false;

	tile = gMapTilesPtr->get_tile_at(coord);
	if (!tile)
		return false;	

	if (tile->contains_monster() || tile->contains_npc() ||
		tile->contains_player() || tile->contains_summon())
		return false;
	
	if (tile->get_top_item_id() == corporses_id)
		return false;
		
	return true;
}
