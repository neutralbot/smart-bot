#pragma once
#include "Looter.h"
#include "GeneralSettings.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class Settings : public Telerik::WinControls::UI::RadForm{
	public:	Settings(void){
		InitializeComponent();
		NeutralBot::FastUIPanelController::get()->install_controller(this);
	}
	protected: ~Settings(){
				   unique = nullptr;

				   if (components)
					   delete components;
	}

	protected: static Settings^ unique;

	public:	static void CloseUnique(){
				if (unique)unique->Close();
	}
	public: static void ShowUnique(){
				if (unique == nullptr)
					unique = gcnew Settings();

				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Settings();
	}
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_press_wait;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_type_wait;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_click_wait;
	private: Telerik::WinControls::UI::RadDropDownList^  box_stuck_ctrl_shift;
	private: Telerik::WinControls::UI::RadDropDownList^  box_keyboard_mode;
	private: Telerik::WinControls::UI::RadDropDownList^  box_move_speed;
	private: Telerik::WinControls::UI::RadDropDownList^  box_stuck_cursor;
	private: Telerik::WinControls::UI::RadDropDownList^  box_scroll_mode;
	private: Telerik::WinControls::UI::RadDropDownList^  box_mouse_mode;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
	private: Telerik::WinControls::UI::RadLabel^  radLabel4;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: Telerik::WinControls::UI::RadLabel^  radLabel5;
	private: Telerik::WinControls::UI::RadLabel^  radLabel9;
	private: Telerik::WinControls::UI::RadLabel^  radLabel8;
	private: Telerik::WinControls::UI::RadLabel^  radLabel7;
	private: Telerik::WinControls::UI::RadLabel^  radLabel6;
	protected:
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Settings::typeid));
				 this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->spin_press_wait = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_type_wait = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->box_stuck_ctrl_shift = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->box_keyboard_mode = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel9 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel8 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel7 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel6 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_click_wait = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->box_move_speed = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->box_stuck_cursor = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->box_scroll_mode = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->box_mouse_mode = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				 this->radGroupBox1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_press_wait))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_type_wait))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_stuck_ctrl_shift))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_keyboard_mode))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
				 this->radGroupBox2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_click_wait))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_move_speed))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_stuck_cursor))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_scroll_mode))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_mouse_mode))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // radGroupBox1
				 // 
				 this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox1->Controls->Add(this->spin_press_wait);
				 this->radGroupBox1->Controls->Add(this->radLabel4);
				 this->radGroupBox1->Controls->Add(this->spin_type_wait);
				 this->radGroupBox1->Controls->Add(this->radLabel3);
				 this->radGroupBox1->Controls->Add(this->radLabel2);
				 this->radGroupBox1->Controls->Add(this->box_stuck_ctrl_shift);
				 this->radGroupBox1->HeaderText = L"KeyBoard";
				 this->radGroupBox1->Location = System::Drawing::Point(12, 12);
				 this->radGroupBox1->Name = L"radGroupBox1";
				 this->radGroupBox1->Size = System::Drawing::Size(360, 106);
				 this->radGroupBox1->TabIndex = 0;
				 this->radGroupBox1->Text = L"KeyBoard";
				 // 
				 // spin_press_wait
				 // 
				 this->spin_press_wait->Location = System::Drawing::Point(162, 72);
				 this->spin_press_wait->Name = L"spin_press_wait";
				 this->spin_press_wait->Size = System::Drawing::Size(180, 20);
				 this->spin_press_wait->TabIndex = 3;
				 this->spin_press_wait->TabStop = false;
				 this->spin_press_wait->ValueChanged += gcnew System::EventHandler(this, &Settings::spin_press_wait_ValueChanged);
				 // 
				 // radLabel4
				 // 
				 this->radLabel4->Location = System::Drawing::Point(5, 73);
				 this->radLabel4->Name = L"radLabel4";
				 this->radLabel4->Size = System::Drawing::Size(88, 18);
				 this->radLabel4->TabIndex = 2;
				 this->radLabel4->Text = L"Press Wait Time:";
				 // 
				 // spin_type_wait
				 // 
				 this->spin_type_wait->Location = System::Drawing::Point(162, 46);
				 this->spin_type_wait->Name = L"spin_type_wait";
				 this->spin_type_wait->Size = System::Drawing::Size(180, 20);
				 this->spin_type_wait->TabIndex = 4;
				 this->spin_type_wait->TabStop = false;
				 this->spin_type_wait->ValueChanged += gcnew System::EventHandler(this, &Settings::spin_type_wait_ValueChanged);
				 // 
				 // radLabel3
				 // 
				 this->radLabel3->Location = System::Drawing::Point(5, 47);
				 this->radLabel3->Name = L"radLabel3";
				 this->radLabel3->Size = System::Drawing::Size(89, 18);
				 this->radLabel3->TabIndex = 6;
				 this->radLabel3->Text = L"Type Wait Time: ";
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(5, 21);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(116, 18);
				 this->radLabel2->TabIndex = 5;
				 this->radLabel2->Text = L"Stuck Ctrl/Shift Policy:";
				 // 
				 // box_stuck_ctrl_shift
				 // 
				 this->box_stuck_ctrl_shift->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->box_stuck_ctrl_shift->Location = System::Drawing::Point(162, 21);
				 this->box_stuck_ctrl_shift->Name = L"box_stuck_ctrl_shift";
				 this->box_stuck_ctrl_shift->Size = System::Drawing::Size(180, 20);
				 this->box_stuck_ctrl_shift->TabIndex = 2;
				 this->box_stuck_ctrl_shift->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Settings::box_stuck_ctrl_shift_SelectedIndexChanged);
				 // 
				 // box_keyboard_mode
				 // 
				 this->box_keyboard_mode->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->box_keyboard_mode->Location = System::Drawing::Point(833, 124);
				 this->box_keyboard_mode->Name = L"box_keyboard_mode";
				 this->box_keyboard_mode->Size = System::Drawing::Size(180, 20);
				 this->box_keyboard_mode->TabIndex = 1;
				 this->box_keyboard_mode->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Settings::box_keyboard_mode_SelectedIndexChanged);
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(676, 124);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(89, 18);
				 this->radLabel1->TabIndex = 0;
				 this->radLabel1->Text = L"KeyBoard Mode:";
				 // 
				 // radGroupBox2
				 // 
				 this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox2->Controls->Add(this->radLabel9);
				 this->radGroupBox2->Controls->Add(this->radLabel8);
				 this->radGroupBox2->Controls->Add(this->radLabel7);
				 this->radGroupBox2->Controls->Add(this->radLabel6);
				 this->radGroupBox2->Controls->Add(this->spin_click_wait);
				 this->radGroupBox2->Controls->Add(this->box_move_speed);
				 this->radGroupBox2->Controls->Add(this->box_stuck_cursor);
				 this->radGroupBox2->Controls->Add(this->box_scroll_mode);
				 this->radGroupBox2->HeaderText = L"Mouse";
				 this->radGroupBox2->Location = System::Drawing::Point(12, 124);
				 this->radGroupBox2->Name = L"radGroupBox2";
				 this->radGroupBox2->Size = System::Drawing::Size(360, 129);
				 this->radGroupBox2->TabIndex = 1;
				 this->radGroupBox2->Text = L"Mouse";
				 // 
				 // radLabel9
				 // 
				 this->radLabel9->Location = System::Drawing::Point(5, 100);
				 this->radLabel9->Name = L"radLabel9";
				 this->radLabel9->Size = System::Drawing::Size(85, 18);
				 this->radLabel9->TabIndex = 8;
				 this->radLabel9->Text = L"Click Wait Time:";
				 // 
				 // radLabel8
				 // 
				 this->radLabel8->Location = System::Drawing::Point(5, 73);
				 this->radLabel8->Name = L"radLabel8";
				 this->radLabel8->Size = System::Drawing::Size(74, 18);
				 this->radLabel8->TabIndex = 7;
				 this->radLabel8->Text = L"Move Speed: ";
				 // 
				 // radLabel7
				 // 
				 this->radLabel7->Location = System::Drawing::Point(5, 47);
				 this->radLabel7->Name = L"radLabel7";
				 this->radLabel7->Size = System::Drawing::Size(104, 18);
				 this->radLabel7->TabIndex = 6;
				 this->radLabel7->Text = L"Sutck Cursor Policy:";
				 // 
				 // radLabel6
				 // 
				 this->radLabel6->Location = System::Drawing::Point(5, 21);
				 this->radLabel6->Name = L"radLabel6";
				 this->radLabel6->Size = System::Drawing::Size(68, 18);
				 this->radLabel6->TabIndex = 5;
				 this->radLabel6->Text = L"Scroll Mode:";
				 // 
				 // spin_click_wait
				 // 
				 this->spin_click_wait->Location = System::Drawing::Point(162, 99);
				 this->spin_click_wait->Name = L"spin_click_wait";
				 this->spin_click_wait->Size = System::Drawing::Size(180, 20);
				 this->spin_click_wait->TabIndex = 2;
				 this->spin_click_wait->TabStop = false;
				 this->spin_click_wait->ValueChanged += gcnew System::EventHandler(this, &Settings::spin_click_wait_ValueChanged);
				 // 
				 // box_move_speed
				 // 
				 this->box_move_speed->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->box_move_speed->Location = System::Drawing::Point(162, 73);
				 this->box_move_speed->Name = L"box_move_speed";
				 this->box_move_speed->Size = System::Drawing::Size(180, 20);
				 this->box_move_speed->TabIndex = 4;
				 this->box_move_speed->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Settings::box_mode_speed_SelectedIndexChanged);
				 // 
				 // box_stuck_cursor
				 // 
				 this->box_stuck_cursor->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->box_stuck_cursor->Location = System::Drawing::Point(162, 47);
				 this->box_stuck_cursor->Name = L"box_stuck_cursor";
				 this->box_stuck_cursor->Size = System::Drawing::Size(180, 20);
				 this->box_stuck_cursor->TabIndex = 3;
				 this->box_stuck_cursor->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Settings::box_stuck_cursor_SelectedIndexChanged);
				 // 
				 // box_scroll_mode
				 // 
				 this->box_scroll_mode->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->box_scroll_mode->Location = System::Drawing::Point(162, 21);
				 this->box_scroll_mode->Name = L"box_scroll_mode";
				 this->box_scroll_mode->Size = System::Drawing::Size(180, 20);
				 this->box_scroll_mode->TabIndex = 2;
				 this->box_scroll_mode->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Settings::box_scroll_mode_SelectedIndexChanged);
				 // 
				 // box_mouse_mode
				 // 
				 this->box_mouse_mode->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->box_mouse_mode->Location = System::Drawing::Point(833, 149);
				 this->box_mouse_mode->Name = L"box_mouse_mode";
				 this->box_mouse_mode->Size = System::Drawing::Size(180, 20);
				 this->box_mouse_mode->TabIndex = 1;
				 this->box_mouse_mode->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Settings::box_mouse_mode_SelectedIndexChanged);
				 // 
				 // radLabel5
				 // 
				 this->radLabel5->Location = System::Drawing::Point(676, 149);
				 this->radLabel5->Name = L"radLabel5";
				 this->radLabel5->Size = System::Drawing::Size(75, 18);
				 this->radLabel5->TabIndex = 0;
				 this->radLabel5->Text = L"Mouse Mode:";
				 // 
				 // Settings
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(385, 261);
				 this->Controls->Add(this->radGroupBox2);
				 this->Controls->Add(this->radGroupBox1);
				 this->Controls->Add(this->radLabel1);
				 this->Controls->Add(this->box_keyboard_mode);
				 this->Controls->Add(this->radLabel5);
				 this->Controls->Add(this->box_mouse_mode);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->Name = L"Settings";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->Text = L"Settings";
				 this->Load += gcnew System::EventHandler(this, &Settings::Settings_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				 this->radGroupBox1->ResumeLayout(false);
				 this->radGroupBox1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_press_wait))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_type_wait))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_stuck_ctrl_shift))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_keyboard_mode))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
				 this->radGroupBox2->ResumeLayout(false);
				 this->radGroupBox2->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_click_wait))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_move_speed))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_stuck_cursor))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_scroll_mode))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_mouse_mode))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion
			 bool disable_update = false;
	private: System::Void box_keyboard_mode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;
	}
	private: System::Void box_stuck_ctrl_shift_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralSettings::get()->set_stuck_ctrl_shift_index((stuck_ctrl_shift_policy_t)box_stuck_ctrl_shift->SelectedIndex);
	}
	private: System::Void box_mouse_mode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;
	}
	private: System::Void box_scroll_mode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralSettings::get()->set_scroll_mode_index((scroll_mode_t)box_scroll_mode->SelectedIndex);
	}
	private: System::Void box_stuck_cursor_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralSettings::get()->set_stuck_cursor_index((stuck_cursor_policy_t)box_stuck_cursor->SelectedIndex);
	}
	private: System::Void box_mode_speed_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralSettings::get()->set_move_speed_index((move_speed_t)box_move_speed->SelectedIndex);
	}
	private: System::Void spin_click_wait_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralSettings::get()->set_click_wait_time((int)spin_click_wait->Value);
	}
	private: System::Void spin_type_wait_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralSettings::get()->set_type_wait_time((int)spin_type_wait->Value);
	}
	private: System::Void spin_press_wait_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralSettings::get()->set_press_wait_time((int)spin_press_wait->Value);
	}

	private: System::Void Settings_Load(System::Object^  sender, System::EventArgs^  e) {
				 disable_update = true;

				 for (int i = 1; i < stuck_ctrl_shift_policy_t::stuck_ctrl_shift_policy_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(std::string("stuck_ctrl_shift_policy_t_") + std::to_string(i));
					 item->Value = i;
					 box_stuck_ctrl_shift->Items->Add(item);
				 }
				 for (int i = 1; i < stuck_cursor_policy_t::stuck_cursor_policy_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(std::string("stuck_cursor_policy_t_") + std::to_string(i));
					 item->Value = i;
					 box_stuck_cursor->Items->Add(item);
				 }
				 for (int i = 1; i < scroll_mode_t::scroll_mode_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(std::string("scroll_mode_t_") + std::to_string(i));
					 item->Value = i;
					 box_scroll_mode->Items->Add(item);
				 }
				 for (int i = 1; i < move_speed_t::move_speed_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(std::string("move_speed_t_") + std::to_string(i));
					 item->Value = i;
					 box_move_speed->Items->Add(item);
				 }
				 loadAll();
				 disable_update = false;
	}

			 void loadAll(){
				 spin_press_wait->Value = GeneralSettings::get()->get_press_wait_time();
				 spin_type_wait->Value = GeneralSettings::get()->get_type_wait_time();
				 spin_click_wait->Value = GeneralSettings::get()->get_click_wait_time();
				 box_stuck_ctrl_shift->SelectedIndex = (int)GeneralSettings::get()->get_stuck_ctrl_shift_index() - 1;
				 box_move_speed->SelectedIndex = (int)GeneralSettings::get()->get_move_speed_index() - 1;
				 box_stuck_cursor->SelectedIndex = (int)GeneralSettings::get()->get_stuck_cursor_index() - 1;
				 box_scroll_mode->SelectedIndex = (int)GeneralSettings::get()->get_scroll_mode_index() - 1;
				 //box_keyboard_mode->SelectedIndex = ;
				 //box_mouse_mode->SelectedIndex = ;
			 }
	};
}
