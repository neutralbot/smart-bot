#pragma once
#include "ManagedUtil.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for LuaStyleEditor
	/// </summary>
	public ref class LuaStyleEditor : public Telerik::WinControls::UI::RadForm
	{
	public:
		LuaStyleEditor(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~LuaStyleEditor()
		{
			if (components)
			{
				delete components;
			}
		}

	
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = gcnew System::ComponentModel::Container();
			this->Size = System::Drawing::Size(300,300);
			this->Text = L"LuaStyleEditor";
			this->Padding = System::Windows::Forms::Padding(0);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		}
#pragma endregion
	};
}
