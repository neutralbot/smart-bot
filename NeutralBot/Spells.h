 #pragma once
#include "SpellManager.h" 
#include "MonsterAttackInfo.h"
#include "Core\Util.h"
#include <msclr\marshal_cppstd.h>
#include "LanguageManager.h"
#include "EnumToTranslate.h"
#include "ManagedUtil.h"
#include "GeneralManager.h"
#include "AutoGenerateLoot.h"
#include "SpellsAutoConfig.h"
#include "SpellEnum.h"
#include "GifManager.h"
#include "ControlManager2.h"
#include <direct.h>

namespace Neutral {
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;
	using namespace System::Collections;
	using namespace System::ComponentModel;
	using namespace System::IO;

	public ref class Spells : public Telerik::WinControls::UI::RadForm{

	public:
		Spells(void){
			InitializeComponent();

			disable_notify = false;
			managed_util::setToggleCheckButtonStyle(radToggleButtonSpellcaster);
			this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}

	protected:
		~Spells(){
			managed_util::unsetToggleCheckButtonStyle(radToggleButtonSpellcaster);
			unique = nullptr;

			if (components)
				delete components;
		}


	public: static void HideUnique(){
				if (unique)unique->Hide();
	}

	protected:
		Telerik::WinControls::UI::RadDropDownList^  spellDropDownUseType;
		Telerik::WinControls::UI::RadMenu^  radMenu1;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
		Telerik::WinControls::UI::RadMenuItem^  bt_save_;
		Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
		Telerik::WinControls::UI::RadMenuItem^  bt_load_;
		Telerik::WinControls::UI::RadPanel^  radPanel2;
		Telerik::WinControls::UI::RadPanel^  radPanel3;
		Telerik::WinControls::UI::RadPanel^  radPanel5;
		Telerik::WinControls::UI::RadPanel^  radPanel4;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonSpellcaster;
		Telerik::WinControls::UI::RadPageView^  radPageView1;
		Telerik::WinControls::UI::RadPageViewPage^  radPageViewPage1;
		Telerik::WinControls::UI::RadPageViewPage^  radPageViewPage2;
		Telerik::WinControls::UI::RadListView^  spellSpellListAttack;
	private: Telerik::WinControls::UI::RadPageView^  radPageView2;	
	protected:
	protected:
	private: Telerik::WinControls::UI::RadPageViewPage^  page_property;
	private: Telerik::WinControls::UI::RadPageViewPage^  page_settings;
	private: Telerik::WinControls::UI::RadListView^  ListMonster;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	private: System::Windows::Forms::PictureBox^  gifPictureBox;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: Telerik::WinControls::UI::RadSpinEditor^  spinmin_hp;
	private: Telerik::WinControls::UI::RadSpinEditor^  spinmax_hp;
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadButton^  btDeleteMonster;
	private: Telerik::WinControls::UI::RadButton^  btNewMonster;
	private: Telerik::WinControls::UI::RadDropDownList^  boxMonsterName;
	private: Telerik::WinControls::UI::RadPageViewPage^  radPageViewPage3;
	private: Telerik::WinControls::UI::RadTreeView^  radTreeView1;
	private: Telerik::WinControls::UI::RadContextMenu^  radContextMenu1;
	private: Telerik::WinControls::UI::RadContextMenuManager^  radContextMenuManager1;
	private: Telerik::WinControls::UI::RadButton^  radButton1;
	private: Telerik::WinControls::UI::RadMenuItem^  menu_config;
	private: Telerik::WinControls::UI::RadCheckBox^  check_any_monster;
	private: Telerik::WinControls::UI::RadPageViewPage^  PageViewAdvanced;
	private: Telerik::WinControls::UI::RadListView^  ListViewCondition;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox5;
	private: Telerik::WinControls::UI::RadLabel^  radLabel12;
	private: Telerik::WinControls::UI::RadLabel^  radLabel13;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListCondition;
	private: Telerik::WinControls::UI::RadButton^  ButtonRemoveCondition;
	private: Telerik::WinControls::UI::RadButton^  ButtonNewCondition;
	private: Telerik::WinControls::UI::RadTextBox^  TextBoxValueCondition;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox3;
	private: Telerik::WinControls::UI::RadSpinEditor^  SpinEditorValuePoint;
	private: Telerik::WinControls::UI::RadLabel^  radLabel4;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox4;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListAction;
	protected:
	protected: static Spells^ unique;

	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew Spells();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Spells();
	}
	public: static void ReloadForm(){
				unique->Spells_Load(nullptr, nullptr);
	}
			static void CloseUnique(){
				if (unique)unique->Close();
			}

			MonsterAttackInfo^ monsterInfo;
			Telerik::WinControls::UI::RadButton^  spellAddButton;
			Telerik::WinControls::UI::RadTextBox^  spellTextBoxId;
			Telerik::WinControls::UI::RadPropertyGrid^  spellSpellProperty;
			Telerik::WinControls::UI::RadListView^  spellSpellListHeal;
			Telerik::WinControls::UI::RadDropDownList^  spellDropDownspell_type;
			Telerik::WinControls::UI::RadDropDownList^  radDropDownList1;
	private: System::ComponentModel::IContainer^  components;
	public:
		bool disable_notify;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Id",
				L"Id"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"spell_type",
				L"spell_type"));
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem5 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem6 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem7 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem8 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0",
				L"Creatures"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn4 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Condition",
				L"Condition"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn5 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Action",
				L"Action"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn6 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Value",
				L"Value"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Id",
				L"Id"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn8 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"spell_type",
				L"spell_type"));
			this->spellAddButton = (gcnew Telerik::WinControls::UI::RadButton());
			this->spellTextBoxId = (gcnew Telerik::WinControls::UI::RadTextBox());
			this->spellSpellProperty = (gcnew Telerik::WinControls::UI::RadPropertyGrid());
			this->spellSpellListHeal = (gcnew Telerik::WinControls::UI::RadListView());
			this->radDropDownList1 = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->spellDropDownspell_type = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->spellDropDownUseType = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
			this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_save_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
			this->bt_load_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->menu_config = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPageView2 = (gcnew Telerik::WinControls::UI::RadPageView());
			this->page_property = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->page_settings = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->check_any_monster = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->radButton1 = (gcnew Telerik::WinControls::UI::RadButton());
			this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->boxMonsterName = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->spinmin_hp = (gcnew Telerik::WinControls::UI::RadSpinEditor());
			this->spinmax_hp = (gcnew Telerik::WinControls::UI::RadSpinEditor());
			this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->btDeleteMonster = (gcnew Telerik::WinControls::UI::RadButton());
			this->btNewMonster = (gcnew Telerik::WinControls::UI::RadButton());
			this->gifPictureBox = (gcnew System::Windows::Forms::PictureBox());
			this->ListMonster = (gcnew Telerik::WinControls::UI::RadListView());
			this->PageViewAdvanced = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->radGroupBox4 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->DropDownListAction = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radGroupBox3 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->SpinEditorValuePoint = (gcnew Telerik::WinControls::UI::RadSpinEditor());
			this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->ButtonRemoveCondition = (gcnew Telerik::WinControls::UI::RadButton());
			this->ButtonNewCondition = (gcnew Telerik::WinControls::UI::RadButton());
			this->radGroupBox5 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->TextBoxValueCondition = (gcnew Telerik::WinControls::UI::RadTextBox());
			this->radLabel12 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel13 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->DropDownListCondition = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->ListViewCondition = (gcnew Telerik::WinControls::UI::RadListView());
			this->radPageView1 = (gcnew Telerik::WinControls::UI::RadPageView());
			this->radPageViewPage1 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->radPageViewPage2 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->spellSpellListAttack = (gcnew Telerik::WinControls::UI::RadListView());
			this->radPageViewPage3 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->radTreeView1 = (gcnew Telerik::WinControls::UI::RadTreeView());
			this->radContextMenu1 = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
			this->radPanel3 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel5 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radToggleButtonSpellcaster = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radContextMenuManager1 = (gcnew Telerik::WinControls::UI::RadContextMenuManager());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellAddButton))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellTextBoxId))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellSpellProperty))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellSpellListHeal))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownList1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellDropDownspell_type))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellDropDownUseType))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView2))->BeginInit();
			this->radPageView2->SuspendLayout();
			this->page_property->SuspendLayout();
			this->page_settings->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_any_monster))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
			this->radGroupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxMonsterName))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spinmin_hp))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spinmax_hp))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
			this->radGroupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btDeleteMonster))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btNewMonster))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->gifPictureBox))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListMonster))->BeginInit();
			this->PageViewAdvanced->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->BeginInit();
			this->radGroupBox4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListAction))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->BeginInit();
			this->radGroupBox3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpinEditorValuePoint))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemoveCondition))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonNewCondition))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox5))->BeginInit();
			this->radGroupBox5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->TextBoxValueCondition))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListCondition))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewCondition))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->BeginInit();
			this->radPageView1->SuspendLayout();
			this->radPageViewPage1->SuspendLayout();
			this->radPageViewPage2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellSpellListAttack))->BeginInit();
			this->radPageViewPage3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTreeView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->BeginInit();
			this->radPanel3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->BeginInit();
			this->radPanel5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
			this->radPanel4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellcaster))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// spellAddButton
			// 
			this->spellAddButton->Location = System::Drawing::Point(6, 392);
			this->spellAddButton->Name = L"spellAddButton";
			this->spellAddButton->Size = System::Drawing::Size(229, 21);
			this->spellAddButton->TabIndex = 11;
			this->spellAddButton->Text = L"Add";
			this->spellAddButton->ThemeName = L"Office2013Dark";
			this->spellAddButton->Click += gcnew System::EventHandler(this, &Spells::spellAddButton_Click);
			// 
			// spellTextBoxId
			// 
			this->spellTextBoxId->Location = System::Drawing::Point(6, 338);
			this->spellTextBoxId->Name = L"spellTextBoxId";
			this->spellTextBoxId->NullText = L"Spell Name";
			this->spellTextBoxId->Size = System::Drawing::Size(229, 20);
			this->spellTextBoxId->TabIndex = 21;
			this->spellTextBoxId->ThemeName = L"Office2013Dark";
			this->spellTextBoxId->TextChanged += gcnew System::EventHandler(this, &Spells::spellTextBoxId_TextChanged);
			// 
			// spellSpellProperty
			// 
			this->spellSpellProperty->Dock = System::Windows::Forms::DockStyle::Fill;
			this->spellSpellProperty->Location = System::Drawing::Point(0, 0);
			this->spellSpellProperty->Name = L"spellSpellProperty";
			// 
			// 
			// 
			this->spellSpellProperty->RootElement->Opacity = 1;
			this->spellSpellProperty->Size = System::Drawing::Size(444, 344);
			this->spellSpellProperty->SortOrder = System::Windows::Forms::SortOrder::Ascending;
			this->spellSpellProperty->TabIndex = 19;
			this->spellSpellProperty->Text = L"radPropertyGrid1";
			this->spellSpellProperty->ThemeName = L"Office2013Dark";
			this->spellSpellProperty->ToolbarVisible = true;
			this->spellSpellProperty->Edited += gcnew Telerik::WinControls::UI::PropertyGridItemEditedEventHandler(this, &Spells::spellSpellProperty_Edited);
			(cli::safe_cast<Telerik::WinControls::UI::PropertyGridElement^>(this->spellSpellProperty->GetChildAt(0)))->Padding = System::Windows::Forms::Padding(0);
			(cli::safe_cast<Telerik::WinControls::UI::PropertyGridSplitElement^>(this->spellSpellProperty->GetChildAt(0)->GetChildAt(1)))->Padding = System::Windows::Forms::Padding(0);
			(cli::safe_cast<Telerik::WinControls::UI::PropertyGridSplitElement^>(this->spellSpellProperty->GetChildAt(0)->GetChildAt(1)))->Margin = System::Windows::Forms::Padding(0);
			(cli::safe_cast<Telerik::WinControls::UI::PropertyGridTableElement^>(this->spellSpellProperty->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->Margin = System::Windows::Forms::Padding(0,
				-1, 0, 0);
			(cli::safe_cast<Telerik::WinControls::UI::VirtualizedStackContainer<Telerik::WinControls::UI::PropertyGridItemBase^ >^>(this->spellSpellProperty->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)->GetChildAt(0)))->Margin = System::Windows::Forms::Padding(0);
			// 
			// spellSpellListHeal
			// 
			listViewDetailColumn1->HeaderText = L"Id";
			listViewDetailColumn2->HeaderText = L"spell_type";
			this->spellSpellListHeal->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(2) {
				listViewDetailColumn1,
					listViewDetailColumn2
			});
			this->spellSpellListHeal->Dock = System::Windows::Forms::DockStyle::Fill;
			this->spellSpellListHeal->Location = System::Drawing::Point(0, 0);
			this->spellSpellListHeal->Name = L"spellSpellListHeal";
			this->spellSpellListHeal->ShowCheckBoxes = true;
			this->spellSpellListHeal->Size = System::Drawing::Size(208, 281);
			this->spellSpellListHeal->TabIndex = 18;
			this->spellSpellListHeal->Text = L"radListView1";
			this->spellSpellListHeal->ThemeName = L"Office2013Dark";
			this->spellSpellListHeal->SelectedItemChanged += gcnew System::EventHandler(this, &Spells::spellSpellList_SelectedItemChanged);
			this->spellSpellListHeal->ItemCheckedChanged += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &Spells::spellSpellListAttack_ItemCheckedChanged);
			this->spellSpellListHeal->ItemValueChanged += gcnew Telerik::WinControls::UI::ListViewItemValueChangedEventHandler(this, &Spells::spellsListViewItemValueChanged);
			this->spellSpellListHeal->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Spells::spellSpellList_ItemRemoving);
			// 
			// radDropDownList1
			// 
			this->radDropDownList1->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
			this->radDropDownList1->Location = System::Drawing::Point(239, 6);
			this->radDropDownList1->Name = L"radDropDownList1";
			this->radDropDownList1->NullText = L"item or magic words";
			this->radDropDownList1->Size = System::Drawing::Size(458, 20);
			this->radDropDownList1->TabIndex = 0;
			this->radDropDownList1->ThemeName = L"Office2013Dark";
			this->radDropDownList1->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Spells::radDropDownList1_SelectedIndexChanged);
			this->radDropDownList1->TextChanged += gcnew System::EventHandler(this, &Spells::radDropDownList1_TextChanged);
			// 
			// spellDropDownspell_type
			// 
			this->spellDropDownspell_type->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			radListDataItem1->Text = L"Healing Spell";
			radListDataItem2->Text = L"Attack Spell Target";
			radListDataItem3->Text = L"Attack Spell Area";
			radListDataItem4->Text = L"Rune Spell Target";
			radListDataItem5->Text = L"Rune Spell Area";
			this->spellDropDownspell_type->Items->Add(radListDataItem1);
			this->spellDropDownspell_type->Items->Add(radListDataItem2);
			this->spellDropDownspell_type->Items->Add(radListDataItem3);
			this->spellDropDownspell_type->Items->Add(radListDataItem4);
			this->spellDropDownspell_type->Items->Add(radListDataItem5);
			this->spellDropDownspell_type->Location = System::Drawing::Point(6, 365);
			this->spellDropDownspell_type->Name = L"spellDropDownspell_type";
			this->spellDropDownspell_type->NullText = L"Spell Type";
			this->spellDropDownspell_type->Size = System::Drawing::Size(229, 20);
			this->spellDropDownspell_type->TabIndex = 24;
			this->spellDropDownspell_type->ThemeName = L"Office2013Dark";
			this->spellDropDownspell_type->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Spells::spellDropDownUseType_SelectedIndexChanged);
			// 
			// spellDropDownUseType
			// 
			radListDataItem6->Text = L"Spell Fast";
			radListDataItem7->Text = L"Spell By Monster Count";
			radListDataItem8->Text = L"Spell By Safety";
			this->spellDropDownUseType->Items->Add(radListDataItem6);
			this->spellDropDownUseType->Items->Add(radListDataItem7);
			this->spellDropDownUseType->Items->Add(radListDataItem8);
			this->spellDropDownUseType->Location = System::Drawing::Point(239, 392);
			this->spellDropDownUseType->Name = L"spellDropDownUseType";
			this->spellDropDownUseType->NullText = L"Use Spell Mode";
			this->spellDropDownUseType->Size = System::Drawing::Size(458, 20);
			this->spellDropDownUseType->TabIndex = 25;
			this->spellDropDownUseType->ThemeName = L"Office2013Dark";
			this->spellDropDownUseType->Visible = false;
			this->spellDropDownUseType->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Spells::spellDropDownUseType_SelectedIndexChanged);
			// 
			// radMenu1
			// 
			this->radMenu1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->radMenuItem1, this->menu_config });
			this->radMenu1->Location = System::Drawing::Point(0, 0);
			this->radMenu1->Name = L"radMenu1";
			this->radMenu1->Size = System::Drawing::Size(603, 20);
			this->radMenu1->TabIndex = 0;
			this->radMenu1->Text = L"radMenu1";
			// 
			// radMenuItem1
			// 
			this->radMenuItem1->AccessibleDescription = L"Menu";
			this->radMenuItem1->AccessibleName = L"Menu";
			this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
				this->bt_save_, this->radMenuSeparatorItem1,
					this->bt_load_
			});
			this->radMenuItem1->Name = L"radMenuItem1";
			this->radMenuItem1->Text = L"Menu";
			// 
			// bt_save_
			// 
			this->bt_save_->AccessibleDescription = L"Save";
			this->bt_save_->AccessibleName = L"Save";
			this->bt_save_->Name = L"bt_save_";
			this->bt_save_->Text = L"Save";
			this->bt_save_->Click += gcnew System::EventHandler(this, &Spells::bt_save__Click);
			// 
			// radMenuSeparatorItem1
			// 
			this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
			// 
			// bt_load_
			// 
			this->bt_load_->AccessibleDescription = L"Load";
			this->bt_load_->AccessibleName = L"Load";
			this->bt_load_->Name = L"bt_load_";
			this->bt_load_->Text = L"Load";
			this->bt_load_->Click += gcnew System::EventHandler(this, &Spells::bt_load__Click);
			// 
			// menu_config
			// 
			this->menu_config->AccessibleDescription = L"AutoConfig";
			this->menu_config->AccessibleName = L"AutoConfig";
			this->menu_config->Name = L"menu_config";
			this->menu_config->Text = L"AutoConfig";
			this->menu_config->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			this->menu_config->Click += gcnew System::EventHandler(this, &Spells::menu_config_Click);
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radPageView2);
			this->radPanel2->Controls->Add(this->spellDropDownspell_type);
			this->radPanel2->Controls->Add(this->spellDropDownUseType);
			this->radPanel2->Controls->Add(this->spellAddButton);
			this->radPanel2->Controls->Add(this->radDropDownList1);
			this->radPanel2->Controls->Add(this->spellTextBoxId);
			this->radPanel2->Controls->Add(this->radPageView1);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel2->Location = System::Drawing::Point(0, 22);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(703, 419);
			this->radPanel2->TabIndex = 29;
			// 
			// radPageView2
			// 
			this->radPageView2->Controls->Add(this->page_property);
			this->radPageView2->Controls->Add(this->page_settings);
			this->radPageView2->Controls->Add(this->PageViewAdvanced);
			this->radPageView2->Location = System::Drawing::Point(236, 27);
			this->radPageView2->Name = L"radPageView2";
			this->radPageView2->SelectedPage = this->page_property;
			this->radPageView2->Size = System::Drawing::Size(465, 392);
			this->radPageView2->TabIndex = 29;
			this->radPageView2->Text = L"Advanced";
			this->radPageView2->SelectedPageChanged += gcnew System::EventHandler(this, &Spells::radPageView2_SelectedPageChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView2->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView2->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
			// 
			// page_property
			// 
			this->page_property->Controls->Add(this->spellSpellProperty);
			this->page_property->ItemSize = System::Drawing::SizeF(59, 28);
			this->page_property->Location = System::Drawing::Point(10, 37);
			this->page_property->Name = L"page_property";
			this->page_property->Size = System::Drawing::Size(444, 344);
			this->page_property->Text = L"Property";
			// 
			// page_settings
			// 
			this->page_settings->Controls->Add(this->check_any_monster);
			this->page_settings->Controls->Add(this->radButton1);
			this->page_settings->Controls->Add(this->radGroupBox2);
			this->page_settings->Controls->Add(this->radGroupBox1);
			this->page_settings->Controls->Add(this->ListMonster);
			this->page_settings->ItemSize = System::Drawing::SizeF(64, 28);
			this->page_settings->Location = System::Drawing::Point(10, 37);
			this->page_settings->Name = L"page_settings";
			this->page_settings->Size = System::Drawing::Size(444, 344);
			this->page_settings->Text = L"Creatures";
			// 
			// check_any_monster
			// 
			this->check_any_monster->Location = System::Drawing::Point(245, 184);
			this->check_any_monster->Name = L"check_any_monster";
			this->check_any_monster->Size = System::Drawing::Size(84, 18);
			this->check_any_monster->TabIndex = 4;
			this->check_any_monster->Text = L"Any monster";
			this->check_any_monster->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Spells::check_any_monster_ToggleStateChanged);
			// 
			// radButton1
			// 
			this->radButton1->Location = System::Drawing::Point(245, 154);
			this->radButton1->Name = L"radButton1";
			this->radButton1->Size = System::Drawing::Size(196, 24);
			this->radButton1->TabIndex = 3;
			this->radButton1->Text = L"AutoGenerate";
			this->radButton1->Click += gcnew System::EventHandler(this, &Spells::radButton1_Click);
			// 
			// radGroupBox2
			// 
			this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox2->Controls->Add(this->boxMonsterName);
			this->radGroupBox2->Controls->Add(this->radLabel2);
			this->radGroupBox2->Controls->Add(this->radLabel1);
			this->radGroupBox2->Controls->Add(this->spinmin_hp);
			this->radGroupBox2->Controls->Add(this->spinmax_hp);
			this->radGroupBox2->HeaderText = L"";
			this->radGroupBox2->Location = System::Drawing::Point(245, 80);
			this->radGroupBox2->Name = L"radGroupBox2";
			this->radGroupBox2->Size = System::Drawing::Size(196, 68);
			this->radGroupBox2->TabIndex = 2;
			// 
			// boxMonsterName
			// 
			this->boxMonsterName->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
			this->boxMonsterName->Location = System::Drawing::Point(8, 8);
			this->boxMonsterName->Name = L"boxMonsterName";
			this->boxMonsterName->Size = System::Drawing::Size(183, 20);
			this->boxMonsterName->TabIndex = 6;
			this->boxMonsterName->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Spells::boxMonsterName_SelectedIndexChanged);
			this->boxMonsterName->TextChanged += gcnew System::EventHandler(this, &Spells::boxMonsterName_TextChanged);
			// 
			// radLabel2
			// 
			this->radLabel2->Location = System::Drawing::Point(5, 38);
			this->radLabel2->Name = L"radLabel2";
			this->radLabel2->Size = System::Drawing::Size(63, 18);
			this->radLabel2->TabIndex = 5;
			this->radLabel2->Text = L"Min/Max %";
			// 
			// radLabel1
			// 
			this->radLabel1->Location = System::Drawing::Point(126, 38);
			this->radLabel1->Name = L"radLabel1";
			this->radLabel1->Size = System::Drawing::Size(14, 18);
			this->radLabel1->TabIndex = 4;
			this->radLabel1->Text = L"~";
			// 
			// spinmin_hp
			// 
			this->spinmin_hp->Location = System::Drawing::Point(75, 37);
			this->spinmin_hp->Name = L"spinmin_hp";
			this->spinmin_hp->Size = System::Drawing::Size(45, 20);
			this->spinmin_hp->TabIndex = 3;
			this->spinmin_hp->TabStop = false;
			this->spinmin_hp->ValueChanged += gcnew System::EventHandler(this, &Spells::spinmin_hp_ValueChanged);
			// 
			// spinmax_hp
			// 
			this->spinmax_hp->Location = System::Drawing::Point(146, 37);
			this->spinmax_hp->Name = L"spinmax_hp";
			this->spinmax_hp->Size = System::Drawing::Size(45, 20);
			this->spinmax_hp->TabIndex = 2;
			this->spinmax_hp->TabStop = false;
			this->spinmax_hp->ValueChanged += gcnew System::EventHandler(this, &Spells::spinmax_hp_ValueChanged);
			// 
			// radGroupBox1
			// 
			this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox1->Controls->Add(this->btDeleteMonster);
			this->radGroupBox1->Controls->Add(this->btNewMonster);
			this->radGroupBox1->Controls->Add(this->gifPictureBox);
			this->radGroupBox1->HeaderText = L"";
			this->radGroupBox1->Location = System::Drawing::Point(245, 3);
			this->radGroupBox1->Name = L"radGroupBox1";
			this->radGroupBox1->Size = System::Drawing::Size(196, 71);
			this->radGroupBox1->TabIndex = 1;
			// 
			// btDeleteMonster
			// 
			this->btDeleteMonster->Location = System::Drawing::Point(65, 38);
			this->btDeleteMonster->Name = L"btDeleteMonster";
			this->btDeleteMonster->Size = System::Drawing::Size(126, 24);
			this->btDeleteMonster->TabIndex = 2;
			this->btDeleteMonster->Text = L"Remove";
			this->btDeleteMonster->Click += gcnew System::EventHandler(this, &Spells::btDeleteMonster_Click);
			// 
			// btNewMonster
			// 
			this->btNewMonster->Location = System::Drawing::Point(65, 8);
			this->btNewMonster->Name = L"btNewMonster";
			this->btNewMonster->Size = System::Drawing::Size(126, 24);
			this->btNewMonster->TabIndex = 1;
			this->btNewMonster->Text = L"New";
			this->btNewMonster->Click += gcnew System::EventHandler(this, &Spells::btNewMonster_Click);
			// 
			// gifPictureBox
			// 
			this->gifPictureBox->Location = System::Drawing::Point(5, 8);
			this->gifPictureBox->Name = L"gifPictureBox";
			this->gifPictureBox->Size = System::Drawing::Size(54, 54);
			this->gifPictureBox->TabIndex = 0;
			this->gifPictureBox->TabStop = false;
			// 
			// ListMonster
			// 
			this->ListMonster->AllowEdit = false;
			listViewDetailColumn3->HeaderText = L"Creatures";
			listViewDetailColumn3->MaxWidth = 237;
			listViewDetailColumn3->MinWidth = 237;
			listViewDetailColumn3->Width = 237;
			this->ListMonster->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(1) { listViewDetailColumn3 });
			this->ListMonster->ItemSpacing = -1;
			this->ListMonster->Location = System::Drawing::Point(0, 3);
			this->ListMonster->Name = L"ListMonster";
			this->ListMonster->Size = System::Drawing::Size(239, 335);
			this->ListMonster->TabIndex = 0;
			this->ListMonster->Text = L"radListView1";
			this->ListMonster->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			this->ListMonster->SelectedItemChanged += gcnew System::EventHandler(this, &Spells::ListMonster_SelectedItemChanged);
			this->ListMonster->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Spells::ListMonster_ItemRemoving);
			// 
			// PageViewAdvanced
			// 
			this->PageViewAdvanced->Controls->Add(this->radGroupBox4);
			this->PageViewAdvanced->Controls->Add(this->radGroupBox3);
			this->PageViewAdvanced->Controls->Add(this->ButtonRemoveCondition);
			this->PageViewAdvanced->Controls->Add(this->ButtonNewCondition);
			this->PageViewAdvanced->Controls->Add(this->radGroupBox5);
			this->PageViewAdvanced->Controls->Add(this->ListViewCondition);
			this->PageViewAdvanced->ItemSize = System::Drawing::SizeF(66, 28);
			this->PageViewAdvanced->Location = System::Drawing::Point(10, 37);
			this->PageViewAdvanced->Name = L"PageViewAdvanced";
			this->PageViewAdvanced->Size = System::Drawing::Size(444, 344);
			this->PageViewAdvanced->Text = L"Advanced";
			// 
			// radGroupBox4
			// 
			this->radGroupBox4->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox4->Controls->Add(this->radLabel3);
			this->radGroupBox4->Controls->Add(this->DropDownListAction);
			this->radGroupBox4->HeaderText = L"";
			this->radGroupBox4->Location = System::Drawing::Point(0, 284);
			this->radGroupBox4->Name = L"radGroupBox4";
			this->radGroupBox4->Size = System::Drawing::Size(228, 57);
			this->radGroupBox4->TabIndex = 24;
			// 
			// radLabel3
			// 
			this->radLabel3->Location = System::Drawing::Point(5, 5);
			this->radLabel3->Name = L"radLabel3";
			this->radLabel3->Size = System::Drawing::Size(38, 18);
			this->radLabel3->TabIndex = 18;
			this->radLabel3->Text = L"Action";
			this->radLabel3->ThemeName = L"ControlDefault";
			// 
			// DropDownListAction
			// 
			this->DropDownListAction->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			this->DropDownListAction->Location = System::Drawing::Point(5, 29);
			this->DropDownListAction->Name = L"DropDownListAction";
			this->DropDownListAction->Size = System::Drawing::Size(218, 20);
			this->DropDownListAction->TabIndex = 21;
			this->DropDownListAction->ThemeName = L"ControlDefault";
			this->DropDownListAction->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Spells::DropDownListAction_SelectedIndexChanged);
			// 
			// radGroupBox3
			// 
			this->radGroupBox3->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox3->Controls->Add(this->SpinEditorValuePoint);
			this->radGroupBox3->Controls->Add(this->radLabel4);
			this->radGroupBox3->HeaderText = L"";
			this->radGroupBox3->Location = System::Drawing::Point(237, 305);
			this->radGroupBox3->Name = L"radGroupBox3";
			this->radGroupBox3->Size = System::Drawing::Size(199, 36);
			this->radGroupBox3->TabIndex = 24;
			// 
			// SpinEditorValuePoint
			// 
			this->SpinEditorValuePoint->Location = System::Drawing::Point(126, 8);
			this->SpinEditorValuePoint->Name = L"SpinEditorValuePoint";
			this->SpinEditorValuePoint->Size = System::Drawing::Size(68, 20);
			this->SpinEditorValuePoint->TabIndex = 20;
			this->SpinEditorValuePoint->TabStop = false;
			this->SpinEditorValuePoint->ValueChanged += gcnew System::EventHandler(this, &Spells::SpinEditorValuePoint_ValueChanged);
			// 
			// radLabel4
			// 
			this->radLabel4->Location = System::Drawing::Point(5, 9);
			this->radLabel4->Name = L"radLabel4";
			this->radLabel4->Size = System::Drawing::Size(115, 18);
			this->radLabel4->TabIndex = 19;
			this->radLabel4->Text = L"Required value points";
			this->radLabel4->ThemeName = L"ControlDefault";
			// 
			// ButtonRemoveCondition
			// 
			this->ButtonRemoveCondition->Location = System::Drawing::Point(237, 229);
			this->ButtonRemoveCondition->Name = L"ButtonRemoveCondition";
			this->ButtonRemoveCondition->Size = System::Drawing::Size(199, 24);
			this->ButtonRemoveCondition->TabIndex = 25;
			this->ButtonRemoveCondition->Text = L"Remove";
			this->ButtonRemoveCondition->Click += gcnew System::EventHandler(this, &Spells::ButtonRemoveCondition_Click);
			// 
			// ButtonNewCondition
			// 
			this->ButtonNewCondition->Location = System::Drawing::Point(237, 199);
			this->ButtonNewCondition->Name = L"ButtonNewCondition";
			this->ButtonNewCondition->Size = System::Drawing::Size(199, 24);
			this->ButtonNewCondition->TabIndex = 24;
			this->ButtonNewCondition->Text = L"New";
			this->ButtonNewCondition->Click += gcnew System::EventHandler(this, &Spells::ButtonNewCondition_Click);
			// 
			// radGroupBox5
			// 
			this->radGroupBox5->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox5->Controls->Add(this->TextBoxValueCondition);
			this->radGroupBox5->Controls->Add(this->radLabel12);
			this->radGroupBox5->Controls->Add(this->radLabel13);
			this->radGroupBox5->Controls->Add(this->DropDownListCondition);
			this->radGroupBox5->HeaderText = L"";
			this->radGroupBox5->Location = System::Drawing::Point(0, 198);
			this->radGroupBox5->Name = L"radGroupBox5";
			this->radGroupBox5->Size = System::Drawing::Size(228, 80);
			this->radGroupBox5->TabIndex = 23;
			// 
			// TextBoxValueCondition
			// 
			this->TextBoxValueCondition->Location = System::Drawing::Point(45, 54);
			this->TextBoxValueCondition->Name = L"TextBoxValueCondition";
			this->TextBoxValueCondition->Size = System::Drawing::Size(178, 20);
			this->TextBoxValueCondition->TabIndex = 22;
			this->TextBoxValueCondition->TextChanged += gcnew System::EventHandler(this, &Spells::TextBoxValueCondition_TextChanged);
			// 
			// radLabel12
			// 
			this->radLabel12->Location = System::Drawing::Point(5, 5);
			this->radLabel12->Name = L"radLabel12";
			this->radLabel12->Size = System::Drawing::Size(55, 18);
			this->radLabel12->TabIndex = 18;
			this->radLabel12->Text = L"Condition";
			this->radLabel12->ThemeName = L"ControlDefault";
			// 
			// radLabel13
			// 
			this->radLabel13->Location = System::Drawing::Point(5, 55);
			this->radLabel13->Name = L"radLabel13";
			this->radLabel13->Size = System::Drawing::Size(34, 18);
			this->radLabel13->TabIndex = 19;
			this->radLabel13->Text = L"Value";
			this->radLabel13->ThemeName = L"ControlDefault";
			// 
			// DropDownListCondition
			// 
			this->DropDownListCondition->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			this->DropDownListCondition->Location = System::Drawing::Point(5, 29);
			this->DropDownListCondition->Name = L"DropDownListCondition";
			this->DropDownListCondition->Size = System::Drawing::Size(218, 20);
			this->DropDownListCondition->TabIndex = 21;
			this->DropDownListCondition->ThemeName = L"ControlDefault";
			this->DropDownListCondition->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Spells::DropDownListCondition_SelectedIndexChanged);
			// 
			// ListViewCondition
			// 
			this->ListViewCondition->AllowEdit = false;
			listViewDetailColumn4->HeaderText = L"Condition";
			listViewDetailColumn4->MaxWidth = 220;
			listViewDetailColumn4->MinWidth = 220;
			listViewDetailColumn4->Width = 220;
			listViewDetailColumn5->HeaderText = L"Action";
			listViewDetailColumn5->MaxWidth = 150;
			listViewDetailColumn5->MinWidth = 150;
			listViewDetailColumn5->Width = 150;
			listViewDetailColumn6->HeaderText = L"Value";
			listViewDetailColumn6->MaxWidth = 70;
			listViewDetailColumn6->MinWidth = 70;
			listViewDetailColumn6->Width = 70;
			this->ListViewCondition->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(3) {
				listViewDetailColumn4,
					listViewDetailColumn5, listViewDetailColumn6
			});
			this->ListViewCondition->ItemSpacing = -1;
			this->ListViewCondition->Location = System::Drawing::Point(0, 3);
			this->ListViewCondition->Name = L"ListViewCondition";
			this->ListViewCondition->Size = System::Drawing::Size(441, 189);
			this->ListViewCondition->TabIndex = 0;
			this->ListViewCondition->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			this->ListViewCondition->SelectedItemChanged += gcnew System::EventHandler(this, &Spells::ListViewCondition_SelectedItemChanged);
			this->ListViewCondition->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Spells::ListViewCondition_ItemRemoving);
			// 
			// radPageView1
			// 
			this->radPageView1->Controls->Add(this->radPageViewPage1);
			this->radPageView1->Controls->Add(this->radPageViewPage2);
			this->radPageView1->Controls->Add(this->radPageViewPage3);
			this->radPageView1->Location = System::Drawing::Point(6, 3);
			this->radPageView1->Name = L"radPageView1";
			this->radPageView1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->radPageView1->SelectedPage = this->radPageViewPage2;
			this->radPageView1->Size = System::Drawing::Size(229, 329);
			this->radPageView1->TabIndex = 28;
			this->radPageView1->Text = L"radPageView1";
			this->radPageView1->SelectedPageChanged += gcnew System::EventHandler(this, &Spells::radPageView1_SelectedPageChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->ItemFitMode = Telerik::WinControls::UI::StripViewItemFitMode::None;
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
			// 
			// radPageViewPage1
			// 
			this->radPageViewPage1->Controls->Add(this->spellSpellListHeal);
			this->radPageViewPage1->ItemSize = System::Drawing::SizeF(54, 28);
			this->radPageViewPage1->Location = System::Drawing::Point(10, 37);
			this->radPageViewPage1->Name = L"radPageViewPage1";
			this->radPageViewPage1->Size = System::Drawing::Size(208, 281);
			this->radPageViewPage1->Text = L"Healing";
			// 
			// radPageViewPage2
			// 
			this->radPageViewPage2->Controls->Add(this->spellSpellListAttack);
			this->radPageViewPage2->ItemSize = System::Drawing::SizeF(48, 28);
			this->radPageViewPage2->Location = System::Drawing::Point(10, 37);
			this->radPageViewPage2->Name = L"radPageViewPage2";
			this->radPageViewPage2->Size = System::Drawing::Size(208, 281);
			this->radPageViewPage2->Text = L"Attack";
			// 
			// spellSpellListAttack
			// 
			listViewDetailColumn7->HeaderText = L"Id";
			listViewDetailColumn8->HeaderText = L"spell_type";
			this->spellSpellListAttack->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(2) {
				listViewDetailColumn7,
					listViewDetailColumn8
			});
			this->spellSpellListAttack->Dock = System::Windows::Forms::DockStyle::Fill;
			this->spellSpellListAttack->Location = System::Drawing::Point(0, 0);
			this->spellSpellListAttack->Name = L"spellSpellListAttack";
			this->spellSpellListAttack->ShowCheckBoxes = true;
			this->spellSpellListAttack->Size = System::Drawing::Size(208, 281);
			this->spellSpellListAttack->TabIndex = 19;
			this->spellSpellListAttack->Text = L"radListView1";
			this->spellSpellListAttack->ThemeName = L"Office2013Dark";
			this->spellSpellListAttack->SelectedItemChanged += gcnew System::EventHandler(this, &Spells::spellSpellList_SelectedItemChanged);
			this->spellSpellListAttack->ItemCheckedChanged += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &Spells::spellSpellListAttack_ItemCheckedChanged);
			this->spellSpellListAttack->ItemValueChanged += gcnew Telerik::WinControls::UI::ListViewItemValueChangedEventHandler(this, &Spells::spellsListViewItemValueChanged);
			this->spellSpellListAttack->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Spells::spellSpellList_ItemRemoving);
			// 
			// radPageViewPage3
			// 
			this->radPageViewPage3->Controls->Add(this->radTreeView1);
			this->radPageViewPage3->ItemSize = System::Drawing::SizeF(48, 28);
			this->radPageViewPage3->Location = System::Drawing::Point(10, 37);
			this->radPageViewPage3->Name = L"radPageViewPage3";
			this->radPageViewPage3->Size = System::Drawing::Size(208, 281);
			this->radPageViewPage3->Text = L"Profile";
			// 
			// radTreeView1
			// 
			this->radTreeView1->AllowEdit = true;
			this->radTreeView1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(233)), static_cast<System::Int32>(static_cast<System::Byte>(240)),
				static_cast<System::Int32>(static_cast<System::Byte>(249)));
			this->radTreeView1->Cursor = System::Windows::Forms::Cursors::Default;
			this->radTreeView1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radTreeView1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
			this->radTreeView1->ForeColor = System::Drawing::Color::Black;
			this->radTreeView1->Location = System::Drawing::Point(0, 0);
			this->radTreeView1->Name = L"radTreeView1";
			this->radTreeView1->RadContextMenu = this->radContextMenu1;
			this->radTreeView1->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->radTreeView1->Size = System::Drawing::Size(208, 281);
			this->radTreeView1->SpacingBetweenNodes = -1;
			this->radTreeView1->TabIndex = 0;
			this->radTreeView1->Text = L"radTreeView1";
			this->radTreeView1->Editing += gcnew Telerik::WinControls::UI::TreeNodeEditingEventHandler(this, &Spells::radTreeView1_Editing);
			this->radTreeView1->ValueChanged += gcnew Telerik::WinControls::UI::TreeNodeValueChangedEventHandler(this, &Spells::radTreeView1_ValueChanged);
			// 
			// radContextMenu1
			// 
			this->radContextMenu1->DropDownOpening += gcnew System::ComponentModel::CancelEventHandler(this, &Spells::radContextMenu1_DropDownOpening);
			// 
			// radPanel3
			// 
			this->radPanel3->Controls->Add(this->radPanel5);
			this->radPanel3->Controls->Add(this->radPanel4);
			this->radPanel3->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel3->Location = System::Drawing::Point(0, 0);
			this->radPanel3->Name = L"radPanel3";
			this->radPanel3->Size = System::Drawing::Size(703, 22);
			this->radPanel3->TabIndex = 1;
			this->radPanel3->Text = L"radPanel3";
			// 
			// radPanel5
			// 
			this->radPanel5->Controls->Add(this->radMenu1);
			this->radPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel5->Location = System::Drawing::Point(100, 0);
			this->radPanel5->Name = L"radPanel5";
			this->radPanel5->Size = System::Drawing::Size(603, 22);
			this->radPanel5->TabIndex = 3;
			this->radPanel5->Text = L"radPanel5";
			// 
			// radPanel4
			// 
			this->radPanel4->Controls->Add(this->radToggleButtonSpellcaster);
			this->radPanel4->Dock = System::Windows::Forms::DockStyle::Left;
			this->radPanel4->Location = System::Drawing::Point(0, 0);
			this->radPanel4->Name = L"radPanel4";
			this->radPanel4->Size = System::Drawing::Size(100, 22);
			this->radPanel4->TabIndex = 2;
			this->radPanel4->Text = L"radPanel4";
			// 
			// radToggleButtonSpellcaster
			// 
			this->radToggleButtonSpellcaster->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radToggleButtonSpellcaster->Location = System::Drawing::Point(0, 0);
			this->radToggleButtonSpellcaster->Name = L"radToggleButtonSpellcaster";
			this->radToggleButtonSpellcaster->Size = System::Drawing::Size(100, 22);
			this->radToggleButtonSpellcaster->TabIndex = 0;
			this->radToggleButtonSpellcaster->Text = L"radToggleButton1";
			// 
			// Spells
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(703, 441);
			this->Controls->Add(this->radPanel2);
			this->Controls->Add(this->radPanel3);
			this->Cursor = System::Windows::Forms::Cursors::Default;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MaximizeBox = false;
			this->Name = L"Spells";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Spells";
			this->ThemeName = L"Office2013Dark";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Spells::Spells_FormClosing);
			this->Load += gcnew System::EventHandler(this, &Spells::Spells_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellAddButton))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellTextBoxId))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellSpellProperty))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellSpellListHeal))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownList1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellDropDownspell_type))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellDropDownUseType))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			this->radPanel2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView2))->EndInit();
			this->radPageView2->ResumeLayout(false);
			this->page_property->ResumeLayout(false);
			this->page_settings->ResumeLayout(false);
			this->page_settings->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_any_monster))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
			this->radGroupBox2->ResumeLayout(false);
			this->radGroupBox2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxMonsterName))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spinmin_hp))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spinmax_hp))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
			this->radGroupBox1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btDeleteMonster))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btNewMonster))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->gifPictureBox))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListMonster))->EndInit();
			this->PageViewAdvanced->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->EndInit();
			this->radGroupBox4->ResumeLayout(false);
			this->radGroupBox4->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListAction))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->EndInit();
			this->radGroupBox3->ResumeLayout(false);
			this->radGroupBox3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpinEditorValuePoint))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemoveCondition))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonNewCondition))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox5))->EndInit();
			this->radGroupBox5->ResumeLayout(false);
			this->radGroupBox5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->TextBoxValueCondition))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListCondition))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewCondition))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->EndInit();
			this->radPageView1->ResumeLayout(false);
			this->radPageViewPage1->ResumeLayout(false);
			this->radPageViewPage2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spellSpellListAttack))->EndInit();
			this->radPageViewPage3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTreeView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->EndInit();
			this->radPanel3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->EndInit();
			this->radPanel5->ResumeLayout(false);
			this->radPanel5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
			this->radPanel4->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellcaster))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

		Telerik::WinControls::UI::RadMenuItem^ ItemImport;
		Telerik::WinControls::UI::RadMenuItem^ ItemExport;

		void creatContextMenu(){
			ItemImport = gcnew Telerik::WinControls::UI::RadMenuItem("Import");
			ItemExport = gcnew Telerik::WinControls::UI::RadMenuItem("Export Current");

			ItemImport->Click += gcnew System::EventHandler(this, &Spells::MenuImport_Click);
			ItemExport->Click += gcnew System::EventHandler(this, &Spells::MenuExport_Click);
		}

		System::Void MenuImport_Click(Object^ sender, EventArgs^ e){
			if (!radTreeView1->SelectedNode)
				return;

			Telerik::WinControls::UI::RadTreeNode^ father = radTreeView1->SelectedNode->Parent;
			String^ dir = "\\";

			while (father){
				dir = "\\" + father->Text + dir;
				father = father->Parent;
			}

			String^ finaldir = Directory::GetCurrentDirectory() + dir;

			String ^path = finaldir + radTreeView1->SelectedNode->Text;

			System::Windows::Forms::DialogResult dialogResult = Telerik::WinControls::RadMessageBox::Show(this, "Import", "Deseja mesmo importa profile?", MessageBoxButtons::YesNo);

			if (dialogResult == System::Windows::Forms::DialogResult::No)
				return;

			spellSpellListHeal->Items->Clear();
			spellSpellListAttack->Items->Clear();
			radTreeView1->Nodes->Clear();
			radDropDownList1->Items->Clear();
			radContextMenu1->Items->Clear();

			if (!import_script(managed_util::fromSS(path)))
				Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
			else
				Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

			Spells_Load(nullptr, nullptr);
		}
		System::Void MenuExport_Click(Object^ sender, EventArgs^ e){
			if (!radTreeView1->SelectedNode)
				return;

			Telerik::WinControls::UI::RadTreeNode^ father = radTreeView1->SelectedNode->Parent;
			String^ dir = "\\";

			while (father){
				dir = "\\" + father->Text + dir;
				father = father->Parent;
			}

			uint32_t temp_count = 0;
			String^ finaldir = Directory::GetCurrentDirectory() + dir + radTreeView1->SelectedNode->Text + "\\" + radTreeView1->SelectedNode->Text;

			while (true){
				Sleep(1);

				String^ temp_dir = finaldir;
				temp_dir = temp_dir + temp_count + ".spell";

				if (!File::Exists(temp_dir)){
					finaldir = temp_dir;
					break;
				}

				temp_count += 1;
			}

			save_script(managed_util::fromSS(finaldir));
			radTreeView1->Nodes->Clear();
			loadFilesLua();
		}

		void loadPotions(){
			//radDropDownList1->BeginUpdate();

			std::vector<std::string> potions = ItemsManager::get()->get_potions();
			for (std::string potion : potions)
				radDropDownList1->Items->Add(gcnew String(&potion[0]));

			//radDropDownList1->EndUpdate();
		}
		void loadSpellsInfo();
		void loadConditionAndActions(){
			//DropDownListCondition->BeginUpdate();
			for (int i = 0; i < spells_conditions_t::spell_condition_total; i++){
				Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
				item->Text = GET_MANAGED_TR(std::string("spells_conditions_t_") + std::to_string(i));
				item->Value = i;
				DropDownListCondition->Items->Add(item);
			}
			//DropDownListCondition->EndUpdate();

			//DropDownListAction->BeginUpdate();
			for (int i = 0; i < spell_actions_t::spell_actions_total; i++){
				Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
				item->Text = GET_MANAGED_TR(std::string("spell_actions_t_") + std::to_string(i));
				item->Value = i;
				DropDownListAction->Items->Add(item);
			}
			DropDownListCondition->SelectedIndex = 0;
			DropDownListAction->SelectedIndex = 0;

			//DropDownListAction->EndUpdate();
		}

		System::Void Spells_Load(System::Object^  sender, System::EventArgs^  e);
		System::Void loadSpellManager();
		System::Void spellAddButton_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void spellSpellProperty_Edited(System::Object^ sender, Telerik::WinControls::UI::PropertyGridItemEditedEventArgs^  e);
		System::Void spellsListViewItemValueChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangedEventArgs^  e);
		System::Void spellSpellList_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
		Telerik::WinControls::UI::RadPropertyStore^ getGrid(int spell_type);
		System::Void spellSpellList_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void loadDateInGrid();
		System::Void radDropDownList1_TextChanged(System::Object^  sender, System::EventArgs^  e);

		bool save_script(std::string file_name){
			Json::Value file;
			file["version"] = 0100;

			file["SpellManager"] = SpellManager::get()->parse_class_to_json();

			return saveJsonFile(file_name, file);
		}
		bool load_script(std::string file_name){
			Json::Value file = loadJsonFile(file_name);
			if (file.isNull())
				return false;

			SpellManager::get()->parse_json_to_class(file["SpellManager"]);
			return true;
		}
		bool import_script(std::string file_name){
			Json::Value file = loadJsonFile(file_name);
			if (file.isNull())
				return false;

			SpellManager::get()->parse_json_to_classImport(file["SpellManager"]);
			return true;
		}

		System::Void spellDropDownUseType_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
		System::Void bt_save__Click(System::Object^  sender, System::EventArgs^  e) {
			System::Windows::Forms::SaveFileDialog fileDialog;
			fileDialog.Filter = "Spell Config Object|*.spell";
			fileDialog.Title = "Save Spell Config";

			if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
				return;

			if (!save_script(managed_util::fromSS(fileDialog.FileName)))
				Telerik::WinControls::RadMessageBox::Show(this, "Error Saved", "Save/Load");
			else
				Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved", "Save/Load");
		}
		System::Void bt_load__Click(System::Object^  sender, System::EventArgs^  e) {
			System::Windows::Forms::OpenFileDialog fileDialog;
			fileDialog.Filter = "Spell Config Object|*.spell";
			fileDialog.Title = "Open Spell Config";

			if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
				return;
			radDropDownList1->Items->Clear();
			spellSpellListHeal->Items->Clear();
			spellSpellListAttack->Items->Clear();

			SpellManager::get()->clear_all();

			if (!load_script(managed_util::fromSS(fileDialog.FileName)))
				Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
			else
				Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

			Spells_Load(nullptr, nullptr);
		}
		Telerik::WinControls::UI::RadListView^ get_current_list_view();
		System::Void radPageView1_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
			disable_notify = true;

			if (get_current_list_view()->Items->Count < 1)
				spellSpellProperty->SelectedObject = nullptr;
			else
				loadDateInGrid();

			if (radPageView1->SelectedPage->Name == "radPageViewPage2")
				enable_page_creatures();
			else
				disable_page_creatures();

			if (radPageView1->SelectedPage->Text == "Profile")
				disable_page_condition();
			else
				enable_page_condition();

			if (ListViewCondition->Items->Count <= 0 || get_current_list_view()->Items->Count <= 0)
				disable_page_condition();
			else
				enable_page_condition();

			updateSelectedItemCondition();

			disable_notify = false;
		}
		SpellBase* get_current_spell(){
			Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;
			if (!item || !item["spell_type"])
				return nullptr;

			int spell_type = (int)item["spell_type"];
			int id = Convert::ToInt32(item["Id"]->ToString());

			Telerik::WinControls::UI::RadPropertyStore^ GridSpell = getGrid(spell_type);

			if (spell_type > 0) {
				std::shared_ptr<SpellAttack> attack = SpellManager::get()->getAttackSpellInListById(id);
				return attack.get();
			}
			else
			{
				std::shared_ptr<SpellHealth> health = SpellManager::get()->getHealthSpellInListById(id);
				return health.get();
			}
		}
		System::Void spellTextBoxId_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			auto lv = get_current_list_view();
			if (lv && lv->SelectedItem){
				if (lv->SelectedItem->Text != spellTextBoxId->Text){
					lv->SelectedItem->Text = spellTextBoxId->Text;
					SpellBase* current = get_current_spell();

					if (current){
						current->set_visual_name(managed_util::fromSS(spellTextBoxId->Text));
						if (current->get_spell_type() > 0)
							ControlManager::get()->changePropertySpellsAttack(managed_util::fromSS(spellTextBoxId->Text),
							managed_util::fromSS(lv->SelectedItem["id"]->ToString()) + "Attack", "text");
						else
							ControlManager::get()->changePropertySpellsAttack(managed_util::fromSS(spellTextBoxId->Text),
							managed_util::fromSS(lv->SelectedItem["id"]->ToString()) + "Health", "text");
					}
				}
			}
		}

	private: System::Void btNewMonster_Click(System::Object^  sender, System::EventArgs^  e) {
				 add_in_listmonster("new monster");
	}
	private: System::Void ListMonster_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_notify)
					 return;

				 updateSelectedData();
				 UpdateGif();
	}

			 void add_in_listmonster(String^ NAME){
				 Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;
				 if (!item || !item["spell_type"])
					 return;

				 int id = Convert::ToInt32(item["Id"]->ToString());

				 std::shared_ptr<SpellAttack> attack;

				 if ((int)item["spell_type"] > 0)
					 attack = SpellManager::get()->getAttackSpellInListById(id);

				 if (!attack)
					 return;

				 Telerik::WinControls::UI::ListViewDataItem^ dataItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
				 dataItem->SubItems->Add(NAME);
				 dataItem->Tag = Convert::ToString(attack.get()->request_new_id());
				 ListMonster->Items->Add(dataItem);

				 if (ListMonster->Items->Count > 0)
					 radGroupBox2->Enabled = true;
			 }

			 void updateSelectedData(){
				 if (!ListMonster->SelectedItem)
					 return;

				 if (radPageView2->SelectedPage->Name != "page_settings")
					 return;

				 Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;
				 if (!item || !item["spell_type"])
					 return;

				 int id = Convert::ToInt32(item["Id"]->ToString());


				 std::shared_ptr<SpellAttack> attack;


				 if ((int)item["spell_type"] > 0)
					 attack = SpellManager::get()->getAttackSpellInListById(id);

				 if (!attack)
					 return;

				 if (ListMonster->SelectedItem->Tag == "")
					 return;

				 int monsterId = Convert::ToInt32(ListMonster->SelectedItem->Tag);
				 std::shared_ptr<MonsterInfo> monsterRule = attack.get()->get_monster_rule(monsterId);
				 String^ name = gcnew String(monsterRule->get_name().c_str());
				 int max = monsterRule->get_max_hp();
				 int min = monsterRule->get_min_hp();

				 disable_notify = true;
				 boxMonsterName->Text = name;
				 spinmax_hp->Value = max;
				 spinmin_hp->Value = min;
				 disable_notify = false;

				 updateSelectedItemCondition();
			 }
			 std::shared_ptr<SpellAttack> get_spell_attack_rule(){
				 Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;
				 if (!item || !item["spell_type"])
					 return nullptr;

				 int id = Convert::ToInt32(item["Id"]->ToString());

				 if ((int)item["spell_type"] > 0){
					 std::shared_ptr<SpellAttack> attack = SpellManager::get()->getAttackSpellInListById(id);

					 if (!attack)
						 return nullptr;

					 return attack;
				 }
				 return nullptr;
			 }

	private: System::Void spinmax_hp_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_notify)
					 return;

				 Telerik::WinControls::UI::RadListView^ radListview = ListMonster;

				 if (!radListview)
					 return;

				 if (!radListview->SelectedItem)
					 return;

				 std::shared_ptr<SpellAttack> attack = get_spell_attack_rule();

				 if (!attack)
					 return;

				 int monsterId = Convert::ToInt32(ListMonster->SelectedItem->Tag->ToString());
				 auto monsterRule = attack.get()->get_monster_rule(monsterId);

				 if (!monsterRule)
					 return;

				 monsterRule->set_max_hp((int)spinmax_hp->Value);
	}
	private: System::Void spinmin_hp_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_notify)
					 return;

				 if (!ListMonster->SelectedItem)
					 return;

				 std::shared_ptr<SpellAttack> attack = get_spell_attack_rule();

				 if (!attack)
					 return;

				 int monsterId = Convert::ToInt32(ListMonster->SelectedItem->Tag->ToString());
				 auto monsterRule = attack.get()->get_monster_rule(monsterId);

				 if (!monsterRule)
					 return;

				 monsterRule->set_min_hp((int)spinmin_hp->Value);
	}
	private: System::Void boxMonsterName_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_notify)
					 return;

				 if (!ListMonster->SelectedItem)
					 return;

				 ListMonster->SelectedItem[0] = boxMonsterName->Text;

				 std::shared_ptr<SpellAttack> attack = get_spell_attack_rule();

				 if (!attack)
					 return;

				 int monsterId;

				 try{
					 monsterId = Convert::ToInt32(ListMonster->SelectedItem->Tag->ToString());
				 }
				 catch (...){
					 return;
				 }

				 auto monsterRule = attack.get()->get_monster_rule(monsterId);

				 if (!monsterRule)
					 return;

				 monsterRule->set_name(managed_util::fromSS(boxMonsterName->Text));

				 UpdateGif();
	}
	private: System::Void btDeleteMonster_Click(System::Object^  sender, System::EventArgs^  e) {
				 ListMonster_ItemRemoving(sender, nullptr);
	}
	private: System::Void ListMonster_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
				 Telerik::WinControls::UI::ListViewDataItem^ itemRow = ListMonster->SelectedItem;

				 if (!itemRow)
					 return;

				 String^ tempString = (String^)itemRow->Tag;

				 if (tempString->Trim() == "")
					 return;

				 std::shared_ptr<SpellAttack> attack = get_spell_attack_rule();

				 if (!attack)
					 return;

				 attack.get()->remove_id(Convert::ToInt32(tempString));
				 ListMonster->Items->Remove(itemRow);

				 if (ListMonster->Items->Count > 0)
					 radGroupBox2->Enabled = true;
				 else
					 radGroupBox2->Enabled = false;
	}

			 void disable_page_creatures(){
				 ListMonster->Enabled = false;
				 radGroupBox1->Enabled = false;
				 radGroupBox2->Enabled = false;
			 }
			 void enable_page_creatures(){
				 ListMonster->Enabled = true;
				 radGroupBox1->Enabled = true;
				 radGroupBox2->Enabled = true;
			 }

			 void disable_page_condition(){
				 if (ListViewCondition->Enabled || radGroupBox5->Enabled || radGroupBox4->Enabled){
					 ListViewCondition->Enabled = false;
					 radGroupBox5->Enabled = false;
					 radGroupBox4->Enabled = false;
				 }
			 }
			 void enable_page_condition(){
				 if (!ListViewCondition->Enabled || !radGroupBox5->Enabled || !radGroupBox4->Enabled){
					 ListViewCondition->Enabled = true;
					 radGroupBox5->Enabled = true;
					 radGroupBox4->Enabled = true;
				 }
			 }

	private: System::Void radPageView2_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
				 disable_notify = true;
				 if (radPageView1->SelectedPage->Name == "radPageViewPage2"){
					 if (get_current_list_view()->Items->Count < 1){
						 ListMonster->Enabled = false;
						 radGroupBox1->Enabled = false;
						 radGroupBox2->Enabled = false;
						 radButton1->Enabled = false;
					 }
				 }
				 else if (radPageView1->SelectedPage->Name == "radPageViewPage1"){
					 ListMonster->Enabled = false;
					 radGroupBox1->Enabled = false;
					 radGroupBox2->Enabled = false;
					 radButton1->Enabled = false;
				 }
				 ListMonster->SelectedIndex = -1;

				 if (ListViewCondition->Items->Count <= 0)
					 disable_page_condition();
				 else
					 enable_page_condition();

				 disable_notify = false;
	}
	private: System::Void listProfile_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
				 e->Cancel = true;
	}
	private: System::Void listProfile_ItemMouseDoubleClick(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
				 System::Windows::Forms::OpenFileDialog fileDialog;
				 fileDialog.Filter = "Spell Config Object|*.spell";
				 fileDialog.Title = "Open Spell Config";

				 if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 disable_notify = true;
				 spellSpellListHeal->Items->Clear();
				 spellSpellListAttack->Items->Clear();
				 radDropDownList1->Items->Clear();
				 disable_notify = false;

				 if (!import_script(managed_util::fromSS(fileDialog.FileName)))
					 Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
				 else
					 Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

				 Spells_Load(nullptr, nullptr);
	}

			 void loadFilesLua(){
				// radTreeView1->BeginUpdate();

				 String^ dirFiles = Directory::GetCurrentDirectory() + "\\Profile";

				 if (!Directory::Exists(dirFiles))
					 _mkdir(&managed_util::fromSS(dirFiles)[0]);

				 DirectoryInfo^ directory = gcnew DirectoryInfo(dirFiles);
				 Telerik::WinControls::UI::RadTreeNode^ Node = gcnew Telerik::WinControls::UI::RadTreeNode(directory->Name);

				 Node->Expanded = true;
				 Node->Value = "folder";
				 loadDirectoriesAndFiles(directory, Node);

				 radTreeView1->Nodes->AddRange(Node);

				/// radTreeView1->EndUpdate();
			 }
			 void loadDirectoriesAndFiles(DirectoryInfo^ dir, Telerik::WinControls::UI::RadTreeNode^ parentNode){
				 for each(FileInfo^ f in dir->GetFiles()){
					 Telerik::WinControls::UI::RadTreeNode^ fileNode = gcnew Telerik::WinControls::UI::RadTreeNode(f->Name);
					 fileNode->Value = f->Name;
					 parentNode->Nodes->AddRange(fileNode);
				 }

				 for each(DirectoryInfo^ d in dir->GetDirectories()){
					 Telerik::WinControls::UI::RadTreeNode^ dirNode = gcnew Telerik::WinControls::UI::RadTreeNode(d->Name);
					 dirNode->Value = "folder";
					 parentNode->Nodes->AddRange(dirNode);
					 loadDirectoriesAndFiles(d, dirNode);
				 }
			 }

			 void UpdateGif(){
				 String^ name = boxMonsterName->Text;
				 std::pair<char*, uint32_t> data_buffer = GifManager::get()->get_item_creature_buffer(managed_util::fromSS(name + ".gif"));
				 if (data_buffer.first){
					 System::IO::UnmanagedMemoryStream^ readStream =
						 gcnew System::IO::UnmanagedMemoryStream
						 ((unsigned char*)data_buffer.first, data_buffer.second);
					 Image^ new_image = System::Drawing::Image::FromStream(readStream);
					 if (new_image->Width > gifPictureBox->Width || new_image->Height > gifPictureBox->Height)
						 gifPictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
					 else
						 gifPictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
					 this->gifPictureBox->Image = new_image;
				 }
				 else
					 this->gifPictureBox->Image = nullptr;
			 }

	private: System::Void radContextMenu1_DropDownOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
				 radContextMenu1->Items->Clear();

				 if (!radTreeView1->SelectedNode)
					 return;

				 if (radTreeView1->SelectedNode == radTreeView1->TopNode){
					 e->Cancel = true;
				 }

				 if (radTreeView1->SelectedNode->Value != "folder"){
					 radContextMenu1->Items->Add(ItemImport);
					 return;
				 }
				 else{
					 radContextMenu1->Items->Add(ItemExport);
				 }
	}

			 void setDefault(String^ nameSpell);

	private: System::Void radDropDownList1_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 Telerik::WinControls::UI::RadDropDownList ^ control = (Telerik::WinControls::UI::RadDropDownList^)sender;
				 setDefault(control->Text);
	}
	private: System::Void boxMonsterName_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_notify)
					 return;

				 if (!ListMonster->SelectedItem)
					 return;

				 ListMonster->SelectedItem[0] = boxMonsterName->Text;

				 std::shared_ptr<SpellAttack> attack = get_spell_attack_rule();

				 if (!attack)
					 return;

				 int monsterId;

				 try{
					 monsterId = Convert::ToInt32(ListMonster->SelectedItem->Tag->ToString());
				 }
				 catch (...){
					 return;
				 }

				 auto monsterRule = attack.get()->get_monster_rule(monsterId);

				 if (!monsterRule)
					 return;

				 monsterRule->set_name(managed_util::fromSS(boxMonsterName->Text));

				 UpdateGif();
	}
	private: System::Void spellSpellListAttack_ItemCheckedChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
				 Telerik::WinControls::UI::ListViewDataItem^ item = e->Item;
				 int id = Convert::ToInt32(item["id"]);
				 int spell_type = Convert::ToInt32(item["spell_type"]);

				 if (spell_type > 0) {
					 std::shared_ptr<SpellAttack> spell = SpellManager::get()->getAttackSpellInListById(id);
					 spell->set_checked(item->CheckState ==
						 Telerik::WinControls::Enumerations::ToggleState::On);

					 ControlManager::get()->changePropertySpellsAttack(item->CheckState == Telerik::WinControls::Enumerations::ToggleState::On ? "true" : "false",
						 managed_util::fromSS(id.ToString()) + "Attack", "state");
				 }
				 else {
					 std::shared_ptr<SpellHealth> spell = SpellManager::get()->getHealthSpellInListById(id);
					 spell->set_checked(item->CheckState == Telerik::WinControls::Enumerations::ToggleState::On);

					 ControlManager::get()->changePropertySpellsHealth(item->CheckState == Telerik::WinControls::Enumerations::ToggleState::On ? "true" : "false",
						 managed_util::fromSS(id.ToString()) + "Health", "state");
				 }

				 
	}
	private: System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
				 NeutralBot::AutoGenerateLoot^ tempForm = gcnew NeutralBot::AutoGenerateLoot(1);
				 tempForm->ShowDialog();
				 bool check_exist = true;
				 if (tempForm->comfirmed){
					 for each(String^ item in tempForm->selectedItems){
						 add_in_listmonster(item);
						 boxMonsterName->Text = ListMonster->SelectedItem[0]->ToString();
					 }
				 }
	}
			 String^ oldValue;
	private: System::Void radTreeView1_Editing(System::Object^  sender, Telerik::WinControls::UI::TreeNodeEditingEventArgs^  e) {
				 if (e->Node->Value == "folder")
					 e->Cancel = true;

				 oldValue = e->Node->Text;
	}
	private: System::Void radTreeView1_NodeMouseDoubleClick(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
				 e->Node->BeginEdit();
	}
	private: System::Void radTreeView1_ValueChanged(System::Object^  sender, Telerik::WinControls::UI::TreeNodeValueChangedEventArgs^  e) {
				 if (!radTreeView1->SelectedNode)
					 return;

				 if (radTreeView1->SelectedNode->Text == oldValue)
					 return;

				 Telerik::WinControls::UI::RadTreeNode^ father = radTreeView1->SelectedNode->Parent;
				 String^ dir = "\\";

				 while (father){
					 dir = "\\" + father->Text + dir;
					 father = father->Parent;
				 }

				 String^ finaldir = Directory::GetCurrentDirectory() + dir;
				 String^ antigo = finaldir + oldValue;
				 String^ novo = finaldir + radTreeView1->SelectedNode->Text + ".spell";

				 if (!File::Exists(novo)){
					 File::Move(antigo, novo);
					 radTreeView1->SelectedNode->Text = radTreeView1->SelectedNode->Text + ".spell";
				 }
				 else
					 radTreeView1->SelectedNode->Text = oldValue;

	}
	private: System::Void menu_config_Click(System::Object^  sender, System::EventArgs^  e) {
				 auto temp_config = (gcnew NeutralBot::SpellsAutoConfig);

				 temp_config->ShowDialog();
				 radDropDownList1->Items->Clear();
				 spellSpellListHeal->Items->Clear();
				 spellSpellListAttack->Items->Clear();
				 radTreeView1->Nodes->Clear();
				 radContextMenu1->Items->Clear();
				 Spells_Load(nullptr, nullptr);
	}

			 void update_idiom(){
				 spellAddButton->Text = gcnew String(&GET_TR(managed_util::fromSS(spellAddButton->Text))[0]);
				 bt_save_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save_->Text))[0]);
				 bt_load_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load_->Text))[0]);
				 menu_config->Text = gcnew String(&GET_TR(managed_util::fromSS(menu_config->Text))[0]);
				 page_property->Text = gcnew String(&GET_TR(managed_util::fromSS(page_property->Text))[0]);
				 page_settings->Text = gcnew String(&GET_TR(managed_util::fromSS(page_settings->Text))[0]);
				 PageViewAdvanced->Text = gcnew String(&GET_TR(managed_util::fromSS(PageViewAdvanced->Text))[0]);
				 radButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton1->Text))[0]);
				 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
				 btDeleteMonster->Text = gcnew String(&GET_TR(managed_util::fromSS(btDeleteMonster->Text))[0]);
				 btNewMonster->Text = gcnew String(&GET_TR(managed_util::fromSS(btNewMonster->Text))[0]);
				 radPageViewPage1->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewPage1->Text))[0]);
				 radPageViewPage2->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewPage2->Text))[0]);
				 radPageViewPage3->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewPage3->Text))[0]);
				 check_any_monster->Text = gcnew String(&GET_TR(managed_util::fromSS(check_any_monster->Text))[0]);
				 radLabel12->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel12->Text))[0]);
				 radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
				 radLabel13->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel13->Text))[0]);
				 radLabel4->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel4->Text))[0]);
				 ButtonNewCondition->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonNewCondition->Text))[0]);
				 ButtonRemoveCondition->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonRemoveCondition->Text))[0]);

				 for each(auto colum in ListMonster->Columns)
					 colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);

				 for each(auto colum in ListViewCondition->Columns)
					 colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
			 }
			 void update_idiom_listbox(){
				 spellDropDownspell_type->BeginUpdate();
				 spellDropDownspell_type->Items->Clear();

				 for (int i = spell_type_t_1; i < (int)spell_type_t_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(&(std::string("spell_type_t_") + std::to_string(i))[0]);
					 item->Value = i;
					 spellDropDownspell_type->Items->Add(item);
				 }
				 spellDropDownspell_type->EndUpdate();


				 spellDropDownUseType->BeginUpdate();
				 spellDropDownUseType->Items->Clear();
				 for (int i = spell_type_t_6; i < (int)spell_type_t_total2; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(&(std::string("spell_type_t_") + std::to_string(i))[0]);
					 item->Value = i;
					 spellDropDownUseType->Items->Add(item);
				 }
				 spellDropDownUseType->EndUpdate();
			 }

	private: System::Void check_any_monster_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (radGroupBox1->Enabled || radGroupBox2->Enabled){
					 radGroupBox1->Enabled = false;
					 radGroupBox2->Enabled = false;
				 }
				 else{
					 radGroupBox1->Enabled = true;
					 radGroupBox2->Enabled = true;
				 }

				 std::shared_ptr<SpellAttack> attack = get_spell_attack_rule();

				 if (!attack)
					 return;

				 attack->set_any_monster(check_any_monster->Checked);
	}
	private: System::Void Spells_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {

				 if (GeneralManager::get()->get_practice_mode()){
					 this->Hide();
					 e->Cancel = true;
					 return;
				 }
	}
			 
	private: System::Void ButtonNewCondition_Click(System::Object^  sender, System::EventArgs^  e) {
				 newItemCondition();
	}
	private: System::Void ButtonRemoveCondition_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (ListViewCondition->Items->Count <= 0)
					 disable_page_condition();
				 else
					 enable_page_condition();

				 ListViewCondition_ItemRemoving(nullptr, nullptr);
	}

	private: System::Void ListViewCondition_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_notify)
					 return;

				 updateSelectedItemCondition();
	}
	private: System::Void ListViewCondition_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
				 Telerik::WinControls::UI::ListViewDataItem^ itemRow = ListViewCondition->SelectedItem;

				 if (!itemRow)
					 return;

				 auto spellBaseCoreRule = get_current_spell();
				 if (!spellBaseCoreRule)
					 return;

				 int index = ListViewCondition->Items->IndexOf(ListViewCondition->SelectedItem);

				 spellBaseCoreRule->vector_condition.erase(spellBaseCoreRule->vector_condition.begin() + index);
				 ListViewCondition->Items->Remove(itemRow);

				 if (ListMonster->Items->Count > 0)
					 radGroupBox2->Enabled = true;
				 else
					 radGroupBox2->Enabled = false;
	}

			 void newItemCondition(){
				 auto spellBaseRule = get_current_spell();
				 if (!spellBaseRule)
					 return;

				 Telerik::WinControls::UI::ListViewDataItem^ dataItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
				 dataItem[0] = "none";
				 dataItem[1] = "none";

				 spellBaseRule->add_vector_condition("", (spells_conditions_t)0, (spell_actions_t)0);

				 ListViewCondition->Items->Add(dataItem);

				 updateSelectedItemCondition();
				 
				 enable_page_condition();
			 }

			 void updateSelectedItemCondition(){
				 if (!ListViewCondition->SelectedItem)
					 return;

				 if (radPageView2->SelectedPage->Name != "PageViewAdvanced")
					 return;

				 auto spellBaseRule = get_current_spell();
				 if (!spellBaseRule)
					 return;
				 
				 int index = ListViewCondition->Items->IndexOf(ListViewCondition->SelectedItem);
				 std::shared_ptr<SpellCondition> conditionRule = spellBaseRule->get_vector_condition_rule(index);
				 if (!conditionRule)
					 return;

				 String^ conditionValue = gcnew String(conditionRule->get_value().c_str());

				 disable_notify = true;
				 DropDownListCondition->SelectedIndex = (int)conditionRule->condition_type;
				 DropDownListAction->SelectedIndex = (int)conditionRule->action_type;

				 TextBoxValueCondition->Text = conditionValue;
				 ListViewCondition->SelectedItem[0] = DropDownListCondition->Text;
				 ListViewCondition->SelectedItem[1] = DropDownListAction->Text;

				 SpinEditorValuePoint->Value = spellBaseRule->get_required_value_point();
				 disable_notify = false;
			 }

	private: System::Void TextBoxValueCondition_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_notify)
					 return;

				 if (!ListViewCondition->SelectedItem)
					 return;

				 auto spellBaseCoreRule = get_current_spell();
				 if (!spellBaseCoreRule)
					 return;

				 int index = ListViewCondition->Items->IndexOf(ListViewCondition->SelectedItem);
				 spellBaseCoreRule->vector_condition[index]->set_value(managed_util::fromSS(TextBoxValueCondition->Text));
				 ListViewCondition->SelectedItem["Value"] = TextBoxValueCondition->Text;
	}
	private: System::Void SpinEditorValuePoint_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 auto spellBaseCoreRule = get_current_spell();
				 if (!spellBaseCoreRule)
					 return;

				 spellBaseCoreRule->set_required_value_point((int)SpinEditorValuePoint->Value);
	}

	private: System::Void DropDownListAction_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_notify)
					 return;

				 if (!ListViewCondition->SelectedItem)
					 return;

				 auto spellBaseCoreRule = get_current_spell();
				 if (!spellBaseCoreRule)
					 return;

				 int index = ListViewCondition->Items->IndexOf(ListViewCondition->SelectedItem);

				 spellBaseCoreRule->vector_condition[index]->action_type = (spell_actions_t)DropDownListAction->SelectedIndex;
				 ListViewCondition->SelectedItem["Action"] = DropDownListAction->Text;
	}
	private: System::Void DropDownListCondition_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_notify)
					 return;

				 if (!ListViewCondition->SelectedItem)
					 return;

				 auto spellBaseCoreRule = get_current_spell();
				 if (!spellBaseCoreRule)
					 return;

				 int index = ListViewCondition->Items->IndexOf(ListViewCondition->SelectedItem);

				 spellBaseCoreRule->vector_condition[index]->condition_type = (spells_conditions_t)DropDownListCondition->SelectedIndex;
				 ListViewCondition->SelectedItem[0] = DropDownListCondition->Text;
	}
	};
}