#pragma once 
#include "NavigationManager.h"
#include "Core\TibiaProcess.h"
#include "Core\HunterCore.h"
#include "Core\Inventory.h"
#include "WaypointManager.h"
#include "Core\ContainerManager.h"
#include "JsonManager.h"
#include <thread>

void NavigationManager::send_player_info(std::string player_to_send){
	Json::Value json_player_info = TibiaProcess::get_default()->character_info->parse_class_to_json();
	std::string string_player_info = NaviJsonManager::get()->parse_json_to_string(json_player_info);
	std::string _data = change_message_to_packet(string_player_info, protocol_t::protocol_player_info, player_to_send);

	std::shared_ptr<std::string> _buffer(new std::string(_data.data(), _data.size()));
	client_in->add_send_data(_buffer);
	return;
}

void NavigationManager::send_player_inventory(std::string player_to_send){
	Json::Value json_player_info = Inventory::get()->parse_class_to_json();
	std::string string_player_info = NaviJsonManager::get()->parse_json_to_string(json_player_info);
	std::string _data = change_message_to_packet(string_player_info, protocol_t::protocol_player_inventory, player_to_send);
	
	std::shared_ptr<std::string> _buffer(new std::string(_data.data(), _data.size()));
	client_in->add_send_data(_buffer);
	return;
}

void NavigationManager::send_player_containers(std::string player_to_send){
	Json::Value json_player_info = ContainerManager::get()->parse_class_to_json();
	std::string string_player_info = NaviJsonManager::get()->parse_json_to_string(json_player_info);
	std::string _data = change_message_to_packet(string_player_info, protocol_t::protocol_player_containers, player_to_send);

	std::shared_ptr<std::string> _buffer(new std::string(_data.data(), _data.size()));
	client_in->add_send_data(_buffer);
	return;
}

void NavigationManager::send_player_waypointer(std::string player_to_send){
	Json::Value json_player_info = WaypointManager::get()->parse_class_to_json_navi();
	std::string string_player_info = NaviJsonManager::get()->parse_json_to_string(json_player_info);
	std::string _data = change_message_to_packet(string_player_info, protocol_t::protocol_player_waypointer, player_to_send);

	std::shared_ptr<std::string> _buffer(new std::string(_data.data(), _data.size()));
	client_in->add_send_data(_buffer);
	return;
}

void NavigationManager::send_player_creatures(std::string player_to_send){
	Json::Value json_player_info = BattleList::get()->parse_class_to_json();
	std::string string_player_info = NaviJsonManager::get()->parse_json_to_string(json_player_info);
	std::string _data = change_message_to_packet(string_player_info, protocol_t::protocol_player_creatures, player_to_send);

	std::shared_ptr<std::string> _buffer(new std::string(_data.data(), _data.size()));
	client_in->add_send_data(_buffer);
	return;
}
void NavigationManager::send_player_message(std::string message,std::string player_to_send){
	std::string _data = change_message_to_packet(message, protocol_t::protocol_text_message, player_to_send);

	std::shared_ptr<std::string> _buffer(new std::string(_data.data(), _data.size()));
	client_in->add_send_data(_buffer);
	return;
}

std::string NavigationManager::change_message_to_packet(std::string& message, protocol_t protocol_in, std::string player_to_send){
	std::string string_to_send = message;

	NaviStreamWriter writer;

	writer.allocate(sizeof(NaviPacketHeader)-1);

	writer.add_string(TibiaProcess::get_default()->character_info->get_name_char());
	writer.add(uint32_t(1));
	writer.add_string(player_to_send);
	writer.add(uint32_t(2));
	writer.add_string(string_to_send);

	std::string _data = writer.get_data();

	NaviPacketHeader* header = (NaviPacketHeader*)_data.data();
	header->len = _data.size();
	header->protocol = protocol_in;
	header->update_checksun();

	return _data;
}

void NavigationManager::process_message(char* buffer, uint16_t dados_no_buffer){
	if (!dados_no_buffer)
		return;

	if (buffer)
		rest_buffer.append(buffer, dados_no_buffer);

	NaviStreamReader reader((uint8_t*)rest_buffer.data(), rest_buffer.size());
	if (!reader.can_read_bytes(sizeof(NaviPacketHeader)-1))
		return;

	NaviPacketHeader* header = (NaviPacketHeader*)reader.get_current_data_pointer();
	if (!header)
		return;

	reader.skip(sizeof(NaviPacketHeader)-1);

	if (!header->len)
		return;

	if (rest_buffer.length() < header->len)
		return;

	if (!header->verify_checksun())
		return;

	std::string char_name = reader.get_string();
	uint32_t member = reader.get<uint32_t>();
	std::string player_to_send = reader.get_string();
	uint32_t member2 = reader.get<uint32_t>();

	mtx.lock();
	switch (header->protocol){
	case protocol_none:{
						   return;
	}
		break;
	case protocol_text_message:{
		std::string string_recv = reader.get_string();
		simple_string = string_recv;
		std::cout << "\n SIMPLE_STRING: " << string_recv;
	}
		break;
	case protocol_data_message:{

	}
		break;
	case protocol_player_info:{
		std::string string_recv = reader.get_string();

		Json::Value json_player_info = NaviJsonManager::get()->parse_string_to_json(string_recv);
		NavigationManager::get()->add_player_info(char_name, json_player_info);
	}
		break;
	case protocol_player_inventory:{
		std::string string_recv = reader.get_string();

		Json::Value json_player_info = NaviJsonManager::get()->parse_string_to_json(string_recv);
		NavigationManager::get()->add_player_inventory(char_name, json_player_info);
	}
		break;
	case protocol_player_containers:{
		std::string string_recv = reader.get_string();

		Json::Value json_player_info = NaviJsonManager::get()->parse_string_to_json(string_recv);
		NavigationManager::get()->add_player_containers(char_name, json_player_info);
	}
		break;
	case protocol_player_creatures:{
		std::string string_recv = reader.get_string();

		Json::Value json_player_info = NaviJsonManager::get()->parse_string_to_json(string_recv);
		NavigationManager::get()->add_player_creatures(char_name, json_player_info);
	}
		break;
	case protocol_player_storage:{}
		break;
	case protocol_total:{}
		break;
	default:
		break;
	}
	mtx.unlock();

	if (rest_buffer.length() == header->len)
		rest_buffer = "";
	else
		rest_buffer = std::string(reader.get_current_data_pointer(), reader.bytes_left());
	//rest_buffer.substr(header->len, rest_buffer.size() - header->len);

	std::cout << "\n CHAR NAME: " << char_name;

	if (!rest_buffer.size())
		return;

	process_message(0, dados_no_buffer - header->len);
}

void NavigationManager::star_loop_client(){
	std::thread([&](){		
		client_in = std::shared_ptr<Client>(new Client(svc));

		while (true) {
			Sleep(10);
			try {
				svc.run();
			}
			catch (std::exception& ex) {
				std::cout << "\n Runner out reason: " << ex.what();
			}
		}
	}).detach();
}

void NavigationManager::connect_to_server(std::string ip, uint32_t port, std::string server_name, std::string server_pass){
	if (!client_in)
		return;

	mtx.lock();
	if (current_room != server_name || current_pass == server_pass){
		current_room = server_name;
		current_pass = server_pass;
	}
	mtx.unlock();

	std::string temp = "enchendo linguica";

	this->add_task(std::bind([](std::string _ip, uint16_t _port, std::shared_ptr<Client> _cli) -> void {
		_cli->set_connect(_ip, _port);
	},
		ip, port, client_in),2000);	
}

void NavigationManager::frist_packet(){
	Json::Value serverInfoClass;

	mtx.lock();
	serverInfoClass["current_name"] = current_room;
	serverInfoClass["current_pass"] = current_pass;
	mtx.unlock();

	std::string str_json = NaviJsonManager::get()->parse_json_to_string(serverInfoClass);
	std::string str_to_end = NavigationManager::get()->change_message_to_packet(str_json, protocol_t::protocol_change_room);

	std::shared_ptr<std::string> _buffer(new std::string(str_to_end.data(), str_to_end.size()));

	client_in->add_send_data(_buffer);

	std::cout << "\n connectado com sucesso!";
}