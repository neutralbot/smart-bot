#pragma once
#include "WaypointInfo.h"
#include "map"
#include "iostream"
#include <vector>
#include <json\json.h>

class WaypointPath{
	std::string label;
	std::vector<std::shared_ptr<WaypointInfo>> waypointInfoList;
	std::string name; 

public:
	GET_SET(std::string, label);

	int requestNewWaypointInfoIndex();
	std::vector<std::shared_ptr<WaypointInfo>> getwaypointInfoList(){
		return waypointInfoList; 
	}

	void setWaypointInfo(int index, std::shared_ptr<WaypointInfo> newWaypointInfo);
	std::shared_ptr<WaypointInfo> getWaypointInfo(int index);
	std::shared_ptr<WaypointInfo> getWaypointInfoByRawPointer(WaypointInfo* raw_ptr);

	void changeIndexWaypointInfo(int indexOld, int indexNew){
		std::shared_ptr<WaypointInfo> waypointInfo = getWaypointInfo(indexOld);

		if (!waypointInfo)
			return;

		removeWaypointInfo(indexOld);

		if (indexNew > static_cast<int>(this->waypointInfoList.size()))
			waypointInfoList.push_back(waypointInfo);
		else
			insertWaypointInfo(indexNew, waypointInfo);
	}

	void removeWaypointInfo(int index);
	void insertWaypointInfo(int index, std::shared_ptr<WaypointInfo> waypointInfo);
	void removeWaypointInfo(std::string label);
	int getNextWaypointInfoIdIgnoreLure(int currentIndex);

	void setName(std::string name);
	std::string getName();
	int getNextWaypointInfoId(int currentIndex);

	int getBeforeWaypointInfoId(int currentIndex);
	std::shared_ptr<WaypointInfo> getNextWaypointInfo(int currentIndex);
	std::shared_ptr<WaypointInfo> getBeforeWaypointInfo(int currentIndex);
	std::shared_ptr<WaypointInfo> getBeforeWaypointInfoLure(int currentIndex, int offset);
	
	int getWaypointInfoIdByLabel(std::string label);
	std::shared_ptr<WaypointInfo> getWaypointInfoByLabel(std::string label);
	int getWaypointIndexOf(std::shared_ptr<WaypointInfo> info);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject,int version);
};
