#pragma once
#include "FastUIPanel.h"

namespace NeutralBot {
	public ref class FormItemInfo{
	public:
		FormItemInfo(System::String^ _id, System::String^ _text, System::String^ _icon_name,
			System::Windows::Forms::Form^ _form);

		System::String^ id, ^text, ^icon_name;
		System::Windows::Forms::Form^ form_ref;
		bool is_resized = 0;
		System::Drawing::Size size_before_resize;
		bool is_in_panel = false;
	};

	public ref class FastUIPanelController{
		static FastUIPanel^ form_panel;

		System::Collections::Generic::LinkedList<FormItemInfo^> installed_forms;

		void init();

		FastUIPanelController();

		FormItemInfo^ get_form_info_by_form_ref(System::Windows::Forms::Form^ form);

		bool is_form_in_list(System::Windows::Forms::Form^ form);

		bool is_form_id_in_list(System::String^ id);

		void remove_form_from_list(System::Windows::Forms::Form^ form, bool close);

		FormItemInfo^ get_form_info_by_form_id(String^ id);

	public:
		bool top_most = true;

		static FastUIPanelController^ mFastUIPanelController;
		static FastUIPanelController^ get();

		FastUIPanel^ get_fast_ui_form();

		void on_form_windows_state_change(System::Windows::Forms::Form^ form);

		bool add_ui_button(System::String^ button_id,
			System::Windows::Forms::Form^ form, System::String^ text, System::String^ icon_name);

		void on_form_close(System::Windows::Forms::Form^ form);

		void on_form_minimize(System::Windows::Forms::Form^ form);

		void on_form_out_focus(System::Windows::Forms::Form^ form);

		void on_form_maximize(System::Windows::Forms::Form^ form);

		void show_form_by_id(String^ id);

		void install_controller(System::Windows::Forms::Form^ form);

		bool is_istalled_hook_form(System::Windows::Forms::Form^ form);

		void unistall_hook_form(System::Windows::Forms::Form^ form);

		System::Void event_Form_Shown(System::Object^  sender, System::EventArgs^  e);

		System::Void event_Form_Resize(System::Object^  sender, System::EventArgs^  e);

		System::Void event_Form_Deactivate(System::Object^  sender, System::EventArgs^  e);

		System::Void event_Form_Activated(System::Object^  sender, System::EventArgs^  e);

		System::Void event_Form_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e);

		void SizeDecreaseForm(System::Windows::Forms::Form^ form);

		void SizeIncreaseForm(System::Windows::Forms::Form^ form);

		void on_resize_option_change(bool state);

		void on_top_most_option_change(bool state);

		void on_panel_shortcut_option_change(bool state);
	};
}








