#pragma once
#include "WaypointPath.h"
#include "LanguageManager.h"
#include <json\json.h>

#pragma pack(push,1)
struct actionInfo {

	std::string id;
	std::string type; 
	std::string text;
	std::map<std::string,std::string> options;
public:
	GET_SET(std::string, id); 
	GET_SET(std::string, type);
	GET_SET(std::string, text);
	std::map<std::string, std::string> get_options(){
		return options; 
	}
	void add_option(std::string key, std::string option_str){
		options[key] = (option_str);
	}
};

class WaypointManager {

	std::map<int, std::shared_ptr<WaypointPath>> waypointPathList;
	std::map<int, std::map<int, std::shared_ptr<actionInfo>>> actionInfoList;

	neutral_mutex mtx_access;

	int lastWaypointInfoId;
	int lastWaypointPathId;
	int currentWaypointPathId;
	int currentWaypointInfoId;
	bool enabled;
	bool require_lure = false;
	uint32_t current_lure_recursive_index = 0;

public:
	WaypointManager();

	static WaypointManager* get();

	Coordinate get_lure_coord();

	bool get_lua_stay_after_lure();
	bool get_lure_advance_reached();
	bool get_lure_recursive();
	uint32_t get_lure_increase_sqm_lure();
	
	void increase_lure_recursive_index();

	void advanceCurrentWaypointInfoIgnoreLures();

	void advanceCurrentWaypointPath();

	std::vector<Coordinate> get_all_coordinate_lure(){
		std::vector<Coordinate> retval;
		for (auto path : waypointPathList){
			for (auto waypoint : path.second->getwaypointInfoList()){
				if (waypoint->get_action() == waypoint_lure_here_distance || waypoint->get_action() == waypoint_lure_here_knight_mode)
					retval.push_back(waypoint->get_destination_coord());
			}
		}
		return retval;
	}
	
	int getcurrentWaypointInfoId();

	void setcurrentWaypointInfoId(int id);

	bool is_require_lure();

	GET_SET(bool, enabled);
	GET_SET(int, lastWaypointInfoId);
	GET_SET(int, lastWaypointPathId);
	GET_SET(int, currentWaypointPathId);


	std::map<int, std::shared_ptr<WaypointPath>> getwaypointPathList();

	void initVariables();

	// WaypointManager
	std::shared_ptr<WaypointPath> getCurrentWaypointPath();
	void setCurrentWaypointPathByLabel(std::string label, std::string waypointLabel);
	void setCurrentWaypointPathByLabel(std::string label, int waypointInfoIndex);
	static waypoint_t getStringAsWaypoint(std::string alert_as_str);
	static std::string getWaypointAsString(waypoint_t type);

	void updateActionList(Json::Value jsonObject);
	std::map<int, std::shared_ptr<actionInfo>> getActionInfoList(int key);

	// WaypointPath
	int requestNewWaypointPathId();
	void setWaypointPath(int id, std::shared_ptr<WaypointPath> newWaypoint);
	std::shared_ptr<WaypointPath> getLastWaypointPath();
	std::shared_ptr<WaypointPath> getWaypointPathByLabel(std::string label);
	std::shared_ptr<WaypointPath> getWaypointPath(int id);
	void removeWaypointPath(int id);
	void removeWaypointPathByLabel(std::string label);
	void changeIndexWaypointPath(int pathId, int newPosition);

	// WaypointInfo
	void advanceCurrentWaypointInfo();
	std::shared_ptr<WaypointInfo> getCurrentWaypointInfo();
	std::shared_ptr<WaypointInfo> getLastWaypointInfo();
	std::shared_ptr<WaypointInfo> getBeforeWaypointInfo();
	std::shared_ptr<WaypointInfo> getBeforeLureWaypointInfo();
		
	std::shared_ptr<WaypointInfo> getNextWapointInfo();
	void setCurrentWaypointInfo(std::string& label);
	void setCurrentWaypointInfo(int index);

	Json::Value parse_class_to_json();

	Json::Value parse_class_to_json_navi(){
		Json::Value waypointManagerClass;
		
		std::shared_ptr<WaypointInfo> current_waypointer = getCurrentWaypointInfo();
		std::shared_ptr<WaypointPath> current_waypointer_path = getCurrentWaypointPath();

		waypointManagerClass["current_waypoint_coordinate_x"] = current_waypointer->get_position_x();
		waypointManagerClass["current_waypoint_coordinate_y"] = current_waypointer->get_position_y();
		waypointManagerClass["current_waypoint_coordinate_z"] = current_waypointer->get_position_z();
		waypointManagerClass["current_waypoint_label"] = current_waypointer->get_label();
		waypointManagerClass["current_waypoint_path"] = current_waypointer_path->get_label();
		waypointManagerClass["current_waypoint_action"] = current_waypointer->get_action();

		return waypointManagerClass;
	}

	void parse_json_to_class(Json::Value jsonObject, int version);

	std::shared_ptr<WaypointPath> get_waypoint_path_by_raw_pointer(WaypointPath* pointer);

	bool is_location(uint32_t dist_near);
	
	bool is_location(uint32_t x, uint32_t y, int8_t z, uint32_t dist_near);

	void clear();
};


#pragma pack(pop)
