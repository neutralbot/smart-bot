#pragma once
#include <string>
#include <stdint.h>
#include <map>


#include <windows.h>
//macro GET_TRANSLATION(char* key)
#define GET_TR(key) LanguageManager::get()->get_translation(key)
#define GET_REVERSE_TR(key) LanguageManager::get()->get_reverse_translation(key)
#define GET_MANAGED_TR(key) gcnew String(&LanguageManager::get()->get_translation(key)[0])
/*
Get translation of your variable name
*/
#define GET_TR_VAR(key_var) GET_TR(#key_var)


class LanguageManager{
	
	std::string current_lang;
	std::map<std::string /*key*/, std::string /*value*/> dictionary;
	/*
		function that load xml translation files, private, used by set_current_lang if 
		current lang differs from loaded
	*/
	bool load(std::string lang);
	LanguageManager();
public:
	/*
		set and load new lang
		usage:
		set_current_lang("pt")
		will set pt as new language and internal will call load() once it will load pt.xml content to
		current dictionary.
	*/
	void set_current_lang(std::string lang);

	/*
		get current lang ex:
		std::string lang = get_current_lang();
		now lang content is "pt" if the current idiom is pt.
	*/
	std::string get_current_lang();

	std::string get_translation(std::string key);

	std::string get_reverse_translation(std::string value);

	static LanguageManager* get();
};
