#pragma once
#include "Core\InputManager.h"
#include "Core\Util.h"
#include "Core\Time.h"
#include "Core\LuaCore.h"

#define MAX_HOTKEYS_IN_QUEUE 10
#define MAX_HOTKEY_TIMEOUT 1000

enum hotkey_value_t{
	hotkey_value_unpressed,
	hotkey_value_pressed,
	hotkey_value_any,
	hotkey_value_toggled,
	hotkey_value_untoggled
};
#pragma pack(push,1)


class Hotkey{
public:
	std::vector <std::string> events;
	Hotkey();
	Hotkey(uint32_t _id, uint32_t _button_code, bool _keyboard);
	uint32_t id;

	std::string name;
	bool state;
	
	hotkey_value_t ctrl, shift, alt, numlock, scrolllock, capslock, insert;
	uint32_t button_code;
	bool keyboard;
	
	std::string button_as_string;
	bool match(InputBaseEvent* input_event);

	void assign(InputBaseEvent* input_event);

	Json::Value ClassToJson();

	void JsonToClass(Json::Value& json_in);

};

struct HotkeyScriptEvent{
	std::shared_ptr<Hotkey> owner = nullptr;
	TimeChronometer start_time;
	
	HotkeyScriptEvent(std::shared_ptr<Hotkey> _owner = nullptr);
	bool has_expired();
};


class HotkeysManager{
	std::shared_ptr<LuaCore> mLuaCore;
	neutral_mutex event_mtx;
	bool has_event = false;
	uint32_t mListenerId;
	bool edit_script = false;
	
	std::vector<std::shared_ptr<Hotkey>> hotkeys;
	std::vector<std::shared_ptr<HotkeyScriptEvent>> event_queue;

	bool exist_id(uint32_t id);

	std::shared_ptr<Hotkey> find_hotkey_by_id(uint32_t id);
	
	void process_inputs(InputBaseEvent* input_event);

	void process_events();

	void remove_expired_events();

	void add_event(std::shared_ptr<Hotkey>&);

	bool has_recent_event(std::shared_ptr<Hotkey>& _event_hk);

	std::shared_ptr<HotkeyScriptEvent> get_event_from_queue();

	Json::Value ClassToJson();

	bool JsonToClass(Json::Value& json_in);

public:

	void set_edit_script(bool in);

	uint32_t generate_new_id();

	bool Load(std::string path = DEFAULT_HOTKEY_FILE_PATH);

	bool Save(std::string path = DEFAULT_HOTKEY_FILE_PATH);

	HotkeysManager();

	std::shared_ptr<Hotkey> new_hotkey();

	std::shared_ptr<Hotkey> get_hotkey_by_id(uint32_t id);

	void remove_hotkey(uint32_t id);

	void reset_default();

	void init();

	std::vector<std::shared_ptr<Hotkey>> get_hotkeys();

	static HotkeysManager* get();
};
#pragma pack(pop)


