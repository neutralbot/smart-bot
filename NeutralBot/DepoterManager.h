#pragma once
#include <string>
#include <map>
#include "xml.h"
#include <memory>
#include "Core\Util.h"
#include <json\json.h>
#include "Core\ContainerManager.h"


#pragma pack(push,1)
struct DepotItem {

	int item_id;	
	uint32_t maxqty = 0;
	uint32_t minqty = 0;

public:
	std::string itemName;
	int get_item_id();
	std::string get_itemName();
	void set_itemName(std::string itemName);
	DEFAULT_GET_SET(uint32_t, maxqty);
	DEFAULT_GET_SET(uint32_t, minqty);
};

class DepotContainer {
	int Containeritem_id;
	std::string ContainerName;
	std::map<std::string, std::shared_ptr<DepotItem>> mapDepotItem;

public:
	std::string requestNewitem_id();
	std::string addDepotItem(std::string item, uint32_t maxqty, uint32_t minqty);
	void removeDepotItem(std::string id);

	int get_item_id();

	void set_Containeritem_id(uint32_t Container){
		Containeritem_id = Container;
		ContainerName = ItemsManager::get()->getItemNameFromId(Containeritem_id); //int searchitem_id = ItemsManager::get()->getitem_idFromName(ContainerName);
	}
	uint32_t get_Containeritem_id(){
		return Containeritem_id;
	}
	uint32_t get_depot_box_index(){
		uint32_t result = Containeritem_id  - 22797;
		return result;
	}

	bool contains_item_id(uint32_t item_id){
		for (auto item : mapDepotItem)
			if (item.second->get_item_id() == item_id)
				return true;
		return false;
	}

	void set_ContainerName(std::string ContainerName);
	std::string get_ContainerName();
	std::map<std::string, std::shared_ptr<DepotItem>> getMapDepoItens();
	std::shared_ptr<DepotItem> getDepotItemById(std::string id);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);
};

class DepoterManager {
	bool use_depot_old_style;
	int Containeritem_id;
	std::string ContainerName;
	std::string DepotBoxName;
	int DepotBoxId;
	static std::shared_ptr<DepoterManager> mDepoterManager;
	std::map<std::string, std::shared_ptr<DepotContainer>> mapDepotContainer;

	DepoterManager();
public:
	static void reset();
	void clear();
	
	void set_use_old_style(bool in){
		use_depot_old_style = in;
	}

	bool get_use_old_style(){
		return use_depot_old_style;
	}

	int get_item_id();
	int get_DepotBoxId(){
		return DepotBoxId;
	}
	std::string get_DepotBoxName(){
		return DepotBoxName;
	}
	std::string get_ContainerName();
	void set_ContainerName(std::string itemName);

	std::string requestNewDepotContainer();
	std::shared_ptr<DepotContainer> getDepotContainer(std::string id_);
	void removeDepotContainer(std::string id);
	std::map<std::string, std::shared_ptr<DepotContainer>> getMapDepotContainer();

	void set_DepotBox(std::string itemName,int index);
	bool changeDepoId(std::string newId, std::string oldId);
	bool checkNecessaryDepot();
	bool checkNecessaryDepot(std::string DepotContainer);

	bool contains_item_id(uint32_t item_id){
		for (auto container : mapDepotContainer)
			if (container.second->contains_item_id(item_id))
				return true;
		return false;
	}

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);

	static std::shared_ptr<DepoterManager> get(){
		if (!mDepoterManager)
			reset();
		return mDepoterManager;
	}
};
#pragma pack(pop)