#pragma once
#include "Alerts.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class ConfigSound : public Telerik::WinControls::UI::RadForm{
	public: ConfigSound(void){
				InitializeComponent();
	}
	protected: ~ConfigSound(){
				   unique = nullptr;
				   if (components)
					   delete components;
	}

	protected: static ConfigSound^ unique;
	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew ConfigSound();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew ConfigSound();
	}

	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	protected:
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListAlerts;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListSoundName;
	private: Telerik::WinControls::UI::RadButton^  ButtonLoadSound;
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ConfigSound::typeid));
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->DropDownListAlerts = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->DropDownListSoundName = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->ButtonLoadSound = (gcnew Telerik::WinControls::UI::RadButton());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListAlerts))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListSoundName))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonLoadSound))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(12, 38);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(71, 18);
				 this->radLabel2->TabIndex = 10;
				 this->radLabel2->Text = L"Sound Name";
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(12, 12);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(50, 18);
				 this->radLabel1->TabIndex = 9;
				 this->radLabel1->Text = L"Alert List";
				 // 
				 // DropDownListAlerts
				 // 
				 this->DropDownListAlerts->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->DropDownListAlerts->Location = System::Drawing::Point(68, 12);
				 this->DropDownListAlerts->Name = L"DropDownListAlerts";
				 this->DropDownListAlerts->Size = System::Drawing::Size(158, 20);
				 this->DropDownListAlerts->TabIndex = 8;
				 this->DropDownListAlerts->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &ConfigSound::DropDownListAlerts_SelectedIndexChanged);
				 // 
				 // DropDownListSoundName
				 // 
				 this->DropDownListSoundName->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->DropDownListSoundName->Location = System::Drawing::Point(89, 38);
				 this->DropDownListSoundName->Name = L"DropDownListSoundName";
				 this->DropDownListSoundName->Size = System::Drawing::Size(137, 20);
				 this->DropDownListSoundName->TabIndex = 7;
				 this->DropDownListSoundName->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &ConfigSound::DropDownListSoundName_SelectedIndexChanged);
				 // 
				 // ButtonLoadSound
				 // 
				 this->ButtonLoadSound->Location = System::Drawing::Point(12, 64);
				 this->ButtonLoadSound->Name = L"ButtonLoadSound";
				 this->ButtonLoadSound->Size = System::Drawing::Size(214, 24);
				 this->ButtonLoadSound->TabIndex = 6;
				 this->ButtonLoadSound->Text = L"Load Sound";
				 this->ButtonLoadSound->Click += gcnew System::EventHandler(this, &ConfigSound::ButtonChangeSound_Click);
				 // 
				 // ConfigSound
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(237, 99);
				 this->Controls->Add(this->radLabel2);
				 this->Controls->Add(this->radLabel1);
				 this->Controls->Add(this->DropDownListAlerts);
				 this->Controls->Add(this->DropDownListSoundName);
				 this->Controls->Add(this->ButtonLoadSound);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->Name = L"ConfigSound";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->ShowIcon = false;
				 this->Text = L"ConfigSound";
				 this->Load += gcnew System::EventHandler(this, &ConfigSound::ConfigSound_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListAlerts))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListSoundName))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonLoadSound))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion

			 bool disable_update = false;

			 void SoundChange(System::Windows::Forms::OpenFileDialog^ newSound){
				 String^ fileName = System::IO::Path::GetFileName(newSound->FileName);
				 String^ sourcePath = newSound->FileName;
				 String^ targetPath = gcnew String(&DEFAULT_DIR_SETTINGS_PATH[0]);

				 String^ sourceFile = System::IO::Path::Combine(sourcePath);
				 String^ destFile = System::IO::Path::Combine(targetPath, fileName);

				 if (!System::IO::Directory::Exists(targetPath))
					 System::IO::Directory::CreateDirectory(targetPath);

				 System::IO::File::Copy(sourceFile, destFile, true);

				 for each(auto item in DropDownListSoundName->Items){
					 if (item->Text->Trim() != fileName->Trim())
						 continue;

					 return;
				 }

				 GeneralManager::get()->set_sound_filename((alert_t)DropDownListAlerts->SelectedIndex, managed_util::fromSS(fileName));
				 GeneralManager::get()->vector_all_sound.push_back(managed_util::fromSS(fileName));

				 Telerik::WinControls::UI::RadListDataItem^ item2 = gcnew Telerik::WinControls::UI::RadListDataItem();
				 item2->Text = fileName;

				 disable_update = true;
				 DropDownListSoundName->Items->Add(item2);
				 DropDownListSoundName->SelectedItem = item2;
				 disable_update = false;
			 }

	private: System::Void DropDownListAlerts_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;

				 DropDownListSoundName->Text = gcnew String(GeneralManager::get()->get_sound_filename((alert_t)DropDownListAlerts->SelectedIndex).c_str());
	}

	private: System::Void DropDownListSoundName_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update)
					 return;

				 GeneralManager::get()->set_sound_filename((alert_t)DropDownListAlerts->SelectedIndex, managed_util::fromSS(DropDownListSoundName->Text));
	}

	private: System::Void ButtonChangeSound_Click(System::Object^  sender, System::EventArgs^  e) {
				 System::Windows::Forms::OpenFileDialog^ fileDialog = gcnew  System::Windows::Forms::OpenFileDialog();
				 fileDialog->Filter = "Sound Files|*.wav";
				 fileDialog->Title = "Open Script";

				 if (fileDialog->ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 SoundChange(fileDialog);
	}

	private: System::Void DropDownListSoundName_PopupOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
				 if (DropDownListAlerts->Text->Trim() == "")
					 e->Cancel = true;
	}

	private: System::Void ConfigSound_Load(System::Object^  sender, System::EventArgs^  e) {
				 for (int i = 0; i < alert_t::alert_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = gcnew String(&AlertsManager::getAlertAsString((alert_t)i)[0]);
					 item->Value = i;
					 DropDownListAlerts->Items->Add(item);
				 }

				 update_idiom_listbox();
				 update_idiom();

				 std::map<alert_t, std::string/*filename*/> myMap = GeneralManager::get()->get_map_sound();
				 std::vector<std::string> myVector = GeneralManager::get()->vector_all_sound;
				 for (auto it : myVector){
					 if (string_util::trim(it) == "")
						 continue;

					 bool continue_loop = false;

					 for each(auto item in DropDownListAlerts->Items){
						 String^ fileName = gcnew String(&it[0]);
						 if (item->Text->Trim() == fileName->Trim())
							 continue_loop = true;
					 }

					 if (continue_loop)
						 continue;

					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 String^ fileName = System::IO::Path::GetFileName(gcnew String(&it[0]));
					 item->Text = fileName;

					 DropDownListSoundName->Items->Add(item);
				 }
	}

			 void update_idiom_listbox(){
				 DropDownListAlerts->BeginUpdate();
				 DropDownListAlerts->Items->Clear();
				 for (int i = 0; i < alert_t::alert_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = gcnew String(&AlertsManager::getAlertAsString((alert_t)i)[0]);
					 item->Value = i;
					 DropDownListAlerts->Items->Add(item);
				 }
				 DropDownListAlerts->EndUpdate();				
			 }

			 void update_idiom(){
				 this->Text = gcnew String(&GET_TR(managed_util::fromSS(this->Text))[0]);
				 ButtonLoadSound->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonLoadSound->Text))[0]);
				 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
				 radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
			 }
	};
}
