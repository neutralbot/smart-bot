#include "SpellManager.h"

static std::vector<std::vector<int>> AREA_NONE = {
	{ 0, 0, 0 },
	{ 0, 0, 0 },
	{ 0, 0, 0 }
};

/*
stone shower rune
3175
avalance rune
3161
thunderstorm rune
3202
great fireball rune
3191
*/
static std::vector<std::vector<int>> AREA_CIRCLE3X3 = {
	{ 0, 0, 1, 1, 1, 0, 0 },
	{ 0, 1, 1, 1, 1, 1, 0 },
	{ 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 3, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 1 },
	{ 0, 1, 1, 1, 1, 1, 0 },
	{ 0, 0, 1, 1, 1, 0, 0 }
};

/*
	explosion runne
	3200
*/
static std::vector<std::vector<int>> AREA_CROSS2X2 = {
		{ 0, 1, 0 },
		{ 1, 1, 1 },
		{ 0, 1, 0 }
};
	
/*
energy bomb rune
3149
posion bomb rune
3191
firebomb
3192
light stone shower runes
21351
*/
static std::vector<std::vector<int>> AREA_RECT2X2 = {
		{ 1, 1, 1 },
		{ 1, 1, 1 },
		{ 1, 1, 1 }
};



/*
stalagmite rune
*/

SpellManager* SpellManager::get(){
	static SpellManager* mSpellManager = nullptr;
	if (!mSpellManager)
		mSpellManager = new SpellManager();
	return mSpellManager;
}

bool SpellManager::get_health_spells_state(){
	return health_spells_state;
}

bool SpellManager::get_attack_spells_state(){
	return attack_spells_state;
}

void SpellManager::set_health_spells_state(bool state){
	health_spells_state = state;
}

void SpellManager::set_attack_spells_state(bool state){
	attack_spells_state = state;
}

void SpellManager::parse_json_to_classImport(Json::Value jsonObject){

	Json::Value spellClasse = jsonObject;
	Json::Value health_spell_list = jsonObject["health_spell_list"];
	Json::Value attack_spell_list = jsonObject["attack_spell_list"];

	Json::Value::Members members = health_spell_list.getMemberNames();

	for (auto member : members){
		std::shared_ptr<SpellHealth> spell(new SpellHealth);
		spell->parse_json_to_class(health_spell_list[member]);
		healthSpells[requestNewHealthSpells()] = spell;
	}
	members = attack_spell_list.getMemberNames();

	for (auto member : members){
		Json::Value& spellInfo = attack_spell_list[member];
		std::shared_ptr<SpellAttack> spell(new SpellAttack);
		spell->parse_json_to_class(spellInfo);
		attackSpells[requestNewAttackSpells()] = spell;
	}
}

int SpellManager::requestNewHealthSpells(){
	int id = 0;
	while (healthSpells.find(id) != healthSpells.end())
		id++;

	healthSpells[id] = std::shared_ptr<SpellHealth>(new SpellHealth);
	return id;
}
int SpellManager::requestNewAttackSpells(){
	int id = 0;
	while (attackSpells.find(id) != attackSpells.end())
		id++;

	attackSpells[id] = std::shared_ptr<SpellAttack>(new SpellAttack);
	return id;
}



std::vector<std::pair<std::string, spell_type_t>> SpellManager::missing_hotkeys(){
	std::vector<std::pair<std::string, spell_type_t>> retval;
	for (auto atackSpell : attackSpells){
		std::string spell_name = atackSpell.second->get_visual_cast_value();
		uint32_t item_id = atackSpell.second->get_item_id();

		if (item_id > 0){
			switch (atackSpell.second->get_spell_type()){
			case spell_type_t::spell_type_rune_on_target:{
				if (HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_on_target, false) != hotkey_none)
					continue;

				retval.push_back(std::make_pair(spell_name, spell_type_t::spell_type_on_target));
				continue;
			}
				break;
			case spell_type_t::spell_type_rune_area:{
				if (HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_with_crosshairs, false) != hotkey_none)
					continue;

				retval.push_back(std::make_pair(spell_name, spell_type_t::spell_type_rune_area));
				continue;
			}
				break;
			}
		}

		if (HotkeyManager::get()->findHotkeyByText(spell_name, true) != hotkey_none)
			continue;

		retval.push_back(std::make_pair(spell_name, spell_type_t::spell_type_on_target));
	}

	for (auto healthSpell : healthSpells){
		std::string spell_name = healthSpell.second->get_visual_cast_value();
		uint32_t item_id = healthSpell.second->get_item_id();

		if (item_id > 0){
			if (HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_on_self, false) != hotkey_none)
				continue;

			retval.push_back(std::make_pair(spell_name, spell_type_t::spell_type_healing));
			continue;
		}
		if (HotkeyManager::get()->findHotkeyByText(spell_name, true) != hotkey_none)
			continue;

		retval.push_back(std::make_pair(spell_name, spell_type_t::spell_type_healing));
	}
	return retval;
}

SpellManager::SpellManager() {
	spellAreaList[3175] = area_type_t::area_type_circle_3x3;
	spellAreaList[3161] = area_type_t::area_type_circle_3x3;
	spellAreaList[3202] = area_type_t::area_type_circle_3x3;
	spellAreaList[3191] = area_type_t::area_type_circle_3x3;

	spellAreaList[3200] = area_type_t::area_type_cross_2x2;

	spellAreaList[3149] = area_type_t::area_type_rect_2x2;
	spellAreaList[3191] = area_type_t::area_type_rect_2x2;
	spellAreaList[3192] = area_type_t::area_type_rect_2x2;
	spellAreaList[21351] = area_type_t::area_type_rect_2x2;
	attack_spells_state = false;
	health_spells_state = false;
}

std::vector<std::vector<int>> SpellManager::get_spell_area_by_name(area_type_t type) {
	switch (type){
		case area_type_t::area_type_circle_3x3:
			return AREA_CIRCLE3X3;
		case area_type_t::area_type_cross_2x2:
			return AREA_CROSS2X2;
		case area_type_t::area_type_rect_2x2:
			return AREA_RECT2X2;
	}
	return AREA_NONE;
}

area_type_t SpellManager::getSpellAreaEffectByitem_id(int item_id) {
	auto find = spellAreaList.find(item_id);

	if (find != spellAreaList.end())
		return find->second;

	return area_type_t::area_type_none;
}

Json::Value SpellManager::parse_class_to_json() {
	Json::Value spellClasse;
	Json::Value health_spell_list;
	Json::Value attack_spell_list;

	for (auto it : healthSpells)
		health_spell_list[std::to_string(it.first)] = healthSpells[it.first]->parse_class_to_json();	

	spellClasse["health_spell_list"] = health_spell_list;
	
	for (auto it : attackSpells)
		attack_spell_list[std::to_string(it.first)] = attackSpells[it.first]->parse_class_to_json();
	
	spellClasse["attack_spell_list"] = attack_spell_list;

	spellClasse["healing_state"] = health_spells_state;
	spellClasse["attack_state"] = attack_spells_state;

	return spellClasse;
}

void SpellManager::parse_json_to_class(Json::Value jsonObject) {
	clear_all();
	if (jsonObject["healing_state"].isNull() || jsonObject["healing_state"].empty())
		health_spells_state = true;
	else
		health_spells_state = jsonObject["healing_state"].asBool();
		
	
	if (jsonObject["attack_state"].isNull() || jsonObject["attack_state"].empty())
		attack_spells_state = true;
	else
		attack_spells_state = jsonObject["healing_state"].asBool();

	
	if (!jsonObject["healthSpellList"].empty() || !jsonObject["healthSpellList"].isNull()){
		Json::Value health_spell_list = jsonObject["healthSpellList"];

		Json::Value::Members members = health_spell_list.getMemberNames();
		for (auto member : members){
			std::shared_ptr<SpellHealth> spell(new SpellHealth);
			spell->parse_json_to_class(health_spell_list[member]);
			healthSpells[atoi(&member[0])] = spell;
		}
	}
	else if (!jsonObject["health_spell_list"].empty() || !jsonObject["health_spell_list"].isNull()){
		Json::Value health_spell_list = jsonObject["health_spell_list"];

		Json::Value::Members members = health_spell_list.getMemberNames();
		for (auto member : members){
			std::shared_ptr<SpellHealth> spell(new SpellHealth);
			spell->parse_json_to_class(health_spell_list[member]);
			healthSpells[atoi(&member[0])] = spell;
		}
	}

	if (!jsonObject["attackSpellList"].empty() || !jsonObject["attackSpellList"].isNull()){
		Json::Value attack_spell_list = jsonObject["attackSpellList"];

		Json::Value::Members members2 = attack_spell_list.getMemberNames();
		for (auto member : members2){
			Json::Value& spellInfo = attack_spell_list[member];
			std::shared_ptr<SpellAttack> spell(new SpellAttack);
			spell->parse_json_to_class(spellInfo);
			attackSpells[atoi(&member[0])] = spell;
		}
	}
	else if (!jsonObject["attack_spell_list"].empty() || !jsonObject["attack_spell_list"].isNull()){
		Json::Value attack_spell_list = jsonObject["attack_spell_list"];

		Json::Value::Members members2 = attack_spell_list.getMemberNames();
		for (auto member : members2){
			Json::Value& spellInfo = attack_spell_list[member];
			std::shared_ptr<SpellAttack> spell(new SpellAttack);
			spell->parse_json_to_class(spellInfo);
			attackSpells[atoi(&member[0])] = spell;
		}
	}

	
}

int SpellManager::addNewHealthSpell()
{
	int id = 0;

	while (healthSpells.find(id) != healthSpells.end())
	{
		id++;
	}

	healthSpells[id] = std::shared_ptr<SpellHealth>(new SpellHealth(false,0, 0, 0, 0, 0, 0, "", 0));
	return id;
}

void SpellManager::removeHealthSpellInList(int id)
{
	if (healthSpells.find(id) != healthSpells.end()) {
		healthSpells.erase(id);
	}
}

std::shared_ptr<SpellHealth> SpellManager::getHealthSpellInListById(int id)
{
	if (healthSpells.find(id) == healthSpells.end())
		return nullptr;

	return healthSpells[id];
}

int SpellManager::addAttackSpellInList()
{
	int id = 0;

	while (attackSpells.find(id) != attackSpells.end())
	{
		id++;
	}

	attackSpells[id] = std::shared_ptr<SpellAttack>(new SpellAttack(false,spell_type_t::spell_type_healing, 0, 0, "", 0, 0, 0, 0, 0, 0, 0, 0));
	return id;
}

void SpellManager::removeAttackSpellInList(int id)
{
	auto it = attackSpells.find(id);
	if (it != attackSpells.end()) {
		attackSpells.erase(it);
	}
}

std::shared_ptr<SpellAttack> SpellManager::getAttackSpellInListById(uint32_t id){
	if (id > attackSpells.size())
		return nullptr;

	return attackSpells[id];
}

void SpellManager::clear_all(){
	healthSpells.clear();
	attackSpells.clear();
}

std::map<int, std::shared_ptr<SpellHealth>>& SpellManager::getHealthList() {
	return healthSpells;
}

std::map<int, std::shared_ptr<SpellAttack>>& SpellManager::getAttackList() {
	return attackSpells;
}

SpellBase* SpellManager::get_spell_by_id(std::string id){
	for (auto spell : healthSpells){
		if (spell.second->get_visual_name() == id)
			return (SpellBase*)spell.second.get();
	}
	return nullptr;
}

bool SpellManager::get_spell_state_by_id(std::string id){
	SpellBase* spell = get_spell_by_id(id);
	if (!spell)
		return false;
	return spell->get_state();
}

void SpellManager::disable_spell_by_id(std::string id){
	SpellBase* _spell = get_spell_by_id(id);
	if (!_spell)
		return;
	_spell->set_state(false);
}

void SpellManager::enable_spell_by_id(std::string id){
	SpellBase* _spell = get_spell_by_id(id);
	if (!_spell)
		return;
	_spell->set_state(true);
}


