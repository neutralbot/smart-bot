#pragma once
#include <string>
#include "Core\Util.h"
#include "Core\constants.h"



class XMLSpellInfo{
	std::string name;
	std::string cast;
	int mana;
	int level;
	int magicLevel;
	bool premium;
	XMLCategory category;
	XMLspell_type type;
	int soul;
	int range;
	int min_hp;
	int max_hp;
	int cooldownCategory;
	int cooldownSpell;
	int cooldownId;

public:
	XMLSpellInfo();
	XMLSpellInfo(std::string name, std::string cast, int min_hp, int max_hp, int mana, int level, int magicLevel, bool premium, int category, int type, int soul, int range, int cooldownCategory, int cooldownSpell, int cooldownId);
	
	GET_SET(std::string, name);
	GET_SET(std::string, cast);
	GET_SET(int, mana);
	GET_SET(int, level);
	GET_SET(int, min_hp);
	GET_SET(int, max_hp);
	GET_SET(int, magicLevel);
	GET_SET(bool, premium);
	GET_SET(int, soul);
	GET_SET(int, range);
	GET_SET(int, cooldownCategory);
	GET_SET(int, cooldownSpell);
	GET_SET(int, cooldownId);

	void set_category(int num);
	int get_category();

	void set_type(int num);
	int get_type();
};
