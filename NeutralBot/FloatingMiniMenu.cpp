#include "Core\FloatingMiniMenu.h"
#include "Core\Input.h"
#include "Core\Time.h"
#include "Core\Interface.h"
#include "Time.h"

bool raw_floating_menu::is_open(){
		int address_window = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_WINDOW));
		if (!address_window || address_window != current_address)
			return false;
		return true;
	}

std::vector<std::shared_ptr<raw_floating_menu_option>> raw_floating_menu::get_options(){
	std::vector<std::shared_ptr<raw_floating_menu_option>> retval;
	uint32_t address = first_option_address;
	int count = 0;
	while (address && count < 100){
		std::shared_ptr<raw_floating_menu_option> option(new raw_floating_menu_option);
		if (TibiaProcess::get_default()->read_memory_block(address, option.get(), sizeof(raw_floating_menu_option), false)
			== sizeof(raw_floating_menu_option))
			retval.push_back(option);
		else
			break;
		count++;
		address = option->next_address;
	}
	return retval;
}

std::shared_ptr<raw_floating_menu_option> FloatingMiniMenu::get_option(std::string option_caption){
	auto options = raw.get_options();
	for (auto option : options)
		if (std::string(&option->caption[0]) == option_caption)
			return option;
	return nullptr;
}

bool FloatingMiniMenu::click_at_option(std::string option_caption){
	Rect option_real_rect;
	std::shared_ptr<raw_floating_menu_option> option = get_option(option_caption);
	if (!option)
		return false;
	option_real_rect = option->get_rect() + Point( raw.x, raw.y);

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	Coordinate dimension = Interface->get_client_dimension();
	Point center = option_real_rect.get_center();

	//x ta pra direita da tela noob
	if (center.x >= dimension.x){
		int diff = dimension.x - raw.x;

		if (diff)
			diff = diff / 2;		

		center.x = raw.x + diff;
	}

	input->click_left(center);
	return true;
}

std::shared_ptr<FloatingMiniMenu> FloatingMiniMenu::open_at_coordinate(Coordinate& coord, int32_t timeout){
	TimeChronometer timer;
	int address_window = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_WINDOW));
	if (!Interface->close_dialog(timeout))
		return nullptr;

	auto input = HighLevelVirtualInput::get_default();
	do{
		if (!input->can_use_input())
			continue;

		input->click_tibia_coordinate_right(coord, Coordinate(), true);
		TimeChronometer loop_timer;
		while (loop_timer.elapsed_milliseconds() < 100){
			uint32_t address = Interface->get_dialog_address();
			if (address){
				FloatingMiniMenu* ret_ptr = new FloatingMiniMenu;
				if (TibiaProcess::get_default()->read_memory_block(address, ret_ptr, sizeof(FloatingMiniMenu), false)
					!= sizeof(FloatingMiniMenu)){
					delete ret_ptr;
					return nullptr;
				}
				else
					return std::shared_ptr<FloatingMiniMenu>(ret_ptr);
			}
			Sleep(10);
		}
	} while (address_window && timer.elapsed_milliseconds() < timeout);

	return nullptr;
}

bool FloatingMiniMenu::open_option_at_coordinate(Coordinate& coord, std::string option_caption, int32_t timeout){
	TimeChronometer timer;
	auto floating_menu = open_at_coordinate(coord, timeout);
	if (!floating_menu)
		return false;
	while (timer.elapsed_milliseconds() < timeout){
		floating_menu->click_at_option(option_caption);
		return true;
	}
	return false;
}



