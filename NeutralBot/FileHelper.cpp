#include "Core\FileHelper.h"

FileReader::FileReader(std::string path) :file(path){
	file_path = path;
	carret_pos = 0;
	if (is_good()){
		FILE *f = fopen(&path[0], "rb");
		fseek(f, 0, SEEK_END);
		long fsize = ftell(f);
		fseek(f, 0, SEEK_SET);

		data = new char[fsize];
		fread(data, fsize, 1, f);
		fclose(f);
		length = fsize;
	}
	else
		length = 0;
}

FileReader::~FileReader(){
	file.close();
}

void FileReader::read_bytes(void* ptr_out, uint32_t len){
	if (get_bytes_left() < len)
		throw new std::string("trying to read size greater than left in file in " +
		std::string(__FILE__) + ":" + std::to_string((long long)(__LINE__)));
	else
		memcpy(ptr_out,&data[carret_pos], len);
		advance_carret(len);
}

bool FileReader::is_good(){
	return file.good();
}

bool FileReader::is_end(){
	return file.eof();
}

uint32_t FileReader::get_bytes_left(){
	return length - carret_pos;
}

std::string FileReader::getString(){
	std::string str;
	uint16_t len = get<uint16_t>();
	if (len > 0 && len < 8192 && get_bytes_left() >= len) {
		char buffer[8192];
		read_bytes(buffer, len);
		buffer[len] = 0;
		str = buffer;
	}
	else if (len != 0)
		throw new std::exception("read failed because string is too big");
	return str;
}


