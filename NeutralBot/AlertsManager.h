#pragma once
#include "Core\Util.h"
#include "Core\Time.h"
#include "GeneralManager.h"
#include <json\json.h>

#pragma pack(push,1)
struct AlertsStruct{

public:
	AlertsStruct(){
		type = alert_t::alert_none;
		sound = false;
		pause = false;
		logout = false;
		value = 0;
	}

	TimeChronometer timer;
	alert_t type = alert_t::alert_none;
	bool sound = false;
	bool pause = false;
	bool logout = false;
	int value = 0;
	bool need_execute_delay(){
		if (timer.elapsed_milliseconds() > 2000)
			return true;
		return false;
	}

	DEFAULT_GET_SET(alert_t, type);
	DEFAULT_GET_SET(bool, sound);
	DEFAULT_GET_SET(bool, pause);
	DEFAULT_GET_SET(bool, logout);
	DEFAULT_GET_SET(int, value);

	void parse_json_to_class(Json::Value& json);
	Json::Value parse_class_to_json();

	std::string to_string();
};
#pragma pack(pop)
#pragma pack(push,1)
class AlertsManager{
	AlertsManager();

	std::map<uint32_t, std::shared_ptr<AlertsStruct>> alertsmap;

public:
	std::map<uint32_t, std::shared_ptr<AlertsStruct>> GetMap();
	void updateitem(uint32_t id, std::string type, bool sound, bool pause, bool logout, int value);
	std::shared_ptr<AlertsStruct> GetRuleById(uint32_t id);
	uint32_t CreatNewMapId();
	bool creatNewMapById(uint32_t id);
	bool CheckMap(uint32_t id);
	void deleteMap(uint32_t id_);
	bool updateValue(uint32_t id, uint32_t value);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);

	void clear();
	void reset();

	static std::string getAlertAsString(alert_t type);
	static alert_t getStringAsAlert(std::string in);
	static std::shared_ptr<AlertsManager> mAlertsManager;

	static std::shared_ptr<AlertsManager> get(){
		if (!mAlertsManager)
			mAlertsManager = std::shared_ptr<AlertsManager>(new AlertsManager());
		return mAlertsManager;
	}
};
#pragma pack(pop)
