#include "LanguageManager.h"
#include <3PartLib\xml\xml.h>
bool LanguageManager::load(std::string lang){
	TiXmlDocument doc(&("language\\" + lang + ".xml")[0]);
	if (doc.LoadFile()){
		dictionary.clear();
		TiXmlElement* el = (TiXmlElement*)doc.FirstChild("dictionary");
		if (!el)
			return false;
		el = el->FirstChildElement("word");
		while (el){
			const char* att_key = el->Attribute("key");
			const char* att_value = el->Attribute("value");
			if (att_key && att_value)
				dictionary[att_key] = att_value;
			el = el->NextSiblingElement();
		}
		return true;
	}
	return false;
}

LanguageManager::LanguageManager(){
	set_current_lang("langbase");
}


void LanguageManager::set_current_lang(std::string lang){
	if (current_lang == lang)
		return;
	load(lang);
	current_lang = lang;
}

std::string LanguageManager::get_current_lang(){
	return current_lang;
}

std::string LanguageManager::get_translation(std::string key){
	auto it = dictionary.find(key);
	if (it == dictionary.end())
		return key;
	return it->second;
}

std::string LanguageManager::get_reverse_translation(std::string value){
	for (auto it : dictionary)
		if (it.second == value)
			return it.first;
	return value;
}

LanguageManager* LanguageManager::get(){
	static LanguageManager* mLanguageManager = nullptr;
	if (!mLanguageManager)
		mLanguageManager = new LanguageManager;
	return mLanguageManager;
}

