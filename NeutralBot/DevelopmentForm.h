#pragma once
#include "DevelopmentManager.h"
#include "MapWall.h"
#include "Core\ItemsManager.h"
#include "GifManager.h"
#include "Core\ContainerManager.h"
#include "ManagedUtil.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class DevelopmentForm : public Telerik::WinControls::UI::RadForm{
	public:
		DevelopmentForm(void){
			InitializeComponent();
			FastUIPanelController::get()->install_controller(this);
			creatMemory();
		}

	protected: ~DevelopmentForm(){
			if (components)
				delete components;
		}
	protected: Telerik::WinControls::UI::RadPageView^  Page;


	protected:
	 Telerik::WinControls::UI::RadPageViewPage^  pageTime;
	 Telerik::WinControls::UI::RadListView^  radListView1;
	 System::Windows::Forms::Timer^  timer1;
	 Telerik::WinControls::UI::RadPageViewPage^  pageThreads;
	 Telerik::WinControls::UI::RadListView^  radListViewThreads;
	 Telerik::WinControls::UI::RadPanel^  radPanel1;
	 Telerik::WinControls::UI::RadMenu^  radMenu1;
	 Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
	 Telerik::WinControls::UI::RadPanel^  radPanel2;
	private: Telerik::WinControls::UI::RadPageViewPage^  pageOthers;
	protected:
	private: System::Windows::Forms::Button^  button1;
	private: Telerik::WinControls::UI::RadPageViewPage^  pageContainers;
	private: System::Windows::Forms::ListBox^  listBox1;
	protected: System::Windows::Forms::Timer^  timer2;
	private: Telerik::WinControls::UI::RadPageViewPage^  memoryPage;
	protected: Telerik::WinControls::UI::RadListView^  radListView2;
	private:
	protected:

	protected:
	private:
	private: System::ComponentModel::IContainer^  components;


	
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Scope",
				L"Scope"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"ScopeSub",
				L"ScopeSub"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Time",
				L"Time"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn4 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"MinTime",
				L"MinTime"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn5 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"MaxTime",
				L"MaxTime"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn6 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"ThreadId",
				L"ThreadId"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"ThreadMethod",
				L"ThreadMethod"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn8 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"ThreadDelta",
				L"ThreadDelta"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn9 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Paused",
				L"Paused"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn10 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"ClassName",
				L"ClassName"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn11 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Counter",
				L"Counter"));
			this->Page = (gcnew Telerik::WinControls::UI::RadPageView());
			this->pageTime = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
			this->pageThreads = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->radListViewThreads = (gcnew Telerik::WinControls::UI::RadListView());
			this->pageOthers = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->pageContainers = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->listBox1 = (gcnew System::Windows::Forms::ListBox());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
			this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->timer2 = (gcnew System::Windows::Forms::Timer(this->components));
			this->memoryPage = (gcnew Telerik::WinControls::UI::RadPageViewPage());
			this->radListView2 = (gcnew Telerik::WinControls::UI::RadListView());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Page))->BeginInit();
			this->Page->SuspendLayout();
			this->pageTime->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->BeginInit();
			this->pageThreads->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListViewThreads))->BeginInit();
			this->pageOthers->SuspendLayout();
			this->pageContainers->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			this->memoryPage->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// Page
			// 
			this->Page->Controls->Add(this->pageTime);
			this->Page->Controls->Add(this->pageThreads);
			this->Page->Controls->Add(this->pageOthers);
			this->Page->Controls->Add(this->pageContainers);
			this->Page->Controls->Add(this->memoryPage);
			this->Page->Dock = System::Windows::Forms::DockStyle::Fill;
			this->Page->Location = System::Drawing::Point(0, 0);
			this->Page->Name = L"Page";
			this->Page->SelectedPage = this->memoryPage;
			this->Page->Size = System::Drawing::Size(736, 398);
			this->Page->TabIndex = 0;
			this->Page->Text = L"Memory Page";
			// 
			// pageTime
			// 
			this->pageTime->Controls->Add(this->radListView1);
			this->pageTime->ItemSize = System::Drawing::SizeF(80, 28);
			this->pageTime->Location = System::Drawing::Point(10, 37);
			this->pageTime->Name = L"pageTime";
			this->pageTime->Size = System::Drawing::Size(715, 350);
			this->pageTime->Text = L"Performance";
			// 
			// radListView1
			// 
			listViewDetailColumn1->HeaderText = L"Scope";
			listViewDetailColumn1->MinWidth = 175;
			listViewDetailColumn1->Width = 175;
			listViewDetailColumn2->HeaderText = L"ScopeSub";
			listViewDetailColumn2->MinWidth = 175;
			listViewDetailColumn2->Width = 175;
			listViewDetailColumn3->HeaderText = L"Time";
			listViewDetailColumn3->MinWidth = 175;
			listViewDetailColumn3->Width = 175;
			listViewDetailColumn4->HeaderText = L"MinTime";
			listViewDetailColumn5->HeaderText = L"MaxTime";
			this->radListView1->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(5) {
				listViewDetailColumn1,
					listViewDetailColumn2, listViewDetailColumn3, listViewDetailColumn4, listViewDetailColumn5
			});
			this->radListView1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radListView1->ItemSpacing = -1;
			this->radListView1->Location = System::Drawing::Point(0, 0);
			this->radListView1->Name = L"radListView1";
			this->radListView1->Size = System::Drawing::Size(715, 350);
			this->radListView1->TabIndex = 0;
			this->radListView1->Text = L"radListView1";
			this->radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			// 
			// pageThreads
			// 
			this->pageThreads->Controls->Add(this->radListViewThreads);
			this->pageThreads->ItemSize = System::Drawing::SizeF(56, 28);
			this->pageThreads->Location = System::Drawing::Point(10, 37);
			this->pageThreads->Name = L"pageThreads";
			this->pageThreads->Size = System::Drawing::Size(715, 350);
			this->pageThreads->Text = L"Threads";
			// 
			// radListViewThreads
			// 
			listViewDetailColumn6->HeaderText = L"ThreadId";
			listViewDetailColumn7->HeaderText = L"ThreadMethod";
			listViewDetailColumn8->HeaderText = L"ThreadDelta";
			listViewDetailColumn9->HeaderText = L"Paused";
			this->radListViewThreads->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(4) {
				listViewDetailColumn6,
					listViewDetailColumn7, listViewDetailColumn8, listViewDetailColumn9
			});
			this->radListViewThreads->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radListViewThreads->ItemSpacing = -1;
			this->radListViewThreads->Location = System::Drawing::Point(0, 0);
			this->radListViewThreads->Name = L"radListViewThreads";
			this->radListViewThreads->Size = System::Drawing::Size(715, 350);
			this->radListViewThreads->TabIndex = 0;
			this->radListViewThreads->Text = L"radListViewThreads";
			this->radListViewThreads->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			this->radListViewThreads->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &DevelopmentForm::radListViewThreads_KeyDown);
			// 
			// pageOthers
			// 
			this->pageOthers->Controls->Add(this->button1);
			this->pageOthers->ItemSize = System::Drawing::SizeF(50, 28);
			this->pageOthers->Location = System::Drawing::Point(10, 37);
			this->pageOthers->Name = L"pageOthers";
			this->pageOthers->Size = System::Drawing::Size(715, 350);
			this->pageOthers->Text = L"Others";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(3, 3);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(106, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"GenerateMissingGifs";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &DevelopmentForm::button1_Click);
			// 
			// pageContainers
			// 
			this->pageContainers->Controls->Add(this->listBox1);
			this->pageContainers->ItemSize = System::Drawing::SizeF(112, 28);
			this->pageContainers->Location = System::Drawing::Point(10, 37);
			this->pageContainers->Name = L"pageContainers";
			this->pageContainers->Size = System::Drawing::Size(715, 350);
			this->pageContainers->Text = L"radPageViewPage1";
			// 
			// listBox1
			// 
			this->listBox1->FormattingEnabled = true;
			this->listBox1->Location = System::Drawing::Point(74, 27);
			this->listBox1->Name = L"listBox1";
			this->listBox1->Size = System::Drawing::Size(120, 277);
			this->listBox1->TabIndex = 0;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 50;
			this->timer1->Tick += gcnew System::EventHandler(this, &DevelopmentForm::timer1_Tick);
			// 
			// radPanel1
			// 
			this->radPanel1->AutoSize = true;
			this->radPanel1->Controls->Add(this->radMenu1);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel1->Location = System::Drawing::Point(0, 0);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(736, 20);
			this->radPanel1->TabIndex = 1;
			this->radPanel1->Text = L"radPanel1";
			// 
			// radMenu1
			// 
			this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->radMenuItem1 });
			this->radMenu1->Location = System::Drawing::Point(0, 0);
			this->radMenu1->Name = L"radMenu1";
			this->radMenu1->Size = System::Drawing::Size(736, 20);
			this->radMenu1->TabIndex = 0;
			this->radMenu1->Text = L"radMenu1";
			// 
			// radMenuItem1
			// 
			this->radMenuItem1->AccessibleDescription = L"MapWalls";
			this->radMenuItem1->AccessibleName = L"MapWalls";
			this->radMenuItem1->Name = L"radMenuItem1";
			this->radMenuItem1->Text = L"MapWalls";
			this->radMenuItem1->Click += gcnew System::EventHandler(this, &DevelopmentForm::radMenuItem1_Click);
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->Page);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel2->Location = System::Drawing::Point(0, 20);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(736, 398);
			this->radPanel2->TabIndex = 2;
			this->radPanel2->Text = L"radPanel2";
			// 
			// timer2
			// 
			this->timer2->Enabled = true;
			this->timer2->Interval = 1;
			this->timer2->Tick += gcnew System::EventHandler(this, &DevelopmentForm::timer2_Tick);
			// 
			// memoryPage
			// 
			this->memoryPage->Controls->Add(this->radListView2);
			this->memoryPage->ItemSize = System::Drawing::SizeF(58, 28);
			this->memoryPage->Location = System::Drawing::Point(10, 37);
			this->memoryPage->Name = L"memoryPage";
			this->memoryPage->Size = System::Drawing::Size(715, 350);
			this->memoryPage->Text = L"Memory";
			// 
			// radListView2
			// 
			listViewDetailColumn10->HeaderText = L"ClassName";
			listViewDetailColumn10->Width = 400;
			listViewDetailColumn11->HeaderText = L"Counter";
			listViewDetailColumn11->Width = 300;
			this->radListView2->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(2) {
				listViewDetailColumn10,
					listViewDetailColumn11
			});
			this->radListView2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radListView2->ItemSpacing = -1;
			this->radListView2->Location = System::Drawing::Point(0, 0);
			this->radListView2->Name = L"radListView2";
			this->radListView2->Size = System::Drawing::Size(715, 350);
			this->radListView2->TabIndex = 1;
			this->radListView2->Text = L"radListView2";
			this->radListView2->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			// 
			// DevelopmentForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(736, 418);
			this->Controls->Add(this->radPanel2);
			this->Controls->Add(this->radPanel1);
			this->Name = L"DevelopmentForm";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"DevelopmentForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Page))->EndInit();
			this->Page->ResumeLayout(false);
			this->pageTime->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->EndInit();
			this->pageThreads->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListViewThreads))->EndInit();
			this->pageOthers->ResumeLayout(false);
			this->pageContainers->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			this->radPanel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			this->memoryPage->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


	Telerik::WinControls::UI::ListViewDataItem^ GetTimeListViewItem(String^ Id, String^ subId){
		for each (auto item in radListView1->Items){
			if (item["Scope"]->ToString() == Id && item["ScopeSub"]->ToString() == subId)
				return item;
		}
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
		radListView1->Items->Add(newItem);
		newItem["Scope"] = Id;
		newItem["ScopeSub"] = subId;
		newItem["Time"] = safe_cast<System::UInt64^>(nullptr);
		newItem["MaxTime"] = System::UInt64(0xffffffffffffffff);
		newItem["MinTime"] = System::UInt64(0xffffffffffffffff);
		return newItem;
	}

	void UpdateTime(String^ strId, String^ subId, uint64_t time){
		auto item = GetTimeListViewItem(strId, subId);
		if ((uint64_t)item["MaxTime"] == 0xffffffffffffffff || time > (uint64_t)item["MaxTime"]){
			item["MaxTime"] = time;
		}
		else if ((uint64_t)item["MinTime"] == 0xffffffffffffffff || time < (uint64_t)item["MinTime"]){
			item["MinTime"] = time;
		}
		item["Time"] = time;
	}
	
	void SetThread(uint32_t ThreadId, String^ ThreadMethod){
		for each (auto item in radListViewThreads->Items){
			if ((uint32_t)item["ThreadId"] == ThreadId){
				item["ThreadDelta"] = DevelopmentUtil::get_thread_delta(ThreadId);
				return;
			}
				
		}
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
		radListViewThreads->Items->Add(newItem);
		newItem["ThreadId"] = ThreadId;
		newItem["ThreadMethod"] = ThreadMethod;
		newItem["ThreadDelta"] = DevelopmentUtil::get_thread_delta(ThreadId);
		newItem["Paused"] = false;
		
	}

	void SetThreadState(uint32_t tid ,bool pause){
		for each (auto item in radListViewThreads->Items){
			if ((uint32_t)item["ThreadId"] == tid){
				item["Paused"] = pause;
				return;
			}
		}
	}

	void UpdateTimes(){
		auto costs = DevelopmentManager::get()->get_time_costs();
		for (auto cost = costs.begin(); cost != costs.end(); cost++){
			for (auto sub_cost = cost->second.begin(); sub_cost != cost->second.end(); sub_cost++){
				UpdateTime(gcnew String(&cost->first[0]), gcnew String(&sub_cost->first[0]), sub_cost->second);
			}
		}
	}

	void UpdateThreads(){
		auto threads = DevelopmentManager::get()->get_threads();
		for (auto thread = threads.begin(); thread != threads.end(); thread++){
			SetThread(thread->first, gcnew String(&thread->second[0]));
		}
	}

	void creatMemory();
	void updateMemory();
	
	 System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
		UpdateTimes();
		UpdateThreads();
		updateMemory();
	}
 System::Void radListViewThreads_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	if (e->KeyCode == System::Windows::Forms::Keys::Return){
		auto item = radListViewThreads->SelectedItem;
		if (item){
			if (((bool)item["Paused"]))
				DevelopmentUtil::resume_thread((uint32_t)item["ThreadId"]);
			else
				DevelopmentUtil::suspend_thread((uint32_t)item["ThreadId"]);
			SetThreadState((uint32_t)item["ThreadId"], !((bool)item["Paused"]));

		}
	}
}
 System::Void radMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
	(gcnew MapWall)->Show();
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

	String^ file_string = gcnew String("");

	auto items = ItemsManager::get()->get_itens();
	std::vector<std::string> missing_items;
	for (auto item : items){
		if(!GifManager::get()->has_item(item)){
			if (std::find(missing_items.begin(), missing_items.end(), item) == missing_items.end())
				missing_items.push_back(item);
		}
	}

	for (auto item : missing_items)
	{

		file_string += gcnew String(&item[0]) + "\n";
	}
	
	System::IO::File::WriteAllText("missing_gifs.txt", file_string);
}
private: System::Void timer2_Tick(System::Object^  sender, System::EventArgs^  e) {
	auto containers = ContainerManager::get()->get_containers();
	for (auto container : containers){
		if (!listBox1->Items->Contains(container.second.get()->get_id())){
			listBox1->Items->Add(container.second.get()->get_id());
		}
	}
	std::vector<uint32_t> list;
	for each(auto item in listBox1->Items){
		bool contains = false;
		for (auto container : containers){
			if (container.second.get()->get_id() == (uint32_t)item){
				contains = true;
				break;
			}
		}
		if (contains)
			continue;
		list.push_back((uint32_t)item);
	}
	for(auto item_remove : list){
		listBox1->Items->Remove(item_remove);
	}
}
};
}
