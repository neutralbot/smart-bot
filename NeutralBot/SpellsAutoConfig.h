#pragma once
#include "ManagedUtil.h"
#include "SpellManager.h"
#include "Core\Util.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::IO;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class SpellsAutoConfig : public Telerik::WinControls::UI::RadForm{

	public: SpellsAutoConfig(void){
			InitializeComponent();
		}
	protected: ~SpellsAutoConfig(){
			unique = nullptr;

			if (components)
				delete components;
		}

	protected: static SpellsAutoConfig^ unique;

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}
	public: static void CloseUnique(){
				if (unique)unique->Close();
	}
	public: static void ShowUnique(){
				if (unique == nullptr)
					unique = gcnew SpellsAutoConfig();

				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew SpellsAutoConfig();
	}

	private: Telerik::WinControls::UI::RadTreeView^  ListProfile;
	private: Telerik::WinControls::UI::RadContextMenu^  radContextMenu1;
	private: Telerik::WinControls::UI::RadContextMenuManager^  radContextMenuManager1;
	private: System::ComponentModel::IContainer^  components;
	protected:
	protected:


#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(SpellsAutoConfig::typeid));
			this->ListProfile = (gcnew Telerik::WinControls::UI::RadTreeView());
			this->radContextMenu1 = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
			this->radContextMenuManager1 = (gcnew Telerik::WinControls::UI::RadContextMenuManager());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListProfile))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// ListProfile
			// 
			this->ListProfile->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(191)), static_cast<System::Int32>(static_cast<System::Byte>(219)),
				static_cast<System::Int32>(static_cast<System::Byte>(255)));
			this->ListProfile->Cursor = System::Windows::Forms::Cursors::Default;
			this->ListProfile->Dock = System::Windows::Forms::DockStyle::Fill;
			this->ListProfile->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
			this->ListProfile->ForeColor = System::Drawing::Color::Black;
			this->ListProfile->Location = System::Drawing::Point(0, 0);
			this->ListProfile->Name = L"ListProfile";
			this->ListProfile->RadContextMenu = this->radContextMenu1;
			this->ListProfile->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->ListProfile->Size = System::Drawing::Size(409, 330);
			this->ListProfile->SpacingBetweenNodes = -1;
			this->ListProfile->TabIndex = 0;
			this->ListProfile->Text = L"radTreeView1";
			this->ListProfile->NodeMouseDoubleClick += gcnew Telerik::WinControls::UI::RadTreeView::TreeViewEventHandler(this, &SpellsAutoConfig::ListProfile_NodeMouseDoubleClick);
			// 
			// radContextMenu1
			// 
			this->radContextMenu1->DropDownOpening += gcnew System::ComponentModel::CancelEventHandler(this, &SpellsAutoConfig::radContextMenu1_DropDownOpening);
			// 
			// SpellsAutoConfig
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(409, 330);
			this->Controls->Add(this->ListProfile);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"SpellsAutoConfig";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"SpellsAutoConfig";
			this->Load += gcnew System::EventHandler(this, &SpellsAutoConfig::SpellsAutoConfig_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListProfile))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
		Telerik::WinControls::UI::RadMenuItem^ ItemImport;
		
		void creatContextMenu(){
			ItemImport = gcnew Telerik::WinControls::UI::RadMenuItem("Import");
			ItemImport->Click += gcnew System::EventHandler(this, &SpellsAutoConfig::MenuImport_Click);
		}
		void loadFilesLua(){
			/*String^ dirFiles = gcnew String(DEFAULT_TEMP_FILE_SCRIPT.c_str());

			DirectoryInfo^ directory = gcnew DirectoryInfo(dirFiles);
			Telerik::WinControls::UI::RadTreeNode^ Node = gcnew Telerik::WinControls::UI::RadTreeNode(directory->Name);

			Node->Expanded = true;
			Node->Value = "folder";
			loadDirectoriesAndFiles(directory, Node);
			ListProfile->Nodes->AddRange(Node);*/
		}
		bool import_script(std::string file_name){
			Json::Value file = loadJsonFile(file_name);
			if (file.isNull())
				return false;

			SpellManager::get()->parse_json_to_classImport(file["SpellManager"]);
			return true;
		}
		void loadDirectoriesAndFiles(DirectoryInfo^ dir, Telerik::WinControls::UI::RadTreeNode^ parentNode){
			if (!dir->Exists)
				return;

			for each(FileInfo^ f in dir->GetFiles()){
				Telerik::WinControls::UI::RadTreeNode^ fileNode = gcnew Telerik::WinControls::UI::RadTreeNode(f->Name);
				fileNode->Value = f->Name;
				parentNode->Nodes->AddRange(fileNode);
			}

			for each(DirectoryInfo^ d in dir->GetDirectories()){
				Telerik::WinControls::UI::RadTreeNode^ dirNode = gcnew Telerik::WinControls::UI::RadTreeNode(d->Name);
				dirNode->Value = "folder";
				parentNode->Nodes->AddRange(dirNode);
				loadDirectoriesAndFiles(d, dirNode);
			}
		}

		System::Void MenuImport_Click(Object^ sender, EventArgs^ e){
			if (!ListProfile->SelectedNode)
				return;

			Telerik::WinControls::UI::RadTreeNode^ father = ListProfile->SelectedNode->Parent;
			String^ dir = "\\";

			while (father){
				dir = "\\" + father->Text + dir;
				father = father->Parent;
			}

			String^ finaldir = Directory::GetCurrentDirectory() + dir;

			String ^path = finaldir + ListProfile->SelectedNode->Text;

			System::Windows::Forms::DialogResult dialogResult = Telerik::WinControls::RadMessageBox::Show(this, "Import", "Deseja mesmo importa profile?", MessageBoxButtons::YesNo);

			if (dialogResult == System::Windows::Forms::DialogResult::No)
				return;

			ListProfile->Nodes->Clear();
			radContextMenu1->Items->Clear();

			if (!import_script(managed_util::fromSS(path)))
				Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
			else
				Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

			SpellsAutoConfig_Load(nullptr, nullptr);
		}
	
private: System::Void SpellsAutoConfig_Load(System::Object^  sender, System::EventArgs^  e) {
				 loadFilesLua();
				 creatContextMenu();
	}
	private: System::Void radContextMenu1_DropDownOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
				 radContextMenu1->Items->Clear();

				 if (!ListProfile->SelectedNode)
					 return;

				 if (ListProfile->SelectedNode == ListProfile->TopNode){
					 e->Cancel = true;
					 return;
				 }

				 if (ListProfile->SelectedNode->Value != "folder"){
					 radContextMenu1->Items->Add(ItemImport);
					 return;
				 }
				 else{
					 e->Cancel = true;
					 return;
				 }
	}
	private: System::Void ListProfile_NodeMouseDoubleClick(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
				 if (!ListProfile->SelectedNode)
					 return;

				 if (ListProfile->SelectedNode == ListProfile->TopNode){
					 return;
				 }

				 if (ListProfile->SelectedNode->Value != "folder"){
					 MenuImport_Click(nullptr, nullptr);
					 return;
				 }
				 else
					 return;				 
	}
	};
}
