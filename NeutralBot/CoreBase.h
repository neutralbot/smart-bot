#pragma once
#include <stdint.h>
#pragma pack(push,1)
enum core_priority : uint32_t{
	core_priority_lowest = 0,
	core_priority_lower,
	core_priority_normal,
	core_priority_high,
	core_priority_highest
};

class CoreBase{
	
	bool _state;
public:
	virtual void set_core_priority(core_priority curr_priority);

	virtual core_priority get_core_priority();

	virtual void set_priority_high();

	virtual void set_priority_normal();

	CoreBase();


	void set_state(bool state);

	bool get_state();
	uint32_t current_priority;
};

#pragma pack(pop)