#pragma once
#include "HUDInputCore.h"
#include "Core\InputManager.h"
#include <thread>
#include "DevelopmentManager.h"
#include "Core\Neutral.h"

HUDInputCore::HUDInputCore(){
	start();
}

void HUDInputCore::start(){
	std::thread(std::bind(&HUDInputCore::run_async, this)).detach(); 
}


void HUDInputCore::run_async(){
	//PODE DA ERRO
	MONITOR_THREAD(__FUNCTION__);

	InputBaseEvent* input_retval;
	input_match_params params;
	params.filter_mouse_moves = false;

	while (true){
		Sleep(5);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		std::shared_ptr<InputHolder> input = InputManager::get()->wait_input(UINT32_MAX/*, params*/);
		if (!input)
			continue;

		if (input->input_ptr->is_mouse_event())
			input_retval = new MouseInput(*(MouseInput*)input->input_ptr);
		else if (input->input_ptr->is_keyboard_event())
			input_retval = new KeyboardInput(*(KeyboardInput*)input->input_ptr);

	}
}