#include "MonsterManager.h"



MonsterManager::MonsterManager(){
		load_script(".\\conf\\defaults\\monster.json");
	}


std::shared_ptr<MonsterStruct> MonsterManager::get_creature_by_name(std::string name){
	auto it = std::find_if(mapMonsterStruct.begin(), mapMonsterStruct.end(), [&](std::pair<std::string, std::shared_ptr<MonsterStruct>> monster_pair){
			return _stricmp(&name[0], &monster_pair.first[0]) == 0;
		});
		if (it != mapMonsterStruct.end()){
			return it->second;
		}
		return nullptr;
	}

void MonsterManager::addinvector(std::string monstername){
		MonsterList.push_back(monstername);
	}

std::vector<std::string> MonsterManager::get_MonsterList(){
		return MonsterList;
	}
std::map<std::string, std::shared_ptr<MonsterStruct>> MonsterManager::get_mapMonsterStruct(){
		return mapMonsterStruct;
	}

Json::Value MonsterManager::parse_class_to_json() {
		Json::Value monster;
		Json::Value monsterList;
		Json::Value monsterMap;

		for (int i = 0; i < (int)MonsterList.size(); i++) {
			Json::Value monsterListItem;
			monsterListItem["name"] = MonsterList[i];

			monsterList[i] = monsterListItem;
		}

		for (auto it : mapMonsterStruct){
			Json::Value monsterItem;

			monsterItem["name"] = it.second->get_name();

			monsterMap[it.first] = monsterItem;
		}

		monster["monsterList"] = monsterList;
		monster["monsterMap"] = monsterMap;
		return monster;
	}

void MonsterManager::parse_json_to_class(Json::Value jsonObject) {
		Json::Value monsterList = jsonObject["monsterList"];
		Json::Value monsterMap = jsonObject["monsterMap"];

		for (auto monster : monsterList){
			std::shared_ptr<MonsterStruct> monsterItem(new MonsterStruct);

			monsterItem->set_name(monster["name"].asString());
			Json::Value& items = monster["loot"];
			if (!items.empty()){
				for (auto item : items){
					//try{

					monsterItem->items_id.push_back(item.asString());
					//}
					//catch (...){
					//}

				}
			}
			mapMonsterStruct[monster["name"].asString()] = monsterItem;
			MonsterList.push_back(monster["name"].asString());
		}
	}

bool MonsterManager::load_script(std::string file_name){
		Json::Value file = loadJsonFile(file_name);
		if (file.isNull())
			return false;

		parse_json_to_class(file["MonsterManager"]);

		return true;
	}

bool MonsterManager::saveJsonFile(std::string file, Json::Value json) {
		std::ofstream jsonSet;

		jsonSet.open(file);
		if (!jsonSet.is_open())
			return false;

		jsonSet << json.toStyledString();
		jsonSet.close();
		return true;
	}

bool MonsterManager::save_script(std::string file_name){
		Json::Value file;
		file["version"] = 0100;

		file["MonsterManager"] = parse_class_to_json();

		return saveJsonFile(file_name, file);
	}

MonsterManager* MonsterManager::get(){
		static MonsterManager* m = nullptr;
		if (!m){
			m = new MonsterManager();
		}

		return m;
	}
