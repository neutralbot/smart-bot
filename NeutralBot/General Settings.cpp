#pragma once
#include "General Settings.h"
#include "GeneralManager.h"
#include "LanguageManager.h"
#include "ManagedUtil.h"

using namespace NeutralBot;

System::Void GeneralSettings::radCheckBox1_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	GeneralManager::get()->set_interface_most_top(radCheckTopMost->Checked);
	FastUIPanelController::get()->on_top_most_option_change(radCheckTopMost->Checked);
}
System::Void GeneralSettings::radCheckBox2_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	GeneralManager::get()->set_interface_auto_resize(radCheckAutoResize->Checked);
	FastUIPanelController::get()->on_resize_option_change(radCheckAutoResize->Checked);
}
System::Void GeneralSettings::radCheckBox3_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	GeneralManager::get()->set_interface_shortcut(radCheckButtonPanel->Checked);
	FastUIPanelController::get()->on_panel_shortcut_option_change(radCheckButtonPanel->Checked);
}
System::Void GeneralSettings::practice_mode_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	GeneralManager::get()->set_practice_mode(practice_mode->Checked);
}

void GeneralSettings::update_idiom(){
	radCheckTopMost->Text = gcnew String(&GET_TR(managed_util::fromSS(radCheckTopMost->Text))[0]);
	radCheckAutoResize->Text = gcnew String(&GET_TR(managed_util::fromSS(radCheckAutoResize->Text))[0]);
	radCheckButtonPanel->Text = gcnew String(&GET_TR(managed_util::fromSS(radCheckButtonPanel->Text))[0]);
	practice_mode->Text = gcnew String(&GET_TR(managed_util::fromSS(practice_mode->Text))[0]);
	radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
	radGroupBox1->Text = gcnew String(&GET_TR(managed_util::fromSS(radGroupBox1->Text))[0]);
}

System::Void GeneralSettings::loadSettings(){
	disable_update = true;
	practice_mode->Checked = GeneralManager::get()->get_practice_mode();
	radCheckTopMost->Checked = GeneralManager::get()->get_interface_most_top();
	radCheckAutoResize->Checked = GeneralManager::get()->get_interface_auto_resize();
	radCheckButtonPanel->Checked = GeneralManager::get()->get_interface_shortcut();
	spin_auto_save->Value = (int)GeneralManager::get()->get_auto_save_sec() / 1000;
	disable_update = false;
}

System::Void GeneralSettings::spin_auto_save_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_update)
		return;

	if ((int)spin_auto_save->Value < 60)
		spin_auto_save->Value = 60;

	GeneralManager::get()->set_auto_save_sec((uint32_t)spin_auto_save->Value * 1000);
}

GeneralSettings::GeneralSettings(void){
	InitializeComponent();
	this->Icon = gcnew System::Drawing::Icon("img\\smartboticon.ico");
	loadSettings();
}

GeneralSettings::~GeneralSettings(){
	if (components)
		delete components;

	unique = nullptr;
}
