#pragma once
#include <thread>
#include "Core\AlertsCore.h"
#include <Thread>
#include <windows.h>
#include <mmsystem.h>
#include "DevelopmentManager.h"
#include "GeneralManager.h"

#pragma comment(lib, "winmm.lib")

AlertsCore::AlertsCore(){
	std::thread([&](){
		MONITOR_THREAD("AlertsCore::refresh");
		refresh(); 
	}).detach();
}

void AlertsCore::play_sound(alert_t type){
	std::string pathSound = GeneralManager::get()->get_sound_filename(type);

	if (string_util::trim(pathSound) == "")
		pathSound = DEFAULT_ALARM_PATH;
	else
		pathSound = DEFAULT_DIR_SETTINGS_PATH + "\\" + pathSound;

	if (FILE *file = fopen(pathSound.c_str(), "r")){
		PlaySound(pathSound.c_str(), NULL, SND_ASYNC);
		fclose(file);
	}
}

void AlertsCore::refresh(){
	while (true){
		Sleep(100);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_alerts_state() || 
			!NeutralManager::get()->get_bot_state() || 
			NeutralManager::get()->is_paused())
			continue;

		run();
	}
}

void AlertsCore::run(){
	std::map<uint32_t, std::shared_ptr<AlertsStruct>> myMap = AlertsManager::get()->GetMap();
	for (auto it = myMap.begin(); it != myMap.end(); ++it){
		if (!need_execute(it->second->get_type(), it->second->value))
			continue;

		if (it->second->get_sound()){
			if (!it->second->need_execute_delay())
				continue;

			mtx_access.lock();

			play_sound(it->second->type);
			it->second->timer.reset();

			mtx_access.unlock();
		}

		if (it->second->get_logout())
			loggout();

		if (it->second->get_pause())
			pause_bot();
	}
}