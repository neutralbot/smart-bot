#include "HunterManager.h"

void HunterManager::set_keep_distance_algorithm(std::string in){
	keep_distance_algorithm = in;
}

char* HunterManager::get_keep_distance_algorithm_data(){
	return &keep_distance_algorithm[0];
}

void HunterManager::set_weapon(uint32_t id){
	weapon_id = id;
}

uint32_t HunterManager::get_weapon(){
	return weapon_id;
}

bool HunterManager::get_wait_pulse(){
	return wait_pulse;
}

HunterManager::HunterManager(){
	group_status = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	reset_keep_distance_algorithm();
	min_points = 0;
}

void HunterManager::reset_distance_algorithm(){
	std::ifstream in_data(".\\");
	keep_distance_algorithm;
}

std::string HunterManager::request_new_creature(){
	lock_guard lock(mtx_access);
	int id_ = 0;
	std::string current_id = "Target" + std::to_string(id_);
	while (target_rules.find(current_id) != target_rules.end()){
		id_++;
		current_id = "Target" + std::to_string(id_);
	}
	std::shared_ptr<Target> new_target(new Target);
	new_target->set_name("none");
	target_rules[current_id] = new_target;
	return current_id;
}

std::shared_ptr<Target> HunterManager::get_target_rule_by_id(std::string id_){
	lock_guard lock(mtx_access);
	auto it = target_rules.find(id_);
	if (it == target_rules.end())
		return 0;
	return it->second;
}

std::map<std::string /*Id do Monstro*/, std::shared_ptr<Target>> HunterManager::get_target_rules(){
	lock_guard lock(mtx_access);
	return target_rules;
}

std::pair<TargetPtr, TargetNonSharedPtr> HunterManager::get_target_by_values(std::string name, uint32_t life_percent, uint32_t current_distance){
	lock_guard lock(mtx_access);
	return get_target_by_values(target_rules, &name[0], life_percent, current_distance);
}

bool HunterManager::contains_creature_name(std::string name){
	lock_guard lock(mtx_access);
	for (auto target : target_rules){
		if (_stricmp(&name[0], &target.second->get_name()[0]) == 0)
			return true;
	}
	return false;
}

std::pair<TargetPtr, TargetNonSharedPtr> HunterManager::get_target_by_values(TargetPtrMap& target_map,
	std::string& name, uint32_t life_percent, uint32_t current_distance){
	return get_target_by_values(target_map, &name[0], life_percent, current_distance);
}

std::pair<TargetPtr, TargetNonSharedPtr> HunterManager::get_target_by_values(TargetPtrMap& target_map, char* name, 
	uint32_t life_percent, uint32_t current_distance){

	for (auto target : target_map){
		if (_stricmp(&name[0], &target.second->get_name()[0]) != 0)
			continue;
		TargetNonSharedPtr retval = target.second->get_group_life_in_range(life_percent);
		if (retval)
			return std::pair<TargetPtr, TargetNonSharedPtr>(target.second, retval);
	}

	return std::pair<TargetPtr, TargetNonSharedPtr>();
}

void HunterManager::Clear(){
	group_status = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	target_rules.clear();
}

void HunterManager::disable_group(uint32_t group_id){
	set_group_state(group_id, false);
}

void HunterManager::enable_group(uint32_t group_id){
	set_group_state(group_id, true);
}

void HunterManager::set_group_state(uint32_t group_id, bool state){
	if (group_status.size() <= group_id)
		return;
	group_status[group_id] = state;
}

bool HunterManager::get_group_state(uint32_t group_id){
	if (group_status.size() <= group_id)
		return false;

	return group_status[group_id];
}

void HunterManager::set_any_monster(bool state){
	any_monster = state;
}

bool HunterManager::get_any_monster(){
	return any_monster;
}

void HunterManager::delete_id(std::string id_){
	lock_guard lock(mtx_access);
	auto i = target_rules.find(id_);
	if (i != target_rules.end())
		target_rules.erase(i);
}

Target::Target(){
	name = "";
}

TargetNonShared::TargetNonShared(){
	danger = 1;
	order = 1;
	min_hp = 0;
	max_hp = 100;
	distance = 1;
	diagonal = false;
	shooter = false;
	pathable = true;
	shotable = false;
	loot = true;
}



void TargetNonShared::set_shooter(bool in){
	shooter = in;
}

bool TargetNonShared::get_shooter(){
	return shooter;
}

bool TargetNonShared::in_distance_range(uint32_t current_distance){
	return current_distance >= target_range_min && current_distance <= target_range_max;
}

void TargetNonShared::set_type_lure(lure_mode_t in){
	type_lure = in;
}

lure_mode_t TargetNonShared::get_type_lure(){
	return type_lure;
}

void TargetNonShared::set_weapon(uint32_t id){
	weapon_id = id;
}

uint32_t TargetNonShared::get_weapon(){
	return weapon_id;
}

bool TargetNonShared::get_use_weapon(){
	return weapon_id != 0;
}

void TargetNonShared::set_use_weapon(bool use){
}


std::shared_ptr<TargetNonShared> Target::get_at_group(int group){
	if (non_shared_info.find(group) == non_shared_info.end()){
		non_shared_info[group] = TargetNonSharedPtr(new TargetNonShared);
	}
	return non_shared_info[group];
}

TargetNonSharedPtr Target::get_group_match(uint32_t life_percent, uint32_t current_range, bool pathable){
	TargetNonSharedPtr retval = nullptr;
	for (auto it : non_shared_info){
		if (!HunterManager::get()->get_group_state(it.first))
			continue;
		
		if (it.second->get_min_hp() > life_percent)
			continue;
		if (it.second->get_max_hp() < life_percent)
			continue;
		if (!it.second->in_distance_range(current_range))
			continue;		

		if (!it.second->get_pathable() || pathable){
			if (!retval || retval->get_order() < it.second->get_order()){
				retval = it.second;
			}
		}
	}
	return retval;
}

TargetNonSharedPtr Target::get_group_life_in_range(uint32_t life_percent){
	for (auto it : non_shared_info){
		if (it.second->get_min_hp() > life_percent)
			continue;
		if (it.second->get_max_hp() < life_percent)
			continue;
		return it.second;
	}
	return nullptr;
}

Json::Value HunterManager::parse_class_to_json() {
	Json::Value retval;
	retval["any_monster"] = get_any_monster();
	retval["wait_pulse"] = wait_pulse;
	retval["request_points"] = min_points;
	retval["weapon_id"] = weapon_id;
	retval["has_keep_distance_hook"] = has_keep_distance_hook;
	retval["keep_distance_algorithm"] = keep_distance_algorithm;

	retval["min_points_to_change"] = min_points_to_change;

	retval["path_index"] = path_index;
	retval["path_index_two"] = path_index_two;

	retval["new_mode"] = new_mode;
	retval["old_mode"] = old_mode;

	Json::Value& json_active_groups = retval["active_groups"];
	Json::Value& json_target_rules = retval["target_rules"];

	for (auto target_rule : target_rules)
		json_target_rules[target_rule.first] = target_rule.second->parse_class_to_json();

	int index = 0;
	for (auto group : group_status){
		json_active_groups[index] = index;
		index++;
	}
	return retval;
}

bool HunterManager::reset_keep_distance_algorithm(){
	std::ifstream event_str(".\\lua\\events\\on_hunter_runner.lua");
	if (event_str.is_open() == false){
		return false;
	}
	keep_distance_algorithm = std::string((std::istreambuf_iterator<char>(event_str)), std::istreambuf_iterator<char>());
	return true;
}

void HunterManager::parse_json_to_class(Json::Value& hunterValue) {
	Clear();

	if (!hunterValue["weapon_id"].empty() || !hunterValue["weapon_id"].isNull()){
		weapon_id = hunterValue["weapon_id"].asInt();
	}
	else{
		weapon_id = 0;
	}

	if (!hunterValue["wait_pulse"].empty() || !hunterValue["wait_pulse"].isNull()){
		wait_pulse = hunterValue["wait_pulse"].asBool();
	}
	else
		wait_pulse = false;

	if (!hunterValue["anyMonster"].empty() || !hunterValue["anyMonster"].isNull()){
		set_any_monster(hunterValue["anyMonster"].asBool());
	}
	else if (!hunterValue["any_monster"].empty() || !hunterValue["any_monster"].isNull()){
		set_any_monster(hunterValue["any_monster"].asBool());
	}
		
	
	if (!hunterValue["has_keep_distance_hook"].empty() || !hunterValue["has_keep_distance_hook"].isNull())
		has_keep_distance_hook = (hunterValue["has_keep_distance_hook"].asBool());
	else{
		has_keep_distance_hook = false;
	}

	if (!hunterValue["keep_distance_algorithm"].empty() || !hunterValue["keep_distance_algorithm"].isNull())
		keep_distance_algorithm = (hunterValue["keep_distance_algorithm"].asString());
	else{
		reset_keep_distance_algorithm();
	}

	if (!hunterValue["min_points_to_change"].empty() || !hunterValue["min_points_to_change"].isNull())
		set_min_points_to_change(hunterValue["min_points_to_change"].asInt());

	if (!hunterValue["request_points"].empty() || !hunterValue["request_points"].isNull())
		set_min_points(hunterValue["request_points"].asInt());

	if (!hunterValue["path_index"].empty() || !hunterValue["path_index"].isNull())
		set_path_index(hunterValue["path_index"].asInt());

	if (!hunterValue["path_index_two"].empty() || !hunterValue["path_index_two"].isNull())
		set_path_index_two(hunterValue["path_index_two"].asInt());

	if (!hunterValue["new_mode"].empty() || !hunterValue["new_mode"].isNull())
		set_new_mode((attack_mode_t)hunterValue["new_mode"].asInt());

	if (!hunterValue["old_mode"].empty() || !hunterValue["old_mode"].isNull())
		set_old_mode((attack_mode_t)hunterValue["old_mode"].asInt());

	//clear
	for (uint32_t i = 0; i < group_status.size(); i++)
		group_status[i] = false;

	if (!hunterValue["active_groups"].empty() || !hunterValue["active_groups"].isNull()){
		Json::Value& json_active_groups = hunterValue["active_groups"];

		for (uint32_t index = 0; index < json_active_groups.size(); index++)
			group_status[json_active_groups[index].asInt()] = true;
	}

	if (!hunterValue["target_rules"].empty() || !hunterValue["target_rules"].isNull()){
		Json::Value& json_target_rules = hunterValue["target_rules"];

		Json::Value::Members target_members = json_target_rules.getMemberNames();
		for (std::string member : target_members){
			TargetPtr _target(new Target);
			_target->parse_json_to_class(json_target_rules[member]);
			target_rules[member] = _target;
		}
	}
}

Json::Value Target::parse_class_to_json() {
	Json::Value json_target;
	json_target["name"] = name;

	Json::Value& json_groups_attributes = json_target["groups_attributes"];
	for (auto pair_group : non_shared_info)
		json_groups_attributes[std::to_string(pair_group.first)] = pair_group.second->parse_class_to_json();

	return json_target;
}

void Target::parse_json_to_class(Json::Value& json_target) {
	if (!json_target["groups_attributes"].empty() || !json_target["groups_attributes"].isNull()){
		Json::Value json_groups_attributes = json_target["groups_attributes"];
		Json::Value::Members& members_groups_attributes = json_groups_attributes.getMemberNames();

		if (!json_target["name"].empty() || !json_target["name"].isNull())
			name = json_target["name"].asString();

		for (std::string member : members_groups_attributes){
			TargetNonSharedPtr _attributes(new TargetNonShared);
			_attributes->parse_json_to_class(json_groups_attributes[member]);
			non_shared_info[atoi(&member[0])] = _attributes;
		}
	}
}

Json::Value TargetNonShared::parse_class_to_json() {
	Json::Value attr;
	attr["danger"] = danger;
	attr["order"] = order;
	attr["min_hp"] = min_hp;
	attr["max_hp"] = max_hp;
	attr["distance"] = distance;
	attr["shooter"] = shooter;
	attr["pathable"] = pathable;
	attr["shotable"] = shotable;
	attr["diagonal"] = diagonal;
	attr["loot"] = loot;
	attr["target_range_min"] = target_range_min;
	attr["target_range_max"] = target_range_max;
	attr["attack_if_trapped"] = attack_if_trapped;
	attr["attack_if_block_path"] = attack_if_block_path;
	attr["point"] = point;
	attr["weapon_id"] = weapon_id;

	return attr;
}

bool HunterManager::get_has_keep_distance_hook(){
	return has_keep_distance_hook;
}

void HunterManager::set_has_keek_distance_hook(bool state){
	has_keep_distance_hook = state;
}

HunterManager* HunterManager::get(){
	static HunterManager* m = nullptr;
	if (!m)
		m = new HunterManager();
	return m;
}

void HunterManager::set_wait_pulse(bool state){
	wait_pulse = state;
}

void HunterManager::set_min_points(int temp){
	min_points = temp;
}

int HunterManager::get_min_points(){
	return min_points;
}

void TargetNonShared::parse_json_to_class(Json::Value& attr) {

	if (!attr["point"].empty() || !attr["point"].isNull())
		point = attr["point"].asInt();
	else{
		point = 0;
	}

	if (!attr["attack_if_trapped"].empty() || !attr["attack_if_trapped"].isNull())
		attack_if_trapped = attr["attack_if_trapped"].asBool();
	else{
		attack_if_trapped = false;
	}

	if (!attr["attack_if_block_path"].empty() || !attr["attack_if_block_path"].isNull())
		attack_if_block_path = attr["attack_if_block_path"].asBool();
	else{
		attack_if_block_path = false;
	}

	if (!attr["point"].empty() || !attr["point"].isNull())
		point = attr["point"].asInt();
	else{
		point = 0;
	}

	if (!attr["weapon_id"].empty() || !attr["weapon_id"].isNull())
		weapon_id = attr["weapon_id"].asInt();
	else{
		weapon_id = 0;
	}

	if (!attr["target_range_min"].empty() || !attr["target_range_min"].isNull())
		target_range_min = attr["target_range_min"].asInt();
	else{
		target_range_min = 0;
	}

	if (!attr["target_range_max"].empty() || !attr["target_range_max"].isNull())
		target_range_max = attr["target_range_max"].asInt();
	else{
		target_range_max = 10;
	}

	if (!attr["danger"].empty() || !attr["danger"].isNull())
		danger = attr["danger"].asInt();

	if (!attr["danger"].empty() || !attr["danger"].isNull())
		danger = attr["danger"].asInt();

	if (!attr["order"].empty() || !attr["order"].isNull())
		order = attr["order"].asInt();

	if (!attr["min_hp"].empty() || !attr["min_hp"].isNull())
		min_hp = attr["min_hp"].asInt();

	if (!attr["max_hp"].empty() || !attr["max_hp"].isNull())
		max_hp = attr["max_hp"].asInt();

	if (!attr["distance"].empty() || !attr["distance"].isNull())
		distance = attr["distance"].asInt();

	if (!attr["shooter"].empty() || !attr["shooter"].isNull())
		shooter = attr["shooter"].asBool();

	if (!attr["pathable"].empty() || !attr["pathable"].isNull())
		pathable = attr["pathable"].asBool();

	if (!attr["shotable"].empty() || !attr["shotable"].isNull())
		shotable = attr["shotable"].asBool();

	if (!attr["diagonal"].empty() || !attr["diagonal"].isNull())
		diagonal = attr["diagonal"].asBool();

	if (!attr["loot"].empty() || !attr["loot"].isNull())
		loot = attr["loot"].asBool();
}

std::vector<std::string> HunterManager::get_duplicated_row_name(){
	std::vector <std::string > retval;
	std::vector <std::string > id_to_keep;

	std::function<bool(std::string)> is_id_in_retval = [&](std::string str){
		return std::find(id_to_keep.begin(), id_to_keep.end(), str) != id_to_keep.end();
	};

	std::function<bool(std::string)> is_id_to_keep = [&](std::string str){
		return std::find(id_to_keep.begin(), id_to_keep.end(), str) != id_to_keep.end();
	};

	for (auto it = target_rules.rbegin(); it != target_rules.rend(); it++){
		for (auto it_internal = target_rules.rbegin(); it_internal != target_rules.rend(); it_internal++){

			if (it->first == it_internal->first)
				continue;

			if (is_id_in_retval(it_internal->first) || is_id_to_keep(it_internal->first))
				continue;

			if (_stricmp(&it->second->get_name()[0], &it_internal->second->get_name()[0]) != 0){
				continue;
			}
			id_to_keep.push_back(it->first);
			retval.push_back(it_internal->first);
		}
	}
	return retval;
}