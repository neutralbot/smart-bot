#pragma once
#include "PlayerWaypoint.h"
#include <mutex>

Json::Value NaviPlayerWaypointer::parse_class_to_json(){
	return WaypointManager::get()->parse_class_to_json_navi();
}
void NaviPlayerWaypointer::parse_json_to_class(Json::Value jsonValue){
	((std::mutex*)mtx_access)->lock();
	current_waypoint_coordinate = Coordinate(jsonValue["current_waypoint_coordinate_x"].asInt(), jsonValue["current_waypoint_coordinate_y"].asInt(), jsonValue["current_waypoint_coordinate_z"].asInt());
	current_waypoint_label = jsonValue["current_waypoint_label"].asString();
	current_waypoint_path = jsonValue["current_waypoint_path"].asString();
	current_waypoint_action = (waypoint_t)jsonValue["current_waypoint_action"].asInt();
	((std::mutex*)mtx_access)->unlock();
}

NaviPlayerWaypointer::NaviPlayerWaypointer(){
	mtx_access = (uint32_t*)new std::mutex();
}