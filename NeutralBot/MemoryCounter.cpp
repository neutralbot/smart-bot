#include "MemoryCounter.h"

std::map<std::string, uint64_t > MemoryCounter::class_counts;
neutral_mutex MemoryCounter::mtx_access;

MemoryCounter::MemoryCounter(std::string class_name){
	my_class_name = class_name;
	//mtx_access.lock();
	auto _found = class_counts.find(class_name);
	if (_found == class_counts.end()){
		class_counts[class_name] = 1;
	}
	else{
		_found->second++;
	}

	//mtx_access.unlock();
}
MemoryCounter::~MemoryCounter(){
	//mtx_access.lock();
	auto _found = class_counts.find(my_class_name);
	if (_found == class_counts.end()){
		//MessageBox(0, "error", 0, MB_OK);
	}
	else{
		_found->second--;
	}
	//mtx_access.unlock();
}

uint64_t MemoryCounter::get_counter(std::string class_name){
	if (class_counts[class_name])
		return class_counts[class_name];

	return 0;
}
std::map<std::string, uint64_t> MemoryCounter::get_class_count(){
	//	lock_guard _guard(mtx_access);
	return class_counts;
}