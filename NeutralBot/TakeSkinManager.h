#pragma once
#include "Core\Util.h"

#pragma pack(push,1)
struct TakeSkinStruct{
	uint32_t condition_index;
	uint32_t corpse_id;
	uint32_t item_id;
};

class TakeSkinManager{
	std::map<uint32_t, std::shared_ptr<TakeSkinStruct>> mapTakeSkin;
	bool take_skin_state = false;
	bool priority_looter = false;

public:
	void set_priority_looter(bool in){
		priority_looter = in;
	}
	bool get_priority_looter(){
		return priority_looter;
	}
	void set_take_skin_state(bool in){
		take_skin_state = in;
	}
	bool get_take_skin_state(){
		return take_skin_state;
	}

	bool remove_in_map(uint32_t corporses_id){
		auto it = mapTakeSkin.find(corporses_id);		
		if (it != mapTakeSkin.end()){
			mapTakeSkin.erase(corporses_id);
			return true;
		}
		return false;
	}
	uint32_t request_new_id(){
		uint32_t current_id = 0;

		while (mapTakeSkin.find(current_id) != mapTakeSkin.end())
			current_id++;

		mapTakeSkin[current_id] = std::shared_ptr<TakeSkinStruct>(new TakeSkinStruct);
		mapTakeSkin[current_id]->condition_index = 0;
		mapTakeSkin[current_id]->corpse_id = 0;
		mapTakeSkin[current_id]->item_id = 0;
		return current_id;
	}

	std::map<uint32_t, std::shared_ptr<TakeSkinStruct>> getMapTakeSkin(){
		return mapTakeSkin;
	}
	void change_item_id(uint32_t current_id, uint32_t item_id){
		auto map_find = mapTakeSkin.find(current_id);

		if (map_find == mapTakeSkin.end())
			return;

		mapTakeSkin[current_id]->item_id = item_id;
	}
	void change_corposes_id(uint32_t current_id, uint32_t corporses_id){
		auto map_find = mapTakeSkin.find(current_id);

		if (map_find == mapTakeSkin.end())
			return;

		mapTakeSkin[current_id]->corpse_id = corporses_id;
	}
	void change_condition_index(uint32_t current_id, uint32_t condition_index){
		auto map_find = mapTakeSkin.find(current_id);

		if (map_find == mapTakeSkin.end())
			return;

		mapTakeSkin[current_id]->condition_index = condition_index;
	}

	Json::Value parse_class_to_json() {
		Json::Value takeskinclass;
		Json::Value takeskinList;

		for (auto it : mapTakeSkin){
			Json::Value takeskinItem;

			takeskinItem["condition_index"] = it.second->condition_index;
			takeskinItem["corpse_id"] = it.second->corpse_id;
			takeskinItem["item_id"] = it.second->item_id;

			takeskinList[std::to_string(it.first)] = takeskinItem;
		}

		takeskinclass["takeskinList"] = takeskinList;
		takeskinclass["take_skin_state"] = this->take_skin_state;
		return takeskinclass;
	}

	void parse_json_to_class(Json::Value jsonObject) {
		if (!jsonObject["take_skin_state"].empty() || !jsonObject["take_skin_state"].isNull())
			this->take_skin_state = jsonObject["take_skin_state"].asBool();

		if (!jsonObject["takeskinList"].empty() || !jsonObject["takeskinList"].isNull()){
			Json::Value takeskinList = jsonObject["takeskinList"];

			Json::Value::Members members = takeskinList.getMemberNames();

			for (auto member : members){
				Json::Value takeskinItemJson = takeskinList[member];
				std::shared_ptr<TakeSkinStruct> takeskinItem(new TakeSkinStruct);

				takeskinItem->condition_index = takeskinItemJson["condition_index"].asInt();
				takeskinItem->corpse_id = takeskinItemJson["corpse_id"].asInt();
				takeskinItem->item_id = takeskinItemJson["item_id"].asInt();

				uint32_t temp = boost::lexical_cast<uint32_t>(member);

				mapTakeSkin[temp] = takeskinItem;
			}
		}
	}


	static TakeSkinManager* get(){
		static TakeSkinManager* mGifManager = nullptr;
		if (!mGifManager){
			mGifManager = new TakeSkinManager();
		}
		return mGifManager;
	}
};
#pragma pack(pop)