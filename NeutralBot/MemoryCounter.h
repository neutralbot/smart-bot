#pragma once
#include "Core\Util.h"

class MemoryCounter{
	static neutral_mutex mtx_access;
	static std::map<std::string, uint64_t > class_counts;

	std::string my_class_name;
public:
	MemoryCounter(std::string class_name);
	~MemoryCounter();

	static uint64_t get_counter(std::string class_name);
	static std::map<std::string, uint64_t> get_class_count();
};