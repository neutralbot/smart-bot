#include "Core\AddressManager.h"
#include "Core\LuaCore.h"
#include "Core\Process.h"

AddressManager::AddressManager(){
	load_addresses_from_lua_state();
}

void AddressManager::set_base_address(int address){
	if (address != 0)
		BASE_ADDRESS = address;
}

uint32_t AddressManager::get_address_by_var(std::string varname){
	return LuaCore::getState(0)->get_uin32_t(varname);
}

bool AddressManager::load_address(std::string file_name){
	mtx_access.lock();
	address_map.clear();
	
	if (!LuaCore::getState(0)->LoadLib(".\\conf\\defaults\\" + file_name))
		std::cout << "\n Fail to load address Lib";

	load_addresses_from_lua_state();
	mtx_access.unlock();
	return true;
}

void AddressManager::load_addresses_from_lua_state(){
	address_map[ADDRESS_LOGGED] = get_address_by_var("lookAddressLogged");
	address_map[ADDRESS_CTRL_SHIFT] = get_address_by_var("AddressCtrl");
	address_map[ADDRESS_XOR] = get_address_by_var("XorAddress");
	address_map[ADDRESS_TOTAL_HP] = get_address_by_var("AddressMaxHp");
	address_map[ADDRESS_PLAYER_ID] = get_address_by_var("lookAddressIdChar");
	address_map[ADDRESS_BATTLE_FIRST_ID] = get_address_by_var("lookAddressPrimeiroIdBattle");
	address_map[ADDRESS_CURRENT_HOTKEY] = get_address_by_var("AddressActualHotkeySchema");
	address_map[ADDRESS_HOTKEYS_REFERENCE] = get_address_by_var("AddressSchemaHotkeyReference");
	address_map[ADDRESS_RECEIVE_WINSOCK_BUFFER] = get_address_by_var("AddressReceivBuffer");
	address_map[ADDRESS_STATUS] = get_address_by_var("AddressStatus");
	address_map[ADDRESS_OFFLINE_TRAINING] = get_address_by_var("AddressTreino");
	address_map[ADDRESS_EXPERIENCE] = get_address_by_var("AddressExp");
	address_map[ADDRESS_MAX_MP] = get_address_by_var("AddressMaxMp");
	address_map[ADDRESS_LEVEL] = get_address_by_var("AddressLvl");
	address_map[ADDRESS_SOUL] = BASE_ADDRESS + get_address_by_var("AddressSoul");
	address_map[ADDRESS_MAGIC_LEVEL] = get_address_by_var("AddressMagicLvl");
	address_map[ADDRESS_MAGIC_LEVEL_PERCENT] = get_address_by_var("AddressMagicLvlPc");
	address_map[ADDRESS_TARGET_RED] = get_address_by_var("AddressTargetRed");
	address_map[ADDRESS_TARGET_WHITE] = get_address_by_var("AddressWhiteTarget");
	address_map[ADDRESS_MP] = get_address_by_var("AddressMp");
	address_map[ADDRESS_FIRST_PERCENT] = get_address_by_var("AddressFirstPc");
	address_map[ADDRESS_STAMINA] = get_address_by_var("AddressStamina");
	address_map[ADDRESS_CLUB_PERCENT] = get_address_by_var("AddressClubPc");
	address_map[ADDRESS_SWORD_PERCENT] = get_address_by_var("AddressSwordPc");

	address_map[ADDRESS_AXE_PERCENT] = get_address_by_var("AddressAxePc");
	address_map[ADDRESS_DISTANCE_PERCENT] = get_address_by_var("AddressDistancePc");
	address_map[ADDRESS_SHIELDING_PERCENT] = get_address_by_var("AddressShieldingPc");
	address_map[ADDRESS_FISHING_PERCENT] = get_address_by_var("AddressFishingPc");
	address_map[ADDRESS_OF_CONTAINER] = get_address_by_var("AddressOfBpPointer");
	address_map[ADDRESS_MOUSE1_X] = get_address_by_var("AddressMouseX1");
	address_map[ADDRESS_MOUSE1_Y] = get_address_by_var("AddressMouseY1");
	address_map[ADDRESS_MOUSE_FIX_X] = get_address_by_var("AddressMouse_fix_x");
	address_map[ADDRESS_MOUSE_FIX_Y] = get_address_by_var("AddressMouse_fix_y");
	address_map[ADDRESS_FOLLOW] = get_address_by_var("AddressFollow");
	address_map[ADDRESS_SKULL_MODE_NEW] = get_address_by_var("AddressSkullModeNew");
	address_map[ADDRESS_COLLDOWN_BAR] = get_address_by_var("AddressCooldownBar");
	address_map[ADDRESS_SKULL_MODE_OLD] = get_address_by_var("AddressSkullModeOld");
	address_map[ADDRESS_MOUSE] = get_address_by_var("address_mouse");
	address_map[ADDRESS_WINDOW_X] = get_address_by_var("AddressTelaX");
	address_map[ADDRESS_WINDOW_Y] = get_address_by_var("AddressTelaY");
	address_map[ADDRESS_MOUSE_2] = get_address_by_var("address_mouse_2");
	address_map[ADDRESS_WINDOW_HANDLER] = get_address_by_var("AddressHWND");
	address_map[ADDRESS_WINDOW] = get_address_by_var("AddressOfWindow");
	address_map[ADDRESS_MESSAGE_SERVER] = get_address_by_var("AddressServerMessage");
	address_map[ADDRESS_MESSAGE_NOT_POSSIBLE] = get_address_by_var("AddressMessageNotPossible");
	address_map[ADDRESS_MESSAGE_PLAYER] = get_address_by_var("AddressMessagePlayer");
	address_map[ADDRESS_POINTER_SPELLS] = get_address_by_var("address_pointer_spells");
	address_map[ADDRESS_POINTER_SPELLS_BASIC] = get_address_by_var("address_pointer_spells_basic");
	address_map[ADDRESS_TOTAL_SPELLS] = get_address_by_var("address_total_spells");
	address_map[ADDRESS_EXPERIENCE_HOUR] = get_address_by_var("AddressExpHour");
	address_map[ADDRESS_FIRST_MINIMAP] = get_address_by_var("addressOfFirstMap");
	address_map[ADDRESS_MOUSE2_X] = get_address_by_var("AddressMouseX2");
	address_map[ADDRESS_MOUSE2_Y] = get_address_by_var("AddressMouseY2");
	address_map[ADDRESS_ACCOUNT] = get_address_by_var("AddressAcc");
	address_map[ADDRESS_PASSWORD] = get_address_by_var("AddressPass");
	address_map[ADDRESS_LOGGED_CHARACTER_INDEX] = get_address_by_var("AddressIndexChar");
	address_map[ADDRESS_ITEM_USE] = get_address_by_var("address_item_to_be_used");
	address_map[ADDRESS_ITEM_TO_MOVE] = get_address_by_var("address_item_to_be_moved");
	address_map[ADDRESS_BASE_LOG_LIST] = get_address_by_var("AddressBaseLogList");

	
	address_map[ADDRESS_ATTACK_MODE] = get_address_by_var("ADDRESS_ATTACK_MODE");
	address_map[ADDRESS_POINTER_VIP_PLAYERS] = get_address_by_var("pointer_vip_players");
	address_map[ADDRESS_HP] = get_address_by_var("AddressHp");
	address_map[ADDRESS_Z_GO] = get_address_by_var("AddressZGO");
	address_map[ADDRESS_Y_GO] = get_address_by_var("AddressYGO");
	address_map[ADDRESS_X_GO] = get_address_by_var("AddressXGO");
	address_map[ADDRESS_Z] = get_address_by_var("AddressZ");
	address_map[ADDRESS_Y] = get_address_by_var("AddressY");
	address_map[ADDRESS_X] = get_address_by_var("AddressX");
	address_map[ADDRESS_FIRST_SKILL] = get_address_by_var("AddressFirst");
	address_map[ADDRESS_CAP] = get_address_by_var("AddressCap");
	address_map[ADDRESS_MAX_HP] = get_address_by_var("AddressMaxHp");
	address_map[ADDRESS_BASE_GUI] = get_address_by_var("base_address_in_gui");
	address_map[ADDRESS_LOGIN_BASE] = get_address_by_var("AddressBaseLogList");
	address_map[ADDRESS_HELMET] = get_address_by_var("AddressHelmet");
	address_map[ADDRESS_POINTER_BUFFER_MAP] = get_address_by_var("PointerInicioMap");
	address_map[ADDRESS_POINTER_OFFSET_BUFFER_MAP] = get_address_by_var("PointerInicioOffsetMap");
	address_map[ADDRESS_CONTAINER_BASE_NEW] = get_address_by_var("address_bp_base_new");
	address_map[ADDRESS_TIBIA_TIME] = get_address_by_var("address_tibia_time");
	address_map[ADDRESS_CLUB] = get_address_by_var("AddressClub");
	address_map[ADDRESS_SWORD] = get_address_by_var("AddressSword");
	address_map[ADDRESS_AXE] = get_address_by_var("AddressAxe");
	address_map[ADDRESS_DISTANCE] = get_address_by_var("AddressDistance");
	address_map[ADDRESS_SHIELDING] = get_address_by_var("AddressShielding");
	address_map[ADDRESS_FISHING] = get_address_by_var("AddressFishing");
	address_map[ADDRESS_AMULET] = get_address_by_var("AddressAmulet");
	address_map[ADDRESS_BAG] = get_address_by_var("AddressBag") != 0
		? get_address_by_var("AddressBag") : get_address_by_var("AddressMainBp");

	address_map[ADDRESS_ARMOR] = get_address_by_var("AddressArmor");
	address_map[ADDRESS_SHIELD] = get_address_by_var("AddressShield");
	address_map[ADDRESS_WEAPON] = get_address_by_var("AddressWeapon");
	address_map[ADDRESS_LEG] = get_address_by_var("AddressLeg");
	address_map[ADDRESS_BOOTS] = get_address_by_var("AddressBoot");
	address_map[ADDRESS_RING] = get_address_by_var("AddressRing");
	address_map[ADDRESS_ROPE] = get_address_by_var("AddressRope");
	address_map[ADDRESS_BASE_PING] = get_address_by_var("AddressBasePing");

}

uint32_t AddressManager::getAddress(ADDRESS_TYPE t){
	mtx_access.lock();
	uint32_t retval = address_map[t];
	mtx_access.unlock();
	return retval;
}

//singletoon
AddressManager* AddressManager::get(){
	static AddressManager* m_address_static = nullptr;
	if (!m_address_static)
		m_address_static = new AddressManager();
	return m_address_static;
}

