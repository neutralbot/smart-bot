#pragma once
#include "SellManager.h"
#include "Core\RepoterCore.h"
#include "Core\LooterCore.h"
#include "Core\ItemsManager.h"
#include "Core\Messages.h"
#include "Core\NpcTrade.h"
#include <regex>

class SellCore{
public:
	SellCore();

	static bool open_trade();

	static uint32_t get_count_sell();

	static bool sell_items(std::string sell_item_id);
};
