#include "Core\Util.h"

class SharedMemoryObject{

};

typedef std::shared_ptr<SharedMemoryObject> SharedMemoryObjectPtr;

class SharedMemoryManager{
	SharedMemoryManager();
	~SharedMemoryManager();

	
	SharedMemoryObjectPtr get_shared_from_client_pid(uint32_t pid);

};