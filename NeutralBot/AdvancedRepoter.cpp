#include "AdvancedRepoter.h"
#include "Core\Actions.h"
#include "Core\RepoterCore.h"
#include "Core\DepoterCore.h"
#include "Core\FloatingMiniMenu.h"

using namespace Neutral;

Void AdvancedRepoter::waypointPageAddNewPage(std::string pageid){

	std::string pageName = pageid;

	Telerik::WinControls::UI::RadPageViewPage^ radPageViewPage1 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
	Telerik::WinControls::UI::RadListView^ radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
	Telerik::WinControls::UI::RadDropDownList^ dropDownList = (gcnew Telerik::WinControls::UI::RadDropDownList());
	Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0", L"Item"));
	  
	// radPageViewPage1
	radPageViewPage1->Controls->Add(radListView1);
	radPageViewPage1->ItemSize = System::Drawing::SizeF(39, 28);
	radPageViewPage1->Location = System::Drawing::Point(10, 37);
	radPageViewPage1->Name = "Page_" + gcnew String(pageName.c_str());
	radPageViewPage1->Size = System::Drawing::Size(773, 392);
	radPageViewPage1->Text = gcnew String(pageName.c_str());

	// radListView1
	radListView1->Dock = System::Windows::Forms::DockStyle::Fill;
	radListView1->ShowGridLines = true;
	listViewDetailColumn7->HeaderText = L"Item";
	listViewDetailColumn7->MaxWidth = 297;
	listViewDetailColumn7->MinWidth = 297;
	listViewDetailColumn7->Width = 297;
	radListView1->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(1) { listViewDetailColumn7 });
	radListView1->ItemSpacing = -1;
	radListView1->Location = System::Drawing::Point(0, 29);
	radListView1->Name = "list_" + gcnew String(pageName.c_str());
	radListView1->Size = System::Drawing::Size(372, 201);
	radListView1->TabIndex = 0;
	radListView1->Text = L"";
	radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
	radListView1->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &AdvancedRepoter::ItemListViewItemRemoving);
	radListView1->SelectedItemChanged += gcnew System::EventHandler(this, &AdvancedRepoter::ItemListViewItemChanged);
	radListView1->ListViewElement->ItemSize = System::Drawing::Size(radListView1->ListViewElement->ItemSize.Width, 40);

	PageRepot->Controls->Add(radPageViewPage1);
}
Void AdvancedRepoter::addItemOnView(String^ ContainerName, String^ idBp, String^ Item, int max, int min){
	array<Control^>^ getRadListView = this->PageRepot->SelectedPage->Controls->Find("list_" + PageRepot->SelectedPage->Text, true);
	AdvancedRepoterManager::get()->requestNewRepotIdById(managed_util::fromSS(idBp));
	std::shared_ptr<AdvancedRepoterId> Itens = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(idBp));

	std::string idItem = Itens->addRepotItem(managed_util::fromSS(Item), min, max);

	if (getRadListView->Length < 0)
		return;

	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	array<String^>^ columnArrayItems = { Item, Convert::ToString(min), Convert::ToString(max) };

	Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(idItem.c_str()), columnArrayItems);
	radListView->Items->AddRange(newItem);
	newItem->Image = managed_util::get_item_image(Item, 30);
	radListView->SelectedIndex = radListView->Items->Count - 1;
}
Void AdvancedRepoter::loadItemOnView(std::string id_){
	array<Control^>^ getRadListView = Controls->Find("list_" + gcnew String(id_.c_str()), true);
	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];

	std::map<std::string, std::shared_ptr<AdvancedRepoterId>> mapRepotId = AdvancedRepoterManager::get()->getMapRepoId();
	std::map<std::string, std::shared_ptr<AdvancedRepoterItens>> mapRepotItens = mapRepotId[id_]->getMapRepoItens();
	
	radListView->BeginUpdate();
	for (auto i = mapRepotItens.begin(); i != mapRepotItens.end(); i++){
		array<String^>^ columnArrayItems = { gcnew String(i->second->get_item().c_str()), Convert::ToString(i->second->get_min()), Convert::ToString(i->second->get_max()) };
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(i->first.c_str()), columnArrayItems);

		radListView->Items->Add(newItem);
		newItem->Image = managed_util::get_item_image(gcnew String(i->first.c_str()), 30);
	}
	radListView->EndUpdate();
}
Void AdvancedRepoter::loadAll(){
	std::map<std::string, std::shared_ptr<AdvancedRepoterId>> map = AdvancedRepoterManager::get()->getMapRepoId();
	for (auto it = map.begin(); it != map.end(); it++){
		waypointPageAddNewPage(it->first);
		loadItemOnView(it->first);
	}
}
Void AdvancedRepoter::updateSelectedData(){
	if (!PageRepot->SelectedPage)
		return;

	Telerik::WinControls::UI::RadListView^ radlist = getItemListView();

	if (!radlist->SelectedItem){
		radGroupBox1->Enabled = false;
		return;
	}

	radGroupBox1->Enabled = true;

	std::shared_ptr<AdvancedRepoterId> mapId = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
	auto mapItens = mapId->getMapRepoItens();

	std::string idMap = managed_util::fromSS(radlist->SelectedItem->Text);

	String^ ItemName = gcnew String(mapItens[idMap]->get_item().c_str());
	uint32_t Min = mapItens[idMap]->get_min();
	uint32_t Max = mapItens[idMap]->get_max();

	disable_item_update = true;
	box_itemname->Text = ItemName;
	box_min->Value = Min;
	box_max->Value = Max;
	disable_item_update = false;
}

Telerik::WinControls::UI::RadListView^  AdvancedRepoter::getItemListView(){
	auto selected_page = this->PageRepot->SelectedPage;

	if (!selected_page)
		return nullptr;

	array<Control^>^ getRadListView = selected_page->Controls->Find("list_" + PageRepot->SelectedPage->Text, true);

	if (!getRadListView->Length)
		return nullptr;

	return (Telerik::WinControls::UI::RadListView^) getRadListView[0];
}

System::Void AdvancedRepoter::ItemListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e){
	array<Control^>^ getRadListView = this->PageRepot->SelectedPage->Controls->Find("list_" + PageRepot->SelectedPage->Text, true);
	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;
	auto mapRepotId = AdvancedRepoterManager::get()->getMapRepoId();

	if (itemRow)
		mapRepotId[managed_util::fromSS(PageRepot->SelectedPage->Text)]->removeRepotItem(managed_util::fromSS(itemRow->Text));

	disable_item_update = true;
	if (radListView->Items->Count <= 1)
		radGroupBox1->Enabled = false;
	disable_item_update = false;
}
System::Void AdvancedRepoter::ItemListViewItemChanged(System::Object^  sender, System::EventArgs^  e){
	updateSelectedData();
}

System::Void AdvancedRepoter::PageRepot_NewPageRequested(System::Object^  sender, System::EventArgs^  e) {
	waypointPageAddNewPage(AdvancedRepoterManager::get()->requestNewRepotId());
	(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
}
System::Void AdvancedRepoter::PageRepot_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e) {
	AdvancedRepoterManager::get()->removeRepotId(managed_util::fromSS(e->Page->Text));

	close_button();
}

System::Void AdvancedRepoter::bt_add_Click_1(System::Object^  sender, System::EventArgs^  e) {
	if (!PageRepot->SelectedPage)
		return;

	String^ item = "new";

	int max = 0;
	int min = 0;

	if (item == String::Empty)
		return;

	if (!PageRepot->SelectedPage)
		return;

	addItemOnView(box_itemname->Text, PageRepot->SelectedPage->Text, item, max, min);
}
System::Void AdvancedRepoter::bt_delete_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!PageRepot->SelectedPage)
		return;

	array<Control^>^ getRadListView = this->PageRepot->SelectedPage->Controls->Find("list_" + PageRepot->SelectedPage->Text, true);
	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;
	auto mapRepotId = AdvancedRepoterManager::get()->getMapRepoId();

	if (!itemRow)
		return;

	radListView->Items->Remove(itemRow);
	mapRepotId[managed_util::fromSS(PageRepot->SelectedPage->Text)]->removeRepotItem(managed_util::fromSS(itemRow->Text));

	if (radListView->Items->Count <= 0)
		radGroupBox1->Enabled = false;
}

System::Void AdvancedRepoter::AdvancedRepoter_Load(System::Object^  sender, System::EventArgs^  e) {
	std::vector<std::string> arrayitems = ItemsManager::get()->get_itens();
	box_itemname->BeginUpdate();
	for (auto item : arrayitems)
		box_itemname->Items->Add(gcnew String(item.c_str()));
	box_itemname->EndUpdate();

	std::vector<std::string> arraycontainer = ItemsManager::get()->get_containers();
	box_charactercontainer->BeginUpdate();
	for (auto item : arraycontainer)
		box_charactercontainer->Items->Add(gcnew String(item.c_str()));
	box_charactercontainer->EndUpdate();
		
	PageRepot->ViewElement->AllowEdit = true;
	PageRepot->ViewElement->EditorInitialized += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEditorEventArgs^>(this, &AdvancedRepoter::Page_EditorInitialized);

	loadAll();
	update_idiom();

	close_button();
	radGroupBox1->Enabled = false;
}

System::Void AdvancedRepoter::Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e){
	this->PageRepot->SelectedPage->TextChanged += gcnew System::EventHandler(this, &AdvancedRepoter::PageRename_TextChanged);
	text = e->Value->ToString();
}
System::Void AdvancedRepoter::PageRename_TextChanged(System::Object^  sender, System::EventArgs^ e){
	String^ newValue = PageRepot->SelectedPage->Text;
	String^ oldValue = text;

	if (newValue == "" || oldValue == "")
		return;

	if (newValue == oldValue)
		return;

	int count = 0;
	for each(auto items in PageRepot->Pages){
		if (items->Text == newValue){
			if (count == 0)
				count++;
			else{
				PageRepot->SelectedPage->Text = oldValue;
				return;
			}
		}
	}


	std::string newName = managed_util::to_std_string(newValue);
	std::string oldName = managed_util::to_std_string(oldValue);

	if (!AdvancedRepoterManager::get()->changeRepotId(newName, oldName)){
		PageRepot->SelectedPage->Text = oldValue;
		return;
	}

	text = newValue;

	array<Control^>^ getRadListView = PageRepot->SelectedPage->Controls->Find("list_" + oldValue, true);

	if (getRadListView->Length == 0)
		return;

	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	radListView->Name = "list_" + newValue;
}
System::Void AdvancedRepoter::PageRepot_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
	if (clean)
		return;

	std::shared_ptr<AdvancedRepoterId> Itens = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));

	box_charactercontainer->Text = gcnew String(Itens->get_characterBackpack().c_str());
	uint32_t item_id = Itens->depotBoxId;

	box_chest->Text = gcnew String(ItemsManager::get()->getItemNameFromId(item_id).c_str());
	
	auto listview = getItemListView();

	if (!listview)
		return;

	if (listview->Items->Count <= 0)
		radGroupBox1->Enabled = false;
	else
		radGroupBox1->Enabled = true;

	box_itemname_SelectedIndexChanged(nullptr, nullptr);
	box_charactercontainer_SelectedIndexChanged(nullptr, nullptr);
}

System::Void AdvancedRepoter::box_itemname_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	//pictureBoxItem
	if (disable_item_update)
		return;

	Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

	if (!radListview)
		return;

	if (!radListview->SelectedItem)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ radItem = (Telerik::WinControls::UI::ListViewDataItem^)radListview->SelectedItem;
	radListview->SelectedItem[0] = box_itemname->Text;
	radListview->SelectedItem->Image = managed_util::get_item_image(box_itemname->Text, 25);

	std::shared_ptr<AdvancedRepoterId> srd_bp = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
	srd_bp->set_Item(managed_util::fromSS(radListview->SelectedItem->Text), managed_util::fromSS(box_itemname->Text));

	this->pictureBoxItem->Image = managed_util::get_item_image(box_itemname->Text, this->pictureBoxItem->Height);
}
System::Void AdvancedRepoter::box_min_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;

	Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

	if (!radListview)
		return;

	if (!radListview->SelectedItem)
		return;

	std::shared_ptr<AdvancedRepoterId> srd_bp = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
	srd_bp->set_Min(managed_util::fromSS(radListview->SelectedItem->Text), Convert::ToInt32(box_min->Value));
}
System::Void AdvancedRepoter::box_max_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;

	Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

	if (!radListview)
		return;

	if (!radListview->SelectedItem)
		return;

	std::shared_ptr<AdvancedRepoterId> srd_bp = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
	srd_bp->set_Max(managed_util::fromSS(radListview->SelectedItem->Text), Convert::ToInt32(box_max->Value));
}