#pragma once
#include "Panorama.h"
#include "Core\Neutral.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class SelectedCharacter : public Telerik::WinControls::UI::RadForm{
	
	public:	SelectedCharacter(int pid){
			InitializeComponent();
			update_idiom();

			/*if (list_character->Items->Count <= 0)
				return;

			this->WindowState = FormWindowState::Normal;*/
		}
	protected:	~SelectedCharacter(){
					unique = nullptr;
					if (components)
						delete components;
	}

	protected:		
	public: static bool temp_close;

	public: static SelectedCharacter^ unique;
	public: static void ShowUnique(int pid){
				if (unique == nullptr)
					unique = gcnew SelectedCharacter(pid);

				temp_close = false;
				unique->ShowDialog();
				unique->Close();
	}
	public: static void CloseUnique(){
				if (unique)
					unique->Close();
	}

	private: Telerik::WinControls::UI::RadListView^  list_character;
	protected:
	protected:
	private: Telerik::WinControls::UI::RadButton^  radButton1;
	private: System::Windows::Forms::Timer^  RefreshTibiaOpen;
	private: System::ComponentModel::IContainer^  components;



#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 this->components = (gcnew System::ComponentModel::Container());
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0",
					 L"Version"));
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"CharacterName",
					 L"Character Name"));
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Pid",
					 L"Pid"));
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(SelectedCharacter::typeid));
				 this->list_character = (gcnew Telerik::WinControls::UI::RadListView());
				 this->radButton1 = (gcnew Telerik::WinControls::UI::RadButton());
				 this->RefreshTibiaOpen = (gcnew System::Windows::Forms::Timer(this->components));
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->list_character))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // list_character
				 // 
				 this->list_character->AllowEdit = false;
				 listViewDetailColumn1->HeaderText = L"Version";
				 listViewDetailColumn1->MaxWidth = 60;
				 listViewDetailColumn1->MinWidth = 60;
				 listViewDetailColumn1->Width = 60;
				 listViewDetailColumn2->HeaderText = L"Character Name";
				 listViewDetailColumn2->MaxWidth = 230;
				 listViewDetailColumn2->MinWidth = 230;
				 listViewDetailColumn2->Width = 230;
				 listViewDetailColumn3->HeaderText = L"Pid";
				 listViewDetailColumn3->MinWidth = 0;
				 listViewDetailColumn3->Width = 0;
				 this->list_character->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(3) {
					 listViewDetailColumn1,
						 listViewDetailColumn2, listViewDetailColumn3
				 });
				 this->list_character->ItemSpacing = -1;
				 this->list_character->Location = System::Drawing::Point(8, 8);
				 this->list_character->Name = L"list_character";
				 this->list_character->Size = System::Drawing::Size(291, 172);
				 this->list_character->TabIndex = 0;
				 this->list_character->Text = L"radListView1";
				 this->list_character->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				 this->list_character->ItemMouseDoubleClick += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &SelectedCharacter::list_character_ItemMouseDoubleClick);
				 this->list_character->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &SelectedCharacter::list_character_KeyPress);
				 // 
				 // radButton1
				 // 
				 this->radButton1->Location = System::Drawing::Point(189, 186);
				 this->radButton1->Name = L"radButton1";
				 this->radButton1->Size = System::Drawing::Size(110, 24);
				 this->radButton1->TabIndex = 1;
				 this->radButton1->Text = L"Launch SmartBot";
				 this->radButton1->Click += gcnew System::EventHandler(this, &SelectedCharacter::radButton1_Click);
				 // 
				 // RefreshTibiaOpen
				 // 
				 this->RefreshTibiaOpen->Enabled = true;
				 this->RefreshTibiaOpen->Interval = 500;
				 this->RefreshTibiaOpen->Tick += gcnew System::EventHandler(this, &SelectedCharacter::RefreshTibiaOpen_Tick);
				 // 
				 // SelectedCharacter
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(306, 213);
				 this->Controls->Add(this->radButton1);
				 this->Controls->Add(this->list_character);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->MaximizeBox = false;
				 this->Name = L"SelectedCharacter";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"SmartBot";
				 this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &SelectedCharacter::SelectedCharacter_FormClosing);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->list_character))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion



			 void update_idiom(){
				 radButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton1->Text))[0]);

				 for each(auto colum in list_character->Columns)
					 colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
			 }

	private: System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!list_character->SelectedItem)
			return;

		SetCurrent();

		temp_close = true;

		this->Close();
	}
	private: System::Void RefreshTibiaOpen_Tick(System::Object^  sender, System::EventArgs^  e) {
				 if (temp_close)
					 return;

				 Process::get()->RefresherTibiaClients();

				 std::map<uint32_t, TibiaProcessInfo> processes = Process::get()->GetProcessesMap();
				 if (processes.size() <= 0){
					 list_character->Items->Clear();
					 return;
				 }

				 System::Collections::Generic::LinkedList<Telerik::WinControls::UI::ListViewDataItem^> items_remove;
				 for (auto process : processes){
					 uint32_t version = process.second.version;
					 if (version == 0){
						 list_character->Items->Clear();
						 continue;
					 }

					 bool continue_for = false;

					 String^ namechar = (process.second.player_name != "") ? gcnew String(process.second.player_name.c_str()) : "Offline";
					 array<String^>^ arrayOffline = { Convert::ToString(version), namechar, Convert::ToString(process.second.pid) };
					 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(process.second.pid), arrayOffline);

					 for each(auto item in list_character->Items){
						 item->SubItems[1] = gcnew String("");
						 if (Convert::ToInt32(item->SubItems[2]) == (int)process.second.pid){
							 continue_for = true;
							 break;
						 }
					 }

					 if (continue_for)
						 continue;

					 
					 list_character->Items->Add(newItem);
				 }

				
				 for each(auto item in list_character->Items){
					 int temp_pid = Convert::ToInt32(item->SubItems[2]);

					 if (processes.find(temp_pid) == processes.end())
						 items_remove.AddLast(item);
					 else{
						 auto it = processes.find(temp_pid);
						 String^ temp_name = (processes[temp_pid].player_name != "") ? gcnew String(processes[temp_pid].player_name.c_str()) : "Offline";
						 item["CharacterName"] = temp_name;
					 }
				 }

				 for each(auto item in items_remove)
					 list_character->Items->Remove(item);

	}
	private: System::Void SelectedCharacter_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 if (temp_close)
					 return;
				 
				 if (Telerik::WinControls::RadMessageBox::Show(this, "Deseja realmente fechar o sistema? ", "Smart Bot", MessageBoxButtons::YesNo, Telerik::WinControls::RadMessageIcon::Question) == System::Windows::Forms::DialogResult::Yes)
					 NeutralManager::get()->close();
				 else
					 e->Cancel = true;
	}
	private: System::Void list_character_ItemMouseDoubleClick(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
				 if (!list_character->SelectedItems)
					 return;

				 SetCurrent();

				 temp_close = true;
				 this->Close();
	}
	private: System::Void list_character_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
				 if (e->KeyChar != Convert::ToChar(Keys::Return))
					 return;

				 if (!list_character->SelectedItems)
					 return;

				 SetCurrent();

				 Sleep(50);

				 temp_close = true;
				 this->Close();
	}

			 void SetCurrent(){
				 int temp_pid = Convert::ToInt32(list_character->SelectedItem["Pid"]);
				  Sleep(50);
				 TibiaProcess::get_default()->set_pid(temp_pid);
			 }
	};
}
