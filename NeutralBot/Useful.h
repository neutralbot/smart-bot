#pragma once
#include "ManagedUtil.h"
#include "UtilityCore.h"
#include "LuaBackgroundManager.h"
#include "Core\Login.h"
#include "Core\ItemsManager.h"
#include "LanguageManager.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class Useful : public Telerik::WinControls::UI::RadForm{

	public:	Useful(void){
				InitializeComponent();
				NeutralBot::FastUIPanelController::get()->install_controller(this);
	}
	protected: ~Useful(){
				   unique = nullptr;
				   if (components)
					   delete components;
	}

	public: static void CloseUnique(){
				if (unique)unique->Close();
	}
	public: static void ReloadForm(){
				unique->Useful_Load(nullptr, nullptr);
	}
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox6;
	public:
	private: Telerik::WinControls::UI::RadLabel^  radLabel13;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_rune_maker_time;
	private: Telerik::WinControls::UI::RadLabel^  radLabel12;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_rune_maker_soul;
	private: Telerik::WinControls::UI::RadLabel^  radLabel9;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_rune_maker;
	private: Telerik::WinControls::UI::RadLabel^  radLabel11;
	private: Telerik::WinControls::UI::RadTextBox^  text_utility_rune_maker;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_rune_maker;

	private: Telerik::WinControls::UI::RadCheckBox^  utility_auto_fishing;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_auto_sio;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox7;


	private: Telerik::WinControls::UI::RadLabel^  radLabel15;
	private: Telerik::WinControls::UI::RadTextBox^  text_utility_auto_sio;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_auto_utura;

	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox8;
	private: Telerik::WinControls::UI::RadLabel^  radLabel16;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_auto_utura;

	private: Telerik::WinControls::UI::RadLabel^  radLabel17;
	private: Telerik::WinControls::UI::RadTextBox^  text_utility_auto_utura;
	private: Telerik::WinControls::UI::RadLabel^  radLabel19;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_auto_sio;

	private: Telerik::WinControls::UI::RadLabel^  radLabel18;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_auto_utura_;

	private: Telerik::WinControls::UI::RadCheckBox^  check_autorelogin;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox9;
	private: Telerik::WinControls::UI::RadTextBox^  box_account;

	private: Telerik::WinControls::UI::RadTextBox^  box_password;
	private: Telerik::WinControls::UI::RadLabel^  radLabel22;
	private: Telerik::WinControls::UI::RadLabel^  radLabel21;
	private: Telerik::WinControls::UI::RadLabel^  radLabel20;
	private: Telerik::WinControls::UI::RadSpinEditor^  box_index;
	private: Telerik::WinControls::UI::RadListView^  ListViewFriend;

	private: Telerik::WinControls::UI::RadButton^  ButtonRemove;
	private: Telerik::WinControls::UI::RadButton^  ButtonNew;


	private: Telerik::WinControls::UI::RadLabel^  radLabel14;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_auto_sio;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox10;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_soft_change;

	private: Telerik::WinControls::UI::RadLabel^  radLabel23;

	private: Telerik::WinControls::UI::RadLabel^  radLabel25;
	private: Telerik::WinControls::UI::RadLabel^  radLabel24;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_soft_change_;

	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_soft_change;
	private: Telerik::WinControls::UI::RadDropDownList^  text_utility_soft_change;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListFoodName;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_auto_mount;






	protected: static Useful^ unique;

	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew Useful();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();

				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Useful();
	}

	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	protected:
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_anti_paralyze;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: Telerik::WinControls::UI::RadTextBox^  text_utility_anti_paralyze;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_auto_haste;
	private: Telerik::WinControls::UI::RadLabel^  radLabel4;
	private: Telerik::WinControls::UI::RadTextBox^  text_utility_auto_haste;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_anti_paralyze;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_auto_haste;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_auto_mana_shield;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox3;
	private: Telerik::WinControls::UI::RadLabel^  radLabel5;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_auto_mana_shield;
	private: Telerik::WinControls::UI::RadLabel^  radLabel6;
	private: Telerik::WinControls::UI::RadTextBox^  text_utility_auto_mana_shield;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox4;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_anti_afk;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_drop_vial;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_screenshot;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_eat_food;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_eat_food;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_refil_amun;
	private: Telerik::WinControls::UI::RadDropDownList^  box_utility_refil_amun;
	private: Telerik::WinControls::UI::RadLabel^  radLabel10;
	private: Telerik::WinControls::UI::RadDropDownList^  box_utility_auto_ring;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_auto_ring;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_utility_anti_poison;
	private: Telerik::WinControls::UI::RadTextBox^  text_utility_anti_poison;
	private: Telerik::WinControls::UI::RadCheckBox^  utility_anti_poison;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox5;
	private: Telerik::WinControls::UI::RadLabel^  radLabel7;
	private: Telerik::WinControls::UI::RadLabel^  radLabel8;
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void){
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem5 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem6 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem7 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem8 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem9 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem10 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem11 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem12 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem13 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem14 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem15 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem16 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem17 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem18 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem19 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem20 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem21 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem22 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem23 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem24 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem25 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem26 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem27 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem28 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem29 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem30 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem31 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem32 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem33 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem34 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Name",
					 L"Name"));
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Hp",
					 L"Hp"));
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem35 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem36 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem37 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem38 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem39 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem40 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem41 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem42 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem43 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem44 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem45 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem46 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem47 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem48 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem49 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem50 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem51 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem52 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem53 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem54 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Useful::typeid));
				 this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_anti_paralyze = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->text_utility_anti_paralyze = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_auto_haste = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->text_utility_auto_haste = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->utility_anti_paralyze = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_auto_haste = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_auto_mana_shield = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->radGroupBox3 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_auto_mana_shield = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel6 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->text_utility_auto_mana_shield = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->radGroupBox4 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->utility_auto_mount = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->DropDownListFoodName = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->utility_auto_fishing = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->box_utility_auto_ring = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->utility_auto_ring = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_screenshot = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_drop_vial = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_refil_amun = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_anti_afk = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->box_utility_refil_amun = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel10 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_eat_food = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->utility_eat_food = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_anti_poison = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->radGroupBox5 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel7 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_anti_poison = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel8 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->text_utility_anti_poison = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->radGroupBox6 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel13 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_rune_maker_time = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel12 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_rune_maker_soul = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel9 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_rune_maker = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel11 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->text_utility_rune_maker = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->utility_rune_maker = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->utility_auto_sio = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->radGroupBox7 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->ButtonRemove = (gcnew Telerik::WinControls::UI::RadButton());
				 this->ButtonNew = (gcnew Telerik::WinControls::UI::RadButton());
				 this->ListViewFriend = (gcnew Telerik::WinControls::UI::RadListView());
				 this->radLabel14 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_auto_sio = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel19 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_auto_sio = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel15 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->text_utility_auto_sio = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->utility_auto_utura = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->radGroupBox8 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel18 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel16 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_auto_utura = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel17 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_auto_utura_ = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->text_utility_auto_utura = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->check_autorelogin = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 this->radGroupBox9 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel22 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel21 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel20 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->box_index = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->box_account = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->box_password = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->radGroupBox10 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->text_utility_soft_change = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel25 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel24 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_utility_soft_change_ = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->spin_utility_soft_change = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel23 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->utility_soft_change = (gcnew Telerik::WinControls::UI::RadCheckBox());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				 this->radGroupBox1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_anti_paralyze))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_anti_paralyze))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
				 this->radGroupBox2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_haste))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_haste))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_anti_paralyze))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_haste))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_mana_shield))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->BeginInit();
				 this->radGroupBox3->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_mana_shield))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_mana_shield))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->BeginInit();
				 this->radGroupBox4->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_mount))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListFoodName))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_fishing))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_utility_auto_ring))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_ring))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_screenshot))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_drop_vial))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_refil_amun))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_anti_afk))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_utility_refil_amun))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel10))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_eat_food))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_eat_food))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_anti_poison))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox5))->BeginInit();
				 this->radGroupBox5->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_anti_poison))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_anti_poison))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox6))->BeginInit();
				 this->radGroupBox6->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_rune_maker_time))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_rune_maker_soul))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_rune_maker))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel11))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_rune_maker))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_rune_maker))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_sio))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox7))->BeginInit();
				 this->radGroupBox7->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemove))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonNew))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewFriend))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel14))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_auto_sio))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel19))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_sio))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel15))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_sio))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_utura))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox8))->BeginInit();
				 this->radGroupBox8->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel18))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel16))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_utura))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel17))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_utura_))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_utura))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_autorelogin))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox9))->BeginInit();
				 this->radGroupBox9->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel22))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel21))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel20))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_index))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_account))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_password))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox10))->BeginInit();
				 this->radGroupBox10->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_soft_change))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel25))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel24))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_soft_change_))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_soft_change))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel23))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_soft_change))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // radGroupBox1
				 // 
				 this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox1->Controls->Add(this->radLabel2);
				 this->radGroupBox1->Controls->Add(this->spin_utility_anti_paralyze);
				 this->radGroupBox1->Controls->Add(this->radLabel1);
				 this->radGroupBox1->Controls->Add(this->text_utility_anti_paralyze);
				 this->radGroupBox1->HeaderText = L"";
				 this->radGroupBox1->Location = System::Drawing::Point(2, 11);
				 this->radGroupBox1->Name = L"radGroupBox1";
				 this->radGroupBox1->Size = System::Drawing::Size(180, 80);
				 this->radGroupBox1->TabIndex = 0;
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(9, 47);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(34, 18);
				 this->radLabel2->TabIndex = 2;
				 this->radLabel2->Text = L"Mana";
				 // 
				 // spin_utility_anti_paralyze
				 // 
				 this->spin_utility_anti_paralyze->Location = System::Drawing::Point(49, 46);
				 this->spin_utility_anti_paralyze->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_anti_paralyze->Name = L"spin_utility_anti_paralyze";
				 this->spin_utility_anti_paralyze->Size = System::Drawing::Size(69, 20);
				 this->spin_utility_anti_paralyze->TabIndex = 2;
				 this->spin_utility_anti_paralyze->TabStop = false;
				 this->spin_utility_anti_paralyze->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_mana_antiparalyze_ValueChanged);
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(10, 21);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(33, 18);
				 this->radLabel1->TabIndex = 1;
				 this->radLabel1->Text = L"Spell:";
				 // 
				 // text_utility_anti_paralyze
				 // 
				 this->text_utility_anti_paralyze->Location = System::Drawing::Point(49, 20);
				 this->text_utility_anti_paralyze->Name = L"text_utility_anti_paralyze";
				 this->text_utility_anti_paralyze->Size = System::Drawing::Size(126, 20);
				 this->text_utility_anti_paralyze->TabIndex = 0;
				 this->text_utility_anti_paralyze->TextChanged += gcnew System::EventHandler(this, &Useful::text_spell_antiparalyze_TextChanged);
				 // 
				 // radGroupBox2
				 // 
				 this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox2->Controls->Add(this->radLabel3);
				 this->radGroupBox2->Controls->Add(this->spin_utility_auto_haste);
				 this->radGroupBox2->Controls->Add(this->radLabel4);
				 this->radGroupBox2->Controls->Add(this->text_utility_auto_haste);
				 this->radGroupBox2->HeaderText = L"";
				 this->radGroupBox2->Location = System::Drawing::Point(188, 120);
				 this->radGroupBox2->Name = L"radGroupBox2";
				 this->radGroupBox2->Size = System::Drawing::Size(180, 80);
				 this->radGroupBox2->TabIndex = 3;
				 // 
				 // radLabel3
				 // 
				 this->radLabel3->Location = System::Drawing::Point(9, 47);
				 this->radLabel3->Name = L"radLabel3";
				 this->radLabel3->Size = System::Drawing::Size(34, 18);
				 this->radLabel3->TabIndex = 2;
				 this->radLabel3->Text = L"Mana";
				 // 
				 // spin_utility_auto_haste
				 // 
				 this->spin_utility_auto_haste->Location = System::Drawing::Point(49, 46);
				 this->spin_utility_auto_haste->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_auto_haste->Name = L"spin_utility_auto_haste";
				 this->spin_utility_auto_haste->Size = System::Drawing::Size(69, 20);
				 this->spin_utility_auto_haste->TabIndex = 2;
				 this->spin_utility_auto_haste->TabStop = false;
				 this->spin_utility_auto_haste->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_mana_autohaste_ValueChanged);
				 // 
				 // radLabel4
				 // 
				 this->radLabel4->Location = System::Drawing::Point(10, 21);
				 this->radLabel4->Name = L"radLabel4";
				 this->radLabel4->Size = System::Drawing::Size(33, 18);
				 this->radLabel4->TabIndex = 1;
				 this->radLabel4->Text = L"Spell:";
				 // 
				 // text_utility_auto_haste
				 // 
				 this->text_utility_auto_haste->Location = System::Drawing::Point(49, 20);
				 this->text_utility_auto_haste->Name = L"text_utility_auto_haste";
				 this->text_utility_auto_haste->Size = System::Drawing::Size(126, 20);
				 this->text_utility_auto_haste->TabIndex = 0;
				 this->text_utility_auto_haste->TextChanged += gcnew System::EventHandler(this, &Useful::text_spell_autohaste_TextChanged);
				 // 
				 // utility_anti_paralyze
				 // 
				 this->utility_anti_paralyze->Location = System::Drawing::Point(17, 6);
				 this->utility_anti_paralyze->Name = L"utility_anti_paralyze";
				 this->utility_anti_paralyze->Size = System::Drawing::Size(86, 18);
				 this->utility_anti_paralyze->TabIndex = 4;
				 this->utility_anti_paralyze->Text = L"Anti-Paralyze";
				 this->utility_anti_paralyze->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_antiparalyze_ToggleStateChanged);
				 // 
				 // utility_auto_haste
				 // 
				 this->utility_auto_haste->Location = System::Drawing::Point(203, 115);
				 this->utility_auto_haste->Name = L"utility_auto_haste";
				 this->utility_auto_haste->Size = System::Drawing::Size(77, 18);
				 this->utility_auto_haste->TabIndex = 5;
				 this->utility_auto_haste->Text = L"Auto-Haste";
				 this->utility_auto_haste->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_autohaste_ToggleStateChanged);
				 // 
				 // utility_auto_mana_shield
				 // 
				 this->utility_auto_mana_shield->Location = System::Drawing::Point(17, 92);
				 this->utility_auto_mana_shield->Name = L"utility_auto_mana_shield";
				 this->utility_auto_mana_shield->Size = System::Drawing::Size(111, 18);
				 this->utility_auto_mana_shield->TabIndex = 7;
				 this->utility_auto_mana_shield->Text = L"Auto-Mana Shield";
				 this->utility_auto_mana_shield->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_automana_ToggleStateChanged);
				 // 
				 // radGroupBox3
				 // 
				 this->radGroupBox3->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox3->Controls->Add(this->radLabel5);
				 this->radGroupBox3->Controls->Add(this->spin_utility_auto_mana_shield);
				 this->radGroupBox3->Controls->Add(this->radLabel6);
				 this->radGroupBox3->Controls->Add(this->text_utility_auto_mana_shield);
				 this->radGroupBox3->HeaderText = L"";
				 this->radGroupBox3->Location = System::Drawing::Point(2, 97);
				 this->radGroupBox3->Name = L"radGroupBox3";
				 this->radGroupBox3->Size = System::Drawing::Size(180, 80);
				 this->radGroupBox3->TabIndex = 6;
				 // 
				 // radLabel5
				 // 
				 this->radLabel5->Location = System::Drawing::Point(9, 47);
				 this->radLabel5->Name = L"radLabel5";
				 this->radLabel5->Size = System::Drawing::Size(34, 18);
				 this->radLabel5->TabIndex = 2;
				 this->radLabel5->Text = L"Mana";
				 // 
				 // spin_utility_auto_mana_shield
				 // 
				 this->spin_utility_auto_mana_shield->Location = System::Drawing::Point(49, 46);
				 this->spin_utility_auto_mana_shield->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_auto_mana_shield->Name = L"spin_utility_auto_mana_shield";
				 this->spin_utility_auto_mana_shield->Size = System::Drawing::Size(69, 20);
				 this->spin_utility_auto_mana_shield->TabIndex = 2;
				 this->spin_utility_auto_mana_shield->TabStop = false;
				 this->spin_utility_auto_mana_shield->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_mana_automana_ValueChanged);
				 // 
				 // radLabel6
				 // 
				 this->radLabel6->Location = System::Drawing::Point(10, 21);
				 this->radLabel6->Name = L"radLabel6";
				 this->radLabel6->Size = System::Drawing::Size(33, 18);
				 this->radLabel6->TabIndex = 1;
				 this->radLabel6->Text = L"Spell:";
				 // 
				 // text_utility_auto_mana_shield
				 // 
				 this->text_utility_auto_mana_shield->Location = System::Drawing::Point(49, 20);
				 this->text_utility_auto_mana_shield->Name = L"text_utility_auto_mana_shield";
				 this->text_utility_auto_mana_shield->Size = System::Drawing::Size(126, 20);
				 this->text_utility_auto_mana_shield->TabIndex = 0;
				 this->text_utility_auto_mana_shield->TextChanged += gcnew System::EventHandler(this, &Useful::text_spell_automana_TextChanged);
				 // 
				 // radGroupBox4
				 // 
				 this->radGroupBox4->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox4->Controls->Add(this->utility_auto_mount);
				 this->radGroupBox4->Controls->Add(this->DropDownListFoodName);
				 this->radGroupBox4->Controls->Add(this->utility_auto_fishing);
				 this->radGroupBox4->Controls->Add(this->box_utility_auto_ring);
				 this->radGroupBox4->Controls->Add(this->utility_auto_ring);
				 this->radGroupBox4->Controls->Add(this->utility_screenshot);
				 this->radGroupBox4->Controls->Add(this->utility_drop_vial);
				 this->radGroupBox4->Controls->Add(this->utility_refil_amun);
				 this->radGroupBox4->Controls->Add(this->utility_anti_afk);
				 this->radGroupBox4->Controls->Add(this->box_utility_refil_amun);
				 this->radGroupBox4->Controls->Add(this->radLabel10);
				 this->radGroupBox4->Controls->Add(this->spin_utility_eat_food);
				 this->radGroupBox4->Controls->Add(this->utility_eat_food);
				 this->radGroupBox4->HeaderText = L"General";
				 this->radGroupBox4->Location = System::Drawing::Point(188, 4);
				 this->radGroupBox4->Name = L"radGroupBox4";
				 this->radGroupBox4->Size = System::Drawing::Size(366, 110);
				 this->radGroupBox4->TabIndex = 7;
				 this->radGroupBox4->Text = L"General";
				 // 
				 // utility_auto_mount
				 // 
				 this->utility_auto_mount->Location = System::Drawing::Point(125, 85);
				 this->utility_auto_mount->Name = L"utility_auto_mount";
				 this->utility_auto_mount->Size = System::Drawing::Size(82, 18);
				 this->utility_auto_mount->TabIndex = 15;
				 this->utility_auto_mount->Text = L"Auto-Mount";
				 this->utility_auto_mount->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::utility_auto_mount_ToggleStateChanged);
				 // 
				 // DropDownListFoodName
				 // 
				 this->DropDownListFoodName->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->DropDownListFoodName->Location = System::Drawing::Point(96, 14);
				 this->DropDownListFoodName->Name = L"DropDownListFoodName";
				 this->DropDownListFoodName->Size = System::Drawing::Size(111, 20);
				 this->DropDownListFoodName->TabIndex = 14;
				 this->DropDownListFoodName->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Useful::DropDownListFoodName_SelectedIndexChanged);
				 // 
				 // utility_auto_fishing
				 // 
				 this->utility_auto_fishing->Location = System::Drawing::Point(229, 85);
				 this->utility_auto_fishing->Name = L"utility_auto_fishing";
				 this->utility_auto_fishing->Size = System::Drawing::Size(83, 18);
				 this->utility_auto_fishing->TabIndex = 3;
				 this->utility_auto_fishing->Text = L"Auto Fishing";
				 this->utility_auto_fishing->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_auto_fishing_ToggleStateChanged);
				 // 
				 // box_utility_auto_ring
				 // 
				 this->box_utility_auto_ring->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->box_utility_auto_ring->Location = System::Drawing::Point(96, 61);
				 this->box_utility_auto_ring->Name = L"box_utility_auto_ring";
				 this->box_utility_auto_ring->Size = System::Drawing::Size(111, 20);
				 this->box_utility_auto_ring->TabIndex = 13;
				 this->box_utility_auto_ring->TextChanged += gcnew System::EventHandler(this, &Useful::box_autoring_TextChanged);
				 // 
				 // utility_auto_ring
				 // 
				 this->utility_auto_ring->Location = System::Drawing::Point(10, 61);
				 this->utility_auto_ring->Name = L"utility_auto_ring";
				 this->utility_auto_ring->Size = System::Drawing::Size(72, 18);
				 this->utility_auto_ring->TabIndex = 12;
				 this->utility_auto_ring->Tag = L"";
				 this->utility_auto_ring->Text = L"Auto-Ring";
				 this->utility_auto_ring->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_autoring_ToggleStateChanged);
				 // 
				 // utility_screenshot
				 // 
				 this->utility_screenshot->Location = System::Drawing::Point(10, 85);
				 this->utility_screenshot->Name = L"utility_screenshot";
				 this->utility_screenshot->Size = System::Drawing::Size(75, 18);
				 this->utility_screenshot->TabIndex = 11;
				 this->utility_screenshot->Text = L"Screenshot";
				 this->utility_screenshot->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_screenshot_ToggleStateChanged);
				 // 
				 // utility_drop_vial
				 // 
				 this->utility_drop_vial->Location = System::Drawing::Point(229, 61);
				 this->utility_drop_vial->Name = L"utility_drop_vial";
				 this->utility_drop_vial->Size = System::Drawing::Size(106, 18);
				 this->utility_drop_vial->TabIndex = 2;
				 this->utility_drop_vial->Text = L"Drop Empty Vials";
				 this->utility_drop_vial->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_dropemptyvials_ToggleStateChanged);
				 // 
				 // utility_refil_amun
				 // 
				 this->utility_refil_amun->Location = System::Drawing::Point(10, 37);
				 this->utility_refil_amun->Name = L"utility_refil_amun";
				 this->utility_refil_amun->Size = System::Drawing::Size(75, 18);
				 this->utility_refil_amun->TabIndex = 11;
				 this->utility_refil_amun->Tag = L"";
				 this->utility_refil_amun->Text = L"Refil Amun";
				 this->utility_refil_amun->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_refilamun_ToggleStateChanged);
				 // 
				 // utility_anti_afk
				 // 
				 this->utility_anti_afk->Location = System::Drawing::Point(229, 37);
				 this->utility_anti_afk->Name = L"utility_anti_afk";
				 this->utility_anti_afk->Size = System::Drawing::Size(63, 18);
				 this->utility_anti_afk->TabIndex = 0;
				 this->utility_anti_afk->Text = L"Anti-Idle";
				 this->utility_anti_afk->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_antiafk_ToggleStateChanged);
				 // 
				 // box_utility_refil_amun
				 // 
				 this->box_utility_refil_amun->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 radListDataItem1->Text = L"Assassin Star";
				 radListDataItem2->Text = L"Enchanted Spear";
				 radListDataItem3->Text = L"Glooth Spear";
				 radListDataItem4->Text = L"Hunting Spear";
				 radListDataItem5->Text = L"Mean Spear";
				 radListDataItem6->Text = L"Royal Spear";
				 radListDataItem7->Text = L"Spear";
				 radListDataItem8->Text = L"Small Stone";
				 radListDataItem9->Text = L"Snowball";
				 radListDataItem10->Text = L"Spear";
				 radListDataItem11->Text = L"Throwing Cake";
				 radListDataItem12->Text = L"Throwing Knife";
				 radListDataItem13->Text = L"Throwing Star";
				 radListDataItem14->Text = L"Viper Star";
				 radListDataItem15->Text = L"Arrow";
				 radListDataItem16->Text = L"Burst Arrow";
				 radListDataItem17->Text = L"Crystalline Arrow";
				 radListDataItem18->Text = L"Earth Arrow";
				 radListDataItem19->Text = L"Envenomed Arrow";
				 radListDataItem20->Text = L"Flaming Arrow";
				 radListDataItem21->Text = L"Flash Arrow";
				 radListDataItem22->Text = L"Onyx Arrow";
				 radListDataItem23->Text = L"Poison Arrow";
				 radListDataItem24->Text = L"Shiver Arrow";
				 radListDataItem25->Text = L"Simple Arrow";
				 radListDataItem26->Text = L"Sniper Arrow";
				 radListDataItem27->Text = L"Tarsal Arrow";
				 radListDataItem28->Text = L"Bolt";
				 radListDataItem29->Text = L"Drill Bolt";
				 radListDataItem30->Text = L"Infernal Bolt";
				 radListDataItem31->Text = L"Piercing Bolt";
				 radListDataItem32->Text = L"Power Bolt";
				 radListDataItem33->Text = L"Prismatic Bolt";
				 radListDataItem34->Text = L"Vortex Bolt";
				 this->box_utility_refil_amun->Items->Add(radListDataItem1);
				 this->box_utility_refil_amun->Items->Add(radListDataItem2);
				 this->box_utility_refil_amun->Items->Add(radListDataItem3);
				 this->box_utility_refil_amun->Items->Add(radListDataItem4);
				 this->box_utility_refil_amun->Items->Add(radListDataItem5);
				 this->box_utility_refil_amun->Items->Add(radListDataItem6);
				 this->box_utility_refil_amun->Items->Add(radListDataItem7);
				 this->box_utility_refil_amun->Items->Add(radListDataItem8);
				 this->box_utility_refil_amun->Items->Add(radListDataItem9);
				 this->box_utility_refil_amun->Items->Add(radListDataItem10);
				 this->box_utility_refil_amun->Items->Add(radListDataItem11);
				 this->box_utility_refil_amun->Items->Add(radListDataItem12);
				 this->box_utility_refil_amun->Items->Add(radListDataItem13);
				 this->box_utility_refil_amun->Items->Add(radListDataItem14);
				 this->box_utility_refil_amun->Items->Add(radListDataItem15);
				 this->box_utility_refil_amun->Items->Add(radListDataItem16);
				 this->box_utility_refil_amun->Items->Add(radListDataItem17);
				 this->box_utility_refil_amun->Items->Add(radListDataItem18);
				 this->box_utility_refil_amun->Items->Add(radListDataItem19);
				 this->box_utility_refil_amun->Items->Add(radListDataItem20);
				 this->box_utility_refil_amun->Items->Add(radListDataItem21);
				 this->box_utility_refil_amun->Items->Add(radListDataItem22);
				 this->box_utility_refil_amun->Items->Add(radListDataItem23);
				 this->box_utility_refil_amun->Items->Add(radListDataItem24);
				 this->box_utility_refil_amun->Items->Add(radListDataItem25);
				 this->box_utility_refil_amun->Items->Add(radListDataItem26);
				 this->box_utility_refil_amun->Items->Add(radListDataItem27);
				 this->box_utility_refil_amun->Items->Add(radListDataItem28);
				 this->box_utility_refil_amun->Items->Add(radListDataItem29);
				 this->box_utility_refil_amun->Items->Add(radListDataItem30);
				 this->box_utility_refil_amun->Items->Add(radListDataItem31);
				 this->box_utility_refil_amun->Items->Add(radListDataItem32);
				 this->box_utility_refil_amun->Items->Add(radListDataItem33);
				 this->box_utility_refil_amun->Items->Add(radListDataItem34);
				 this->box_utility_refil_amun->Location = System::Drawing::Point(96, 37);
				 this->box_utility_refil_amun->Name = L"box_utility_refil_amun";
				 this->box_utility_refil_amun->Size = System::Drawing::Size(111, 20);
				 this->box_utility_refil_amun->TabIndex = 12;
				 this->box_utility_refil_amun->TextChanged += gcnew System::EventHandler(this, &Useful::box_refilamun_TextChanged);
				 // 
				 // radLabel10
				 // 
				 this->radLabel10->Location = System::Drawing::Point(246, 14);
				 this->radLabel10->Name = L"radLabel10";
				 this->radLabel10->Size = System::Drawing::Size(49, 18);
				 this->radLabel10->TabIndex = 2;
				 this->radLabel10->Text = L"seconds:";
				 // 
				 // spin_utility_eat_food
				 // 
				 this->spin_utility_eat_food->Location = System::Drawing::Point(301, 13);
				 this->spin_utility_eat_food->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_eat_food->Name = L"spin_utility_eat_food";
				 this->spin_utility_eat_food->Size = System::Drawing::Size(56, 20);
				 this->spin_utility_eat_food->TabIndex = 3;
				 this->spin_utility_eat_food->TabStop = false;
				 this->spin_utility_eat_food->Tag = L"";
				 this->spin_utility_eat_food->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_sec_eatfood_ValueChanged);
				 // 
				 // utility_eat_food
				 // 
				 this->utility_eat_food->Location = System::Drawing::Point(10, 14);
				 this->utility_eat_food->Name = L"utility_eat_food";
				 this->utility_eat_food->Size = System::Drawing::Size(62, 18);
				 this->utility_eat_food->TabIndex = 10;
				 this->utility_eat_food->Tag = L"Refil Amun";
				 this->utility_eat_food->Text = L"Eat food";
				 this->utility_eat_food->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_eat_food_ToggleStateChanged);
				 // 
				 // utility_anti_poison
				 // 
				 this->utility_anti_poison->Location = System::Drawing::Point(389, 115);
				 this->utility_anti_poison->Name = L"utility_anti_poison";
				 this->utility_anti_poison->Size = System::Drawing::Size(79, 18);
				 this->utility_anti_poison->TabIndex = 7;
				 this->utility_anti_poison->Text = L"Anti-Poison";
				 this->utility_anti_poison->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_antipoison_ToggleStateChanged);
				 // 
				 // radGroupBox5
				 // 
				 this->radGroupBox5->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox5->Controls->Add(this->radLabel7);
				 this->radGroupBox5->Controls->Add(this->spin_utility_anti_poison);
				 this->radGroupBox5->Controls->Add(this->radLabel8);
				 this->radGroupBox5->Controls->Add(this->text_utility_anti_poison);
				 this->radGroupBox5->HeaderText = L"";
				 this->radGroupBox5->Location = System::Drawing::Point(374, 120);
				 this->radGroupBox5->Name = L"radGroupBox5";
				 this->radGroupBox5->Size = System::Drawing::Size(180, 80);
				 this->radGroupBox5->TabIndex = 6;
				 // 
				 // radLabel7
				 // 
				 this->radLabel7->Location = System::Drawing::Point(9, 47);
				 this->radLabel7->Name = L"radLabel7";
				 this->radLabel7->Size = System::Drawing::Size(34, 18);
				 this->radLabel7->TabIndex = 2;
				 this->radLabel7->Text = L"Mana";
				 // 
				 // spin_utility_anti_poison
				 // 
				 this->spin_utility_anti_poison->Location = System::Drawing::Point(49, 46);
				 this->spin_utility_anti_poison->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_anti_poison->Name = L"spin_utility_anti_poison";
				 this->spin_utility_anti_poison->Size = System::Drawing::Size(69, 20);
				 this->spin_utility_anti_poison->TabIndex = 2;
				 this->spin_utility_anti_poison->TabStop = false;
				 this->spin_utility_anti_poison->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_mana_antipoison_ValueChanged);
				 // 
				 // radLabel8
				 // 
				 this->radLabel8->Location = System::Drawing::Point(10, 21);
				 this->radLabel8->Name = L"radLabel8";
				 this->radLabel8->Size = System::Drawing::Size(33, 18);
				 this->radLabel8->TabIndex = 1;
				 this->radLabel8->Text = L"Spell:";
				 // 
				 // text_utility_anti_poison
				 // 
				 this->text_utility_anti_poison->Location = System::Drawing::Point(49, 20);
				 this->text_utility_anti_poison->Name = L"text_utility_anti_poison";
				 this->text_utility_anti_poison->Size = System::Drawing::Size(123, 20);
				 this->text_utility_anti_poison->TabIndex = 0;
				 this->text_utility_anti_poison->TextChanged += gcnew System::EventHandler(this, &Useful::text_spell_antipoison_TextChanged);
				 // 
				 // radGroupBox6
				 // 
				 this->radGroupBox6->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox6->Controls->Add(this->radLabel13);
				 this->radGroupBox6->Controls->Add(this->spin_utility_rune_maker_time);
				 this->radGroupBox6->Controls->Add(this->radLabel12);
				 this->radGroupBox6->Controls->Add(this->spin_utility_rune_maker_soul);
				 this->radGroupBox6->Controls->Add(this->radLabel9);
				 this->radGroupBox6->Controls->Add(this->spin_utility_rune_maker);
				 this->radGroupBox6->Controls->Add(this->radLabel11);
				 this->radGroupBox6->Controls->Add(this->text_utility_rune_maker);
				 this->radGroupBox6->HeaderText = L"";
				 this->radGroupBox6->Location = System::Drawing::Point(2, 183);
				 this->radGroupBox6->Name = L"radGroupBox6";
				 this->radGroupBox6->Size = System::Drawing::Size(180, 124);
				 this->radGroupBox6->TabIndex = 7;
				 // 
				 // radLabel13
				 // 
				 this->radLabel13->Location = System::Drawing::Point(9, 99);
				 this->radLabel13->Name = L"radLabel13";
				 this->radLabel13->Size = System::Drawing::Size(31, 18);
				 this->radLabel13->TabIndex = 5;
				 this->radLabel13->Text = L"Time";
				 // 
				 // spin_utility_rune_maker_time
				 // 
				 this->spin_utility_rune_maker_time->Location = System::Drawing::Point(49, 98);
				 this->spin_utility_rune_maker_time->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_rune_maker_time->Name = L"spin_utility_rune_maker_time";
				 this->spin_utility_rune_maker_time->Size = System::Drawing::Size(126, 20);
				 this->spin_utility_rune_maker_time->TabIndex = 6;
				 this->spin_utility_rune_maker_time->TabStop = false;
				 this->spin_utility_rune_maker_time->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_check_runemaker_time_ValueChanged);
				 // 
				 // radLabel12
				 // 
				 this->radLabel12->Location = System::Drawing::Point(9, 73);
				 this->radLabel12->Name = L"radLabel12";
				 this->radLabel12->Size = System::Drawing::Size(28, 18);
				 this->radLabel12->TabIndex = 3;
				 this->radLabel12->Text = L"Soul";
				 // 
				 // spin_utility_rune_maker_soul
				 // 
				 this->spin_utility_rune_maker_soul->Location = System::Drawing::Point(49, 72);
				 this->spin_utility_rune_maker_soul->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_rune_maker_soul->Name = L"spin_utility_rune_maker_soul";
				 this->spin_utility_rune_maker_soul->Size = System::Drawing::Size(126, 20);
				 this->spin_utility_rune_maker_soul->TabIndex = 4;
				 this->spin_utility_rune_maker_soul->TabStop = false;
				 this->spin_utility_rune_maker_soul->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_check_runemaker_soul_ValueChanged);
				 // 
				 // radLabel9
				 // 
				 this->radLabel9->Location = System::Drawing::Point(9, 47);
				 this->radLabel9->Name = L"radLabel9";
				 this->radLabel9->Size = System::Drawing::Size(34, 18);
				 this->radLabel9->TabIndex = 2;
				 this->radLabel9->Text = L"Mana";
				 // 
				 // spin_utility_rune_maker
				 // 
				 this->spin_utility_rune_maker->Location = System::Drawing::Point(49, 46);
				 this->spin_utility_rune_maker->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_rune_maker->Name = L"spin_utility_rune_maker";
				 this->spin_utility_rune_maker->Size = System::Drawing::Size(126, 20);
				 this->spin_utility_rune_maker->TabIndex = 2;
				 this->spin_utility_rune_maker->TabStop = false;
				 this->spin_utility_rune_maker->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_check_runemaker_mana_ValueChanged);
				 // 
				 // radLabel11
				 // 
				 this->radLabel11->Location = System::Drawing::Point(10, 21);
				 this->radLabel11->Name = L"radLabel11";
				 this->radLabel11->Size = System::Drawing::Size(33, 18);
				 this->radLabel11->TabIndex = 1;
				 this->radLabel11->Text = L"Spell:";
				 // 
				 // text_utility_rune_maker
				 // 
				 this->text_utility_rune_maker->Location = System::Drawing::Point(49, 20);
				 this->text_utility_rune_maker->Name = L"text_utility_rune_maker";
				 this->text_utility_rune_maker->Size = System::Drawing::Size(126, 20);
				 this->text_utility_rune_maker->TabIndex = 0;
				 this->text_utility_rune_maker->TextChanged += gcnew System::EventHandler(this, &Useful::text_check_runemaker_TextChanged);
				 // 
				 // utility_rune_maker
				 // 
				 this->utility_rune_maker->Location = System::Drawing::Point(17, 178);
				 this->utility_rune_maker->Name = L"utility_rune_maker";
				 this->utility_rune_maker->Size = System::Drawing::Size(141, 18);
				 this->utility_rune_maker->TabIndex = 8;
				 this->utility_rune_maker->Text = L"Rune Maker\\Mana Train";
				 this->utility_rune_maker->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_rune_maker_ToggleStateChanged);
				 // 
				 // utility_auto_sio
				 // 
				 this->utility_auto_sio->Location = System::Drawing::Point(204, 201);
				 this->utility_auto_sio->Name = L"utility_auto_sio";
				 this->utility_auto_sio->Size = System::Drawing::Size(66, 18);
				 this->utility_auto_sio->TabIndex = 7;
				 this->utility_auto_sio->Text = L"Exura Sio";
				 this->utility_auto_sio->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::utility_auto_sio_ToggleStateChanged);
				 // 
				 // radGroupBox7
				 // 
				 this->radGroupBox7->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox7->Controls->Add(this->ButtonRemove);
				 this->radGroupBox7->Controls->Add(this->ButtonNew);
				 this->radGroupBox7->Controls->Add(this->ListViewFriend);
				 this->radGroupBox7->Controls->Add(this->radLabel14);
				 this->radGroupBox7->Controls->Add(this->spin_auto_sio);
				 this->radGroupBox7->Controls->Add(this->radLabel19);
				 this->radGroupBox7->Controls->Add(this->spin_utility_auto_sio);
				 this->radGroupBox7->Controls->Add(this->radLabel15);
				 this->radGroupBox7->Controls->Add(this->text_utility_auto_sio);
				 this->radGroupBox7->HeaderText = L"";
				 this->radGroupBox7->Location = System::Drawing::Point(189, 206);
				 this->radGroupBox7->Name = L"radGroupBox7";
				 this->radGroupBox7->Size = System::Drawing::Size(180, 209);
				 this->radGroupBox7->TabIndex = 6;
				 // 
				 // ButtonRemove
				 // 
				 this->ButtonRemove->Location = System::Drawing::Point(93, 82);
				 this->ButtonRemove->Name = L"ButtonRemove";
				 this->ButtonRemove->Size = System::Drawing::Size(82, 24);
				 this->ButtonRemove->TabIndex = 14;
				 this->ButtonRemove->Text = L"Remove";
				 this->ButtonRemove->Click += gcnew System::EventHandler(this, &Useful::ButtonRemove_Click);
				 // 
				 // ButtonNew
				 // 
				 this->ButtonNew->Location = System::Drawing::Point(5, 82);
				 this->ButtonNew->Name = L"ButtonNew";
				 this->ButtonNew->Size = System::Drawing::Size(82, 24);
				 this->ButtonNew->TabIndex = 13;
				 this->ButtonNew->Text = L"New";
				 this->ButtonNew->Click += gcnew System::EventHandler(this, &Useful::ButtonNew_Click);
				 // 
				 // ListViewFriend
				 // 
				 this->ListViewFriend->AllowColumnReorder = false;
				 this->ListViewFriend->AllowColumnResize = false;
				 this->ListViewFriend->AllowEdit = false;
				 this->ListViewFriend->AllowRemove = false;
				 listViewDetailColumn1->HeaderText = L"Name";
				 listViewDetailColumn1->MaxWidth = 134;
				 listViewDetailColumn1->MinWidth = 134;
				 listViewDetailColumn1->Width = 134;
				 listViewDetailColumn2->HeaderText = L"Hp";
				 listViewDetailColumn2->MaxWidth = 35;
				 listViewDetailColumn2->MinWidth = 35;
				 listViewDetailColumn2->Width = 35;
				 this->ListViewFriend->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(2) {
					 listViewDetailColumn1,
						 listViewDetailColumn2
				 });
				 this->ListViewFriend->ItemSpacing = -1;
				 this->ListViewFriend->Location = System::Drawing::Point(5, 108);
				 this->ListViewFriend->Name = L"ListViewFriend";
				 this->ListViewFriend->Size = System::Drawing::Size(170, 96);
				 this->ListViewFriend->TabIndex = 12;
				 this->ListViewFriend->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				 this->ListViewFriend->SelectedItemChanged += gcnew System::EventHandler(this, &Useful::ListViewFriend_SelectedItemChanged);
				 // 
				 // radLabel14
				 // 
				 this->radLabel14->Location = System::Drawing::Point(2, 58);
				 this->radLabel14->Name = L"radLabel14";
				 this->radLabel14->Size = System::Drawing::Size(46, 18);
				 this->radLabel14->TabIndex = 5;
				 this->radLabel14->Text = L"Min Hp:";
				 // 
				 // spin_auto_sio
				 // 
				 this->spin_auto_sio->Location = System::Drawing::Point(49, 57);
				 this->spin_auto_sio->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_auto_sio->Name = L"spin_auto_sio";
				 this->spin_auto_sio->Size = System::Drawing::Size(126, 20);
				 this->spin_auto_sio->TabIndex = 6;
				 this->spin_auto_sio->TabStop = false;
				 this->spin_auto_sio->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_utility_auto_sio_ValueChanged);
				 // 
				 // radLabel19
				 // 
				 this->radLabel19->Location = System::Drawing::Point(6, 14);
				 this->radLabel19->Name = L"radLabel19";
				 this->radLabel19->Size = System::Drawing::Size(42, 18);
				 this->radLabel19->TabIndex = 3;
				 this->radLabel19->Text = L"My Hp:";
				 // 
				 // spin_utility_auto_sio
				 // 
				 this->spin_utility_auto_sio->Location = System::Drawing::Point(49, 13);
				 this->spin_utility_auto_sio->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_auto_sio->Name = L"spin_utility_auto_sio";
				 this->spin_utility_auto_sio->Size = System::Drawing::Size(126, 20);
				 this->spin_utility_auto_sio->TabIndex = 4;
				 this->spin_utility_auto_sio->TabStop = false;
				 this->spin_utility_auto_sio->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_utility_auto_sio_ValueChanged_1);
				 // 
				 // radLabel15
				 // 
				 this->radLabel15->Location = System::Drawing::Point(8, 36);
				 this->radLabel15->Name = L"radLabel15";
				 this->radLabel15->Size = System::Drawing::Size(40, 18);
				 this->radLabel15->TabIndex = 1;
				 this->radLabel15->Text = L"Friend:";
				 // 
				 // text_utility_auto_sio
				 // 
				 this->text_utility_auto_sio->Location = System::Drawing::Point(49, 35);
				 this->text_utility_auto_sio->Name = L"text_utility_auto_sio";
				 this->text_utility_auto_sio->Size = System::Drawing::Size(126, 20);
				 this->text_utility_auto_sio->TabIndex = 0;
				 this->text_utility_auto_sio->TextChanged += gcnew System::EventHandler(this, &Useful::text_utility_auto_sio_TextChanged);
				 // 
				 // utility_auto_utura
				 // 
				 this->utility_auto_utura->Location = System::Drawing::Point(389, 201);
				 this->utility_auto_utura->Name = L"utility_auto_utura";
				 this->utility_auto_utura->Size = System::Drawing::Size(77, 18);
				 this->utility_auto_utura->TabIndex = 9;
				 this->utility_auto_utura->Text = L"Auto-Utura";
				 this->utility_auto_utura->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::utility_auto_utura_ToggleStateChanged);
				 // 
				 // radGroupBox8
				 // 
				 this->radGroupBox8->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox8->Controls->Add(this->radLabel18);
				 this->radGroupBox8->Controls->Add(this->radLabel16);
				 this->radGroupBox8->Controls->Add(this->spin_utility_auto_utura);
				 this->radGroupBox8->Controls->Add(this->radLabel17);
				 this->radGroupBox8->Controls->Add(this->spin_utility_auto_utura_);
				 this->radGroupBox8->Controls->Add(this->text_utility_auto_utura);
				 this->radGroupBox8->HeaderText = L"";
				 this->radGroupBox8->Location = System::Drawing::Point(374, 206);
				 this->radGroupBox8->Name = L"radGroupBox8";
				 this->radGroupBox8->Size = System::Drawing::Size(180, 101);
				 this->radGroupBox8->TabIndex = 8;
				 // 
				 // radLabel18
				 // 
				 this->radLabel18->Location = System::Drawing::Point(19, 46);
				 this->radLabel18->Name = L"radLabel18";
				 this->radLabel18->Size = System::Drawing::Size(30, 18);
				 this->radLabel18->TabIndex = 3;
				 this->radLabel18->Text = L"Hp%";
				 // 
				 // radLabel16
				 // 
				 this->radLabel16->Location = System::Drawing::Point(10, 72);
				 this->radLabel16->Name = L"radLabel16";
				 this->radLabel16->Size = System::Drawing::Size(34, 18);
				 this->radLabel16->TabIndex = 2;
				 this->radLabel16->Text = L"Mana";
				 // 
				 // spin_utility_auto_utura
				 // 
				 this->spin_utility_auto_utura->Location = System::Drawing::Point(49, 71);
				 this->spin_utility_auto_utura->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_auto_utura->Name = L"spin_utility_auto_utura";
				 this->spin_utility_auto_utura->Size = System::Drawing::Size(126, 20);
				 this->spin_utility_auto_utura->TabIndex = 2;
				 this->spin_utility_auto_utura->TabStop = false;
				 this->spin_utility_auto_utura->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_utility_auto_utura_ValueChanged);
				 // 
				 // radLabel17
				 // 
				 this->radLabel17->Location = System::Drawing::Point(10, 21);
				 this->radLabel17->Name = L"radLabel17";
				 this->radLabel17->Size = System::Drawing::Size(33, 18);
				 this->radLabel17->TabIndex = 1;
				 this->radLabel17->Text = L"Spell:";
				 // 
				 // spin_utility_auto_utura_
				 // 
				 this->spin_utility_auto_utura_->Location = System::Drawing::Point(49, 45);
				 this->spin_utility_auto_utura_->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->spin_utility_auto_utura_->Name = L"spin_utility_auto_utura_";
				 this->spin_utility_auto_utura_->Size = System::Drawing::Size(126, 20);
				 this->spin_utility_auto_utura_->TabIndex = 4;
				 this->spin_utility_auto_utura_->TabStop = false;
				 this->spin_utility_auto_utura_->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_utility_auto_utura__ValueChanged);
				 // 
				 // text_utility_auto_utura
				 // 
				 this->text_utility_auto_utura->Location = System::Drawing::Point(49, 20);
				 this->text_utility_auto_utura->Name = L"text_utility_auto_utura";
				 this->text_utility_auto_utura->Size = System::Drawing::Size(126, 20);
				 this->text_utility_auto_utura->TabIndex = 0;
				 this->text_utility_auto_utura->TextChanged += gcnew System::EventHandler(this, &Useful::text_utility_auto_utura_TextChanged);
				 // 
				 // check_autorelogin
				 // 
				 this->check_autorelogin->Location = System::Drawing::Point(384, 308);
				 this->check_autorelogin->Name = L"check_autorelogin";
				 this->check_autorelogin->Size = System::Drawing::Size(85, 18);
				 this->check_autorelogin->TabIndex = 11;
				 this->check_autorelogin->Text = L"Auto Relogin";
				 this->check_autorelogin->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::check_autorelogin_ToggleStateChanged);
				 // 
				 // radGroupBox9
				 // 
				 this->radGroupBox9->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox9->Controls->Add(this->radLabel22);
				 this->radGroupBox9->Controls->Add(this->radLabel21);
				 this->radGroupBox9->Controls->Add(this->radLabel20);
				 this->radGroupBox9->Controls->Add(this->box_index);
				 this->radGroupBox9->Controls->Add(this->box_account);
				 this->radGroupBox9->Controls->Add(this->box_password);
				 this->radGroupBox9->HeaderText = L"";
				 this->radGroupBox9->Location = System::Drawing::Point(374, 314);
				 this->radGroupBox9->Name = L"radGroupBox9";
				 this->radGroupBox9->Size = System::Drawing::Size(180, 101);
				 this->radGroupBox9->TabIndex = 10;
				 // 
				 // radLabel22
				 // 
				 this->radLabel22->Location = System::Drawing::Point(22, 70);
				 this->radLabel22->Name = L"radLabel22";
				 this->radLabel22->Size = System::Drawing::Size(36, 18);
				 this->radLabel22->TabIndex = 6;
				 this->radLabel22->Text = L"Index:";
				 // 
				 // radLabel21
				 // 
				 this->radLabel21->Location = System::Drawing::Point(2, 44);
				 this->radLabel21->Name = L"radLabel21";
				 this->radLabel21->Size = System::Drawing::Size(56, 18);
				 this->radLabel21->TabIndex = 5;
				 this->radLabel21->Text = L"Password:";
				 // 
				 // radLabel20
				 // 
				 this->radLabel20->Location = System::Drawing::Point(8, 17);
				 this->radLabel20->Name = L"radLabel20";
				 this->radLabel20->Size = System::Drawing::Size(50, 18);
				 this->radLabel20->TabIndex = 4;
				 this->radLabel20->Text = L"Account:";
				 // 
				 // box_index
				 // 
				 this->box_index->Location = System::Drawing::Point(58, 69);
				 this->box_index->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999, 0, 0, 0 });
				 this->box_index->Name = L"box_index";
				 this->box_index->Size = System::Drawing::Size(117, 20);
				 this->box_index->TabIndex = 3;
				 this->box_index->TabStop = false;
				 this->box_index->ValueChanged += gcnew System::EventHandler(this, &Useful::box_index_ValueChanged);
				 // 
				 // box_account
				 // 
				 this->box_account->Location = System::Drawing::Point(58, 16);
				 this->box_account->Name = L"box_account";
				 this->box_account->NullText = L"Account...";
				 this->box_account->Size = System::Drawing::Size(117, 20);
				 this->box_account->TabIndex = 0;
				 this->box_account->TextChanged += gcnew System::EventHandler(this, &Useful::box_account_TextChanged);
				 // 
				 // box_password
				 // 
				 this->box_password->Location = System::Drawing::Point(58, 43);
				 this->box_password->Name = L"box_password";
				 this->box_password->NullText = L"Password...";
				 this->box_password->Size = System::Drawing::Size(117, 20);
				 this->box_password->TabIndex = 1;
				 this->box_password->TextChanged += gcnew System::EventHandler(this, &Useful::box_password_TextChanged);
				 // 
				 // radGroupBox10
				 // 
				 this->radGroupBox10->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox10->Controls->Add(this->text_utility_soft_change);
				 this->radGroupBox10->Controls->Add(this->radLabel25);
				 this->radGroupBox10->Controls->Add(this->radLabel24);
				 this->radGroupBox10->Controls->Add(this->spin_utility_soft_change_);
				 this->radGroupBox10->Controls->Add(this->spin_utility_soft_change);
				 this->radGroupBox10->Controls->Add(this->radLabel23);
				 this->radGroupBox10->HeaderText = L"";
				 this->radGroupBox10->Location = System::Drawing::Point(2, 314);
				 this->radGroupBox10->Name = L"radGroupBox10";
				 this->radGroupBox10->Size = System::Drawing::Size(180, 100);
				 this->radGroupBox10->TabIndex = 12;
				 // 
				 // text_utility_soft_change
				 // 
				 this->text_utility_soft_change->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->text_utility_soft_change->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 radListDataItem35->Text = L"Boots of Haste";
				 radListDataItem36->Text = L"Crocodile Boots";
				 radListDataItem37->Text = L"Crystal Boots";
				 radListDataItem38->Text = L"Depth Calcei";
				 radListDataItem39->Text = L"Draken Boots";
				 radListDataItem40->Text = L"Fur Boots";
				 radListDataItem41->Text = L"Firewalker Boots";
				 radListDataItem42->Text = L"Glacier Shoes";
				 radListDataItem43->Text = L"Guardian Boots";
				 radListDataItem44->Text = L"Leather Boots";
				 radListDataItem45->Text = L"Lightning Boots";
				 radListDataItem46->Text = L"Magma Boots";
				 radListDataItem47->Text = L"Metal Spats";
				 radListDataItem48->Text = L"Pirate Boots";
				 radListDataItem49->Text = L"Prismatic Boots";
				 radListDataItem50->Text = L"Steel Boots";
				 radListDataItem51->Text = L"Terra Boots";
				 radListDataItem52->Text = L"Treader of Torment";
				 radListDataItem53->Text = L"Void Boots";
				 radListDataItem54->Text = L"Zaoan Shoes";
				 this->text_utility_soft_change->Items->Add(radListDataItem35);
				 this->text_utility_soft_change->Items->Add(radListDataItem36);
				 this->text_utility_soft_change->Items->Add(radListDataItem37);
				 this->text_utility_soft_change->Items->Add(radListDataItem38);
				 this->text_utility_soft_change->Items->Add(radListDataItem39);
				 this->text_utility_soft_change->Items->Add(radListDataItem40);
				 this->text_utility_soft_change->Items->Add(radListDataItem41);
				 this->text_utility_soft_change->Items->Add(radListDataItem42);
				 this->text_utility_soft_change->Items->Add(radListDataItem43);
				 this->text_utility_soft_change->Items->Add(radListDataItem44);
				 this->text_utility_soft_change->Items->Add(radListDataItem45);
				 this->text_utility_soft_change->Items->Add(radListDataItem46);
				 this->text_utility_soft_change->Items->Add(radListDataItem47);
				 this->text_utility_soft_change->Items->Add(radListDataItem48);
				 this->text_utility_soft_change->Items->Add(radListDataItem49);
				 this->text_utility_soft_change->Items->Add(radListDataItem50);
				 this->text_utility_soft_change->Items->Add(radListDataItem51);
				 this->text_utility_soft_change->Items->Add(radListDataItem52);
				 this->text_utility_soft_change->Items->Add(radListDataItem53);
				 this->text_utility_soft_change->Items->Add(radListDataItem54);
				 this->text_utility_soft_change->Location = System::Drawing::Point(69, 16);
				 this->text_utility_soft_change->Name = L"text_utility_soft_change";
				 this->text_utility_soft_change->Size = System::Drawing::Size(106, 20);
				 this->text_utility_soft_change->TabIndex = 7;
				 this->text_utility_soft_change->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Useful::text_utility_soft_change_SelectedIndexChanged);
				 // 
				 // radLabel25
				 // 
				 this->radLabel25->Location = System::Drawing::Point(5, 68);
				 this->radLabel25->Name = L"radLabel25";
				 this->radLabel25->Size = System::Drawing::Size(101, 18);
				 this->radLabel25->TabIndex = 6;
				 this->radLabel25->Text = L"Mana to unquiped:";
				 // 
				 // radLabel24
				 // 
				 this->radLabel24->Location = System::Drawing::Point(5, 42);
				 this->radLabel24->Name = L"radLabel24";
				 this->radLabel24->Size = System::Drawing::Size(94, 18);
				 this->radLabel24->TabIndex = 5;
				 this->radLabel24->Text = L"Mana to equiped:";
				 // 
				 // spin_utility_soft_change_
				 // 
				 this->spin_utility_soft_change_->Location = System::Drawing::Point(106, 66);
				 this->spin_utility_soft_change_->Name = L"spin_utility_soft_change_";
				 this->spin_utility_soft_change_->Size = System::Drawing::Size(69, 20);
				 this->spin_utility_soft_change_->TabIndex = 4;
				 this->spin_utility_soft_change_->TabStop = false;
				 this->spin_utility_soft_change_->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_utility_soft_change__ValueChanged);
				 // 
				 // spin_utility_soft_change
				 // 
				 this->spin_utility_soft_change->Location = System::Drawing::Point(106, 41);
				 this->spin_utility_soft_change->Name = L"spin_utility_soft_change";
				 this->spin_utility_soft_change->Size = System::Drawing::Size(69, 20);
				 this->spin_utility_soft_change->TabIndex = 2;
				 this->spin_utility_soft_change->TabStop = false;
				 this->spin_utility_soft_change->ValueChanged += gcnew System::EventHandler(this, &Useful::spin_utility_soft_change_ValueChanged);
				 // 
				 // radLabel23
				 // 
				 this->radLabel23->Location = System::Drawing::Point(5, 16);
				 this->radLabel23->Name = L"radLabel23";
				 this->radLabel23->Size = System::Drawing::Size(58, 18);
				 this->radLabel23->TabIndex = 1;
				 this->radLabel23->Text = L"Old boots:";
				 // 
				 // utility_soft_change
				 // 
				 this->utility_soft_change->Location = System::Drawing::Point(17, 308);
				 this->utility_soft_change->Name = L"utility_soft_change";
				 this->utility_soft_change->Size = System::Drawing::Size(82, 18);
				 this->utility_soft_change->TabIndex = 9;
				 this->utility_soft_change->Text = L"Soft Change";
				 this->utility_soft_change->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Useful::utility_soft_change_ToggleStateChanged);
				 // 
				 // Useful
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(557, 417);
				 this->Controls->Add(this->utility_soft_change);
				 this->Controls->Add(this->radGroupBox10);
				 this->Controls->Add(this->check_autorelogin);
				 this->Controls->Add(this->radGroupBox9);
				 this->Controls->Add(this->utility_auto_utura);
				 this->Controls->Add(this->utility_auto_sio);
				 this->Controls->Add(this->radGroupBox8);
				 this->Controls->Add(this->radGroupBox7);
				 this->Controls->Add(this->utility_rune_maker);
				 this->Controls->Add(this->radGroupBox6);
				 this->Controls->Add(this->utility_anti_poison);
				 this->Controls->Add(this->radGroupBox4);
				 this->Controls->Add(this->radGroupBox5);
				 this->Controls->Add(this->utility_auto_mana_shield);
				 this->Controls->Add(this->radGroupBox3);
				 this->Controls->Add(this->utility_auto_haste);
				 this->Controls->Add(this->utility_anti_paralyze);
				 this->Controls->Add(this->radGroupBox2);
				 this->Controls->Add(this->radGroupBox1);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->MaximizeBox = false;
				 this->Name = L"Useful";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->Text = L"Utility";
				 this->Load += gcnew System::EventHandler(this, &Useful::Useful_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				 this->radGroupBox1->ResumeLayout(false);
				 this->radGroupBox1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_anti_paralyze))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_anti_paralyze))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
				 this->radGroupBox2->ResumeLayout(false);
				 this->radGroupBox2->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_haste))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_haste))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_anti_paralyze))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_haste))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_mana_shield))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->EndInit();
				 this->radGroupBox3->ResumeLayout(false);
				 this->radGroupBox3->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_mana_shield))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_mana_shield))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->EndInit();
				 this->radGroupBox4->ResumeLayout(false);
				 this->radGroupBox4->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_mount))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListFoodName))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_fishing))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_utility_auto_ring))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_ring))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_screenshot))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_drop_vial))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_refil_amun))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_anti_afk))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_utility_refil_amun))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel10))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_eat_food))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_eat_food))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_anti_poison))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox5))->EndInit();
				 this->radGroupBox5->ResumeLayout(false);
				 this->radGroupBox5->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_anti_poison))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_anti_poison))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox6))->EndInit();
				 this->radGroupBox6->ResumeLayout(false);
				 this->radGroupBox6->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_rune_maker_time))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_rune_maker_soul))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_rune_maker))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel11))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_rune_maker))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_rune_maker))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_sio))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox7))->EndInit();
				 this->radGroupBox7->ResumeLayout(false);
				 this->radGroupBox7->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemove))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonNew))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewFriend))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel14))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_auto_sio))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel19))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_sio))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel15))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_sio))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_auto_utura))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox8))->EndInit();
				 this->radGroupBox8->ResumeLayout(false);
				 this->radGroupBox8->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel18))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel16))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_utura))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel17))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_auto_utura_))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_auto_utura))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_autorelogin))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox9))->EndInit();
				 this->radGroupBox9->ResumeLayout(false);
				 this->radGroupBox9->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel22))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel21))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel20))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_index))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_account))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_password))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox10))->EndInit();
				 this->radGroupBox10->ResumeLayout(false);
				 this->radGroupBox10->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->text_utility_soft_change))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel25))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel24))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_soft_change_))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_utility_soft_change))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel23))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->utility_soft_change))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion
			 bool disable_item_update = false;

			 void update_idiom() {
				 radLabel10->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel10->Text))[0]);
				 ButtonNew->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonNew->Text))[0]);
				 ButtonRemove->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonRemove->Text))[0]);

				 for each(auto colum in ListViewFriend->Columns)
					 colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
			 }

	private: System::Void check_antiparalyze_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_anti_paralyze);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_anti_paralyze->Checked);
	}
	private: System::Void check_automana_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_mana_shield);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_auto_mana_shield->Checked);
	}
	private: System::Void check_autohaste_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_haste);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_auto_haste->Checked);
	}
	private: System::Void check_antipoison_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_anti_poison);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_anti_poison->Checked);
	}
	private: System::Void check_eat_food_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_eat_food);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_eat_food->Checked);
	}
	private: System::Void check_refilamun_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_refil_amun);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_refil_amun->Checked);
	}
	private: System::Void check_autoring_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_ring);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_auto_ring->Checked);
	}
	private: System::Void check_screenshot_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_screenshot);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_screenshot->Checked);
	}
	private: System::Void check_antiafk_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_anti_afk);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_delay(120 * 1000);
				 scriptInfoRule->set_active(utility_anti_afk->Checked);
	}
	private: System::Void check_dropemptyvials_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_drop_vial);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_delay(10 * 1000);
				 scriptInfoRule->set_active(utility_drop_vial->Checked);
	}
	private: System::Void check_rune_maker_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_rune_maker);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_rune_maker->Checked);
	}
	private: System::Void check_auto_fishing_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_fishing);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_auto_fishing->Checked);
	}
	private: System::Void utility_auto_sio_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_sio);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_auto_sio->Checked);
	}

	private: System::Void text_check_runemaker_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_rune_maker);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(text_utility_rune_maker->Text));
	}
	private: System::Void text_spell_antiparalyze_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_anti_paralyze);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(text_utility_anti_paralyze->Text));
	}
	private: System::Void text_spell_automana_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_mana_shield);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(text_utility_auto_mana_shield->Text));
	}
	private: System::Void text_spell_autohaste_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_haste);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(text_utility_auto_haste->Text));
	}
	private: System::Void text_spell_antipoison_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_anti_poison);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(text_utility_anti_poison->Text));
	}
			 String^ old;
			 bool first = true;
	private: System::Void text_utility_auto_sio_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_sio);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);
				 if (!scriptInfoRule)
					 return;

				 Telerik::WinControls::UI::ListViewDataItem^ selectedItem = ListViewFriend->SelectedItem;
				 if (!selectedItem)
					 return;


				 std::string newName = managed_util::fromSS(text_utility_auto_sio->Text);
				 std::string oldName = managed_util::fromSS(old);

				 disable_item_update = true;

				 if (!scriptInfoRule->changeFriendName(newName, oldName)){
					 text_utility_auto_sio->Text = old;
					 selectedItem["Name"] = old;
					 disable_item_update = false;
					 return;
				 }

				 selectedItem["Name"] = text_utility_auto_sio->Text;
				 old = text_utility_auto_sio->Text;
				 disable_item_update = false;
	}

	private: System::Void box_refilamun_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_refil_amun);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(box_utility_refil_amun->Text));
	}
	private: System::Void box_autoring_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_ring);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(box_utility_auto_ring->Text));
	}

	private: System::Void spin_mana_antiparalyze_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_anti_paralyze);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_anti_paralyze->Value);
	}
	private: System::Void spin_mana_automana_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_mana_shield);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_auto_mana_shield->Value);
	}
	private: System::Void spin_mana_autohaste_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_haste);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_auto_haste->Value);
	}
	private: System::Void spin_mana_antipoison_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_anti_poison);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_anti_poison->Value);
	}
	private: System::Void spin_check_runemaker_mana_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_rune_maker);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_rune_maker->Value);
	}
	private: System::Void spin_check_runemaker_soul_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_rune_maker);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_soul((int)spin_utility_rune_maker_soul->Value);
	}
	private: System::Void spin_check_runemaker_time_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_rune_maker);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_delay((int)spin_utility_rune_maker_time->Value);
	}
	private: System::Void spin_sec_eatfood_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_eat_food);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_delay((int)spin_utility_eat_food->Value);
	}
	private: System::Void spin_utility_auto_sio_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_sio);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);
				 if (!scriptInfoRule)
					 return;

				 Telerik::WinControls::UI::ListViewDataItem^ selectedItem = ListViewFriend->SelectedItem;
				 if (!selectedItem)
					 return;

				 disable_item_update = true;
				 std::string charName = managed_util::fromSS((String^)selectedItem["Name"]);

				 selectedItem["Hp"] = (int)spin_auto_sio->Value;

				 scriptInfoRule->set_value_map_friend(charName, (int)spin_auto_sio->Value);

				 disable_item_update = false;
	}

			 void loadinform(){
				 auto myMap = UtilityCore::get()->get_map_scriptInfo();
				 if (myMap.empty())
					 return;

				 disable_item_update = true;
				 for (auto it = myMap.begin(); it != myMap.end(); it++){
					 if (!it->second)
						 return;

					 if (it->second->script_type == utility_t::utility_soft_change){
						 this->spin_utility_soft_change_->Value = (int)it->second->get_hp();
						 this->spin_utility_soft_change->Value = (int)it->second->get_mana();
					 }

					 if (it->second->script_type == utility_t::utility_auto_utura){
						 this->spin_utility_auto_utura_->Value = (int)it->second->get_hp();
						 this->spin_utility_auto_utura->Value = (int)it->second->get_mana();
						 this->utility_auto_utura->Checked = it->second->get_active();
					 }

					 if (it->second->script_type == utility_t::utility_rune_maker){
						 this->utility_rune_maker->Checked = it->second->get_active();
						 this->text_utility_rune_maker->Text = gcnew String(it->second->get_value_string().c_str());
						 this->spin_utility_rune_maker->Value = (int)it->second->get_mana();
						 this->spin_utility_rune_maker_time->Value = (int)it->second->get_delay();
						 this->spin_utility_rune_maker_soul->Value = (int)it->second->get_soul();
						 continue;
					 }

					 auto find_check = this->Controls->Find(gcnew String(it->first.c_str()), true);
					 auto find_check_box = this->Controls->Find("box_" + gcnew String(it->first.c_str()), true);
					 auto find_check_text = this->Controls->Find("text_" + gcnew String(it->first.c_str()), true);
					 auto find_check_mana = this->Controls->Find("spin_" + gcnew String(it->first.c_str()), true);

					 if (find_check->Length > 0){
						 if (Telerik::WinControls::UI::RadCheckBox::typeid == find_check[0]->GetType()){
							 Telerik::WinControls::UI::RadCheckBox^ radListView = (Telerik::WinControls::UI::RadCheckBox^) find_check[0];

							 if (radListView)
								 radListView->Checked = it->second->get_active();
						 }
					 }
					 if (find_check_text->Length > 0){
						 if (Telerik::WinControls::UI::RadTextBox::typeid == find_check_text[0]->GetType()){
							 Telerik::WinControls::UI::RadTextBox^ radListView = (Telerik::WinControls::UI::RadTextBox^) find_check_text[0];

							 if (radListView)
								 radListView->Text = gcnew String(&it->second->get_value_string()[0]);
						 }
					 }
					 if (find_check_box->Length > 0){
						 if (Telerik::WinControls::UI::RadDropDownList::typeid == find_check_box[0]->GetType()){
							 Telerik::WinControls::UI::RadDropDownList^ radListView = (Telerik::WinControls::UI::RadDropDownList^) find_check_box[0];

							 if (radListView)
								 radListView->Text = gcnew String(&it->second->get_value_string()[0]);
						 }
					 }
					 if (find_check_mana->Length > 0){
						 if (Telerik::WinControls::UI::RadSpinEditor::typeid == find_check_mana[0]->GetType()){
							 Telerik::WinControls::UI::RadSpinEditor^ radListView = (Telerik::WinControls::UI::RadSpinEditor^) find_check_mana[0];

							 if (radListView)
								 radListView->Value = it->second->get_mana();
						 }
					 }
					 if (it->second->script_type == utility_t::utility_eat_food){
						 int value_to_set = (int)it->second->get_delay() == 2000 ? 2 : (int)it->second->get_delay();
						 this->spin_utility_eat_food->Value = value_to_set;
						 DropDownListFoodName->Text = gcnew String(it->second->get_value_string().c_str());
					 }
				 }
				 disable_item_update = false;
			 }
			 void load_relogin(){
				 disable_item_update = true;
				 check_autorelogin->Checked = LoginManager::get()->get_automatic_relogin_state();
				 box_account->Text = gcnew String(LoginManager::get()->get_account().c_str());
				 box_password->Text = gcnew String(LoginManager::get()->get_password().c_str());
				 box_index->Value = LoginManager::get()->get_character_index();
				 disable_item_update = false;
			 }

	private: System::Void Useful_Load(System::Object^  sender, System::EventArgs^  e) {
				 array<String^>^ arrayItemsRing = { "Axe Ring", "Club Ring", "Death Ring", "Dwarven Ring", "Energy Ring", "Life Ring", "Might Ring", "Power Ring", "Prismatic Ring"
					 "Ring of Healing", "Stealth Ring", "Sword Ring", "Time Ring" };

				 for each(auto ring in arrayItemsRing){
					 Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem(ring));
					 radListDataItem1->Text = ring;
					 this->box_utility_auto_ring->Items->Add(radListDataItem1);
				 }


				 auto itemsFood = ItemsManager::get()->get_items_as_food();
				 for (auto item : itemsFood){
					 std::string nameFood = ItemsManager::get()->getItemNameFromId(item);
					 if (nameFood == "")
						 continue;

					 String^ nameFoodEnd = gcnew String(nameFood.c_str());
					 DropDownListFoodName->Items->Add(nameFoodEnd);
				 }

				 loadinform();
				 load_auto_sio();

				 updateFriendItem();

				 load_relogin();
	}


	private: System::Void check_autorelogin_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 LoginManager::get()->set_automatic_relogin_state(check_autorelogin->Checked);
	}
	private: System::Void box_account_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 if (box_account->Text == "")
					 box_account->NullText = "Account...";

				 LoginManager::get()->set_account(managed_util::fromSS(box_account->Text));
	}
	private: System::Void box_password_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 if (box_password->Text == "")
					 box_password->NullText = "Password...";

				 LoginManager::get()->set_password(managed_util::fromSS(box_password->Text));
	}
	private: System::Void box_index_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 LoginManager::get()->set_character_index((int)box_index->Value);
	}

			 void updateFriendItem(){
				 if (disable_item_update)
					 return;

				 Telerik::WinControls::UI::ListViewDataItem^ selectedItem = ListViewFriend->SelectedItem;
				 if (!selectedItem)
					 return;

				 std::string name = managed_util::fromSS((String^)selectedItem["Name"]);
				 std::shared_ptr<ScriptInfo> ruleScriptInfo = UtilityCore::get()->get_rule_scriptInfo("utility_auto_sio");
				 if (!ruleScriptInfo)
					 return;

				 disable_item_update = true;
				 text_utility_auto_sio->Text = (String^)selectedItem["Name"];
				 spin_auto_sio->Value = Convert::ToInt32(selectedItem["Hp"]);
				 old = (String^)selectedItem["Name"];
				 disable_item_update = false;
			 }

	private: System::Void ButtonNew_Click(System::Object^  sender, System::EventArgs^  e) {
				 std::shared_ptr<ScriptInfo> ruleScriptInfo = UtilityCore::get()->get_rule_scriptInfo("utility_auto_sio");
				 if (!ruleScriptInfo)
					 return;

				 String^ newId = gcnew String(ruleScriptInfo->request_new_id().c_str());
				 array<String^>^ columnArrayItems = { newId, "0" };
				 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem("", columnArrayItems);
				 newItem->Text = newId;
				 ListViewFriend->Items->Add(newItem);
				 ListViewFriend->SelectedItem = newItem;

				 updateFriendItem();
	}

	private: System::Void ButtonRemove_Click(System::Object^  sender, System::EventArgs^  e){
				 Telerik::WinControls::UI::ListViewDataItem^ selectedItem = ListViewFriend->SelectedItem;
				 if (!selectedItem)
					 return;

				 std::string name = managed_util::fromSS((String^)selectedItem["Name"]);
				 std::shared_ptr<ScriptInfo> ruleScriptInfo = UtilityCore::get()->get_rule_scriptInfo("utility_auto_sio");
				 if (!ruleScriptInfo)
					 return;

				 ruleScriptInfo->remove_map_friend(name);
				 ListViewFriend->Items->Remove(selectedItem);
	}

	private: System::Void ListViewFriend_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
				 updateFriendItem();
	}

			 void load_auto_sio(){
				 disable_item_update = true;
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo("utility_auto_sio");
				 if (!scriptInfoRule)
					 return;

				 spin_utility_auto_sio->Value = scriptInfoRule->get_mana();
				 utility_auto_sio->Checked = scriptInfoRule->get_active();

				 auto myMap = scriptInfoRule->get_map_friend();
				 for (auto it : myMap){

					 String^ newId = gcnew String(it.first.c_str());
					 array<String^>^ columnArrayItems = { newId, Convert::ToString((int)it.second) };
					 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(newId, columnArrayItems);
					 newItem->Text = newId;
					 ListViewFriend->Items->Add(newItem);
					 ListViewFriend->SelectedItem = newItem;
				 }

				 disable_item_update = false;
			 }

	private: System::Void spin_utility_auto_sio_ValueChanged_1(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_sio);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_auto_sio->Value);
	}
	private: System::Void spin_utility_auto_utura__ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_utura);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_hp((int)spin_utility_auto_utura_->Value);
	}
	private: System::Void utility_auto_utura_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_utura);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_auto_utura->Checked);
	}
	private: System::Void text_utility_auto_utura_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_utura);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(text_utility_auto_utura->Text));
	}
	private: System::Void spin_utility_auto_utura_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_utura);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_auto_utura->Value);
	}
	private: System::Void text_utility_soft_change_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_soft_change);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(text_utility_soft_change->Text));
	}
	private: System::Void spin_utility_soft_change_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_soft_change);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_mana((int)spin_utility_soft_change->Value);
	}
	private: System::Void spin_utility_soft_change__ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_soft_change);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_hp((int)spin_utility_soft_change_->Value);
	}
	private: System::Void DropDownListFoodName_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_eat_food);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_value_string(managed_util::fromSS(DropDownListFoodName->Text));

	}
	private: System::Void utility_soft_change_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_soft_change);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_soft_change->Checked);
	}
	private: System::Void utility_auto_mount_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 std::string keyScriptInfo = UtilityCore::get()->get_script_utility(utility_t::utility_auto_mount);
				 std::shared_ptr<ScriptInfo> scriptInfoRule = UtilityCore::get()->get_rule_scriptInfo(keyScriptInfo);

				 if (!scriptInfoRule)
					 return;

				 scriptInfoRule->set_active(utility_auto_mount->Checked);
	}
	};
}
