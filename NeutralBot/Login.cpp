#include "Core\Login.h"
#include "Core\Time.h"
#include <thread>
#include <mutex>
#include "DevelopmentManager.h"


std::mutex mtx;

void LoginManager::set_automatic_relogin_state(bool state){
	mtx.lock();
	automatic_relogin_state = state;
	mtx.unlock();
}
void LoginManager::set_account(std::string accountt){
	mtx.lock();
	account = accountt;
	mtx.unlock();
}
void LoginManager::set_password(std::string passwordd){
	mtx.lock();
	password = passwordd;
	mtx.unlock();
}
void LoginManager::set_character_index(int character_indexx){
	mtx.lock();
	character_index = character_indexx;
	mtx.unlock();
}

LoginManager::LoginManager(){
	automatic_relogin_state = false;
	account = "";
	password = "";
	character_index = 0;
	std::thread([&](){
		MONITOR_THREAD("LoginManager::refresh");
		refresh(); }).detach(); 
}

void LoginManager::refresh(){
	while (true){
		Sleep(500);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state())
			continue;

		if (!automatic_relogin_state)
			continue;
		
		TryLogin(account, password, character_index);
	}
}

void LoginManager::run(){
}

void LoginManager::CancelAction(){
	TimeChronometer time;
	while (time.elapsed_milliseconds() < HighLevelVirtualInput::get_default()->get_time_out()){
		if (HighLevelVirtualInput::get_default()->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!HighLevelVirtualInput::get_default()->can_use_input())
		return;

	HighLevelVirtualInput::get_default()->send_esc();
}

void LoginManager::LoginWriteString(std::string str){
	TimeChronometer time;
	while (time.elapsed_milliseconds() < HighLevelVirtualInput::get_default()->get_time_out()){
		if (HighLevelVirtualInput::get_default()->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!HighLevelVirtualInput::get_default()->can_use_input())
		return;

	HighLevelVirtualInput::get_default()->send_string(str);
}

void LoginManager::PressLoginButton(){	
	Coordinate size = ClientGeneralInterface::get()->get_client_dimension();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < HighLevelVirtualInput::get_default()->get_time_out()){
		if (HighLevelVirtualInput::get_default()->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!HighLevelVirtualInput::get_default()->can_use_input())
		return;

	HighLevelVirtualInput::get_default()->click_left(80, size.y - 201);
}

void LoginManager::PressEnter(){
	TimeChronometer time;
	while (time.elapsed_milliseconds() < HighLevelVirtualInput::get_default()->get_time_out()){
		if (HighLevelVirtualInput::get_default()->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!HighLevelVirtualInput::get_default()->can_use_input())
		return;

	HighLevelVirtualInput::get_default()->send_enter();
}

void LoginManager::SendTab(){
	TimeChronometer time;
	while (time.elapsed_milliseconds() < HighLevelVirtualInput::get_default()->get_time_out()){
		if (HighLevelVirtualInput::get_default()->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!HighLevelVirtualInput::get_default()->can_use_input())
		return;

	HighLevelVirtualInput::get_default()->sent_tab();
}

std::string LoginManager::GetLoginWindowCaption(){
	int Address_window = AddressManager::get()->getAddress(ADDRESS_WINDOW);
	std::string text;
	
	if (TibiaProcess::get_default()->get_current_tibia_version() >= 1093)
		text = TibiaProcess::get_default()->read_string(Address_window, std::vector<uint32_t>{0x40, 0x24, 0x0});
	else
		text = TibiaProcess::get_default()->read_string(Address_window, std::vector<uint32_t>{0x54});
	
	return text;
}

void LoginManager::TryLogin(std::string account, std::string password, uint32_t character_index, int32_t timeout){
	if (TibiaProcess::get_default()->get_is_logged())
		return;

	CancelAction();
	Sleep(300);
	CancelAction();
	Sleep(300);
	CancelAction();
	
	int try_count = 0;
	boost::chrono::steady_clock::time_point now = boost::chrono::steady_clock::now();
	while ((boost::chrono::duration_cast<boost::chrono::milliseconds>(boost::chrono::steady_clock::now() - now)).count() < timeout){
		PressLoginButton();

		std::string window_caption = GetLoginWindowCaption();
		if (GetLoginWindowCaption() != "Enter Game" && GetLoginWindowCaption() != "Select Character" && GetLoginWindowCaption() != "Sorry" &&
			GetLoginWindowCaption() != "Please Wait" && GetLoginWindowCaption() != "Connecting" && GetLoginWindowCaption().find("Day") == std::string::npos &&
			GetLoginWindowCaption() != "Connection failed"){
			
			PressLoginButton();

			wait_change_caption(window_caption);
			continue;
		}

		window_caption = GetLoginWindowCaption();
		if (window_caption == "Enter Game"){
			LoginWriteString(account);
			Sleep(300);
			SendTab();
			Sleep(300);
			LoginWriteString(password);
			Sleep(300);
			PressEnter();

			wait_change_caption(window_caption);
		}
		else if (window_caption == "Sorry"){
			CancelAction();

			wait_change_caption(window_caption);
		}
		else if (window_caption == "Select Character"){
			selected_char(character_index);

			Sleep(300);

			PressEnter();

			wait_change_caption(window_caption);
		}
		else if (window_caption == "Please Wait"){
			CancelAction();

			wait_change_caption(window_caption);
		}
		else if (window_caption == "Connecting"){	
			Sleep(3000);

			wait_change_caption(window_caption);
		}
		else if (window_caption == "Connection failed"){

			CancelAction();
			Sleep(300);
			CancelAction();
			Sleep(300);

			wait_change_caption(window_caption);
		}
		else if (window_caption.find("Day") != std::string::npos){
			PressEnter();

			wait_change_caption(window_caption);
		}

		try_count++;
	}
}

LoginManager* LoginManager::get(){
	static LoginManager* m_address_static = nullptr;
	if (!m_address_static)
		m_address_static = new LoginManager();
	return m_address_static;
}