local num_to_go = 1;
--square

local cords_center = {x=32918,y=31314,z=10}

local cords = {
    {x=32914,y=31314,z=10},
    {x=32914,y=31316,z=10},
    {x=32922,y=31316,z=10},
    {x=32922,y=31314,z=10},
}
local inside = false

while WaypointManager.is_location(cords_center,15) and Creatures.get_target("War Golem") ~= nil do
    --if (Repoter.is_necessary_all_repot() or Self.cap() < 200) then
    --  WaypointManager.set_current_waypoint_by_label("GoCity")
    --    return
    -- end
    
    inside = true;

    HunterManager.disable_group(1)
    HunterManager.enable_group(2)

    if(WaypointManager.is_location(cords[num_to_go],0) == false) then
        Pathfinder.step_to_coordinate(cords[num_to_go]);
        if num_to_go == 2 or num_to_go == 4 then
            Thread.sleep(300)
        else
            Thread.sleep(1)
        end
    else
        num_to_go = num_to_go +1
        if num_to_go > 4 then
            num_to_go = 1
        end
    end

    while WaypointManager.is_location(cords_center,10) and Creatures.get_target("War Golem") == nil do
        if LooterManager.need_operate() == false then
            WaypointManager.set_enabled_state(false)
           
            Actions.goto_coord(cords_center,0)
            
            if WaypointManager.is_location(cords_center,0) then         
                Actions.use_crosshair_item_on_coordinate({x=Self.posx(),y=Self.posy()+1,z=10},3166)
                Thread.sleep(500)

                local raw_tile = Map.get_raw_tile_at({x=32918,y=31314+1,z=10},true)                
                if(Tile.get_top_item_id(raw_tile) == 2122)then
                    WaypointManager.set_enabled_state(true)
                    break;
                end
            end
        else
            Thread.sleep(200)
        end
    end
    
    if inside then
        if(Status.haste() == false) then
            Actions.says("utani hur")
        end
    end
end

    HunterManager.disable_group(2)
    HunterManager.enable_group(1)

--[[
(retangulo,delay dos passos,(options)de qnt em qnt passos)
fazer funcao para use o item
verificacao de monstrons entre outras coisas fica por conta do usuario

]]--