#pragma once
#include "MonsterManager.h"
#include "HunterManager.h"
#include "ManagedUtil.h"
#include "DepoterManager.h"
#include "LanguageManager.h"
#include "Core\ItemsManager.h"
#include "LooterManager.h"
#include "Core\HunterCore.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class AutoGenerateLoot : public Telerik::WinControls::UI::RadForm{
	public:
		AutoGenerateLoot(String^ FormName){
			comfirmed = false;
			InitializeComponent();

			formName = FormName;
			selectedItems = gcnew System::Collections::Generic::List<String^>();

			this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
			std::vector<std::string> items_ids;

			if (formName == "Looter"){
				TargetPtrMap targets = HunterManager::get()->get_target_rules();
				for (auto target = targets.begin(); target != targets.end(); target++){
					std::string name = target->second->get_name();

					auto creature = MonsterManager::get()->get_creature_by_name(name);
					if (creature)
						for (auto item_id = creature->items_id.begin(); item_id != creature->items_id.end(); item_id++)
							if (std::find(items_ids.begin(), items_ids.end(), *item_id) == items_ids.end())
								items_ids.push_back(*item_id);

				}
			}
			else
				items_ids = LooterManager::get()->get_all_items_name();

			std::sort(items_ids.begin(), items_ids.end());

			radCheckedListBox1->BeginUpdate();
			for (auto item = items_ids.begin(); item != items_ids.end(); item++){
				uint32_t item_id = ItemsManager::get()->getitem_idFromName(*item);
				if (item_id == UINT32_MAX || item_id == 0)
					continue;

				AddListViewItem(gcnew String(&(*item)[0]));
			}
			radCheckedListBox1->EndUpdate();
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}

		AutoGenerateLoot(int type){
			comfirmed = false;
			selectedItems = gcnew System::Collections::Generic::List<String^>();
			InitializeComponent();

			radCheckedListBox1->BeginUpdate();
			if (type == 1){
				TargetPtrMap targets = HunterManager::get()->get_target_rules();
				for (auto target = targets.begin(); target != targets.end(); target++){
					std::string name = target->second->get_name();

					if (!check_exist(string_util::lower(name)))
						AddListViewItem(gcnew String(&(name)[0]));
				}
			}
			else{
				auto creatues_names = BattleList::get()->get_monsters_appeared();
				for (auto name : creatues_names){
					if (!check_exist(string_util::lower(name)))
						AddListViewItem(gcnew String(&(name)[0]));
				}
			}

			radCheckedListBox1->EndUpdate();
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}

		String^ formName;
		bool check_exist(std::string name){
			for each (auto item in radCheckedListBox1->Items){
				if (item->Text == gcnew String(name.c_str()))
					return true;
			}
			return false;
		}

		void AddListViewItem(String^ item_name){
			if (radCheckedListBox1->Items->IndexOf(item_name) != -1)
				return;

			Telerik::WinControls::UI::ListViewDataItem^ item = gcnew Telerik::WinControls::UI::ListViewDataItem(item_name);

			uint32_t item_id = ItemsManager::get()->getitem_idFromName(managed_util::fromSS(item_name));
			if (formName == "Looter")
				if (LooterManager::get()->get_container_owns_by_item_id(item_id))
					item->Enabled = false;

			if (formName == "Depoter")
				if (DepoterManager::get()->contains_item_id(item_id))
					item->Enabled = false;

			radCheckedListBox1->Items->Add(item);

			item->CheckState = Telerik::WinControls::Enumerations::ToggleState::Off;
		}

	protected: ~AutoGenerateLoot(){
				   if (components)
					   delete components;
	}

	public: bool comfirmed;
	public: System::Collections::Generic::List<String^>^ selectedItems;
			Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
			Telerik::WinControls::UI::RadButton^  radButton1;
			Telerik::WinControls::UI::RadButton^  radButton2;
			Telerik::WinControls::UI::RadCheckedListBox^  radCheckedListBox1;
	protected: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			   void InitializeComponent(void){
				   Telerik::WinControls::UI::ListViewDataItem^  listViewDataItem1 = (gcnew Telerik::WinControls::UI::ListViewDataItem(L"Select all"));
				   Telerik::WinControls::Data::SortDescriptor^  sortDescriptor1 = (gcnew Telerik::WinControls::Data::SortDescriptor());
				   this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				   this->radCheckedListBox1 = (gcnew Telerik::WinControls::UI::RadCheckedListBox());
				   this->radButton1 = (gcnew Telerik::WinControls::UI::RadButton());
				   this->radButton2 = (gcnew Telerik::WinControls::UI::RadButton());
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				   this->radGroupBox1->SuspendLayout();
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckedListBox1))->BeginInit();
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->BeginInit();
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton2))->BeginInit();
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				   this->SuspendLayout();
				   // 
				   // radGroupBox1
				   // 
				   this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				   this->radGroupBox1->Controls->Add(this->radCheckedListBox1);
				   this->radGroupBox1->HeaderText = L"Items";
				   this->radGroupBox1->Location = System::Drawing::Point(3, 2);
				   this->radGroupBox1->Name = L"radGroupBox1";
				   this->radGroupBox1->Size = System::Drawing::Size(277, 282);
				   this->radGroupBox1->TabIndex = 1;
				   this->radGroupBox1->Text = L"Items";
				   // 
				   // radCheckedListBox1
				   // 
				   listViewDataItem1->Text = L"Select all";
				   this->radCheckedListBox1->Items->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDataItem^  >(1) { listViewDataItem1 });
				   this->radCheckedListBox1->Location = System::Drawing::Point(9, 18);
				   this->radCheckedListBox1->Name = L"radCheckedListBox1";
				   this->radCheckedListBox1->ShowGridLines = true;
				   this->radCheckedListBox1->Size = System::Drawing::Size(260, 256);
				   this->radCheckedListBox1->SortDescriptors->AddRange(gcnew cli::array< Telerik::WinControls::Data::SortDescriptor^  >(1) { sortDescriptor1 });
				   this->radCheckedListBox1->TabIndex = 0;
				   this->radCheckedListBox1->Text = L"radCheckedListBox1";
				   this->radCheckedListBox1->ItemCheckedChanged += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &AutoGenerateLoot::radCheckedListBox1_ItemCheckedChanged);
				   // 
				   // radButton1
				   // 
				   this->radButton1->Location = System::Drawing::Point(3, 288);
				   this->radButton1->Name = L"radButton1";
				   this->radButton1->Size = System::Drawing::Size(132, 24);
				   this->radButton1->TabIndex = 2;
				   this->radButton1->Text = L"Confirm";
				   this->radButton1->Click += gcnew System::EventHandler(this, &AutoGenerateLoot::radButton1_Click);
				   // 
				   // radButton2
				   // 
				   this->radButton2->Location = System::Drawing::Point(148, 288);
				   this->radButton2->Name = L"radButton2";
				   this->radButton2->Size = System::Drawing::Size(132, 24);
				   this->radButton2->TabIndex = 3;
				   this->radButton2->Text = L"Cancel";
				   this->radButton2->Click += gcnew System::EventHandler(this, &AutoGenerateLoot::radButton2_Click);
				   // 
				   // AutoGenerateLoot
				   // 
				   this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				   this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				   this->ClientSize = System::Drawing::Size(284, 317);
				   this->Controls->Add(this->radButton2);
				   this->Controls->Add(this->radButton1);
				   this->Controls->Add(this->radGroupBox1);
				   this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				   this->MaximizeBox = false;
				   this->Name = L"AutoGenerateLoot";
				   // 
				   // 
				   // 
				   this->RootElement->ApplyShapeToControl = true;
				   this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				   this->Text = L"AutoGenerateLoot";
				   this->Load += gcnew System::EventHandler(this, &AutoGenerateLoot::AutoGenerateLoot_Load);
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				   this->radGroupBox1->ResumeLayout(false);
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckedListBox1))->EndInit();
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->EndInit();
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton2))->EndInit();
				   (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				   this->ResumeLayout(false);

			   }
#pragma endregion

			   System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
				   comfirmed = true;
				   this->Close();
			   }
			   System::Void radCheckedListBox1_ItemCheckedChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
				  int index = radCheckedListBox1->Items->IndexOf(e->Item);

				   if (index < 0)
					   return;
				   else if (index == 0){
					   if (e->Item->CheckState == Telerik::WinControls::Enumerations::ToggleState::On){
						   for each(auto item in radCheckedListBox1->Items){
							   if (!item->Enabled)
								   continue;

							   if (item->CheckState != Telerik::WinControls::Enumerations::ToggleState::On)
								   item->CheckState = Telerik::WinControls::Enumerations::ToggleState::On;
						   }
						   return;
					   }
					   else if (e->Item->CheckState == Telerik::WinControls::Enumerations::ToggleState::Off){
						   for each(auto item in radCheckedListBox1->Items){
							   if (item->CheckState != Telerik::WinControls::Enumerations::ToggleState::Off)
								   item->CheckState = Telerik::WinControls::Enumerations::ToggleState::Off;
						   }
						   return;
					   }
				   }

				   Telerik::WinControls::UI::ListViewDataItem^ item = (Telerik::WinControls::UI::ListViewDataItem^)e->Item;
				   String^ item_name = (String^)item->Value;

				   if (item->CheckState == Telerik::WinControls::Enumerations::ToggleState::On)
					   selectedItems->Add(item_name);
				   else
					   selectedItems->Remove(item_name);
			   }
			   System::Void radButton2_Click(System::Object^  sender, System::EventArgs^  e) {
				   this->Close();
			   }

			   void update_idiom(){
				   radButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton1->Text))[0]);
				   radButton2->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton2->Text))[0]);
			   }

			   System::Void AutoGenerateLoot_Load(System::Object^  sender, System::EventArgs^  e) {
				   update_idiom();
			   }
	};
}
