

function dectohex(num)
	return tonumber(num, 16);
end

lookAddressIdChar					= dectohex("0x6D5034")
lookAddressPrimeiroIdBattle 		= dectohex("0x72F6F0") 
AddressSkullModeNew					= dectohex("0x5488F4")
AddressSkullModeOld					= dectohex("0x54891A")
lookAddressLogged					= dectohex("0x53770C")

AddressBasePing						= {
	dectohex("0x1A14C"), dectohex("0x4")
};

AddressActualHotkeySchema 			= dectohex("0x521C08")

AddressHWND							= dectohex("0x537654")
AddressTreino						= dectohex("0x537664")
XorAddress							= dectohex("0x5376A0")
AddressExp							= dectohex("0x5376A8")
AddressLvl							= dectohex("0x5376B8")
AddressSoul							= dectohex("0x5376BC")
AddressMagicLvl						= dectohex("0x5376C0")
AddressMagicLvlPc					= dectohex("0x5376C8")
AddressTargetRed					= dectohex("0x5376CC")
AddressMp							= dectohex("0x5376D0")
AddressFirstPc						= dectohex("0x5376D8")
AddressWhiteTarget					= dectohex("0x5376F4")
AddressStamina						= dectohex("0x537704")
AddressStatus						= dectohex("0x53765C")

AddressOfWindow						= dectohex("0x537970")
AddressOfBpPointer					= dectohex("0x53796C")
AdressEquip							= dectohex("0x53796C")

AddressReceivBuffer					= dectohex("0x537394")

AddressMouseX1						= dectohex("0x5486D4")
AddressFollow						= dectohex("0x5478D0")
AddressCooldownBar					= dectohex("0x5487EA")
AddressTelaY						= dectohex("0x5491C8")
address_mouse						= dectohex("0x5486D4")
address_mouse_2						= address_mouse;

AddressTelaX						= dectohex("0x549164")
AddressServerMessage				= dectohex("0x58AFE0")
AddressMessagePlayer				= AddressServerMessage
AddressMessageWarning				= dectohex("0x58ADD4")
AddressMessageNotPossible			= AddressMessageWarning 
address_pointer_spells_basic		= dectohex("0x58CA58")
address_pointer_spells				= address_pointer_spells_basic 	+ 8
address_total_spells				= address_pointer_spells 		+ 4
AddressExpHour						= dectohex("0x58CAEC")

addressOfFirstMap					= dectohex("0x590A70")
pointer_vip_players					= dectohex("0x6D0FEC")
AddressBaseLogList					= dectohex("0x6AF8D0")
AddressCtrl							= dectohex("0x6D11E8")
AddressSchemaHotkeyReference		= dectohex("0x6D129C")
address_item_to_be_used				= dectohex("0x6D14D8")
AddressMouseX2						= dectohex("0x6D14F0")

--6FF4F0
AddressMouseY2						= AddressMouseX2 + 4;
address_item_to_be_moved			= dectohex("0x6D1500")

AddressCap							= dectohex("0x6D5024")

AddressHp							= dectohex("0x6D5000")
AddressZGO							= dectohex("0x6D5004")
AddressFirst						= dectohex("0x6D5008")

AddressYGO							= dectohex("0x6D5028")
AddressMaxHp						= dectohex("0x6D502C")
AddressXGO							= dectohex("0x6D5030")

AddressY							= dectohex("0x6D5038");
AddressX							= AddressY + 8
AddressZ							= AddressY + 4

base_address_in_gui					= dectohex("0x7756F4")
AddressHelmet						= dectohex("0x775898")

PointerInicioMap					= dectohex("0x7758E0")
PointerInicioOffsetMap				= dectohex("0x77A410")

address_bp_base_new					= dectohex("0x77B9AC")
address_tibia_time					= dectohex("0x77C488")

AddressMouseY1						= AddressMouseX1 + 4
AddressMaxMp						= XorAddress + 4;


AddressClubPc					= AddressFirstPc + 4;
AddressSwordPc					= AddressFirstPc + 4 * 2;
AddressAxePc					= AddressFirstPc + 4 * 3;
AddressDistancePc				= AddressFirstPc + 4 * 4;
AddressShieldingPc				= AddressFirstPc + 4 * 5;
AddressFishingPc				= AddressFirstPc + 4 * 6;

AddressMouse_fix_x				= AddressMouseX1;
AddressMouse_fix_y				= AddressMouse_fix_x + 4;

AddressClub						= AddressFirst		+ 4;
AddressSword					= AddressFirst		+ 4 *2;
AddressAxe						= AddressFirst		+ 4 *3;
AddressDistance					= AddressFirst		+ 4 *4;
AddressShielding				= AddressFirst		+ 4 *5;
AddressFishing					= AddressFirst		+ 4 *6;

offset_beetwen_body_item		= dectohex("0x20");	

AddressAmulet					= AddressHelmet		- offset_beetwen_body_item * 1;
AddressBag						= AddressHelmet		- offset_beetwen_body_item * 2;
AddressMainBp					= AddressBag;
AddressArmor					= AddressHelmet		- offset_beetwen_body_item * 3;
AddressShield					= AddressHelmet		- offset_beetwen_body_item * 4;
AddressWeapon					= AddressHelmet		- offset_beetwen_body_item * 5;
AddressLeg						= AddressHelmet		- offset_beetwen_body_item * 6;
AddressBoot						= AddressHelmet		- offset_beetwen_body_item * 7;
AddressRing						= AddressHelmet		- offset_beetwen_body_item * 8;
AddressRope						= AddressHelmet		- offset_beetwen_body_item * 9;
AddressLogged					= lookAddressLogged;	
AddressIdChar					= lookAddressIdChar;
AddressPrimeiroNomeBattle		= lookAddressPrimeiroIdBattle + 4;




AddressAcc							= dectohex("0x5D1380")--ignore deprecated
AddressPass							= dectohex("0x5D138C")--ignore deprecated
AddressIndexChar					= dectohex("0x5D13BC")--ignore deprecated


