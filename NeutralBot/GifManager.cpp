#include "GifManager.h"
#include <vector>
#include <map>
#include <stdint.h>
#include <boost\filesystem.hpp>
#include "Core\Util.h"
#include <thread>
#include "DevelopmentManager.h"
#include "Core\Neutral.h"

using namespace boost::filesystem;

void GifManager::items_load(){
	MONITOR_THREAD(__FUNCTION__)
	std::map<std::string, std::pair<char*, uint32_t>> temp_gifs_map = get_unziped_files(".\\gifs\\items.gifdat");

	lock_guard _lock(mtx_access);
	if (item_ref)
		item_gifs_ptr = temp_gifs_map;
	else{
		for (auto file_buffer = temp_gifs_map.begin(); file_buffer !=
			temp_gifs_map.end(); file_buffer++){
			delete[] file_buffer->second.first;
		}
	}
	loading_items = false;
}

void GifManager::creatures_load(){
	MONITOR_THREAD(__FUNCTION__)
	std::map<std::string, std::pair<char*, uint32_t>> temp_gifs_map = get_unziped_files(".\\gifs\\creatures.gifdat");

	lock_guard _lock(mtx_access);
	if (creatures_ref)
		creature_gifs_ptr = temp_gifs_map;
	else{
		for (auto file_buffer = temp_gifs_map.begin(); file_buffer !=
			temp_gifs_map.end(); file_buffer++){
			delete[] file_buffer->second.first;
		}
	}
	loading_creatures = false;
}

void GifManager::items_release(){
	for (auto it = item_gifs_ptr.begin(); it != item_gifs_ptr.end(); it++){
		delete[] it->second.first;
	}
	item_gifs_ptr.clear();
}

bool GifManager::has_item(std::string& item_name){
	item_name = item_name + ".gif";
	for (auto item_pair : item_gifs_ptr){
		if (_stricmp(&item_pair.first[0], &item_name[0]) == 0)
			return true;
	}
	return false;
}

void GifManager::creatures_release(){
	for (auto it = creature_gifs_ptr.begin(); it != creature_gifs_ptr.end(); it++){
		delete [] it->second.first;
	}
	creature_gifs_ptr.clear();
}

void GifManager::start_load_creature(){
	loading_creatures = true;
	std::thread(std::bind(&GifManager::creatures_load, this)).detach(); 
	//creatures_load();
}

void GifManager::start_load_items(){
	loading_items = true;
	std::thread(std::bind(&GifManager::items_load, this)).detach(); 
	//items_load();
}

void GifManager::request_items_load(){
	lock_guard _lock(mtx_access);
	item_ref++;
	if (!loading_items && !item_gifs_ptr.size()){
		start_load_items();
	}
}

void GifManager::request_items_unload(){
	lock_guard _lock(mtx_access);
	item_ref--;
	if (!item_ref){
		items_release();
	}
}

void GifManager::request_load_creatures(){
	lock_guard _lock(mtx_access);
	creatures_ref++;
	if (!loading_creatures && !creature_gifs_ptr.size()){
		start_load_creature();
	}
}

void GifManager::request_unload_creatures(){
	return;
	lock_guard _lock(mtx_access);
	creatures_ref--;
	if (!creatures_ref){
		creatures_release();
	}
}

std::pair<char*, uint32_t> GifManager::get_item_gif_buffer(std::string file_name){
	lock_guard _lock(mtx_access);
	for (auto it = item_gifs_ptr.begin(); it != item_gifs_ptr.end(); it++){
		if (_stricmp(&file_name[0], &it->first[0]) == 0){
			return it->second;
		}
	}
	return std::pair<char*, uint32_t>(0, 0);
}

std::pair<char*, uint32_t> GifManager::get_item_creature_buffer(std::string file_name){
	lock_guard _lock(mtx_access);
	for (auto it = creature_gifs_ptr.begin(); it != creature_gifs_ptr.end(); it++){
		if (_stricmp(&file_name[0], &it->first[0]) == 0){
			return it->second;
		}
	}
	return std::pair<char*, uint32_t>(0, 0);
}
