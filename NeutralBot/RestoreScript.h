#pragma once
#include "Core\Neutral.h"
#include "ManagedUtil.h"
#include "Panorama.h"

namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class RestoreScript : public Telerik::WinControls::UI::RadForm{
	public:	RestoreScript(void){
				InitializeComponent();
	}
	protected:	~RestoreScript(){
					unique = nullptr;
					if (components)
						delete components;
	}
	private: Telerik::WinControls::UI::RadContextMenuManager^  radContextMenuManager1;
	protected:
	private: Telerik::WinControls::UI::RadContextMenu^  radContextMenu1;
	private: Telerik::WinControls::UI::RadMenuItem^  MenuRestoreScript;

	protected: static RestoreScript^ unique;

	public:	static void CloseUnique(){
				if (unique)unique->Close();
	}
	public: static void ShowUnique(){
				if (unique == nullptr)
					unique = gcnew RestoreScript();

				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}

	private: Telerik::WinControls::UI::RadListView^  radListView1;
	private: System::ComponentModel::IContainer^  components;
	protected:



#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 this->components = (gcnew System::ComponentModel::Container());
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Name",
					 L"Name"));
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Day",
					 L"Day"));
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Hour",
					 L"Hour"));
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn4 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Minute",
					 L"Minute"));
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(RestoreScript::typeid));
				 this->radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
				 this->radContextMenu1 = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
				 this->MenuRestoreScript = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->radContextMenuManager1 = (gcnew Telerik::WinControls::UI::RadContextMenuManager());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // radListView1
				 // 
				 this->radListView1->AllowColumnResize = false;
				 this->radListView1->AllowEdit = false;
				 this->radListView1->AllowRemove = false;
				 listViewDetailColumn1->HeaderText = L"Name";
				 listViewDetailColumn1->MaxWidth = 250;
				 listViewDetailColumn1->MinWidth = 250;
				 listViewDetailColumn1->Width = 250;
				 listViewDetailColumn2->HeaderText = L"Day";
				 listViewDetailColumn2->MinWidth = 50;
				 listViewDetailColumn2->Width = 50;
				 listViewDetailColumn3->HeaderText = L"Hour";
				 listViewDetailColumn3->MaxWidth = 50;
				 listViewDetailColumn3->MinWidth = 50;
				 listViewDetailColumn3->Width = 50;
				 listViewDetailColumn4->HeaderText = L"Minute";
				 listViewDetailColumn4->MaxWidth = 50;
				 listViewDetailColumn4->MinWidth = 50;
				 listViewDetailColumn4->Width = 50;
				 this->radListView1->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(4) {
					 listViewDetailColumn1,
						 listViewDetailColumn2, listViewDetailColumn3, listViewDetailColumn4
				 });
				 this->radListView1->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->radListView1->EnableColumnSort = true;
				 this->radListView1->EnableSorting = true;
				 this->radListView1->ItemSpacing = -1;
				 this->radListView1->Location = System::Drawing::Point(0, 0);
				 this->radListView1->Name = L"radListView1";
				 this->radContextMenuManager1->SetRadContextMenu(this->radListView1, this->radContextMenu1);
				 this->radListView1->ShowGridLines = true;
				 this->radListView1->Size = System::Drawing::Size(418, 261);
				 this->radListView1->TabIndex = 0;
				 this->radListView1->Text = L"radListView1";
				 this->radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				 // 
				 // radContextMenu1
				 // 
				 this->radContextMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->MenuRestoreScript });
				 // 
				 // MenuRestoreScript
				 // 
				 this->MenuRestoreScript->AccessibleDescription = L"Restore Script";
				 this->MenuRestoreScript->AccessibleName = L"Restore Script";
				 this->MenuRestoreScript->Name = L"MenuRestoreScript";
				 this->MenuRestoreScript->Text = L"Restore Script";
				 // 
				 // RestoreScript
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(418, 261);
				 this->Controls->Add(this->radListView1);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::SizableToolWindow;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->MaximizeBox = false;
				 this->Name = L"RestoreScript";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->Text = L"RestoreScript";
				 this->Load += gcnew System::EventHandler(this, &RestoreScript::RestoreScript_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion

			 void update_idiom(){
				 for each(auto colum in radListView1->Columns)
					 colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
			 }

			 System::Void MenuRestoreScript_Click(Object^ sender, EventArgs^ e){
				 if (!radListView1->SelectedItem)
					 return;

				 std::string file_path = DEFAULT_DIR_SETTINGS_PATH + "\\TEMPSCRIPT\\" + managed_util::fromSS((String^)radListView1->SelectedItem["Name"]);
				 
				 if (NeutralManager::get()->load_script(file_path)){
					 NeutralBot::Panorama::closeAllOtherForms();
					 Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded.", "Save/Load");
				 }
				 else
					 Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded.", "Save/Load");

				 managed_util::ManagedControlsHook::get()->updateButtonsByVars();
			 }
	private: System::Void RestoreScript_Load(System::Object^  sender, System::EventArgs^  e) {
				 update_idiom();
				 this->MenuRestoreScript->Click += gcnew System::EventHandler(this, &RestoreScript::MenuRestoreScript_Click);

				 std::vector<std::pair<std::string, std::time_t>>  vector_files = NeutralManager::get()->files_in_path(DEFAULT_DIR_SETTINGS_PATH + "\\TEMPSCRIPT");

				 radListView1->BeginUpdate();
				 for (auto file : vector_files){
					 Telerik::WinControls::UI::ListViewDataItem^ radItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(file.first.c_str()));
					 std::time_t time = file.second;
					 tm *gmtm = gmtime(&time);
					 radListView1->Items->Add(radItem);

					 radItem->Text = gcnew String(file.first.c_str());

					 radItem["Name"] = gcnew String(file.first.c_str());

					 if (gmtm->tm_min > 10)
						 radItem["Minute"] = gmtm->tm_min;					 
					 else
						 radItem["Minute"] = "0" + gmtm->tm_min;
					 
					 if (gmtm->tm_hour > 10)
						 radItem["Hour"] = gmtm->tm_hour;
					 else
						 radItem["Hour"] = "0" + gmtm->tm_hour;

					 if (gmtm->tm_mday > 10)
						 radItem["Day"] = gmtm->tm_mday;
					 else
						 radItem["Day"] = "0" + gmtm->tm_mday;
				 }			 
				 radListView1->EndUpdate();
	}
	};
}
