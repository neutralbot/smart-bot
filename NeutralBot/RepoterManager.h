#pragma once
#include <string>
#include <map>
#include "xml.h"
#include <memory>
#include <vector>
#include "Core\Util.h"
#include <json\json.h>
#include "Core\ItemsManager.h"
#include "Core\ContainerManager.h"

struct RepotItens{

	int item_id;

public:
	std::string itemName;
	uint32_t min;
	uint32_t max;
	int gpeach;
	int get_item_id();

	void set_item_id(uint32_t item_id);
	void set_item_name(std::string id){
		item_id = ItemsManager::get()->getitem_idFromName(id);
		itemName = id;
	}
	
	std::string get_itemName();

	DEFAULT_GET_SET(uint32_t, min);
	DEFAULT_GET_SET(uint32_t, max);
	DEFAULT_GET_SET(int, gpeach);
};

class RepotId{
	static std::shared_ptr<RepotId> mRepotId;
	std::map<std::string, std::shared_ptr<RepotItens>> mapRepotItens;

public:
	static void reset();
	void clear();
	std::shared_ptr<RepotItens> getRepotItensRule(std::string id);


	void set_Item(std::string id, std::string name);

	void set_Min(std::string id, uint32_t min);

	void set_Max(std::string id, uint32_t max);

	void set_GpEach(std::string id, uint32_t gpeach);

	std::string get_Item(std::string id);

	uint32_t get_Min(std::string id, uint32_t min);

	uint32_t get_Max(std::string id, uint32_t max);

	uint32_t get_GpEach(std::string id, uint32_t gpeach);

	void removeRepotItem(std::string id);
	std::map<std::string, std::shared_ptr<RepotItens>> getMapRepotItens();
	std::string requestNewItemById(std::string current_id);
	std::string updateItem(std::string Item, int min, int max, int gpeach);
	std::string requestNewitem_id();

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);

	static std::shared_ptr<RepotId> get(){
		if (!mRepotId)
			reset();
		return mRepotId;
	}
};

class RepoterManager {
	bool reorganize_containers = true;
	bool minimize_all_containers = false;

	static std::shared_ptr<RepoterManager> mRepoterManager;
	std::map<std::string, std::shared_ptr<RepotId>> mapRepotId;

	RepoterManager();
public:
	DEFAULT_GET_SET(bool, reorganize_containers);
	DEFAULT_GET_SET(bool, minimize_all_containers);

	static void reset();
	void clear();
	void InitializeMap();

	std::vector<std::shared_ptr<RepotItens>> get_all_itens(){
		std::vector<std::shared_ptr<RepotItens>> retval;
		for (auto repotId : mapRepotId){
			std::map<std::string, std::shared_ptr<RepotItens>> myMap = repotId.second->getMapRepotItens();

			for (auto repotItem : myMap)
				retval.push_back(repotItem.second);			
		}
		return retval;
	}
	void removeRepotId(std::string id);
	bool changeRepotId(std::string newId, std::string oldId);
	std::string RepoterManager::requestNewRepotId();
	std::shared_ptr<RepotId> getRepoIdRule(std::string id_);
	std::map<std::string, std::shared_ptr<RepotId>> getMapRepotId();

	bool checkNecessaryRepot();
	bool checkNecessaryRepot(std::string repotId);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);

	static std::shared_ptr<RepoterManager> get();
};


