
#pragma pack(push, 1)
template <typename valueT>
class stringmap :public std::enable_shared_from_this<stringmap<valueT>> {
public:
	void set_value(valueT _value){
		is_null = false;
		value = _value;

	}
	valueT get_value(){
		return value;
	}

	stringmap(std::shared_ptr<stringmap<valueT>> parent = nullptr){
		this->parent = parent;
		is_null = true;
	}

	~stringmap(){
	}

	void set(std::string key, valueT value){
		set((unsigned char*)&key[0], key.length(), value);
	}

	valueT find(std::string key, bool ignore_case = true){
		int len = key.length();
		return find((unsigned char*)&key[0], len, ignore_case);
	}

	void clear(){
		for (auto it : childs)
			it.second->clear();
		childs.clear();
		parent = std::shared_ptr<stringmap<valueT>>();
	}
	void erase(std::string key){
		erase((unsigned char*)&key[0], key.length());
	}
private:

	std::map <unsigned char, std::shared_ptr<stringmap<valueT>> > childs;
	valueT value;
	bool is_null;
	void set(unsigned char* str, int key_len, valueT value){
		if (key_len > 0){
			std::shared_ptr<stringmap<valueT>> next
				= childs[*str];
			if (!next){
				next = std::shared_ptr < stringmap >(new stringmap<valueT>(shared_from_this()));
				childs[*str] = next;
			}

			if (key_len == 1){
				next->set_value(value);
			}
			else{
				next->set(str + 1, key_len - 1, value);
			}
		}
	}




	valueT find(unsigned char* str, int key_len, bool ignore_case){

		if (key_len > 1){

			if (ignore_case){
				valueT retval;

				auto next
					= childs.find((unsigned char)std::tolower((char)*str));
				if (next != childs.end())
					retval = next->second->find(str + 1, key_len - 1, ignore_case);
				if (retval)
					return retval;

				next = childs.find((unsigned char)std::toupper((char)*str));
				if (next != childs.end())
					retval = next->second->find(str + 1, key_len - 1, ignore_case);
				return retval;
			}
			else
			{
				auto next
					= childs.find(*str);
				if (next == childs.end())
					return valueT();
				return (valueT)next->second->find(str + 1, key_len - 1, ignore_case);
			}


		}
		else if (key_len == 1){

			if (ignore_case){
				valueT retval;

				auto next
					= childs.find((unsigned char)std::tolower((char)*str));
				if (next != childs.end())
					return next->second->get_value();

				next = childs.find((unsigned char)std::toupper((char)*str));
				if (next != childs.end())
					retval = next->second->get_value();
				return retval;
			}
			else
			{
				auto next
					= childs.find(*str);
				if (next == childs.end())
					return valueT();
				return (valueT)next->second->get_value();
			}
		}
		else
			return valueT();




		if (key_len >= 0){
			std::shared_ptr<stringmap<valueT>> next
				= childs[*str];
			if (!next){
				next = std::shared_ptr < stringmap >(new stringmap<valueT>(shared_from_this()));
				childs[*str] = next;
			}

			if (key_len == 0){
				next->set_value(value);
			}
			else{
				next->set(str + 1, key_len - 1, value);
			}
		}














	}

	void erase(unsigned char* str, int key_len){
		if (key_len > 1){
			auto next
				= childs.find(*str);
			if (next == childs.end())
				return;
			next->second->erase(str + 1, key_len - 1);
		}
		else if (key_len == 1){
			if (!childs.size()){
				if (parent)
					parent->erase_chield(shared_from_this());
			}
			if (!is_null){
				value = valueT();
				is_null = true;
			}

		}
		else
			return;
	}

protected:


	void erase_chield(std::shared_ptr<stringmap<valueT>> chield){
		std::map <unsigned char, std::shared_ptr<stringmap<valueT>>>::iterator it = childs.begin();
		for (; it != childs.end(); it++)
			if (it->second == chield)
				break;

		if (it != childs.end())
			childs.erase(it);

		if (!parent)
			return;

		if (!childs.size() && is_null)
			parent->erase_chield(shared_from_this());
	}
	std::shared_ptr<stringmap<valueT>> parent;


};

#pragma pack(pop)