#include "MonsterInfo.h"

MonsterInfo::MonsterInfo(){
}

MonsterInfo::MonsterInfo(std::string name, int min_hp, int max_hp,int id){
	set_name(name);
	set_min_hp(min_hp);
	set_id(id);
	set_max_hp(max_hp);
}