#pragma once
#include "ManagedUtil.h"

namespace Neutral {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for LooterStatistics
	/// </summary>
	public ref class LooterStatistics : public Telerik::WinControls::UI::RadForm
	{
	public:
		LooterStatistics(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~LooterStatistics()
		{
			if (components)
			{
				mLooterStatistics = nullptr;
				delete components;
			}
		}

	protected:
		static LooterStatistics^ mLooterStatistics = nullptr;

	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;		
				if (!mLooterStatistics)
					mLooterStatistics = gcnew LooterStatistics;
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				mLooterStatistics->Show();
	}	
	public: static void CreatUnique(){
				if (mLooterStatistics == nullptr)
					mLooterStatistics = gcnew LooterStatistics();
	}

	 Telerik::WinControls::UI::RadListView^  listViewToPerform;
	 System::Windows::Forms::Timer^  timer1;
	 Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer1;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel1;
	public:

	 Telerik::WinControls::UI::RadPanel^  radPanel2;
	 Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer3;
	 Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer5;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel5;
	public:
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel2;



	 Telerik::WinControls::UI::RadPanel^  radPanel1;
	 Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer2;
	 Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer4;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel7;
	public:
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel3;



	 Telerik::WinControls::UI::RadListView^  radListViewPerformed;
	private: System::ComponentModel::IContainer^  components;
	public:


	
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"name",
				L"name"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"diedtime",
				L"diedtime"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"message",
				L"message"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn4 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"stack",
				L"stack"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn5 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"timeout",
				L"timeout"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn6 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"fail",
				L"fail"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"position",
				L"position"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn8 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"finalstate",
				L"finalstate"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn9 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"name",
				L"name"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn10 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"diedtime",
				L"diedtime"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn11 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"message",
				L"message"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn12 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"stack",
				L"stack"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn13 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"timeout",
				L"timeout"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn14 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"fail",
				L"fail"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn15 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"position",
				L"position"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn16 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"finalstate",
				L"finalstate"));
			this->listViewToPerform = (gcnew Telerik::WinControls::UI::RadListView());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->radSplitContainer1 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
			this->splitPanel1 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radSplitContainer3 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
			this->radSplitContainer5 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
			this->splitPanel5 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->splitPanel2 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radSplitContainer2 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
			this->radSplitContainer4 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
			this->splitPanel7 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->radListViewPerformed = (gcnew Telerik::WinControls::UI::RadListView());
			this->splitPanel3 = (gcnew Telerik::WinControls::UI::SplitPanel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->listViewToPerform))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer1))->BeginInit();
			this->radSplitContainer1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel1))->BeginInit();
			this->splitPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer3))->BeginInit();
			this->radSplitContainer3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer5))->BeginInit();
			this->radSplitContainer5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel5))->BeginInit();
			this->splitPanel5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel2))->BeginInit();
			this->splitPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer2))->BeginInit();
			this->radSplitContainer2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer4))->BeginInit();
			this->radSplitContainer4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel7))->BeginInit();
			this->splitPanel7->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListViewPerformed))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// listViewToPerform
			// 
			this->listViewToPerform->AllowEdit = false;
			this->listViewToPerform->AllowRemove = false;
			listViewDetailColumn1->HeaderText = L"name";
			listViewDetailColumn1->MinWidth = 0;
			listViewDetailColumn2->HeaderText = L"diedtime";
			listViewDetailColumn2->MinWidth = 0;
			listViewDetailColumn2->Width = 50;
			listViewDetailColumn3->HeaderText = L"message";
			listViewDetailColumn3->MinWidth = 0;
			listViewDetailColumn4->HeaderText = L"stack";
			listViewDetailColumn4->MinWidth = 0;
			listViewDetailColumn4->Width = 150;
			listViewDetailColumn5->HeaderText = L"timeout";
			listViewDetailColumn5->MinWidth = 0;
			listViewDetailColumn5->Width = 50;
			listViewDetailColumn6->HeaderText = L"fail";
			listViewDetailColumn6->MinWidth = 0;
			listViewDetailColumn6->Width = 50;
			listViewDetailColumn7->HeaderText = L"position";
			listViewDetailColumn7->MinWidth = 0;
			listViewDetailColumn7->Width = 80;
			listViewDetailColumn8->HeaderText = L"finalstate";
			listViewDetailColumn8->MinWidth = 0;
			listViewDetailColumn8->Width = 80;
			this->listViewToPerform->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(8) {
				listViewDetailColumn1,
					listViewDetailColumn2, listViewDetailColumn3, listViewDetailColumn4, listViewDetailColumn5, listViewDetailColumn6, listViewDetailColumn7,
					listViewDetailColumn8
			});
			this->listViewToPerform->Dock = System::Windows::Forms::DockStyle::Fill;
			this->listViewToPerform->ItemSpacing = -1;
			this->listViewToPerform->Location = System::Drawing::Point(0, 0);
			this->listViewToPerform->Name = L"listViewToPerform";
			this->listViewToPerform->Size = System::Drawing::Size(907, 212);
			this->listViewToPerform->TabIndex = 0;
			this->listViewToPerform->ThemeName = L"ControlDefault";
			this->listViewToPerform->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 200;
			this->timer1->Tick += gcnew System::EventHandler(this, &LooterStatistics::timer1_Tick);
			// 
			// radSplitContainer1
			// 
			this->radSplitContainer1->Controls->Add(this->splitPanel1);
			this->radSplitContainer1->Controls->Add(this->splitPanel2);
			this->radSplitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radSplitContainer1->Location = System::Drawing::Point(0, 0);
			this->radSplitContainer1->Name = L"radSplitContainer1";
			this->radSplitContainer1->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// 
			// 
			this->radSplitContainer1->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->radSplitContainer1->Size = System::Drawing::Size(907, 402);
			this->radSplitContainer1->TabIndex = 2;
			this->radSplitContainer1->TabStop = false;
			this->radSplitContainer1->Text = L"radSplitContainer1";
			// 
			// splitPanel1
			// 
			this->splitPanel1->Controls->Add(this->radPanel2);
			this->splitPanel1->Location = System::Drawing::Point(0, 0);
			this->splitPanel1->Name = L"splitPanel1";
			// 
			// 
			// 
			this->splitPanel1->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel1->Size = System::Drawing::Size(907, 212);
			this->splitPanel1->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.03266332F);
			this->splitPanel1->SizeInfo->SplitterCorrection = System::Drawing::Size(0, -61);
			this->splitPanel1->TabIndex = 0;
			this->splitPanel1->TabStop = false;
			this->splitPanel1->Text = L"splitPanel1";
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radSplitContainer3);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel2->Location = System::Drawing::Point(0, 0);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(907, 212);
			this->radPanel2->TabIndex = 1;
			this->radPanel2->Text = L"radPanel2";
			// 
			// radSplitContainer3
			// 
			this->radSplitContainer3->Controls->Add(this->radSplitContainer5);
			this->radSplitContainer3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radSplitContainer3->Location = System::Drawing::Point(0, 0);
			this->radSplitContainer3->Name = L"radSplitContainer3";
			this->radSplitContainer3->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// 
			// 
			this->radSplitContainer3->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->radSplitContainer3->Size = System::Drawing::Size(907, 212);
			this->radSplitContainer3->TabIndex = 1;
			this->radSplitContainer3->TabStop = false;
			this->radSplitContainer3->Text = L"radSplitContainer3";
			// 
			// radSplitContainer5
			// 
			this->radSplitContainer5->Controls->Add(this->splitPanel5);
			this->radSplitContainer5->Location = System::Drawing::Point(0, 0);
			this->radSplitContainer5->Name = L"radSplitContainer5";
			this->radSplitContainer5->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// 
			// 
			this->radSplitContainer5->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->radSplitContainer5->Size = System::Drawing::Size(907, 212);
			this->radSplitContainer5->TabIndex = 1;
			this->radSplitContainer5->TabStop = false;
			this->radSplitContainer5->Text = L"radSplitContainer5";
			// 
			// splitPanel5
			// 
			this->splitPanel5->Controls->Add(this->listViewToPerform);
			this->splitPanel5->Location = System::Drawing::Point(0, 0);
			this->splitPanel5->Name = L"splitPanel5";
			// 
			// 
			// 
			this->splitPanel5->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel5->Size = System::Drawing::Size(907, 212);
			this->splitPanel5->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.3687259F);
			this->splitPanel5->SizeInfo->SplitterCorrection = System::Drawing::Size(0, 297);
			this->splitPanel5->TabIndex = 1;
			this->splitPanel5->TabStop = false;
			this->splitPanel5->Text = L"splitPanel5";
			// 
			// splitPanel2
			// 
			this->splitPanel2->Controls->Add(this->radPanel1);
			this->splitPanel2->Location = System::Drawing::Point(0, 216);
			this->splitPanel2->Name = L"splitPanel2";
			// 
			// 
			// 
			this->splitPanel2->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel2->Size = System::Drawing::Size(907, 186);
			this->splitPanel2->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.03266332F);
			this->splitPanel2->SizeInfo->SplitterCorrection = System::Drawing::Size(0, 61);
			this->splitPanel2->TabIndex = 1;
			this->splitPanel2->TabStop = false;
			this->splitPanel2->Text = L"splitPanel2";
			// 
			// radPanel1
			// 
			this->radPanel1->Controls->Add(this->radSplitContainer2);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel1->Location = System::Drawing::Point(0, 0);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(907, 186);
			this->radPanel1->TabIndex = 0;
			this->radPanel1->Text = L"radPanel1";
			// 
			// radSplitContainer2
			// 
			this->radSplitContainer2->Controls->Add(this->radSplitContainer4);
			this->radSplitContainer2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radSplitContainer2->Location = System::Drawing::Point(0, 0);
			this->radSplitContainer2->Name = L"radSplitContainer2";
			this->radSplitContainer2->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// 
			// 
			this->radSplitContainer2->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->radSplitContainer2->Size = System::Drawing::Size(907, 186);
			this->radSplitContainer2->TabIndex = 0;
			this->radSplitContainer2->TabStop = false;
			this->radSplitContainer2->Text = L"radSplitContainer2";
			// 
			// radSplitContainer4
			// 
			this->radSplitContainer4->Controls->Add(this->splitPanel7);
			this->radSplitContainer4->Location = System::Drawing::Point(0, 0);
			this->radSplitContainer4->Name = L"radSplitContainer4";
			this->radSplitContainer4->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// 
			// 
			this->radSplitContainer4->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->radSplitContainer4->Size = System::Drawing::Size(907, 186);
			this->radSplitContainer4->TabIndex = 1;
			this->radSplitContainer4->TabStop = false;
			this->radSplitContainer4->Text = L"radSplitContainer4";
			// 
			// splitPanel7
			// 
			this->splitPanel7->Controls->Add(this->radListViewPerformed);
			this->splitPanel7->Location = System::Drawing::Point(0, 0);
			this->splitPanel7->Name = L"splitPanel7";
			// 
			// 
			// 
			this->splitPanel7->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel7->Size = System::Drawing::Size(907, 186);
			this->splitPanel7->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.3880309F);
			this->splitPanel7->SizeInfo->SplitterCorrection = System::Drawing::Size(0, 310);
			this->splitPanel7->TabIndex = 1;
			this->splitPanel7->TabStop = false;
			this->splitPanel7->Text = L"splitPanel7";
			// 
			// radListViewPerformed
			// 
			this->radListViewPerformed->AllowEdit = false;
			this->radListViewPerformed->AllowRemove = false;
			listViewDetailColumn9->HeaderText = L"name";
			listViewDetailColumn9->MinWidth = 0;
			listViewDetailColumn10->HeaderText = L"diedtime";
			listViewDetailColumn10->MinWidth = 0;
			listViewDetailColumn10->Width = 50;
			listViewDetailColumn11->HeaderText = L"message";
			listViewDetailColumn11->MinWidth = 0;
			listViewDetailColumn12->HeaderText = L"stack";
			listViewDetailColumn12->MinWidth = 0;
			listViewDetailColumn12->Width = 150;
			listViewDetailColumn13->HeaderText = L"timeout";
			listViewDetailColumn13->MinWidth = 0;
			listViewDetailColumn13->Width = 50;
			listViewDetailColumn14->HeaderText = L"fail";
			listViewDetailColumn14->MinWidth = 0;
			listViewDetailColumn14->Width = 50;
			listViewDetailColumn15->HeaderText = L"position";
			listViewDetailColumn15->MinWidth = 0;
			listViewDetailColumn15->Width = 80;
			listViewDetailColumn16->HeaderText = L"finalstate";
			listViewDetailColumn16->MinWidth = 0;
			listViewDetailColumn16->Width = 80;
			this->radListViewPerformed->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(8) {
				listViewDetailColumn9,
					listViewDetailColumn10, listViewDetailColumn11, listViewDetailColumn12, listViewDetailColumn13, listViewDetailColumn14, listViewDetailColumn15,
					listViewDetailColumn16
			});
			this->radListViewPerformed->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radListViewPerformed->ItemSpacing = -1;
			this->radListViewPerformed->Location = System::Drawing::Point(0, 0);
			this->radListViewPerformed->Name = L"radListViewPerformed";
			this->radListViewPerformed->Size = System::Drawing::Size(907, 186);
			this->radListViewPerformed->TabIndex = 1;
			this->radListViewPerformed->ThemeName = L"ControlDefault";
			this->radListViewPerformed->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			// 
			// splitPanel3
			// 
			this->splitPanel3->Location = System::Drawing::Point(0, 0);
			this->splitPanel3->Name = L"splitPanel3";
			// 
			// 
			// 
			this->splitPanel3->RootElement->MinSize = System::Drawing::Size(25, 25);
			this->splitPanel3->Size = System::Drawing::Size(200, 98);
			this->splitPanel3->TabIndex = 0;
			this->splitPanel3->TabStop = false;
			this->splitPanel3->Text = L"splitPanel3";
			// 
			// LooterStatistics
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(907, 402);
			this->Controls->Add(this->radSplitContainer1);
			this->Name = L"LooterStatistics";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"Looter Statistics";
			this->ThemeName = L"ControlDefault";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &LooterStatistics::LooterStatistics_FormClosing);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->listViewToPerform))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer1))->EndInit();
			this->radSplitContainer1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel1))->EndInit();
			this->splitPanel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer3))->EndInit();
			this->radSplitContainer3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer5))->EndInit();
			this->radSplitContainer5->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel5))->EndInit();
			this->splitPanel5->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel2))->EndInit();
			this->splitPanel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer2))->EndInit();
			this->radSplitContainer2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer4))->EndInit();
			this->radSplitContainer4->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel7))->EndInit();
			this->splitPanel7->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListViewPerformed))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	 System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e);
	 System::Void LooterStatistics_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 //e->Cancel = false;
				 //ooterStatistics::get()->Hide();
	}
};
}
