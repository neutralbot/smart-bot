#pragma once
#include "FastUIPanelController.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class GeneralSettings : public Telerik::WinControls::UI::RadForm{

	public:	GeneralSettings(void);
	protected: ~GeneralSettings();
	private: Telerik::WinControls::UI::RadCheckBox^  practice_mode;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_auto_save;

	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	protected:

	protected: static GeneralSettings^ unique;
	public:	static void CloseUnique(){
		if (unique)unique->Close();
	}
	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
		
		if (unique == nullptr){
			unique = gcnew GeneralSettings();
			unique->Show();
		}
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
	}

	private: Telerik::WinControls::UI::RadCheckBox^  radCheckTopMost;
	private: Telerik::WinControls::UI::RadCheckBox^  radCheckAutoResize;
	private: Telerik::WinControls::UI::RadCheckBox^  radCheckButtonPanel;
	public:
	protected:
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	protected:
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->radCheckTopMost = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->radCheckAutoResize = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->radCheckButtonPanel = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->spin_auto_save = (gcnew Telerik::WinControls::UI::RadSpinEditor());
			this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->practice_mode = (gcnew Telerik::WinControls::UI::RadCheckBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckTopMost))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckAutoResize))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckButtonPanel))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
			this->radGroupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_auto_save))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->practice_mode))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// radCheckTopMost
			// 
			this->radCheckTopMost->Location = System::Drawing::Point(5, 21);
			this->radCheckTopMost->Name = L"radCheckTopMost";
			this->radCheckTopMost->Size = System::Drawing::Size(68, 18);
			this->radCheckTopMost->TabIndex = 2;
			this->radCheckTopMost->Text = L"Top Most";
			this->radCheckTopMost->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &GeneralSettings::radCheckBox1_ToggleStateChanged);
			// 
			// radCheckAutoResize
			// 
			this->radCheckAutoResize->Location = System::Drawing::Point(5, 45);
			this->radCheckAutoResize->Name = L"radCheckAutoResize";
			this->radCheckAutoResize->Size = System::Drawing::Size(79, 18);
			this->radCheckAutoResize->TabIndex = 3;
			this->radCheckAutoResize->Text = L"Auto Resize";
			this->radCheckAutoResize->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &GeneralSettings::radCheckBox2_ToggleStateChanged);
			// 
			// radCheckButtonPanel
			// 
			this->radCheckButtonPanel->Location = System::Drawing::Point(5, 69);
			this->radCheckButtonPanel->Name = L"radCheckButtonPanel";
			this->radCheckButtonPanel->Size = System::Drawing::Size(173, 18);
			this->radCheckButtonPanel->TabIndex = 3;
			this->radCheckButtonPanel->Text = L"Dinamic Shotcurt Button Panel";
			this->radCheckButtonPanel->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &GeneralSettings::radCheckBox3_ToggleStateChanged);
			// 
			// radGroupBox1
			// 
			this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox1->Controls->Add(this->spin_auto_save);
			this->radGroupBox1->Controls->Add(this->radLabel1);
			this->radGroupBox1->Controls->Add(this->practice_mode);
			this->radGroupBox1->Controls->Add(this->radCheckTopMost);
			this->radGroupBox1->Controls->Add(this->radCheckButtonPanel);
			this->radGroupBox1->Controls->Add(this->radCheckAutoResize);
			this->radGroupBox1->Dock = System::Windows::Forms::DockStyle::Top;
			this->radGroupBox1->HeaderText = L"Bot Interface Mode:";
			this->radGroupBox1->Location = System::Drawing::Point(0, 0);
			this->radGroupBox1->Name = L"radGroupBox1";
			this->radGroupBox1->Size = System::Drawing::Size(313, 119);
			this->radGroupBox1->TabIndex = 4;
			this->radGroupBox1->Text = L"Bot Interface Mode:";
			// 
			// spin_auto_save
			// 
			this->spin_auto_save->Location = System::Drawing::Point(248, 20);
			this->spin_auto_save->Name = L"spin_auto_save";
			this->spin_auto_save->Size = System::Drawing::Size(53, 20);
			this->spin_auto_save->TabIndex = 6;
			this->spin_auto_save->TabStop = false;
			this->spin_auto_save->ValueChanged += gcnew System::EventHandler(this, &GeneralSettings::spin_auto_save_ValueChanged);
			// 
			// radLabel1
			// 
			this->radLabel1->Location = System::Drawing::Point(161, 21);
			this->radLabel1->Name = L"radLabel1";
			this->radLabel1->Size = System::Drawing::Size(81, 18);
			this->radLabel1->TabIndex = 5;
			this->radLabel1->Text = L"Auto-Save Sec:";
			// 
			// practice_mode
			// 
			this->practice_mode->Location = System::Drawing::Point(5, 93);
			this->practice_mode->Name = L"practice_mode";
			this->practice_mode->Size = System::Drawing::Size(92, 18);
			this->practice_mode->TabIndex = 4;
			this->practice_mode->Text = L"Practice Mode";
			this->practice_mode->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &GeneralSettings::practice_mode_ToggleStateChanged);
			// 
			// GeneralSettings
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(313, 119);
			this->Controls->Add(this->radGroupBox1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"GeneralSettings";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"GeneralSettings";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckTopMost))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckAutoResize))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckButtonPanel))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
			this->radGroupBox1->ResumeLayout(false);
			this->radGroupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_auto_save))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->practice_mode))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
		bool disable_update = false;
		void update_idiom();

	private: System::Void radCheckBox1_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void radCheckBox2_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void radCheckBox3_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void loadSettings();
	private: System::Void practice_mode_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void spin_auto_save_ValueChanged(System::Object^  sender, System::EventArgs^  e);
};
}


