#pragma once
#include "FastUIPanelController.h"
#include "ControlManager2.h"

namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Text::RegularExpressions;

	public ref class FormEditor : public Telerik::WinControls::UI::RadForm{
	public:	FormEditor(void){
		temp_disable = true;
		InitializeComponent();
		disable_update = true;
		update_idiom();
		disable_update = false;
		creatContextMenu();
		disable_update = false;
		edit_mode = false;

		this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
		NeutralBot::FastUIPanelController::get()->install_controller(this);
	}

	protected: ~FormEditor(){
				   unique = nullptr;
				   FormEditor_Resize(nullptr, nullptr);
				   if (components)
					   delete components;
	}

	private: Telerik::WinControls::UI::RadContextMenuManager^  radContextMenuManager1;
	private: Telerik::WinControls::UI::RadContextMenu^  MenuControls;
	private: Telerik::WinControls::UI::RadPanel^  radPanel2;
	private: Telerik::WinControls::UI::RadMenu^  radMenu1;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
	private: Telerik::WinControls::UI::RadPanel^  radPanel1;
	private: Telerik::WinControls::UI::RadTreeView^  TreeViewControls;
	private: Telerik::WinControls::UI::RadPanel^  radPanel4;
	private: Telerik::WinControls::UI::RadPropertyGrid^  radPropertyGrid1;
	private: Telerik::WinControls::UI::RadPanel^  radPanel3;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem2;
	private: Telerik::WinControls::UI::RadMenuItem^  buttonSave;
	private: Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
	private: Telerik::WinControls::UI::RadMenuItem^  buttonLoad;


	private: Telerik::WinControls::UI::RadMenuItem^  bt_move;

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}


	private: System::ComponentModel::IContainer^  components;
	protected:

#pragma region Windows Form Designer generated code
		void InitializeComponent(void){
			this->components = (gcnew System::ComponentModel::Container());
			Telerik::WinControls::UI::RadTreeNode^  radTreeNode1 = (gcnew Telerik::WinControls::UI::RadTreeNode());
			this->MenuControls = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
			this->radContextMenuManager1 = (gcnew Telerik::WinControls::UI::RadContextMenuManager());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
			this->radMenuItem2 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->buttonSave = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
			this->buttonLoad = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_move = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->TreeViewControls = (gcnew Telerik::WinControls::UI::RadTreeView());
			this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPropertyGrid1 = (gcnew Telerik::WinControls::UI::RadPropertyGrid());
			this->radPanel3 = (gcnew Telerik::WinControls::UI::RadPanel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->TreeViewControls))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
			this->radPanel4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPropertyGrid1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// MenuControls
			// 
			this->MenuControls->DropDownOpening += gcnew System::ComponentModel::CancelEventHandler(this, &FormEditor::MenuControls_DropDownOpening);
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radMenu1);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel2->Location = System::Drawing::Point(0, 0);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(808, 21);
			this->radPanel2->TabIndex = 1;
			this->radPanel2->Text = L"radPanel2";
			// 
			// radMenu1
			// 
			this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
				this->radMenuItem2, this->radMenuItem1,
					this->bt_move
			});
			this->radMenu1->Location = System::Drawing::Point(0, 0);
			this->radMenu1->Name = L"radMenu1";
			this->radMenu1->Size = System::Drawing::Size(808, 20);
			this->radMenu1->TabIndex = 0;
			this->radMenu1->Text = L"radMenu1";
			// 
			// radMenuItem2
			// 
			this->radMenuItem2->AccessibleDescription = L"Menu";
			this->radMenuItem2->AccessibleName = L"Menu";
			this->radMenuItem2->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
				this->buttonSave, this->radMenuSeparatorItem1,
					this->buttonLoad
			});
			this->radMenuItem2->Name = L"radMenuItem2";
			this->radMenuItem2->Text = L"Menu";
			// 
			// buttonSave
			// 
			this->buttonSave->AccessibleDescription = L"Save";
			this->buttonSave->AccessibleName = L"Save";
			this->buttonSave->Name = L"buttonSave";
			this->buttonSave->Text = L"Save";
			this->buttonSave->Click += gcnew System::EventHandler(this, &FormEditor::buttonSave_Click);
			// 
			// radMenuSeparatorItem1
			// 
			this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
			// 
			// buttonLoad
			// 
			this->buttonLoad->AccessibleDescription = L"Load";
			this->buttonLoad->AccessibleName = L"Load";
			this->buttonLoad->Name = L"buttonLoad";
			this->buttonLoad->Text = L"Load";
			this->buttonLoad->Click += gcnew System::EventHandler(this, &FormEditor::buttonLoad_Click);
			// 
			// radMenuItem1
			// 
			this->radMenuItem1->AccessibleDescription = L"Edit Mode";
			this->radMenuItem1->AccessibleName = L"Edit Mode";
			this->radMenuItem1->Name = L"radMenuItem1";
			this->radMenuItem1->Text = L"Edit Mode";
			this->radMenuItem1->Click += gcnew System::EventHandler(this, &FormEditor::radMenuItem1_Click);
			// 
			// bt_move
			// 
			this->bt_move->AccessibleDescription = L"Move Mode";
			this->bt_move->AccessibleName = L"Move Mode";
			this->bt_move->Name = L"bt_move";
			this->bt_move->Text = L"Move Mode Off";
			this->bt_move->Click += gcnew System::EventHandler(this, &FormEditor::bt_move_Click);
			// 
			// radPanel1
			// 
			this->radPanel1->Controls->Add(this->TreeViewControls);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Left;
			this->radPanel1->Location = System::Drawing::Point(0, 21);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(200, 467);
			this->radPanel1->TabIndex = 0;
			this->radPanel1->Text = L"radPanel1";
			// 
			// TreeViewControls
			// 
			this->TreeViewControls->AllowAdd = true;
			this->TreeViewControls->AllowEdit = true;
			this->TreeViewControls->AllowRemove = true;
			this->TreeViewControls->BackColor = System::Drawing::SystemColors::Control;
			this->TreeViewControls->Cursor = System::Windows::Forms::Cursors::Default;
			this->TreeViewControls->Dock = System::Windows::Forms::DockStyle::Fill;
			this->TreeViewControls->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
			this->TreeViewControls->ForeColor = System::Drawing::Color::Black;
			this->TreeViewControls->Location = System::Drawing::Point(0, 0);
			this->TreeViewControls->Name = L"TreeViewControls";
			radTreeNode1->ContextMenu = this->MenuControls;
			radTreeNode1->Expanded = true;
			radTreeNode1->Name = L"Controls";
			radTreeNode1->Text = L"Controls";
			this->TreeViewControls->Nodes->AddRange(gcnew cli::array< Telerik::WinControls::UI::RadTreeNode^  >(1) { radTreeNode1 });
			this->TreeViewControls->RadContextMenu = this->MenuControls;
			this->TreeViewControls->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->TreeViewControls->Size = System::Drawing::Size(200, 467);
			this->TreeViewControls->SpacingBetweenNodes = -1;
			this->TreeViewControls->TabIndex = 0;
			this->TreeViewControls->Text = L"radTreeView1";
			this->TreeViewControls->Editing += gcnew Telerik::WinControls::UI::TreeNodeEditingEventHandler(this, &FormEditor::TreeViewControls_Editing);
			// 
			// radPanel4
			// 
			this->radPanel4->Controls->Add(this->radPropertyGrid1);
			this->radPanel4->Dock = System::Windows::Forms::DockStyle::Right;
			this->radPanel4->Location = System::Drawing::Point(608, 21);
			this->radPanel4->Name = L"radPanel4";
			this->radPanel4->Size = System::Drawing::Size(200, 467);
			this->radPanel4->TabIndex = 3;
			this->radPanel4->Text = L"radPanel4";
			// 
			// radPropertyGrid1
			// 
			this->radPropertyGrid1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPropertyGrid1->HelpVisible = false;
			this->radPropertyGrid1->Location = System::Drawing::Point(0, 0);
			this->radPropertyGrid1->Name = L"radPropertyGrid1";
			this->radPropertyGrid1->PropertySort = System::Windows::Forms::PropertySort::Categorized;
			this->radPropertyGrid1->Size = System::Drawing::Size(200, 467);
			this->radPropertyGrid1->SortOrder = System::Windows::Forms::SortOrder::Ascending;
			this->radPropertyGrid1->TabIndex = 0;
			this->radPropertyGrid1->Text = L"radPropertyGrid1";
			this->radPropertyGrid1->ToolbarVisible = true;
			this->radPropertyGrid1->Editing += gcnew Telerik::WinControls::UI::PropertyGridItemEditingEventHandler(this, &FormEditor::radPropertyGrid1_Editing);
			this->radPropertyGrid1->Edited += gcnew Telerik::WinControls::UI::PropertyGridItemEditedEventHandler(this, &FormEditor::radPropertyGrid1_Edited);
			this->radPropertyGrid1->PropertyValidating += gcnew Telerik::WinControls::UI::PropertyValidatingEventHandler(this, &FormEditor::radPropertyGrid1_PropertyValidating);
			// 
			// radPanel3
			// 
			this->radPanel3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel3->Location = System::Drawing::Point(200, 21);
			this->radPanel3->Name = L"radPanel3";
			this->radPanel3->Size = System::Drawing::Size(408, 467);
			this->radPanel3->TabIndex = 4;
			// 
			// FormEditor
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(808, 488);
			this->Controls->Add(this->radPanel3);
			this->Controls->Add(this->radPanel4);
			this->Controls->Add(this->radPanel1);
			this->Controls->Add(this->radPanel2);
			this->MinimumSize = System::Drawing::Size(500, 300);
			this->Name = L"FormEditor";
			this->RightToLeft = System::Windows::Forms::RightToLeft::No;
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Setup";
			this->Load += gcnew System::EventHandler(this, &FormEditor::FormEditor_Load);
			this->Resize += gcnew System::EventHandler(this, &FormEditor::FormEditor_Resize);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			this->radPanel2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->TreeViewControls))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
			this->radPanel4->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPropertyGrid1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion


		static FormEditor^ unique;
	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew FormEditor();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}public: static void CreatUnique(){
		if (unique == nullptr)
			unique = gcnew FormEditor();
	}
			static void CloseUnique(){
				if (unique)unique->Close();
			}
			public: static void ReloadForm(){
						unique->FormEditor_Load(nullptr, nullptr);
			}
			
			bool disable_update;
			bool edit_mode;
			String^ oldValue;
			int tabindex = 0;
			bool dragInProgress = false;
			bool move_mode = false;
			bool temp_disable;
			bool pressed = false;
			int MouseDownX = 0;
			int MouseDownY = 0;		
			bool disable_update2;
			String^ OldValue_Grid;

			Telerik::WinControls::UI::RadMenuItem^ ItemGroup;
			Telerik::WinControls::UI::RadMenuItem^ ItemCheckBox;
			Telerik::WinControls::UI::RadMenuItem^ ItemButton;
			Telerik::WinControls::UI::RadMenuItem^ ItemTextBox;
			Telerik::WinControls::UI::RadMenuItem^ ItemLabel;
			Telerik::WinControls::UI::RadMenuItem^ ItemCombo;
			Telerik::WinControls::UI::RadMenuItem^ ItemComboBoxItem;
			Telerik::WinControls::UI::RadMenuItem^ ItemComboBoxbp;
			Telerik::WinControls::UI::RadMenuItem^ ItemSpinEditor;
			Telerik::WinControls::UI::RadMenuItem^ ItemEdit;
			Telerik::WinControls::UI::RadMenuItem^ ItemLooterGroup;
			Telerik::WinControls::UI::RadMenuItem^ ItemRepoterGroup;
			Telerik::WinControls::UI::RadMenuItem^ ItemDepoterGroup;
			Telerik::WinControls::UI::RadMenuItem^ ItemSpellsAttackGroup;
			Telerik::WinControls::UI::RadMenuItem^ ItemSpellsHealthGroup;

			void creatContextMenu();

			void creatEvents();
			void update_idiom();


			bool changeChieldId(bool group, String^ groupKey, String^ controlKey, String^ newControlKey);
	private: System::Void TreeViewControls_ValueChanging(System::Object^  sender, Telerik::WinControls::UI::TreeNodeValueChangingEventArgs^  e);
	private: System::Void TreeViewControls_Editing(System::Object^  sender, Telerik::WinControls::UI::TreeNodeEditingEventArgs^  e);
	private: System::Void TreeViewControls_NodeRemoving(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewCancelEventArgs^  e);;
	private: System::Void TreeViewControls_NodeMouseDoubleClick(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);
	private: System::Void TreeViewControls_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);

			 Telerik::WinControls::UI::RadButton^ createButton(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size);
			 Telerik::WinControls::UI::RadGroupBox^ createGroup(String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size);
			 Telerik::WinControls::UI::RadTextBox^ createTextBox(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size);
			 Telerik::WinControls::UI::RadCheckBox^ createCheckBox(Control^ group, bool state, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size);
			 Telerik::WinControls::UI::RadSpinEditor^ createSpinEditor(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size, double max, double min, int value);
			 Telerik::WinControls::UI::RadDropDownList^ createComboBox(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size, array<String^>^ items);
			 Telerik::WinControls::UI::RadLabel^ createLabel(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size);

			 void setPropertyButton(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadButton^ control);
			 void setPropertyTextBox(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadTextBox^ control);
			 void setPropertyCheckBox(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadCheckBox^ control);
			 void setPropertySpinEditor(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadSpinEditor^ control);
			 void setPropertyComboBox(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadDropDownList^ control);
			 void setPropertyLabel(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadLabel^ control);
			 void setPropertyGroup(String^ controlKey, Telerik::WinControls::UI::RadGroupBox^ control);

			 void addGroupInForm();
			 void addLoooterGroupInForm();
			 void addRepoterGroupInForm();
			 void addDepoterGroupInForm();
			 void addSpellsAttackGroupInForm();
			 void addSpellsHealthGroupInForm();
			 void addButtonInForm();
			 void addTextBoxInForm();
			 void addCheckBoxInForm();
			 void addSpinEditorInForm();
			 void addComboBoxInForm();
			 void addComboBoxItemInForm();
			 void addComboBoxBpInForm();
			 void addLabelInForm();

			 void openLuaEditor(String^ groupKey, String^ controlKey);

			 void updateLooterGroup();
			 void updateRepoterGroup();
			 void updateDepoterGroup();
			 void updateSpellsAttackGroup();
			 void updateSpellsHealthGroup();

			 System::Void MenuGroup_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuEdit_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuButton_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuItemLooterGroup_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuItemRepoterGroup_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuItemDepoterGroup_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuItemSpellsHealthGroup_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuItemSpellsAttackGroup_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuCheckBox_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuTextBox_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuLabel_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuCombo_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuComboBoxItem_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuComboBoxBp_Click(Object^ sender, EventArgs^ e);
			 System::Void MenuSpinEditor_Click(Object^ sender, EventArgs^ e);

			 Control^ get_control(String^ controlName);
			 
	private: System::Void radMenuItem1_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void FormEditor_Load(System::Object^  sender, System::EventArgs^  e);

			 void filterProperty(Telerik::WinControls::UI::PropertyGridItemCollection^ Items);

			 std::shared_ptr<ControlInfoChield> getControlChieldRule(String^ groupKey, String^ controlKey);
			 std::shared_ptr<ControlInfo> getControlRule(String^ controlKey);

	private: System::Void EventClick(System::Object^  sender, System::EventArgs^  e);
	private: System::Void EventMouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
	private: System::Void EventMouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
	private: System::Void EventMouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
	private: System::Void EventSelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	private: System::Void EventValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void EventLocationChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void EventResize(System::Object^  sender, System::EventArgs^  e);
	private: System::Void EventCheckStateChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void Event_TextChanged(System::Object^  sender, System::EventArgs^  e);
			 System::Void Looter_TextChanged(System::Object^  sender, System::EventArgs^  e);
			 System::Void Repoter_ValueChanged(System::Object^  sender, System::EventArgs^  e);
			 System::Void Repoter_TextChanged(System::Object^  sender, System::EventArgs^  e);
			 System::Void Depoter_TextChanged(System::Object^  sender, System::EventArgs^  e);
			 System::Void Spells_StateChanged(System::Object^  sender, System::EventArgs^  e);

			 String^ lastContainerName;
	private: System::Void MenuControls_DropDownOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e);

			 void loadControlInForm();

			 bool save_script(std::string file_name);
			 bool load_script(std::string file_name);

	private: System::Void radPropertyGrid1_Edited(System::Object^  sender, Telerik::WinControls::UI::PropertyGridItemEditedEventArgs^  e);
	
private: System::Void buttonSave_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void buttonLoad_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void FormEditor_Resize(System::Object^  sender, System::EventArgs^  e);
			 
	

	private: System::Void bt_move_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void Event_Label_TextChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radPropertyGrid1_PropertyValidating(System::Object^  sender, Telerik::WinControls::UI::PropertyValidatingEventArgs^  e);

private: System::Void radPropertyGrid1_Editing(System::Object^  sender, Telerik::WinControls::UI::PropertyGridItemEditingEventArgs^  e);
};
}