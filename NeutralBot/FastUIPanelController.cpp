#include "FastUIPanelController.h"
#include "FastUIPanel.h"
#include <iostream>
#include "GeneralManager.h"

using namespace NeutralBot;


FormItemInfo::FormItemInfo(System::String^ _id, System::String^ _text, System::String^ _icon_name, System::Windows::Forms::Form^ _form){
	id = _id;
	text = _text;
	icon_name = _icon_name;
	form_ref = _form;
}

void FastUIPanelController::init(){
	form_panel = gcnew FastUIPanel();
}

FastUIPanelController::FastUIPanelController(){
	init();
}

FormItemInfo^ FastUIPanelController::get_form_info_by_form_ref(System::Windows::Forms::Form^ form){
	for each(FormItemInfo^ form_info in installed_forms){
		if (form_info->form_ref == form){
			return form_info;
		}
	}
	return nullptr;
}

bool FastUIPanelController::is_form_in_list(System::Windows::Forms::Form^ form){
	for each(auto form_info in installed_forms){
		if (form_info->form_ref == form)
			return true;
	}
	return false;
}

bool FastUIPanelController::is_form_id_in_list(System::String^ id){
	for each(FormItemInfo^ form_info in installed_forms){
		if (form_info->id == id)
			return true;
	} 
	return false;
}

void FastUIPanelController::remove_form_from_list(System::Windows::Forms::Form^ form, bool close){
	FormItemInfo^ item = get_form_info_by_form_ref(form);
	if (item){
		if (close)
			item->form_ref->Close();
		installed_forms.Remove(item);
		form_panel->remove_form(form->Name, false);
	}
}

FormItemInfo^ FastUIPanelController::get_form_info_by_form_id(String^ id){
	for each(FormItemInfo^ form_info in installed_forms){
		if (form_info->id == id)
			return form_info;
	}
	return nullptr;
}

FastUIPanelController^ FastUIPanelController::get(){
	if (!mFastUIPanelController)
		mFastUIPanelController = gcnew FastUIPanelController;

	return mFastUIPanelController;
}

FastUIPanel^ FastUIPanelController::get_fast_ui_form(){
	return form_panel;
}

void FastUIPanelController::on_form_windows_state_change(System::Windows::Forms::Form^ form){
	/*if (System::Windows::Forms::FormWindowState::Maximized == form->WindowState){

	}
	else */
	/*if (System::Windows::Forms::FormWindowState::Minimized == form->WindowState){
		on_form_minimize(form);
		}
		else if (System::Windows::Forms::FormWindowState::Normal == form->WindowState){
		on_form_maximize(form);
		}*/
}

bool FastUIPanelController::add_ui_button(System::String^ button_id, System::Windows::Forms::Form^ form, System::String^ text, System::String^ icon_name){
	if (!GeneralManager::get()->get_interface_shortcut())
		return false;

	FormItemInfo^ fmr = get_form_info_by_form_ref(form);

	if (!fmr)
		return false;

	if (fmr->is_in_panel)
		return false;

	form_panel->add_form_button(button_id, text, icon_name);

	fmr->is_in_panel = true;
	return true;
}

void FastUIPanelController::on_form_close(System::Windows::Forms::Form^ form){
	remove_form_from_list(form, false);
}

void FastUIPanelController::on_form_minimize(System::Windows::Forms::Form^ form){
	//form->Hide();
	add_ui_button(form->Name, form, form->Text, form->Name);
}

void FastUIPanelController::on_form_out_focus(System::Windows::Forms::Form^ form){
	/*
		form->Hide();
		add_ui_button(form->Name, form, form->Text, form->Name);
		*/
}

void FastUIPanelController::on_form_maximize(System::Windows::Forms::Form^ form){
	//remove_form_from_list(form, false);
}

void FastUIPanelController::show_form_by_id(String^ id){
	FormItemInfo^ form = get_form_info_by_form_id(id);
	if (form){
		form->form_ref->Show();
		form->form_ref->WindowState = System::Windows::Forms::FormWindowState::Normal;
		form->form_ref->BringToFront();
	}
}

void FastUIPanelController::install_controller(System::Windows::Forms::Form^ form){
	form->TopMost = true;

	form->Activated += gcnew System::EventHandler(this, &FastUIPanelController::event_Form_Activated);
	form->Deactivate += gcnew System::EventHandler(this, &FastUIPanelController::event_Form_Deactivate);
	form->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &FastUIPanelController::event_Form_FormClosed);
	form->Shown += gcnew System::EventHandler(this, &FastUIPanelController::event_Form_Shown);
	form->Resize += gcnew System::EventHandler(this, &FastUIPanelController::event_Form_Resize);
	
	installed_forms.AddLast(gcnew FormItemInfo(form->Name, nullptr, nullptr, form));
}

System::Void FastUIPanelController::event_Form_Resize(System::Object^  sender, System::EventArgs^  e) {
}

bool FastUIPanelController::is_istalled_hook_form(System::Windows::Forms::Form^ form){
	for each(FormItemInfo^ f_info in installed_forms)
	if (f_info->form_ref == form)
		return true;
	return false;
}

void FastUIPanelController::unistall_hook_form(System::Windows::Forms::Form^ form){
	FormItemInfo^ form_it = nullptr;
	for each(FormItemInfo^ f_info in installed_forms)
	if (f_info->form_ref == form){
		form_it = f_info;
		break;
	}

	if (form_it == nullptr)
		return;
	remove_form_from_list(form_it->form_ref, false);
}

System::Void FastUIPanelController::event_Form_Shown(System::Object^  sender, System::EventArgs^  e) {
	if (!is_istalled_hook_form((System::Windows::Forms::Form^)sender))
		return;

	FastUIPanelController::get()->on_form_windows_state_change((System::Windows::Forms::Form^)sender);
}

System::Void FastUIPanelController::event_Form_Deactivate(System::Object^  sender, System::EventArgs^  e) {
	if (!is_istalled_hook_form((System::Windows::Forms::Form^)sender))
		return;

	FastUIPanelController::get()->on_form_minimize((System::Windows::Forms::Form^)sender);
	SizeDecreaseForm((System::Windows::Forms::Form^)sender);

}

System::Void FastUIPanelController::event_Form_Activated(System::Object^  sender, System::EventArgs^  e) {
	if (!is_istalled_hook_form((System::Windows::Forms::Form^)sender))
		return;
	SizeIncreaseForm((System::Windows::Forms::Form^)sender);
	FastUIPanelController::get()->on_form_maximize((System::Windows::Forms::Form^)sender);
}

System::Void FastUIPanelController::event_Form_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
	if (!is_istalled_hook_form((System::Windows::Forms::Form^)sender))
		return;
	unistall_hook_form((System::Windows::Forms::Form^)sender);
	FastUIPanelController::get()->on_form_close((System::Windows::Forms::Form^)sender);
}

void FastUIPanelController::SizeDecreaseForm(System::Windows::Forms::Form^ form){
	if (!GeneralManager::get()->get_interface_auto_resize())
		return;
	
	if (form->WindowState == System::Windows::Forms::FormWindowState::Minimized)
		return;
	
	else if (!form->Visible)
		return;
	
	FormItemInfo^ form_info = get_form_info_by_form_ref(form);
	if (!form_info)
		return;
	
	if (form_info->is_resized)
		return;

	form_info->size_before_resize = form->Size;
	auto current_sz = form->Size;
	current_sz.Height = 30;
	form->Size = current_sz;
	form_info->is_resized = true;
	
}

void FastUIPanelController::SizeIncreaseForm(System::Windows::Forms::Form^ form){
	FormItemInfo^ form_info = get_form_info_by_form_ref(form);
	if (form_info){
		if (!form_info->is_resized)
			return;
		form->Size = form_info->size_before_resize;
		form_info->is_resized = false;
		if (form->Name == "Waypoint"){
			form->Visible = false;
			form->Visible = true;
		}

	}
}

void FastUIPanelController::on_resize_option_change(bool state){
	for each(FormItemInfo^ form in installed_forms){
		if (!state)
			SizeIncreaseForm(form->form_ref);
		else
			SizeDecreaseForm(form->form_ref);
	}
}

void FastUIPanelController::on_top_most_option_change(bool state){
	for each(FormItemInfo^ form in installed_forms){
		if (!state){
			if (!form->form_ref->TopMost)
				continue;

			form->form_ref->TopMost = false;
			top_most = false;
		}
		else{
			if (form->form_ref->TopMost)
				continue;

			form->form_ref->TopMost = true;
			top_most = true;

		}
	}
}

void FastUIPanelController::on_panel_shortcut_option_change(bool state){
	form_panel->Hide();
	for each(FormItemInfo^ form in installed_forms){
		if (!state)
			form_panel->remove_form(form->form_ref->Name, true);
		else{
			form_panel->add_form_button(form->form_ref->Name, form->form_ref->Text, form->form_ref->Name);
		}
	}
}
