#pragma once 
#include "LooterManager.h"
#include "Core\LooterCore.h"
#include "Core\ItemsManager.h"
#include "Core\Actions.h"
#include "ControlManager2.h"

//TODO, quando for setado o item name setar o id para nao ficar buscando toda hora o id no itemsmanager
//TODO, quando for setado o nome da bp setar o o id
//TODO modificar as funcoes de contais, quando for corrigido os erros acima.

LooterManager::LooterManager() {
	request_new_bp_id();
	for (int i = 0; i < 10; i++)
		group_status.push_back(true);
	
	set_eat_from_corpses_delays(std::make_pair(20,120));
}

std::string LooterManager::request_new_bp_id(){
	int id = 0;
	std::string current_id = "Bp" + std::to_string(id);
	while (containers.find(current_id) != containers.end()){
		id++;
		current_id = "Bp" + std::to_string(id);
	}
	containers[current_id] = std::shared_ptr<LooterContainer>(new LooterContainer);
	containers[current_id]->set_container_name("none");
	return current_id;
}

void LooterManager::delete_bp_id(std::string id_){
	auto i = containers.find(id_);
	if (i != containers.end())
		containers.erase(i);
}

bool LooterManager::change_bp_id(std::string newId, std::string oldId){
	if (newId == "" || oldId == "")
		return false;

	auto it = containers.find(oldId);
	if (it == containers.end())
		return false;

	containers[newId] = it->second;
	containers.erase(it);
	return true;
}

bool LooterManager::contains_container_id(uint32_t item_id){
	for (auto container : containers)
	if (container.second->get_container_id() == item_id)
		return true;

	return false;
}

LooterContainerPtr LooterManager::get_looter_container_by_id(std::string id){
	auto it = containers.find(id);
	if (it == containers.end())
		return 0;
	return it->second;
}

LooterContainerPtr LooterManager::get_container_owns_by_item_id(uint32_t item_id){
	for (auto container : containers)
		if (container.second->contains_item_id(item_id))
			return container.second;
	return 0;
}

LooterContainerPtr LooterManager::get_container_owns_by_item_name(std::string& item_name){
	for (auto container : containers)
	if (container.second->contains_item_name(item_name))
		return container.second;
	return 0;
}

LooterItemNonSharedPtr LooterManager::get_looter_item_by_item_id(uint32_t item_id) {
	std::shared_ptr<LooterItem> item_info = nullptr;

	auto container_owns = get_container_owns_by_item_id(item_id);
	for (auto item : container_owns->items) {
		for (uint32_t index = 0; index < this->group_status.size(); index++) {
			if (!this->group_status[index])
				continue;

			if (item.second->get_item_id() != item_id)
				continue;

			return item.second->get_at_group(index);
		}
	}

	return 0;
}

std::pair<LooterItemPtr, LooterItemNonSharedPtr> LooterManager::get_item_by_name_and_count(std::string& name, uint32_t count){
	for (auto container : containers){
		std::pair<LooterItemPtr, LooterItemNonSharedPtr> item = container.second->get_item_by_name_and_count(name, count);
		if (item.first)
			return item;
	}
	return std::pair<LooterItemPtr, LooterItemNonSharedPtr>();
}

std::vector<uint32_t> LooterManager::get_our_containers_id(){
	std::vector<uint32_t> looter_containers_ids;
	std::for_each(containers.begin(), containers.end(),
		[&](std::map<std::string /*id*/, std::shared_ptr<LooterContainer>>::value_type& container_in_looter)->void{
		looter_containers_ids.push_back(container_in_looter.second->get_container_id());
	});
	return looter_containers_ids;
}

Json::Value LooterManager::parse_class_to_json(){
	Json::Value looterValue;
	looterValue["filter_type"] = (int)current_filter_type;
	looterValue["time_to_clean"] = (int)time_to_clean;
	looterValue["sequence_type"] = (int)current_sequence_type;
	looterValue["max_distance"] = (int)max_distance;


	Json::Value& eat_from_corpses_values = looterValue["eat_from_corpses"];
	eat_from_corpses_values["state"] = eat_from_corpses;
	Json::Value& eat_delay_range = eat_from_corpses_values["delay_range"];
	eat_delay_range["min"] = eat_from_corpses_delay.first;
	eat_delay_range["max"] = eat_from_corpses_delay.second;


	//Json::Value& eat_food_values = looterValue["eat_food"];
	//if (!eat_food_values.null && !eat_food_values.empty()){
	//
	//}

	looterValue["open_next_container"] = open_next_container;
	looterValue["auto_reopen_container"] = auto_reopen_container;

	Json::Value& json_containers = looterValue["json_containers"];

	for (auto it : containers)
		json_containers[it.first] = it.second->parse_class_to_json();

	return looterValue;
}

void LooterManager::parse_json_to_class(Json::Value& looterValue){
	clear();

	if (!looterValue["filter_type"].empty() || !looterValue["filter_type"].isNull())
		current_filter_type = (loot_filter_t)looterValue["filter_type"].asInt();

	if (!looterValue["sequence_type"].empty() || !looterValue["sequence_type"].isNull())
		current_sequence_type = (loot_sequence_t)looterValue["sequence_type"].asInt();

	if (!looterValue["time_to_clean"].empty() || !looterValue["time_to_clean"].isNull())
		time_to_clean = looterValue["time_to_clean"].asInt();

	if (!looterValue["max_distance"].empty() || !looterValue["max_distance"].isNull())
		max_distance = looterValue["max_distance"].asInt();

	Json::Value& eat_from_corpses_values = looterValue["eat_from_corpses"];
	if (!eat_from_corpses_values.null && !eat_from_corpses_values.empty()){
		eat_from_corpses_delay.first = eat_from_corpses_values["delay_range"]["min"].asInt();
		eat_from_corpses_delay.second = eat_from_corpses_values["delay_range"]["max"].asInt();
		eat_from_corpses = eat_from_corpses_values["state"].asBool();
	}

	if (!looterValue["open_next_container"].null && !looterValue["open_next_container"].empty()){
		open_next_container = looterValue["open_next_container"].asBool();
	}

	if (!looterValue["auto_reopen_container"].null && !looterValue["auto_reopen_container"].empty()){
		auto_reopen_container = looterValue["auto_reopen_container"].asBool();
	}

	if (!looterValue["json_containers"].empty() || !looterValue["json_containers"].isNull()){
		Json::Value& json_containers = looterValue["json_containers"];
		Json::Value::Members members = json_containers.getMemberNames();

		for (std::string member : members){
			LooterContainerPtr container(new LooterContainer);
			container->parse_json_to_class(json_containers[member]);
			containers[member] = container;
		}
	}
}

std::shared_ptr<LooterItem> LooterContainer::GetLooterRuleById(std::string id_){
	auto it = items.find(id_);
	if (it == items.end())
		return 0;
	return it->second;
}

std::string LooterContainer::request_new_idItens(){
	int id = 0;
	std::string current_id = "Item" + std::to_string(id);
	while (items.find(current_id) != items.end()){
		id++;
		current_id = "Item" + std::to_string(id);
	}

	items[current_id] = std::shared_ptr<LooterItem>(new LooterItem);
	return current_id;
}

void LooterContainer::delete_idItem(std::string id_){
	auto i = items.find(id_);
	if (i != items.end())
		items.erase(i);
}

std::string LooterContainer::additem(std::string name, loot_item_priority_t important, loot_item_condition_t condition, int value, int minqty, int group){
	std::string id = request_new_idItens();
	items[id]->set_name(name);
	LooterItemNonSharedPtr item_info = items[id]->get_at_group(group);

	item_info->set_condition(condition);

	item_info->set_important(important);
	item_info->set_minimun_count(minqty);
	item_info->set_value(value);

	return id;
}

bool LooterContainer::contains_item_id(uint32_t item_id){
	for (auto item : items)
	if (item.second->get_item_id() == item_id)
		return true;
	return false;
}

bool LooterContainer::contains_item_name(std::string& item_name){
	if (item_name.size() && *item_name.rbegin() == 's'){
		std::string not_plural_name = item_name.substr(0, item_name.length() - 1);
		for (auto item : items)
		if (item.second->match_name(item_name) || item.second->match_name(not_plural_name))
			return true;
	}
	else{
		for (auto item : items)
		if (item.second->match_name(item_name))
			return true;
	}
	return false;
}

uint32_t LooterContainer::get_container_id(){
	return container_id;
}

void LooterItem::set_name(std::string name){
	item_id = ItemsManager::get()->getitem_idFromName(name);
	this->name = name;
}

std::string LooterItem::get_name(){
	return name;
}

void LooterContainer::set_container_name(std::string name){
	container_id = ItemsManager::get()->getitem_idFromName(name);
	container_name = name;
}

std::string LooterContainer::get_container_name(){
	return container_name;
}

loot_type_t LooterManager::get_current_loot_type(){

	return current_loot_type;
}

void LooterManager::set_current_loot_type(loot_type_t in){
	if (current_loot_type == in)
		return;

	current_loot_type = in;
}

void LooterManager::set_current_filter_type(loot_filter_t type){
	if (current_filter_type == type)
		return;
	current_filter_type = type;
	LooterCore::set_current_filter_mode(type);
}

loot_filter_t LooterManager::get_current_filter_type(){
	return current_filter_type;
}

void LooterManager::set_current_sequence_type(loot_sequence_t type){
	if (current_sequence_type == type)
		return;
	current_sequence_type = type;
	LooterCore::get()->set_current_sequence_mode(type);
}

LooterItemNonShared::LooterItemNonShared(){
	important = loot_item_priority_t::loot_item_priority_max;
	condition = loot_item_condition_t::loot_item_condition_none;
	value = 0;
	minimun_count = 0;
}
uint32_t LooterItemNonShared::get_value(){
	return value;
}

LooterItem::LooterItem(){
	item_id = 0;
}

LooterContainer* LooterContainer::get(){
	static LooterContainer* m = nullptr;
	if (!m)
		m = new LooterContainer();
	return m;
}


void LooterItemNonShared::set_value(uint32_t value){
	this->value = value;
}

void LooterItemNonShared::change_content(uint32_t index, uint32_t action, uint32_t condition, uint32_t value){
	if (index > vector_condition.size() - 1)
		return;

	vector_condition[index]->action = (loot_item_action_t)action;
	vector_condition[index]->condition = (loot_item_new_condition_t)condition;
	vector_condition[index]->value = value;
}

void LooterItemNonShared::add_vector_condition(loot_item_action_t action, loot_item_new_condition_t condition, uint32_t value){

	std::shared_ptr<LooterConditions> new_item = std::shared_ptr<LooterConditions>(new LooterConditions);
	new_item->condition = condition;
	new_item->action = action;
	new_item->value = value;

	vector_condition.push_back(new_item);
}

void LooterItemNonShared::remove_vector_condition(uint32_t index){
	lock_guard mtx_(mtx_access);

	if (vector_condition.size() - 1 < index || index < 0)
		return;

	vector_condition.erase(vector_condition.begin() + index);
}

std::vector<std::shared_ptr<LooterConditions>> LooterItemNonShared::get_vector_condition(){
	return vector_condition;
}


loot_sequence_t LooterManager::get_current_sequence_type(){
	return current_sequence_type;
}

void LooterManager::set_min_cap(uint32_t state){
	min_cap = state;
}

uint32_t LooterManager::get_min_cap(){
	return min_cap;
}

void LooterManager::set_force_move(bool state){
	force_move = state;
}

bool LooterManager::get_force_movep(){
	return force_move;
}

void LooterManager::set_min_cap_to_loot(bool state){
	min_cap_to_loot = state;
}

bool LooterManager::get_min_cap_to_loot(){
	return min_cap_to_loot;
}

std::pair<LooterItemPtr, LooterItemNonSharedPtr> LooterContainer::get_item_by_name_and_count(std::string& name, uint32_t count){
	for (auto item : items){
		std::string current_name = item.second->get_name();
		int len_dif = name.length() - current_name.length();
		if (len_dif > 1)
			continue;
		//TODO improve comparisson betwwen diferent strings but almost the same
		if (len_dif > 0){
			std::string name_not_plural = name;
			if (*name_not_plural.rbegin() == 's' || *name_not_plural.rbegin() == 'S')
				name_not_plural[(name_not_plural.length() - 1)] = 0;

			if (string_util::string_compare(current_name, name_not_plural, true)){
				LooterItemNonSharedPtr nom_shared = item.second->GetItemNonSharedByCount(count);

				if (!nom_shared)
					continue;

				if (nom_shared->get_condition() == loot_item_condition_t::dont_loot_if_have_more_than)
				if (ContainerManager::get()->get_item_count(item.second->get_item_id()) > nom_shared->get_value())
					continue;

				return std::pair<LooterItemPtr, LooterItemNonSharedPtr>(item.second, nom_shared);
			}
		}
		else if (string_util::string_compare(current_name, name, true)){
			LooterItemNonSharedPtr nom_shared = item.second->GetItemNonSharedByCount(count);
			if (nom_shared)
				return std::pair<LooterItemPtr, LooterItemNonSharedPtr>(item.second, nom_shared);
		}
	}
	return std::pair<LooterItemPtr, LooterItemNonSharedPtr>();
}

Json::Value LooterContainer::parse_class_to_json(){
	Json::Value retval;
	retval["container_id"] = container_id;
	retval["container_name"] = get_container_name();
	retval["name"] = get_container_name();
	Json::Value& container_items = retval["container_items"];

	for (auto item : items)
		container_items[item.first] = item.second->parse_class_to_json();

	return retval;
}

void LooterContainer::parse_json_to_class(Json::Value& retval){
	if (!retval["container_name"].empty() || !retval["container_name"].isNull())
		set_container_name(retval["container_name"].asString());

	if (!retval["name"].empty() || !retval["name"].isNull())
		set_container_name(retval["name"].asString());

	if (!retval["container_items"].empty() || !retval["container_items"].isNull()){
		Json::Value& container_items = retval["container_items"];
		Json::Value::Members members = container_items.getMemberNames();

		for (std::string member : members){
			LooterItemPtr item(new LooterItem);
			item->parse_json_to_class(container_items[member]);
			items[member] = item;
		}
	}
}

Json::Value LooterItem::parse_class_to_json(){
	Json::Value loot_item;
	loot_item["name"] = name;
	loot_item["item_id"] = item_id;

	Json::Value& attr_members = loot_item["group_attributes"];
	for (auto pair_group : non_shared_info)
		attr_members[std::to_string(pair_group.first)] = pair_group.second->parse_class_to_json();
	return loot_item;
}

void LooterItem::parse_json_to_class(Json::Value& loot_item){
	if (!loot_item["name"].empty() || !loot_item["name"].isNull())
		name = loot_item["name"].asString();

	if (name != "new item"){
		if (!loot_item["item_id"].empty() || !loot_item["item_id"].isNull()){
			try{
				uint32_t item_id = loot_item["item_id"].asInt();
				set_item_id(item_id);
			}
			catch (...){
				MessageBox(0, "Error find item id", "", MB_OK);
				set_item_id(0);
			}
		}
	}


	if (!loot_item["group_attributes"].empty() || !loot_item["group_attributes"].isNull()){
		Json::Value& attr_members = loot_item["group_attributes"];
		Json::Value::Members members = attr_members.getMemberNames();

		for (std::string member : members){
			LooterItemNonSharedPtr _looter_item_non_shared(new LooterItemNonShared);
			_looter_item_non_shared->parse_json_to_class(attr_members[member]);
			non_shared_info[atoi(&member[0])] = _looter_item_non_shared;
		}
	}
}

Json::Value LooterItemNonShared::parse_class_to_json(){
	Json::Value attr;
	Json::Value vector_conditions_json;

	for (auto repot : vector_condition){
		Json::Value monsterListItem;

		monsterListItem["action"] = repot->action;
		monsterListItem["condition"] = repot->condition;
		monsterListItem["value"] = repot->value;

		vector_conditions_json.append(monsterListItem);
	}

	attr["vector_conditions_json"] = vector_conditions_json;
	attr["important"] = (int)important;
	attr["condition"] = (int)condition;
	attr["value"] = (int)value;
	attr["minimun_count"] = (int)minimun_count;

	return attr;
}

void LooterItemNonShared::parse_json_to_class(Json::Value& attr){
	vector_condition.clear();

	if (!attr["vector_conditions_json"].isNull() && !attr["vector_conditions_json"].empty()){
		Json::Value vector_conditions_json = attr["vector_conditions_json"];

		for (auto member : vector_conditions_json){
			std::shared_ptr<LooterConditions> newItem = std::shared_ptr<LooterConditions>(new LooterConditions);
			newItem->action = (loot_item_action_t)member["action"].asInt();
			newItem->condition = (loot_item_new_condition_t)member["condition"].asInt();
			newItem->value = member["value"].asInt();

			vector_condition.push_back(newItem);
		}
	}

	if (!attr["important"].empty() && !attr["important"].isNull())
		set_important((loot_item_priority_t)attr["important"].asInt());

	if (!attr["condition"].empty() && !attr["condition"].isNull())
		set_condition((loot_item_condition_t)attr["condition"].asInt());

	if (!attr["value"].empty() && !attr["value"].isNull())
		set_value(attr["value"].asInt());

	if (!attr["minimun_count"].empty() && !attr["minimun_count"].isNull())
		set_minimun_count(attr["minimun_count"].asInt());
}

LooterItemNonSharedPtr LooterItem::GetItemNonSharedByCount(uint32_t count){
	for (auto group : non_shared_info)
	if (group.second->get_minimun_count() <= count)
		return group.second;

	return nullptr;
}

bool LooterItem::match_name(std::string& item_name){
	return string_util::string_compare(item_name, name, true);
}

LooterItemNonSharedPtr LooterItem::get_at_group(int group){
	if (non_shared_info.find(group) == non_shared_info.end())
		non_shared_info[group] = LooterItemNonSharedPtr(new LooterItemNonShared);

	return non_shared_info[group];
}

void LooterManager::disable_loot_group(int group){
	set_group_state(group, false);
}

void LooterManager::enable_loot_group(int group){
	set_group_state(group, true);
}

void LooterManager::set_group_state(uint32_t group, bool state){
	if (group_status.size() < group)
		return;
	group_status[group] = state;
}

bool LooterManager::get_loot_group_state(uint32_t group){
	if (group_status.size() < group)
		return false;
	return group_status[group];
}

void LooterManager::set_loots_bag_id(std::string tab_name, uint32_t container_item_id){
	LooterContainerPtr _find = get_loot_container_by_tab_name(tab_name);
	if (_find){
		_find->set_container_id(container_item_id);
	}
}

uint32_t LooterManager::get_loots_bag_id(std::string tab_name){
	LooterContainerPtr _find = get_loot_container_by_tab_name(tab_name);
	if (_find)
		return _find->get_container_id();
	return 0;
}

uint32_t LooterManager::get_loots_bag_item_id(std::string tab_name){
	LooterContainerPtr _find = get_loot_container_by_tab_name(tab_name);
	if (_find){
		return _find->get_container_id();
	}
	return 0;
}

LooterContainerPtr LooterManager::get_loot_container_by_tab_name(std::string name){
	for (auto container_ptr : containers){
		if (_stricmp(&container_ptr.second->get_tab_name()[0], &name[0]) != 0)
			continue;
		return container_ptr.second;
	}
	return nullptr;
}

void LooterManager::set_auto_reopen_container(bool in){
	auto_reopen_container = in;
}

bool LooterManager::get_auto_reopen_container(){
	return auto_reopen_container;
}

void LooterManager::set_open_next_container(bool in){
	open_next_container = in;
}

bool LooterManager::get_open_next_container(){
	return open_next_container;
}

action_retval_t LooterManager::try_sort_loot_containers(){
	std::map< uint32_t /*item_id*/, uint32_t/*container id*/> sort_map;
	for (auto container : containers){
		uint32_t container_id = container.second->get_container_id();
		auto& items_ref = container.second->items;
		for (auto item : items_ref)
			sort_map[item.second->get_item_id()] = container_id;
	}
	return Actions::sort_containers_content(sort_map);
}

void LooterManager::clear(){
	containers.clear();
	for (int i = 0; i < 10; i++){
		group_status[i] = true;
	}
	current_filter_type = loot_filter_t_from_target;
	current_sequence_type = loot_sequence_inteligent;
}

void LooterContainer::set_container_id(uint32_t id){
	container_id = id;
	container_name = ItemsManager::get()->getItemNameFromId(id);
}

void LooterContainer::set_tab_name(std::string name){
	tab_name = name;
}

std::string LooterContainer::get_tab_name(){
	return tab_name;
}

uint32_t LooterManager::get_item_destination_id(uint32_t item_id){
	for (auto container : containers){
		if (container.second->contains_item_id(item_id))
			return container.second->get_container_id();
	}
	return 0;
}

void LooterManager::set_eat_from_corpses_state(bool state){
	eat_from_corpses = state;
}

bool LooterManager::get_eat_from_corpses_state(){
	return eat_from_corpses;
}

void LooterManager::set_eat_from_corpses_delays(std::pair<uint32_t, uint32_t> delays){
	eat_from_corpses_delay = delays;
}

std::pair<uint32_t, uint32_t>  LooterManager::get_eat_from_corpses_delays(){
	return eat_from_corpses_delay;
}

LooterManager* LooterManager::get(){
	static LooterManager* mLooterManager = nullptr;
	if (!mLooterManager)
		mLooterManager = new LooterManager();
	return mLooterManager;
}

void LooterManager::set_time_to_clean(uint32_t state){
	time_to_clean = state;
}

uint32_t LooterManager::get_time_to_clean(){
	return time_to_clean;
}

uint32_t LooterManager::get_next_eat_from_corpses_delay(){
	return rand() % (eat_from_corpses_delay.second - eat_from_corpses_delay.first + 1) + eat_from_corpses_delay.first;
}



std::map<std::string/**/, std::vector<std::pair<std::string, std::string>>> LooterManager::get_duplicated_items(){
	std::map<std::string/**/, std::vector<std::pair<std::string, std::string>>> uniques;
	std::map<std::string/**/, std::vector<std::pair<std::string, std::string>>> duplicateds;


	std::function<bool(std::string, std::string, std::string)> is_duplicated = [&](
		std::string bp_id, std::string item_id, std::string item_name){
		auto it_container = uniques[bp_id];
		for (auto it : it_container){
			if (_stricmp(&it.second[0], &item_name[0]) == 0){
				return true;
			}
		}
		return false;
	};

	for (auto container : containers){
		for (auto item : container.second->items){
			if (is_duplicated(container.first, item.first, item.second->get_name())){
				duplicateds[container.first].push_back(std::pair < std::string, std::string >(item.first, item.second->get_name()));
				continue;
			}
			uniques[container.first].push_back(std::pair < std::string, std::string >(item.first, item.second->get_name()));
		}
	}
	return duplicateds;
}


void LooterManager::set_max_distance(uint32_t state){
	max_distance = state;
}
uint32_t LooterManager::get_max_distance(){
	return max_distance;
}
std::vector<uint32_t> LooterManager::get_all_containers_id(){
	std::vector<uint32_t> rectval;

	for (auto container : containers)
		rectval.push_back(container.second->get_container_id());

	return rectval;
}

bool LooterManager::find_item_by_id(uint32_t item_id){
	for (auto container : containers){

		for (auto item : container.second->items)
		if (item.second->get_item_id() == item_id)
			return true;

	}
	return false;
}

std::vector<uint32_t> LooterManager::get_all_items(){
	std::vector<uint32_t> retval;
	for (auto container : containers)
	for (auto item : container.second->items)
		retval.push_back(item.second->get_item_id());

	return retval;
}
std::vector<std::string> LooterManager::get_all_items_name(){
	std::vector<std::string> retval;
	for (auto container : containers)
		for (auto item : container.second->items)
			retval.push_back(item.second->get_name());

	return retval;
}