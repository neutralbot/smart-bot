#pragma once
#include <string>
#include <map>
#include "xml.h"
#include <memory>
#include <vector>
#include "Core\Util.h"

#pragma pack(push,1)
struct LooterConditions{
	loot_item_action_t action;
	loot_item_new_condition_t condition;
	uint32_t value;
};

class LooterItemNonShared{
public:
	neutral_mutex mtx_access;
	loot_item_priority_t important;
	loot_item_condition_t condition;
	std::vector<std::shared_ptr<LooterConditions>> vector_condition;
	uint32_t value;
	uint32_t minimun_count;

	LooterItemNonShared();

	uint32_t get_value();

	void set_value(uint32_t value);

	void change_content(uint32_t index, uint32_t action, uint32_t condition, uint32_t value);

	void add_vector_condition(loot_item_action_t action, loot_item_new_condition_t condition, uint32_t value);

	void remove_vector_condition(uint32_t index);

	std::vector<std::shared_ptr<LooterConditions>> get_vector_condition();

	DEFAULT_GET_SET(uint32_t, minimun_count)
	DEFAULT_GET_SET(loot_item_priority_t, important)
	DEFAULT_GET_SET(loot_item_condition_t, condition)

	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value& attr);
};

typedef std::shared_ptr<LooterItemNonShared> LooterItemNonSharedPtr;



class LooterItem{
	uint32_t item_id;
	std::string name;
	std::map<int, LooterItemNonSharedPtr> non_shared_info;

public:
	LooterItem();

	void set_name(std::string name);
	std::string get_name();

	DEFAULT_GET_SET(uint32_t, item_id);

	bool match_name(std::string& item_name);

	LooterItemNonSharedPtr GetItemNonSharedByCount(uint32_t count);

	LooterItemNonSharedPtr get_at_group(int group);

	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value& loot_item);
};

typedef std::shared_ptr<LooterItem> LooterItemPtr;


class LooterContainer {
	uint32_t container_id;//armazenar este
	std::string container_name;
	std::string tab_name;

public:
	std::map<std::string /*id*/, std::shared_ptr<LooterItem>> items;

	static LooterContainer* get();
	void set_container_name(std::string name);
	std::string get_container_name();
	uint32_t get_container_id();

	std::string request_new_idItens();
	std::string additem(std::string name, loot_item_priority_t important, loot_item_condition_t condition, int valuee, int minqty, int group);
	void delete_idItem(std::string id_);
	bool contains_item_id(uint32_t item_id);
	bool contains_item_name(std::string& item_id);

	std::shared_ptr<LooterItem> GetLooterRuleById(std::string id_);
	std::shared_ptr<LooterItem> get_target_rule_by_idItens();
	std::pair<LooterItemPtr, LooterItemNonSharedPtr> LooterContainer::get_item_by_name_and_count(std::string& name, uint32_t count);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value& retval);
	void set_container_id(uint32_t id);
	void set_tab_name(std::string name);
	std::string get_tab_name();
};


typedef std::shared_ptr<LooterContainer> LooterContainerPtr;
class LooterManager{
	loot_filter_t current_filter_type;
	loot_sequence_t current_sequence_type;
	std::vector<bool> group_status;
	std::string tab_name;
	bool recoorder = true;
	bool recoord_only_when_hunter_on = false;
	bool auto_reopen_container = true;
	bool eat_from_corpses = true;
	bool open_next_container = true;
	bool min_cap_to_loot = true;
	uint32_t time_to_clean = 30000;
	uint32_t max_distance = 60;
	uint32_t min_cap = 50;
	bool force_move = false;
	std::pair<uint32_t, uint32_t> eat_from_corpses_delay;

public:
	DEFAULT_GET_SET(bool, recoorder);
	DEFAULT_GET_SET(bool, recoord_only_when_hunter_on);
	std::map<std::string/**/, std::vector<std::pair<std::string, std::string>>> get_duplicated_items();


	uint32_t get_next_eat_from_corpses_delay();
	void set_eat_from_corpses_state(bool state);
	bool get_eat_from_corpses_state();
	void set_time_to_clean(uint32_t state);
	uint32_t get_time_to_clean();
	void set_max_distance(uint32_t state);

	void set_min_cap(uint32_t state);

	uint32_t get_min_cap();
	
	void set_force_move(bool state);

	bool get_force_movep();

	void set_min_cap_to_loot(bool state);

	bool get_min_cap_to_loot();

	uint32_t get_max_distance();

	void set_eat_from_corpses_delays(std::pair<uint32_t, uint32_t> delays);
	std::pair<uint32_t, uint32_t> get_eat_from_corpses_delays();
	std::map<std::string /*id*/, std::shared_ptr<LooterContainer>> containers;

	std::vector<uint32_t> get_all_containers_id();
	static LooterManager* get();

	void clear();

	LooterManager();
	std::string request_new_bp_id();
	void delete_bp_id(std::string id);
	bool change_bp_id(std::string newId, std::string oldId);
	bool contains_container_id(uint32_t item_id);
	void set_auto_reopen_container(bool in);

	bool find_item_by_id(uint32_t item_id);
	bool get_auto_reopen_container();

	void set_open_next_container(bool in);

	bool get_open_next_container();

	action_retval_t try_sort_loot_containers();

	std::vector<uint32_t> get_all_items();
	std::vector<std::string> get_all_items_name();

	LooterContainerPtr get_looter_container_by_id(std::string id);
	LooterContainerPtr get_container_owns_by_item_id(uint32_t item_id);
	LooterContainerPtr get_container_owns_by_item_name(std::string& item_name);
	LooterContainerPtr get_loot_container_by_tab_name(std::string name);
	LooterItemNonSharedPtr get_looter_item_by_item_id(uint32_t item_id);


	std::pair<LooterItemPtr, LooterItemNonSharedPtr> get_item_by_name_and_count(std::string& name, uint32_t count);
	/*
		Setter internal sends LooterCore interval var, as this type.
		we dont share this var, just cuzz lua scripts can change this, and we dont want to save changes made by lua scripts,
		just by the user interface
		*/

	loot_type_t current_loot_type;
	loot_type_t LooterManager::get_current_loot_type();

	void LooterManager::set_current_loot_type(loot_type_t in);

	void set_current_filter_type(loot_filter_t type);

	loot_filter_t get_current_filter_type();

	void set_current_sequence_type(loot_sequence_t type);

	loot_sequence_t get_current_sequence_type();

	void disable_loot_group(int group);

	void enable_loot_group(int group);

	void set_group_state(uint32_t group, bool state);

	bool get_loot_group_state(uint32_t group);

	void set_loots_bag_id(std::string tab_name, uint32_t container_item_id);

	uint32_t get_loots_bag_id(std::string tab_name);

	uint32_t get_loots_bag_item_id(std::string tab_name);

	uint32_t get_item_destination_id(uint32_t item_id);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value& hunterValue);

	std::vector<uint32_t> get_our_containers_id();

};

#pragma pack(pop)

