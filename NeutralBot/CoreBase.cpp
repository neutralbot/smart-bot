#include "CoreBase.h"


void CoreBase::set_core_priority(core_priority curr_priority){
	current_priority = curr_priority;
}

core_priority CoreBase::get_core_priority(){
	return (core_priority)current_priority;
}

void CoreBase::set_priority_high(){
	current_priority = core_priority::core_priority_high;
}

void CoreBase::set_priority_normal(){
	current_priority = core_priority::core_priority_normal;
}

CoreBase::CoreBase(){
	current_priority = core_priority::core_priority_normal;
	_state = false;
}

void CoreBase::set_state(bool state){
	//lock_guard _lock(mtx_access);
	_state = state;
}

bool CoreBase::get_state(){
	//lock_guard _lock(mtx_access);
	return _state;
}