#pragma once
#include <vector>
#include <map>
#include <stdint.h>
#include "Core\Util.h"
#pragma pack(push,1)
class GifManager {
	uint32_t item_ref;
	uint32_t creatures_ref;
	
	neutral_mutex mtx_access;

	bool loading_creatures;
	bool loading_items;
	void items_load();
	void creatures_load();
	void items_release();
	void creatures_release();
	GifManager(){
		loading_items = false;
		loading_creatures = false;

		//FAZRER CHECK
		request_items_load();
	}

	void start_load_creature();
	void start_load_items();
public:

	bool has_item(std::string& item_name);
	std::map<std::string, std::pair<char*, uint32_t>> item_gifs_ptr;
	std::map<std::string, std::pair<char*, uint32_t>> creature_gifs_ptr;

	std::pair<char*, uint32_t> get_item_gif_buffer(std::string file_name);
	std::pair<char*, uint32_t> get_item_creature_buffer(std::string file_name);

	void request_items_load();
	void request_items_unload();
	void request_load_creatures();
	void request_unload_creatures();


	void wait_load_items(){
		while (loading_items)
			Sleep(1);
	}
	void wait_load_creatures(){
		while (loading_creatures)
			Sleep(1);
	}
	static GifManager* get(){
		static GifManager* mGifManager = nullptr;

		if (!mGifManager)
			mGifManager = new GifManager();
		
		return mGifManager;
	}
};

#pragma pack(pop)