#pragma once
#include "SpellInfo.h"
#include <json\json.h>
#include "Core\Hotkey.h"

#pragma pack(push,1)
enum area_type_t{
	area_type_circle_3x3,
	area_type_cross_2x2,
	area_type_rect_2x2,
	area_type_none
};

class SpellManager{
	std::map<int, std::shared_ptr<SpellHealth>> healthSpells;
	std::map<int, std::shared_ptr<SpellAttack>> attackSpells;
	std::map<int, area_type_t> spellAreaList;

	use_spell_type spell_type = use_spell_type::use_spell_by_mounster_count;
	bool attack_spells_state = false;
	bool health_spells_state = false;
public:

	SpellManager();

	GET_SET(use_spell_type, spell_type);
	int addNewHealthSpell();
	void removeHealthSpellInList(int id);
	int addAttackSpellInList();
	static std::vector<std::vector<int>> get_spell_area_by_name(area_type_t type);
	area_type_t getSpellAreaEffectByitem_id(int item_id);
	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);

	static SpellManager* get();
	bool get_health_spells_state();
	bool get_attack_spells_state();
	void set_health_spells_state(bool state);
	void set_attack_spells_state(bool state);
	void parse_json_to_classImport(Json::Value jsonObject);
	
	int requestNewHealthSpells();
	int requestNewAttackSpells();
	std::vector<std::pair<std::string, spell_type_t>> missing_hotkeys();

	void removeAttackSpellInList(int index);
	std::shared_ptr<SpellAttack> getAttackSpellInListById(uint32_t id);
	std::shared_ptr<SpellHealth> getHealthSpellInListById(int id);

	void clear_all();
	
	std::map<int, std::shared_ptr<SpellHealth>>& getHealthList();	
	std::map<int, std::shared_ptr<SpellAttack>>& getAttackList();
	
	SpellBase* get_spell_by_id(std::string id);
	bool get_spell_state_by_id(std::string id);

	void disable_spell_by_id(std::string id);
	void enable_spell_by_id(std::string id);
};
#pragma pack(pop)

