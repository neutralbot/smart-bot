#pragma once
using namespace System;
using namespace System::ComponentModel;

public ref class MonsterAttackInfo : public System::ComponentModel::Component {

protected:
	String^ _name;
	int _min_hp = 0;
	int _max_hp = 100;

public:
	[Description("Monster Name.")]
	virtual property String^ Name {
		virtual void set(String^ in){
			_name = in;
		}

		virtual String^ get(){
			return _name;
		}
	}

	[Description("Monster min health to cast spell.")]
	virtual property int min_hp {
		virtual void set(int in){
			_min_hp = in;
		}

		virtual int get(){
			return _min_hp;
		}
	}

	[Description("Monster maxn health to cast spell.")]
	virtual property int max_hp{
		virtual void set(int in){
			_max_hp = in;
		}

		virtual int get(){
			return _max_hp;
		}
	}
};