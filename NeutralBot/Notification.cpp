#include "Notification.h"
#include <stdint.h>
#include <iostream>

bool IsVistaOrLater() {
	DWORD version = GetVersion();
	DWORD major = (DWORD)(LOBYTE(LOWORD(version)));
	DWORD minor = (DWORD)(HIBYTE(LOWORD(version)));
	return (major > 6) || ((major == 6) && (minor >= 0));
}

BOOL SetMessageDuration(uint32_t timeout, uint32_t flags = 0){
	return SystemParametersInfo(0x2017, 0, IntToPtr(timeout), flags);
}

void notify(int id, char* message, char*title, int timeout){
	if (IsVistaOrLater())
		SetMessageDuration(timeout, 0);
	NOTIFYICONDATA nid;
	memset(&nid, 0, sizeof(NOTIFYICONDATA));
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = 0;
	nid.uID = id;
	nid.uFlags = NIF_INFO;
	//memcpy(nid.szInfo, message, 50);

	for (int i = 0; i < 255; i++){
		nid.szInfo[i] = message[i];
		if (!message[i])
			break;
	}

	for (int i = 0; i < 63; i++){
		nid.szInfoTitle[i] = title[i];
		if (!title[i])
			break;
	}


	nid.dwInfoFlags = NIIF_INFO;
	nid.uTimeout = timeout;

	Shell_NotifyIcon(NIM_DELETE, &nid);
	Shell_NotifyIcon(NIM_ADD, &nid);
}


void NotificationManager::notify_baloon(char* caption, char* text, int time_ms){
	notify(9999, text, caption, time_ms);
}
void NotificationManager::print_console(char* text){
	std::cout << text;
}
void NotificationManager::add_error_log(char* error_group, char* error_text){
	//IMPLEMENT
}
bool NotificationManager::show_message_box(char* caption, char* text){
	//IMPLEMENT TYPE
	return MessageBoxA(0, text, caption, MB_YESNO) == 1;
}
