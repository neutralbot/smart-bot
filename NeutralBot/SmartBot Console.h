#pragma once
#include "KeywordManager.h"
#include "ManagedUtil.h"
namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class SmartBotConsole : public Telerik::WinControls::UI::RadForm{
	public:
		SmartBotConsole(void){
			InitializeComponent();
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}

	protected: ~SmartBotConsole(){
				   unique = nullptr;
			if (components)
				delete components;
		}
	protected: static SmartBotConsole^ unique;

	public: static void ShowUnique(){
				if (unique == nullptr)
					unique = gcnew SmartBotConsole();

				unique->BringToFront();
				unique->Show();
	}public: static void CreatUnique(){
		if (unique == nullptr)
			unique = gcnew SmartBotConsole();
	}

			   static void CloseUnique(){
				   if (unique)unique->Close();
			   }
	private: Telerik::WinControls::UI::RadListView^  list_console;
	protected:

	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(SmartBotConsole::typeid));
			this->list_console = (gcnew Telerik::WinControls::UI::RadListView());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->list_console))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// list_console
			// 
			this->list_console->Dock = System::Windows::Forms::DockStyle::Fill;
			this->list_console->Location = System::Drawing::Point(0, 0);
			this->list_console->Name = L"list_console";
			this->list_console->Size = System::Drawing::Size(450, 340);
			this->list_console->TabIndex = 0;
			// 
			// SmartBotConsole
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(450, 340);
			this->Controls->Add(this->list_console);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			//this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"SmartBotConsole";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->ShowIcon = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"SmartBotConsole";
			this->Load += gcnew System::EventHandler(this, &SmartBotConsole::SmartBotConsole_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->list_console))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void SmartBotConsole_Load(System::Object^  sender, System::EventArgs^  e) {
				 auto temp = KeywordManager::get()->missing_sugestion;
				 for (auto item : temp)
					 list_console->Items->Add(gcnew String(item.c_str()));
				 
	}
	};
}
