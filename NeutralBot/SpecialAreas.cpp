#pragma once
#include "SpecialAreas.h"
#include "Core\\TibiaProcess.h"
#include "Core\LuaCore.h"

void SABaseElement::on_delete(){
	for (auto child : childrens)
		delete child;
	
	delete avoidance_ptr; 

	delete walk_dificult_ptr;
} 
 
SABaseElement::SABaseElement(special_area_element_t type, SABaseElement* parent) :
	owner(parent), element_t(type){ 

	avoidance_ptr = new SAAvoidance; 
	walk_dificult_ptr = new SAWalkDificult;  

	creat_events(); 
} 

SABaseElement::~SABaseElement(){
	on_delete();
}

void SABaseElement::clear(){
	for (auto child : childrens){
		remove_element((SABaseElement*)child);
	}
}

SAAvoidance* SABaseElement::get_avoidance(){
	return avoidance_ptr;
}

SAWalkDificult* SABaseElement::get_walk_dificult(){
	return walk_dificult_ptr;
}

bool SABaseElement::get_block_walk(){
	return block_walk;
}

Coordinate SABaseElement::get_center(){
	//this->get_();
	Rect rect = ((SAArea*)this)->get_rect();

	return Coordinate(rect.get_center().x, rect.get_center().y);
}


void SABaseElement::set_block_walk(bool block){
	block_walk = block;
}

std::string SABaseElement::get_name(){
	return name;
}

void SABaseElement::set_name(std::string name){
	this->name = name;
}

void SABaseElement::set_owner(SABaseElement* new_onwer){
	this->owner = new_onwer;
}

SABaseElement* SABaseElement::get_owner(){
	return owner;
}

bool SABaseElement::is_area(){
	return element_t == sa_element_area;
}


special_area_activation_t SABaseElement::get_activation_trigger(){
	return activation_trigger;
}

void SABaseElement::set_activation_trigger(special_area_activation_t trigger_type){
	activation_trigger = trigger_type;
}

void SABaseElement::set_state(bool _state){
	state = _state;
}

bool SABaseElement::get_state(){
	return state;
}

bool SABaseElement::exist_raw_pointer(SABaseElement* element){
	if (element == this)
		return true;

	for (auto child : childrens){
		if (element == child){
			return true;
		}
		else if (element->exist_raw_pointer(element)){
			return true;
		}
	}
	return false;
}

std::vector<SABaseElement*> SABaseElement::get_childrens(){
	return childrens;
}


bool SABaseElement::is_wall(){
	return element_t == sa_element_wall;
}

bool SABaseElement::is_point(){
	return element_t == sa_element_point;
}

special_area_element_t SABaseElement::get_type(){
	return element_t;
}

void SABaseElement::set_type(special_area_element_t type){
	element_t = type;
}

void SABaseElement::check_coord_change(Coordinate& coord){
	if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
		return;

	if (bool_is_in_area){
		if (!(bool_is_in_area = is_in_area(coord.x, coord.y, coord.z))){
			on_out_area();
		}
	}
	else{
		if ((bool_is_in_area = is_in_area(coord.x, coord.y, coord.z))){
			on_enter_area();
		}
	}
}

void SABaseElement::on_enter_area(){
	lock_guard m_lock(mtx_access);

	std::shared_ptr<SAEvent> rule = get_rule_event((int)special_area_event_enter_area);
	if (!rule)
		return;

	mtx_access.lock();
	std::string event_code = rule->get_event_code();
	std::string event_code_end = string_util::trim(event_code);
	mtx_access.unlock();

	if (event_code_end == "")
		return;	

	LuaCore::getState(lua_core_ids::lua_core_special_areas_id)->RunScript(event_code, "special_area_event_enter_area");

}

void SABaseElement::on_out_area(){
	std::shared_ptr<SAEvent> rule = get_rule_event((int)special_area_event_leave_area);
	if (!rule)
		return;

	mtx_access.lock();
	std::string event_code = rule->event_code;
	std::string event_code_end = string_util::trim(event_code);
	mtx_access.unlock();

	if (event_code_end == "")
		return;
	
	LuaCore::getState(lua_core_ids::lua_core_special_areas_id)->RunScript(event_code, "special_area_event_leave_area");
}


bool SABaseElement::is_in_area(uint32_t x, uint32_t y, int16_t z){
	switch (get_type()){
	case special_area_element_t::sa_element_area:{
													 if (((SAArea*)this)->z != z)
														 return false;

		return Util::rect_contains_point(((SAArea*)this)->get_rect(), Point(x, y));
	}
		break;
	case special_area_element_t::sa_element_wall:{
													 if (((SAWallElement*)this)->z != z)
														 return false;

		return Util::rect_contains_point(((SAWallElement*)this)->get_rect(), Point(x, y));
	}
		break;
	case special_area_element_t::sa_element_point:{
													  if (((SAPointElement*)this)->z != z)
														  return false;

		return
			Util::circle_contains_point(((SAPointElement*)this)->get_avoidance_rect(), Point(x, y))
			||
			Util::circle_contains_point(((SAPointElement*)this)->get_walkability_rect(), Point(x, y));
	}
	}
	return false;
}

SABaseElement* SABaseElement::create_child(special_area_element_t type){
	SABaseElement* retval = nullptr;
	switch (type){
	case sa_element_area:{
		retval = new SAArea(this);
		retval->set_name("area element");
	}
		break;
	case sa_element_point:{
		retval = new SAPointElement(this);
		retval->set_name("area point");
	}
		break;
	case sa_element_wall:{
		retval = new SAWallElement(this);
		retval->set_name("area wall");
	}
		break;
	}
	retval->set_owner(this);
	childrens.push_back(retval);
	return retval;
}

bool SABaseElement::is_type_of(special_area_element_t element_t){
	if (element_t == sa_element_any)
		return true;
	return this->element_t == element_t;
}

Rect SAWallElement::get_rect(){
	return Rect(start_x, start_y, end_x - start_x, end_y - start_y);
}

std::string SABaseElement::get_on_enter_area_event(){
	if (event_enter_str_ptr)
		return *event_enter_str_ptr;
	return "";
}

std::string SABaseElement::get_on_out_area_event(){
	if (event_out_str_ptr)
		return *event_out_str_ptr;
	return "";
}

SpecialAreaManager::SpecialAreaManager() : root_area(new SAArea(nullptr)){
	root_area->set_name("root");
	TibiaProcess::get_default()->add_on_change_coordinate_callback(
		[&](Coordinate& old_coord, Coordinate& new_coord){
			root_area->check_coord_change(new_coord);
			auto childs = root_area->get_childrens();
			for (auto child : childs){
				if (!child)
					continue;

				child->check_coord_change(new_coord);
			}
		});
}

bool SpecialAreaManager::exist_raw_pointer(SABaseElement* element){
	return root_area->exist_raw_pointer(element);
}

SAArea* SpecialAreaManager::get_root(){
	return root_area;
}

SpecialAreaManager* SpecialAreaManager::get(){
	static SpecialAreaManager* mSpecialAreaManager;

	if (!mSpecialAreaManager)
		mSpecialAreaManager = new SpecialAreaManager();
	
	return mSpecialAreaManager;
}

SABaseElement* SABaseElement::get_child_by_name(std::string name, bool recursive, special_area_element_t element_t){
	for (auto child : childrens){
		if (child->get_name() == name && child->is_type_of(element_t))
			return child;

		if (!recursive)
			continue;

		SABaseElement* child_retval = child->get_child_by_name(name, recursive, element_t);
		if (child_retval)
			return child_retval;
	}
	return nullptr;
}

std::vector<SABaseElement*> SABaseElement::get_childrens_by_name(std::string name, bool recursive, special_area_element_t element_t){
	std::vector<SABaseElement*> retval;
	for (auto child : childrens){
		if (child->get_name() == name && child->is_type_of(element_t))
			retval.push_back(child);

		if (!recursive)
			continue;
		std::vector<SABaseElement*> child_retval = child->get_childrens_by_name(name, recursive, element_t);
		retval.insert(std::end(retval), std::begin(child_retval), std::end(child_retval));
	}
	return retval;
}

std::vector<SABaseElement*> SABaseElement::get_childrens_by_type(special_area_element_t _element_t, bool recursive ){
	std::vector<SABaseElement*> retval;
	for (auto child : childrens){
		if (child->is_type_of(element_t))
			retval.push_back(child);

		if (!recursive)
			continue;

		std::vector<SABaseElement*> child_retval = child->get_childrens_by_type(element_t, recursive);
		retval.insert(std::end(retval), std::begin(child_retval), std::end(child_retval));
	}
	return retval;
}

SABaseElement* SABaseElement::get_childrens_by_raw_pointer(SABaseElement* pointer, bool recursive){

	for (auto child = childrens.begin(); child != childrens.end(); child++){
		if (*child == pointer){
			return pointer;
		}
	}
	return nullptr;
}

SABaseElement* SABaseElement::remove_element(SABaseElement* element_ptr, bool delete_after_remove){
	for (auto child = childrens.begin(); child != childrens.end(); child++){
		if (*child == element_ptr){
			if (delete_after_remove){
				delete *child;
				childrens.erase(child);
				return nullptr;
			}
			else{
				childrens.erase(child);
				return *child;
			}
		}
	}
	return nullptr;
}

SAArea::SAArea(SABaseElement* owner) :
	SABaseElement(special_area_element_t::sa_element_area){
	this->walk_dificult_ptr->decay = 0;
	this->walk_dificult_ptr->range = 0;
	this->walk_dificult_ptr->value = 0;

	this->avoidance_ptr->decay = 0;
	this->avoidance_ptr->range = 0;
	this->avoidance_ptr->value = 0;
}

void SAArea::normalize_negative(){
	if (end_x < start_x){
		int32_t swap_val = end_x;
		end_x = start_x;
		start_x = swap_val;
	}
	if (end_x < 0 || start_x < 0 || end_y < 0 || start_y < 0){
		int ss = int();
	}
	if (end_y < start_y){
		int32_t swap_val = end_y;
		end_y = start_y;
		start_y = swap_val;
	}
}

SAPointElement::SAPointElement(SABaseElement* owner) :
	SABaseElement(special_area_element_t::sa_element_point){
	this->walk_dificult_ptr->decay = 0;
	this->walk_dificult_ptr->range = 10;
	this->walk_dificult_ptr->value = 0;

	this->avoidance_ptr->decay = 0;
	this->avoidance_ptr->range = 10;
	this->avoidance_ptr->value = 0;
}

SAWallElement::SAWallElement(SABaseElement* owner) :
	SABaseElement(special_area_element_t::sa_element_wall){
	block_walk = true;

	this->walk_dificult_ptr->decay = 0;
	this->walk_dificult_ptr->range = 0;
	this->walk_dificult_ptr->value = 0;

	this->avoidance_ptr->decay = 0;
	this->avoidance_ptr->range = 0;
	this->avoidance_ptr->value = 0;

	set_width(1);
}

void SAWallElement::set_width(int32_t value){
	width = value;
}

int32_t SAWallElement::get_width(){
	return width;
}

Json::Value SABaseElement::ClassToJson(){
	Json::Value value;
	value["name"] = name;
	value["sub_type"] = (uint32_t)element_t;
	value["activation_trigger"] = this->activation_trigger;
	value["avoidance_info"] = this->avoidance_ptr->ClassToJson();
	value["block_walk"] = this->block_walk;
	value["walk_dificult_info"] = this->walk_dificult_ptr->ClassToJson();
	value["state"] = this->state;


	switch (element_t){
	case special_area_element_t::sa_element_point:{
		SAPointElement* point = (SAPointElement*)this;
		value["x"] = point->x;
		value["y"] = point->y;
		value["z"] = point->z;
	}
	case special_area_element_t::sa_element_wall:{
		SAWallElement* wall = (SAWallElement*)this;
		value["start_x"] = wall->start_x;
		value["start_y"] = wall->start_y;
		value["end_x"] = wall->end_x;
		value["end_y"] = wall->end_y;
		value["z"] = wall->z;
		value["width"] = wall->width;
	}
	case special_area_element_t::sa_element_area:{
		SAArea* area = (SAArea*)this;
		value["start_x"] = area->start_x;
		value["start_y"] = area->start_y;
		value["end_x"] = area->end_x;
		value["end_y"] = area->end_y;
		value["z"] = area->z;
		value["width"] = area->width;
	}
	}


	Json::Value& childs = value["childrens"];
	for (auto child : childrens)
		childs.append(child->ClassToJson());
	
	Json::Value vector_event_json;
	Json::Value vector_activation_trigger_json;

	for (auto repot : vector_event){
		Json::Value monsterListItem;

		monsterListItem["event_code"] = repot->get_event_code();
		monsterListItem["event_type"] = repot->get_event_type();

		vector_event_json.append(monsterListItem);
	}
	for (auto repot : vector_activation_trigger){
		Json::Value monsterListItem;

		monsterListItem["activation_trigger"] = (int)repot->activation_trigger;

		vector_activation_trigger_json.append(monsterListItem);
	}

	value["vector_event_json"] = vector_event_json;
	value["vector_activation_trigger_json"] = vector_activation_trigger_json;

	return value;
}

bool SABaseElement::JsonToClass(Json::Value& value){
	clear();

	if (!value["name"].empty() || !value["name"].isNull())
		name = value["name"].asString();

	if (!value["sub_type"].empty() || !value["sub_type"].isNull())
		element_t = (special_area_element_t)value["sub_type"].asUInt();

	if (!value["activation_trigger"].empty() || !value["activation_trigger"].isNull())
		activation_trigger = (special_area_activation_t)value["activation_trigger"].asUInt();

	if (!value["avoidance_info"].empty() || !value["avoidance_info"].isNull())
		avoidance_ptr = SAAvoidance::JsonToClass(value["avoidance_info"]);

	if (!value["block_walk"].empty() || !value["block_walk"].isNull())
		block_walk = value["block_walk"].asBool();

	if (!value["walk_dificult_info"].empty() || !value["walk_dificult_info"].isNull())
		walk_dificult_ptr = SAWalkDificult::JsonToClass(value["walk_dificult_info"]);

	if (!value["state"].empty() || !value["state"].isNull())
		state = value["state"].asBool();
	
	mtx_access.lock();
	vector_event.clear();
	mtx_access.unlock();

	if (!value["vector_event_json"].isNull() && !value["vector_event_json"].empty()){
		Json::Value vector_event_json = value["vector_event_json"];

		for (auto member : vector_event_json){
			std::string event_code = "";
			if (!member["event_code"].empty() || !member["event_code"].isNull())
				event_code = member["event_code"].asString();

			special_area_event_t event_type = (special_area_event_t)0;
			if (!member["event_type"].empty() || !member["event_type"].isNull())
				event_type = (special_area_event_t)member["event_type"].asInt();

			std::shared_ptr<SAEvent> newItem = std::shared_ptr<SAEvent>(new SAEvent());

			newItem->set_event_code(event_code);
			newItem->set_event_type(event_type);

			vector_event.push_back(newItem);
		}
	}
	else
		creat_events();

	vector_activation_trigger.clear();

	if (!value["vector_activation_trigger_json"].isNull() && !value["vector_activation_trigger_json"].empty()){
		Json::Value vector_activation_trigger_json = value["vector_activation_trigger_json"];

		for (auto member : vector_activation_trigger_json){
			special_area_activation_t activation_trigger = (special_area_activation_t)0;
			if (!member["activation_trigger"].empty() || !member["activation_trigger"].isNull())
				activation_trigger = (special_area_activation_t)member["activation_trigger"].asInt();

			std::shared_ptr<SAActivationTrigger> newItem = std::shared_ptr<SAActivationTrigger>(new SAActivationTrigger());
			newItem->set_activation_trigger(activation_trigger);

			vector_activation_trigger.push_back(newItem);
		}
	}



	switch (element_t){
	case special_area_element_t::sa_element_point:{
		SAPointElement* point = (SAPointElement*)this;
		if (!value["x"].empty() || !value["x"].isNull())
			point->x = value["x"].asInt();
		if (!value["y"].empty() || !value["y"].isNull())
			point->y = value["y"].asInt();
		if (!value["z"].empty() || !value["z"].isNull())
			point->z = value["z"].asInt();
	}
	case special_area_element_t::sa_element_wall:{
		SAWallElement* wall = (SAWallElement*)this;
		if (!value[""].empty() || !value[""].isNull())
			wall->start_x = value["start_x"].asInt();
		if (!value[""].empty() || !value[""].isNull())
			wall->start_y = value["start_y"].asInt();
		if (!value[""].empty() || !value[""].isNull())
			wall->end_x = value["end_x"].asInt();
		if (!value[""].empty() || !value[""].isNull())
			wall->end_y = value["end_y"].asInt();
		if (!value[""].empty() || !value[""].isNull())
			wall->z = value["z"].asInt();
		if (!value[""].empty() || !value[""].isNull())
			wall->width = value["width"].asInt();
	}
	case special_area_element_t::sa_element_area:{
		SAArea* area = (SAArea*)this;
		if (!value["start_x"].empty() || !value["start_x"].isNull())
			area->start_x = value["start_x"].asInt();
		if (!value["start_y"].empty() || !value["start_y"].isNull())
			area->start_y = value["start_y"].asInt();
		if (!value["end_x"].empty() || !value["end_x"].isNull())
			area->end_x = value["end_x"].asInt();
		if (!value["end_y"].empty() || !value["end_y"].isNull())
			area->end_y = value["end_y"].asInt();
		if (!value["z"].empty() || !value["z"].isNull())
			area->z = value["z"].asInt();
		if (!value["width"].empty() || !value["width"].isNull())
			area->width = value["width"].asInt();
	}
	}

	if (!value["childrens"].empty() || !value["childrens"].isNull()){
		Json::Value& childrens = value["childrens"];

		for (auto child : childrens){
			SABaseElement* belement = create_child((special_area_element_t)child["sub_type"].asUInt());
			belement->JsonToClass(child);
		}
	}
	return true;
}

Rect SAArea::get_rect(){
	return Rect(start_x, start_y, end_x - start_x, end_y - start_y);
}

Point SAPointElement::get_point(){
	return Point(x, y);
}

Rect SAPointElement::get_avoidance_rect(){
	uint32_t radius = get_avoidance()->range;
	return Rect(x - radius, y - radius, radius * 2, radius * 2);
}

Rect SAPointElement::get_walkability_rect(){
	uint32_t radius = get_walk_dificult()->range;
	return Rect(x - radius, y - radius, radius * 2, radius * 2);
}

std::pair<Point, Point> SAWallElement::get_line(){
	uint32_t w = get_width() / 2;
	return std::pair<Point, Point>(Point(start_x - w, start_y - w), Point(end_x + w, end_y + w));
}

Json::Value SAWalkDificult::ClassToJson(){
	Json::Value retval;
	retval["range"] = range;
	retval["value"] = value;
	retval["decay"] = decay;
	return retval;
}

SAWalkDificult* SAWalkDificult::JsonToClass(Json::Value& value){
	SAWalkDificult* retval = new SAWalkDificult();
	retval->range = value["range"].asInt();
	retval->value = value["value"].asInt();
	retval->decay = (float)value["decay"].asDouble();
	return retval;
}
Json::Value SAAvoidance::ClassToJson(){
	Json::Value retval;
	retval["range"] = range;
	retval["value"] = value;
	retval["decay"] = decay;
	return retval;
}

SAAvoidance* SAAvoidance::JsonToClass(Json::Value& value){
	SAAvoidance* retval = new SAAvoidance();
	retval->range = value["range"].asInt();
	retval->value = value["value"].asInt();
	retval->decay = (float)value["decay"].asDouble();
	return retval;
}
