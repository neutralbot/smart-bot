#include "WaypointPath.h"
#include <boost/lexical_cast.hpp>

int WaypointPath::requestNewWaypointInfoIndex(){
	int currentId = this->waypointInfoList.size();

	auto newWaypointInfo = std::shared_ptr<WaypointInfo>(new WaypointInfo);
	this->waypointInfoList.push_back(newWaypointInfo);

	return currentId; 
} 

void WaypointPath::setWaypointInfo(int index, std::shared_ptr<WaypointInfo> newWaypointInfo){
	if (index >= static_cast<int>(this->waypointInfoList.size()))
		return;
	 
	this->waypointInfoList[index] = newWaypointInfo; 
}

std::shared_ptr<WaypointInfo> WaypointPath::getWaypointInfo(int index)
{
	if (index >= static_cast<int>(this->waypointInfoList.size()) || index < 0)
		return nullptr;

	return this->waypointInfoList[index];
}

int WaypointPath::getWaypointIndexOf(std::shared_ptr<WaypointInfo> info){
	auto it = std::find(waypointInfoList.begin(), waypointInfoList.end(), info);
	
	return (it != waypointInfoList.end() ? (it - waypointInfoList.begin()) : -1);
}

std::shared_ptr<WaypointInfo> WaypointPath::getWaypointInfoByRawPointer(WaypointInfo* raw_ptr){
	for (auto wpt : waypointInfoList){
		if (wpt.get() == raw_ptr)
			return wpt;
	}
	return nullptr;
}

void WaypointPath::removeWaypointInfo(int index)
{
	if (index >= static_cast<int>(this->waypointInfoList.size()))
		return;

	this->waypointInfoList.erase(this->waypointInfoList.begin() + index);
}

int WaypointPath::getNextWaypointInfoIdIgnoreLure(int currentIndex){
	if (!waypointInfoList.size())
		return -1;

	int index = currentIndex + 1;
	while (true){
		if (index >= static_cast<int>(this->waypointInfoList.size()))
			return 0;

		if (index < 0)
			return waypointInfoList.size() - 1;

		if (waypointInfoList[index]->get_action() != waypoint_lure_here_distance)
			return index;

		index++;
		Sleep(10);
	}
	return index;
}

void WaypointPath::removeWaypointInfo(std::string label){
	auto found = std::find_if(waypointInfoList.begin(), waypointInfoList.end(), [&](std::shared_ptr<WaypointInfo> info){ return info->get_label() == label; });
}

void WaypointPath::insertWaypointInfo(int index, std::shared_ptr<WaypointInfo> waypointInfo){
	if (index > static_cast<int>(this->waypointInfoList.size()))
		return;
	else if (index == static_cast<int>(this->waypointInfoList.size())){
		this->waypointInfoList.push_back(waypointInfo);
		return;
	}


	this->waypointInfoList.insert(this->waypointInfoList.begin() + index, waypointInfo);
}

int WaypointPath::getNextWaypointInfoId(int currentIndex)
{
	int index = currentIndex + 1;
	if (!waypointInfoList.size())
		return -1;
	if (index >= static_cast<int>(this->waypointInfoList.size()))
		return 0;
	if (index < 0)
		return waypointInfoList.size() - 1;

	return index;
}

int WaypointPath::getBeforeWaypointInfoId(int currentIndex){
	int index = currentIndex - 1;
	if (index < 0)
		index = (int)waypointInfoList.size() - 1;
	if (index >= static_cast<int>(this->waypointInfoList.size()))
		return 0;
	return index;
}

std::shared_ptr<WaypointInfo> WaypointPath::getNextWaypointInfo(int currentIndex)
{
	return getWaypointInfo(getNextWaypointInfoId(currentIndex));
}

std::shared_ptr<WaypointInfo> WaypointPath::getBeforeWaypointInfo(int currentIndex)
{
	return getWaypointInfo(getBeforeWaypointInfoId(currentIndex));
}

std::shared_ptr<WaypointInfo> WaypointPath::getBeforeWaypointInfoLure(int currentIndex, int offset){
	int index = (currentIndex - 1) - offset;
	if (index < 0)
		index = (int)waypointInfoList.size() - 1;
	if (index >= static_cast<int>(this->waypointInfoList.size()) || !this->waypointInfoList.size())
		return 0;
	return waypointInfoList[index];
}

int WaypointPath::getWaypointInfoIdByLabel(std::string label) {
	for (uint32_t index = 0; index < waypointInfoList.size(); index++) {
		if (string_util::lower(waypointInfoList[index]->get_label()) == string_util::lower(label))
			return index;
	}
	return -1;
}

std::shared_ptr<WaypointInfo> WaypointPath::getWaypointInfoByLabel(std::string label){
	for (uint32_t index = 0; index < waypointInfoList.size(); index++) {
		std::string label_waypointInfo = waypointInfoList[index]->get_label();
		
		if (_stricmp(&label_waypointInfo[0], &label[0]) == 0)
			return waypointInfoList[index];
	}
	return nullptr;
}

Json::Value WaypointPath::parse_class_to_json() {
	Json::Value waypointPath;
	waypointPath["label"] = this->label;
	waypointPath["name"] = this->name;	

	Json::Value waypointInfoList;

	for (uint32_t index = 0; index < this->waypointInfoList.size(); index++)
		waypointInfoList[index] = this->waypointInfoList[index]->parse_class_to_json();
	


	waypointPath["waypointInfoList"] = waypointInfoList;
	return waypointPath;
}

void WaypointPath::parse_json_to_class(Json::Value jsonObject,int version) {
	if (!jsonObject["label"].empty() || !jsonObject["label"].isNull())
		this->label = jsonObject["label"].asString();

	if (!jsonObject["name"].empty() || !jsonObject["name"].isNull())
		this->name = jsonObject["name"].asString();

	if (!jsonObject["waypointInfoList"].empty() || !jsonObject["waypointInfoList"].isNull()){
		Json::Value& jsonWaypointInfoList = jsonObject["waypointInfoList"];


		for (uint32_t index = 0; index < jsonWaypointInfoList.size(); index++) {
			Json::Value& waypointInfo = jsonWaypointInfoList[index];
			std::shared_ptr<WaypointInfo> waypoint(new WaypointInfo);
			waypoint->parse_json_to_class(waypointInfo, version);
			waypointInfoList.push_back(waypoint);
		}
	}
}

void WaypointPath::setName(std::string _name){
	name = _name;
}

std::string WaypointPath::getName(){
	return name;
}