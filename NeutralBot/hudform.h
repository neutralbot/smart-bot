#pragma once
#include "Core\Interface.h"
#include "Core\ContainerManager.h"
namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for hudform
	/// </summary>
	public ref class hudform : public System::Windows::Forms::Form
	{
	public:
		hudform(void)
		{
			InitializeComponent();
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~hudform()
		{
			if (components)
			{
				delete components;
			}
		}
	 System::Windows::Forms::Timer^  autoupdatetimer;
	 Telerik::WinControls::UI::RadPanel^  bp1;
	 Telerik::WinControls::UI::RadPanel^  slot8;
	 Telerik::WinControls::UI::RadPanel^  slot7;
	 Telerik::WinControls::UI::RadPanel^  slot6;
	 Telerik::WinControls::UI::RadPanel^  slot5;
	 Telerik::WinControls::UI::RadPanel^  slot4;
	 Telerik::WinControls::UI::RadPanel^  slot2;
	 Telerik::WinControls::UI::RadPanel^  slot3;
	 Telerik::WinControls::UI::RadPanel^  slot1;
	 Telerik::WinControls::UI::RadPanel^  slot12;
	 Telerik::WinControls::UI::RadPanel^  slot11;
	 Telerik::WinControls::UI::RadPanel^  slot10;
	 Telerik::WinControls::UI::RadPanel^  slot9;

	protected:
	 System::ComponentModel::IContainer^  components;

	
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->autoupdatetimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->bp1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot12 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot11 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot10 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot9 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot8 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot7 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot6 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot5 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot4 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot3 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->slot1 = (gcnew Telerik::WinControls::UI::RadPanel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp1))->BeginInit();
			this->bp1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot12))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot11))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot10))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot9))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot8))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot7))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot6))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot5))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot1))->BeginInit();
			this->SuspendLayout();
			// 
			// autoupdatetimer
			// 
			this->autoupdatetimer->Enabled = true;
			this->autoupdatetimer->Tick += gcnew System::EventHandler(this, &hudform::autoupdatetimer_Tick);
			// 
			// bp1
			// 
			this->bp1->Controls->Add(this->slot12);
			this->bp1->Controls->Add(this->slot11);
			this->bp1->Controls->Add(this->slot10);
			this->bp1->Controls->Add(this->slot9);
			this->bp1->Controls->Add(this->slot8);
			this->bp1->Controls->Add(this->slot7);
			this->bp1->Controls->Add(this->slot6);
			this->bp1->Controls->Add(this->slot5);
			this->bp1->Controls->Add(this->slot4);
			this->bp1->Controls->Add(this->slot2);
			this->bp1->Controls->Add(this->slot3);
			this->bp1->Controls->Add(this->slot1);
			this->bp1->Location = System::Drawing::Point(-46, 40);
			this->bp1->Name = L"bp1";
			this->bp1->Size = System::Drawing::Size(578, 391);
			this->bp1->TabIndex = 0;
			this->bp1->Text = L"radPanel1";
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->bp1->GetChildAt(0)))->Text = L"radPanel1";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->bp1->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->bp1->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Red;
			// 
			// slot12
			// 
			this->slot12->Location = System::Drawing::Point(213, 169);
			this->slot12->Name = L"slot12";
			this->slot12->Size = System::Drawing::Size(200, 100);
			this->slot12->TabIndex = 5;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot12->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot12->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot12->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot11
			// 
			this->slot11->Location = System::Drawing::Point(205, 161);
			this->slot11->Name = L"slot11";
			this->slot11->Size = System::Drawing::Size(200, 100);
			this->slot11->TabIndex = 4;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot11->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot11->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot11->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot10
			// 
			this->slot10->Location = System::Drawing::Point(197, 153);
			this->slot10->Name = L"slot10";
			this->slot10->Size = System::Drawing::Size(200, 100);
			this->slot10->TabIndex = 3;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot10->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot10->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot10->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot9
			// 
			this->slot9->Location = System::Drawing::Point(189, 145);
			this->slot9->Name = L"slot9";
			this->slot9->Size = System::Drawing::Size(200, 100);
			this->slot9->TabIndex = 2;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot9->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot9->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot9->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot8
			// 
			this->slot8->Location = System::Drawing::Point(309, 16);
			this->slot8->Name = L"slot8";
			this->slot8->Size = System::Drawing::Size(200, 100);
			this->slot8->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot8->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot8->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot8->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot7
			// 
			this->slot7->Location = System::Drawing::Point(74, 139);
			this->slot7->Name = L"slot7";
			this->slot7->Size = System::Drawing::Size(200, 100);
			this->slot7->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot7->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot7->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot7->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot6
			// 
			this->slot6->Location = System::Drawing::Point(131, 136);
			this->slot6->Name = L"slot6";
			this->slot6->Size = System::Drawing::Size(200, 100);
			this->slot6->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot6->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot6->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot6->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot5
			// 
			this->slot5->Location = System::Drawing::Point(322, 30);
			this->slot5->Name = L"slot5";
			this->slot5->Size = System::Drawing::Size(200, 100);
			this->slot5->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot5->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot5->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot5->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot4
			// 
			this->slot4->Location = System::Drawing::Point(352, 95);
			this->slot4->Name = L"slot4";
			this->slot4->Size = System::Drawing::Size(200, 100);
			this->slot4->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot4->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot4->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot4->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot2
			// 
			this->slot2->Location = System::Drawing::Point(92, 16);
			this->slot2->Name = L"slot2";
			this->slot2->Size = System::Drawing::Size(200, 100);
			this->slot2->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot2->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot2->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot2->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot3
			// 
			this->slot3->Location = System::Drawing::Point(177, 168);
			this->slot3->Name = L"slot3";
			this->slot3->Size = System::Drawing::Size(200, 100);
			this->slot3->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot3->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot3->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot3->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// slot1
			// 
			this->slot1->Location = System::Drawing::Point(53, 30);
			this->slot1->Name = L"slot1";
			this->slot1->Size = System::Drawing::Size(200, 100);
			this->slot1->TabIndex = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->slot1->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->slot1->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->slot1->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Lime;
			// 
			// hudform
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(61)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->ClientSize = System::Drawing::Size(446, 460);
			this->Controls->Add(this->bp1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Name = L"hudform";
			this->Text = L"hudform";
			this->TopMost = true;
			this->TransparencyKey = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(61)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp1))->EndInit();
			this->bp1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot12))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot11))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot10))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot9))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot8))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot7))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot6))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot5))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	 System::Void autoupdatetimer_Tick(System::Object^  sender, System::EventArgs^  e) {
		auto dimension = Interface->get_client_dimension();
		auto position = Interface->get_client_placement();
		
		this->SetBounds(position.x, position.y, dimension.x, dimension.y);
		
		auto first = ContainerManager::get()->get_container_by_index(0);
		if (!first){
			first = ContainerManager::get()->get_browse_field();
			if (!first){
				bp1->Visible = false;
				return;
			}
			
		}
		auto rect = first->get_rect_client();
		if (rect.is_null())
		{
			bp1->Visible = false;
			return;
		}
		bp1->SetBounds(rect.get_x(), rect.get_y(), rect.get_width(), rect.get_height());
		bp1->Visible = true;

		for (int i = 0; i < 12; i++){
			auto slot_controls = this->Controls->Find("slot" + (i + 1), true);
			Telerik::WinControls::UI::RadPanel^ slot = (Telerik::WinControls::UI::RadPanel^)slot_controls[0];

			auto slot_rect = first->get_slot_rect_client(i);
			if (!slot_rect.is_null() && slot_rect.is_valid()){
				
				slot->Visible = true;
				slot->SetBounds(slot_rect.get_x() - bp1->Location.X,
					slot_rect.get_y() - bp1->Location.Y, slot_rect.get_width(), slot_rect.get_height());
			}
			else{
				slot->Visible = false;
			}
		}
		

	}
	};
}
