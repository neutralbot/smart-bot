#pragma once
#include <string>
#include "ManagedUtil.h"
#include "LuaBackgroundManager.h"
#include "KeywordManager.h"
#include "LanguageManager.h"
#include <direct.h>
#include <stdlib.h>
#include "GeneralManager.h"
#include <stdio.h>
#include "LuaStyleEditor.h"
#include "Keywords.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Collections::Generic;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Text;

	public ref class LuaBackground : public Telerik::WinControls::UI::RadForm {

	public: LuaBackground(String^ code){
		InitializeComponent();
		fastColoredTextBox1->Enabled = false;
		fastColoredTextBox1->Text = "";
		managed_util::setToggleCheckButtonStyle(radToggleButtonLua);
		this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
		loadInformations();
		if (code){
			loadCode(code);
			splitPanelScripts->Collapsed = true;
			fastColoredTextBox1->Enabled = true;
		}

		NeutralBot::FastUIPanelController::get()->install_controller(this);
	}

			static void CloseUnique(){
				if (unique)unique->Close();
			}

	protected: ~LuaBackground(){
		managed_util::unsetToggleCheckButtonStyle(radToggleButtonLua);
		unique = nullptr;
		if (components){
			delete components;
		}
	}

	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew LuaBackground("");
	}
	public: static void HideUnique(){
				if (unique)unique->Hide();
	}

			   uint64_t last_index = 0;
	protected:
		Telerik::WinControls::UI::RadContextMenu^  radContextMenu1;
		Telerik::WinControls::RootRadElement^  object_6f50bb71_9810_4af1_81ac_0d4c91058b71;
	private: System::ComponentModel::IContainer^  components;

	protected: System::Windows::Forms::Timer^  time_lua_log;
	public:
	public:
	public:	System::Windows::Forms::ImageList^  symbol_images;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanelOutput;
	public:
	protected: System::Windows::Forms::RichTextBox^  tx_lua_log;
	private:
	private: Telerik::WinControls::UI::SplitPanel^  radSplitCOntainer;
	protected:
	public: Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer1;
	private:
	private: Telerik::WinControls::UI::SplitPanel^  splitPanelScripts;
	public:
	private: Telerik::WinControls::UI::RadPageView^  radPageView1;
	private: Telerik::WinControls::UI::RadPageViewPage^  radPageViewPageHuds;
	protected: Telerik::WinControls::UI::RadTreeView^  radTreeView2;
	private:
	private: Telerik::WinControls::UI::RadPageViewPage^  radPageViewPage2;
	protected:
	protected: Telerik::WinControls::UI::RadTreeView^  radThreeViewHuds;
	private:
	private: Telerik::WinControls::UI::SplitPanel^  splitPanelTextEditor;
	protected:
	public: FastColoredTextBoxNS::FastColoredTextBox^  fastColoredTextBox1;
	private:
	private: Telerik::WinControls::UI::SplitPanel^  splitPanelExample;
	public:
	protected: Telerik::WinControls::UI::RadTreeView^  radTreeView1;
	private:
	private: Telerik::WinControls::UI::SplitPanel^  splitPanelLibInfo;
	protected:
	private: Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer3;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel1;
	public: Telerik::WinControls::UI::RadTreeView^  infoCodeThree;
	private:
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel2;
	public:
	private: System::Windows::Forms::RichTextBox^  richTextBox1;
	private: Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer2;
	private: Telerik::WinControls::UI::RadPanel^  radPanel1;
	public: Telerik::WinControls::UI::RadMenu^  radMenu1;
	private:
	private: Telerik::WinControls::UI::RadMenuItem^  menu_add;
	public:
	private: Telerik::WinControls::UI::RadMenuItem^  menu_remove;
	public: Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
	private:
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem4;
	public:
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem5;
	private: Telerik::WinControls::UI::RadMenuItem^  menu_clearlog;
	public: Telerik::WinControls::UI::RadMenuItem^  radMenuItem2;
	private: Telerik::WinControls::UI::RadPanel^  radPanel2;
	private: Telerik::WinControls::UI::RadPanel^  radPanel3;
	private: Telerik::WinControls::UI::RadPanel^  radPanel5;
	private: Telerik::WinControls::UI::RadPanel^  radPanel4;
	private: Telerik::WinControls::UI::RadDropDownList^  bt_search;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem3;
	private: Telerik::WinControls::UI::RadToggleButton^  radToggleButtonLua;
	protected:
		static LuaBackground^ mLuaBackground = nullptr;

	public: static LuaBackground^ get(String^ code) {
		if (!mLuaBackground)
			mLuaBackground = gcnew LuaBackground(code);

		return gcnew LuaBackground(code);
	}
							public: static void ReloadForm(){
										unique->LuaBackground_Load(nullptr, nullptr);
							}

#pragma region Windows Form Designer generated code
			void InitializeComponent(void){
				this->components = (gcnew System::ComponentModel::Container());
				System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(LuaBackground::typeid));
				Telerik::WinControls::UI::RadTreeNode^  radTreeNode1 = (gcnew Telerik::WinControls::UI::RadTreeNode());
				Telerik::WinControls::UI::RadTreeNode^  radTreeNode2 = (gcnew Telerik::WinControls::UI::RadTreeNode());
				this->radContextMenu1 = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
				this->symbol_images = (gcnew System::Windows::Forms::ImageList(this->components));
				this->object_6f50bb71_9810_4af1_81ac_0d4c91058b71 = (gcnew Telerik::WinControls::RootRadElement());
				this->time_lua_log = (gcnew System::Windows::Forms::Timer(this->components));
				this->splitPanelOutput = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->tx_lua_log = (gcnew System::Windows::Forms::RichTextBox());
				this->radSplitCOntainer = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->radSplitContainer1 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
				this->splitPanelScripts = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->radPageView1 = (gcnew Telerik::WinControls::UI::RadPageView());
				this->radPageViewPageHuds = (gcnew Telerik::WinControls::UI::RadPageViewPage());
				this->radTreeView2 = (gcnew Telerik::WinControls::UI::RadTreeView());
				this->radPageViewPage2 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
				this->radThreeViewHuds = (gcnew Telerik::WinControls::UI::RadTreeView());
				this->splitPanelTextEditor = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->fastColoredTextBox1 = (gcnew FastColoredTextBoxNS::FastColoredTextBox());
				this->splitPanelExample = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->radTreeView1 = (gcnew Telerik::WinControls::UI::RadTreeView());
				this->splitPanelLibInfo = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->radSplitContainer3 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
				this->splitPanel1 = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->radPanel5 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->infoCodeThree = (gcnew Telerik::WinControls::UI::RadTreeView());
				this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->bt_search = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->splitPanel2 = (gcnew Telerik::WinControls::UI::SplitPanel());
				this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
				this->radSplitContainer2 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
				this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				this->menu_add = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->menu_remove = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuItem4 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuItem5 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->menu_clearlog = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuItem3 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuItem2 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radToggleButtonLua = (gcnew Telerik::WinControls::UI::RadToggleButton());
				this->radPanel3 = (gcnew Telerik::WinControls::UI::RadPanel());
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelOutput))->BeginInit();
				this->splitPanelOutput->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitCOntainer))->BeginInit();
				this->radSplitCOntainer->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer1))->BeginInit();
				this->radSplitContainer1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelScripts))->BeginInit();
				this->splitPanelScripts->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->BeginInit();
				this->radPageView1->SuspendLayout();
				this->radPageViewPageHuds->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTreeView2))->BeginInit();
				this->radPageViewPage2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radThreeViewHuds))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelTextEditor))->BeginInit();
				this->splitPanelTextEditor->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fastColoredTextBox1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelExample))->BeginInit();
				this->splitPanelExample->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTreeView1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelLibInfo))->BeginInit();
				this->splitPanelLibInfo->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer3))->BeginInit();
				this->radSplitContainer3->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel1))->BeginInit();
				this->splitPanel1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->BeginInit();
				this->radPanel5->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->infoCodeThree))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
				this->radPanel4->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_search))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel2))->BeginInit();
				this->splitPanel2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer2))->BeginInit();
				this->radSplitContainer2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
				this->radPanel1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
				this->radPanel2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLua))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->BeginInit();
				this->radPanel3->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				this->SuspendLayout();
				// 
				// radContextMenu1
				// 
				this->radContextMenu1->DropDownOpening += gcnew System::ComponentModel::CancelEventHandler(this, &LuaBackground::radContextMenu1_DropDownOpening);
				// 
				// symbol_images
				// 
				this->symbol_images->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^>(resources->GetObject(L"symbol_images.ImageStream")));
				this->symbol_images->TransparentColor = System::Drawing::Color::Transparent;
				this->symbol_images->Images->SetKeyName(0, L"class_symbol");
				this->symbol_images->Images->SetKeyName(1, L"enum_t_symbol");
				this->symbol_images->Images->SetKeyName(2, L"function_symbol");
				this->symbol_images->Images->SetKeyName(3, L"method_symbol");
				this->symbol_images->Images->SetKeyName(4, L"lib_symbol");
				this->symbol_images->Images->SetKeyName(5, L"code_edit_12");
				this->symbol_images->Images->SetKeyName(6, L"code_edit_22");
				this->symbol_images->Images->SetKeyName(7, L"code_edit_3");
				this->symbol_images->Images->SetKeyName(8, L"lua_file2");
				this->symbol_images->Images->SetKeyName(9, L"folder2");
				this->symbol_images->Images->SetKeyName(10, L"folder_2");
				this->symbol_images->Images->SetKeyName(11, L"folder_3");
				this->symbol_images->Images->SetKeyName(12, L"timer2");
				this->symbol_images->Images->SetKeyName(13, L"timer_2");
				this->symbol_images->Images->SetKeyName(14, L"folder_3.png");
				this->symbol_images->Images->SetKeyName(15, L"script_group_1");
				this->symbol_images->Images->SetKeyName(16, L"script_group_2");
				this->symbol_images->Images->SetKeyName(17, L"script_group_3");
				this->symbol_images->Images->SetKeyName(18, L"script_group_4");
				this->symbol_images->Images->SetKeyName(19, L"folder");
				this->symbol_images->Images->SetKeyName(20, L"timer");
				this->symbol_images->Images->SetKeyName(21, L"code_edit_1");
				this->symbol_images->Images->SetKeyName(22, L"lua_file");
				this->symbol_images->Images->SetKeyName(23, L"keyword_png");
				// 
				// object_6f50bb71_9810_4af1_81ac_0d4c91058b71
				// 
				this->object_6f50bb71_9810_4af1_81ac_0d4c91058b71->Name = L"object_6f50bb71_9810_4af1_81ac_0d4c91058b71";
				this->object_6f50bb71_9810_4af1_81ac_0d4c91058b71->StretchHorizontally = true;
				this->object_6f50bb71_9810_4af1_81ac_0d4c91058b71->StretchVertically = true;
				// 
				// time_lua_log
				// 
				this->time_lua_log->Enabled = true;
				this->time_lua_log->Interval = 300;
				this->time_lua_log->Tick += gcnew System::EventHandler(this, &LuaBackground::time_lua_log_Tick);
				// 
				// splitPanelOutput
				// 
				this->splitPanelOutput->Controls->Add(this->tx_lua_log);
				this->splitPanelOutput->Location = System::Drawing::Point(0, 441);
				this->splitPanelOutput->Name = L"splitPanelOutput";
				// 
				// 
				// 
				this->splitPanelOutput->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->splitPanelOutput->Size = System::Drawing::Size(909, 162);
				this->splitPanelOutput->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.2277311F);
				this->splitPanelOutput->SizeInfo->SplitterCorrection = System::Drawing::Size(0, -176);
				this->splitPanelOutput->TabIndex = 3;
				this->splitPanelOutput->TabStop = false;
				// 
				// tx_lua_log
				// 
				this->tx_lua_log->BackColor = System::Drawing::Color::Black;
				this->tx_lua_log->Dock = System::Windows::Forms::DockStyle::Fill;
				this->tx_lua_log->Font = (gcnew System::Drawing::Font(L"Consolas", 8.25F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					static_cast<System::Byte>(0)));
				this->tx_lua_log->ForeColor = System::Drawing::Color::White;
				this->tx_lua_log->HideSelection = false;
				this->tx_lua_log->Location = System::Drawing::Point(0, 0);
				this->tx_lua_log->MaxLength = 5;
				this->tx_lua_log->Name = L"tx_lua_log";
				this->tx_lua_log->ReadOnly = true;
				this->tx_lua_log->Size = System::Drawing::Size(909, 162);
				this->tx_lua_log->TabIndex = 0;
				this->tx_lua_log->Text = L"";
				// 
				// radSplitCOntainer
				// 
				this->radSplitCOntainer->Controls->Add(this->radSplitContainer1);
				this->radSplitCOntainer->Location = System::Drawing::Point(0, 0);
				this->radSplitCOntainer->Name = L"radSplitCOntainer";
				// 
				// 
				// 
				this->radSplitCOntainer->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->radSplitCOntainer->Size = System::Drawing::Size(909, 433);
				this->radSplitCOntainer->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.2277311F);
				this->radSplitCOntainer->SizeInfo->SplitterCorrection = System::Drawing::Size(0, -9);
				this->radSplitCOntainer->TabIndex = 2;
				this->radSplitCOntainer->TabStop = false;
				// 
				// radSplitContainer1
				// 
				this->radSplitContainer1->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				this->radSplitContainer1->Controls->Add(this->splitPanelScripts);
				this->radSplitContainer1->Controls->Add(this->splitPanelTextEditor);
				this->radSplitContainer1->Controls->Add(this->splitPanelExample);
				this->radSplitContainer1->Controls->Add(this->splitPanelLibInfo);
				this->radSplitContainer1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radSplitContainer1->EnableCollapsing = true;
				this->radSplitContainer1->Location = System::Drawing::Point(0, 0);
				this->radSplitContainer1->Name = L"radSplitContainer1";
				// 
				// 
				// 
				this->radSplitContainer1->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->radSplitContainer1->Size = System::Drawing::Size(909, 433);
				this->radSplitContainer1->SplitterWidth = 8;
				this->radSplitContainer1->TabIndex = 1;
				this->radSplitContainer1->TabStop = false;
				this->radSplitContainer1->Text = L"radSplitContainer1";
				this->radSplitContainer1->UseSplitterButtons = true;
				this->radSplitContainer1->Click += gcnew System::EventHandler(this, &LuaBackground::radSplitContainer1_Click);
				// 
				// splitPanelScripts
				// 
				this->splitPanelScripts->Controls->Add(this->radPageView1);
				this->splitPanelScripts->Location = System::Drawing::Point(0, 0);
				this->splitPanelScripts->Name = L"splitPanelScripts";
				// 
				// 
				// 
				this->splitPanelScripts->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->splitPanelScripts->Size = System::Drawing::Size(186, 431);
				this->splitPanelScripts->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0.06993205F, 0);
				this->splitPanelScripts->SizeInfo->SplitterCorrection = System::Drawing::Size(-776, 0);
				this->splitPanelScripts->TabIndex = 0;
				this->splitPanelScripts->TabStop = false;
				// 
				// radPageView1
				// 
				this->radPageView1->Controls->Add(this->radPageViewPageHuds);
				this->radPageView1->Controls->Add(this->radPageViewPage2);
				this->radPageView1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPageView1->Location = System::Drawing::Point(0, 0);
				this->radPageView1->Name = L"radPageView1";
				this->radPageView1->SelectedPage = this->radPageViewPage2;
				this->radPageView1->Size = System::Drawing::Size(186, 431);
				this->radPageView1->TabIndex = 6;
				this->radPageView1->Text = L"radPageView1";
				this->radPageView1->SelectedPageChanged += gcnew System::EventHandler(this, &LuaBackground::radPageView1_SelectedPageChanged);
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
				// 
				// radPageViewPageHuds
				// 
				this->radPageViewPageHuds->Controls->Add(this->radTreeView2);
				this->radPageViewPageHuds->ItemSize = System::Drawing::SizeF(49, 28);
				this->radPageViewPageHuds->Location = System::Drawing::Point(10, 37);
				this->radPageViewPageHuds->Name = L"radPageViewPageHuds";
				this->radPageViewPageHuds->Size = System::Drawing::Size(155, 383);
				this->radPageViewPageHuds->Text = L"Scripts";
				// 
				// radTreeView2
				// 
				this->radTreeView2->AllowAdd = true;
				this->radTreeView2->AllowEdit = true;
				this->radTreeView2->AllowRemove = true;
				this->radTreeView2->BackColor = System::Drawing::Color::Gray;
				this->radTreeView2->Cursor = System::Windows::Forms::Cursors::Default;
				this->radTreeView2->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radTreeView2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
				this->radTreeView2->ForeColor = System::Drawing::Color::Black;
				this->radTreeView2->ImageList = this->symbol_images;
				this->radTreeView2->Location = System::Drawing::Point(0, 0);
				this->radTreeView2->Name = L"radTreeView2";
				radTreeNode1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radTreeNode1.Image")));
				radTreeNode1->ImageKey = L"script_group_1";
				radTreeNode1->Name = L"Scripts";
				radTreeNode1->Text = L"Scripts";
				this->radTreeView2->Nodes->AddRange(gcnew cli::array< Telerik::WinControls::UI::RadTreeNode^  >(1) { radTreeNode1 });
				this->radTreeView2->RadContextMenu = this->radContextMenu1;
				this->radTreeView2->RightToLeft = System::Windows::Forms::RightToLeft::No;
				this->radTreeView2->Size = System::Drawing::Size(155, 383);
				this->radTreeView2->SortOrder = System::Windows::Forms::SortOrder::Ascending;
				this->radTreeView2->SpacingBetweenNodes = -1;
				this->radTreeView2->TabIndex = 2;
				this->radTreeView2->Editing += gcnew Telerik::WinControls::UI::TreeNodeEditingEventHandler(this, &LuaBackground::radTreeView2_Editing);
				this->radTreeView2->ValueChanging += gcnew Telerik::WinControls::UI::TreeNodeValueChangingEventHandler(this, &LuaBackground::radTreeView2_ValueChanging);
				this->radTreeView2->NodeCheckedChanged += gcnew Telerik::WinControls::UI::TreeNodeCheckedEventHandler(this, &LuaBackground::radTreeView2_NodeCheckedChanged);
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->ShowExpandCollapse = true;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->ItemHeight = 20;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->ShowLines = false;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->ShowRootLines = true;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->LineColor = System::Drawing::Color::Gray;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->TreeIndent = 20;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->FullRowSelect = true;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->NodeSpacing = -1;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView2->GetChildAt(0)))->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)),
					static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)));
				// 
				// radPageViewPage2
				// 
				this->radPageViewPage2->Controls->Add(this->radThreeViewHuds);
				this->radPageViewPage2->ItemSize = System::Drawing::SizeF(42, 28);
				this->radPageViewPage2->Location = System::Drawing::Point(10, 37);
				this->radPageViewPage2->Name = L"radPageViewPage2";
				this->radPageViewPage2->Size = System::Drawing::Size(165, 383);
				this->radPageViewPage2->Text = L"Huds";
				// 
				// radThreeViewHuds
				// 
				this->radThreeViewHuds->AllowAdd = true;
				this->radThreeViewHuds->AllowEdit = true;
				this->radThreeViewHuds->AllowRemove = true;
				this->radThreeViewHuds->BackColor = System::Drawing::Color::Gray;
				this->radThreeViewHuds->Cursor = System::Windows::Forms::Cursors::Default;
				this->radThreeViewHuds->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radThreeViewHuds->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
				this->radThreeViewHuds->ForeColor = System::Drawing::Color::Black;
				this->radThreeViewHuds->ImageList = this->symbol_images;
				this->radThreeViewHuds->Location = System::Drawing::Point(0, 0);
				this->radThreeViewHuds->Name = L"radThreeViewHuds";
				radTreeNode2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radTreeNode2.Image")));
				radTreeNode2->ImageKey = L"script_group_1";
				radTreeNode2->Name = L"Huds";
				radTreeNode2->Text = L"Huds";
				this->radThreeViewHuds->Nodes->AddRange(gcnew cli::array< Telerik::WinControls::UI::RadTreeNode^  >(1) { radTreeNode2 });
				this->radThreeViewHuds->RadContextMenu = this->radContextMenu1;
				this->radThreeViewHuds->RightToLeft = System::Windows::Forms::RightToLeft::No;
				this->radThreeViewHuds->Size = System::Drawing::Size(165, 383);
				this->radThreeViewHuds->SortOrder = System::Windows::Forms::SortOrder::Ascending;
				this->radThreeViewHuds->SpacingBetweenNodes = -1;
				this->radThreeViewHuds->TabIndex = 3;
				this->radThreeViewHuds->Editing += gcnew Telerik::WinControls::UI::TreeNodeEditingEventHandler(this, &LuaBackground::radTreeView2_Editing);
				this->radThreeViewHuds->ValueChanging += gcnew Telerik::WinControls::UI::TreeNodeValueChangingEventHandler(this, &LuaBackground::radTreeView2_ValueChanging);
				this->radThreeViewHuds->NodeCheckedChanged += gcnew Telerik::WinControls::UI::TreeNodeCheckedEventHandler(this, &LuaBackground::radTreeView2_NodeCheckedChanged);
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->ShowExpandCollapse = true;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->ItemHeight = 20;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->ShowLines = false;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->ShowRootLines = true;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->LineColor = System::Drawing::Color::Gray;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->TreeIndent = 20;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->FullRowSelect = true;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->NodeSpacing = -1;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radThreeViewHuds->GetChildAt(0)))->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)),
					static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)));
				// 
				// splitPanelTextEditor
				// 
				this->splitPanelTextEditor->Controls->Add(this->fastColoredTextBox1);
				this->splitPanelTextEditor->Location = System::Drawing::Point(194, 0);
				this->splitPanelTextEditor->Name = L"splitPanelTextEditor";
				// 
				// 
				// 
				this->splitPanelTextEditor->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->splitPanelTextEditor->Size = System::Drawing::Size(391, 431);
				this->splitPanelTextEditor->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0.1928086F, 0);
				this->splitPanelTextEditor->SizeInfo->SplitterCorrection = System::Drawing::Size(489, 0);
				this->splitPanelTextEditor->TabIndex = 1;
				this->splitPanelTextEditor->TabStop = false;
				// 
				// fastColoredTextBox1
				// 
				this->fastColoredTextBox1->AutoCompleteBracketsList = gcnew cli::array< System::Char >(10) {
					'(', ')', '{', '}', '[', ']', '\"',
						'\"', '\'', '\''
				};
				this->fastColoredTextBox1->AutoIndentChars = false;
				this->fastColoredTextBox1->AutoIndentCharsPatterns = L"\r\n^\\s*[\\w\\.]+(\\s\\w+)\?\\s*(\?<range>=)\\s*(\?<range>.+)\r\n";
				this->fastColoredTextBox1->AutoScrollMinSize = System::Drawing::Size(27, 14);
				this->fastColoredTextBox1->BackBrush = nullptr;
				this->fastColoredTextBox1->BracketsHighlightStrategy = FastColoredTextBoxNS::BracketsHighlightStrategy::Strategy2;
				this->fastColoredTextBox1->CharHeight = 14;
				this->fastColoredTextBox1->CharWidth = 8;
				this->fastColoredTextBox1->CommentPrefix = L"--";
				this->fastColoredTextBox1->Cursor = System::Windows::Forms::Cursors::IBeam;
				this->fastColoredTextBox1->DisabledColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(100)),
					static_cast<System::Int32>(static_cast<System::Byte>(180)), static_cast<System::Int32>(static_cast<System::Byte>(180)), static_cast<System::Int32>(static_cast<System::Byte>(180)));
				this->fastColoredTextBox1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->fastColoredTextBox1->Font = (gcnew System::Drawing::Font(L"Courier New", 9.75F));
				this->fastColoredTextBox1->IsReplaceMode = false;
				this->fastColoredTextBox1->Language = FastColoredTextBoxNS::Language::Lua;
				this->fastColoredTextBox1->LeftBracket = '(';
				this->fastColoredTextBox1->LeftBracket2 = '{';
				this->fastColoredTextBox1->Location = System::Drawing::Point(0, 0);
				this->fastColoredTextBox1->Name = L"fastColoredTextBox1";
				this->fastColoredTextBox1->Paddings = System::Windows::Forms::Padding(0);
				this->fastColoredTextBox1->RightBracket = ')';
				this->fastColoredTextBox1->RightBracket2 = '}';
				this->fastColoredTextBox1->SelectionColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(60)),
					static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(255)));
				this->fastColoredTextBox1->ServiceColors = (cli::safe_cast<FastColoredTextBoxNS::ServiceColors^>(resources->GetObject(L"fastColoredTextBox1.ServiceColors")));
				this->fastColoredTextBox1->Size = System::Drawing::Size(391, 431);
				this->fastColoredTextBox1->TabIndex = 5;
				this->fastColoredTextBox1->Zoom = 100;
				this->fastColoredTextBox1->TextChanged += gcnew System::EventHandler<FastColoredTextBoxNS::TextChangedEventArgs^ >(this, &LuaBackground::fastColoredTextBox1_TextChanged);
				this->fastColoredTextBox1->TextChangedDelayed += gcnew System::EventHandler<FastColoredTextBoxNS::TextChangedEventArgs^ >(this, &LuaBackground::fastColoredTextBox1_TextChangedDelayed_1);
				this->fastColoredTextBox1->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &LuaBackground::fastColoredTextBox1_KeyUp_1);
				// 
				// splitPanelExample
				// 
				this->splitPanelExample->Controls->Add(this->radTreeView1);
				this->splitPanelExample->Location = System::Drawing::Point(593, 0);
				this->splitPanelExample->Name = L"splitPanelExample";
				// 
				// 
				// 
				this->splitPanelExample->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->splitPanelExample->Size = System::Drawing::Size(306, 431);
				this->splitPanelExample->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0.09597962F, 0);
				this->splitPanelExample->SizeInfo->SplitterCorrection = System::Drawing::Size(-3556, 0);
				this->splitPanelExample->TabIndex = 2;
				this->splitPanelExample->TabStop = false;
				// 
				// radTreeView1
				// 
				this->radTreeView1->AllowAdd = true;
				this->radTreeView1->AllowRemove = true;
				this->radTreeView1->BackColor = System::Drawing::SystemColors::Control;
				this->radTreeView1->Cursor = System::Windows::Forms::Cursors::Default;
				this->radTreeView1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radTreeView1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
				this->radTreeView1->ForeColor = System::Drawing::Color::Black;
				this->radTreeView1->ImageList = this->symbol_images;
				this->radTreeView1->Location = System::Drawing::Point(0, 0);
				this->radTreeView1->Name = L"radTreeView1";
				this->radTreeView1->RightToLeft = System::Windows::Forms::RightToLeft::No;
				this->radTreeView1->Size = System::Drawing::Size(306, 431);
				this->radTreeView1->SortOrder = System::Windows::Forms::SortOrder::Ascending;
				this->radTreeView1->SpacingBetweenNodes = -1;
				this->radTreeView1->TabIndex = 1;
				this->radTreeView1->DoubleClick += gcnew System::EventHandler(this, &LuaBackground::radTreeView1_DoubleClick);
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView1->GetChildAt(0)))->NodeSpacing = -1;
				(cli::safe_cast<Telerik::WinControls::UI::RadTreeViewElement^>(this->radTreeView1->GetChildAt(0)))->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)),
					static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)));
				// 
				// splitPanelLibInfo
				// 
				this->splitPanelLibInfo->Controls->Add(this->radSplitContainer3);
				this->splitPanelLibInfo->Location = System::Drawing::Point(907, 0);
				this->splitPanelLibInfo->Name = L"splitPanelLibInfo";
				// 
				// 
				// 
				this->splitPanelLibInfo->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->splitPanelLibInfo->Size = System::Drawing::Size(0, 431);
				this->splitPanelLibInfo->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0.02689694F, 0);
				this->splitPanelLibInfo->SizeInfo->SplitterCorrection = System::Drawing::Size(3843, 0);
				this->splitPanelLibInfo->TabIndex = 3;
				this->splitPanelLibInfo->TabStop = false;
				// 
				// radSplitContainer3
				// 
				this->radSplitContainer3->Controls->Add(this->splitPanel1);
				this->radSplitContainer3->Controls->Add(this->splitPanel2);
				this->radSplitContainer3->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radSplitContainer3->Location = System::Drawing::Point(0, 0);
				this->radSplitContainer3->Name = L"radSplitContainer3";
				this->radSplitContainer3->Orientation = System::Windows::Forms::Orientation::Horizontal;
				// 
				// 
				// 
				this->radSplitContainer3->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->radSplitContainer3->Size = System::Drawing::Size(0, 431);
				this->radSplitContainer3->TabIndex = 6;
				this->radSplitContainer3->TabStop = false;
				this->radSplitContainer3->Text = L"radSplitContainer3";
				// 
				// splitPanel1
				// 
				this->splitPanel1->Controls->Add(this->radPanel5);
				this->splitPanel1->Controls->Add(this->radPanel4);
				this->splitPanel1->Location = System::Drawing::Point(0, 0);
				this->splitPanel1->Name = L"splitPanel1";
				// 
				// 
				// 
				this->splitPanel1->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->splitPanel1->Size = System::Drawing::Size(0, 287);
				this->splitPanel1->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.1711409F);
				this->splitPanel1->SizeInfo->SplitterCorrection = System::Drawing::Size(0, 52);
				this->splitPanel1->TabIndex = 0;
				this->splitPanel1->TabStop = false;
				// 
				// radPanel5
				// 
				this->radPanel5->Controls->Add(this->infoCodeThree);
				this->radPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPanel5->Location = System::Drawing::Point(0, 20);
				this->radPanel5->Name = L"radPanel5";
				this->radPanel5->Size = System::Drawing::Size(0, 267);
				this->radPanel5->TabIndex = 4;
				this->radPanel5->Text = L"radPanel5";
				// 
				// infoCodeThree
				// 
				this->infoCodeThree->BackColor = System::Drawing::SystemColors::Control;
				this->infoCodeThree->Cursor = System::Windows::Forms::Cursors::Default;
				this->infoCodeThree->Dock = System::Windows::Forms::DockStyle::Fill;
				this->infoCodeThree->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
				this->infoCodeThree->ForeColor = System::Drawing::Color::Black;
				this->infoCodeThree->ImageList = this->symbol_images;
				this->infoCodeThree->Location = System::Drawing::Point(0, 0);
				this->infoCodeThree->Name = L"infoCodeThree";
				this->infoCodeThree->RightToLeft = System::Windows::Forms::RightToLeft::No;
				this->infoCodeThree->ShowLines = true;
				this->infoCodeThree->ShowNodeToolTips = true;
				this->infoCodeThree->Size = System::Drawing::Size(0, 267);
				this->infoCodeThree->SortOrder = System::Windows::Forms::SortOrder::Ascending;
				this->infoCodeThree->SpacingBetweenNodes = -1;
				this->infoCodeThree->TabIndex = 2;
				this->infoCodeThree->Text = L"radTreeView1";
				this->infoCodeThree->DoubleClick += gcnew System::EventHandler(this, &LuaBackground::infoCodeThree_DoubleClick);
				// 
				// radPanel4
				// 
				this->radPanel4->Controls->Add(this->bt_search);
				this->radPanel4->Dock = System::Windows::Forms::DockStyle::Top;
				this->radPanel4->Location = System::Drawing::Point(0, 0);
				this->radPanel4->Name = L"radPanel4";
				this->radPanel4->Size = System::Drawing::Size(0, 20);
				this->radPanel4->TabIndex = 3;
				this->radPanel4->Text = L"radPanel4";
				// 
				// bt_search
				// 
				this->bt_search->Dock = System::Windows::Forms::DockStyle::Fill;
				this->bt_search->Location = System::Drawing::Point(0, 0);
				this->bt_search->Name = L"bt_search";
				this->bt_search->Size = System::Drawing::Size(0, 20);
				this->bt_search->TabIndex = 3;
				this->bt_search->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &LuaBackground::bt_search_SelectedIndexChanged);
				this->bt_search->TextChanged += gcnew System::EventHandler(this, &LuaBackground::bt_search_TextChanged);
				// 
				// splitPanel2
				// 
				this->splitPanel2->Controls->Add(this->richTextBox1);
				this->splitPanel2->Location = System::Drawing::Point(0, 291);
				this->splitPanel2->Name = L"splitPanel2";
				// 
				// 
				// 
				this->splitPanel2->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->splitPanel2->Size = System::Drawing::Size(0, 140);
				this->splitPanel2->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.1711409F);
				this->splitPanel2->SizeInfo->SplitterCorrection = System::Drawing::Size(0, -52);
				this->splitPanel2->TabIndex = 1;
				this->splitPanel2->TabStop = false;
				// 
				// richTextBox1
				// 
				this->richTextBox1->BackColor = System::Drawing::Color::DimGray;
				this->richTextBox1->BorderStyle = System::Windows::Forms::BorderStyle::None;
				this->richTextBox1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->richTextBox1->Enabled = false;
				this->richTextBox1->Font = (gcnew System::Drawing::Font(L"Courier New", 9.75F));
				this->richTextBox1->Location = System::Drawing::Point(0, 0);
				this->richTextBox1->Name = L"richTextBox1";
				this->richTextBox1->Size = System::Drawing::Size(0, 140);
				this->richTextBox1->TabIndex = 0;
				this->richTextBox1->Text = L"";
				// 
				// radSplitContainer2
				// 
				this->radSplitContainer2->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				this->radSplitContainer2->Controls->Add(this->radSplitCOntainer);
				this->radSplitContainer2->Controls->Add(this->splitPanelOutput);
				this->radSplitContainer2->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radSplitContainer2->EnableCollapsing = true;
				this->radSplitContainer2->Location = System::Drawing::Point(0, 0);
				this->radSplitContainer2->Name = L"radSplitContainer2";
				this->radSplitContainer2->Orientation = System::Windows::Forms::Orientation::Horizontal;
				// 
				// 
				// 
				this->radSplitContainer2->RootElement->MinSize = System::Drawing::Size(0, 0);
				this->radSplitContainer2->Size = System::Drawing::Size(911, 605);
				this->radSplitContainer2->SplitterWidth = 8;
				this->radSplitContainer2->TabIndex = 6;
				this->radSplitContainer2->TabStop = false;
				this->radSplitContainer2->Text = L"radSplitContainer2";
				// 
				// radPanel1
				// 
				this->radPanel1->BackColor = System::Drawing::Color::Transparent;
				this->radPanel1->Controls->Add(this->radMenu1);
				this->radPanel1->Location = System::Drawing::Point(129, 0);
				this->radPanel1->Name = L"radPanel1";
				this->radPanel1->Size = System::Drawing::Size(1491, 35);
				this->radPanel1->TabIndex = 9;
				this->radPanel1->Text = L"radPanel1";
				(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->radPanel1->GetChildAt(0)))->Text = L"radPanel1";
				(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->radPanel1->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
				// 
				// radMenu1
				// 
				this->radMenu1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(8) {
					this->menu_add, this->menu_remove,
						this->radMenuItem1, this->radMenuItem4, this->radMenuItem5, this->menu_clearlog, this->radMenuItem3, this->radMenuItem2
				});
				this->radMenu1->Location = System::Drawing::Point(0, 0);
				this->radMenu1->Name = L"radMenu1";
				this->radMenu1->Size = System::Drawing::Size(1491, 36);
				this->radMenu1->TabIndex = 9;
				this->radMenu1->Text = L"radMenu1";
				// 
				// menu_add
				// 
				this->menu_add->AccessibleDescription = L"Add";
				this->menu_add->AccessibleName = L"Add";
				this->menu_add->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"menu_add.Image")));
				this->menu_add->Name = L"menu_add";
				this->menu_add->Text = L"Add";
				this->menu_add->Click += gcnew System::EventHandler(this, &LuaBackground::menu_add_Click);
				// 
				// menu_remove
				// 
				this->menu_remove->AccessibleDescription = L"Remove";
				this->menu_remove->AccessibleName = L"Remove";
				this->menu_remove->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"menu_remove.Image")));
				this->menu_remove->Name = L"menu_remove";
				this->menu_remove->Text = L"Remove";
				this->menu_remove->Click += gcnew System::EventHandler(this, &LuaBackground::menu_remove_Click);
				// 
				// radMenuItem1
				// 
				this->radMenuItem1->AccessibleDescription = L"Execute";
				this->radMenuItem1->AccessibleName = L"Execute";
				this->radMenuItem1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radMenuItem1.Image")));
				this->radMenuItem1->Name = L"radMenuItem1";
				this->radMenuItem1->Text = L"Execute";
				this->radMenuItem1->Click += gcnew System::EventHandler(this, &LuaBackground::radMenuItem1_Click_1);
				// 
				// radMenuItem4
				// 
				this->radMenuItem4->AccessibleDescription = L"Import Script";
				this->radMenuItem4->AccessibleName = L"Import Script";
				this->radMenuItem4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radMenuItem4.Image")));
				this->radMenuItem4->Name = L"radMenuItem4";
				this->radMenuItem4->Text = L"Import Script";
				this->radMenuItem4->Click += gcnew System::EventHandler(this, &LuaBackground::radMenuItem4_Click);
				// 
				// radMenuItem5
				// 
				this->radMenuItem5->AccessibleDescription = L"Export Script";
				this->radMenuItem5->AccessibleName = L"Export Script";
				this->radMenuItem5->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radMenuItem5.Image")));
				this->radMenuItem5->Name = L"radMenuItem5";
				this->radMenuItem5->Text = L"Export Script";
				this->radMenuItem5->Click += gcnew System::EventHandler(this, &LuaBackground::radMenuItem5_Click);
				// 
				// menu_clearlog
				// 
				this->menu_clearlog->AccessibleDescription = L"Clear Log";
				this->menu_clearlog->AccessibleName = L"Clear Log";
				this->menu_clearlog->Alignment = System::Drawing::ContentAlignment::TopLeft;
				this->menu_clearlog->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"menu_clearlog.Image")));
				this->menu_clearlog->ImageAlignment = System::Drawing::ContentAlignment::TopLeft;
				this->menu_clearlog->Name = L"menu_clearlog";
				this->menu_clearlog->Text = L"Clear Log";
				this->menu_clearlog->Click += gcnew System::EventHandler(this, &LuaBackground::menu_clearlog_Click);
				// 
				// radMenuItem3
				// 
				this->radMenuItem3->AccessibleDescription = L"Keywords";
				this->radMenuItem3->AccessibleName = L"Keywords";
				this->radMenuItem3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radMenuItem3.Image")));
				this->radMenuItem3->Name = L"radMenuItem3";
				this->radMenuItem3->Text = L"Keywords";
				this->radMenuItem3->Click += gcnew System::EventHandler(this, &LuaBackground::radMenuItem3_Click_1);
				// 
				// radMenuItem2
				// 
				this->radMenuItem2->AccessibleDescription = L"Style";
				this->radMenuItem2->AccessibleName = L"Style";
				this->radMenuItem2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radMenuItem2.Image")));
				this->radMenuItem2->Name = L"radMenuItem2";
				this->radMenuItem2->Text = L"Style";
				this->radMenuItem2->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
				// 
				// radPanel2
				// 
				this->radPanel2->Controls->Add(this->radToggleButtonLua);
				this->radPanel2->Controls->Add(this->radPanel1);
				this->radPanel2->Dock = System::Windows::Forms::DockStyle::Top;
				this->radPanel2->Location = System::Drawing::Point(0, 0);
				this->radPanel2->Name = L"radPanel2";
				this->radPanel2->Size = System::Drawing::Size(911, 36);
				this->radPanel2->TabIndex = 10;
				// 
				// radToggleButtonLua
				// 
				this->radToggleButtonLua->Location = System::Drawing::Point(1, 5);
				this->radToggleButtonLua->Name = L"radToggleButtonLua";
				this->radToggleButtonLua->Size = System::Drawing::Size(129, 28);
				this->radToggleButtonLua->TabIndex = 0;
				this->radToggleButtonLua->Text = L"radToggleButton1";
				// 
				// radPanel3
				// 
				this->radPanel3->Controls->Add(this->radSplitContainer2);
				this->radPanel3->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPanel3->Location = System::Drawing::Point(0, 36);
				this->radPanel3->Name = L"radPanel3";
				this->radPanel3->Size = System::Drawing::Size(911, 605);
				this->radPanel3->TabIndex = 11;
				this->radPanel3->Text = L"radPanel3";
				// 
				// LuaBackground
				// 
				this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				this->ClientSize = System::Drawing::Size(911, 641);
				this->Controls->Add(this->radPanel3);
				this->Controls->Add(this->radPanel2);
				this->Name = L"LuaBackground";
				// 
				// 
				// 
				this->RootElement->ApplyShapeToControl = true;
				this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				this->Text = L"Lua";
				this->ThemeName = L"ControlDefault";
				this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &LuaBackground::LuaBackground_FormClosing);
				this->Load += gcnew System::EventHandler(this, &LuaBackground::LuaBackground_Load);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelOutput))->EndInit();
				this->splitPanelOutput->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitCOntainer))->EndInit();
				this->radSplitCOntainer->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer1))->EndInit();
				this->radSplitContainer1->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelScripts))->EndInit();
				this->splitPanelScripts->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->EndInit();
				this->radPageView1->ResumeLayout(false);
				this->radPageViewPageHuds->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTreeView2))->EndInit();
				this->radPageViewPage2->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radThreeViewHuds))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelTextEditor))->EndInit();
				this->splitPanelTextEditor->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->fastColoredTextBox1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelExample))->EndInit();
				this->splitPanelExample->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTreeView1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanelLibInfo))->EndInit();
				this->splitPanelLibInfo->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer3))->EndInit();
				this->radSplitContainer3->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel1))->EndInit();
				this->splitPanel1->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->EndInit();
				this->radPanel5->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->infoCodeThree))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
				this->radPanel4->ResumeLayout(false);
				this->radPanel4->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_search))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel2))->EndInit();
				this->splitPanel2->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer2))->EndInit();
				this->radSplitContainer2->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
				this->radPanel1->ResumeLayout(false);
				this->radPanel1->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
				this->radPanel2->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLua))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->EndInit();
				this->radPanel3->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				this->ResumeLayout(false);

			}
#pragma endregion

			ref class MethodAutocompleteItem2 : FastColoredTextBoxNS::MethodAutocompleteItem{
			public:
				String^ firstPart;
				String^ lastPart;

				MethodAutocompleteItem2(String^ text) : FastColoredTextBoxNS::MethodAutocompleteItem(text){
					auto i = text->LastIndexOf('.');
					if (i < 0)
						firstPart = text;
					else{
						firstPart = text->Substring(0, i);
						lastPart = text->Substring(i + 1);
					}
				}

				virtual FastColoredTextBoxNS::CompareResult Compare(String^ fragmentText)override{
					int i = fragmentText->LastIndexOf('.');

					if (i < 0){
						if (firstPart->StartsWith(fragmentText) && String::IsNullOrEmpty(lastPart))
							return FastColoredTextBoxNS::CompareResult::VisibleAndSelected;
						if (firstPart->ToLower()->Contains(fragmentText->ToLower()) && String::IsNullOrEmpty(lastPart))
							return FastColoredTextBoxNS::CompareResult::Visible;
					}
					else{
						auto fragmentFirstPart = fragmentText->Substring(0, i);
						auto fragmentLastPart = fragmentText->Substring(i + 1);


						if (firstPart != fragmentFirstPart)
							return  FastColoredTextBoxNS::CompareResult::Hidden;

						if (lastPart != nullptr && lastPart->StartsWith(fragmentLastPart))
							return  FastColoredTextBoxNS::CompareResult::VisibleAndSelected;

						if (lastPart != nullptr && lastPart->ToLower()->Contains(fragmentLastPart->ToLower()))
							return  FastColoredTextBoxNS::CompareResult::Visible;

					}
					return  FastColoredTextBoxNS::CompareResult::Hidden;
				}

				virtual String^ GetTextForReplace()override{
					if (lastPart == nullptr)
						return firstPart;

					return firstPart + "." + lastPart;
				}

				virtual String^ ToString()override{
					if (lastPart == nullptr)
						return firstPart;

					return lastPart;
				}
			};

			FastColoredTextBoxNS::Style^ FunctionNameStyle = gcnew FastColoredTextBoxNS::TextStyle(System::Drawing::Brushes::Magenta, nullptr, System::Drawing::FontStyle::Regular);

			Telerik::WinControls::UI::RadMenuItem^ ItemNew;
			Telerik::WinControls::UI::RadMenuItem^ ItemDelete;
			Telerik::WinControls::UI::RadMenuItem^ ItemExecute;

			static LuaBackground^ unique;
	public: static String^ ShowUnique(String^ code);

			String^ temp_strMet = "(firts";
			String^ nameClass = "(firts";
			String^ antigoNome;
			bool Waypoint;
			String^ loadId;

			void loadDirectoriesAndFiles(DirectoryInfo^ dir, Telerik::WinControls::UI::RadTreeNode^ parentNode);
			void creatContextMenu();
			void addEvents();
			void loadFilesLua();
			void ReloadLuaFiles();
			void loadCodeinFiles(Telerik::WinControls::UI::RadTreeViewEventArgs^  e);
			void loadItemOnView();
			void addInAutoComplete();

			System::Void LuaBackground_Load(System::Object^  sender, System::EventArgs^  e);

			System::Void MenuNew_Click(Object^ sender, EventArgs^ e);
			System::Void MenuDelete_Click(Object^ sender, EventArgs^ e);
			System::Void MenuExecute_Click(Object^ sender, EventArgs^ e);
			bool is_hud_tab_selected();
			Telerik::WinControls::UI::RadTreeView^ get_current_three_view();

			void loadStyle();

			System::Void fastColoredTextBox1_TextChanged(System::Object^  sender, FastColoredTextBoxNS::TextChangedEventArgs^  e);

			System::Void radTreeView2_ValueChanging(System::Object^  sender, Telerik::WinControls::UI::TreeNodeValueChangingEventArgs^  e);
			System::Void radTreeView2_Editing(System::Object^  sender, Telerik::WinControls::UI::TreeNodeEditingEventArgs^  e);
			System::Void radTreeView2_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);
			System::Void radTreeView2_NodeRemoving_1(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewCancelEventArgs^  e);
			System::Void radTreeView2_NodeCheckedChanged(System::Object^  sender, Telerik::WinControls::UI::TreeNodeCheckedEventArgs^  e);

			System::Void radTreeView1_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);

			void loadCode(String^ idCode);

			System::Void fastColoredTextBox1_TextChangedDelayed_1(System::Object^  sender, FastColoredTextBoxNS::TextChangedEventArgs^  e);
			System::Void fastColoredTextBox1_KeyUp_1(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e);
			System::Void radTreeView2_NodeMouseDoubleClick_1(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);
			int lastAdd = 0;
			System::Void time_lua_log_Tick(System::Object^  sender, System::EventArgs^  e);

	public:


		void hide_left_pane();

		void hide_bottom_pane();

		void hide_right_pane();

		void hide_libs_info();

		System::Void radSplitContainer1_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void radMenuItem1_Click(System::Object^  sender, System::EventArgs^  e);

		Telerik::WinControls::UI::RadTreeNode^ get_node(lua_table_t type, cli::array<String^>^ paths);

		String^ get_lib_description(String^ libname);

		String^ get_table_description(String^ libname, String^ classname);

		String^ get_method_description(String^ libname, String^ classname, String^ methodname);

		String^ get_table_variable_description(String^ libname, String^ tablename, String^ varname);

		String^ get_function_description(String^ libname, String^ functionname);

		void update_current_description();

		void set_description(String^ type, String^ description);

		Telerik::WinControls::UI::RadTreeNode^ get_class_node(String^ libname, String^ classname, lua_table_t type);

		Telerik::WinControls::UI::RadTreeNode^ get_class_method_node(String^ libname, String^ classname, String^ method);

		Telerik::WinControls::UI::RadTreeNode^ get_function_node(String^ libname, String^ functionname);

		Telerik::WinControls::UI::RadTreeNode^ get_lib_node(String^ libname);

		void add_class_method(String^ libname, String^ classname, String^ method);

		void add_class_method_suggestion(String^ libname, String^ classname, String^ method, String^ suggestion);

		String^ getLuaTableNodeAsString(lua_table_t table_type);

		Telerik::WinControls::UI::RadTreeNode^ add_class(String^ libname, String^ classname, lua_table_t lua_table_type);

		void add_table_variable(String^ libname, String^ table_name, String^ variable_name);

		void add_function_suggestion(String^ libname, String^ function, String^ suggestion);

		void add_function(String^ libname, String^ function);

		void add_lib(String^ libname);

		void loadInformations();

		System::Void infoCodeThree_SelectedNodeChanged_1(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);

		System::Void infoCodeThree_DoubleClick(System::Object^  sender, System::EventArgs^  e);

		System::Void radMenuItem3_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void radMenuItem1_Click_1(System::Object^  sender, System::EventArgs^  e);

	private: System::Void infoCodeThree_SelectedNodesChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {

		if (!e->Node){
			richTextBox1->Text = "";
			return;
		}
		auto selected = e->Node;
		Telerik::WinControls::UI::RadTreeNode^ root = selected;
		while (root->Parent){
			root = root->Parent;
		}

		String^ desc;
		if (selected->Name){
			Console::WriteLine(selected->Name);
		}

		if (selected->Name == "library"){
			desc = get_lib_description(selected->Text);
		}
		else if (selected->Name == "function"){
			desc = get_function_description(root->Text, selected->Text);
		}
		else if (selected->Name == "function_suggestion"){
			desc = get_function_description(root->Parent->Text, selected->Parent->Text);
		}
		else if (selected->Name == "class"){
			desc = get_table_description(root->Text, selected->Text);
		}
		else if (selected->Name == "method_suggestion"){
			desc = get_method_description(root->Text, selected->Parent->Parent->Text, selected->Parent->Text);
		}
		else if (selected->Name == "class_method"){
			desc = get_method_description(root->Text, selected->Parent->Text, selected->Text);
		}
		else if (selected->Name == "table_variable"){
			desc = get_table_variable_description(root->Text, selected->Parent->Text, selected->Text);
		}
		else if (selected->Name == "variable"){
			desc = get_method_description(selected->Parent->Parent->Text, selected->Parent->Text, selected->Text);
		}
		richTextBox1->Text = desc;
	}

	private: System::Void radContextMenu1_DropDownOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
		if (radTreeView2->TopNode == radTreeView2->SelectedNode){
			radContextMenu1->Items->Clear();
			radContextMenu1->Items->Add(ItemNew);
		}
		else{
			radContextMenu1->Items->Clear();
			radContextMenu1->Items->Add(ItemNew);
			radContextMenu1->Items->Add(ItemDelete);
			radContextMenu1->Items->Add(ItemExecute);
		}
	}

	private: System::Void radMenuItem4_Click(System::Object^  sender, System::EventArgs^  e) {
		System::Windows::Forms::OpenFileDialog fileDialog;
		fileDialog.Filter = "Lua Script|*.lua";
		fileDialog.Title = "Import Lua Script";
		fileDialog.InitialDirectory = Directory::GetCurrentDirectory() + "\\lua\\scripts\\";

		System::Windows::Forms::DialogResult result = fileDialog.ShowDialog();

		if (result == System::Windows::Forms::DialogResult::Cancel)
			return;

		String^ path = fileDialog.FileName;

		try{
			MenuNew_Click(nullptr, nullptr);
			fastColoredTextBox1->Text = File::ReadAllText(path);
		}
		catch (...){
			Telerik::WinControls::RadMessageBox::Show(this, "HANDLE ERROR EXPORT SCRIPT", "ERROR");
		}

		Telerik::WinControls::RadMessageBox::Show(this, "HANDLE SUCCESS EXPORT SCRIPT", "ERROR");

	}

	private: System::Void radMenuItem5_Click(System::Object^  sender, System::EventArgs^  e) {


		System::Windows::Forms::SaveFileDialog fileDialog;
		fileDialog.Filter = "Lua Script|*.lua";
		fileDialog.Title = "Export Lua Script";
		fileDialog.InitialDirectory = Directory::GetCurrentDirectory() + "\\lua\\scripts\\";

		System::Windows::Forms::DialogResult result = fileDialog.ShowDialog();

		if (result == System::Windows::Forms::DialogResult::Cancel)
			return;

		String^ path = fileDialog.FileName;
		try{
			File::WriteAllText(path, fastColoredTextBox1->Text);
		}
		catch (...){
			Telerik::WinControls::RadMessageBox::Show(this, "HANDLE ERROR EXPORT SCRIPT", "ERROR");
		}
		ReloadLuaFiles();
		Telerik::WinControls::RadMessageBox::Show(this, "HANDLE SUCCESS EXPORT SCRIPT", "ERROR");
	}

	private: System::Void radTreeView1_DoubleClick(System::Object^  sender, System::EventArgs^  e) {
		if (!radTreeView1->SelectedNode)
			return;

		if (radTreeView1->SelectedNode == radTreeView1->TopNode)
			return;

		if (radTreeView1->SelectedNode->Tag == "folder")
			return;

		Telerik::WinControls::UI::RadTreeNode^ father = radTreeView1->SelectedNode->Parent;
		String^ dir = "\\";

		while (father){
			dir = "\\" + father->Text + dir;
			father = father->Parent;
		}

		String^ finaldir = Directory::GetCurrentDirectory() + "\\lua" + dir;

		String ^path = finaldir + radTreeView1->SelectedNode->Text;

		StreamReader^ streamReader = gcnew StreamReader(path);
		//fastColoredTextBox1->Text = ;

		LuaBackground^ mLuaEditor = (gcnew LuaBackground(streamReader->ReadToEnd()));
		mLuaEditor->hide_bottom_pane();
		mLuaEditor->hide_right_pane();
		mLuaEditor->hide_left_pane();
		mLuaEditor->Show();
		streamReader->Close();

		//loadCodeinFiles(e);
	}

	private: System::Void radPageView1_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
		get_current_three_view()->SelectedNode = nullptr;
		fastColoredTextBox1->Enabled = false;
		fastColoredTextBox1->Text = "";
	}

	private: System::Void menu_add_Click(System::Object^  sender, System::EventArgs^  e) {
		MenuNew_Click(nullptr, nullptr);
	}

	private: System::Void menu_remove_Click(System::Object^  sender, System::EventArgs^  e) {
					 MenuDelete_Click(nullptr, nullptr);			 
	}

	private: System::Void menu_clearlog_Click(System::Object^  sender, System::EventArgs^  e) {
		tx_lua_log->Clear();
	}

	void update_idiom(){
		ItemExecute->Text = gcnew String(&GET_TR(managed_util::fromSS(ItemExecute->Text))[0]);
		ItemNew->Text = gcnew String(&GET_TR(managed_util::fromSS(ItemNew->Text))[0]);
		ItemDelete->Text = gcnew String(&GET_TR(managed_util::fromSS(ItemDelete->Text))[0]);
		radMenuItem2->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem2->Text))[0]);
		radMenuItem1->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem1->Text))[0]);
		menu_add->Text = gcnew String(&GET_TR(managed_util::fromSS(menu_add->Text))[0]);
		menu_remove->Text = gcnew String(&GET_TR(managed_util::fromSS(menu_remove->Text))[0]);
		radMenuItem4->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem4->Text))[0]);
		menu_clearlog->Text = gcnew String(&GET_TR(managed_util::fromSS(menu_clearlog->Text))[0]);
		radMenuItem5->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem5->Text))[0]);
		radPageViewPageHuds->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewPageHuds->Text))[0]);
		radPageViewPage2->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewPage2->Text))[0]);
	}
	private: System::Void bt_search_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 //if ()
				 /*String^ searchWithinThis = "ABCDEFGHIJKLMNOP";
				 String^ searchForThis = "W";
				 int firstCharacter = searchWithinThis->IndexOf(searchForThis);

				 Console::WriteLine("First occurrence: {0}", firstCharacter);*/
				 /*for each(Telerik::WinControls::UI::RadTreeNode^ node in infoCodeThree->Nodes)
					search_function(node, bt_search->Text);*/
	}

	bool search_function(Telerik::WinControls::UI::RadTreeNode^ node, String^ functionName){
		


		/*String^ functionName_new = functionName->Replace(" ", "_");
		if (node->Text == functionName_new){
			infoCodeThree->SelectedNode = node;
			return true;
		}

		for each(Telerik::WinControls::UI::RadTreeNode^ actualNode in node->Nodes){
			bool temp = search_function(actualNode, functionName);

			if (temp == true)
				return true;
		}
		return false;*/
		return false;
	}
	private: System::Void radMenuItem3_Click_1(System::Object^  sender, System::EventArgs^  e) {
				 NeutralBot::Keywords::ShowUnique();
	}
	private: System::Void LuaBackground_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 if (GeneralManager::get()->get_practice_mode()){
					 this->Hide();
					 e->Cancel = true;
					 return;
				 }
	}
	private: System::Void bt_search_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 String^ searchForThis = bt_search->Text->Trim()->ToLower();
				 if (searchForThis == ""){
					 infoCodeThree->BeginUpdate();
					 infoCodeThree->CollapseAll();
					 for each(auto actualNode in infoCodeThree->Nodes){
						 for each(auto Node in actualNode->Nodes){
							 for each(auto temp in Node->Nodes){
								 for each(auto CANSEI in temp->Nodes){
									 if (!CANSEI->Visible)
										 CANSEI->Visible = true;
								 }
							 }
						 }
					 }
					 infoCodeThree->EndUpdate();
					 return;
				 }

				 infoCodeThree->BeginUpdate();
				 infoCodeThree->ExpandAll();
				 for each(auto actualNode in infoCodeThree->Nodes){
					 for each(auto Node in actualNode->Nodes){
						 for each(auto temp in Node->Nodes){
							 for each(auto CANSEI in temp->Nodes){
								 int firstCharacter2 = CANSEI->Text->IndexOf(searchForThis);
								 if ((firstCharacter2 >= 0 && !CANSEI->Visible) || CANSEI->Text == searchForThis)
									 CANSEI->Visible = true;
								 else if (CANSEI->Visible)
									 CANSEI->Visible = false;
							 }
						 }
					 }
				 }
				 infoCodeThree->EndUpdate();
	}

};
}

