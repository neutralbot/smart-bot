#include "RepoterManager.h"

//RepoterId
std::shared_ptr<RepotId> RepotId::mRepotId;

std::string RepotItens::get_itemName() {
	return itemName;
}
int RepotItens::get_item_id() {
	return item_id;
}
void RepotId::reset(){
	mRepotId = std::shared_ptr<RepotId>(new RepotId);
}
void RepotId::clear(){
	mapRepotItens.clear();
}


void RepotItens::set_item_id(uint32_t _item_id){
	item_id = _item_id;
	itemName = ItemsManager::get()->getItemNameFromId(_item_id);
}



void RepotId::removeRepotItem(std::string id){
	auto it = mapRepotItens.find(id);
	if (it != mapRepotItens.end())
		mapRepotItens.erase(id);
}
std::string RepotId::requestNewitem_id(){
	int id = 0;
	std::string current_id = "Item" + std::to_string(id);
	while (mapRepotItens.find(current_id) != mapRepotItens.end()){
		id++;
		current_id = "Item" + std::to_string(id);
	}
	mapRepotItens[current_id] = std::shared_ptr<RepotItens>(new RepotItens);
	mapRepotItens[current_id]->itemName = "none";
	return current_id;
}
std::string RepotId::updateItem(std::string Item, int min, int max, int gpeach){
	std::string id_ = requestNewitem_id();
	mapRepotItens[id_]->set_item_id(ItemsManager::get()->getitem_idFromName(Item));
	mapRepotItens[id_]->set_max(max);
	mapRepotItens[id_]->set_min(min);
	mapRepotItens[id_]->set_gpeach(gpeach);
	return id_;
}
std::string RepotId::requestNewItemById(std::string current_id){
	auto it = mapRepotItens.find(current_id);
	if (it == mapRepotItens.end())
		return 0;

	mapRepotItens[current_id] = std::shared_ptr<RepotItens>(new RepotItens);
	return current_id;
}
std::map<std::string, std::shared_ptr<RepotItens>> RepotId::getMapRepotItens(){
	return mapRepotItens;
}

Json::Value RepotId::parse_class_to_json() {
	Json::Value spellClasse;
	Json::Value repotItemList;

	for (auto it : mapRepotItens){
		Json::Value item;

		item["itemName"] = it.second->get_itemName();
		item["min"] = it.second->get_min();
		item["max"] = it.second->get_max();
		item["gpeach"] = it.second->get_gpeach();
		repotItemList[it.first] = item;
	}

	spellClasse["repotItemList"] = repotItemList;
	return spellClasse;
}
void RepotId::parse_json_to_class(Json::Value jsonObject) {
	if (!jsonObject["repotItemList"].empty() || !jsonObject["repotItemList"].isNull()){
		Json::Value repotItemList = jsonObject["repotItemList"];
		Json::Value::Members members = repotItemList.getMemberNames();

		for (auto member : members){
			Json::Value item = repotItemList[member];
			std::shared_ptr<RepotItens> newItem(new RepotItens);

			std::string id_str = item["itemName"].asString();
			uint32_t item_id = ItemsManager::get()->getitem_idFromName(id_str);

			newItem->set_item_id(item_id);
			newItem->set_min(item["min"].asInt());
			newItem->set_max(item["max"].asInt());
			newItem->set_gpeach(item["gpeach"].asInt());
			mapRepotItens[member] = newItem;
		}
	}
}
//RepoterManager
std::shared_ptr<RepoterManager> RepoterManager::mRepoterManager;

bool RepoterManager::checkNecessaryRepot() {
	for (auto depot : mapRepotId) {
		auto itemList = depot.second->getMapRepotItens();
		auto containersList = ContainerManager::get()->get_containers();

		for (auto container : containersList) {
			for (auto item : itemList) {
				int item_id = item.second->get_item_id();
				int containerItemCount = container.second->get_item_count_by_id(item_id);
				int itemMin = item.second->get_min();

				if (containerItemCount >= itemMin)
					return false;
			}
		}
	}
	return true;
}
bool RepoterManager::checkNecessaryRepot(std::string repotId) {
	auto repot = mapRepotId.find(repotId);

	auto itemList = repot->second->getMapRepotItens();
	auto containersList = ContainerManager::get()->get_containers();

	for (auto item : itemList) {
		int item_id = item.second->get_item_id();

		if (ContainerManager::get()->get_item_count(item_id) <= item.second->get_min())
			return true;
	}

	return false;
}
void RepoterManager::removeRepotId(std::string id_){
	auto i = mapRepotId.find(id_);
	if (i != mapRepotId.end())
		mapRepotId.erase(i);
}
void RepoterManager::reset(){
	mRepoterManager = std::shared_ptr<RepoterManager>(new RepoterManager);
}
void RepoterManager::clear(){
	mapRepotId.clear();
}
void RepoterManager::InitializeMap(){
	requestNewRepotId();
}
bool RepoterManager::changeRepotId(std::string newId, std::string oldId){
	if (newId == "" || oldId == "")
		return false;

	auto it = mapRepotId.find(oldId);
	if (it == mapRepotId.end())
		return false;

	mapRepotId[newId] = it->second;
	mapRepotId.erase(it);
	return true;
}
std::string RepoterManager::requestNewRepotId(){
	int id = 0;
	std::string current_id = "repot" + std::to_string(id);
	while (mapRepotId.find(current_id) != mapRepotId.end()){
		id++;
		current_id = "repot" + std::to_string(id);
	}
	mapRepotId[current_id] = std::shared_ptr<RepotId>(new RepotId);
	return current_id;
}
std::shared_ptr<RepotId> RepoterManager::getRepoIdRule(std::string id_){
	auto it = mapRepotId.find(id_);
	if (it == mapRepotId.end())
		return 0;
	return it->second;
}
std::map<std::string, std::shared_ptr<RepotId>> RepoterManager::getMapRepotId(){
	return mapRepotId;
}
RepoterManager::RepoterManager(){
	InitializeMap();
}

Json::Value RepoterManager::parse_class_to_json() {
	Json::Value repoterManager;
	Json::Value repotIdList;

	for (auto repot : mapRepotId){
		std::string repotId = repot.first;

		string_util::lower(repotId);

		repotIdList[repotId] = repot.second->parse_class_to_json();
	}

	repoterManager["repotIdList"] = repotIdList;
	repoterManager["reorganize_containers"] = reorganize_containers;
	repoterManager["minimize_all_containers"] = minimize_all_containers;

	return repoterManager;
}
void RepoterManager::parse_json_to_class(Json::Value jsonObject){
	if (!jsonObject["repotIdList"].empty() || !jsonObject["repotIdList"].isNull()){
		Json::Value repotIdList = jsonObject["repotIdList"];
		Json::Value::Members members = repotIdList.getMemberNames();

		for (auto member : members){
			std::shared_ptr<RepotId> repotId(new RepotId);
			repotId->parse_json_to_class(repotIdList[member]);
			mapRepotId[member] = repotId;
		}
	}		
	
	if (!jsonObject["reorganize_containers"].empty() || !jsonObject["reorganize_containers"].isNull())
		set_reorganize_containers(jsonObject["reorganize_containers"].asBool());

	if (!jsonObject["minimize_all_containers"].empty() || !jsonObject["minimize_all_containers"].isNull())
		set_minimize_all_containers(jsonObject["minimize_all_containers"].asBool());
}



std::shared_ptr<RepoterManager> RepoterManager::get(){
	if (!mRepoterManager)
		reset();
	return mRepoterManager;
}

std::shared_ptr<RepotItens> RepotId::getRepotItensRule(std::string id){
	return mapRepotItens[id];
}

void RepotId::set_Item(std::string id, std::string name){
	mapRepotItens[id]->set_item_id(ItemsManager::get()->getitem_idFromName(name));
}

void RepotId::set_Min(std::string id, uint32_t min){
	mapRepotItens[id]->set_min(min);
}

void RepotId::set_Max(std::string id, uint32_t max){
	mapRepotItens[id]->set_max(max);
}

void RepotId::set_GpEach(std::string id, uint32_t gpeach){
	mapRepotItens[id]->set_gpeach(gpeach);
}

std::string RepotId::get_Item(std::string id){
	return mapRepotItens[id]->get_itemName();
}

uint32_t RepotId::get_Min(std::string id, uint32_t min){
	return mapRepotItens[id]->get_min();
}

uint32_t RepotId::get_Max(std::string id, uint32_t max){
	return mapRepotItens[id]->get_max();
}

uint32_t RepotId::get_GpEach(std::string id, uint32_t gpeach){
	return mapRepotItens[id]->get_gpeach();
}
