#pragma once
#include "LuaThread.h"
#include <Thread>
#include "Core\LuaCore.h"
#include "DevelopmentManager.h"
#include "HUDManager.h"
#include <regex>

LuaCore* luaCore;

void LuaThread::process_input(InputBaseEvent* input){	
	bool use_keys = HUDManager::get_default()->get_default()->get_use_keys();
	bool use_mouse = HUDManager::get_default()->get_default()->get_use_mouseclicks();

	auto myMap = LuaBackgroundManager::get()->getMap();
	for (auto it = myMap.begin(); it != myMap.end(); ++it){
		Sleep(5);
		if (!it->second->is_hud())
			continue;

		if (!it->second->get_active())
			continue;

		if (input->is_mouse_event())
			if (!use_mouse)
				continue;

		if (input->is_keyboard_event())
			if (!use_keys)
				continue;

		mtx_access.lock();
		std::string name_function = "inputevents" + std::to_string(it->second->get_hast_code());
		luaHudCore->getGlobal(name_function, input);
		mtx_access.unlock();
	}
}

void LuaThread::run_wait_input(){
	while (true){
		Sleep(1);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		std::shared_ptr<InputHolder> input_retval = InputManager::get()->wait_input(UINT32_MAX);
		if (!input_retval)
			continue;

		process_input(input_retval->input_ptr);
	}
}

LuaThread::LuaThread(){
	luaCore = new LuaCore();

	luaHudCore = LuaCore::getHudLuaCore().get();
	lua_background_ptr = LuaBackgroundManager::get();
	existent_threads.push_back(CreatThreadHandler());

	std::thread([&](){
		MONITOR_THREAD("LuaThread::refresh")
		refresh(); }).detach(); 
	std::thread([&](){
		MONITOR_THREAD("LuaThread::run_wait_input")
		run_wait_input(); }).detach(); 
}

LuaThread* LuaThread::get(){
	static LuaThread* m = nullptr;
	if (!m)
		m = new LuaThread();
	return m;
}

std::shared_ptr<ThreadHandler> LuaThread::CreatThreadHandler(){
	std::shared_ptr<ThreadHandler> gp1(new ThreadHandler);

	return gp1->getPtr();
}

std::shared_ptr<ThreadHandler> LuaThread::get_free_thread_handled(bool create){
	lock_guard _lock(mtx_access);
	for (auto threadsIt = existent_threads.begin(); threadsIt != existent_threads.end(); ++threadsIt)
	if (!(*threadsIt)->get_is_used())
		return *threadsIt;

	if (create){
		auto new_thread = CreatThreadHandler();
		existent_threads.push_back(new_thread);
		return new_thread;
	}

	return nullptr;
}

void LuaThread::refresh(){
	MONITOR_THREAD(__FUNCTION__);
	while (true){
		Sleep(10);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;
			
		if (!NeutralManager::get()->get_lua_state() || !NeutralManager::get()->get_bot_state()){
			mtx_access.lock();
			if (HUDManager::get_default()->is_visible())
				HUDManager::get_default()->set_visible(false);
			mtx_access.unlock(); 
			continue;			
		}
		else if (NeutralManager::get()->get_lua_state() || NeutralManager::get()->get_bot_state()){
			mtx_access.lock();
			if (!HUDManager::get_default()->is_visible())
				HUDManager::get_default()->set_visible(true);
			mtx_access.unlock();
		}

		bool hud_state = lua_background_ptr->get_hud_state();
		bool has_elapsed_hud = false;
		bool need_draw_hud = false;

		HUDManager::get_default()->set_visible(true);
		std::vector<LuaBackgroundCodes*> events_hud_temp_vector;
		
		auto myMap = LuaBackgroundManager::get()->getMap();
		for (auto it = myMap.begin(); it != myMap.end(); ++it){
			if (it->second->is_hud()){
				if (hud_state){
					if (!it->second->get_active())
						continue;					

					need_draw_hud = true;

					if (it->second->need_execute()){
						has_elapsed_hud = true;

						auto temp = hud_vector.find(it->first);
						if (temp != hud_vector.end())							
							continue;
						
						hud_vector[it->first] = it->second.get();
					}
				}		
				continue;				
			}

			if (!it->second.get()->need_execute())
				continue;

			if (!it->second->get_enabled())
				continue;
			
			if (it->second->get_thread_on())
				continue;
			
			std::shared_ptr<ThreadHandler> thread_handler = get_free_thread_handled();
			thread_handler->set_current_script(it->second);
		}

		std::shared_ptr<LuaBackgroundCodes> code;
		while (code = LuaBackgroundManager::get()->get_elapsed_time_event()){
			Sleep(10);

			if (code->is_hud()){
				events_hud_temp_vector.push_back(code.get());
			}
			else{
				std::shared_ptr<ThreadHandler> thread_handler = get_free_thread_handled();
				thread_handler->set_current_script(code);
			}
		}

		bool enablehud = HUDManager::get_default()->get_enable();
		uint32_t hud_count_active = LuaBackgroundManager::get()->get_hud_count_active();
		if (hud_count_active > 0 && !enablehud){
			HUDManager::get_default()->set_enabled(true);
		}
		else if (hud_count_active <= 0 && enablehud){
			HUDManager::get_default()->set_enabled(false);
			HUDManager::get_default()->swap_buffer();
		}

		if (!need_draw_hud){
			if (HUDManager::get_default()->is_visible())
				HUDManager::get_default()->set_visible(false);

			continue;
		}

		if (has_elapsed_hud){
			HUDManager::get_default()->reset_mapHUDInfo();			
			std::map<std::string, LuaBackgroundCodes*> hud_vector_in = hud_vector;

			for (auto item_hud : hud_vector_in){
				if (!item_hud.second)
					continue;				

				if (!luaHudCore)
					continue;				

				if (item_hud.second->get_last_init_elapsed() > item_hud.second->get_last_update_elapsed()){
					std::string temp_string = item_hud.second->get_InitCode();
					temp_string = std::regex_replace(temp_string, std::regex("inputevents"), "inputevents" + std::to_string(item_hud.second->get_hast_code()));
					
					luaHudCore->RunScript(temp_string);					
					item_hud.second->reset_last_init();
				}

				luaHudCore->RunScript(item_hud.second->get_code());
				item_hud.second->reset_timer();
			}

			HUDManager::get_default()->swap_buffer();
		}
	}
}

void LuaThread::simple_execute(std::string &script, std::string name){
	lock_guard _lock(mtx_access);
	for (auto threadsIt = existent_threads.begin(); threadsIt != existent_threads.end(); ++threadsIt){
		if ((*threadsIt)->get_is_used())
			continue;

		luaCore->RunScript(script, name);
		return;
	}


}

LuaHandler* LuaHandler::get(){
	static LuaHandler* m = nullptr;
	if (!m)
		m = new LuaHandler();
	return m;
}

LuaHandler::LuaHandler(){
	print_id = 0;
}

void LuaHandler::add_lua_error_log(std::string error){
	mtx_access.lock();
	lua_error_log.push_back(std::make_pair(print_id, error));

	if (lua_error_log.size() > 100)
		lua_error_log.erase(lua_error_log.begin());

	print_id++;
	mtx_access.unlock();
}

std::vector<std::pair<uint64_t, std::string>> LuaHandler::get_index_greater_than(uint64_t index, bool clear){
	std::vector<std::pair<uint64_t, std::string>> retval;
	mtx_access.lock();
	for (auto log_error : lua_error_log)
		if (log_error.first > index)
			retval.push_back(log_error);

	if (clear)
		lua_error_log.clear();

	mtx_access.unlock();
	return retval;
}