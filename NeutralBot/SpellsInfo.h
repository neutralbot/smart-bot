#include "Core/Util.h"


struct spell_info{
	std::string name;
	std::string cast_words;
	int mp;
	int level;
	int ml;
	bool premmy;
	int category;
	int type;
	int soul;
	int range;
	int cooldown1;
	int cooldown2;
	int id;
};
/*
	doesnot need mutex cuzz this will be loaden when app start
*/
class SpellsInfo{
	std::vector<spell_info> spell_infos;

public:
	spell_info* get_spell_by_id(uint32_t id){
		for (auto it : spell_infos)
			if (it.id == id)
				return &it;
		
		return 0;
	}

	spell_info* get_spell_by_name(std::string name){
		for (auto it : spell_infos)
			if (it.name == name)
				return &it;
		
		return 0;
	}

	spell_info* get_spell_by_cast_words(std::string cast_words){
		for (auto it : spell_infos)
			if (it.cast_words == cast_words)
				return &it;
		
		return 0;
	}

	uint32_t get_spells_count(){
		return spell_infos.size();
	}

	static SpellsInfo* get(){
		static SpellsInfo* mSpellsInfo = nullptr;
		if (!mSpellsInfo)
			mSpellsInfo = new SpellsInfo;
		return mSpellsInfo;
	}


};

