#pragma once
#include "Core\Util.h"
#include <iostream>
#include <string>
#include <regex>
#include <iterator>
#include <unordered_map>

struct ControlInfoChield{
	std::map<std::string, std::string> mapControlInfo;
	std::vector<std::string> itemComboBox;

public:

	void clear_itemCombox(){
		itemComboBox.clear();
	}
	void add_itemComboBox(std::string itemName){
		itemComboBox.push_back(itemName);
	}
	void set_vectorItem(std::vector<std::string> itemName){
		itemComboBox = itemName;
	}
	std::vector<std::string> vectorItemComboBox(){
		return itemComboBox;
	}
	std::map<std::string, std::string> getMapInfo(){
		return mapControlInfo;
	}
	void set_property(std::string property_, std::string value_){
		mapControlInfo[property_] = value_;
	}
	std::string get_property(std::string property_){
		return mapControlInfo[property_];
	}

	Json::Value parse_class_to_json() {
		Json::Value ControlInfoClass;
		Json::Value controlInfo;
		Json::Value controlInfoVector;

		for (auto controlInfoItem : mapControlInfo)
			controlInfo[controlInfoItem.first] = mapControlInfo[controlInfoItem.first];

		for (uint32_t index = 0; index < itemComboBox.size(); index++)
			controlInfoVector.append(itemComboBox[index]);

		ControlInfoClass["ControlInfoList"] = controlInfo;
		ControlInfoClass["ControlInfoVector"] = controlInfoVector;

		return ControlInfoClass;
	}
	void parse_json_to_class(Json::Value jsonObject) {
		Json::Value ControlInfoVector = jsonObject["ControlInfoVector"];
		Json::Value ControlInfoList = jsonObject["ControlInfoList"];

		Json::Value::Members membersControlInfo = ControlInfoList.getMemberNames();

		for (auto Item : ControlInfoVector)
			itemComboBox.push_back(Item.asString());

		for (auto memberControlInfo : membersControlInfo){
			Json::Value item = ControlInfoList[memberControlInfo];
			mapControlInfo[memberControlInfo] = item.asString();
		}
	}
};

struct ControlInfo{
	std::map<std::string, std::shared_ptr<ControlInfoChield>> mapControlInfoChield;
	std::map<std::string, std::string> mapControlInfo;
	std::vector<std::string> itemComboBox_;

public:
	void set_vectorItem(std::vector<std::string> itemName){
		itemComboBox_ = itemName;
	}
	std::vector<std::string> vectorItemComboBox(){
		return itemComboBox_;
	}
	void add_itemComboBox_(std::string itemName){
		itemComboBox_.push_back(itemName);
	}
	void clear_itemCombox_(){
		itemComboBox_.clear();
	}
	std::map<std::string, std::string> getMapInfo(){
		return mapControlInfo;
	}
	std::map<std::string, std::shared_ptr<ControlInfoChield>> getMapChield(){
		return mapControlInfoChield;
	}
	
	//FUNCTIONS
	std::shared_ptr<ControlInfoChield> getControlInfoChieldRule(std::string key){
		auto it = mapControlInfoChield.find(key);
		if (it == mapControlInfoChield.end())
			return nullptr;

		return mapControlInfoChield[key];
	}
	void set_property(std::string property_, std::string value){
		mapControlInfo[property_] = value;
	}
	std::string get_property(std::string property_){
		return mapControlInfo[property_];
	}

	void removeControl(std::string id){
		auto it = mapControlInfoChield.find(id);
		if (it != mapControlInfoChield.end())
			mapControlInfoChield.erase(id);
	}
	std::string request_new_id(){
		int id = 0;
		std::string current_id = "childItem" + std::to_string(id);
		while (mapControlInfoChield.find(current_id) != mapControlInfoChield.end()){
			id++;
			current_id = "childItem" + std::to_string(id);
		}
		mapControlInfoChield[current_id] = std::shared_ptr<ControlInfoChield>(new ControlInfoChield);
		return current_id;
	}
	std::string request_new_label_id(){
		int id = 0;
		std::string current_id = "Label " + std::to_string(id);
		while (mapControlInfoChield.find(current_id) != mapControlInfoChield.end()){
			id++;
			current_id = "Label " + std::to_string(id);
		}
		mapControlInfoChield[current_id] = std::shared_ptr<ControlInfoChield>(new ControlInfoChield);
		return current_id;
	}
	std::string requestNewById(std::string id){
		auto it = mapControlInfoChield.find(id);
		if (it == mapControlInfoChield.end())
			mapControlInfoChield[id] = std::shared_ptr<ControlInfoChield>(new ControlInfoChield);

		return id;
	}
	bool changeId(std::string newId, std::string oldId){
		if (newId == "" || oldId == "")
			return false;

		auto it = mapControlInfoChield.find(oldId);
		if (it == mapControlInfoChield.end())
			return false;

		mapControlInfoChield[newId] = it->second;
		mapControlInfoChield.erase(it);
		return true;
	}
	
	Json::Value parse_class_to_json() {
		Json::Value ControlInfoClass;
		Json::Value controlInfo;
		Json::Value controlInfoVector;
		Json::Value controlInfoChield;

		for (auto controlInfoItem : mapControlInfoChield)
			controlInfoChield[controlInfoItem.first] = mapControlInfoChield[controlInfoItem.first]->parse_class_to_json();

		for (auto controlInfoItem : mapControlInfo)
			controlInfo[controlInfoItem.first] = mapControlInfo[controlInfoItem.first];
		
		for (uint32_t index = 0; index < itemComboBox_.size(); index++)
			controlInfoVector.append(itemComboBox_[index]);

		ControlInfoClass["ControlInfoList"] = controlInfo;
		ControlInfoClass["ControlInfoChieldList"] = controlInfoChield;
		ControlInfoClass["ControlInfoVector"] = controlInfoVector;

		return ControlInfoClass;
	}
	void parse_json_to_class(Json::Value jsonObject) {
		Json::Value ControlInfoList = jsonObject["ControlInfoList"];
		Json::Value ControlInfoVector = jsonObject["ControlInfoVector"];
		Json::Value ControlInfoChieldList = jsonObject["ControlInfoChieldList"];

		Json::Value::Members membersControlInfoList = ControlInfoChieldList.getMemberNames();
		Json::Value::Members membersControlInfo = ControlInfoList.getMemberNames();

		for (auto memberControlInfoList : membersControlInfoList){
			std::shared_ptr<ControlInfoChield> controlInfo(new ControlInfoChield);
			mapControlInfoChield[memberControlInfoList] = controlInfo;
			controlInfo->parse_json_to_class(ControlInfoChieldList[memberControlInfoList]);
		}		

		for (auto memberControlInfo : membersControlInfo){
			Json::Value item = ControlInfoList[memberControlInfo];
			mapControlInfo[memberControlInfo] = item.asString();
		}

		for (auto Item : ControlInfoVector)
			itemComboBox_.push_back(Item.asString());
		
	}
};

class ControlManager{
	std::map<std::string, std::shared_ptr<ControlInfo>> mapControlInfo;
	std::map<std::string, std::string> mapControlInfo_;

public:
	ControlManager(){
		mapControlInfo_["sizeW"] = "500";
		mapControlInfo_["sizeH"] = "500";
	}
	std::map<std::string, std::shared_ptr<ControlInfo>> getMap(){
		return mapControlInfo;
	}
	std::map<std::string, std::string> getMap_(){
		return mapControlInfo_;
	}
	
	bool changePropertyRepoter(std::string newtext, std::string controlName, std::string property_){
		std::shared_ptr<ControlInfo> groupRule = ControlManager::get()->getControlInfoRule("RepoterGroup");
		if (!groupRule)
			return false;


		if (property_ == "name"){
			std::map<std::string, std::shared_ptr<ControlInfoChield>> myMap = groupRule->mapControlInfoChield;
			for (auto it : myMap){
				if (it.first.find(controlName) == std::string::npos)
					continue;
					
				std::string old_name = it.first;
				std::string new_name = std::regex_replace(old_name, std::regex(controlName), newtext);
				
				groupRule->changeId(new_name, old_name);
			}
		}

		std::shared_ptr<ControlInfoChield> controlRule = groupRule->getControlInfoChieldRule(controlName);
		if (!controlRule)
			return false;

		controlRule->set_property(property_, newtext);
		return true;
	}
	bool changePropertyLooter(std::string newtext, std::string controlName, std::string property_){
		std::shared_ptr<ControlInfo> groupRule = ControlManager::get()->getControlInfoRule("LooterGroup");
		if (!groupRule)
			return false;

		std::shared_ptr<ControlInfoChield> controlRule = groupRule->getControlInfoChieldRule(controlName);
		if (!controlRule)
			return false;

		if (property_ == "name")
			groupRule->changeId(newtext, controlName);

		controlRule->set_property(property_, newtext);
		return true;
	}

	bool changePropertyDepoter(std::string newtext, std::string controlName, std::string property_){
		std::shared_ptr<ControlInfo> groupRule = ControlManager::get()->getControlInfoRule("DepoterGroup");
		if (!groupRule)
			return false;

		std::shared_ptr<ControlInfoChield> controlRule = groupRule->getControlInfoChieldRule(controlName);
		if (!controlRule)
			return false;

		if (property_ == "name")
			groupRule->changeId(newtext, controlName);

		controlRule->set_property(property_, newtext);
		return true;
	}
	bool changePropertySpellsAttack(std::string newtext, std::string controlName, std::string property_){
		std::shared_ptr<ControlInfo> groupRule = ControlManager::get()->getControlInfoRule("SpellsHealthGroup");
		if (!groupRule)
			return false;

		std::shared_ptr<ControlInfoChield> controlRule = groupRule->getControlInfoChieldRule(controlName);
		if (!controlRule)
			return false;

		if (property_ == "name")
			groupRule->changeId(newtext, controlName);

		controlRule->set_property(property_, newtext);
		return true;
	}
	bool changePropertySpellsHealth(std::string newtext, std::string controlName, std::string property_){
		std::shared_ptr<ControlInfo> groupRule = ControlManager::get()->getControlInfoRule("SpellsHealthGroup");
		if (!groupRule)
			return false;

		std::shared_ptr<ControlInfoChield> controlRule = groupRule->getControlInfoChieldRule(controlName);
		if (!controlRule)
			return false;

		if (property_ == "name")
			groupRule->changeId(newtext, controlName);

		controlRule->set_property(property_, newtext);
		return true;
	}
	//FUNCTIONS
	
	std::vector<std::string> get_text_vector_find_all(std::string controlname){
		std::vector<std::string> retval;
		
		auto it_find = mapControlInfo.find(controlname);
		if (it_find != mapControlInfo.end())
			return it_find->second->vectorItemComboBox();

		for (auto it = mapControlInfo.begin(); it != mapControlInfo.end(); it++){
			auto itemfind = it->second->mapControlInfoChield.find(controlname);
			if (itemfind != it->second->mapControlInfoChield.end())
				return itemfind->second->vectorItemComboBox();
		}

		return retval;
	}
	std::string get_property_find_all(std::string controlname, std::string property_){
		auto it_find = mapControlInfo.find(controlname);
		if (it_find != mapControlInfo.end())
			return it_find->second->mapControlInfo[property_];
		
		for (auto it = mapControlInfo.begin(); it != mapControlInfo.end(); it++){
			auto itemfind = it->second->mapControlInfoChield.find(controlname);
			if (itemfind != it->second->mapControlInfoChield.end())
				return itemfind->second->mapControlInfo[property_];
		}
		return "";
	}
	void set_property_find_all(std::string controlname, std::string property_,std::string text){
		auto it_find = mapControlInfo.find(controlname);
		if (it_find != mapControlInfo.end())
			it_find->second->mapControlInfo[property_] = text;


		for (auto it = mapControlInfo.begin(); it != mapControlInfo.end(); it++){
			auto itemfind = it->second->mapControlInfoChield.find(controlname);
			if (itemfind != it->second->mapControlInfoChield.end())
				itemfind->second->mapControlInfo[property_] = text;
		}
	}

	void removeControl(std::string id){
		auto it = mapControlInfo.find(id);
		if (it != mapControlInfo.end())
			mapControlInfo.erase(id);
	}
	bool checkExistControl(std::string id){
		auto it = mapControlInfo.find(id);
		if (it != mapControlInfo.end())
			return true;

		return false;
	}
	std::string request_new_id(){
		int id = 0;
		std::string current_id = "Item" + std::to_string(id);
		while (mapControlInfo.find(current_id) != mapControlInfo.end()){
			id++;
			current_id = "Item" + std::to_string(id);
		}
		mapControlInfo[current_id] = std::shared_ptr<ControlInfo>(new ControlInfo);
		return current_id;
	}
	std::string requestNewGroupId(){
		int id = 0;
		std::string current_id = "Group" + std::to_string(id);
		while (mapControlInfo.find(current_id) != mapControlInfo.end()){
			id++;
			current_id = "Group" + std::to_string(id);
		}
		mapControlInfo[current_id] = std::shared_ptr<ControlInfo>(new ControlInfo);

		return current_id;
	}
	std::string requestNewById(std::string id){
		auto it = mapControlInfo.find(id);
		if (it == mapControlInfo.end())
			mapControlInfo[id] = std::shared_ptr<ControlInfo>(new ControlInfo);

		return id;
	}

	bool changeId(std::string newId, std::string oldId){
		if (newId == "" || oldId == "")
			return false;

		auto it = mapControlInfo.find(oldId);
		if (it == mapControlInfo.end())
			return false;

		mapControlInfo[newId] = it->second;
		mapControlInfo.erase(it);
		return true;
	}
	std::shared_ptr<ControlInfo> getControlInfoRule(std::string key){
		auto it = mapControlInfo.find(key);
		if (it == mapControlInfo.end())
			return nullptr;

		auto oi = mapControlInfo[key];
		return oi;
	}
	void clear(){
		mapControlInfo.clear();
	}
	void set_property(std::string property_, std::string value){
		mapControlInfo_[property_] = value;
	}
	std::string get_property(std::string property_){
		return mapControlInfo_[property_];
	}

	Json::Value parse_class_to_json() {
		Json::Value ControlManagerClass;
		Json::Value ControlInfoList;
		Json::Value controlInfo;

		for (auto controlInfo : mapControlInfo)
			ControlInfoList[controlInfo.first] = mapControlInfo[controlInfo.first]->parse_class_to_json();
		
		for (auto controlInfoItem : mapControlInfo_)
			controlInfo[controlInfoItem.first] = mapControlInfo_[controlInfoItem.first];

		ControlManagerClass["ControlInfoList_"] = controlInfo;
		ControlManagerClass["ControlInfoList"] = ControlInfoList;

		return ControlManagerClass;
	}
	void parse_json_to_class(Json::Value jsonObject) {
		Json::Value ControlInfoList = jsonObject["ControlInfoList"];
		Json::Value ControlInfoList_ = jsonObject["ControlInfoList_"];

		Json::Value::Members members = ControlInfoList.getMemberNames();
		Json::Value::Members membersControlInfoList = ControlInfoList_.getMemberNames();

		for (auto member : members){
			std::shared_ptr<ControlInfo> controlInfo(new ControlInfo);
			mapControlInfo[member] = controlInfo;
			controlInfo->parse_json_to_class(ControlInfoList[member]);
		}

		for (auto memberControlInfo : membersControlInfoList){
			Json::Value item = ControlInfoList_[memberControlInfo];
			mapControlInfo_[memberControlInfo] = item.asString();
		}
	}

	static ControlManager* get(){
		static ControlManager* m = nullptr;
		if (!m)
			m = new ControlManager();
		return m;
	}
};