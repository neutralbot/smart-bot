#pragma once
#include "ManagedUtil.h"
#include "HotkeysManager.h"


namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	
	/// <summary>
	/// Summary for InputWait
	/// </summary>
	public ref class InputWait : public Telerik::WinControls::UI::RadForm
	{
	public:
		InputBaseEvent* input_retval;
		bool finished;
		InputWait(void)
		{
			finished = false;
			input_retval = 0;
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~InputWait()
		{
			if (components)
			{
				delete components;
			}
		}
	 Telerik::WinControls::UI::RadPanel^  panelRetriveInput;
	 System::Windows::Forms::Timer^  timer1;
	 System::ComponentModel::IContainer^  components;
	protected:

	

		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->panelRetriveInput = (gcnew Telerik::WinControls::UI::RadPanel());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelRetriveInput))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// panelRetriveInput
			// 
			this->panelRetriveInput->Dock = System::Windows::Forms::DockStyle::Fill;
			this->panelRetriveInput->Font = (gcnew System::Drawing::Font(L"Segoe UI", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->panelRetriveInput->Location = System::Drawing::Point(0, 0);
			this->panelRetriveInput->Name = L"panelRetriveInput";
			this->panelRetriveInput->Size = System::Drawing::Size(386, 261);
			this->panelRetriveInput->TabIndex = 7;
			this->panelRetriveInput->Text = L"PRESS KEYBOARD OR MOUSE BUTTON";
			this->panelRetriveInput->TextAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 10;
			this->timer1->Tick += gcnew System::EventHandler(this, &InputWait::timer1_Tick);
			// 
			// InputWait
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(386, 261);
			this->Controls->Add(this->panelRetriveInput);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Name = L"InputWait";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"InputWait";
			this->Shown += gcnew System::EventHandler(this, &InputWait::InputWait_Shown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelRetriveInput))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	 System::Void InputWait_Shown(System::Object^  sender, System::EventArgs^  e) {
		input_match_params params;
		params.filter_mouse_moves = false;
		std::shared_ptr<InputHolder> input = InputManager::get()->wait_input(UINT32_MAX, params);
		if (input){
			if (input->input_ptr->is_mouse_event()){
				input_retval = new MouseInput(*(MouseInput*)input->input_ptr);
			}
			else if (input->input_ptr->is_keyboard_event()){
				input_retval = new KeyboardInput(*(KeyboardInput*)input->input_ptr);
			}
			
		}
		finished = true;
		
	}
	 System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
		if (finished){
			Sleep(50);
			this->Close();
		}
	}
};
}
