#pragma once
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <vector>
#include <stdint.h>

#include "FastUIPanelController.h"


namespace managed_util{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	std::string fromSS(System::String^ in);
	System::String^ fromString(std::string in);
	array<System::String^>^ convert_vector_to_array(std::vector<std::string> vct);

	std::string to_std_string(String^ in);

	Image ^ResizeImage(Image^ source, RectangleF destinationBounds);
	Image^ get_creature_image(String^ creature, uint32_t rect_side_size);
	Image^ get_item_image(String^ creature, uint32_t rect_side_size);
	void init();
	public ref class ManagedGiffs{
	public:
		ManagedGiffs();
		System::Collections::Generic::Dictionary<String^, System::Drawing::Image^>^ item_images
			= gcnew System::Collections::Generic::Dictionary<String^, System::Drawing::Image^>();
		System::Collections::Generic::Dictionary<String^, System::Drawing::Image^>^ creature_images 
			= gcnew System::Collections::Generic::Dictionary<String^, System::Drawing::Image^>();
		void update();

		Image^ get_creature_image(String^ creature_image_name, uint32_t rect_sice_len);
		Image^ get_item_image(String^ creature_image_name, uint32_t rect_sice_len);
		static ManagedGiffs^ mManagedGiffs;
		static ManagedGiffs^ get();
	};
	public ref class ListItemStringNum{
		
		int code;
		String^ text;
	public:
		ListItemStringNum(){
			code = 0;
			text = "";
		}
		ListItemStringNum(int code, String^ text){
			this->code = code;
			this->text = text;
		}
		virtual String^ ToString() override{
			return text;
		}
	};


	public ref class ManagedControlsHook{
	public:
		System::Collections::Generic::List<Telerik::WinControls::UI::RadToggleButton^> toggleButtonsList;

		static ManagedControlsHook^ mManagedControlsHook;
		static ManagedControlsHook^ get(){
			if (mManagedControlsHook == nullptr){
				mManagedControlsHook = gcnew ManagedControlsHook();
			}
			return mManagedControlsHook;
		}


		System::Void buttonCheckChange(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);

		void addToggleButtons(Telerik::WinControls::UI::RadToggleButton^ button);

		void removeToggleButtons(Telerik::WinControls::UI::RadToggleButton^ button);

		void updateButtonsByVars();
	};

	void setToggleCheckButtonStyle(Telerik::WinControls::UI::RadToggleButton^ button);
	void unsetToggleCheckButtonStyle(Telerik::WinControls::UI::RadToggleButton^ button);
	bool check_exist_item(String^ item);
}
