#pragma once
#include <Windows.h>
#include <stdint.h>
#include <string>
#include <Psapi.h>

#pragma comment(lib, "Psapi.lib")
#pragma comment(lib, "Version.lib")

class PRODUCT_VERSION{
public:
	PRODUCT_VERSION();
	PRODUCT_VERSION(uint16_t MajorVersion, uint16_t MinorVersion, uint16_t BuildNumber, uint16_t RevisionNumber);

	uint16_t MajorVersion;
	uint16_t MinorVersion;
	uint16_t BuildNumber;
	uint16_t RevisionNumber;

	bool operator > (PRODUCT_VERSION& v2){
		if (this->MajorVersion > v2.MajorVersion)
			return true;
		if (this->MinorVersion > v2.MinorVersion)
			return true;
		if (this->BuildNumber > v2.BuildNumber)
			return true;
		if (this->RevisionNumber > v2.RevisionNumber)
			return true;
		return false;
	}
	bool operator < (PRODUCT_VERSION& v2){
		if (this->MajorVersion < v2.MajorVersion)
			return true;
		if (this->MinorVersion < v2.MinorVersion)
			return true;
		if (this->BuildNumber < v2.BuildNumber)
			return true;
		if (this->RevisionNumber < v2.RevisionNumber)
			return true;
		return false;
	}
	bool operator >= (PRODUCT_VERSION& v2){
		if (this->MajorVersion < v2.MajorVersion)
			return false;
		if (this->MinorVersion < v2.MinorVersion)
			return false;
		if (this->BuildNumber < v2.BuildNumber)
			return false;
		if (this->RevisionNumber < v2.RevisionNumber)
			return false;
		return true;
	}
	bool operator <= (PRODUCT_VERSION& v2){
		if (this->MajorVersion > v2.MajorVersion)
			return false;
		if (this->MinorVersion > v2.MinorVersion)
			return false;
		if (this->BuildNumber > v2.BuildNumber)
			return false;
		if (this->RevisionNumber > v2.RevisionNumber)
			return false;
		return true;
	}

	uint64_t GetUint32Version();

	uint32_t GetTibiaVersion(uint32_t pid);

	static PRODUCT_VERSION* get(){
		static PRODUCT_VERSION* m = nullptr;
		if (!m)
			m = new PRODUCT_VERSION();
		return m;
	}
};


