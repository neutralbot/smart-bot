#pragma once
#include "LanguageManager.h"
#include "EnumToTranslate.h"
namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Telerik::WinControls::UI;
	/// <summary>
	/// Summary for TranslateForm
	/// </summary>
	public ref class TranslateForm : public Telerik::WinControls::UI::RadForm
	{
	public:
		TranslateForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~TranslateForm()
		{
			if (components)
			{
				delete components;
			}
		}
	 System::Windows::Forms::Label^  label1;
	 System::Windows::Forms::Label^  label2;
	 System::Windows::Forms::Label^  label3;
	 System::Windows::Forms::Label^  label4;
	 Telerik::WinControls::UI::RadListView^  radListView1;
	 System::Windows::Forms::Button^  button1;
	 System::Windows::Forms::Button^  button2;

	protected:

	
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(21, 16);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(35, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"label1";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(21, 40);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(35, 13);
			this->label2->TabIndex = 1;
			this->label2->Text = L"label2";
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(21, 65);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(35, 13);
			this->label3->TabIndex = 2;
			this->label3->Text = L"label3";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(21, 93);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(35, 13);
			this->label4->TabIndex = 3;
			this->label4->Text = L"label4";
			// 
			// radListView1
			// 
			this->radListView1->Location = System::Drawing::Point(115, 9);
			this->radListView1->Name = L"radListView1";
			this->radListView1->Size = System::Drawing::Size(120, 95);
			this->radListView1->TabIndex = 4;
			this->radListView1->Text = L"radListView1";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(17, 170);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 5;
			this->button1->Text = L"eng";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &TranslateForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(17, 209);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 6;
			this->button2->Text = L"pt";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &TranslateForm::button2_Click);
			// 
			// TranslateForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->radListView1);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Name = L"TranslateForm";
			this->Text = L"TranslateForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	void update_idiom(){
		label1->Text = gcnew String(&GET_TR("Interface Text 1")[0]);
		label2->Text = gcnew String(&GET_TR("Interface Text 2")[0]);
		label3->Text = gcnew String(&GET_TR("Interface Text 3")[0]);
		label4->Text = gcnew String(&GET_TR("Interface Text 4")[0]);
		update_idiom_listbox();
	}

	void update_idiom_listbox(){
		radListView1->Items->Clear();
		for (int i = example_var_t_1; i < (int)example_var_t_total; i++){
			RadListDataItem^ item = gcnew RadListDataItem();
			item->Text = GET_MANAGED_TR(&(std::string("example_var_t_") + std::to_string(i))[0]);
			item->Value = i;
			radListView1->Items->Add(item);
		}
	}
	 System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		LanguageManager::get()->set_current_lang("eng");
		update_idiom();
	}
 System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	LanguageManager::get()->set_current_lang("pt");
	update_idiom();
}
};
}
