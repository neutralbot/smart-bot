#pragma once
#include <functional>
#include <stdint.h>
#include <iostream>
#include <memory>
#include <chrono>
#include <vector>
#include <map>

#pragma pack(push, 1)
struct HUDCOORD{
	int X;
	int Y;
};

struct HUDARGB{
	int a;
	int r;
	int g;
	int b;
};

struct HUDSIZE{
	int Width;
	int Height;
};

struct HUDRECT{
	HUDRECT(){
	}
	HUDRECT(int x, int y, int w, int h){
		X = x;
		Y = y;
		Width = w;
		Height = h;
	}
	int X;
	int Y;
	int Width;
	int Height;
	bool is_valid(){
		if ((!Width || Width < 0) || (!Height || Height < 0))
			return false;
		return true;
	}
};

class TimeChronometerTEMP{
	std::chrono::system_clock::time_point start_time;

public:
	TimeChronometerTEMP(){
		reset();
	}

	int32_t elapsed_milliseconds(){
		return (int32_t)(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
	}

	void reset(){
		start_time = std::chrono::high_resolution_clock::now();
	}
};

class ImageClass{
public:
	uint32_t Image;
	TimeChronometerTEMP time_on;
	bool sucess;
};

typedef std::shared_ptr<ImageClass> ImagePtr;

class ImageManager{
	std::function<std::pair<char*, uint32_t>(std::string)> get_image_funct;
	std::map<std::string, ImagePtr> mapImage;

	ImagePtr creat_new_imageclass(uint32_t temp_image, std::string filename);
	uint32_t get_image_by_buffer(char* buffer, int length);
	void delete_image();
	ImagePtr load(std::string filename);

public:
	ImagePtr ImageManager::get_item(std::string filename);
	ImagePtr get_image(std::string filename);
	void set_image(std::function<std::pair<char*, uint32_t>(std::string itemname)> image_){
		get_image_funct = image_;
	}

	static ImageManager* get(){
		static ImageManager* m = nullptr;
		if (!m)
			m = new ImageManager;
		return m;
	}
};

struct HUDInfo{
	HUDCOORD hudcoord;
	HUDARGB hudargb;
	HUDSIZE hudsize;
	HUDRECT hudrect;
public:
	void set_hudrect(HUDRECT hudrect){
		this->hudrect = hudrect;
	}
};


class HUDManager{
	bool must_redraw = false;
	uint32_t hWnd;
	uint32_t hDC;
	uint32_t memDC;
	bool use_keys = true;
	bool use_mouseclicks = true;
	uint32_t hMemBmp;
	uint32_t hOldBmp;
	uint32_t* mutex_ptr;

	std::vector<std::pair< float, HUDARGB >> GradientMultiColor;
	HUDCOORD SetPosition;
	HUDARGB BorderColor;
	HUDARGB FontColor;
	uint32_t BorderSize;
	HUDARGB BorderFontColor;
	uint32_t BorderFontSize;
	uint32_t FontSize;
	uint32_t FontWeight;
	std::string FontName;

	HUDARGB ColorLine;

	std::function<uint32_t()> get_windowshandle;
	std::function<std::pair<char*, uint32_t>(std::string)> get_image_funct;
	std::function<bool()> get_states;
	bool disable_hud;

	uint32_t* ptrgraphics;

	bool CreateHudWindow();

	void copy_from_window();
	std::pair<char*, uint32_t> load_file_data(std::string path);
	std::map<uint32_t /*HUD_ID*/, std::shared_ptr<HUDInfo>> mapHUDInfo;

	bool initialized;
	bool enabled;
	bool visible;

public:
	void reset_mapHUDInfo();
	uint32_t generateNewId();


	std::wstring string_to_wchar(std::string string);

	void swap_buffer();

	HUDManager();
	~HUDManager();

	//GET SET
	bool get_use_keys(){
		return use_keys;
	}	
	bool get_use_mouseclicks(){
		return use_mouseclicks;
	}
	void filterinput(bool keys, bool mouseclicks){
		use_keys = keys;
		use_mouseclicks = mouseclicks;
	}
	void set_windowshandle(std::function<uint32_t()> windowshandle){
		get_windowshandle = windowshandle;
	}

	void set_image(std::function<std::pair<char*, uint32_t>(std::string itemname)> image_){
		get_image_funct = image_;
	}

	void set_states(std::function<bool()> state_){
		get_states = state_;
	}

	void reset_drawing_objects();

	void init();

	bool get_enable();

	void startLoop();
	
	std::map<uint32_t /*HUD_ID*/, std::shared_ptr<HUDInfo>> get_mapHUDInfo();
	HUDSIZE get_size_desktop();
	HUDSIZE get_size_client();

	void setfontstyle(std::string fontname, uint32_t size, uint32_t fontweight, HUDARGB  fontcolor, uint32_t bordersize = 0, HUDARGB bordercolor = HUDARGB{ 0, 0, 0, 0 });

	void setfontname(std::string fontname);

	void setfontsize(uint32_t size);

	void setfontborder(int BorderSize, HUDARGB bordercolor);

	void setbordersize(int BorderSize);

	void setfontcolor(HUDARGB FontColor);

	void setbordercolor(HUDARGB color);

	void addgradcolors(std::vector<std::pair< float, HUDARGB >>& intporlations_info);

	void setposition(HUDCOORD position);

	HUDSIZE measurestring(std::string text);

	uint32_t* get_image(char* buffer, int length);

	void drawtext(std::string Text, HUDCOORD Point_);

	uint32_t drawcircle(HUDRECT rect);

	uint32_t drawroundrect(HUDRECT rect, int radiusX, int radiusY);

	uint32_t drawrect(HUDRECT rect);

	void drawitem(std::string Path_or_Name, HUDCOORD XY, HUDSIZE ZOOM);

	void drawimage(std::string Path_or_Name, HUDCOORD XY, HUDCOORD SRCXY, HUDSIZE Size);

	void drawline(HUDCOORD POINT_1, HUDCOORD POINT_2);

	void GetRoundRectPath(uint32_t* pathINT, HUDRECT r, int diax, int diay);

	bool is_enabled();

	void set_enabled(bool in);

	bool is_visible();

	void set_visible(bool in);

	static HUDManager* get_default();
};


#pragma pack(pop)