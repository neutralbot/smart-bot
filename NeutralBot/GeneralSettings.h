#pragma once 
#include "GeneralManager.h"

class GeneralSettings{
	uint32_t type_wait_time = 0;
	uint32_t press_wait_time = 0;
	uint32_t click_wait_time = 0;
	stuck_ctrl_shift_policy_t stuck_ctrl_shift_index = stuck_ctrl_shift_policy_t::stuck_ctrl_shift_policy_release_instantly;
	move_speed_t move_speed_index = move_speed_t::move_speed_instantaneous;
	stuck_cursor_policy_t stuck_cursor_index = stuck_cursor_policy_t::stuck_cursor_policy_release_instantly;
	scroll_mode_t scroll_mode_index = scroll_mode_t::scroll_mode_click_on_scrollbar;
	
public:
	void set_scroll_mode_index(scroll_mode_t temp){
		scroll_mode_index = temp;
	}
	scroll_mode_t get_scroll_mode_index(){
		return scroll_mode_index;
	}

	void set_stuck_cursor_index(stuck_cursor_policy_t temp){
		stuck_cursor_index = temp;
	}
	stuck_cursor_policy_t get_stuck_cursor_index(){
		return stuck_cursor_index;
	}

	void set_move_speed_index(move_speed_t temp){
		move_speed_index = temp;
	}
	move_speed_t get_move_speed_index(){
		return move_speed_index;
	}

	void set_stuck_ctrl_shift_index(stuck_ctrl_shift_policy_t temp){
		stuck_ctrl_shift_index = temp;
	}
	stuck_ctrl_shift_policy_t get_stuck_ctrl_shift_index(){
		return stuck_ctrl_shift_index;
	}

	void set_click_wait_time(uint32_t temp){
		click_wait_time = temp;
	}
	uint32_t get_click_wait_time(){
		return click_wait_time;
	}

	void set_type_wait_time(uint32_t temp){
		type_wait_time = temp;
	}
	uint32_t get_type_wait_time(){
		return type_wait_time;
	}

	void set_press_wait_time(uint32_t temp){
		press_wait_time = temp;
	}
	uint32_t get_press_wait_time(){
		return press_wait_time;
	}
	
	Json::Value parse_class_to_json(){
		Json::Value alertManager;

		alertManager["type_wait_time"] = type_wait_time;
		alertManager["press_wait_time"] = press_wait_time;
		alertManager["click_wait_time"] = click_wait_time;
		alertManager["stuck_ctrl_shift_index"] = stuck_ctrl_shift_index;
		alertManager["move_speed_index"] = move_speed_index;
		alertManager["stuck_cursor_index"] = stuck_cursor_index;
		alertManager["scroll_mode_index"] = scroll_mode_index;

		return alertManager;
	}
	void parse_json_to_class(Json::Value jsonObject) {
		if (!jsonObject["type_wait_time"].empty() || !jsonObject["type_wait_time"].isNull())
			type_wait_time = jsonObject["type_wait_time"].asInt();

		if (!jsonObject["press_wait_time"].empty() || !jsonObject["press_wait_time"].isNull())
			press_wait_time = jsonObject["press_wait_time"].asInt();

		if (!jsonObject["click_wait_time"].empty() || !jsonObject["click_wait_time"].isNull())
			click_wait_time = jsonObject["click_wait_time"].asInt();

		if (!jsonObject["stuck_ctrl_shift_index"].empty() || !jsonObject["stuck_ctrl_shift_index"].isNull())
			stuck_ctrl_shift_index = (stuck_ctrl_shift_policy_t)jsonObject["stuck_ctrl_shift_index"].asInt();

		if (!jsonObject["move_speed_index"].empty() || !jsonObject["move_speed_index"].isNull())
			move_speed_index = (move_speed_t)jsonObject["move_speed_index"].asInt();

		if (!jsonObject["stuck_cursor_index"].empty() || !jsonObject["stuck_cursor_index"].isNull())
			stuck_cursor_index = (stuck_cursor_policy_t)jsonObject["stuck_cursor_index"].asInt();

		if (!jsonObject["scroll_mode_index"].empty() || !jsonObject["scroll_mode_index"].isNull())
			scroll_mode_index = (scroll_mode_t)jsonObject["scroll_mode_index"].asInt();
	}

	static GeneralSettings* get(){
		static GeneralSettings* m = nullptr;
		if (!m)
			m = new GeneralSettings();
		return m;
	}
};