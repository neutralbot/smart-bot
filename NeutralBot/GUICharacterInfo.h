#pragma once
#include "Core\Process.h"
#include "Core\TibiaProcess.h"
#include <msclr\marshal_cppstd.h>
#include "Core\DepoterCore.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	public ref class GUICharacterInfo : public Telerik::WinControls::UI::RadForm
	{
	public:
		GUICharacterInfo(void){
			InitializeComponent();
		}

	protected:
		~GUICharacterInfo(){
			if (components)
				delete components;
		}
	protected:
	 Telerik::WinControls::UI::RadListView^  ClientInfoListView;
	 Telerik::WinControls::UI::RadButton^  LoadClientInfoButton;


	 Telerik::WinControls::UI::RadButton^  radButton2;
	protected:
	
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->ClientInfoListView = (gcnew Telerik::WinControls::UI::RadListView());
			this->LoadClientInfoButton = (gcnew Telerik::WinControls::UI::RadButton());
			this->radButton2 = (gcnew Telerik::WinControls::UI::RadButton());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ClientInfoListView))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->LoadClientInfoButton))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// ClientInfoListView
			// 
			this->ClientInfoListView->Location = System::Drawing::Point(13, 13);
			this->ClientInfoListView->Name = L"ClientInfoListView";
			this->ClientInfoListView->Size = System::Drawing::Size(100, 162);
			this->ClientInfoListView->TabIndex = 1;
			this->ClientInfoListView->Text = L"radListView2";
			// 
			// LoadClientInfoButton
			// 
			this->LoadClientInfoButton->Location = System::Drawing::Point(12, 185);
			this->LoadClientInfoButton->Name = L"LoadClientInfoButton";
			this->LoadClientInfoButton->Size = System::Drawing::Size(101, 24);
			this->LoadClientInfoButton->TabIndex = 2;
			this->LoadClientInfoButton->Text = L"Load Tibia";
			this->LoadClientInfoButton->Click += gcnew System::EventHandler(this, &GUICharacterInfo::LoadClientInfoButton_Click);
			// 
			// radButton2
			// 
			this->radButton2->Location = System::Drawing::Point(119, 81);
			this->radButton2->Name = L"radButton2";
			this->radButton2->Size = System::Drawing::Size(110, 24);
			this->radButton2->TabIndex = 8;
			this->radButton2->Text = L"radButton2";
			this->radButton2->Click += gcnew System::EventHandler(this, &GUICharacterInfo::radButton2_Click);
			// 
			// GUICharacterInfo
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(257, 219);
			this->Controls->Add(this->radButton2);
			this->Controls->Add(this->LoadClientInfoButton);
			this->Controls->Add(this->ClientInfoListView);
			this->Name = L"GUICharacterInfo";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"Neutral Bot";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ClientInfoListView))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->LoadClientInfoButton))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	 System::Void LoadClientInfoButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 ClientInfoListView->Items->Clear();

				 Process::get()->RefresherTibiaClients();

				 std::map<uint32_t, TibiaProcessInfo> processes = Process::get()->GetProcessesMap();

				 for (auto process : processes){
					 Telerik::WinControls::UI::ListViewDataItem^ ClientInfo = (gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(&process.second.player_name[0])));
					 ClientInfo->SubItems->Add(process.second.pid);

					 ClientInfoListView->Items->Add(ClientInfo);
					 TibiaProcess::get_default()->set_pid(process.second.pid);
				 }

				 ItemsManager::get();
				 ContainerManager::get();
				 gMapTiles;
				 gMapMinimap;
	}

	 System::Void radButton2_Click(System::Object^  sender, System::EventArgs^  e) {
				 Actions::open_depot_chest();
	}
};
}
