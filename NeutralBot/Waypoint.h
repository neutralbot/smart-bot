#pragma once
#include "string"
#include "iostream"
#include "WaypointManager.h"
#include <msclr\marshal_cppstd.h>
#include "Core\TibiaProcess.h"
#include "Core\DepoterCore.h"
#include "Core\Util.h"
#include "Core\Actions.h"
#include "GeneralManager.h" 


namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms; 
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace msclr::interop;

	public ref class Waypoint : public Telerik::WinControls::UI::RadForm
	{
		
	public:
		Waypoint(void);
		static void CloseUnique(){
			if (unique)
				unique->Close();
		}
	protected:
		~Waypoint();

	public: static void HideUnique(){
				if (unique)unique->Hide();

	}
			bool is_changing = false;
			enum class waypoint_edition_mode_t{
				insert_edit,
				edit_insert,
				hud_mode,
				minimap_plus_hud_mode
			};
			enum class orientation_enum_t{
				orientation_north,
				orientation_northeast,
				orientation_east,
				orientation_southeast,
				orientation_south,
				orientation_southwest,
				orientation_west,
				orientation_northwest,
				orientation_center
			};
			orientation_enum_t current_hud_orientation;
	protected:
		Telerik::WinControls::UI::RadToggleButton^ currentSideButtonMini;
		Telerik::WinControls::UI::RadToggleButton^ currentSideButton;
		Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer2;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel3;
	protected:

		Telerik::WinControls::UI::RadRepeatButton^  buttonZoomIn;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel4;
	protected:

		Telerik::WinControls::UI::RadRepeatButton^  buttonZoomOut;
		Telerik::WinControls::UI::RadSplitContainer^  radSplitContainer3;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel5;
	protected:
		Telerik::WinControls::UI::RadRepeatButton^  buttonUpLevel;
	private: Telerik::WinControls::UI::SplitPanel^  splitPanel6;
	protected:
		Telerik::WinControls::UI::RadRepeatButton^  buttonDownLevel;
		Telerik::WinControls::UI::RadButton^  radHideMiniMap;
		Telerik::WinControls::UI::RadButton^  setCurrentCordButton;
		Telerik::WinControls::UI::RadButton^  setSelectedCordButton;
		Telerik::WinControls::UI::RadPanel^  panelEdit;
		Telerik::WinControls::UI::RadCheckBox^  checkBoxTopMost;
		Telerik::WinControls::UI::RadPanel^  panelMiniMap;
		Telerik::WinControls::UI::RadPanel^  panelHudMode;
		Telerik::WinControls::UI::RadButton^  btnuse;
		Telerik::WinControls::UI::RadButton^  btnladder;
		Telerik::WinControls::UI::RadButton^  btnmachete;
		Telerik::WinControls::UI::RadButton^  btnwalk;
		Telerik::WinControls::UI::RadButton^  btnrope;
		Telerik::WinControls::UI::RadButton^  btnlua;
		Telerik::WinControls::UI::RadButton^  btnshovel;
		Telerik::WinControls::UI::RadButton^  btnstand;
		Telerik::WinControls::UI::RadButton^  radButton11;
		Telerik::WinControls::UI::RadButton^  btnlever;
		Telerik::WinControls::UI::RadRepeatButton^  radRepeatButton2;
		Telerik::WinControls::UI::RadRepeatButton^  radRepeatButton1;

		Telerik::WinControls::UI::RadCheckBox^  checkBoxShowOtherLevels;
		Telerik::WinControls::UI::RadPanel^  radPanelMenu;
		Telerik::WinControls::UI::RadMenu^  radMenu1;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem7;
		Telerik::WinControls::UI::RadMenuItem^  pathMenu;
		Telerik::WinControls::UI::RadMenuItem^  waypointerMenu;
		Telerik::WinControls::UI::RadMenuComboItem^  radMenuComboItem1;
		Telerik::WinControls::UI::RadToggleButton^  radionortheast;
		Telerik::WinControls::UI::RadToggleButton^  radionorth;
		Telerik::WinControls::UI::RadToggleButton^  radionorthwest;
		Telerik::WinControls::UI::RadToggleButton^  radiosoutheast;
		Telerik::WinControls::UI::RadToggleButton^  radiosouth;
		Telerik::WinControls::UI::RadToggleButton^  radiosouthwest;
		Telerik::WinControls::UI::RadToggleButton^  radioeast;
		Telerik::WinControls::UI::RadToggleButton^  radiocenter;
		Telerik::WinControls::UI::RadToggleButton^  radiowest;
		System::Windows::Forms::ImageList^  imageList1;
		Telerik::WinControls::RootRadElement^  object_2126a577_02be_4922_b078_be4cfcea14ab;
		Telerik::WinControls::RootRadElement^  object_6592ab6f_bcb9_4797_b57a_0225350bf213;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem8;
		Telerik::WinControls::UI::RadPanel^  radPanel1;
		Telerik::WinControls::UI::RadPanel^  radPanel2;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonWaypointer;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem9;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem10;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem11;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem12;
	public: Telerik::WinControls::UI::RadButton^  bt_update_coord;

	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem13;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem14;
	private: Telerik::WinControls::UI::RadCheckBox^  check_lure_reverse;
	private: Telerik::WinControls::UI::RadCheckBox^  check_automatic_lure;
	protected: Telerik::WinControls::UI::RadToggleButton^  radiosoutheastNew;
	private:

	protected: Telerik::WinControls::UI::RadToggleButton^  radiosouthNew;
	private:

	protected: Telerik::WinControls::UI::RadToggleButton^  radiosouthwestNew;
	protected: Telerik::WinControls::UI::RadToggleButton^  radioeastNew;
	protected: Telerik::WinControls::UI::RadToggleButton^  radiocenterNew;



	protected: Telerik::WinControls::UI::RadToggleButton^  radiowestNew;

	protected: Telerik::WinControls::UI::RadToggleButton^  radionorthwestNew;
	protected: Telerik::WinControls::UI::RadToggleButton^  radionortheastNew;


	protected: Telerik::WinControls::UI::RadToggleButton^  radionorthNew;

	public: Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
protected: Telerik::WinControls::UI::RadGroupBox^  radGroupBox10;
public: Telerik::WinControls::UI::RadTextBox^  waypointPathLabelTextBox;
protected:

protected:

protected:

protected:
public:

protected:
public: Telerik::WinControls::UI::RadGroupBox^  radGroupBox11;
public: Telerik::WinControls::UI::RadTextBox^  waypointInfoLabelTextBox;



	protected:

	protected:
	public:
	private:
	protected:
	protected: static Waypoint^ unique;
	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew Waypoint();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void ReloadForm(){
				unique->Waypoint_Load(nullptr, nullptr);
	}
			void MineInitialize(){
				loadWaypointOptions();
			}	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Waypoint();
			}
			MiniMap::MiniMapControl^  miniMapControl1;


			bool isChangingIndex = false;
			Telerik::WinControls::UI::RadContextMenu^  miniMapMenu;
			Telerik::WinControls::UI::RadContextMenu^  waypointConfigureItem;
			Telerik::WinControls::UI::RadMenuItem^  radMenuItem30;
			Telerik::WinControls::UI::RadMenuItem^  radMenuItem4;
			Telerik::WinControls::UI::RadMenuItem^  radMenuItem5;
			Telerik::WinControls::UI::RadMenuItem^  radMenuItem6;
			Telerik::WinControls::UI::RadLabel^  radLabel3;
			System::Windows::Forms::Timer^  timer1;
			Telerik::WinControls::UI::RadPageView^  waypointPageView;
			Telerik::WinControls::UI::RadContextMenu^  waypointPageContextMenu;
			Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
			Telerik::WinControls::UI::RadMenuItem^  radMenuItem2;
			Telerik::WinControls::UI::RadMenuItem^  radMenuItem3;



			Telerik::WinControls::UI::RadButton^  waypointAddButton;
			Telerik::WinControls::UI::RadDropDownList^  waypointActionDropDownList;
	private: System::ComponentModel::IContainer^  components;
	public:
#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Waypoint::typeid));
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			this->waypointPageView = (gcnew Telerik::WinControls::UI::RadPageView());
			this->waypointPageContextMenu = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
			this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem2 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem3 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem14 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->waypointActionDropDownList = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->waypointAddButton = (gcnew Telerik::WinControls::UI::RadButton());
			this->miniMapMenu = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
			this->waypointConfigureItem = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
			this->radMenuItem6 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem4 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem30 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem13 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem5 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->radSplitContainer2 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
			this->splitPanel3 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->buttonZoomIn = (gcnew Telerik::WinControls::UI::RadRepeatButton());
			this->splitPanel4 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->buttonZoomOut = (gcnew Telerik::WinControls::UI::RadRepeatButton());
			this->radSplitContainer3 = (gcnew Telerik::WinControls::UI::RadSplitContainer());
			this->splitPanel5 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->buttonUpLevel = (gcnew Telerik::WinControls::UI::RadRepeatButton());
			this->splitPanel6 = (gcnew Telerik::WinControls::UI::SplitPanel());
			this->buttonDownLevel = (gcnew Telerik::WinControls::UI::RadRepeatButton());
			this->radHideMiniMap = (gcnew Telerik::WinControls::UI::RadButton());
			this->panelEdit = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radiosoutheastNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->imageList1 = (gcnew System::Windows::Forms::ImageList(this->components));
			this->radioeastNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radGroupBox10 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->waypointPathLabelTextBox = (gcnew Telerik::WinControls::UI::RadTextBox());
			this->radGroupBox11 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->waypointInfoLabelTextBox = (gcnew Telerik::WinControls::UI::RadTextBox());
			this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->radiosouthNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radiosouthwestNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radiocenterNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radiowestNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radionorthwestNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radionortheastNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radionorthNew = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->check_lure_reverse = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->check_automatic_lure = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->bt_update_coord = (gcnew Telerik::WinControls::UI::RadButton());
			this->checkBoxTopMost = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->panelMiniMap = (gcnew Telerik::WinControls::UI::RadPanel());
			this->checkBoxShowOtherLevels = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->radRepeatButton2 = (gcnew Telerik::WinControls::UI::RadRepeatButton());
			this->radRepeatButton1 = (gcnew Telerik::WinControls::UI::RadRepeatButton());
			this->miniMapControl1 = (gcnew MiniMap::MiniMapControl());
			this->panelHudMode = (gcnew Telerik::WinControls::UI::RadPanel());
			this->btnlever = (gcnew Telerik::WinControls::UI::RadButton());
			this->radiosoutheast = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radButton11 = (gcnew Telerik::WinControls::UI::RadButton());
			this->btnuse = (gcnew Telerik::WinControls::UI::RadButton());
			this->radiosouth = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->btnladder = (gcnew Telerik::WinControls::UI::RadButton());
			this->btnmachete = (gcnew Telerik::WinControls::UI::RadButton());
			this->radiosouthwest = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->btnwalk = (gcnew Telerik::WinControls::UI::RadButton());
			this->btnrope = (gcnew Telerik::WinControls::UI::RadButton());
			this->radioeast = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->btnlua = (gcnew Telerik::WinControls::UI::RadButton());
			this->radiocenter = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->btnshovel = (gcnew Telerik::WinControls::UI::RadButton());
			this->radiowest = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->btnstand = (gcnew Telerik::WinControls::UI::RadButton());
			this->radionorthwest = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radionortheast = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radionorth = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radPanelMenu = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
			this->radMenuItem7 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->pathMenu = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem9 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem10 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->waypointerMenu = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem11 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem12 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem8 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuComboItem1 = (gcnew Telerik::WinControls::UI::RadMenuComboItem());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radToggleButtonWaypointer = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->object_2126a577_02be_4922_b078_be4cfcea14ab = (gcnew Telerik::WinControls::RootRadElement());
			this->object_6592ab6f_bcb9_4797_b57a_0225350bf213 = (gcnew Telerik::WinControls::RootRadElement());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointPageView))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointActionDropDownList))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointAddButton))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer2))->BeginInit();
			this->radSplitContainer2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel3))->BeginInit();
			this->splitPanel3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonZoomIn))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel4))->BeginInit();
			this->splitPanel4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonZoomOut))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer3))->BeginInit();
			this->radSplitContainer3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel5))->BeginInit();
			this->splitPanel5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonUpLevel))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel6))->BeginInit();
			this->splitPanel6->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonDownLevel))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radHideMiniMap))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelEdit))->BeginInit();
			this->panelEdit->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosoutheastNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radioeastNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox10))->BeginInit();
			this->radGroupBox10->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointPathLabelTextBox))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox11))->BeginInit();
			this->radGroupBox11->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointInfoLabelTextBox))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouthNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouthwestNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiocenterNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiowestNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorthwestNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionortheastNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorthNew))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_lure_reverse))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_automatic_lure))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_update_coord))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->checkBoxTopMost))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelMiniMap))->BeginInit();
			this->panelMiniMap->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->checkBoxShowOtherLevels))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radRepeatButton2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radRepeatButton1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelHudMode))->BeginInit();
			this->panelHudMode->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnlever))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosoutheast))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton11))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnuse))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnladder))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnmachete))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouthwest))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnwalk))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnrope))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radioeast))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnlua))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiocenter))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnshovel))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiowest))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnstand))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorthwest))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionortheast))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorth))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelMenu))->BeginInit();
			this->radPanelMenu->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenuComboItem1->ComboBoxElement))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonWaypointer))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// waypointPageView
			// 
			this->waypointPageView->Location = System::Drawing::Point(7, 30);
			this->waypointPageView->Name = L"waypointPageView";
			this->waypointPageView->Size = System::Drawing::Size(457, 380);
			this->waypointPageView->TabIndex = 0;
			this->waypointPageView->Text = L"Waypoint";
			this->waypointPageView->NewPageRequested += gcnew System::EventHandler(this, &Waypoint::waypointPageView_NewPageRequested);
			this->waypointPageView->PageRemoving += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewCancelEventArgs^ >(this, &Waypoint::waypointPageView_PageRemoving);
			this->waypointPageView->SelectedPageChanged += gcnew System::EventHandler(this, &Waypoint::waypointPageView_SelectedPageChanged);
			this->waypointPageView->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Waypoint::waypointPageContextMenuOpen);
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->waypointPageView->GetChildAt(0)))->NewItemVisibility = Telerik::WinControls::UI::StripViewNewItemVisibility::Front;
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->waypointPageView->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->waypointPageView->GetChildAt(0)))->ShowItemCloseButton = true;
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->waypointPageView->GetChildAt(0)))->ItemDragMode = Telerik::WinControls::UI::PageViewItemDragMode::Immediate;
			(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->waypointPageView->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
			// 
			// waypointPageContextMenu
			// 
			this->waypointPageContextMenu->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(4) {
				this->radMenuItem1,
					this->radMenuItem2, this->radMenuItem3, this->radMenuItem14
			});
			// 
			// radMenuItem1
			// 
			this->radMenuItem1->AccessibleDescription = L"New Waipoint ";
			this->radMenuItem1->AccessibleName = L"New Waipoint ";
			this->radMenuItem1->Name = L"radMenuItem1";
			this->radMenuItem1->Text = L"";
			this->radMenuItem1->Click += gcnew System::EventHandler(this, &Waypoint::waypointPageAddNewPage);
			// 
			// radMenuItem2
			// 
			this->radMenuItem2->AccessibleDescription = L"Load";
			this->radMenuItem2->AccessibleName = L"Load";
			this->radMenuItem2->Name = L"radMenuItem2";
			this->radMenuItem2->Text = L"Load";
			// 
			// radMenuItem3
			// 
			this->radMenuItem3->AccessibleDescription = L"Save";
			this->radMenuItem3->AccessibleName = L"Save";
			this->radMenuItem3->Name = L"radMenuItem3";
			this->radMenuItem3->Text = L"Save";
			// 
			// radMenuItem14
			// 
			this->radMenuItem14->AccessibleDescription = L"Add lure point for selected";
			this->radMenuItem14->AccessibleName = L"Add lure point for selected";
			this->radMenuItem14->Name = L"radMenuItem14";
			this->radMenuItem14->Text = L"Add lure point for selected";
			this->radMenuItem14->Click += gcnew System::EventHandler(this, &Waypoint::waypointLurePointFromMinimap);
			// 
			// waypointActionDropDownList
			// 
			this->waypointActionDropDownList->DefaultItemsCountInDropDown = 30;
			this->waypointActionDropDownList->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			this->waypointActionDropDownList->Location = System::Drawing::Point(9, 4);
			this->waypointActionDropDownList->Name = L"waypointActionDropDownList";
			this->waypointActionDropDownList->NullText = L"Waypoint Type";
			this->waypointActionDropDownList->ShowImageInEditorArea = false;
			this->waypointActionDropDownList->Size = System::Drawing::Size(556, 20);
			this->waypointActionDropDownList->TabIndex = 10;
			this->waypointActionDropDownList->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Waypoint::waypointActionDropDownList_SelectedIndexChanged);
			// 
			// waypointAddButton
			// 
			this->waypointAddButton->Location = System::Drawing::Point(551, 30);
			this->waypointAddButton->Name = L"waypointAddButton";
			this->waypointAddButton->Size = System::Drawing::Size(103, 25);
			this->waypointAddButton->TabIndex = 6;
			this->waypointAddButton->Text = L"New";
			this->waypointAddButton->Click += gcnew System::EventHandler(this, &Waypoint::waypointAddButtonClick);
			// 
			// waypointConfigureItem
			// 
			this->waypointConfigureItem->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(5) {
				this->radMenuItem6,
					this->radMenuItem4, this->radMenuItem30, this->radMenuItem13, this->radMenuItem5
			});
			// 
			// radMenuItem6
			// 
			this->radMenuItem6->AccessibleDescription = L"Set Current Waypoint";
			this->radMenuItem6->AccessibleName = L"Set Current Waypoint";
			this->radMenuItem6->Name = L"radMenuItem6";
			this->radMenuItem6->Text = L"Set Current Waypoint";
			this->radMenuItem6->Click += gcnew System::EventHandler(this, &Waypoint::waypointSetCurrentWaypointInfo);
			// 
			// radMenuItem4
			// 
			this->radMenuItem4->AccessibleDescription = L"Duplicate Up";
			this->radMenuItem4->AccessibleName = L"Duplicate Up";
			this->radMenuItem4->Name = L"radMenuItem4";
			this->radMenuItem4->Text = L"Duplicate Up";
			this->radMenuItem4->Click += gcnew System::EventHandler(this, &Waypoint::waypointInsert);
			// 
			// radMenuItem30
			// 
			this->radMenuItem30->AccessibleDescription = L"Duplicate Down";
			this->radMenuItem30->AccessibleName = L"Duplicate Down";
			this->radMenuItem30->Name = L"radMenuItem30";
			this->radMenuItem30->Text = L"Duplicate Down";
			this->radMenuItem30->Click += gcnew System::EventHandler(this, &Waypoint::waypointInsertDown);
			// 
			// radMenuItem13
			// 
			this->radMenuItem13->AccessibleDescription = L"Add Lure Point For Selected";
			this->radMenuItem13->AccessibleName = L"Add Lure Point For Selected";
			this->radMenuItem13->Name = L"radMenuItem13";
			this->radMenuItem13->Text = L"Add Lure Point For Selected";
			this->radMenuItem13->Click += gcnew System::EventHandler(this, &Waypoint::waypointAddLurePointForSelected);
			// 
			// radMenuItem5
			// 
			this->radMenuItem5->AccessibleDescription = L"Remove";
			this->radMenuItem5->AccessibleName = L"Remove";
			this->radMenuItem5->Name = L"radMenuItem5";
			this->radMenuItem5->Text = L"Remove";
			this->radMenuItem5->Click += gcnew System::EventHandler(this, &Waypoint::waypointRemove);
			// 
			// radLabel3
			// 
			this->radLabel3->Location = System::Drawing::Point(0, 0);
			this->radLabel3->Name = L"radLabel3";
			this->radLabel3->Size = System::Drawing::Size(2, 2);
			this->radLabel3->TabIndex = 21;
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 200;
			this->timer1->Tick += gcnew System::EventHandler(this, &Waypoint::update_current_waypoint);
			// 
			// radSplitContainer2
			// 
			this->radSplitContainer2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->radSplitContainer2->Controls->Add(this->splitPanel3);
			this->radSplitContainer2->Controls->Add(this->splitPanel4);
			this->radSplitContainer2->Location = System::Drawing::Point(427, 4);
			this->radSplitContainer2->Name = L"radSplitContainer2";
			this->radSplitContainer2->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// 
			// 
			this->radSplitContainer2->RootElement->ApplyShapeToControl = false;
			this->radSplitContainer2->RootElement->AutoSize = true;
			this->radSplitContainer2->RootElement->AutoSizeMode = Telerik::WinControls::RadAutoSizeMode::FitToAvailableSize;
			this->radSplitContainer2->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->radSplitContainer2->RootElement->Opacity = 1;
			this->radSplitContainer2->Size = System::Drawing::Size(26, 54);
			this->radSplitContainer2->SplitterWidth = 0;
			this->radSplitContainer2->TabIndex = 0;
			this->radSplitContainer2->TabStop = false;
			(cli::safe_cast<Telerik::WinControls::UI::SplitPanelElement^>(this->radSplitContainer2->GetChildAt(0)))->Opacity = 1;
			// 
			// splitPanel3
			// 
			this->splitPanel3->Controls->Add(this->buttonZoomIn);
			this->splitPanel3->Location = System::Drawing::Point(0, 0);
			this->splitPanel3->Name = L"splitPanel3";
			// 
			// 
			// 
			this->splitPanel3->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel3->Size = System::Drawing::Size(26, 28);
			this->splitPanel3->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.02173913F);
			this->splitPanel3->SizeInfo->SplitterCorrection = System::Drawing::Size(0, -1);
			this->splitPanel3->TabIndex = 0;
			this->splitPanel3->TabStop = false;
			this->splitPanel3->Text = L"+";
			// 
			// buttonZoomIn
			// 
			this->buttonZoomIn->Dock = System::Windows::Forms::DockStyle::Fill;
			this->buttonZoomIn->Location = System::Drawing::Point(0, 0);
			this->buttonZoomIn->Name = L"buttonZoomIn";
			// 
			// 
			// 
			this->buttonZoomIn->RootElement->Opacity = 0.8;
			this->buttonZoomIn->Size = System::Drawing::Size(26, 28);
			this->buttonZoomIn->TabIndex = 17;
			this->buttonZoomIn->Text = L"+";
			this->buttonZoomIn->Click += gcnew System::EventHandler(this, &Waypoint::buttonZoomIn_Click);
			(cli::safe_cast<Telerik::WinControls::UI::RadRepeatButtonElement^>(this->buttonZoomIn->GetChildAt(0)))->Text = L"+";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->buttonZoomIn->GetChildAt(0)->GetChildAt(0)))->Opacity = 1;
			// 
			// splitPanel4
			// 
			this->splitPanel4->Controls->Add(this->buttonZoomOut);
			this->splitPanel4->Location = System::Drawing::Point(0, 28);
			this->splitPanel4->Name = L"splitPanel4";
			// 
			// 
			// 
			this->splitPanel4->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel4->Size = System::Drawing::Size(26, 26);
			this->splitPanel4->SizeInfo->AutoSizeScale = System::Drawing::SizeF(0, 0.02173913F);
			this->splitPanel4->SizeInfo->SizeMode = Telerik::WinControls::UI::Docking::SplitPanelSizeMode::Relative;
			this->splitPanel4->SizeInfo->SplitterCorrection = System::Drawing::Size(0, 1);
			this->splitPanel4->TabIndex = 1;
			this->splitPanel4->TabStop = false;
			this->splitPanel4->Text = L"-";
			// 
			// buttonZoomOut
			// 
			this->buttonZoomOut->Dock = System::Windows::Forms::DockStyle::Fill;
			this->buttonZoomOut->Location = System::Drawing::Point(0, 0);
			this->buttonZoomOut->Name = L"buttonZoomOut";
			// 
			// 
			// 
			this->buttonZoomOut->RootElement->Opacity = 0.8;
			this->buttonZoomOut->Size = System::Drawing::Size(26, 26);
			this->buttonZoomOut->TabIndex = 17;
			this->buttonZoomOut->Text = L"-";
			this->buttonZoomOut->Click += gcnew System::EventHandler(this, &Waypoint::buttonZoomOut_Click);
			// 
			// radSplitContainer3
			// 
			this->radSplitContainer3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->radSplitContainer3->Controls->Add(this->splitPanel5);
			this->radSplitContainer3->Controls->Add(this->splitPanel6);
			this->radSplitContainer3->Location = System::Drawing::Point(456, 4);
			this->radSplitContainer3->Name = L"radSplitContainer3";
			this->radSplitContainer3->Orientation = System::Windows::Forms::Orientation::Horizontal;
			// 
			// 
			// 
			this->radSplitContainer3->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->radSplitContainer3->Size = System::Drawing::Size(27, 54);
			this->radSplitContainer3->SplitterWidth = 0;
			this->radSplitContainer3->TabIndex = 1;
			this->radSplitContainer3->TabStop = false;
			// 
			// splitPanel5
			// 
			this->splitPanel5->Controls->Add(this->buttonUpLevel);
			this->splitPanel5->Location = System::Drawing::Point(0, 0);
			this->splitPanel5->Name = L"splitPanel5";
			// 
			// 
			// 
			this->splitPanel5->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel5->Size = System::Drawing::Size(27, 27);
			this->splitPanel5->TabIndex = 0;
			this->splitPanel5->TabStop = false;
			this->splitPanel5->Text = L"^";
			// 
			// buttonUpLevel
			// 
			this->buttonUpLevel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->buttonUpLevel->Location = System::Drawing::Point(0, 0);
			this->buttonUpLevel->Name = L"buttonUpLevel";
			// 
			// 
			// 
			this->buttonUpLevel->RootElement->Opacity = 0.8;
			this->buttonUpLevel->Size = System::Drawing::Size(27, 27);
			this->buttonUpLevel->TabIndex = 16;
			this->buttonUpLevel->Text = L"^";
			this->buttonUpLevel->Click += gcnew System::EventHandler(this, &Waypoint::buttonUpLevel_Click);
			// 
			// splitPanel6
			// 
			this->splitPanel6->Controls->Add(this->buttonDownLevel);
			this->splitPanel6->Location = System::Drawing::Point(0, 27);
			this->splitPanel6->Name = L"splitPanel6";
			// 
			// 
			// 
			this->splitPanel6->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->splitPanel6->Size = System::Drawing::Size(27, 27);
			this->splitPanel6->TabIndex = 1;
			this->splitPanel6->TabStop = false;
			this->splitPanel6->Text = L"v";
			// 
			// buttonDownLevel
			// 
			this->buttonDownLevel->Dock = System::Windows::Forms::DockStyle::Fill;
			this->buttonDownLevel->Location = System::Drawing::Point(0, 0);
			this->buttonDownLevel->Name = L"buttonDownLevel";
			// 
			// 
			// 
			this->buttonDownLevel->RootElement->Opacity = 0.8;
			this->buttonDownLevel->Size = System::Drawing::Size(27, 27);
			this->buttonDownLevel->TabIndex = 17;
			this->buttonDownLevel->Text = L"v";
			this->buttonDownLevel->Click += gcnew System::EventHandler(this, &Waypoint::buttonDownLevel_Click);
			// 
			// radHideMiniMap
			// 
			this->radHideMiniMap->Location = System::Drawing::Point(571, 4);
			this->radHideMiniMap->Name = L"radHideMiniMap";
			this->radHideMiniMap->Size = System::Drawing::Size(83, 20);
			this->radHideMiniMap->TabIndex = 15;
			this->radHideMiniMap->Text = L"Hide Map";
			this->radHideMiniMap->Click += gcnew System::EventHandler(this, &Waypoint::radHideMiniMap_Click);
			// 
			// panelEdit
			// 
			this->panelEdit->BackColor = System::Drawing::Color::Transparent;
			this->panelEdit->Controls->Add(this->radiosoutheastNew);
			this->panelEdit->Controls->Add(this->radioeastNew);
			this->panelEdit->Controls->Add(this->radGroupBox10);
			this->panelEdit->Controls->Add(this->radiosouthNew);
			this->panelEdit->Controls->Add(this->radiosouthwestNew);
			this->panelEdit->Controls->Add(this->radiocenterNew);
			this->panelEdit->Controls->Add(this->radiowestNew);
			this->panelEdit->Controls->Add(this->radionorthwestNew);
			this->panelEdit->Controls->Add(this->radionortheastNew);
			this->panelEdit->Controls->Add(this->radionorthNew);
			this->panelEdit->Controls->Add(this->check_lure_reverse);
			this->panelEdit->Controls->Add(this->check_automatic_lure);
			this->panelEdit->Controls->Add(this->bt_update_coord);
			this->panelEdit->Controls->Add(this->checkBoxTopMost);
			this->panelEdit->Controls->Add(this->waypointActionDropDownList);
			this->panelEdit->Controls->Add(this->waypointPageView);
			this->panelEdit->Controls->Add(this->radHideMiniMap);
			this->panelEdit->Controls->Add(this->waypointAddButton);
			this->panelEdit->Location = System::Drawing::Point(0, 27);
			this->panelEdit->Name = L"panelEdit";
			this->panelEdit->Size = System::Drawing::Size(654, 437);
			this->panelEdit->TabIndex = 18;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->panelEdit->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panelEdit->GetChildAt(0)->GetChildAt(1)))->Width = 0;
			// 
			// radiosoutheastNew
			// 
			this->radiosoutheastNew->Enabled = false;
			this->radiosoutheastNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiosoutheastNew->ImageKey = L"arrow_2";
			this->radiosoutheastNew->ImageList = this->imageList1;
			this->radiosoutheastNew->Location = System::Drawing::Point(520, 80);
			this->radiosoutheastNew->Name = L"radiosoutheastNew";
			// 
			// 
			// 
			this->radiosoutheastNew->RootElement->AngleTransform = 0;
			this->radiosoutheastNew->Size = System::Drawing::Size(25, 25);
			this->radiosoutheastNew->TabIndex = 38;
			this->radiosoutheastNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosoutheastNew->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosoutheastNew->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosoutheastNew->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radiosoutheastNew->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 225;
			// 
			// imageList1
			// 
			this->imageList1->ImageStream = (cli::safe_cast<System::Windows::Forms::ImageListStreamer^>(resources->GetObject(L"imageList1.ImageStream")));
			this->imageList1->TransparentColor = System::Drawing::Color::Transparent;
			this->imageList1->Images->SetKeyName(0, L"arrow_1");
			this->imageList1->Images->SetKeyName(1, L"arrow_2");
			this->imageList1->Images->SetKeyName(2, L"arrow_3");
			this->imageList1->Images->SetKeyName(3, L"circle_1");
			this->imageList1->Images->SetKeyName(4, L"circle_2");
			this->imageList1->Images->SetKeyName(5, L"circle_3");
			// 
			// radioeastNew
			// 
			this->radioeastNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radioeastNew->ImageKey = L"arrow_2";
			this->radioeastNew->ImageList = this->imageList1;
			this->radioeastNew->Location = System::Drawing::Point(520, 55);
			this->radioeastNew->Name = L"radioeastNew";
			// 
			// 
			// 
			this->radioeastNew->RootElement->AngleTransform = 0;
			this->radioeastNew->Size = System::Drawing::Size(25, 25);
			this->radioeastNew->TabIndex = 35;
			this->radioeastNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radio_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radioeastNew->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image1")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radioeastNew->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radioeastNew->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radioeastNew->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 180;
			// 
			// radGroupBox10
			// 
			this->radGroupBox10->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox10->Controls->Add(this->waypointPathLabelTextBox);
			this->radGroupBox10->Controls->Add(this->radGroupBox11);
			this->radGroupBox10->HeaderText = L"Waypoint Path";
			this->radGroupBox10->Location = System::Drawing::Point(464, 105);
			this->radGroupBox10->Name = L"radGroupBox10";
			this->radGroupBox10->Size = System::Drawing::Size(186, 332);
			this->radGroupBox10->TabIndex = 23;
			this->radGroupBox10->Text = L"Waypoint Path";
			// 
			// waypointPathLabelTextBox
			// 
			this->waypointPathLabelTextBox->Location = System::Drawing::Point(6, 18);
			this->waypointPathLabelTextBox->Name = L"waypointPathLabelTextBox";
			this->waypointPathLabelTextBox->NullText = L"Label Name";
			this->waypointPathLabelTextBox->Size = System::Drawing::Size(175, 20);
			this->waypointPathLabelTextBox->TabIndex = 3;
			this->waypointPathLabelTextBox->TextChanged += gcnew System::EventHandler(this, &Waypoint::waypointPathLabelTextBox_TextChanged);
			// 
			// radGroupBox11
			// 
			this->radGroupBox11->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox11->Controls->Add(this->waypointInfoLabelTextBox);
			this->radGroupBox11->Controls->Add(this->radGroupBox2);
			this->radGroupBox11->HeaderText = L"Waypoint Info";
			this->radGroupBox11->Location = System::Drawing::Point(0, 40);
			this->radGroupBox11->Name = L"radGroupBox11";
			this->radGroupBox11->Size = System::Drawing::Size(186, 292);
			this->radGroupBox11->TabIndex = 1;
			this->radGroupBox11->Text = L"Waypoint Info";
			// 
			// waypointInfoLabelTextBox
			// 
			this->waypointInfoLabelTextBox->Location = System::Drawing::Point(6, 19);
			this->waypointInfoLabelTextBox->Name = L"waypointInfoLabelTextBox";
			this->waypointInfoLabelTextBox->NullText = L"Label Name";
			this->waypointInfoLabelTextBox->Size = System::Drawing::Size(175, 20);
			this->waypointInfoLabelTextBox->TabIndex = 0;
			this->waypointInfoLabelTextBox->TextChanged += gcnew System::EventHandler(this, &Waypoint::waypointInfoLabelTextBox_TextChanged);
			// 
			// radGroupBox2
			// 
			this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox2->HeaderText = L"Action Config";
			this->radGroupBox2->Location = System::Drawing::Point(0, 45);
			this->radGroupBox2->Name = L"radGroupBox2";
			this->radGroupBox2->Size = System::Drawing::Size(186, 247);
			this->radGroupBox2->TabIndex = 2;
			this->radGroupBox2->Text = L"Action Config";
			// 
			// radiosouthNew
			// 
			this->radiosouthNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiosouthNew->ImageKey = L"arrow_2";
			this->radiosouthNew->ImageList = this->imageList1;
			this->radiosouthNew->Location = System::Drawing::Point(495, 80);
			this->radiosouthNew->Name = L"radiosouthNew";
			this->radiosouthNew->Size = System::Drawing::Size(25, 25);
			this->radiosouthNew->TabIndex = 37;
			this->radiosouthNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radio_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthNew->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image2")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthNew->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthNew->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radiosouthNew->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 270;
			// 
			// radiosouthwestNew
			// 
			this->radiosouthwestNew->Enabled = false;
			this->radiosouthwestNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiosouthwestNew->ImageKey = L"arrow_2";
			this->radiosouthwestNew->ImageList = this->imageList1;
			this->radiosouthwestNew->Location = System::Drawing::Point(470, 80);
			this->radiosouthwestNew->Name = L"radiosouthwestNew";
			this->radiosouthwestNew->Size = System::Drawing::Size(25, 25);
			this->radiosouthwestNew->TabIndex = 36;
			this->radiosouthwestNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthwestNew->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image3")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthwestNew->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthwestNew->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radiosouthwestNew->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 315;
			// 
			// radiocenterNew
			// 
			this->radiocenterNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiocenterNew->ImageList = this->imageList1;
			this->radiocenterNew->Location = System::Drawing::Point(495, 55);
			this->radiocenterNew->Name = L"radiocenterNew";
			this->radiocenterNew->Size = System::Drawing::Size(25, 25);
			this->radiocenterNew->TabIndex = 34;
			this->radiocenterNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radio_ToggleStateChanged);
			// 
			// radiowestNew
			// 
			this->radiowestNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiowestNew->ImageKey = L"arrow_2";
			this->radiowestNew->ImageList = this->imageList1;
			this->radiowestNew->Location = System::Drawing::Point(470, 55);
			this->radiowestNew->Name = L"radiowestNew";
			this->radiowestNew->Size = System::Drawing::Size(25, 25);
			this->radiowestNew->TabIndex = 33;
			this->radiowestNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radio_ToggleStateChanged);
			// 
			// radionorthwestNew
			// 
			this->radionorthwestNew->Enabled = false;
			this->radionorthwestNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radionorthwestNew->ImageKey = L"arrow_2";
			this->radionorthwestNew->ImageList = this->imageList1;
			this->radionorthwestNew->Location = System::Drawing::Point(470, 30);
			this->radionorthwestNew->Name = L"radionorthwestNew";
			this->radionorthwestNew->Size = System::Drawing::Size(25, 25);
			this->radionorthwestNew->TabIndex = 30;
			this->radionorthwestNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthwestNew->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image4")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthwestNew->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthwestNew->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radionorthwestNew->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 45;
			// 
			// radionortheastNew
			// 
			this->radionortheastNew->Enabled = false;
			this->radionortheastNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radionortheastNew->ImageKey = L"arrow_2";
			this->radionortheastNew->ImageList = this->imageList1;
			this->radionortheastNew->Location = System::Drawing::Point(520, 30);
			this->radionortheastNew->Name = L"radionortheastNew";
			this->radionortheastNew->Size = System::Drawing::Size(25, 25);
			this->radionortheastNew->TabIndex = 32;
			this->radionortheastNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionortheastNew->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image5")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionortheastNew->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionortheastNew->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radionortheastNew->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 135;
			// 
			// radionorthNew
			// 
			this->radionorthNew->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radionorthNew->ImageKey = L"arrow_2";
			this->radionorthNew->ImageList = this->imageList1;
			this->radionorthNew->Location = System::Drawing::Point(495, 30);
			this->radionorthNew->Name = L"radionorthNew";
			this->radionorthNew->Size = System::Drawing::Size(25, 25);
			this->radionorthNew->TabIndex = 31;
			this->radionorthNew->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radio_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthNew->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image6")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthNew->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthNew->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radionorthNew->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 90;
			// 
			// check_lure_reverse
			// 
			this->check_lure_reverse->Enabled = false;
			this->check_lure_reverse->Location = System::Drawing::Point(212, 416);
			this->check_lure_reverse->Name = L"check_lure_reverse";
			this->check_lure_reverse->Size = System::Drawing::Size(84, 18);
			this->check_lure_reverse->TabIndex = 21;
			this->check_lure_reverse->Text = L"Lure Reverse";
			this->check_lure_reverse->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::check_lure_reverse_ToggleStateChanged);
			// 
			// check_automatic_lure
			// 
			this->check_automatic_lure->Location = System::Drawing::Point(100, 416);
			this->check_automatic_lure->Name = L"check_automatic_lure";
			this->check_automatic_lure->Size = System::Drawing::Size(96, 18);
			this->check_automatic_lure->TabIndex = 20;
			this->check_automatic_lure->Text = L"Automatic Lure";
			this->check_automatic_lure->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::check_automatic_lure_ToggleStateChanged);
			// 
			// bt_update_coord
			// 
			this->bt_update_coord->Location = System::Drawing::Point(313, 413);
			this->bt_update_coord->Name = L"bt_update_coord";
			this->bt_update_coord->Size = System::Drawing::Size(145, 24);
			this->bt_update_coord->TabIndex = 7;
			this->bt_update_coord->Text = L"Update Coordinate";
			this->bt_update_coord->Click += gcnew System::EventHandler(this, &Waypoint::bt_update_coord_Click);
			// 
			// checkBoxTopMost
			// 
			this->checkBoxTopMost->Location = System::Drawing::Point(9, 416);
			this->checkBoxTopMost->Name = L"checkBoxTopMost";
			this->checkBoxTopMost->Size = System::Drawing::Size(68, 18);
			this->checkBoxTopMost->TabIndex = 18;
			this->checkBoxTopMost->Text = L"Top Most";
			this->checkBoxTopMost->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::checkBoxTopMost_CheckedChanged);
			// 
			// panelMiniMap
			// 
			this->panelMiniMap->Controls->Add(this->checkBoxShowOtherLevels);
			this->panelMiniMap->Controls->Add(this->radSplitContainer2);
			this->panelMiniMap->Controls->Add(this->radRepeatButton2);
			this->panelMiniMap->Controls->Add(this->radRepeatButton1);
			this->panelMiniMap->Controls->Add(this->radSplitContainer3);
			this->panelMiniMap->Controls->Add(this->miniMapControl1);
			this->panelMiniMap->Location = System::Drawing::Point(656, 30);
			this->panelMiniMap->Name = L"panelMiniMap";
			this->panelMiniMap->Size = System::Drawing::Size(492, 420);
			this->panelMiniMap->TabIndex = 19;
			// 
			// checkBoxShowOtherLevels
			// 
			this->checkBoxShowOtherLevels->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->checkBoxShowOtherLevels->BackColor = System::Drawing::Color::Transparent;
			this->checkBoxShowOtherLevels->Location = System::Drawing::Point(3, 399);
			this->checkBoxShowOtherLevels->Name = L"checkBoxShowOtherLevels";
			this->checkBoxShowOtherLevels->Size = System::Drawing::Size(123, 18);
			this->checkBoxShowOtherLevels->TabIndex = 20;
			this->checkBoxShowOtherLevels->Text = L"Show another floors.";
			this->checkBoxShowOtherLevels->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::checkBoxShowOtherLevels_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadCheckBoxElement^>(this->checkBoxShowOtherLevels->GetChildAt(0)))->Text = L"Show another floors.";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->checkBoxShowOtherLevels->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			// 
			// radRepeatButton2
			// 
			this->radRepeatButton2->Location = System::Drawing::Point(1, 31);
			this->radRepeatButton2->Name = L"radRepeatButton2";
			// 
			// 
			// 
			this->radRepeatButton2->RootElement->Opacity = 0.8;
			this->radRepeatButton2->Size = System::Drawing::Size(144, 24);
			this->radRepeatButton2->TabIndex = 19;
			this->radRepeatButton2->Text = L"show current coordinate";
			this->radRepeatButton2->Click += gcnew System::EventHandler(this, &Waypoint::radRepeatButton2_Click);
			(cli::safe_cast<Telerik::WinControls::UI::RadRepeatButtonElement^>(this->radRepeatButton2->GetChildAt(0)))->Text = L"show current coordinate";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->radRepeatButton2->GetChildAt(0)->GetChildAt(0)))->Opacity = 1;
			// 
			// radRepeatButton1
			// 
			this->radRepeatButton1->Location = System::Drawing::Point(1, 3);
			this->radRepeatButton1->Name = L"radRepeatButton1";
			// 
			// 
			// 
			this->radRepeatButton1->RootElement->Opacity = 0.8;
			this->radRepeatButton1->Size = System::Drawing::Size(144, 24);
			this->radRepeatButton1->TabIndex = 18;
			this->radRepeatButton1->Text = L"show selected";
			this->radRepeatButton1->Click += gcnew System::EventHandler(this, &Waypoint::radRepeatButton1_Click);
			(cli::safe_cast<Telerik::WinControls::UI::RadRepeatButtonElement^>(this->radRepeatButton1->GetChildAt(0)))->Text = L"show selected";
			(cli::safe_cast<Telerik::WinControls::UI::RadRepeatButtonElement^>(this->radRepeatButton1->GetChildAt(0)))->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(100)),
				static_cast<System::Int32>(static_cast<System::Byte>(191)), static_cast<System::Int32>(static_cast<System::Byte>(219)), static_cast<System::Int32>(static_cast<System::Byte>(255)));
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->radRepeatButton1->GetChildAt(0)->GetChildAt(0)))->Opacity = 1;
			// 
			// miniMapControl1
			// 
			this->miniMapControl1->cursor_image = nullptr;
			this->miniMapControl1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->miniMapControl1->dragingWaypoint = nullptr;
			this->miniMapControl1->hoveredWaypoint = nullptr;
			this->miniMapControl1->Location = System::Drawing::Point(0, 0);
			this->miniMapControl1->Name = L"miniMapControl1";
			this->miniMapControl1->selected_waypoint_image = nullptr;
			this->miniMapControl1->selectedWaypoint = nullptr;
			this->miniMapControl1->Size = System::Drawing::Size(492, 420);
			this->miniMapControl1->TabIndex = 9;
			this->miniMapControl1->waypoint_image = nullptr;
			this->miniMapControl1->onCoordinateMouseClick += gcnew MiniMap::delegateCoordinateClick(this, &Waypoint::miniMapControl1_onCoordinateMouseClick);
			// 
			// panelHudMode
			// 
			this->panelHudMode->Controls->Add(this->btnlever);
			this->panelHudMode->Controls->Add(this->radiosoutheast);
			this->panelHudMode->Controls->Add(this->radButton11);
			this->panelHudMode->Controls->Add(this->btnuse);
			this->panelHudMode->Controls->Add(this->radiosouth);
			this->panelHudMode->Controls->Add(this->btnladder);
			this->panelHudMode->Controls->Add(this->btnmachete);
			this->panelHudMode->Controls->Add(this->radiosouthwest);
			this->panelHudMode->Controls->Add(this->btnwalk);
			this->panelHudMode->Controls->Add(this->btnrope);
			this->panelHudMode->Controls->Add(this->radioeast);
			this->panelHudMode->Controls->Add(this->btnlua);
			this->panelHudMode->Controls->Add(this->radiocenter);
			this->panelHudMode->Controls->Add(this->btnshovel);
			this->panelHudMode->Controls->Add(this->radiowest);
			this->panelHudMode->Controls->Add(this->btnstand);
			this->panelHudMode->Controls->Add(this->radionorthwest);
			this->panelHudMode->Controls->Add(this->radionortheast);
			this->panelHudMode->Controls->Add(this->radionorth);
			this->panelHudMode->ForeColor = System::Drawing::Color::Transparent;
			this->panelHudMode->Location = System::Drawing::Point(1154, 27);
			this->panelHudMode->Name = L"panelHudMode";
			this->panelHudMode->Size = System::Drawing::Size(119, 395);
			this->panelHudMode->TabIndex = 20;
			this->panelHudMode->Visible = false;
			(cli::safe_cast<Telerik::WinControls::UI::RadPanelElement^>(this->panelHudMode->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panelHudMode->GetChildAt(0)->GetChildAt(1)))->Width = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panelHudMode->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Red;
			// 
			// btnlever
			// 
			this->btnlever->Location = System::Drawing::Point(2, 339);
			this->btnlever->Name = L"btnlever";
			this->btnlever->Size = System::Drawing::Size(115, 26);
			this->btnlever->TabIndex = 28;
			this->btnlever->Text = L"LEVER";
			this->btnlever->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// radiosoutheast
			// 
			this->radiosoutheast->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiosoutheast->ImageKey = L"arrow_2";
			this->radiosoutheast->ImageList = this->imageList1;
			this->radiosoutheast->Location = System::Drawing::Point(79, 79);
			this->radiosoutheast->Name = L"radiosoutheast";
			// 
			// 
			// 
			this->radiosoutheast->RootElement->AngleTransform = 0;
			this->radiosoutheast->Size = System::Drawing::Size(38, 38);
			this->radiosoutheast->TabIndex = 29;
			this->radiosoutheast->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosoutheast->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image7")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosoutheast->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosoutheast->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radiosoutheast->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 225;
			// 
			// radButton11
			// 
			this->radButton11->Location = System::Drawing::Point(2, 367);
			this->radButton11->Name = L"radButton11";
			this->radButton11->Size = System::Drawing::Size(115, 24);
			this->radButton11->TabIndex = 28;
			this->radButton11->Text = L"MODE";
			this->radButton11->Click += gcnew System::EventHandler(this, &Waypoint::radButton11_Click);
			// 
			// btnuse
			// 
			this->btnuse->Location = System::Drawing::Point(2, 312);
			this->btnuse->Name = L"btnuse";
			this->btnuse->Size = System::Drawing::Size(115, 26);
			this->btnuse->TabIndex = 27;
			this->btnuse->Text = L"USE";
			this->btnuse->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// radiosouth
			// 
			this->radiosouth->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiosouth->ImageKey = L"arrow_2";
			this->radiosouth->ImageList = this->imageList1;
			this->radiosouth->Location = System::Drawing::Point(41, 79);
			this->radiosouth->Name = L"radiosouth";
			this->radiosouth->Size = System::Drawing::Size(38, 38);
			this->radiosouth->TabIndex = 28;
			this->radiosouth->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouth->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image8")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouth->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouth->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radiosouth->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 270;
			// 
			// btnladder
			// 
			this->btnladder->Location = System::Drawing::Point(2, 285);
			this->btnladder->Name = L"btnladder";
			this->btnladder->Size = System::Drawing::Size(115, 26);
			this->btnladder->TabIndex = 26;
			this->btnladder->Text = L"LADDER";
			this->btnladder->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// btnmachete
			// 
			this->btnmachete->Location = System::Drawing::Point(2, 258);
			this->btnmachete->Name = L"btnmachete";
			this->btnmachete->Size = System::Drawing::Size(115, 26);
			this->btnmachete->TabIndex = 25;
			this->btnmachete->Text = L"MACHETE";
			this->btnmachete->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// radiosouthwest
			// 
			this->radiosouthwest->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiosouthwest->ImageKey = L"arrow_2";
			this->radiosouthwest->ImageList = this->imageList1;
			this->radiosouthwest->Location = System::Drawing::Point(3, 79);
			this->radiosouthwest->Name = L"radiosouthwest";
			this->radiosouthwest->Size = System::Drawing::Size(38, 38);
			this->radiosouthwest->TabIndex = 27;
			this->radiosouthwest->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthwest->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image9")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthwest->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radiosouthwest->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radiosouthwest->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 315;
			// 
			// btnwalk
			// 
			this->btnwalk->Location = System::Drawing::Point(2, 123);
			this->btnwalk->Name = L"btnwalk";
			this->btnwalk->Size = System::Drawing::Size(115, 26);
			this->btnwalk->TabIndex = 21;
			this->btnwalk->Text = L"WALK";
			this->btnwalk->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// btnrope
			// 
			this->btnrope->Location = System::Drawing::Point(2, 231);
			this->btnrope->Name = L"btnrope";
			this->btnrope->Size = System::Drawing::Size(115, 26);
			this->btnrope->TabIndex = 24;
			this->btnrope->Text = L"ROPE";
			this->btnrope->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// radioeast
			// 
			this->radioeast->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radioeast->ImageKey = L"arrow_2";
			this->radioeast->ImageList = this->imageList1;
			this->radioeast->Location = System::Drawing::Point(79, 41);
			this->radioeast->Name = L"radioeast";
			// 
			// 
			// 
			this->radioeast->RootElement->AngleTransform = 0;
			this->radioeast->Size = System::Drawing::Size(38, 38);
			this->radioeast->TabIndex = 26;
			this->radioeast->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radioeast->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image10")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radioeast->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radioeast->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radioeast->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 180;
			// 
			// btnlua
			// 
			this->btnlua->Location = System::Drawing::Point(2, 150);
			this->btnlua->Name = L"btnlua";
			this->btnlua->Size = System::Drawing::Size(115, 26);
			this->btnlua->TabIndex = 22;
			this->btnlua->Text = L"LUA";
			this->btnlua->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// radiocenter
			// 
			this->radiocenter->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiocenter->ImageList = this->imageList1;
			this->radiocenter->Location = System::Drawing::Point(41, 41);
			this->radiocenter->Name = L"radiocenter";
			this->radiocenter->Size = System::Drawing::Size(38, 38);
			this->radiocenter->TabIndex = 25;
			this->radiocenter->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			// 
			// btnshovel
			// 
			this->btnshovel->Location = System::Drawing::Point(2, 204);
			this->btnshovel->Name = L"btnshovel";
			this->btnshovel->Size = System::Drawing::Size(115, 26);
			this->btnshovel->TabIndex = 23;
			this->btnshovel->Text = L"SHOVEL";
			this->btnshovel->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// radiowest
			// 
			this->radiowest->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radiowest->ImageKey = L"arrow_2";
			this->radiowest->ImageList = this->imageList1;
			this->radiowest->Location = System::Drawing::Point(3, 41);
			this->radiowest->Name = L"radiowest";
			this->radiowest->Size = System::Drawing::Size(38, 38);
			this->radiowest->TabIndex = 24;
			this->radiowest->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			// 
			// btnstand
			// 
			this->btnstand->Location = System::Drawing::Point(2, 177);
			this->btnstand->Name = L"btnstand";
			this->btnstand->Size = System::Drawing::Size(115, 26);
			this->btnstand->TabIndex = 22;
			this->btnstand->Text = L"STAND";
			this->btnstand->Click += gcnew System::EventHandler(this, &Waypoint::btn_on_add_hud);
			// 
			// radionorthwest
			// 
			this->radionorthwest->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radionorthwest->ImageKey = L"arrow_2";
			this->radionorthwest->ImageList = this->imageList1;
			this->radionorthwest->Location = System::Drawing::Point(3, 3);
			this->radionorthwest->Name = L"radionorthwest";
			this->radionorthwest->Size = System::Drawing::Size(38, 38);
			this->radionorthwest->TabIndex = 21;
			this->radionorthwest->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthwest->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image11")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthwest->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorthwest->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radionorthwest->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 45;
			// 
			// radionortheast
			// 
			this->radionortheast->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radionortheast->ImageKey = L"arrow_2";
			this->radionortheast->ImageList = this->imageList1;
			this->radionortheast->Location = System::Drawing::Point(79, 3);
			this->radionortheast->Name = L"radionortheast";
			this->radionortheast->Size = System::Drawing::Size(38, 38);
			this->radionortheast->TabIndex = 23;
			this->radionortheast->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionortheast->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image12")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionortheast->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionortheast->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radionortheast->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 135;
			// 
			// radionorth
			// 
			this->radionorth->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radionorth->ImageKey = L"arrow_2";
			this->radionorth->ImageList = this->imageList1;
			this->radionorth->Location = System::Drawing::Point(41, 3);
			this->radionorth->Name = L"radionorth";
			this->radionorth->Size = System::Drawing::Size(38, 38);
			this->radionorth->TabIndex = 22;
			this->radionorth->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Waypoint::radionortheast_ToggleStateChanged);
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorth->GetChildAt(0)))->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"resource.Image13")));
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorth->GetChildAt(0)))->ImageKey = L"arrow_2";
			(cli::safe_cast<Telerik::WinControls::UI::RadToggleButtonElement^>(this->radionorth->GetChildAt(0)))->ImageAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radionorth->GetChildAt(0)->GetChildAt(1)->GetChildAt(0)))->AngleTransform = 90;
			// 
			// radPanelMenu
			// 
			this->radPanelMenu->Controls->Add(this->radPanel1);
			this->radPanelMenu->Controls->Add(this->radPanel2);
			this->radPanelMenu->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanelMenu->Location = System::Drawing::Point(0, 0);
			this->radPanelMenu->Name = L"radPanelMenu";
			this->radPanelMenu->Size = System::Drawing::Size(1271, 22);
			this->radPanelMenu->TabIndex = 22;
			// 
			// radPanel1
			// 
			this->radPanel1->Controls->Add(this->radMenu1);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel1->Location = System::Drawing::Point(100, 0);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(1171, 22);
			this->radPanel1->TabIndex = 0;
			// 
			// radMenu1
			// 
			this->radMenu1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
				this->radMenuItem7, this->radMenuItem8,
					this->radMenuComboItem1
			});
			this->radMenu1->Location = System::Drawing::Point(0, 0);
			this->radMenu1->Name = L"radMenu1";
			this->radMenu1->Size = System::Drawing::Size(1171, 22);
			this->radMenu1->TabIndex = 0;
			this->radMenu1->Visible = false;
			// 
			// radMenuItem7
			// 
			this->radMenuItem7->AccessibleDescription = L"Menu";
			this->radMenuItem7->AccessibleName = L"Menu";
			this->radMenuItem7->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->pathMenu, this->waypointerMenu });
			this->radMenuItem7->Name = L"radMenuItem7";
			this->radMenuItem7->Text = L"Menu";
			this->radMenuItem7->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			// 
			// pathMenu
			// 
			this->pathMenu->AccessibleDescription = L"pathMenu";
			this->pathMenu->AccessibleName = L"pathMenu";
			this->pathMenu->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->radMenuItem9, this->radMenuItem10 });
			this->pathMenu->Name = L"pathMenu";
			this->pathMenu->Text = L"Path";
			this->pathMenu->Click += gcnew System::EventHandler(this, &Waypoint::pathMenu_Click);
			// 
			// radMenuItem9
			// 
			this->radMenuItem9->AccessibleDescription = L"Save";
			this->radMenuItem9->AccessibleName = L"Save";
			this->radMenuItem9->Name = L"radMenuItem9";
			this->radMenuItem9->Text = L"Save";
			this->radMenuItem9->Click += gcnew System::EventHandler(this, &Waypoint::buttonSavePath_Click);
			// 
			// radMenuItem10
			// 
			this->radMenuItem10->AccessibleDescription = L"Load";
			this->radMenuItem10->AccessibleName = L"Load";
			this->radMenuItem10->Name = L"radMenuItem10";
			this->radMenuItem10->Text = L"Load";
			this->radMenuItem10->Click += gcnew System::EventHandler(this, &Waypoint::buttonLoadPath_Click);
			// 
			// waypointerMenu
			// 
			this->waypointerMenu->AccessibleDescription = L"waypointerMenu";
			this->waypointerMenu->AccessibleName = L"waypointerMenu";
			this->waypointerMenu->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->radMenuItem11, this->radMenuItem12 });
			this->waypointerMenu->Name = L"waypointerMenu";
			this->waypointerMenu->Text = L"Waypointer";
			this->waypointerMenu->Click += gcnew System::EventHandler(this, &Waypoint::radMenuItem9_Click);
			// 
			// radMenuItem11
			// 
			this->radMenuItem11->AccessibleDescription = L"Save";
			this->radMenuItem11->AccessibleName = L"Save";
			this->radMenuItem11->Name = L"radMenuItem11";
			this->radMenuItem11->Text = L"Save";
			this->radMenuItem11->Click += gcnew System::EventHandler(this, &Waypoint::buttonSaveWaypointer_Click);
			// 
			// radMenuItem12
			// 
			this->radMenuItem12->AccessibleDescription = L"Load";
			this->radMenuItem12->AccessibleName = L"Load";
			this->radMenuItem12->Name = L"radMenuItem12";
			this->radMenuItem12->Text = L"Load";
			this->radMenuItem12->Click += gcnew System::EventHandler(this, &Waypoint::buttonLoadWaypointer_Click);
			// 
			// radMenuItem8
			// 
			this->radMenuItem8->AccessibleDescription = L"State";
			this->radMenuItem8->AccessibleName = L"State";
			this->radMenuItem8->Alignment = System::Drawing::ContentAlignment::TopLeft;
			this->radMenuItem8->CheckOnClick = true;
			this->radMenuItem8->ClickMode = Telerik::WinControls::ClickMode::Release;
			this->radMenuItem8->ImageAlignment = System::Drawing::ContentAlignment::MiddleLeft;
			this->radMenuItem8->Name = L"radMenuItem8";
			this->radMenuItem8->Text = L"State";
			this->radMenuItem8->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			// 
			// radMenuComboItem1
			// 
			this->radMenuComboItem1->AccessibleDescription = L"Edition Mode";
			this->radMenuComboItem1->AccessibleName = L"Edition Mode";
			this->radMenuComboItem1->AutoSize = false;
			this->radMenuComboItem1->Bounds = System::Drawing::Rectangle(0, 0, 150, 22);
			// 
			// 
			// 
			this->radMenuComboItem1->ComboBoxElement->ArrowButtonMinWidth = 17;
			this->radMenuComboItem1->ComboBoxElement->AutoCompleteAppend = nullptr;
			this->radMenuComboItem1->ComboBoxElement->AutoCompleteDataSource = nullptr;
			this->radMenuComboItem1->ComboBoxElement->AutoCompleteSuggest = nullptr;
			this->radMenuComboItem1->ComboBoxElement->DataMember = L"";
			this->radMenuComboItem1->ComboBoxElement->DataSource = nullptr;
			this->radMenuComboItem1->ComboBoxElement->DefaultValue = nullptr;
			this->radMenuComboItem1->ComboBoxElement->DisplayMember = L"";
			this->radMenuComboItem1->ComboBoxElement->DropDownAnimationEasing = Telerik::WinControls::RadEasingType::InQuad;
			this->radMenuComboItem1->ComboBoxElement->DropDownAnimationEnabled = true;
			this->radMenuComboItem1->ComboBoxElement->EditableElementText = L"";
			this->radMenuComboItem1->ComboBoxElement->EditorElement = this->radMenuComboItem1->ComboBoxElement;
			this->radMenuComboItem1->ComboBoxElement->EditorManager = nullptr;
			this->radMenuComboItem1->ComboBoxElement->Filter = nullptr;
			this->radMenuComboItem1->ComboBoxElement->FilterExpression = L"";
			this->radMenuComboItem1->ComboBoxElement->Focusable = true;
			this->radMenuComboItem1->ComboBoxElement->FormatString = L"";
			this->radMenuComboItem1->ComboBoxElement->FormattingEnabled = true;
			this->radMenuComboItem1->ComboBoxElement->ItemHeight = 18;
			radListDataItem1->Text = L"Insert before edit";
			radListDataItem2->Text = L"Edit before insert";
			radListDataItem3->Text = L"MiniHud";
			radListDataItem4->Text = L"Minihud + Map";
			this->radMenuComboItem1->ComboBoxElement->Items->Add(radListDataItem1);
			this->radMenuComboItem1->ComboBoxElement->Items->Add(radListDataItem2);
			this->radMenuComboItem1->ComboBoxElement->Items->Add(radListDataItem3);
			this->radMenuComboItem1->ComboBoxElement->Items->Add(radListDataItem4);
			this->radMenuComboItem1->ComboBoxElement->MaxDropDownItems = 0;
			this->radMenuComboItem1->ComboBoxElement->MaxLength = 32767;
			this->radMenuComboItem1->ComboBoxElement->MaxValue = nullptr;
			this->radMenuComboItem1->ComboBoxElement->MinValue = nullptr;
			this->radMenuComboItem1->ComboBoxElement->NullText = L"Edition Mode";
			this->radMenuComboItem1->ComboBoxElement->NullValue = nullptr;
			this->radMenuComboItem1->ComboBoxElement->OwnerOffset = 0;
			this->radMenuComboItem1->ComboBoxElement->ShowImageInEditorArea = true;
			this->radMenuComboItem1->ComboBoxElement->SortStyle = Telerik::WinControls::Enumerations::SortStyle::None;
			this->radMenuComboItem1->ComboBoxElement->Value = nullptr;
			this->radMenuComboItem1->ComboBoxElement->ValueMember = L"";
			this->radMenuComboItem1->ComboBoxElement->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Waypoint::dropDownEditionMode_SelectedIndexChanged);
			this->radMenuComboItem1->Name = L"radMenuComboItem1";
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radToggleButtonWaypointer);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Left;
			this->radPanel2->Location = System::Drawing::Point(0, 0);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(100, 22);
			this->radPanel2->TabIndex = 22;
			// 
			// radToggleButtonWaypointer
			// 
			this->radToggleButtonWaypointer->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radToggleButtonWaypointer->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radToggleButtonWaypointer.Image")));
			this->radToggleButtonWaypointer->Location = System::Drawing::Point(0, 0);
			this->radToggleButtonWaypointer->Name = L"radToggleButtonWaypointer";
			this->radToggleButtonWaypointer->Size = System::Drawing::Size(100, 22);
			this->radToggleButtonWaypointer->TabIndex = 21;
			this->radToggleButtonWaypointer->Text = L"Status";
			// 
			// object_2126a577_02be_4922_b078_be4cfcea14ab
			// 
			this->object_2126a577_02be_4922_b078_be4cfcea14ab->AngleTransform = 0;
			this->object_2126a577_02be_4922_b078_be4cfcea14ab->Name = L"object_2126a577_02be_4922_b078_be4cfcea14ab";
			this->object_2126a577_02be_4922_b078_be4cfcea14ab->StretchHorizontally = true;
			this->object_2126a577_02be_4922_b078_be4cfcea14ab->StretchVertically = true;
			// 
			// object_6592ab6f_bcb9_4797_b57a_0225350bf213
			// 
			this->object_6592ab6f_bcb9_4797_b57a_0225350bf213->Name = L"object_6592ab6f_bcb9_4797_b57a_0225350bf213";
			this->object_6592ab6f_bcb9_4797_b57a_0225350bf213->StretchHorizontally = true;
			this->object_6592ab6f_bcb9_4797_b57a_0225350bf213->StretchVertically = true;
			// 
			// Waypoint
			// 
			this->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSize = true;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->ClientSize = System::Drawing::Size(1271, 466);
			this->Controls->Add(this->panelEdit);
			this->Controls->Add(this->panelMiniMap);
			this->Controls->Add(this->panelHudMode);
			this->Controls->Add(this->radPanelMenu);
			this->Controls->Add(this->radLabel3);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->Location = System::Drawing::Point(464, 40);
			this->MaximizeBox = false;
			this->Name = L"Waypoint";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->RootElement->MinSize = System::Drawing::Size(0, 0);
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Waypoint";
			this->TransparencyKey = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(100)), static_cast<System::Int32>(static_cast<System::Byte>(100)),
				static_cast<System::Int32>(static_cast<System::Byte>(100)));
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Waypoint::Waypoint_FormClosing);
			this->Load += gcnew System::EventHandler(this, &Waypoint::Waypoint_Load);
			this->VisibleChanged += gcnew System::EventHandler(this, &Waypoint::Waypoint_VisibleChanged);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointPageView))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointActionDropDownList))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointAddButton))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer2))->EndInit();
			this->radSplitContainer2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel3))->EndInit();
			this->splitPanel3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonZoomIn))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel4))->EndInit();
			this->splitPanel4->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonZoomOut))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSplitContainer3))->EndInit();
			this->radSplitContainer3->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel5))->EndInit();
			this->splitPanel5->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonUpLevel))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->splitPanel6))->EndInit();
			this->splitPanel6->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->buttonDownLevel))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radHideMiniMap))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelEdit))->EndInit();
			this->panelEdit->ResumeLayout(false);
			this->panelEdit->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosoutheastNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radioeastNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox10))->EndInit();
			this->radGroupBox10->ResumeLayout(false);
			this->radGroupBox10->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointPathLabelTextBox))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox11))->EndInit();
			this->radGroupBox11->ResumeLayout(false);
			this->radGroupBox11->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->waypointInfoLabelTextBox))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouthNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouthwestNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiocenterNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiowestNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorthwestNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionortheastNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorthNew))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_lure_reverse))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_automatic_lure))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_update_coord))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->checkBoxTopMost))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelMiniMap))->EndInit();
			this->panelMiniMap->ResumeLayout(false);
			this->panelMiniMap->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->checkBoxShowOtherLevels))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radRepeatButton2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radRepeatButton1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panelHudMode))->EndInit();
			this->panelHudMode->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnlever))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosoutheast))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton11))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnuse))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnladder))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnmachete))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiosouthwest))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnwalk))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnrope))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radioeast))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnlua))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiocenter))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnshovel))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radiowest))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnstand))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorthwest))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionortheast))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radionorth))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelMenu))->EndInit();
			this->radPanelMenu->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			this->radPanel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenuComboItem1->ComboBoxElement))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonWaypointer))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		bool automatic_lure = false;

		bool lure_reverse = false;
		int last_index = 9999;
		int last_index_path = 9999;
		bool drag_drop = false;
		bool disable_update = false;

		MiniMap::Coordinate^ lastMouseClickCoord;
		Telerik::WinControls::UI::RadPageViewPage^ currentWaypointPath;
		Telerik::WinControls::UI::RadListView^ currentListView;
		Telerik::WinControls::UI::ListViewDataItem^ currentListViewItem;
		Telerik::WinControls::RadElement^ selectedItemWithRightCLick;
		System::Void addWaypointLure(bool minimap, MiniMap::Coordinate^ coord);
		System::Void loadAll();
		System::Void Waypoint_Load(System::Object^  sender, System::EventArgs^  e);
		System::Void waypointPageContextMenuOpen(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
		System::Void waypointConfigureItemMenuOpen(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
		void AddNewPage(String^ identifier, String^ text);
		System::Void waypointPageAddNewPage(System::Object^ sender, System::EventArgs^ args);
		System::Void waypointLurePointFromMinimap(System::Object^ sender, System::EventArgs^ args);

		System::Void waypointListViewPreviewDragOver(System::Object^  sender, Telerik::WinControls::RadDragOverEventArgs^  e);
		System::Void waypointListViewStopped(System::Object^  sender, System::EventArgs^  e);
		
		System::Void waypointPageViewPreviewDragStart(System::Object^  sender, Telerik::WinControls::PreviewDragStartEventArgs^  e);
		System::Void waypointPageViewPreviewDragOver(System::Object^  sender, Telerik::WinControls::RadDragOverEventArgs^  e);
		System::Void waypointPageViewStopped(System::Object^  sender, System::EventArgs^  e);

		System::Void waypointInsertDown(System::Object^ sender, System::EventArgs^ args);
		System::Void waypointInsert(System::Object^ sender, System::EventArgs^ args);
		System::Void waypointRemove(System::Object^ sender, System::EventArgs^ args);
		System::Void waypointSetCurrentWaypointInfo(System::Object^ sender, System::EventArgs^ args);
		System::Void waypointAddLurePointForSelected(System::Object^ sender, System::EventArgs^ args);
		System::Void waypointListViewDoubleClick(System::Object^  sender, System::EventArgs^ e);
		System::Void waypointListViewSelectedIndexChanged(System::Object^  sender, System::EventArgs^ e);
		System::Void waypointListViewItemValueChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangedEventArgs^  e);
		System::Void waypointListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
		System::Void waypointListViewItemRemoved(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e);
		std::shared_ptr<WaypointInfo> addCoordinate(MiniMap::Coordinate^ coord, waypoint_t type, orientation_enum_t orientatio_type);
		System::Void waypointAddButtonClick(System::Object^  sender, System::EventArgs^  e);
		System::Void waypointPageView_NewPageRequested(System::Object^  sender, System::EventArgs^  e);
		System::Void Waypoint_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e);
		System::Void waypointPageView_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void waypointActionDropDownList_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
		void loadWaypointOptions();
		System::Void Waypoint::waypointList_EditorRequired(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEditorRequiredEventArgs^  e);
		void update_idiom();
		System::Void loadAction(int waypoint_t_index);
		System::Void unLoadAction();
		System::Void TextEditorInitialize(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e);
		System::Void onPageEditName(System::Object^ sender, System::EventArgs^ args);
		std::shared_ptr<WaypointInfo> getSelectedWaypointInfo();
		std::shared_ptr<WaypointPath> getCurrentWaypointPath();
		void on_coordinate_mouse_down(MiniMap::Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args);
		void on_coordinate_mouse_up(MiniMap::Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args);
		void on_coordinate_click(MiniMap::Coordinate^ coord, System::EventArgs^);
		void on_coordinate_mouse_move(MiniMap::Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args);
		void on_waypoint_out(MiniMap::WaypointInfo^);
		void on_waypoint_enter(MiniMap::WaypointInfo^);
		void on_waypoint_moved(MiniMap::WaypointInfo^);
		side_t::side_t Waypoint::get_side_by_orientation(orientation_enum_t type);
		void on_waypoint_selected_changed(MiniMap::WaypointInfo^ _old, MiniMap::WaypointInfo^ _new);
		System::Void waypointPageView_PageRemoving(System::Object^  sender, Telerik::WinControls::UI::RadPageViewCancelEventArgs^  e);
		System::Void onActionComponent_TextChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void onActionComponent_SelectedIndexChanged(System::Object^ sender);
		System::Void Waypoint::SaveActionComponentValue(System::Object^ sender);
		System::Void waypointInfoLabelTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void waypointPathLabelTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void waypointPageView_TextChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void miniMapControl1_onCoordinateMouseClick(MiniMap::Coordinate^  A_0, System::EventArgs^args);
		System::Void MenuNewWaypointClick(System::Object^  sender, System::EventArgs^  e);
		System::Void updateMiniMapWaypoints();
		bool can_execute();
		System::Void update_current_waypoint(System::Object^  sender, System::EventArgs^  e);
		System::Void Waypoint_VisibleChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonZoomIn_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonZoomOut_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonUpLevel_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonDownLevel_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void radHideMiniMap_Click(System::Object^  sender, System::EventArgs^  e);
		void select_waypoint_in_list_by_identifier(String^ identifier);
		System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void radButton2_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void dropDownEditionMode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
		void set_edition_mode(waypoint_edition_mode_t mode);
		void set_mode_edit_insert();
		void set_mode_insert_edit();
		void set_mode_hud_mode();
		void set_mode_minimap_plus_mode_hud();
		System::Void checkBoxTopMost_CheckedChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
		waypoint_edition_mode_t get_waypoint_edition_mode();
		orientation_enum_t get_waypoint_selected_orientation();
		Coordinate get_offset_to_current_orientation();
		MiniMap::Coordinate^ genMiniMapCoordFromNormalCoord(Coordinate coord);
		System::Void btn_on_add_hud(System::Object^  sender, System::EventArgs^  e);
		System::Void radButton11_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void radionortheast_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
		System::Void radio_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
		System::Void radRepeatButton2_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void radRepeatButton1_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void checkBoxShowOtherLevels_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
		System::Void radMenuItem9_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void pathMenu_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonSavePath_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonLoadPath_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonSaveWaypointer_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void buttonLoadWaypointer_Click(System::Object^  sender, System::EventArgs^  e);
		void update_minimap_current_coord_axis();
		void update_item();

		Waypoint::orientation_enum_t Waypoint::get_orientation_by_side(side_t::side_t type);

		Telerik::WinControls::UI::RadToggleButton^ get_button_side(orientation_enum_t type){
			switch (type){
			case orientation_enum_t::orientation_north:
				return this->radionorthNew;
				break;
			case orientation_enum_t::orientation_northeast:
				return this->radionortheastNew;
				break;
			case orientation_enum_t::orientation_east:
				return this->radioeastNew;
				break;
			case orientation_enum_t::orientation_southeast:
				return this->radiosoutheastNew;
				break;
			case orientation_enum_t::orientation_south:
				return this->radiosouthNew;
				break;
			case orientation_enum_t::orientation_southwest:
				return this->radiosouthwestNew;
				break;
			case orientation_enum_t::orientation_west:
				return this->radiowestNew;
				break;
			case orientation_enum_t::orientation_northwest:
				return this->radionorthwestNew;
				break;
			case orientation_enum_t::orientation_center:
				return this->radiocenterNew;
				break;
			default:
				return nullptr;
				break;
			}
		}

		void clear_button_sides(){
			currentSideButtonMini = radiocenterNew;

			disable_update = true;
			radiosoutheastNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radiosouthNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radiosouthwestNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radioeastNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radiowestNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radionorthwestNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radionortheastNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radionorthNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;
			radiocenterNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::On;
			disable_update = false;
		}

		System::Void onActionComponent_Click(System::Object^ sender, System::EventArgs^  e);
		System::Void bt_update_coord_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void check_automatic_lure_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 check_lure_reverse->Enabled = check_automatic_lure->Checked;
				 automatic_lure = check_automatic_lure->Checked;
	}
	private: System::Void check_lure_reverse_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 lure_reverse = check_lure_reverse->Checked;
	}
};
}