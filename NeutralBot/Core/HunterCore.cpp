#pragma once
#include "HunterCore.h"
#include "Map.h"
#include "..\\WaypointManager.h"
#include "Pvp.h"
#include "TibiaProcess.h"
#include "SpellCasterCore.h"
#include "Time.h"
#include "..\\ConfigPathManager.h"
#include <thread>
#include "Input.h"
#include "Pathfinder.h"
#include "Actions.h"
#include "LooterCore.h"
#include <limits>
#include "..\\DevelopmentManager.h"
#include "Actions.h"
#include "LuaCore.h"
#include "WaypointerCore.h"

bool CreatureOnBattle::is_player(){
	return !type;
}

bool CreatureOnBattle::is_summon(){
	//our summon
	//enemy summon
	return type == creature_type::creature_type_other_summon || type == creature_type::creature_type_our_summon;
}

bool CreatureOnBattle::is_monster(){
	return type == creature_type::creature_type_monster;
}

bool CreatureOnBattle::is_npc(){
	return type == creature_type::creature_type_npc;
}

Coordinate CreatureOnBattle::get_coordinate_monster(){
	return Coordinate(pos_x, pos_y, pos_z);
}

Coordinate CreatureOnBattle::get_axis_displacement(){
	return Coordinate(dist_from_center_x, dist_from_center_y, 0);
}

bool BattleList::is_traped_by_monster(){
	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (gMapTilesPtr->coordinate_is_traped(self_coord))
		return get_creatures_near_coordinate(self_coord).size() != 0;

	return false;
}

void BattleList::swap_backbuffer(){
	{
		lock_guard _lock(mtx_access);
		std::map<int /*position_on_battle*/, std::shared_ptr<CreatureOnBattle>>* temp = creatures;
		creatures = creatures_swap;
		creatures_swap = temp;

		*creatures_on_screen_swap = get_creatures_on_screen();
		std::map<int /*position_on_battle*/, std::shared_ptr<CreatureOnBattle>>* temp_on_screen = creatures_on_screen;
		creatures_on_screen = creatures_on_screen_swap;
		creatures_on_screen_swap = temp_on_screen;
	}

	compare_buffers();

	uint32_t temp_id = TibiaProcess::get_default()->character_info->target_red();
	if (temp_id != current_target_id){
		if (temp_id != 0)
			notify_on_target_changed(find_creature_by_id(temp_id));
		else
			notify_on_target_changed(nullptr);

		last_target_id = current_target_id;
	}
	current_target_id = temp_id;
}

//compare buffers, check creatures died, left, enter screen, and invoke their callbacks
void BattleList::compare_buffers(){
	std::vector<CreatureOnBattlePtr> creatures_entered;
	std::vector<CreatureOnBattlePtr> creatures_left_screen;
	std::vector<CreatureOnBattlePtr> creatures_died;

	auto swap_begin = creatures_on_screen_swap->begin();
	auto swap_end = creatures_on_screen_swap->end();
	//check creatures entered screen
	for (auto creature = creatures_on_screen->begin(); creature != creatures_on_screen->end(); creature++){
		bool _found = false;
		for (; swap_begin != swap_end; swap_begin++){

			if (!creature->second)
				continue;

			if (creature->second->id == swap_begin->second->id){
				_found = true;
				break;
			}
		}

		if (!_found)
			creatures_entered.push_back(creature->second);
	}

	//check creatures left screen and creatures died
	swap_begin = creatures_on_screen_swap->begin();
	for (; swap_begin != swap_end; swap_begin++){
		bool _found = false;
		for (auto creature = creatures_on_screen->begin(); creature != creatures_on_screen->end(); creature++){
			if (creature->second->id == swap_begin->second->id){
				_found = true;
				break;
			}
		}

		if (!_found){
			//died
			auto creature_found = all_creatures.begin();
			for (; creature_found != all_creatures.end(); creature_found++){
				if (!creature_found->get())
					continue;

				if (creature_found->get()->id == swap_begin->second->id)
					break;
			}

			if (creature_found != all_creatures.end()){
				if (!creature_found->get()->life)
					creatures_died.push_back(*creature_found);
			}
			else
				creatures_left_screen.push_back(swap_begin->second);
		}
	}

	//Invoke callbacks
	for (auto creature : creatures_died)
		notify_on_creature_die(creature);


	for (auto creature : creatures_left_screen)
		notify_on_creature_out_screen(creature);


	for (auto creature : creatures_entered)
		notify_on_creature_enter_screen(creature);

}

std::vector<std::string> BattleList::get_monsters_appeared(){
	std::vector<std::string> all_creatures_return;
	mtx_access.lock();
	for (auto it : all_creatures){
		if (it->type != creature_type::creature_type_monster)
			continue;

		auto res = std::find_if(all_creatures_return.begin(), all_creatures_return.end(), [&](std::string str) -> bool {
			return _stricmp(it->name, &str[0]) == 0;
		});

		if (res == all_creatures_return.end()){
			all_creatures_return.push_back(it->name);
		}
	}
	mtx_access.unlock();
	return all_creatures_return;
}

std::vector<std::string> BattleList::get_npcs_appeared(){
	std::vector<std::string> all_creatures_return;
	mtx_access.lock();
	for (auto it : all_creatures){
		if (it->type != creature_type::creature_type_npc)
			continue;

		auto res = std::find_if(all_creatures_return.begin(), all_creatures_return.end(), [&](std::string str) -> bool {
			return _stricmp(it->name, &str[0]) == 0;
		});

		if (res == all_creatures_return.end()){
			all_creatures_return.push_back(it->name);
		}
	}
	mtx_access.unlock();
	return all_creatures_return;
}

std::vector<std::string> BattleList::get_players_appeared(){
	std::vector<std::string> all_creatures_return;
	mtx_access.lock();
	for (auto it : all_creatures){
		if (it->type != creature_type::creature_type_player)
			continue;

		auto res = std::find_if(all_creatures_return.begin(), all_creatures_return.end(), [&](std::string str) -> bool {
			return _stricmp(it->name, &str[0]) == 0;
		});

		if (res == all_creatures_return.end()){
			all_creatures_return.push_back(it->name);
		}
	}
	mtx_access.unlock();
	return all_creatures_return;
}

BattleList::BattleList(){
	last_target_id = 0;
	current_target_id = 0;

	creatures_swap = new std::map<int /*position_on_battle*/, std::shared_ptr<CreatureOnBattle>>();
	creatures = new std::map<int /*position_on_battle*/, std::shared_ptr<CreatureOnBattle>>();

	creatures_on_screen = new std::map<int /*position_on_battle*/, std::shared_ptr<CreatureOnBattle>>();
	creatures_on_screen_swap = new std::map<int /*position_on_battle*/, std::shared_ptr<CreatureOnBattle>>();

	std::thread([&](){
		Sleep(4000);
		MONITOR_THREAD("BattleList::refresher");

		while (true){
			Sleep(1);

			if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
				continue;

			if (!NeutralManager::get()->get_bot_state())
				continue;

			BattleList::get()->update();
		}
	}).detach();

}

std::vector<std::string> BattleList::get_creature_appeared(){
	std::vector<std::string> all_creatures_return;
	mtx_access.lock();
	for (auto it : all_creatures){
		auto res = std::find_if(all_creatures_return.begin(), all_creatures_return.end(), [&](std::string str) -> bool {
			return _stricmp(it->name, &str[0]) == 0;
		});

		if (res == all_creatures_return.end()){
			all_creatures_return.push_back(it->name);
		}
	}
	mtx_access.unlock();
	return all_creatures_return;
}

void BattleList::update(){

	mtx_access.lock();
	all_creatures.clear();
	creatures_swap->clear();
	mtx_access.unlock();

	uint32_t first_address = AddressManager::get()->getAddress(ADDRESS_BATTLE_FIRST_ID);
	uint32_t read_per_round = 100;

	char* buffer = new char[sizeof(CreatureOnBattle)* read_per_round];
	int position = 0;

	while (true){
		Sleep(1);

		if (TibiaProcess::get_default()->read_memory_block(first_address, (unsigned char*)buffer, sizeof(CreatureOnBattle)* read_per_round) !=
			(sizeof(CreatureOnBattle)* read_per_round)){
			read_per_round /= 2;
			if (!read_per_round){
				delete[]buffer;
				swap_backbuffer();
				refresh_count++;
				return;
			}
			continue;
		}
		for (uint32_t i = 0; i < read_per_round; i++){
			std::shared_ptr<CreatureOnBattle> character_on_battle(new CreatureOnBattle);
			memcpy(character_on_battle.get(), &buffer[sizeof(CreatureOnBattle)* i], sizeof(CreatureOnBattle));
			if (character_on_battle.get()->id == 0){
				delete[]buffer;
				swap_backbuffer();
				refresh_count++;
				return;
			}
			else if (character_on_battle->on_screenNew != 0 && character_on_battle->life >= 0){
				if (!character_on_battle)
					continue;

				all_creatures.push_back(character_on_battle);
				creatures_swap->insert(std::pair<int, CreatureOnBattlePtr >(position, character_on_battle));
				position++;
			}
			else{
				if (!character_on_battle)
					continue;

				all_creatures.push_back(character_on_battle);
			}
		}
		first_address += sizeof(CreatureOnBattle)* 100;
	}

	refresh_count++;
}

bool BattleList::player_kill_on_screen(bool ignoreZ, bool lock){
	lock_guard mtx_(mtx_access);

	std::map<int, std::shared_ptr<CreatureOnBattle>> monster_on_screen = get_creatures_on_screen();
	if (!monster_on_screen.size())
		return false;

	for (auto it : monster_on_screen){
		if (!it.second)
			continue;

		if (!it.second->is_player())
			continue;

		if (it.second->skull == 0)
			continue;

		return true;
	}
	return false;
}

std::shared_ptr<CreatureOnBattle> BattleList::get_target(bool lock){
	int id = TibiaProcess::get_default()->character_info->target_red();
	if (id == 0)
		return nullptr;
	return BattleList::get()->find_creature_by_id(id, false, lock);
}

std::shared_ptr<CreatureOnBattle> BattleList::get_self(bool lock){
	int id = TibiaProcess::get_default()->character_info->character_id();
	if (id == 0)
		return nullptr;
	return BattleList::get()->find_creature_by_id(id, false, lock);
}

std::map<int, std::shared_ptr<CreatureOnBattle>> BattleList::get_creatures_at_coordinate(Coordinate coord, bool ignoreZ, bool lock){
	std::map<int, std::shared_ptr<CreatureOnBattle>> retval;
	auto self_pos = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (lock){
		mtx_access.lock();
	}

	for (auto it = creatures->begin(); it != creatures->end(); it++)
	if (ignoreZ || self_pos.z == it->second->pos_z)
	if (it->second->pos_x == coord.x && it->second->pos_y == coord.y && it->second->pos_z == coord.z)
		retval[it->first] = it->second;
	if (lock)
		mtx_access.unlock();
	return retval;
}

std::vector<std::shared_ptr<CreatureOnBattle>> BattleList::get_creatures_near_coordinate(Coordinate coord, uint32_t sqm_to_consider_near, bool ignoreZ
	, bool lock){
	std::vector<std::shared_ptr<CreatureOnBattle>> retval;


	auto self_pos = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (lock){
		mtx_access.lock();
	}

	for (auto it = creatures->begin(); it != creatures->end(); it++)
	if (ignoreZ || it->second->pos_z == self_pos.z)
	if (it->second->get_coordinate_monster().get_axis_max_dist(coord) <= sqm_to_consider_near)
		retval.push_back(it->second);
	if (lock)
		mtx_access.unlock();

	return retval;
}

std::vector<std::shared_ptr<CreatureOnBattle>> BattleList::get_creatures_on_area(Coordinate coord, uint32_t sqm_to_consider_near, bool ignoreZ, bool lock){
	std::vector<std::shared_ptr<CreatureOnBattle>> retval;

	auto self_pos = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (lock){
		mtx_access.lock();
	}


	for (auto it = creatures->begin(); it != creatures->end(); it++) {
		if (ignoreZ || it->second->pos_z == self_pos.z) {
			if (it->second->pos_x < (coord.x - sqm_to_consider_near))
				continue;
			else if (it->second->pos_x >(coord.x + sqm_to_consider_near))
				continue;
			if (it->second->pos_y < (coord.y - sqm_to_consider_near))
				continue;
			else if (it->second->pos_y >(coord.y + sqm_to_consider_near))
				continue;

			retval.push_back(it->second);
		}
	}

	if (lock)
		mtx_access.unlock();

	return retval;
}

std::vector<std::shared_ptr<CreatureOnBattle>> BattleList::get_creatures_on_area_effect(Coordinate coord, area_type_t areaEffect){
	std::map<int, std::shared_ptr<CreatureOnBattle>> creatures_on_screen = get_creatures_on_screen(false, true);
	return get_creatures_on_area_effect(creatures_on_screen, coord, areaEffect);
}

std::vector<std::shared_ptr<CreatureOnBattle>> BattleList::get_creatures_on_area_effect(
	std::map<int, std::shared_ptr<CreatureOnBattle>>& creatures_on_screen, Coordinate coord, area_type_t areaEffect){
	std::vector<std::shared_ptr<CreatureOnBattle>> retval;

	std::vector<std::vector<int>> area = SpellManager::get_spell_area_by_name(areaEffect);
	int total_size = area.size();

	coord.x = (int32_t)std::floor(coord.x - (total_size / 2));
	coord.y = (int32_t)std::floor(coord.y - (total_size / 2));

	for (int x = 0; x < total_size; x++) {
		for (int y = 0; y < total_size; y++) {
			if (area[x][y] == 0)
				continue;

			for (auto creature : creatures_on_screen) {
				if (creature.second->pos_x !=
					(coord.x + x) || creature.second->pos_y != (coord.y + y) || creature.second->pos_z != coord.z)
					continue;
				retval.push_back(creature.second);
			}
		}
	}
	return retval;
}

std::shared_ptr<CreatureOnBattle> BattleList::find_creature_on_screen(std::string name, bool ignoreZ, bool lock){
	auto& vectortest = get_creatures_on_screen(ignoreZ, lock);
	for (auto it = vectortest.begin(); it != vectortest.end(); it++){
		if (_strnicmp(it->second->name, &name[0], sizeof(it->second->name)) != 0)
			continue;

		return it->second;
	}
	return 0;
}

std::shared_ptr<CreatureOnBattle> BattleList::find_creature_by_id(uint32_t id, bool ignoreZ, bool lock){
	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (lock){
		mtx_access.lock();
	}


	for (auto it = creatures->begin(); it != creatures->end(); it++){

		if (id != it->second->id)
			continue;

		if (ignoreZ){
			if (self_coordinate.is_in_near_other(it->second->get_coordinate_monster(), 7, 5, 20)){
				if (lock)
					mtx_access.unlock();
				return it->second;
			}
		}
		else{
			if (self_coordinate.is_in_near_other(it->second->get_coordinate_monster())){
				if (lock)
					mtx_access.unlock();
				return it->second;
			}
		}
		break;
	}
	if (lock)
		mtx_access.unlock();

	return 0;
}

bool BattleList::have_player(){
	std::vector<std::shared_ptr<CreatureOnBattle>> retval;
	lock_guard _lock(mtx_access);
	for (auto it = creatures->begin(); it != creatures->end(); it++){
		if (!it->second->is_player())
			continue;

		if (it->second->name == TibiaProcess::get_default()->character_info->get_name_char())
			continue;

		return true;
	}
	return false;
}

bool BattleList::have_creature_on_screen(){
	uint32_t own_id = TibiaProcess::get_default()->character_info->character_id();
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	lock_guard _lock(mtx_access);
	for (auto it = creatures->begin(); it != creatures->end(); it++){
		if (it->second->pos_z == current_coord.z && it->second->id != own_id
			&& it->second->is_monster()){
			return true;
		}
	}
	return false;
}

bool BattleList::have_other_player(){
	std::vector<std::shared_ptr<CreatureOnBattle>> retval;
	uint32_t own_id = TibiaProcess::get_default()->character_info->character_id();
	lock_guard _lock(mtx_access);
	for (auto it = creatures->begin(); it != creatures->end(); it++){
		if (it->second->id != own_id && it->second->is_player()){
			return true;
		}

	}
	return false;
}

uint32_t BattleList::get_creature_count_in_radius(std::vector<std::string> creatures_name, uint32_t radius, bool must_be_reachable){
	auto creatures = get_creatures_on_screen();

	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	uint32_t creature_count = 0;

	for (auto creature : creatures){
		if (must_be_reachable){
			if (!Pathfinder::get()->is_reachable(creature.second->get_coordinate_monster()))
				continue;
		}

		uint32_t distance = current_coord.get_axis_max_dist(creature.second->get_coordinate_monster());
		if (radius <= distance)
			continue;

		auto _found = std::find_if(creatures_name.begin(), creatures_name.end(), [&](std::string& creature_str){
			return _stricmp(creature.second->name, &creature_str[0]) == 0; });
			if (_found != creatures_name.end()){
				creature_count++;
			}
	}
	return creature_count;
}

std::vector<std::shared_ptr<CreatureOnBattle>> BattleList::get_creatures_in_radius(std::vector<std::string> creatures_name,
	uint32_t radius, bool must_be_reachable){
	std::vector<std::shared_ptr<CreatureOnBattle>> retval;

	auto creatures = get_creatures_on_screen();

	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	for (auto creature : creatures){
		if (!creature.second->is_monster())
			continue;

		if (must_be_reachable){
			if (!Pathfinder::get()->is_reachable(creature.second->get_coordinate_monster()))
				continue;
		}

		uint32_t distance = current_coord.get_axis_max_dist(creature.second->get_coordinate_monster());
		if (radius <= distance)
			continue;


		auto _found = std::find_if(creatures_name.begin(), creatures_name.end(), [&](std::string& creature_str){
			return _stricmp(creature.second->name, &creature_str[0]) == 0;

		});
		if (_found != creatures_name.end()){
			retval.push_back(creature.second);
		}
	}
	return retval;
}

std::map<int, std::shared_ptr<CreatureOnBattle>> BattleList::get_creatures_on_screen(bool ignoreZ, bool lock){
	std::map<int, std::shared_ptr<CreatureOnBattle>> retval;
	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (lock)
		mtx_access.lock();

	for (auto it = creatures->begin(); it != creatures->end(); it++){
		if (ignoreZ || it->second->pos_z == self_coordinate.z)
		if (it->second->on_screenNew){
			if (self_coordinate.is_in_near_other(it->second->get_coordinate_monster())){
				retval[it->first] = it->second;
			}
		}
	}

	if (lock)
		mtx_access.unlock();

	return retval;
}

std::vector<std::shared_ptr<CreatureOnBattle>> BattleList::refresh_battle_in_gui(){
	int our_id = TibiaProcess::get_default()->character_info->character_id();
	std::map<int, std::shared_ptr<CreatureOnBattle>> monster_on_screen = get_creatures_on_screen();
	std::shared_ptr<CreatureOnBattle> PrimeiroList;
	std::vector<std::shared_ptr<CreatureOnBattle>> retval;

	for (auto it = monster_on_screen.begin(); it != monster_on_screen.end(); it++){
		if (our_id == it->second->id)
			continue;

		if (it->second->before == -1){
			PrimeiroList = it->second;
			retval.push_back(it->second);
		}
	}

	if (!PrimeiroList)
		return retval;

	auto found_creature = monster_on_screen.find(PrimeiroList->next);
	while (found_creature != monster_on_screen.end()){
		if (std::find(retval.begin(), retval.end(), found_creature->second) != retval.end())
			return retval;

		retval.push_back(found_creature->second);
		found_creature = monster_on_screen.find(found_creature->second->next);
	}
	return retval;
}

uint32_t BattleList::get_creature_index_in_battle_gui(uint32_t id){

	uint32_t retval = UINT32_MAX;
	auto creatures_local = refresh_battle_in_gui();
	uint32_t index = 0;
	for (auto it : creatures_local){
		if (it->id == id){
			retval = index;
			break;
		}
		index++;
	}
	return retval;
}

uint32_t BattleList::wait_creature_out_of_range(uint32_t creature_id,
	int min_dist, int max_dist, uint32_t timeout, bool use_displaced_coords){
	TimeChronometer timer;
	while (timer.elapsed_milliseconds() < (int32_t)timeout){
		std::shared_ptr<CreatureOnBattle> creature = find_creature_by_id(creature_id);

		if (!creature)
			return -1;

		//Coordinate current_coord = TibiaProcess::get_default()->character_info->get_mixed_coordinate();
		Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		Coordinate creature_coord = creature->get_mixed_coordinate();

		uint32_t dist = current_coord.get_axis_max_dist(creature_coord);
		if ((int32_t)dist < min_dist || (int32_t)dist > max_dist)
			return dist;

		Sleep(10);
	}
	return -1;
}

void BattleList::add_event(hunter_event_t event_type, std::function<void(CreatureOnBattlePtr)> callback){
	mtx_access.lock();

	switch (event_type){
	case hunter_event_t::hunter_event_creature_enter_screen:
		on_creature_enter_screen.push_back(callback);
		break;
	case hunter_event_t::hunter_event_creature_out_screen:
		on_creature_out_screen.push_back(callback);
		break;
	case hunter_event_t::hunter_event_creature_die:
		on_creature_die.push_back(callback);
		break;
	case hunter_event_t::hunter_event_target_changed:
		on_target_changed.push_back(callback);
		break;
	}

	mtx_access.unlock();
}

void BattleList::notify_on_creature_enter_screen(CreatureOnBattlePtr creature){
	for (auto callback : on_creature_enter_screen)
		callback(creature);
}

CreatureOnBattlePtr BattleList::get_creature_by_id(uint32_t id){
	return find_creature_by_id(id);
}

void BattleList::wait_refresh(){
	uint64_t current_count = refresh_count;

	while ((refresh_count - current_count) < 2)
		Sleep(1);
}

void BattleList::notify_on_creature_out_screen(CreatureOnBattlePtr creature){
	for (auto callback : on_creature_out_screen)
		callback(creature);
}

void BattleList::notify_on_creature_die(CreatureOnBattlePtr creature){
	for (auto callback : on_creature_die)
		callback(creature);
}

void BattleList::notify_on_target_changed(CreatureOnBattlePtr creature){
	for (auto callback : on_target_changed)
		callback(creature);
}

bool HunterActions::attack_creature_by_name(std::string name, std::function<bool()> callback){
	auto creature = BattleList::get()->find_creature_on_screen(name);
	if (!creature)
		return false;
	return attack_creature_id(creature->id, nullptr);
}

bool HunterActions::attack_creature_on_coordinates(uint32_t id){
	auto creature = BattleList::get()->find_creature_by_id(id);
	if (!creature)
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	if (!MapTiles::can_click_coord(creature->get_coordinate_monster()))
		Actions::goto_coord_on_screen(creature->get_coordinate_monster(), 0);

	TibiaProcess::get_default()->character_info->wait_stop_waking();

	input->click_tibia_coordinate_right(creature->get_coordinate_monster(), creature->get_axis_displacement());
	return true;
}

bool HunterActions::attack_creature_with_shared_memory(uint32_t id){
	//TODO
	return false;
}

bool HunterActions::attack_creature_on_battle(uint32_t id, int32_t timeout){
	TimeChronometer chronometer;

	while (chronometer.elapsed_milliseconds() < timeout){
		uint32_t creature_index = BattleList::get()->get_creature_index_in_battle_gui(id);
		if (creature_index == UINT32_MAX)
			return false;

		uint32_t target_battle_index = 0;
		Rect visible_battle_area_on_client_area;
		uint32_t hidden_pixel = 0;

		auto interface_battle = Interface->get_battle_container();
		if (!interface_battle)
			return false;

		RightPaneContainerBattle* battle_interface = (RightPaneContainerBattle*)interface_battle.get();

		auto clickable_area = battle_interface->get_clickable_area_to_listbox_index(creature_index);

		if (!clickable_area.is_null()){
			auto input = HighLevelVirtualInput::get_default();
			if (!input->can_use_input())
				continue;

			input->click_left(clickable_area.get_center());
			return true;
		}
		else
			battle_interface->set_listbox_index_visible(creature_index);

		Sleep(1);
	}
	return false;
}

void HunterActions::add_temporary_target(uint32_t creature_id, uint32_t max_time, std::function<bool()> have_to_continue_attacking){
	lock_guard _lock(mtx_access);
	has_temporary = true;
	uint32_t minor_time = 0xffffffff;
	uint32_t retval = 0;
	for (auto temporary : temporary_tagets){
		if (temporary.creature_id == creature_id){
			temporary.function_has_to_continue_attack = have_to_continue_attacking;
			temporary.max_time = max_time;
			temporary.timer.reset();
			return;
		}
	}
	TemporaryTarget temporary_target;
	temporary_target.creature_id = creature_id;
	temporary_target.function_has_to_continue_attack = have_to_continue_attacking;
	temporary_target.max_time = max_time;
	temporary_target.timer.reset();
	temporary_tagets.push_back(temporary_target);
}

void HunterActions::clear_temporary_targets(){
	lock_guard _lock(mtx_access);
	temporary_tagets.clear();
}

std::vector<TemporaryTarget> HunterActions::get_temporary_tagets(){
	lock_guard _lock(mtx_access);
	return temporary_tagets;
}

uint32_t HunterActions::get_temporary_target_id(){
	if (!has_temporary)
		return 0;

	lock_guard _lock(mtx_access);
	erase_expired_temporary_targets();
	uint32_t minor_time = 0xffffffff;
	uint32_t retval = 0;
	for (auto temporary : temporary_tagets){
		if ((uint32_t)temporary.timer.elapsed_milliseconds() < minor_time){
			if (temporary.function_has_to_continue_attack && !temporary.function_has_to_continue_attack()){
				continue;
			}
			retval = temporary.creature_id;
		}
	}
	return retval;
}

bool HunterActions::attack_creature_id(uint32_t id, std::function<bool()> callback, uint32_t timeout){//TODO
	if (TibiaProcess::get_default()->character_info->target_red() == id)
		return true;

	if (attack_creature_with_shared_memory(id))
		return true;

	if (attack_creature_on_battle(id)){
		TimeChronometer timer;
		while (true){

			if ((uint32_t)timer.elapsed_milliseconds() > timeout)
				return false;

			if (TibiaProcess::get_default()->character_info->target_red() == id)
				break;

			Sleep(1);
		}
		return true;
	}

	if (attack_creature_on_coordinates(id)){
		TimeChronometer timer;
		while (true){

			if ((uint32_t)timer.elapsed_milliseconds() > timeout)
				return false;

			if (TibiaProcess::get_default()->character_info->target_red() == id)
				break;

			Sleep(1);
		}

		return true;
	}
	return true;
}

void HunterActions::erase_expired_temporary_targets(){

	if (!temporary_tagets.size()){
		has_temporary = false;
		return;
	}

	for (auto target = temporary_tagets.begin(); target != temporary_tagets.end();){

		if (target->max_time < (uint32_t)target->timer.elapsed_milliseconds())
			target = temporary_tagets.erase(target);
		else
			target++;

	}
}

HunterActions* HunterActions::get(){
	static HunterActions* mHunterManager;

	if (!mHunterManager){
		mHunterManager = new HunterActions;
	}

	return mHunterManager;
}

bool HunterCore::exist_creature_to_attack(){//TODO
	wait_invoke_count();
	return get_need(500);
}

bool HunterCore::need_operate(){//TODO
	return get_need(500);
}

HunterCore* HunterCore::get(){
	if (!mHunterCore)
		init();
	return mHunterCore;
}

void HunterCore::run_async(){//TODO
}

bool HunterCore::can_execute(){
	if (NeutralManager::get()->is_paused() || !NeutralManager::get()->get_bot_state())
		return false;

	if (NeutralManager::get()->get_core_states(CORE_HUNTER) != ENABLED)
		return false;

	if (time_to_pause.elapsed_milliseconds() < timeout_to_pause)
		return false;

	if (SpellCasterCore::get()->need_operate_hunter())
		return false;

	if (HunterManager::get()->get_wait_pulse()){
		if (!is_pulsed()){
			return false;
		}
		else{
		}
	}

	return true;
}

void HunterCore::start(){
	while (true){
		Sleep(1);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state())
			continue;

		LOG_TIME_INIT;

		invoke();
	}
}

void HunterCore::start_refresher(){
	/*timer.expires_from_now(boost::posix_time::milliseconds(100));
	timer.async_wait(boost::bind(&HunterCore::invoke_refresher, this));*/
}

void HunterCore::invoke_refresher(){
	//LOG_TIME_INIT
	//	//FUNCTION_MEASURE_TIME_START(0)
	//	start_refresher();
	////FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
}

std::map < uint32_t, uint32_t> get_side_danger(std::vector<DistanceParamRowBase*>* distance_params = nullptr){
	std::map < uint32_t, uint32_t> retval;
	//init sides
	retval[side_nom_axis_t::side_nom_axis_t::side_west] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_north] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_south] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_east] = 0;

	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	Coordinate current_going_coord = TibiaProcess::get_default()->character_info->get_going_step();
	if (!current_going_coord.is_null()){
		current_coordinate = current_going_coord;
	}
	BattleList::get()->mtx_access.lock();
	std::map<int, std::shared_ptr<CreatureOnBattle>> creatures_copy = BattleList::get()->get_creatures_on_screen();
	BattleList::get()->mtx_access.unlock();

	if (distance_params){
		std::vector<std::pair<std::string, uint32_t>> names;

		uint32_t current_danger = 1;
		Coordinate offset_distance;
		std::function<uint32_t(char*)> get_creature_danger = [&](char* creature_name) -> uint32_t{
			for (auto it = distance_params->begin(); it != distance_params->end(); it++){
				if ((*it)->get_type() != distance_param_row_t::distance_param_creature)
					continue;

				if (_stricmp(creature_name, &(((DistanceParamRowCreature*)(*it))->creature_name)[0]) != 0){
					continue;
				}
				return ((DistanceParamRowCreature*)(*it))->danger;
			}
			return 1;
		};

		for (auto it = distance_params->begin(); it != distance_params->end(); it++){
			switch ((*it)->get_type()){
			case distance_param_row_t::distance_param_coordinate:{
																	 offset_distance = ((DistanceParamRowCoordinate*)(*it))->coord - current_coordinate;
																	 if (offset_distance.get_greatest_axis_value() > 7){
																		 continue;
																	 }
																	 if (offset_distance.x < 0)
																		 retval[side_nom_axis_t::side_nom_axis_t::side_west] += current_danger;
																	 else if (offset_distance.x > 0)
																		 retval[side_nom_axis_t::side_nom_axis_t::side_east] += current_danger;

																	 if (offset_distance.y < 0)
																		 retval[side_nom_axis_t::side_nom_axis_t::side_north] += current_danger;
																	 else if (offset_distance.y> 0)
																		 retval[side_nom_axis_t::side_nom_axis_t::side_south] += current_danger;
			}
				break;
			}
		}

		for (auto creature : creatures_copy){
			Coordinate creature_coord = creature.second->get_coordinate_monster();
			Coordinate deslocated_creature_coord = creature.second->get_displaced_coordinate();
			if (!deslocated_creature_coord.is_null()){
				creature_coord = deslocated_creature_coord;
			}
			Coordinate offset_distance = creature_coord - current_coordinate;


			uint32_t danger = get_creature_danger(creature.second->name);


			if (offset_distance.x < 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_west] += danger;
			else if (offset_distance.x > 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_east] += danger;

			if (offset_distance.y < 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_north] += danger;
			else if (offset_distance.y> 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_south] += danger;
		}

	}
	else{
		auto targets = HunterManager::get()->get_target_rules();
		for (auto creature : creatures_copy){
			Coordinate creature_coord = creature.second->get_coordinate_monster();
			Coordinate deslocated_creature_coord = creature.second->get_displaced_coordinate();
			if (!deslocated_creature_coord.is_null()){
				creature_coord = deslocated_creature_coord;
			}
			Coordinate offset_distance = creature_coord - current_coordinate;
			auto target = HunterManager::get_target_by_values(targets, creature.second->name, creature.second->life,
				creature_coord.get_axis_max_dist(current_coordinate));

			int danger = 1;
			if (target.second)
				danger = target.second->get_danger();

			if (offset_distance.x < 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_west] += danger;
			else if (offset_distance.x > 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_east] += danger;

			if (offset_distance.y < 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_north] += danger;
			else if (offset_distance.y> 0)
				retval[side_nom_axis_t::side_nom_axis_t::side_south] += danger;
		}
	}


	return retval;
}

std::map < uint32_t, uint32_t> get_side_free_tiles(){
	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	std::map < uint32_t, uint32_t> retval;

	retval[side_nom_axis_t::side_nom_axis_t::side_west] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_north] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_south] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_east] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_west + 4] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_north + 4] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_south + 4] = 0;
	retval[side_nom_axis_t::side_nom_axis_t::side_east + 4] = 0;

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_north]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_north]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_north]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 2])
		retval[side_nom_axis_t::side_nom_axis_t::side_north]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 2])
		retval[side_nom_axis_t::side_nom_axis_t::side_north]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 2])
		retval[side_nom_axis_t::side_nom_axis_t::side_north]++;

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_south]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_south]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_south]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 2])
		retval[side_nom_axis_t::side_nom_axis_t::side_south]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 2])
		retval[side_nom_axis_t::side_nom_axis_t::side_south]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 2])
		retval[side_nom_axis_t::side_nom_axis_t::side_south]++;

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_east]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128])
		retval[side_nom_axis_t::side_nom_axis_t::side_east]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_east]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 2][128 - 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_east]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 2][128])
		retval[side_nom_axis_t::side_nom_axis_t::side_east]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 2][128 + 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_east]++;

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_west]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128])
		retval[side_nom_axis_t::side_nom_axis_t::side_west]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_west]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 2][128 - 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_west]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 2][128])
		retval[side_nom_axis_t::side_nom_axis_t::side_west]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 2][128 + 1])
		retval[side_nom_axis_t::side_nom_axis_t::side_west]++;

	retval[side_nom_axis_t::side_nom_axis_t::side_north + 4] = retval[side_nom_axis_t::side_nom_axis_t::side_north];
	retval[side_nom_axis_t::side_nom_axis_t::side_south + 4] = retval[side_nom_axis_t::side_nom_axis_t::side_south];
	retval[side_nom_axis_t::side_nom_axis_t::side_east + 4] = retval[side_nom_axis_t::side_nom_axis_t::side_east];
	retval[side_nom_axis_t::side_nom_axis_t::side_west + 4] = retval[side_nom_axis_t::side_nom_axis_t::side_west];

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 3][-1])
		retval[side_nom_axis_t::side_nom_axis_t::side_east + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 3][0])
		retval[side_nom_axis_t::side_nom_axis_t::side_east + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 3][1])
		retval[side_nom_axis_t::side_nom_axis_t::side_east + 4]++;

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 3][-1])
		retval[side_nom_axis_t::side_nom_axis_t::side_west + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 3][0])
		retval[side_nom_axis_t::side_nom_axis_t::side_west + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 3][1])
		retval[side_nom_axis_t::side_nom_axis_t::side_west + 4]++;

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][3])
		retval[side_nom_axis_t::side_nom_axis_t::side_south + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][3])
		retval[side_nom_axis_t::side_nom_axis_t::side_south + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][3])
		retval[side_nom_axis_t::side_nom_axis_t::side_south + 4]++;

	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][-3])
		retval[side_nom_axis_t::side_nom_axis_t::side_north + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][-3])
		retval[side_nom_axis_t::side_nom_axis_t::side_north + 4]++;
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][-3])
		retval[side_nom_axis_t::side_nom_axis_t::side_north + 4]++;

	return retval;
}

void HunterCore::keep_distance(Coordinate avoid_coord, std::vector<DistanceParamRowBase*>* distance_params){
	static std::vector<Coordinate> path_histories;
	static Coordinate last_coord;

	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	Coordinate current_going_coord = TibiaProcess::get_default()->character_info->get_going_step();

	if (!current_going_coord.is_null())
		current_coordinate = current_going_coord;

	struct { int x, y; }
	axis_offset = { 55, 55 };

	if (path_histories.size()){
		if ((*path_histories.rbegin()) != current_coordinate){
			path_histories.push_back(current_coordinate);

			if (path_histories.size() > 20)
				path_histories.erase(path_histories.begin());
		}
	}
	else
		path_histories.push_back(current_coordinate);

	Actions::unset_follow();
	std::map <uint32_t, uint32_t>  moster_count_side = get_side_danger(distance_params);
	std::vector<std::pair<uint32_t, uint32_t>> monster_count_side_ordered;

#pragma region ORDER

	for (auto it : moster_count_side)
		monster_count_side_ordered.push_back(std::pair<uint32_t, uint32_t>(it.first, it.second));

	bool ordered = true;
	do{
		Sleep(1);
		ordered = true;

		for (int i = 0; i < static_cast<int32_t>(monster_count_side_ordered.size()) - 1; i++){
			if (monster_count_side_ordered.at(i).second > monster_count_side_ordered.at(i + 1).second){
				ordered = false;
				std::iter_swap(monster_count_side_ordered.begin() + i, monster_count_side_ordered.begin() + i + 1);
			}
		}

	} while (!ordered);

#pragma endregion

	auto side_tiles_free = get_side_free_tiles();

	bool fisished = false;
	int free_count = 0;

	for (int x_offset = -1; x_offset < 2; x_offset++)
	for (int y_offset = -1; y_offset < 2; y_offset++)
	if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + x_offset][128 + y_offset])
		free_count++;

	int try_count = 0;

	uint32_t consider_near_yellow = ConfigPathManager::get()->get_consider_near_yellow_coords();
	uint32_t time_to_wait = ConfigPathManager::get()->get_time_to_wait();

	if (current_coordinate.z != last_coordinate_z){
		TibiaProcess::get_default()->wait_ping_delay();
		last_coordinate_z = current_coordinate.z;
	}


	do{
		Sleep(1);
		bool free_diagonal = false;

		if (try_count == 1)
			free_diagonal = true;

		if (side_tiles_free[side_t::side_t::side_north] > 3 || side_tiles_free[side_t::side_t::side_south] > 3 ||
			side_tiles_free[side_t::side_t::side_west] > 3 || side_tiles_free[side_t::side_t::side_east] > 3){

			for (int i = 0; i < 4; i++){
				//NORTH
				if (monster_count_side_ordered[i].first == side_nom_axis_t::side_north){
					if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1]
						&& !MapTiles::coordinate_is_traped(current_coordinate + Coordinate(0, -1))
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1)
						&& (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1]
						&& (moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east]))
						|| (!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != -1))){

						Actions::step_side(side_t::side_north_east, 0);  return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1)
						&& MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1))){
						Actions::step_side(side_t::side_north_west, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y - 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1))){

						Actions::step_side(side_t::side_north, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_south){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1)
						&& (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1] &&
						(moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]))
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_east, 0);  return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_west, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_east){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_east, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1] &&
						(moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1))){

						Actions::step_side(side_t::side_north_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_east, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128] && (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_east, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_west){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128] && (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_west, 0); return;
					}
					else if (free_diagonal && ((!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1] &&
						(moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north]))
						|| (!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_west, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_west, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_west, 0);  return;
					}
				}
			}
		}


		if (side_tiles_free[side_t::side_t::side_north] > 2 || side_tiles_free[side_t::side_t::side_south] > 2 ||
			side_tiles_free[side_t::side_t::side_west] > 2 || side_tiles_free[side_t::side_t::side_east] > 2){
			for (int i = 0; i < 4; i++){
				if (monster_count_side_ordered[i].first == side_nom_axis_t::side_north){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y - 1) && (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1])
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1) && (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1] &&
						(moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]
						&& MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1])) && (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1))){

						Actions::step_side(side_t::side_north_west, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y - 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north, 0);  return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_south){

					if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1) && (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1] &&
						(moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east])) || (!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1])) && (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1))){

						Actions::step_side(side_t::side_south_west, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y + 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0);  return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_east){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_east, 0);  return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1) && (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1] &&
						(moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north])) || (!MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]
						&& MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1])) && (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_east, 0);  return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1))) {

						Actions::step_side(side_t::side_south_east, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0))){


						Actions::step_side(side_t::side_east, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_west){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_west, 0);  return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1] && (moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_west, 0);  return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1))) {

						Actions::step_side(side_t::side_south_west, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_west, 0); return;
					}
				}
			}
		}
		if (side_tiles_free[side_t::side_t::side_north] > 1 || side_tiles_free[side_t::side_t::side_south] > 1 ||
			side_tiles_free[side_t::side_t::side_west] > 1 || side_tiles_free[side_t::side_t::side_east] > 1){
			for (int i = 0; i < 4; i++){
				if (monster_count_side_ordered[i].first == side_nom_axis_t::side_north){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1])
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1] && (moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1))){

						Actions::step_side(side_t::side_north_west, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y - 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_south){

					if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y + 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1] && (moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_west, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y + 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0);  return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_east){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_east, 0);  return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1] && (moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north]))
						|| (!MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_east, 0);  return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1))) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]) {

						Actions::step_side(side_t::side_south_east, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_east, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_west){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_west, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1] && (moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_west, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1))) {

						Actions::step_side(side_t::side_south_west, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_west, 0);  return;
					}
				}
			}
		}
		if (side_tiles_free[side_t::side_t::side_north] > 0 || side_tiles_free[side_t::side_t::side_south] > 0 ||
			side_tiles_free[side_t::side_t::side_west] > 0 || side_tiles_free[side_t::side_t::side_east] > 0){
			for (int i = 0; i < 4; i++){
				if (monster_count_side_ordered[i].first == side_nom_axis_t::side_north){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y - 1) && (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1])
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1] && (moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1))){

						Actions::step_side(side_t::side_north_west, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y - 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_south){

					if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y + 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1] && (moster_count_side[side_nom_axis_t::side_west] >= moster_count_side[side_nom_axis_t::side_east])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1))){

						Actions::step_side(side_t::side_south_west, 0);  return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x, current_coordinate.y + 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 0 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south, 0); return;
					}
				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_east){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_east, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1] && (moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_east, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y + 1) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_east, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x + 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128]
						&& (free_count == 1 || (axis_offset.x != 1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_east, 0);  return;
					}

				}
				else if (monster_count_side_ordered[i].first == side_nom_axis_t::side_west){
					if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y) && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0)))
					{

						Actions::step_side(side_t::side_west, 0); return;
					}
					else if (free_diagonal && (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y - 1) &&
						(MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1] && (moster_count_side[side_nom_axis_t::side_south] >= moster_count_side[side_nom_axis_t::side_north])) ||
						(!MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1] && MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 - 1]))
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != -1)))
					{

						Actions::step_side(side_t::side_north_west, 0); return;
					}
					else if (free_diagonal && !MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y + 1) &&
						MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128 + 1]
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 1)))
					{

						Actions::step_side(side_t::side_south_west, 0); return;
					}
					else if (!MapTiles::coordinate_is_traped(current_coordinate.x - 1, current_coordinate.y)
						&& (free_count == 1 || (axis_offset.x != -1 && axis_offset.y != 0))
						&& MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128])
					{

						Actions::step_side(side_t::side_west, 0);  return;
					}
				}
			}
		}

		try_count++;
		last_coordinate_z = current_coordinate.z;

	} while (try_count < 2);
}

uint32_t HunterCore::last_coordinate_z = 0;

HunterCore::HunterCore() :timer(service), timer_refresher(service_refresher){
	var_need_operate = false;
}

bool HunterCore::get_need(uint32_t delayed_interval){
	Sleep(1);

	hunter_state state = get_state_info();

	bool pulsed = is_pulsed();

	bool temp_hunter_by_path_points = (hunter_by_path_points(true) == action_retval_t::action_retval_success);

	bool temp_state = (state.state || (!state.state && state.time_changed_state < (int32_t)delayed_interval));

	bool temp_count = get_creature_points().second > 0;

	bool temp_end = (temp_count && temp_hunter_by_path_points || temp_state);

	if (HunterManager::get()->get_wait_pulse())
		return temp_end && pulsed;
	else
		return temp_end;
}

hunter_state HunterCore::get_state_info(){
	lock_guard _lock(mtx_access);
	hunter_state retval;
	retval.state = var_need_operate;
	retval.time_changed_state = change_state_timer.elapsed_milliseconds();
	return retval;
}

void HunterCore::set_lure_state(lure_state_t in){
	current_lure_state = in;
}

lure_state_t HunterCore::get_lure_state(){
	if (!get_need(2000))
		current_lure_state = lure_state_t::lure_state_none;

	return current_lure_state;
}

void HunterCore::pathrunner(CreatureOnBattlePtr current_target, std::shared_ptr<Target> current_target_confs, TargetNonSharedPtr group_info, bool any_monster){
	if (LooterCore::get()->get_core_priority() > get_core_priority())
		return;

	if (!current_target)
		return;

	bool require_lure = WaypointManager::get()->is_require_lure();
	if (!require_lure)
		set_lure_state(lure_state_none);

	bool creature_diagonal = false;
	int creature_dist = 1;
	int creature_dist_new = 1;
	move_mode_t creature_move_mode = move_mode_t::move_mode_t_hotkey;

	if (group_info){

		if (group_info->get_shooter()){
			Actions::unset_follow();
			return;
		}

		creature_move_mode = group_info->get_move_mode_type();
		creature_dist = current_target_confs ? group_info->get_distance() : 1;
		creature_dist_new = current_target_confs ? group_info->get_distance() : 1;
		creature_diagonal = group_info ? group_info->get_diagonal() : false;
	}

	creature_dist += WaypointManager::get()->get_lure_increase_sqm_lure();

	std::shared_ptr<CreatureOnBattle> current_target_temp = BattleList::get()->get_target();
	if (current_target_temp)
		if (current_target_temp->id == current_target->id)
			current_target = current_target_temp;

	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	Coordinate target_coord = current_target->get_mixed_coordinate();

	int dist = std::max(target_coord.get_axis_min_dist(current_coordinate),
		target_coord.get_axis_max_dist(current_coordinate));

	int dif_to_keep = abs(dist - creature_dist);
	uint32_t wait_delay = (uint32_t)std::min((long long)500, (long long)
		((float)TibiaProcess::get_default()->get_ping_promedy() * 2.0f));

	if (dist < creature_dist){
		if (require_lure){
			if (last_luring_target != current_target->id){
				last_luring_target = current_target->id;
				set_lure_state(lure_state_luring);
			}

			if (get_lure_state() == lure_state_reached){
				keep_distance();

				if (dif_to_keep <= 1)
					BattleList::get()->wait_creature_out_of_range(current_target->id, creature_dist - 1, creature_dist + 1, wait_delay);
			}
			else{
				Coordinate dest_lure_coord = WaypointManager::get()->get_lure_coord();
				if (dest_lure_coord.is_null()){
					set_lure_state(lure_state_reached);
					keep_distance();

					if (dif_to_keep <= 1)
						BattleList::get()->wait_creature_out_of_range(current_target->id, creature_dist - 1, creature_dist + 1, wait_delay);
				}
				else{
					pathfinder_retval retval = Pathfinder::get()->go_with_hotkey(dest_lure_coord, false, true, map_front_mini_t);
					if (retval == pathfinder_retval::pathfinder_not_reachable){
						set_lure_state(lure_state_not_reachable);
						keep_distance();

						if (dif_to_keep <= 1)
							BattleList::get()->wait_creature_out_of_range(current_target->id, creature_dist - 1, creature_dist + 1, wait_delay);
					}
					else{
						current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
						uint32_t dist = current_coordinate.get_axis_max_dist(dest_lure_coord);
						if (dist <= 1){
							if (WaypointManager::get()->get_lure_advance_reached()){
								WaypointManager::get()->advanceCurrentWaypointInfoIgnoreLures();
								return;
							}
							else if (WaypointManager::get()->get_lua_stay_after_lure()){
								return;
							}
							else if (WaypointManager::get()->get_lure_recursive()){
								WaypointManager::get()->increase_lure_recursive_index();
								return;
							}

							set_lure_state(lure_state_reached);
						}
					}
				}
			}
		}
		else{
			keep_distance();

			if (dif_to_keep <= 1)
				BattleList::get()->wait_creature_out_of_range(current_target->id, creature_dist - 1, creature_dist + 1, wait_delay);
		}

	}
	else if (dist > creature_dist_new) {
		if (dist >= 1){
			if (creature_dist <= 1){
				if (creature_move_mode == move_mode_t::move_mode_t_follow && Pathfinder::get()->is_reachable(target_coord, map_back_mini_t, true)){
					Actions::set_follow();
					std::cout << "\n " << __FUNCTION__ << " : " << "FOLLOW";
				}
				else{
					std::cout << "\n " << __FUNCTION__ << " : " << "";
					Pathfinder::get()->go_with_hotkey(current_target->get_coordinate_monster());
				}
			}
			else{
				Pathfinder::get()->go_with_hotkey(current_target->get_coordinate_monster());
			}
		}
		else
			return;
	}
	else if (creature_diagonal) {
		uint32_t consider_near_yellow = ConfigPathManager::get()->get_consider_near_yellow_coords();
		uint32_t time_to_wait = ConfigPathManager::get()->get_time_to_wait();

		if (current_target->pos_y == current_coordinate.y){

			if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 - 1])
				Actions::step_side(side_t::side_north, 0);
			else if (MapMinimap::walkability[map_type_t::map_front_mini_t][128][128 + 1])
				Actions::step_side(side_t::side_south, 0);

		}
		else if (current_target->pos_x == current_coordinate.x){

			if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 - 1][128])
				Actions::step_side(side_t::side_west, 0);
			else if (MapMinimap::walkability[map_type_t::map_front_mini_t][128 + 1][128])
				Actions::step_side(side_t::side_east, 0);

		}
	}
}

CreatureOnBattlePtr HunterCore::creature_to_attack = nullptr;
TargetNonSharedPtr HunterCore::last_creature_group_info = nullptr;
TargetPtr HunterCore::curret_target_type = nullptr;
neutral_mutex HunterCore::mtx_runner_lock;
bool HunterCore::any_monster = false;
void HunterCore::path_runner_thread(){
	std::shared_ptr<LuaCore> hunter_lua_core = LuaCore::getState(lua_core_hunter_id);

	while (true){
		Sleep(1);

		if (!can_execute() || !TibiaProcess::get_default()->character_info->target_red())
			continue;

		if (HunterManager::get()->get_has_keep_distance_hook()){
			hunter_lua_core->RunScript(HunterManager::get()->get_keep_distance_algorithm_data(), "hunter_core.runner");

			if (hunter_lua_core->getGlobal("cancel_event") == "true")
				continue;

		}

		HunterCore::mtx_runner_lock.lock();

		CreatureOnBattlePtr local_creature_to_attack = creature_to_attack;
		TargetNonSharedPtr local_last_creature_group_info = last_creature_group_info;
		TargetPtr local_curret_target_type = curret_target_type;

		HunterCore::mtx_runner_lock.unlock();

		uint32_t consider_near_yellow = ConfigPathManager::get()->get_consider_near_yellow_coords();
		uint32_t time_to_wait = ConfigPathManager::get()->get_time_to_wait();

		gMapTilesPtr->wait_delay_near_yellow_coord(consider_near_yellow, time_to_wait);

		pathrunner(local_creature_to_attack, local_curret_target_type, local_last_creature_group_info, any_monster);
	}
}

void HunterCore::set_pathrunner_params(CreatureOnBattlePtr _creature_to_attack,
	TargetNonSharedPtr _last_creature_group_info,
	TargetPtr _curret_target_type,
	bool _any_monster){
	mtx_runner_lock.lock();

	creature_to_attack = _creature_to_attack;
	last_creature_group_info = _last_creature_group_info;
	curret_target_type = _curret_target_type;
	any_monster = _any_monster;

	mtx_runner_lock.unlock();
}

std::pair<int32_t, int32_t> HunterCore::get_creature_points(){
	int32_t points = 0;
	int32_t creature_count = 0;
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_mixed_coordinate();
	auto creatures = BattleList::get()->get_creatures_on_screen();
	bool is_any = HunterManager::get()->get_any_monster();
	for (auto creature : creatures){
		if (!creature.second->is_monster())
			continue;
		uint32_t distance = creature.second->get_mixed_coordinate().get_axis_max_dist(current_coord);
		auto target_info = HunterManager::get()->get_target_by_values(creature.second->name,
			creature.second->life, distance);

		if (!target_info.first || !target_info.second){
			if (!is_any){
				continue;
			}
			else{

				if (Pathfinder::get()->is_reachable(creature.second->get_coordinate_monster(), map_front_mini_t, true)){
					points += 1;
					creature_count++;
				}
				continue;
			}
		}
		else
		{
			if (Pathfinder::get()->is_reachable(creature.second->get_coordinate_monster(), map_front_mini_t, true)){
				points += target_info.second->get_point();
				creature_count++;
			}
		}


	}
	return std::pair<int32_t, int32_t>(points, creature_count);
}

void HunterActions::stop_attack(){
	if (!TibiaProcess::get_default()->character_info->target_red())
		return;

	TimeChronometer time;
	auto input = HighLevelVirtualInput::get_default();
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	input->send_esc();
}

std::pair<TargetPtr, TargetNonSharedPtr> HunterManager::get_match_creature(char* str, uint32_t life_percent,
	uint32_t dist, bool pathable){
	for (auto target : target_rules){
		if (_stricmp(str, &target.second->get_name()[0]) == 0){
			auto retval = target.second->get_group_match(life_percent, dist, pathable);
			if (retval){
				retval->get_target_range_max();
				return std::pair<TargetPtr, TargetNonSharedPtr>(target.second, retval);
			}
		}
	}
	return std::pair<TargetPtr, TargetNonSharedPtr>(nullptr, nullptr);
}

uint32_t CreatureOnBattle::get_distance(){
	return TibiaProcess::get_default()->character_info->get_mixed_coordinate().get_axis_max_dist(get_mixed_coordinate());
}

bool HunterCore::check_creature_die(uint32_t creature_id, uint32_t timeout){
	return TimeChronometer::wait_condition(
		[&](){
		CreatureOnBattlePtr creature = BattleList::get()->get_creature_by_id(creature_id);
		if (!creature)
			return true;

		if (!creature->on_screenNew)
			return true;

		return false;
	}
	, timeout);
}

void HunterCore::invoke(){
	static uint32_t last_creature = 0;

	invoked_count++;

	//TODO ISSO PODE SER MELHODADO TANTO QUANTO NA FUNCAO DE POINTER, COMO NA FUNCAO DE COMECAR PROCURAR O TARGET PELO MAIOR PRIORIDADE
	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_mixed_coordinate();

	TimeChronometer time_;
	TargetPtr curret_target_type_func = nullptr;
	TargetNonSharedPtr current_creature_group_info = nullptr;
	std::shared_ptr<Target> last_target_found = nullptr;
	CreatureOnBattlePtr creature_to_attack_func = nullptr;
	TargetNonSharedPtr last_creature_group_info_func = nullptr;

	core_priority waypointer_p = WaypointerCore::get()->get_core_priority();
	core_priority hunter_p = get_core_priority();

	if (waypointer_p > hunter_p)
		return;

	if (!can_execute())
		return;

	//BattleList::get()->wait_refresh();

	std::shared_ptr<CreatureOnBattle> current_target = BattleList::get()->get_target();
	if (current_target)
		last_creature = current_target->id;

	if (hunter_by_path_points(true) == action_retval_t::action_retval_fail){
		set_need(false);
		return;
	}

	if (HunterActions::get()->has_temporary){
		uint32_t temp_id = HunterActions::get()->get_temporary_target_id();

		creature_to_attack_func = BattleList::get()->find_creature_by_id(temp_id);
		if (!creature_to_attack_func){
			set_need(false);
			return;
		}

		if (temp_id){
			set_need(true);

			std::pair<TargetPtr, TargetNonSharedPtr> info =
				HunterManager::get()->get_target_by_values(creature_to_attack_func->name, creature_to_attack_func->life,
				current_coordinate.get_axis_max_dist(creature_to_attack_func->get_coordinate_monster()));

			HunterActions::get()->attack_creature_id(temp_id);
			set_pathrunner_params(creature_to_attack_func, info.second, info.first, true);
		}
		else
			set_need(false);

		return;
	}

	current_target = BattleList::get()->get_target();
	if (current_target) {
		uint32_t distance = current_target->get_coordinate_monster().get_axis_max_dist(current_coordinate);
		auto group_creature_info = HunterManager::get()->get_match_creature(current_target->name, current_target->life, distance, true);
		curret_target_type_func = group_creature_info.first;
		current_creature_group_info = group_creature_info.second;
	}

	uint32_t last_dist = UINT32_MAX;
	uint32_t last_order = 0;

	BattleList::get()->mtx_access.lock();
	std::map<int, std::shared_ptr<CreatureOnBattle>> creatures_copy = BattleList::get()->get_creatures_on_screen();
	BattleList::get()->mtx_access.unlock();

	if (HunterManager::get()->get_any_monster()){
		for (auto creature : creatures_copy) {
			if (!creature.second->is_monster() || creature.second->is_summon())
				continue;

			if (current_target)
				break;

			uint32_t dist = current_coordinate.get_axis_max_dist(creature.second->get_coordinate_monster());
			bool pathable = Pathfinder::get()->is_reachable(creature.second->get_coordinate_monster(), map_front_mini_t, true);
			if (!pathable)
				continue;

			if (current_coordinate.get_axis_min_dist(creature.second->get_coordinate_monster())	< last_dist){
				auto group_creature_info = HunterManager::get()->get_match_creature(creature.second->name, creature.second->life, dist, pathable);
				if (group_creature_info.first && group_creature_info.second){
					if (last_order > group_creature_info.second->get_order())
						continue;

					last_order = group_creature_info.second->get_order();
					last_dist = current_coordinate.get_axis_min_dist(creature.second->get_coordinate_monster());
					creature_to_attack_func = creature.second;
				}
				else{
					last_order = 0;
					last_dist = current_coordinate.get_axis_min_dist(creature.second->get_coordinate_monster());
					creature_to_attack_func = creature.second;
				}
			}
		}
	}
	else{
		for (auto creature : creatures_copy) {
			if (!creature.second->is_monster() || creature.second->is_summon())
				continue;

			uint32_t dist = current_coordinate.get_axis_max_dist(creature.second->get_coordinate_monster());
			bool pathable = Pathfinder::get()->is_reachable(creature.second->get_coordinate_monster(), map_front_t, true);
			std::pair<TargetPtr, TargetNonSharedPtr> group_creature_info =
				HunterManager::get()->get_match_creature(creature.second->name, creature.second->life, dist, pathable);

			if (!group_creature_info.first || !group_creature_info.second)
				continue;

			if (current_creature_group_info && current_creature_group_info->get_order() >= 	group_creature_info.second->get_order())
				continue;

			if (last_creature_group_info_func && last_creature_group_info_func->get_order() > group_creature_info.second->get_order())
				continue;

			bool has_to_continue = true;
			if (!last_creature_group_info_func)
				has_to_continue = false;
			else if (last_creature_group_info_func->get_order() > group_creature_info.second->get_order())
				has_to_continue = false;
			else if (current_coordinate.get_axis_min_dist(creature.second->get_coordinate_monster()) < last_dist)
				has_to_continue = false;

			if (has_to_continue)
				continue;

			last_dist = current_coordinate.get_axis_min_dist(creature.second->get_coordinate_monster());
			last_target_found = group_creature_info.first;
			creature_to_attack_func = creature.second;
			last_creature_group_info_func = group_creature_info.second;
			std::cout << "\n " << __FUNCTION__ << ": " << "SET CURRENT CREATURE | TIME: "  << time_.elapsed_milliseconds() ;
		}
	}

	if (!last_creature_group_info_func && TibiaProcess::get_default()->character_info->character_id() == 0)
		unset_pulse();

	if (last_creature_group_info_func){
		Actions::change_weapon(last_creature_group_info_func->get_weapon());

		uint32_t min_point = HunterManager::get()->get_min_points_to_change();
		attack_mode_t newMode = HunterManager::get()->get_new_mode();
		attack_mode_t oldMode = HunterManager::get()->get_old_mode();

		change_attack_mode(min_point, newMode, oldMode);

		set_need(true);

		set_pathrunner_params(creature_to_attack_func, last_creature_group_info_func, last_target_found, true);

		if (last_creature == creature_to_attack_func->id && check_creature_die(creature_to_attack_func->id))
			return;

		HunterActions::get()->attack_creature_id(creature_to_attack_func->id, std::function<bool()>(), 200);
		
		std::cout << "\n " << __FUNCTION__ << ": " << "ATTACK | TIME: " << time_.elapsed_milliseconds();
	}
	else if (curret_target_type_func && current_target && current_creature_group_info){
		Coordinate target_coordinate = current_target->get_coordinate_monster();		
		if (Pathfinder::get()->is_reachable(target_coordinate, map_front_mini_t,true)) {
			time_to_not_reachable.reset();

			set_need(true);

			set_pathrunner_params(current_target, current_creature_group_info, curret_target_type_func, true);
		}
		else{
			if (time_to_not_reachable.elapsed_milliseconds() > 2000)
				HunterActions::get()->stop_attack();
		}
	}
	else{
		if (HunterManager::get()->get_any_monster() && creature_to_attack_func){
			set_need(true);

			set_pathrunner_params(creature_to_attack_func, last_creature_group_info_func, last_target_found, true);

			if (last_creature == creature_to_attack_func->id && check_creature_die(creature_to_attack_func->id))
				return;

			HunterActions::get()->attack_creature_id(creature_to_attack_func->id, std::function<bool()>(), 200);
			std::cout << "\n " << __FUNCTION__ << ": " << "ATTACK | TIME: " << time_.elapsed_milliseconds();
		}
		else{
			std::vector<CreatureOnBattlePtr> creature = BattleList::get()->get_creatures_near_coordinate(current_coordinate);
			if (creature.size() - 1 <= 0){
				set_need(false);
			}
			else{
				if (!MapMinimap::is_trapped_around(map_front_mini_t)){
					pulse();

					for (auto it : creature){
						HunterActions::get()->add_temporary_target(it->id, 5000, std::bind([](uint32_t creature_id, Coordinate creature_current_coord) -> bool {
							CreatureOnBattlePtr creature = BattleList::get()->find_creature_by_id(creature_id);
							if (!creature)
								return false;

							if (MapMinimap::is_trapped_around(map_front_t) == false)
								return false;

							return creature->get_coordinate_monster() == creature_current_coord;
						}, it->id, it->get_coordinate_monster()));
					}

				}
				else{
					set_need(false);
				}
			}
		}
		//set_need(false);
	}
}

void HunterCore::unset_pulse(){
	lock_guard _mlock(mtx_access);
	m_is_pulsed = false;
}

bool HunterCore::is_pulsed(){
	lock_guard _mlock(mtx_access);
	return m_is_pulsed;
}

void HunterCore::pulse(){
	lock_guard _mlock(mtx_access);
	m_is_pulsed = true;
}

bool HunterCore::wait_kill_all_complete(uint32_t timeout){
	TimeChronometer timer;
	while (get_need() && NeutralManager::get()->get_hunter_state()){
		if ((uint32_t)timer.elapsed_milliseconds() > timeout)
			return false;

		Sleep(50);
	}
	return true;
}

bool HunterCore::wait_pulse_out(uint32_t timeout){
	TimeChronometer timer;
	while (true){
		if (!NeutralManager::get()->get_hunter_state())
			return false;

		if (!m_is_pulsed)
			return true;

		if (!need_operate())
			return true;

		if ((uint32_t)timer.elapsed_milliseconds() > timeout)
			return false;

		Sleep(50);
	}
	return true;
}

bool HunterCore::wait_pulse(uint32_t timeout){
	TimeChronometer timer;
	while (true){
		if (!m_is_pulsed)
			return true;

		if (!NeutralManager::get()->get_hunter_state())
			return false;

		if ((uint32_t)timer.elapsed_milliseconds() > timeout)
			return false;

		Sleep(50);
	}
	return true;
}

void HunterCore::run(){
	std::thread([&](){
		MONITOR_THREAD("HunterCore::run")
			start();
	}).detach();
}

void HunterCore::run_refresher(){
	start_refresher();

	std::thread([&](){
		MONITOR_THREAD("HunterCore::run_refresher_service")
			service_refresher.run();
	}).detach();

	std::thread([&](){
		MONITOR_THREAD("HunterCore::path_runner_thread")
			path_runner_thread();
	}).detach();

}

void HunterCore::init(){
	mHunterCore = new HunterCore;
	mHunterCore->run();
	mHunterCore->run_refresher();
}

HunterCore* HunterCore::mHunterCore = nullptr;

void HunterCore::set_need(bool state){
	lock_guard _lock(mtx_access);
	if (state != var_need_operate){
		change_state_timer.reset();
		var_need_operate = state;
	}

	if (!state){
		if (m_is_pulsed){
			if (change_state_timer.elapsed_milliseconds() > 500) {
				m_is_pulsed = false;
			}
		}
	}
}

Coordinate CreatureOnBattle::get_mixed_coordinate(){
	Coordinate displaced = get_displaced_coordinate();
	if (displaced.is_null()){
		return get_coordinate_monster();
	}
	return displaced;
}

Coordinate CreatureOnBattle::get_displaced_coordinate(){
	if (!is_walking())
		return Coordinate();

	Coordinate displacement = get_axis_displacement();
	if (displacement.is_null())
		return Coordinate();
	Coordinate self_coordinate = get_coordinate_monster();
	Coordinate retval;
	if (displacement.x || displacement.y){
		displacement.z = 0;
		if (displacement.x)
			displacement.x = ((displacement.x > 0) ? -1 : 1);

		if (displacement.y)
			displacement.y = ((displacement.y > 0) ? -1 : 1);

		retval = self_coordinate + displacement;
	}
	return retval;
}

bool CreatureOnBattle::is_walking(){
	return (dist_from_center_y != 0 || dist_from_center_x != 0);
}

DistanceParamRowCoordinate::DistanceParamRowCoordinate(Coordinate c, uint32_t d) :
DistanceParamRowBase(distance_param_row_t::distance_param_coordinate){
	set_danger(d);
	coord = c;
}

DistanceParamRowCreature::DistanceParamRowCreature(std::string creature_n, uint32_t d) :
DistanceParamRowBase(distance_param_row_t::distance_param_creature){
	danger = d;
	creature_name = creature_n;
}

void HunterCore::wait_invoke_count(){
	uint64_t invoke_cout = invoked_count;

	while (invoked_count == invoke_cout)
		Sleep(1);
}

BattleList* BattleList::get(){
	static BattleList* mBattleList = nullptr;
	if (!mBattleList){
		mBattleList = new BattleList;
	}
	return mBattleList;
}

DistanceParamRowBase::DistanceParamRowBase(distance_param_row_t t){
	type = t;
}

distance_param_row_t DistanceParamRowBase::get_type(){
	return type;
}

void DistanceParamRowBase::set_type(distance_param_row_t t){
	type = t;
}

void DistanceParamRowBase::set_danger(uint32_t d){
	danger = d;
}

uint32_t DistanceParamRowBase::get_danger(){
	return danger;
}

void HunterCore::change_attack_mode(uint32_t min_points, attack_mode_t new_mode, attack_mode_t old_mode){
	if (min_points <= 0)
		return;

	if (new_mode == attack_mode_t::attack_mode_no_change || old_mode == attack_mode_t::attack_mode_no_change)
		return;

	if (get_creature_points().first < (int32_t)min_points)
		PvpManager::get()->set_offensive_attack_mode();
	else
		PvpManager::get()->set_defensive_attack_mode();
}

action_retval_t HunterCore::hunter_by_path_points(bool check_looter){
	uint32_t path_index_one = HunterManager::get()->get_path_index();
	uint32_t path_index_two = HunterManager::get()->get_path_index_two();
	uint32_t min_points = HunterManager::get()->get_min_points();

	std::shared_ptr<WaypointPath>  path_rule_one = WaypointManager::get()->getWaypointPath(path_index_one);
	std::shared_ptr<WaypointPath>  path_rule_two = WaypointManager::get()->getWaypointPath(path_index_two);

	std::shared_ptr<CreatureOnBattle> current_target = BattleList::get()->get_target();

	uint32_t current_path = WaypointManager::get()->get_currentWaypointPathId();

	if (min_points <= 0)
		return action_retval_t::action_retval_missing_parameter;

	if (!path_rule_one && !path_rule_two)
		return action_retval_t::action_retval_missing_parameter;

	if (current_path == path_index_one){
		if (!path_rule_one)
			return action_retval_t::action_retval_missing_parameter;

		if (current_target)
			return action_retval_t::action_retval_success;

		if (check_looter)
		if (LooterCore::get()->need_operate())
			return action_retval_t::action_retval_success;

		if ((uint32_t)get_creature_points().first > min_points)
			return action_retval_t::action_retval_success;

		return action_retval_t::action_retval_fail;
	}
	else if (current_path == path_index_two){
		if (!path_rule_two)
			return action_retval_t::action_retval_missing_parameter;

		if (current_target)
			return action_retval_t::action_retval_success;

		if (check_looter)
		if (LooterCore::get()->need_operate())
			return action_retval_t::action_retval_success;

		if ((uint32_t)get_creature_points().first > min_points)
			return action_retval_t::action_retval_success;

		return action_retval_t::action_retval_fail;
	}

	return action_retval_t::action_retval_success;
}