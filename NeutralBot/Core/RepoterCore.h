#pragma once
#include "Actions.h"

class RepoterCore{
public:
	
	static bool open_trade(std::vector<std::string> words);

	static int get_item_count_id(int id);

	static bool buy_item_repot_id(std::string repotId, int timeout = 2000);

	static int total_money_all_repot();

	static int total_money_repot(std::string id);

	static bool is_necessary_all_repot();

	static bool is_necessary_repot(std::string id);

	static bool withdraw_repot();

	static bool sell_empty_vials();
};
