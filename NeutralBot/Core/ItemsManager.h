#pragma once 

#include "Util.h"
#include "FileHelper.h"
#include "Neutral.h"
#include "..\\ConfigPathManager.h"
#include "constants.h"
#include "..\\stringmap.h"

class Size{
public:
	Size(){
		width = 0;
		height = 0;
	}
	Size(int in_width, int in_height){
		width = in_width;
		height = in_height;
	}
	int width, height;

	int area(){
		return width * height;
	}
};

class any {
public:
	struct placeholder {
		virtual ~placeholder()  { }
		virtual const std::type_info& type() const = 0;
		virtual placeholder* clone() const = 0;
	};

	template<typename T>
	struct holder : public placeholder {
		holder(const T& value) : held(value) { }
		const std::type_info& type() const { return typeid(T); }
		placeholder* clone() const { return new holder(held); }
		T held;
	
		holder& operator=(const holder &);
	};

	placeholder* content;

	any() : content(nullptr) { }
	any(const any& other) : content(other.content ? other.content->clone() : nullptr) { }
	template<typename T> any(const T& value) : content(new holder<T>(value)) { }
	~any() { if (content) delete content; }

	any& swap(any& rhs) { std::swap(content, rhs.content); return *this; }

	template<typename T> any& operator=(const T& rhs) { any(rhs).swap(*this); return *this; }
	any& operator=(any rhs) { rhs.swap(*this); return *this; }

	bool empty() const { return !content; }
	template<typename T> const T& cast() const;
	const std::type_info & type() const { return content ? content->type() : typeid(void); }
};

template<typename T>
const T& any_cast(const any& operand) {
	assert(operand.type() == typeid(T));
	return static_cast<any::holder<T>*>(operand.content)->held;
}
template<typename T> const T& any::cast() const { return any_cast<T>(*this); }

template<typename Key>
class dynamic_storage {
public:
	template<typename T> void set(const Key& k, const T& value) {
		if (m_data.size() <= k)
			m_data.resize(k + 1);
		m_data[k] = value;
	}
	bool remove(const Key& k) {
		if (m_data.size() < k)
			return false;
		if (m_data[k].empty())
			return false;
		m_data[k] = any();
		return true;
	}
	template<typename T> T get(const Key& k) const { return has(k) ? any_cast<T>(m_data[k]) : T(); }
	bool has(Key& k) { return k < m_data.size() && !m_data[k].empty(); }
	bool has(const Key& k)const { return k < m_data.size() && !m_data[k].empty(); }

	
	std::size_t size() const {
		std::size_t count = 0;
		for (std::size_t i = 0; i<m_data.size(); ++i)
			if (!m_data[i].empty())
				count++;
		return count;
	}

	void clear() { m_data.clear(); }


	std::vector<any> m_data;
};

struct MarketData {
	std::string name;
	int category;
	uint16_t requiredLevel;
	uint16_t restrictVocation;
	uint16_t showAs;
	uint16_t tradeAs;
};

struct Light {
	Light() { intensity = 0; color = 215; }
	uint8_t intensity;
	uint8_t color;
};

class ThingType{
public:

	bool m_null;
	uint32_t m_id;
	ThingCategory m_category;
	dynamic_storage<uint8_t>  m_attribs;
	uint8_t width;
	uint8_t height;
	void ThingType::unserialize(uint16_t clientId, ThingCategory category, std::shared_ptr< FileReader> fin);

	std::string get_item_name(){
		if (m_attribs.has(ThingAttrMarket))
			return m_attribs.get<MarketData>(ThingAttrMarket).name;
		return "";
	}
};

typedef std::shared_ptr<ThingType> ThingTypePtr;

class TibiaFileParser{
public:
	ThingTypePtr m_nullThingType;
	std::vector<ThingTypePtr> m_thingTypes[ThingLastCategory];
	std::vector<ThingTypePtr> m_thingTypesCustom;
	std::shared_ptr<stringmap<ThingTypePtr>> m_thingTypesStringKey;
	TibiaFileParser();

	void updateStringMap();
	void sort();
	bool LoadDat(std::string file);

	static TibiaFileParser* get();
}; 

class CustomIdAttributes
{
	attr_item_t type;
	std::map<attr_additional_t, std::string> additionals;
public:
	CustomIdAttributes(attr_item_t _type){
		type = _type;
	}

	CustomIdAttributes(){
		type = attr_item_t::attr_item_none;
	}

	attr_item_t get_type(){
		return type;
	}

	void set_type(attr_item_t _type){
		type = _type;
	}

	bool contains_additional(attr_additional_t additional_attr){
		auto res = additionals.find(additional_attr);
		if (res != additionals.end()){
			return true;
		}
		return false;
	}

	std::string get_additional(attr_additional_t additional_attr){
		auto res = additionals.find(additional_attr);
		if (res != additionals.end()){
			return res->second;
		}
		return "";
	}

	void set_additional(attr_additional_t additional_attr, std::string value){
		additionals[additional_attr] = value;
	}
};

class CustomIdDetails{
	std::vector<CustomIdAttributes*> attributes;
public:
	uint32_t id;
	std::string name;
	void add_attribute(attr_item_t attr);
	
	void add_attribute_optional(attr_item_t type, attr_additional_t additional, std::string value);

	CustomIdAttributes* get_attribute(attr_item_t type, bool create = false);

	bool contains_attribute(attr_item_t attr);
};

class CustomIdAttributesManager{
	std::map<uint32_t, CustomIdDetails* > attributes_id;
	void loadFile();
	static attr_item_t attr_item_type_from_string(char* str);
	static attr_additional_t attr_additional_from_string(char* str);
public:
	CustomIdAttributesManager(){
		loadFile();
	}
	CustomIdDetails* get_item_attributes(uint32_t id){
		auto found = attributes_id.find(id);
		if (found != attributes_id.end())
			return found->second;
		return nullptr;
	}
	static CustomIdAttributesManager* get(){
		static CustomIdAttributesManager* mCustomIdAttributesManager = nullptr;
		if (!mCustomIdAttributesManager){
			mCustomIdAttributesManager = new CustomIdAttributesManager;
		}
		return mCustomIdAttributesManager;
	}
};

#pragma pack(push,1)
class ItemsManager{
public:
	TibiaFileParser* dat_parser;

	ItemsManager();

	bool is_food_item_id(uint32_t id){
		return std::find(items_as_food.begin(), items_as_food.end(), id) != items_as_food.end();
	}


	void load_custom();
	bool load(std::string file_path);

	ThingTypePtr getItemTypeById(uint32_t id);

	ThingTypePtr getItemTypeByName(std::string name);

	std::vector<ThingTypePtr> getItemsByAttributes(std::vector<ThingAttr> types);

	std::vector<ThingTypePtr> getItemsByAttribute(ThingAttr types);

	bool isitem_id(uint32_t id); 
	bool is_item(uint32_t id);
	bool isRuneFieldItem(uint32_t id);

	std::shared_ptr<ConfigPathProperty> get_path_property_by_id(uint32_t id);
	
	int getItemPrice(uint32_t id);

	int getitem_idFromName(std::string name);

	std::string getItemNameFromId(uint32_t id);

	void addItem(int id, std::string name, std::vector<ThingAttr> attributes = std::vector<ThingAttr>());

	std::vector<uint32_t> get_depot_ids();

	std::vector<uint32_t> get_depot_box_ids();

	std::vector<uint32_t> get_depot_tiles_ids();

	std::vector<std::string> get_containers();

	std::vector<std::string> get_itens();

	std::vector<std::string> get_potions();

	std::vector<std::string> get_runes();

	std::vector<std::string> get_depot_boxs(){
		std::vector<std::string> retval = { "Depot Chest", "Depot Box I", "Depot Box II", "Depot Box III", "Depot Box IV"
			, "Depot Box V", "Depot Box VI", "Depot Box VII", "Depot Box VIII", "Depot Box IX", "Depot Box X", "Depot Box XI"
			, "Depot Box XII", "Depot Box XIII", "Depot Box XIV", "Depot Box XV", "Depot Box XVI", "Depot Box XVII" };

		return retval;
	}

	std::vector<std::string> get_ammunitions();

	bool is_furniture_id(uint32_t item_id);

	bool is_pick_item(uint32_t item_id);

	bool is_rope_item(uint32_t item_id);

	bool is_shovel_item(uint32_t item_id);

	bool is_machete_item(uint32_t item_id);

	bool is_kitchen_item(uint32_t item_id);

	bool is_knife_item(uint32_t item_id);

	bool is_spoon_item(uint32_t item_id);

	bool is_croowbar_item(uint32_t item_id);

	bool is_scythe_item(uint32_t item_id);

	bool is_sickle_item(uint32_t item_id);

	std::vector<uint32_t> items_as_pick;
	std::vector<uint32_t> items_as_rope;
	std::vector<uint32_t> items_as_shovel;
	std::vector<uint32_t> items_as_machete;
	std::vector<uint32_t> items_as_container;
	std::vector<uint32_t> items_as_kitchen;
	std::vector<uint32_t> items_as_knife;
	std::vector<uint32_t> items_as_spoon;
	std::vector<uint32_t> items_as_crowbar;
	std::vector<uint32_t> items_as_scythe;
	std::vector<uint32_t> items_as_sickle;
	std::vector<uint32_t> items_as_take_skin;
	std::vector<uint32_t> items_as_furniture;
	std::vector<uint32_t> items_as_food;
	std::vector<uint32_t> items_as_empty_potions;
	std::vector<uint32_t> items_as_fields_runes;

	std::vector<uint32_t>& get_items_as_pick(){ return items_as_pick; };
	std::vector<uint32_t>& get_items_as_rope(){ return items_as_rope; };
	std::vector<uint32_t>& get_items_as_shovel(){ return items_as_shovel; };
	std::vector<uint32_t>& get_items_as_take_skin(){ return items_as_take_skin; };
	std::vector<uint32_t>& get_items_as_machete(){ return items_as_machete; };
	std::vector<uint32_t>& get_items_as_kitchen(){ return items_as_kitchen; };
	std::vector<uint32_t>& get_items_as_knife(){ return items_as_knife; };
	std::vector<uint32_t>& get_items_as_spoon(){ return items_as_spoon; };
	std::vector<uint32_t>& get_items_as_crowbar(){ return items_as_crowbar; };
	std::vector<uint32_t>& get_items_as_scythe(){ return items_as_scythe; };
	std::vector<uint32_t>& get_items_as_sickle(){ return items_as_sickle; };
	std::vector<uint32_t>& get_items_as_food(){ return items_as_food; };
	std::vector<uint32_t>& get_items_as_empty_potions(){ return items_as_empty_potions; };
	std::vector<uint32_t>& get_items_as_fields_runes(){ return items_as_fields_runes; };

	bool is_lying_corpse(uint32_t item_id);

	static ItemsManager* get();
};
#pragma pack(pop)

