#pragma once
#include "Util.h"
#include "..\WaypointManager.h"
#include "TibiaProcess.h"
#include "Neutral.h"
#include "LooterCore.h"
#include "..\CoreBase.h"
#include "..\DepoterManager.h"
#include "..\RepoterManager.h"



class WaypointerCore : public CoreBase{ 
	std::shared_ptr<WaypointInfo> last_waypoint_info_ptr; 
	std::shared_ptr<WaypointInfo> current_waypoint_info_ptr;
	bool running;
	
	int tryCountMax = 3;
	neutral_mutex mtx_access;
	
	std::vector<std::string> a;

	Coordinate character_coord;
	pathfinder_state last_callback_retval;
	std::map<std::string, std::string> volatile_waypoint_values;

	TimeChronometer time_to_pause;
	int timeout_to_pause = 0;

	void set_volatile_value(std::string key, uint32_t value);

	void set_volatile_value(std::string key, bool value);

	void set_volatile_value(std::string key, std::string value);

	int get_volatile_int_value(std::string key);

	std::string get_volatile_string_value(std::string key);

	bool get_volatile_bool_value(std::string key);

	void execute_lua(std::shared_ptr<WaypointInfo> waypointer_ptr);
	void run();
public:
	void set_time_to_pause(int timeout){
		time_to_pause.reset();
		timeout_to_pause = timeout;
	}
	void on_waypoint_changed();
	void check_waypoint_changed();
	WaypointerCore();

	bool check_update_state(); 
	void start();
	void invoke();
	/*
		returns if this core needs some type of actions, example if the looter need to loot something then it will return true;
	*/

	bool need_operate(){
		return false;
	}

	bool can_execute();

	void set_label(std::string label);
	void set_waypoint_path(std::string path, std::string waypointInfo);

	Coordinate get_offset_to_current_side(side_t::side_t type);

	void set_go_to(Coordinate coord, uint32_t consider_near = 1, map_type_t map_type = map_front_t, std::function<bool()> force_arrow = std::function<bool()>(), uint32_t arrow_delay = 0);
	bool is_rearched(Coordinate coord, int consider_near = 0);
	void process_waypoint_t(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void internal_update_character_pos();
	bool is_near_destination(uint32_t sqm_consider_near = 1);

	void execute_waypoint_walk_to(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_message(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_message_npc(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_lua_script(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_use(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_hole_or_step(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_exani_hur_up(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_exani_hur_down(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_check_necesary_deposit_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_check_necesary_deposit_any_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_check_necesary_repot_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_check_necesary_repot_any_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_withdraw_value(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_travel(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	
	void execute_waypoint_deposit_items_by_depot_id(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_deposit_items_all_depot(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_repot_from_depot_by_id(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_withdraw_all_repots_if_fail_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_repot_here_by_id(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_rope(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_shovel(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_pick(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_machete(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_scythe(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_tool_id(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_use_item(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_remove_item(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_wait_milliseconds(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_up_ladder(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_use_item_id_to_down_or_up(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_reopen_containers(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_close_containers(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_loggout_this_location(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_close_tibia_this_location(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_if_way_block_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_use_lever(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_rearch_creature(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_teleport_else_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_walk_to_random(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_take_item_up_to_from_depot(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_check_cap_less_than_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_destroy_field(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_open_door(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_go_to_waypoint_path(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_lure_here_distance(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_lure_here_knight_mode(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	void execute_waypoint_drop_item(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_go_to_next_path(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_sell_items_by_sell_id(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_sell_all_items(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_auto_haste(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_auto_utura(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_cast_spell(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_pulse_target(std::shared_ptr<WaypointInfo> waypointInfoPtr);
	void execute_waypoint_pulse_looter(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	// TODO : Fazer
	void execute_waypoint_deposit_money(std::shared_ptr<WaypointInfo> waypointInfoPtr);

	bool is_location(uint32_t sqm_dist = 3);

	static WaypointerCore* mWaypointerCore;
	static void init();
	
	static WaypointerCore* get();

	void check_broke_trap();
};





