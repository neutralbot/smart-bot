#pragma once
#include "WaypointerCore.h"
#include "Pathfinder.h"
#include "Actions.h"
#include "..\\SellCore.h"
#include "SpellcasterCore.h"
#include "..\\WaypointManager.h"
#include "HunterCore.h"
#include "..\\SellManager.h"
#include "..\\TakeSkinCore.h"
#include "..\\TakeSkinManager.h"
#include "AdvancedRepoterCore.h"
#include "DepoterCore.h"
#include "RepoterCore.h"
#include <thread>
#include "Util.h" 
#include "LuaCore.h"
#include "..\\DevelopmentManager.h"


//#include <future>
#pragma region MACROS
/*
we didn't put inside singleton get class cuzz this need be initialized with io_service
*/
#define WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY 1

/*
	Advances current waypoint
	*/
#define WAYPOINTER_ADVANCE_NEXT(wait_time)\
	WaypointManager::get()->advanceCurrentWaypointInfo();\
	Sleep(wait_time);

/*
	Advances if is not location near 2
	*/
#define WAYPOINTER_CHECK_ADVANCE_LOCATION(wait_time)\
	if(!is_location()){\
	WAYPOINTER_ADVANCE_NEXT(wait_time)\
	return;\
			}\

/*
	check z, if z differs then it advance waypoint
	*/
#define WAYPOINTER_CHECK_Z(wait_time)\
	internal_update_character_pos();\
	if (waypointInfoPtr->get_position_z() != character_coord.z){\
		WAYPOINTER_ADVANCE_NEXT(wait_time)\
		return;\
		}\

/*
	verify if current waypoint destination has rearched and advances
	*/
#define WAYPOINTER_CHECK_REARCHED(wait_time, distance_consider_rearched)\
	internal_update_character_pos();\
	if (is_near_destination(distance_consider_rearched)){\
		WAYPOINTER_ADVANCE_NEXT(wait_time)\
		}

#define WAYPOINTER_CHECK_OVERRIDEN_CANCELED()\
	if (!can_execute())\
		return; \
	if (last_callback_retval == pathfinder_walk_overriden || last_callback_retval == pathfinder_walk_canceled){\
		return;\
		}

#define WAYPOINTER_CHECK_UNREARCHABLE_NEXT(wait_time)\
if (last_callback_retval == pathfinder_walk_not_reachable) {\
	WAYPOINTER_ADVANCE_NEXT(wait_time);\
	return;\
}

#define WAYPOINTER_CHECK_LOCATION(radius)\
	if (!WaypointerCore::get()->is_location(radius)){\
	WAYPOINTER_ADVANCE_NEXT(0); \
			return; \
		}
#pragma endregion

void WaypointerCore::execute_lua(std::shared_ptr<WaypointInfo> waypointer_ptr){
	LuaCore::getState(lua_core_ids::lua_core_waypoint_id)->RunScript(waypointer_ptr->get_additionalInfo("script"), "waypointer");
} 

WaypointerCore* WaypointerCore::mWaypointerCore = nullptr;

void WaypointerCore::set_go_to(Coordinate coord, uint32_t consider_near, map_type_t map_type, std::function<bool()> force_arrow_condition, uint32_t arrow_delay){
	last_callback_retval = (*(Pathfinder::get())).set_to_go_while(coord, consider_near, map_type, pathfinder_walk_going,
		std::bind(&WaypointerCore::can_execute, this), false, 5, 100, force_arrow_condition, arrow_delay);
}

bool WaypointerCore::is_rearched(Coordinate coord, int consider_near) {
	Coordinate self_pos = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (self_pos != coord && last_callback_retval == pathfinder_walk_not_reachable && self_pos.get_axis_max_dist(coord) > (uint32_t)consider_near)
		return false;

	last_callback_retval = pathfinder_walk_reached;
	return true;
}

bool WaypointerCore::is_near_destination(uint32_t sqm_consider_near){
	if (!current_waypoint_info_ptr)
		return false;

	internal_update_character_pos();

	if (current_waypoint_info_ptr->get_destination_coord().get_axis_max_dist(character_coord) <= sqm_consider_near)
		return true;

	return false;
}

Coordinate WaypointerCore::get_offset_to_current_side(side_t::side_t type){
	switch (type){
	case side_t::side_t::side_center:
		return Coordinate(0, 0, 0);
		break;
	case side_t::side_t::side_east:
		return Coordinate(1, 0, 0);
		break;
	case side_t::side_t::side_north:
		return Coordinate(0, -1, 0);
		break;
	case side_t::side_t::side_south:
		return Coordinate(0, 1, 0);
		break;
	case side_t::side_t::side_west:
		return Coordinate(-1, 0, 0);
		break;
	default:
		return Coordinate(0, 0, 0);
		break;
	}
	return Coordinate(0, 0, 0);
}

void WaypointerCore::internal_update_character_pos(){
	character_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
}

bool WaypointerCore::can_execute(){
	if (NeutralManager::get()->is_paused() || !NeutralManager::get()->get_bot_state())
		return false;

	if (NeutralManager::get()->get_core_states(CORE_WAYPOINTER) != ENABLED)
		return false;

	if (NeutralManager::get()->get_core_states(CORE_HUNTER) == ENABLED && HunterCore::get()->get_need(100))
		return false;			
	
	if (SpellCasterCore::get()->need_operate_waypoint())
		return false;

	if (NeutralManager::get()->get_core_states(CORE_LOOTER) == ENABLED && LooterCore::get()->need_operate())
		return false;

	if (NeutralManager::get()->get_core_states(CORE_LOOTER) == ENABLED && LooterCore::get()->is_looting())
		return false;

	if (TakeSkinManager::get()->get_take_skin_state() && TakeSkinCore::get()->need_operate())
		return false;

	if (time_to_pause.elapsed_milliseconds() < timeout_to_pause)
		return false;

	return true;
}

void WaypointerCore::set_label(std::string label) {
	WaypointManager::get()->setCurrentWaypointInfo(label);
}

void WaypointerCore::set_waypoint_path(std::string path, std::string waypointInfo) {
	WaypointManager::get()->setCurrentWaypointPathByLabel(path, waypointInfo);
}

void WaypointerCore::execute_waypoint_walk_to(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	int radius = waypointInfoPtr->get_additional_info_as_int("radius");
	WAYPOINTER_CHECK_REARCHED(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY, radius);

	bool force_move = waypointInfoPtr->get_additional_info_as_bool("forcehotkeysmove");
	int delay = waypointInfoPtr->get_additional_info_as_int("delay");
	int min_points = waypointInfoPtr->get_additional_info_as_int("requireddelaypoints");
	int delaymode = waypointInfoPtr->get_additional_info_as_int("delaymode");
	
	set_go_to(waypointInfoPtr->get_destination_coord(), radius, map_front_t,
		[&](){
		if (!force_move)
			return false;
		switch (delaymode){
		case 0:{//"Fixed after creatures points"
			if (get_volatile_string_value("force_delay_static") == "yes"){
				return true;
			}
			else if (HunterCore::get()->get_creature_points().first >= min_points){
				set_volatile_value("force_delay_static", "yes");
				return true;
			}
			else
				return false;
		}
			break;
		case 1:{//"Not after creatures points"
			if (get_volatile_string_value("force_delay_static") == "no"){
				return false;
			}
			else if (HunterCore::get()->get_creature_points().first >= min_points){
				set_volatile_value("force_delay_static", "no");
				return false;
			}
			else
				return true;
		}
			break;
		case 2:{//"Dinamic based creatures points."
			return HunterCore::get()->get_creature_points().first >= min_points;
		}
			break;
		}
		//MessageBox(0, "Error invalid option in waypointer options please report bugs in the forum!", "Error!", MB_OK);
		return false;
	}
	, delay);


	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_message(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::says(waypointInfoPtr->get_additionalInfo("messageText"));

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_message_npc(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string text = waypointInfoPtr->get_additionalInfo("messageText");
	std::string npc_name = waypointInfoPtr->get_additionalInfo("npcName");

	Actions::says_to_npc(text, npc_name);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));

	set_label(labelId);
}

void WaypointerCore::execute_waypoint_lua_script(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	execute_lua(waypointInfoPtr);
	if (waypointInfoPtr == WaypointManager::get()->getCurrentWaypointInfo()){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	}
}

void WaypointerCore::execute_waypoint_use(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go,1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::use_item_on_coordinate(waypointInfoPtr->get_destination_coord());

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_hole_or_step(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();

	is_rearched(position_to_go, 0);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	
	Actions::hole_or_step(position_to_go, waypointInfoPtr->get_side_to_go());

	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_exani_hur_up(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 0);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate char_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (position_to_go.z != char_coord.z)
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	side_nom_axis_t::side_nom_axis_t type_to_go;
	switch (waypointInfoPtr->get_side_to_go()){
	case side_t::side_t::side_north:
		type_to_go = side_nom_axis_t::side_north;
		break;
	case side_t::side_t::side_east:
		type_to_go = side_nom_axis_t::side_east;
		break;
	case side_t::side_t::side_south:
		type_to_go = side_nom_axis_t::side_south;
		break;
	case side_t::side_t::side_west:
		type_to_go = side_nom_axis_t::side_west;
		break;
	default:
		break;
	}

	if (Actions::turn(type_to_go))
		Actions::use_exani_hur(exani_hur_t::exani_hur_up, position_to_go, type_to_go, waypointInfoPtr->get_additional_info_as_bool("forcemove"));

	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_exani_hur_down(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate char_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (position_to_go.z != char_coord.z)
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	side_nom_axis_t::side_nom_axis_t type_to_go;
	switch (waypointInfoPtr->get_side_to_go()){
	case side_t::side_t::side_north:
		type_to_go = side_nom_axis_t::side_north;
		break;
	case side_t::side_t::side_east:
		type_to_go = side_nom_axis_t::side_east;
		break;
	case side_t::side_t::side_south:
		type_to_go = side_nom_axis_t::side_south;
		break;
	case side_t::side_t::side_west:
		type_to_go = side_nom_axis_t::side_west;
		break;
	default:
		break;
	}

	if (Actions::turn(type_to_go))
		Actions::use_exani_hur(exani_hur_t::exani_hur_down, position_to_go, type_to_go, waypointInfoPtr->get_additional_info_as_bool("forcemove"));
	
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_check_necesary_deposit_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 1);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string depotId = waypointInfoPtr->get_additionalInfo("depotId");
	auto depotInfo = DepoterManager::get()->checkNecessaryDepot(depotId);
	if (depotInfo) {
		std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));
		set_label(labelId);
		return;
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_check_necesary_deposit_any_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 1);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	auto depotInfo = DepoterManager::get()->checkNecessaryDepot();
	if (depotInfo) {
		std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));
		set_label(labelId);
		return;
	}
	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_check_necesary_repot_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 1);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	std::string repotId = string_util::lower(waypointInfoPtr->get_additionalInfo("repotId"));

	auto repotInfo = RepoterCore::is_necessary_repot(repotId);
	if (repotInfo) {
		std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));
		set_label(labelId);
		return;
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_check_necesary_repot_any_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 1);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	auto repotInfo = RepoterCore::is_necessary_all_repot();
	if (repotInfo) {
		std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));
		set_label(labelId);
		return;
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_deposit_items_by_depot_id(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	
	Actions::reopen_containers();

	std::string depotId = waypointInfoPtr->get_additionalInfo("depotId");
	action_retval_t retval_open_depot = DepoterCore::open_depot_chest();
	if (retval_open_depot == action_retval_fail){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}
	else if (retval_open_depot == action_retval_trying){
		TibiaProcess::get_default()->wait_ping_delay(3.0);
		return;
	}

	if (depotId.length() <= 0)
		return;


	DepoterCore::request_deposit(depotId);

	Actions::reopen_containers();
	
	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_repot_from_depot_by_id(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	
	std::string depotId = waypointInfoPtr->get_additionalInfo("repotId");
	if (!DepoterCore::open_depot_chest()){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}

	AdvancedRepoterManager::get()->repot_from_depot(depotId);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_deposit_items_all_depot(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 3);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::reopen_containers();

	action_retval_t retval_open_depot;// = DepoterCore::open_depot_chest();

	int total_try_count = 0;
	while (total_try_count <= Actions::Max_Count) {
		retval_open_depot = DepoterCore::open_depot_chest();

		if (retval_open_depot == action_retval_t::action_retval_success)
			break;
		
		total_try_count++;
	}

	if (total_try_count >= Actions::Max_Count){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}

	DepoterCore::request_deposit_all_items();

	Actions::reopen_containers(true);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_withdraw_all_repots_if_fail_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr)  {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::reopen_containers();
	bool withdrawMoney = RepoterCore::withdraw_repot();
	if (!withdrawMoney) {
		std::string labelId = waypointInfoPtr->get_additionalInfo("goToWaypointInfo");
		std::shared_ptr<WaypointPath> currentPath = WaypointManager::get()->getCurrentWaypointPath();
		if (!currentPath || labelId == ""){
			WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
			return;
		}

		auto waypointByLabel = currentPath->getWaypointInfoByLabel(labelId);
		if (!waypointByLabel){
			WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
			return;
		}

		set_label(labelId);
		return;
	}

	WaypointManager::get()->advanceCurrentWaypointInfo();
	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_repot_here_by_id(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	std::string repotId = string_util::lower(waypointInfoPtr->get_additionalInfo("repotId"));
	
	RepoterCore::buy_item_repot_id(repotId);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_rope(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::use_rope_on_coordinate(position_to_go);

	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_shovel(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();

	int startitem_id = waypointInfoPtr->get_additional_info_as_int("startitem_id");
	int enditem_id = waypointInfoPtr->get_additional_info_as_int("enditem_id");

	Coordinate coord_drop_item = position_to_go + get_offset_to_current_side(waypointInfoPtr->get_side_to_go());

	int total_try_count = 0;
	action_retval_t sucess;
	while (total_try_count < Actions::Max_Count) {
		sucess = Actions::open_hole_with_shovel(coord_drop_item, startitem_id, enditem_id, [&]()->bool{
			if (!NeutralManager::get()->get_bot_state() || !NeutralManager::get()->get_waypointer_state())
				return true; 
			return false; });
		if (sucess == action_retval_t::action_retval_success)
			break;
		total_try_count++;
	}
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_pick(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 3);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();

	int startitem_id = waypointInfoPtr->get_additional_info_as_int("startitem_id");
	int enditem_id = waypointInfoPtr->get_additional_info_as_int("enditem_id");

	Coordinate coord_use_pick = position_to_go + get_offset_to_current_side(waypointInfoPtr->get_side_to_go());

	int total_try_count = 0;
	action_retval_t sucess;
	while (total_try_count < Actions::Max_Count) {
		sucess = Actions::open_hole_with_pick(coord_use_pick, startitem_id, enditem_id, [&]()->bool{
			if (!NeutralManager::get()->get_bot_state() || !NeutralManager::get()->get_waypointer_state())
				return true;
			return false; });

		if (sucess == action_retval_t::action_retval_success)
			break;

		total_try_count++;
	}

	/*if (!sucess)
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);*/

	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_machete(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::open_thing_with_machet(position_to_go);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_scythe(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::open_hole_with_scythe(position_to_go);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_tool_id(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	int tool_id = 0;
	try{
		tool_id = std::stoi(waypointInfoPtr->get_additionalInfo("toolId"));
	}
	catch (...){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}


	Actions::open_hole_with_tool_id(position_to_go, tool_id);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_use_item(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::reopen_containers();

	int item_idInfo = waypointInfoPtr->get_additional_info_as_int("itemId");

	Actions::use_crosshair_item_on_coordinate(position_to_go, item_idInfo);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_remove_item(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::clean_coordinate(position_to_go, 1);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_wait_milliseconds(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	std::string sleepTimeInfo = waypointInfoPtr->get_additionalInfo("sleepTime");
	int sleepTime = 0;
	try{
		sleepTime = std::stoi(sleepTimeInfo);
	}
	catch (...){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}
	Sleep(sleepTime);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_up_ladder(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::up_ladder(position_to_go);

	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_use_item_id_to_down_or_up(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	int item_id;
	try{
		item_id = boost::lexical_cast<int32_t>(waypointInfoPtr->get_additionalInfo("item_id"));
	}
	catch (...){
		item_id = 0;
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}

	int current_item_id = item_id > 0 ? item_id : 0;
	Actions::use_item_id_to_down_or_up(position_to_go, current_item_id);

	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_reopen_containers(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	Actions::reopen_containers();

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_close_containers(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	ContainerManager::get()->close_all_containers();

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_loggout_this_location(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (current_coordinate == position_to_go) 
		Actions::loggout();

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_close_tibia_this_location(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (current_coordinate == position_to_go)
		TibiaProcess::get_default()->kill();

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_if_way_block_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	Coordinate new_position = Pathfinder::get()->get_nearest_coordinate_from_another(position_to_go, map_back_mini_t, true, true, 1);

	do {
		set_go_to(new_position);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();

	bool walkeable = gMapMinimapPtr->get_walkability_map(position_to_go, map_front_t, self_coordinate);
	if (!walkeable) {
		std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));
		set_label(labelId);
		return;
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_use_lever(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	int startitem_id = waypointInfoPtr->get_additional_info_as_int("startitem_id");
	int enditem_id = waypointInfoPtr->get_additional_info_as_int("enditem_id");

	Coordinate offset = Coordinate(0, 0, 0);

	switch (waypointInfoPtr->get_side_to_go()){
	case side_t::side_t::side_center:
		offset = Coordinate(0, 0, 0);
		break;
	case side_t::side_t::side_east:
		offset = Coordinate(1, 0, 0);
		break;
	case side_t::side_t::side_north:
		offset = Coordinate(0, -1, 0);
		break;
	case side_t::side_t::side_south:
		offset = Coordinate(0, 1, 0);
		break;
	case side_t::side_t::side_west:
		offset = Coordinate(-1, 0, 0);
		break;	}

	Actions::use_lever(position_to_go, offset, startitem_id, enditem_id);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_rearch_creature(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 3);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string creatureName = waypointInfoPtr->get_additionalInfo("creatureName");

	if (creatureName == ""){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}
	
	int considerNear = waypointInfoPtr->get_additional_info_as_int("considerNear");
	
	Actions::goto_nearest_from_creature(creatureName);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_teleport_else_go_to_label(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	do{
		set_go_to(position_to_go);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	//WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	if (!Actions::join_in_telepot(position_to_go + get_offset_to_current_side(waypointInfoPtr->get_side_to_go()))) {
		std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));
		set_label(labelId);
		return;
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_walk_to_random(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	WAYPOINTER_CHECK_REARCHED(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY, 1);

	set_go_to(waypointInfoPtr->get_destination_coord(), 0);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_check_cap_less_than_goto_label(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	do{
		set_go_to(waypointInfoPtr->get_destination_coord(), 0);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	int self_cap = TibiaProcess::get_default()->character_info->cap();
	int cap = 0;
	try{
		cap = std::stoi(waypointInfoPtr->get_additionalInfo("cap"));
	}
	catch (...){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}

	if (self_cap < cap) {
		std::string labelId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));
		set_label(labelId);
		return;
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_destroy_field(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Actions::destory_field(position_to_go);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_open_door(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do{
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	int item_idInfo = waypointInfoPtr->get_additional_info_as_int("itemId");

	Coordinate temp = position_to_go + get_offset_to_current_side(waypointInfoPtr->get_side_to_go());
	
	Actions::step_side(waypointInfoPtr->get_side_to_go(), 2000);

	TibiaProcess::get_default()->wait_ping_delay(2.5);

	int total_try_count = 0;
	action_retval_t sucess;
	while (total_try_count < Actions::Max_Count) {
		sucess = Actions::open_door(temp, item_idInfo);;

		if (sucess == action_retval_t::action_retval_success)
			break;

		TibiaProcess::get_default()->wait_ping_delay(2.0);

		total_try_count++;
	}
	
	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_go_to_waypoint_path(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string pathId = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointPath"));
	std::string waypointInfo = string_util::lower(waypointInfoPtr->get_additionalInfo("goToWaypointInfo"));

	set_waypoint_path(pathId, waypointInfo);
}

void WaypointerCore::execute_waypoint_lure_here_distance(std::shared_ptr<WaypointInfo> waypointInfoPtr) {
	/*WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	*/
	//WaypointManager::get()->set_require_lure(true);

	std::string stayafterlure = string_util::lower(waypointInfoPtr->get_additionalInfo("stayafterlure"));
	bool stayafterlureBool = false;

	std::string advanceiflurereached = string_util::lower(waypointInfoPtr->get_additionalInfo("advanceiflurereached"));
	bool advanceiflurereachedBool = false;

	std::string recursivenextlure = string_util::lower(waypointInfoPtr->get_additionalInfo("recursivenextlure"));
	bool recursivenextlureBool = false;

	std::string increasecreaturesdistancesqm = string_util::lower(waypointInfoPtr->get_additionalInfo("increasecreaturesdistancesqm"));

	if (advanceiflurereached == "true")
		advanceiflurereachedBool = true;

	if (recursivenextlure == "true")
		recursivenextlureBool = true;

	if (stayafterlure == "true")
		stayafterlureBool = true;

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_lure_here_knight_mode(std::shared_ptr<WaypointInfo> waypointInfoPtr) {

	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
	set_core_priority(core_priority_highest);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();
	TibiaProcess::get_default()->character_info->wait_stop_waking();
	set_go_to(waypointInfoPtr->get_destination_coord());
	set_priority_normal();
	//set_core_priority(core_priority_highest);
	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string depotId = string_util::lower(waypointInfoPtr->get_additionalInfo("repotId"));

	//WaypointManager::get()->set_require_lure(true);
	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

#define execute_type(type)\
	execute_##type(waypointInfoPtr);


void WaypointerCore::execute_waypoint_go_to_next_path(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 5);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	WaypointManager::get()->advanceCurrentWaypointPath();
}
void WaypointerCore::execute_waypoint_drop_item(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	uint32_t item_id = waypointInfoPtr->get_additional_info_as_int("item_id");
	uint32_t item_count = waypointInfoPtr->get_additional_info_as_int("item_count");

	Coordinate coord_drop_item = position_to_go + get_offset_to_current_side(waypointInfoPtr->get_side_to_go());
	uint32_t count_move = 0;
	uint32_t count_to_move = 0;
	TimeChronometer time;
	while (count_move < item_count && time.elapsed_milliseconds() < 2000){
		Sleep(10);

		ContainerPosition itemContainerPosition = ContainerManager::get()->find_item(item_id);
		ItemContainerPtr container = ContainerManager::get()->get_container_by_index(itemContainerPosition.container_index);

		if (!container || itemContainerPosition.is_null())
			continue;

		count_to_move = item_count - count_move;
		if (count_to_move <= 0)
			break;

		uint32_t count_in_stack = container->slots[itemContainerPosition.slot_index].stack_count;
		if (count_in_stack < count_to_move)
			count_to_move = count_in_stack;
		
		if (Actions::dropitem(item_id, coord_drop_item, count_to_move, false) == action_retval_success)
			count_move =+ count_to_move;
	}
	
	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}
void WaypointerCore::execute_waypoint_sell_items_by_sell_id(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string sellId = string_util::lower(waypointInfoPtr->get_additionalInfo("SellId"));
	auto sellItems = SellManager::get()->getSellItems(sellId);

	if (sellItems)
		SellCore::sell_items(sellId);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}
void WaypointerCore::execute_waypoint_sell_all_items(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 2);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	auto sellItemsList = SellManager::get()->get_mapSellItems();
	for (auto depotContainer : sellItemsList){
		Sleep(1);
		SellCore::sell_items(depotContainer.first);
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_auto_haste(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);


	std::string spellName = string_util::lower(waypointInfoPtr->get_additionalInfo("spellName"));
	std::string spellMana = string_util::lower(waypointInfoPtr->get_additionalInfo("spellMana"));
	std::string checkSpell = string_util::lower(waypointInfoPtr->get_additionalInfo("checkSpell"));
	int SpellManaInt = 0;
	bool checkSpellBool = false;

	if (checkSpell == "true")
		checkSpellBool = true;

	try{
		SpellManaInt = std::stoi(spellMana);
	}
	catch (...){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}

	if (TibiaProcess::get_default()->character_info->mp() > (uint32_t)SpellManaInt){
		if (checkSpellBool){
			if (!TibiaProcess::get_default()->character_info->status_haste())
				Actions::says(spellName);
		}
		else
			Actions::says(spellName);
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}
void WaypointerCore::execute_waypoint_auto_utura(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string spellName = string_util::lower(waypointInfoPtr->get_additionalInfo("spellName"));
	std::string spellMana = string_util::lower(waypointInfoPtr->get_additionalInfo("spellMana"));
	std::string checkSpell = string_util::lower(waypointInfoPtr->get_additionalInfo("checkSpell"));
	int SpellManaInt = 0;
	bool checkSpellBool = false;

	if (checkSpell == "true")
		checkSpellBool = true;

	try{
		SpellManaInt = std::stoi(spellMana);
	}
	catch (...){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}

	if (TibiaProcess::get_default()->character_info->mppc() > (uint32_t)SpellManaInt){
		if (checkSpellBool){
			if (!TibiaProcess::get_default()->character_info->status_streng_up())
				Actions::says(spellName);
		}
		else
			Actions::says(spellName);
	}

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}
void WaypointerCore::execute_waypoint_cast_spell(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 1);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string spellName = string_util::lower(waypointInfoPtr->get_additionalInfo("spellName"));
	std::string spellMana = string_util::lower(waypointInfoPtr->get_additionalInfo("spellMana"));
	int SpellManaInt = 0;

	try{
		SpellManaInt = std::stoi(spellMana);
	}
	catch (...){
		WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
		return;
	}

	if (TibiaProcess::get_default()->character_info->mppc() > (uint32_t)SpellManaInt)
		Actions::says(spellName);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_pulse_target(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_LOCATION(10)
		WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	HunterCore::get()->pulse();

	bool wait_kill_all = waypointInfoPtr->get_additional_info_as_bool("wait_kill_all");

	if (wait_kill_all)
		HunterCore::get()->wait_pulse_out(0xffffffff);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}
void WaypointerCore::execute_waypoint_pulse_looter(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	WAYPOINTER_CHECK_LOCATION(10)
		WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	LooterCore::get()->pulse();

	bool loot_remove_timeout = waypointInfoPtr->get_additional_info_as_bool("loot_remove_timeout");
	int loot_timeout = waypointInfoPtr->get_additional_info_as_int("loot_timeout");
	if (loot_remove_timeout)
		LooterCore::get()->remove_timeout(loot_timeout);

	bool loot_remove_max_distance = waypointInfoPtr->get_additional_info_as_bool("loot_remove_max_distance");
	int distance_to_remove = waypointInfoPtr->get_additional_info_as_int("distance_to_remove");
	if (loot_remove_max_distance)
		LooterCore::get()->remove_max_distance(distance_to_remove);

	bool wait_loot_all = waypointInfoPtr->get_additional_info_as_bool("wait_loot_all");
	if (wait_loot_all)
		LooterCore::get()->wait_pulse_out(0xffffffff);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::execute_waypoint_withdraw_value(std::shared_ptr<WaypointInfo> waypointInfoPtr)  {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 2);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	int value = waypointInfoPtr->get_additional_info_as_int("value");

	Actions::reopen_containers();
	Actions::withdraw_value(value);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}
void WaypointerCore::execute_waypoint_travel(std::shared_ptr<WaypointInfo> waypointInfoPtr)  {
	WAYPOINTER_CHECK_Z(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	Coordinate position_to_go = waypointInfoPtr->get_destination_coord();

	do {
		set_go_to(position_to_go, 0);
	} while (last_callback_retval == pathfinder_walk_going);

	TibiaProcess::get_default()->character_info->wait_stop_waking();
	is_rearched(position_to_go, 2);

	WAYPOINTER_CHECK_OVERRIDEN_CANCELED();
	WAYPOINTER_CHECK_UNREARCHABLE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);

	std::string npcName = waypointInfoPtr->get_additionalInfo("npcname");
	std::string passage = waypointInfoPtr->get_additionalInfo("passage");

	Actions::reopen_containers();
	Actions::travel(passage, npcName);

	WAYPOINTER_ADVANCE_NEXT(WAYPOINTER_DEFAULT_ADVANCE_NEXT_DELAY);
}

void WaypointerCore::process_waypoint_t(std::shared_ptr<WaypointInfo> waypointInfoPtr){
	switch (waypointInfoPtr->get_action()){
	case waypoint_walk_to:execute_type(waypoint_walk_to) break;
	case waypoint_message: execute_type(waypoint_message) break;
	case waypoint_message_npc:execute_type(waypoint_message_npc)  break;
	case waypoint_goto_label:execute_type(waypoint_goto_label)  break;
	case waypoint_lua_script: execute_type(waypoint_lua_script) break;
	case waypoint_use:execute_type(waypoint_use)  break;
	case waypoint_hole_or_step:execute_type(waypoint_hole_or_step)  break;
	case waypoint_exani_hur_up:execute_type(waypoint_exani_hur_up)  break;
	case waypoint_exani_hur_down:execute_type(waypoint_exani_hur_down)  break;
	case waypoint_check_necesary_deposit_go_to_label:execute_type(waypoint_check_necesary_deposit_go_to_label)  break;
	case waypoint_check_necesary_deposit_any_goto_label:execute_type(waypoint_check_necesary_deposit_any_goto_label)  break;
	case waypoint_check_necesary_repot_go_to_label:execute_type(waypoint_check_necesary_repot_go_to_label)  break;
	case waypoint_check_necesary_repot_any_go_to_label:execute_type(waypoint_check_necesary_repot_any_go_to_label)  break;
	case waypoint_deposit_items_by_depot_id:execute_type(waypoint_deposit_items_by_depot_id) break;
	case waypoint_deposit_items_all_depot:execute_type(waypoint_deposit_items_all_depot) break;
	case waypoint_withdraw_all_repots_if_fail_goto_label:execute_type(waypoint_withdraw_all_repots_if_fail_goto_label) break;
	case waypoint_repot_here_by_id:execute_type(waypoint_repot_here_by_id) break;
	case waypoint_rope:execute_type(waypoint_rope) break;
	case waypoint_shovel:execute_type(waypoint_shovel) break;
	case waypoint_pick:execute_type(waypoint_pick) break;
	case waypoint_machete:execute_type(waypoint_machete)  break;
	case waypoint_scythe:execute_type(waypoint_scythe) break;
	case waypoint_tool_id:execute_type(waypoint_tool_id) break;
	case waypoint_use_item:execute_type(waypoint_use_item)  break;
	case waypoint_remove_item:execute_type(waypoint_remove_item)  break;
	case waypoint_wait_milliseconds:execute_type(waypoint_wait_milliseconds) break;
	case waypoint_up_ladder:execute_type(waypoint_up_ladder) break;
	case waypoint_use_item_id_to_down_or_up:execute_type(waypoint_use_item_id_to_down_or_up) break;
	case waypoint_reopen_containers:execute_type(waypoint_reopen_containers)  break;
	case waypoint_close_containers:execute_type(waypoint_close_containers)  break;
	case waypoint_loggout_this_location:execute_type(waypoint_loggout_this_location)  break;
	case waypoint_close_tibia_this_location:execute_type(waypoint_close_tibia_this_location)  break;
	case waypoint_if_way_block_goto_label:execute_type(waypoint_if_way_block_goto_label)  break;
	case waypoint_use_lever:execute_type(waypoint_use_lever)  break;
	case waypoint_rearch_creature:execute_type(waypoint_rearch_creature)  break;
	case waypoint_teleport_else_go_to_label:execute_type(waypoint_teleport_else_go_to_label)  break;
	case waypoint_walk_to_random:execute_type(waypoint_walk_to_random)  break;
	case waypoint_check_cap_less_than_goto_label:execute_type(waypoint_check_cap_less_than_goto_label) break;
	case waypoint_destroy_field:execute_type(waypoint_destroy_field)  break;
	case waypoint_open_door:execute_type(waypoint_open_door)  break;
	case waypoint_repot_from_depot_by_id:execute_type(waypoint_repot_from_depot_by_id)  break;
	case waypoint_go_to_waypoint_path:execute_type(waypoint_go_to_waypoint_path)  break;
	case waypoint_lure_here_distance:execute_type(waypoint_lure_here_distance)  break;
	case waypoint_lure_here_knight_mode:execute_type(waypoint_lure_here_knight_mode)  break;
	case waypoint_sell_items_by_sell_id:execute_type(waypoint_sell_items_by_sell_id)  break;
	case waypoint_sell_all_items:execute_type(waypoint_sell_all_items)  break;
	case waypoint_auto_haste:execute_type(waypoint_auto_haste)  break;
	case waypoint_auto_utura:execute_type(waypoint_auto_utura)  break;
	case waypoint_cast_spell:execute_type(waypoint_cast_spell)  break;
	case waypoint_travel:execute_type(waypoint_travel)  break;
	case waypoint_pulse_target:execute_type(waypoint_pulse_target)  break;
	case waypoint_pulse_looter:execute_type(waypoint_pulse_looter) break;
	case waypoint_drop_item:execute_type(waypoint_drop_item) break;
	case waypoint_go_to_next_path:execute_type(waypoint_go_to_next_path) break;
	case waypoint_withdraw_value:execute_type(waypoint_withdraw_value) break;		
	default: break;
	}
}

WaypointerCore::WaypointerCore(){
	/*set_state(false);
	work_delay = 10;*/
}

bool WaypointerCore::check_update_state(){
	//lock_guard _lock(mtx_access);
	return true;
}

void WaypointerCore::start(){
	TimeChronometer timer_change;
	TimeChronometer timer;
	while (true){
		Sleep(1);

		if (!can_execute())
			continue;

		current_waypoint_info_ptr = WaypointManager::get()->getCurrentWaypointInfo();
		timer_change.reset();
		check_waypoint_changed();

		if (!current_waypoint_info_ptr)
			continue;

		timer.reset();
		process_waypoint_t(current_waypoint_info_ptr);
	}
}

void WaypointerCore::invoke(){	
}

void WaypointerCore::run(){
	std::thread([&](){ 
		MONITOR_THREAD("WaypointerCore::run")
			start();
	}).detach();
}

bool WaypointerCore::is_location(uint32_t sqm_dist){
	auto waypointInfoPtr = WaypointManager::get()->getCurrentWaypointInfo();
	if (!waypointInfoPtr)
		return false;
		
	Coordinate coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (abs(coord.x - waypointInfoPtr->get_position_x()) <= static_cast<int>(sqm_dist) && abs(coord.y - waypointInfoPtr->get_position_y()) <= static_cast<int>(sqm_dist)
		&& coord.z == waypointInfoPtr->get_position_z())
		return true;

	return false;
}

void WaypointerCore::init() {
	mWaypointerCore = new WaypointerCore();
	mWaypointerCore->run();
}

WaypointerCore* WaypointerCore::get(){
	if (!mWaypointerCore)
		init();

	return mWaypointerCore;
}

void WaypointerCore::check_broke_trap(){

}

void WaypointerCore::on_waypoint_changed(){
	mtx_access.lock();
	last_waypoint_info_ptr = current_waypoint_info_ptr;
	volatile_waypoint_values.clear();
	mtx_access.unlock();
}

void WaypointerCore::check_waypoint_changed(){
	if (last_waypoint_info_ptr != current_waypoint_info_ptr)
		on_waypoint_changed();
}

void WaypointerCore::set_volatile_value(std::string key, uint32_t value){
	mtx_access.lock();
	volatile_waypoint_values[key] = std::to_string(value);
	mtx_access.unlock();
}

void WaypointerCore::set_volatile_value(std::string key, bool value){
	mtx_access.lock();
	volatile_waypoint_values[key] = value ? "true" : "false";
	mtx_access.unlock();
}

void WaypointerCore::set_volatile_value(std::string key, std::string value){
	mtx_access.lock();
	volatile_waypoint_values[key] = value;
	mtx_access.unlock();
}

int WaypointerCore::get_volatile_int_value(std::string key){
	try{
		mtx_access.lock();
		auto temp = boost::lexical_cast<int>(volatile_waypoint_values[key]);
		mtx_access.unlock();
		return temp;
	}
	catch (...){
		return 0;
	}
}

std::string WaypointerCore::get_volatile_string_value(std::string key){
	mtx_access.lock();
	auto temp = (volatile_waypoint_values[key]);
	mtx_access.unlock();
	return temp;
}

bool WaypointerCore::get_volatile_bool_value(std::string key){
	mtx_access.lock();
	auto temp = string_util::lower(volatile_waypoint_values[key]) == "true";
	mtx_access.unlock();
	return temp;
}
