#pragma once
#include "AdvancedRepoterCore.h"

std::shared_ptr<AdvancedRepoterManager> AdvancedRepoterManager::mAdvancedRepoterManager;
std::shared_ptr<AdvancedRepoterManager> AdvancedRepoterManager::get(){
	if (!mAdvancedRepoterManager)
		reset();
	return mAdvancedRepoterManager;
}

AdvancedRepoterId::AdvancedRepoterId(){
	depotBoxId = ItemsManager::get()->getitem_idFromName("Depot Chest");
}

//AdvancedRepoterManager
bool AdvancedRepoterManager::changeRepotId(std::string newId, std::string oldId){
	if (newId == "" || oldId == "")
		return false;

	auto it = std::find_if(mapRepotId.begin(), mapRepotId.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterId>> info_pair){
		return _stricmp(&oldId[0], &info_pair.first[0]) == 0; });
	if (it == mapRepotId.end())
		return false;

	mapRepotId[newId] = it->second;
	mapRepotId.erase(it);
	return true;
}
std::map<std::string, std::shared_ptr<AdvancedRepoterId>> AdvancedRepoterManager::getMapRepoId(){
	return mapRepotId;
}
void AdvancedRepoterManager::reset(){
	mAdvancedRepoterManager = std::shared_ptr<AdvancedRepoterManager>(new AdvancedRepoterManager);
}
void AdvancedRepoterManager::removeRepotId(std::string id){
	auto it = std::find_if(mapRepotId.begin(), mapRepotId.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterId>> info_pair){
		return _stricmp(&id[0], &info_pair.first[0]) == 0;
	});
	if (it != mapRepotId.end())
		mapRepotId.erase(it);
}
std::shared_ptr<AdvancedRepoterId> AdvancedRepoterManager::getRepotIdRule(std::string id_){
	auto it = std::find_if(mapRepotId.begin(), mapRepotId.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterId>> info_pair){
		return _stricmp(&id_[0], &info_pair.first[0]) == 0;
	});
	if (it == mapRepotId.end())
		return 0;
	return it->second;
}
std::string AdvancedRepoterManager::requestNewRepotId(){
	int id = 0;
	std::string current_id = "Repot" + std::to_string(id);

	auto it = std::find_if(mapRepotId.begin(), mapRepotId.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterId>> info_pair){
		return _stricmp(&current_id[0], &info_pair.first[0]) == 0;
	});

	while (it != mapRepotId.end()){
		id++;
		current_id = "Repot" + std::to_string(id);

		it = std::find_if(mapRepotId.begin(), mapRepotId.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterId>> info_pair){
			return _stricmp(&current_id[0], &info_pair.first[0]) == 0;
		});
	}
	mapRepotId[current_id] = std::shared_ptr<AdvancedRepoterId>(new AdvancedRepoterId);
	return current_id;
}
void AdvancedRepoterManager::clear(){
	mapRepotId.clear();
}
AdvancedRepoterManager::AdvancedRepoterManager(){
	requestNewRepotId();
}
std::string AdvancedRepoterManager::requestNewRepotIdById(std::string current_id){
	auto it = std::find_if(mapRepotId.begin(), mapRepotId.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterId>> info_pair){
		return _stricmp(&current_id[0], &info_pair.first[0]) == 0;
	});
	if (it == mapRepotId.end())
		mapRepotId[current_id] = std::shared_ptr<AdvancedRepoterId>(new AdvancedRepoterId);

	return current_id;
}

Json::Value AdvancedRepoterManager::parse_class_to_json() {
	Json::Value ADVrepoterManager;
	Json::Value ADVrepotIdList;

	for (auto repot : mapRepotId){
		ADVrepotIdList[repot.first] = repot.second->parse_class_to_json();
	}

	ADVrepoterManager["ADVrepotIdList"] = ADVrepotIdList;

	return ADVrepoterManager;
}
void AdvancedRepoterManager::parse_json_to_class(Json::Value jsonObject){

	if (!jsonObject["ADVrepotIdList"].empty() || !jsonObject["ADVrepotIdList"].isNull()){
		Json::Value ADVrepotIdList = jsonObject["ADVrepotIdList"];

		Json::Value::Members members = ADVrepotIdList.getMemberNames();

		for (auto member : members){
			std::shared_ptr<AdvancedRepoterId> ADVrepotId(new AdvancedRepoterId);
			
			ADVrepotId->parse_json_to_class(ADVrepotIdList[member]);

			mapRepotId[member] = ADVrepotId;
		}
	}
}

//AdvancedRepoterId
void AdvancedRepoterId::set_Item(std::string id, std::string name){
	mapRepotItens[id]->set_item(name);
}
void AdvancedRepoterId::set_Min(std::string id, uint32_t min){
	mapRepotItens[id]->set_min(min);
}
void AdvancedRepoterId::set_Max(std::string id, uint32_t max){
	mapRepotItens[id]->set_max(max);
}
std::string AdvancedRepoterId::get_Item(std::string id, std::string name){
	return mapRepotItens[id]->get_item();
}
uint32_t AdvancedRepoterId::get_Min(std::string id, uint32_t min){
	return mapRepotItens[id]->get_min();
}
uint32_t AdvancedRepoterId::get_Max(std::string id, uint32_t max){
	return mapRepotItens[id]->get_max();
}
void AdvancedRepoterId::set_DepotBox(std::string itemName, std::string container_name){
	depotBoxName = itemName;
	depotBoxId = ItemsManager::get()->getitem_idFromName(itemName);
}

std::map<std::string, std::shared_ptr<AdvancedRepoterItens>> AdvancedRepoterId::getMapRepoItens(){
	return mapRepotItens;
}
std::string AdvancedRepoterId::requestNewitem_id(){
	int id = 0;
	std::string current_id = "Item" + std::to_string(id);


	auto it = std::find_if(mapRepotItens.begin(), mapRepotItens.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterItens>> info_pair){
		return _stricmp(&current_id[0], &info_pair.first[0]) == 0;
	});

	while (it != mapRepotItens.end()){
		id++;
		current_id = "Item" + std::to_string(id);

		it = std::find_if(mapRepotItens.begin(), mapRepotItens.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterItens>> info_pair){
			return _stricmp(&current_id[0], &info_pair.first[0]) == 0;
		});
	}
	mapRepotItens[current_id] = std::shared_ptr<AdvancedRepoterItens>(new AdvancedRepoterItens);
	return current_id;
}
void AdvancedRepoterId::removeRepotItem(std::string id){
	auto it = std::find_if(mapRepotItens.begin(), mapRepotItens.end(), [&](std::pair<std::string, std::shared_ptr<AdvancedRepoterItens>> info_pair){
		return _stricmp(&id[0], &info_pair.first[0]) == 0;
	});
	if (it != mapRepotItens.end())
		mapRepotItens.erase(id);
}
std::string AdvancedRepoterId::addRepotItem(std::string item, int min, int max){
	std::string id_ = requestNewitem_id();
	mapRepotItens[id_]->item = item;
	mapRepotItens[id_]->min = min;
	mapRepotItens[id_]->max = max;
	return id_;
}

Json::Value AdvancedRepoterId::parse_class_to_json() {
	Json::Value ADVrepotClasse;
	Json::Value ADVrepotItemList;

	for (auto it : mapRepotItens){
		Json::Value item;

		item["itemName"] = it.second->get_item();
		item["min"] = it.second->get_min();
		item["max"] = it.second->get_max();
		ADVrepotItemList[it.first] = item;
	}

	ADVrepotClasse["ADVrepotItemList"] = ADVrepotItemList;
	ADVrepotClasse["depotBoxId"] = this->depotBoxId;
	ADVrepotClasse["depotBoxName"] = this->depotBoxName;

	ADVrepotClasse["characterBackpack"] = this->characterBackpack;
	ADVrepotClasse["characterBackpackId"] = this->characterBackpackId;

	return ADVrepotClasse;
}
void AdvancedRepoterId::parse_json_to_class(Json::Value jsonObject) {
	if (!jsonObject["depotBoxId"].empty() || !jsonObject["depotBoxId"].isNull()){
		try{
			this->depotBoxId = jsonObject["depotBoxId"].asInt();
		}
		catch (...){
			this->depotBoxId = 0;
		}
	}
		

	if (!jsonObject["depotBoxName"].empty() || !jsonObject["depotBoxName"].isNull())
		this->depotBoxName = jsonObject["depotBoxName"].asString();

	if (!jsonObject["characterBackpackId"].empty() || !jsonObject["characterBackpackId"].isNull())
		this->characterBackpackId = jsonObject["characterBackpackId"].asInt();
	
	if (!jsonObject["characterBackpack"].empty() || !jsonObject["characterBackpack"].isNull())
		this->characterBackpack = jsonObject["characterBackpack"].asString();

	if (!jsonObject["ADVrepotItemList"].empty() || !jsonObject["ADVrepotItemList"].isNull()){
		Json::Value ADVrepotItemList = jsonObject["ADVrepotItemList"];
		Json::Value::Members members = ADVrepotItemList.getMemberNames();
		
		for (auto member : members){
			Json::Value item = ADVrepotItemList[member];
			std::shared_ptr<AdvancedRepoterItens> newItem(new AdvancedRepoterItens);
			
			newItem->set_item(item["itemName"].asString());
			newItem->set_min(item["min"].asInt());
			newItem->set_max(item["max"].asInt());

			mapRepotItens[member] = newItem;
		}
	}
}

void AdvancedRepoterManager::request_all_repot_from_depot(){
	for (auto depoter : mapRepotId)
		repot_from_depot(depoter.first);
}

bool AdvancedRepoterManager::repot_from_depot(std::string DepotContainerId){
	std::shared_ptr<AdvancedRepoterId> RepotId = getRepotIdRule(DepotContainerId);

	if (!RepotId)
		return false;

	ItemContainerPtr DepotBox;
	if (RepotId->depotBoxId == UINT32_MAX || RepotId->depotBoxId ==
		ItemsManager::get()->getitem_idFromName("Depot Chest")){
		DepotBox = ContainerManager::get()->get_container_by_caption("Depot Chest");
		if (!DepotBox)
			return false;		
	}
	else{
		DepotBox = ContainerManager::get()->get_depot_box_container(RepotId->depotBoxId);
		if (!DepotBox)
			DepoterCore::open_depot_box(RepotId->depotBoxId);

		ContainerManager::get()->wait_container_id_open(RepotId->depotBoxId);

		DepotBox = ContainerManager::get()->get_depot_box_container(RepotId->depotBoxId);
	}

	if (!DepotBox)
		return false;

	if (move_items(DepotBox, RepotId))
		return true;

	return false;
}

bool AdvancedRepoterManager::move_items(ItemContainerPtr depotBoxContainer, std::shared_ptr<AdvancedRepoterId> RepotId){
	uint32_t ItemsDeposited = 0;

	auto myMap = RepotId->getMapRepoItens();
	for (auto item : myMap) {
		int destinyBackpackId = ItemsManager::get()->getitem_idFromName(RepotId->characterBackpack);

		if (destinyBackpackId == 0 || destinyBackpackId == UINT32_MAX)
			return false;

		ItemContainerPtr destinyBackpackContainer = ContainerManager::get()->get_container_by_id(destinyBackpackId);
		
		if (!destinyBackpackContainer){
			if (Actions::get()->open_container_id(destinyBackpackId,
				true, TibiaProcess::get_default()->get_action_delay_promedy_time(2)) != action_retval_success){
				return false;
			}
		}

		destinyBackpackContainer = ContainerManager::get()->get_container_by_id(destinyBackpackId);
		if (!destinyBackpackContainer)
			return false;

		int item_id = ItemsManager::get()->getitem_idFromName(item.second->get_item());
		if (item_id == 0 || item_id == UINT32_MAX)
			continue;

		uint32_t itemCount = destinyBackpackContainer->get_item_count_by_id(item_id);
		if (itemCount >= item.second->get_max()){
			ItemsDeposited++;
			continue;
		}

		ContainerPosition itemContainer = ContainerManager::get()->find_item(item_id);

		if (itemContainer.is_null())
			continue;

		if (try_to_move_item(depotBoxContainer, destinyBackpackId, item.second)){
			TibiaProcess::get_default()->wait_ping_delay(1);
			ItemsDeposited++;
			continue;
		}
	}

	if (ItemsDeposited >= myMap.size())
		return true;

	return false;
}

bool AdvancedRepoterManager::try_to_move_item(ItemContainerPtr depotBoxContainer, int destinyBackpackId, std::shared_ptr<AdvancedRepoterItens> item) {
	uint32_t item_id = ItemsManager::get()->getitem_idFromName(item->get_item());
	uint32_t itemCount = 0;

	int loopTryingCount = 0;
	while (true) {
		Sleep(50);

		if (!depotBoxContainer)
			return false;

		ItemContainerPtr destinyBackpackContainer = ContainerManager::get()->get_container_by_id(destinyBackpackId);
		if (!destinyBackpackContainer)
			return false;

		depotBoxContainer = ContainerManager::get()->get_container_by_id(depotBoxContainer->get_id());
		if (!depotBoxContainer)
			return false;
		
		uint32_t currentItemCount = destinyBackpackContainer->get_item_count_by_id(item_id);
		uint32_t currentItemCountInDp = depotBoxContainer->get_item_count_by_id(item_id);
		
		if (currentItemCount >= item->get_max() || currentItemCountInDp <= 0)
			return true;

		if (currentItemCount == itemCount)
			loopTryingCount++;

		if (loopTryingCount == 3)
			loopTryingCount = 0;		

		ContainerPosition destinationBackpackPosition = ContainerPosition(destinyBackpackContainer->get_container_index(), destinyBackpackContainer->get_max_slots() - 1);
		
		if (destinyBackpackContainer->is_full())
			destinyBackpackContainer->open_next();

		ContainerPosition itemContainer = ContainerPosition(depotBoxContainer->get_container_index(), depotBoxContainer->find_item_slot(item_id));//ContainerManager::get()->find_item(item_id, depotBoxContainer->get_id());
		if (itemContainer.is_null())
			return false;

		uint32_t itemCountMove = item->get_max() - currentItemCount;

		if (itemCountMove <= 100)
			Actions::move_item_container_to_container(itemContainer, destinationBackpackPosition, itemCountMove, item_id);
		else{
			while (true){
				Sleep(50);

				if (destinyBackpackContainer->is_full())
					destinyBackpackContainer->open_next();

				itemCountMove = item->get_max() - destinyBackpackContainer->get_item_count_by_id(item_id);
				if (itemCountMove <= 0)
					break;

				ItemContainerPtr itemSoucerContainer = ContainerManager::get()->get_container_by_id(depotBoxContainer->get_id());
				if (!itemSoucerContainer)
					break;

				itemContainer = ContainerPosition(itemSoucerContainer->get_container_index(), depotBoxContainer->find_item_slot(item_id));
				if (itemContainer.is_null())
					break;

				Actions::move_item_container_to_container(itemContainer, destinationBackpackPosition, itemCountMove, item_id);
				
				TibiaProcess::get_default()->wait_ping_delay(2.5);

				destinyBackpackContainer = ContainerManager::get()->get_container_by_id(destinyBackpackId);
				if (!destinyBackpackContainer)
					break;

				depotBoxContainer = ContainerManager::get()->get_container_by_id(depotBoxContainer->get_id());
				if (!depotBoxContainer)
					break;

				currentItemCount = destinyBackpackContainer->get_item_count_by_id(item_id);
				currentItemCountInDp = depotBoxContainer->get_item_count_by_id(item_id);

				if (currentItemCount >= item->get_max() || currentItemCountInDp <= 0)
					break;

			}
		}
		TibiaProcess::get_default()->wait_ping_delay(2.0);
	}
}
