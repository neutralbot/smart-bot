#pragma once
#include "Util.h"
#include "AddressManager.h"
#include "HunterCore.h"


struct EnumData {
	DWORD dwProcessId;
	HWND hWnd;
};

BOOL CALLBACK EnumProc(HWND hWnd, LPARAM lParam);

struct TibiaProcessInfo{
	uint32_t pid;
	std::string player_name;
	uint32_t version;
	bool supported = false;
};

class Process{
	std::map<uint32_t/*pid*/, TibiaProcessInfo> tibia_processes;
	neutral_mutex mtx_access;

	std::vector<uint32_t> version_suport;
public:

	Process::Process(){
		version_suport.push_back(1090);
		version_suport.push_back(1092);
		version_suport.push_back(1093);
		version_suport.push_back(1094);
		version_suport.push_back(1095);
		version_suport.push_back(1096);
	}
	bool load_fundamental = false;
	std::map<uint32_t, TibiaProcessInfo> GetProcessesMap();

	HWND FindWindowFromProcessId(DWORD dwProcessId);

	HWND FindWindowFromProcess(HANDLE hProcess);

	HWND GetProcessIdMainThread(DWORD processId);

	void ShowWindow(HWND window);

	void HideWindow(HWND window);

	int GetForegroundPid();

	void RefresherTibiaClients();

	void RefresherNameChar();

	DWORD GetModuleBase(LPSTR lpModuleName, DWORD dwProcessId);

	static Process* get();

	static DWORD GetParentProcessId();

	static DWORD get_process_id(std::string process_bin);
};

DWORD get_parent_process_id();

std::map<uint32_t, std::string> get_processes_names_by_ntdll();

uint32_t get_process_id_from_name(std::string process_name);