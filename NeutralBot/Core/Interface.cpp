#pragma once
#include "Interface.h"
#include "Input.h"
#include "TibiaProcess.h"
#include "Time.h"
#include <thread>
#include "..\\DevelopmentManager.h"

COORD COORDINATES::COORDINATE_BUTTON_UP_CONTAINER = { 142, 7 };
COORD COORDINATES::COORDINATE_BUTTON_CLOSE_CONTAINER = { 166, 7 };
COORD COORDINATES::COORDINATE_BUTTON_MINIMIZE_CONTAINER = { 153, 7 };

COORD COORDINATES::COORDINATE_BUTTON_FOLLOW = { 15, 35 };
COORD COORDINATES::COORDINATE_BUTTON_NOT_FOLLOW = { 15, 15 };
COORD COORDINATES::COORDINATE_BUTTON_FOLLOW_MINIMIZED = { 86, 40 };
COORD COORDINATES::COORDINATE_BUTTON_NOT_FOLLOW_MINIMIZED = { 100, 40 };

COORD COORDINATES::COORDINATE_BUTTON_ATTACK_MODE_MINIMIZED = { 100, 12 };
COORD COORDINATES::COORDINATE_BUTTON_ATTACK_MODE = { 40, 12 };
COORD COORDINATES::COORDINATE_BUTTON_BALANCED_MODE_MINIMIZED = { 88, 12 };

COORD COORDINATES::COORDINATE_BUTTON_BALANCED_MODE = { 40, 33 };
COORD COORDINATES::COORDINATE_BUTTON_DEFENSIVE_MODE_MINIMIZED = { 67, 12 };
COORD COORDINATES::COORDINATE_BUTTON_DEFENSIVE_MODE = { 40, 56 };

COORD COORDINATES::COORDINATE_BUTTON_SKULL_OLD = { 15, 74 };
COORD COORDINATES::COORDINATE_BUTTON_SKULL_OLD_MINIMIZED = { 70, 40 };

COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_0 = { 40, 103 };
COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_2 = { 40, 125 };
COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_1 = { 15, 103 };
COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_3 = { 15, 125 };

COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_0_minimized = { 40, 15 };
COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_2_minimized = { 40, 35 };
COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_1_minimized = { 15, 15 };
COORD COORDINATES::COORDINATE_BUTTON_SKULL_NEW_3_minimized = { 15, 35 };

COORD COORDINATES::COORDINATE_BUTTON_MAXIMIZE_BODY = { 158, 11 };

COORD COORDINATES::COORDINATE_TRADE_BUTTON_OK = { 145, 18 };
COORD COORDINATES::COORDINATE_TRADE_BUTTON_SELL = { 145, 30 };
COORD COORDINATES::COORDINATE_TRADE_BUTTON_BUY = { 100, 30 };
COORD COORDINATES::COORDINATE_TRADE_FIRST_ITEM = { 52, 51 };
COORD COORDINATES::COORDINATE_TRADE_SCROLL = { 9, 67 };
COORD COORDINATES::COORDINATE_TRADE_SCROLL_COUNT = { COORDINATE_TRADE_FIRST_ITEM.X, 103 };


COORD COORDINATES::COORDINATE_HELMET = { 110, 22 };
COORD COORDINATES::COORDINATE_AMULET = { 146, 36 };
COORD COORDINATES::COORDINATE_BAG = { 100, 36 };
COORD COORDINATES::COORDINATE_ARMOR = { 110, 62 };
COORD COORDINATES::COORDINATE_SHIELD = { 73, 74 };
COORD COORDINATES::COORDINATE_WEAPON = { 25, 70 };
COORD COORDINATES::COORDINATE_LEG = { 110, 95 };
COORD COORDINATES::COORDINATE_BOOTS = { 65, 135 };
COORD COORDINATES::COORDINATE_RING = { 30, 110 };
COORD COORDINATES::COORDINATE_ROPE = { 115, 108 };

int OFFSETS::offset_right_pane_child_position_x = 0x0; // deve ser revisado nao estava em uso porem, nao esta correto
int OFFSETS::offset_right_pane_child_position_y = 0x18;
int OFFSETS::offset_right_pane_child_type = 0x2c;
int OFFSETS::offset_right_pane_child_height = 0x20;
int OFFSETS::offset_right_pane_child_width = 0x0; //deve ser revisado nao estava em uso porem, nao esta correto

int OFFSETS::pos_offset_x = 0x14;
int OFFSETS::pos_offset_y = 0x18;
int OFFSETS::size_offset_x = 0x1C;
int OFFSETS::size_offset_y = 0x20;

int OFFSETS::offset_game_display_left = 0x14;
int OFFSETS::offset_game_display_top = 0x18;
int OFFSETS::offset_game_display_width = 0x1C;
int OFFSETS::offset_game_display_height = 0x20;


uint32_t RightPaneContainer::get_pixel_hidden_slots(bool is_browse_field){
	std::vector<uint32_t> offsets = { 0x24, 0x24, 0x2c, 0x30 };
	if (is_browse_field)
		offsets = { 0x24, 0x10, 0x24, 0x2c, 0x30 };
	return TibiaProcess::get_default()->read_int(self_address + 0x44, offsets, false);
}

bool RightPaneContainer::resize(bool is_browse_field){
	uint32_t final_heigth = DEFAULT_CONTAINER_HEIGHT;
	if (is_browse_field)
		final_heigth = DEFAULT_BROWSE_FIELD_CONTAINER_HEIGHT;
	if (this->heigth == final_heigth)
		return true;
	auto real_location = get_real_location();

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	return input->drag_item_on_screen(Point(real_location.x + pixel_offset_bp_places_x, real_location.y + this->heigth - 2),
		Point(real_location.x + pixel_offset_bp_places_x, real_location.y), false);
}

ClientGeneralInterface::ClientGeneralInterface(){
	std::thread(std::bind(&ClientGeneralInterface::run_check_windialog, this)).detach(); 
}

void ClientGeneralInterface::run_check_windialog(){
	MONITOR_THREAD(__FUNCTION__);
	while (true){
		Sleep(800);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state() || NeutralManager::get()->is_paused())
			continue;

		if (!TibiaProcess::get_default()->get_is_logged())
			continue;

		if (!HighLevelVirtualInput::get_default()->can_use_input())
			continue;

		std::shared_ptr<WindowDialog> dialog_window = get_dialog();
		if (!dialog_window)
			continue;		
		
		close_dialog(500);
	}
}

bool RightPaneContainer::resize(Point dimension){
	auto real_location = get_real_location();

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	return input->drag_item_on_screen(Point(real_location.x + pixel_offset_bp_places_x, real_location.y + this->heigth - 2),
		Point(real_location.x + pixel_offset_bp_places_x, real_location.y + dimension.y), false);
}

Point RightPaneContainer::get_real_location(){
	auto containers_location = Interface->get_containers_pane_placement();
	if (containers_location.is_null())
		return Point();
	return containers_location + Point(0, y);
}

Point RightPaneContainer::get_real_close_button_location(){
	auto this_location = get_real_location();
	if (this_location.is_null())
		return this_location;
	return this_location + Point(COORDINATES::COORDINATE_BUTTON_CLOSE_CONTAINER.X, COORDINATES::COORDINATE_BUTTON_CLOSE_CONTAINER.Y);
}

Point RightPaneContainer::get_real_minimize_button_location(){
	auto this_location = get_real_location();
	if (this_location.is_null())
		return this_location;
	return this_location + Point(COORDINATES::COORDINATE_BUTTON_MINIMIZE_CONTAINER.X, COORDINATES::COORDINATE_BUTTON_MINIMIZE_CONTAINER.Y);
}

Point RightPaneContainer::get_real_up_button_location(){
	auto this_location = get_real_location();
	if (this_location.is_null())
		return this_location;
	return this_location + Point(COORDINATES::COORDINATE_BUTTON_UP_CONTAINER.X, COORDINATES::COORDINATE_BUTTON_UP_CONTAINER.Y);
}

bool RightPaneContainer::up(){
	auto location = get_real_up_button_location();
	if (location.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(10);
	}

	if (!input->can_use_input())
		return false;

	input->click_left(location.x, location.y);
	return true;
}

bool RightPaneContainer::close(){
	auto location = get_real_close_button_location();
	if (location.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_left(location.x, location.y);
	return true;
}

bool RightPaneContainer::minimize(){
	if (is_minimized())
		return true;

	auto location = get_real_minimize_button_location();
	if (location.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_left(location.x, location.y);
	return true;
}

bool RightPaneContainer::maximize(){
	if (!is_minimized())
		return true;

	auto location = get_real_minimize_button_location();
	if (location.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_left(location.x, location.y);
	return true;
}

bool RightPaneContainer::is_minimized(){
	if (this->heigth <= 30)
		return true;

	return false;
}

Rect RightPaneContainer::get_rect_on_client_view(){
	auto real_point = get_real_location();
	return Rect(real_point.x, real_point.y, width, heigth);
}

Rect RightPaneContainerBattle::get_clickable_area_to_listbox_index(uint32_t index){
	Rect listview_area = get_listbox_rect_on_client_view();
	if (listview_area.is_null())
		return listview_area;

	int32_t hidden_pixel = get_hidden_pixels();
	struct { int min, max; } range = { index * 0x15/*each player on battle*/, (index + 1) * 0x15 };

	if (hidden_pixel > (range.max - 3))
		return Rect();
	else if ((hidden_pixel + listview_area.get_height()) < (uint32_t)(range.min + 3))
		return Rect();

	Rect ret(listview_area.get_x(), listview_area.get_y() + (range.min - hidden_pixel), listview_area.get_width(),
		0x15);
	if (ret.get_y() < listview_area.get_y()){
		ret.set_y(listview_area.get_y());
		ret.set_height( ret.get_height() -  (listview_area.get_y() - ret.get_y()));
	}
	if (ret.get_y() + ret.get_height() > listview_area.get_y() + listview_area.get_height())
		ret.set_height((listview_area.get_y() + listview_area.get_height()) - ret.get_y());

	return ret;
}

bool RightPaneContainerBattle::set_listbox_index_visible(uint32_t index){

	auto visible_rect = get_clickable_area_to_listbox_index(index);
	if (!visible_rect.is_null())
		return true;
	int32_t hidden_pixel = get_hidden_pixels();

	struct { int min, max; } range = { index * 0x15/*each player on battle*/, (index + 1) * 0x15 };

	int32_t diference = hidden_pixel - range.min;
	if (diference > 0){
		return scroll_up((uint32_t)(diference / 7.4));
	}
	else if (diference < 0){
		diference = abs(diference);
		return scroll_down((uint32_t)(diference / 7.4));
	}
	return true;
}

uint32_t RightPaneContainerBattle::get_hidden_pixels(){
	return TibiaProcess::get_default()->read_int(self_address + 0x44, { 0x24, 0x24, 0x30 }, false);
}

Rect RightPaneContainerBattle::get_listbox_rect_on_client_view(){
	auto rect = get_rect_on_client_view();
	if (rect.is_null())
		return rect;
	return Rect( rect.get_x() + 2, rect.get_y() + 15/*header*/, rect.get_width() - 2, (rect.get_height() - 3)/*border*/ - 16/*header*/ );
}

Point RightPaneContainerBattle::get_scroll_point(){
	auto client_area = get_listbox_rect_on_client_view();
	if (client_area.is_null())
		return Point();
	return Point(client_area.get_x() + client_area.get_width() / 2, client_area.get_y() + client_area.get_height() / 2);
}

bool RightPaneContainerBattle::scroll_up(uint32_t count){
	auto point_to_scroll = get_scroll_point();
	if (point_to_scroll.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->mouse_scroll_up(point_to_scroll, count);
	return true;
}

bool RightPaneContainerBattle::scroll_down(uint32_t count){
	auto point_to_scroll = get_scroll_point();
	if (point_to_scroll.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->mouse_scroll_down(point_to_scroll, count);
	return true;
}

Coordinate ClientGeneralInterface::get_client_placement(){
	/*WINDOWPLACEMENT placement;
	HWND handler = (HWND)TibiaProcess::get_default()->get_window_handler();
	GetWindowPlacement(
	handler,
	&placement
	);*/
	HWND handler = (HWND)TibiaProcess::get_default()->get_window_handler();
	POINT real = { 0, 0 };
	MapWindowPoints(
		HWND_DESKTOP,
		handler,
		&real,
		1
		);

	Coordinate coord;
	coord.x = -real.x;
	coord.y = -real.y;

	return coord;
}

Coordinate ClientGeneralInterface::get_client_dimension(){
	COORD retval;
	retval.X = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_WINDOW_X));
	retval.Y = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_WINDOW_Y));
	return retval;
}
/*
return -1 if not found address
*/
int ClientGeneralInterface::get_right_pane_child_address(right_pane_child_t id_type){

	int address_base_right_pane =
		TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_OF_CONTAINER),
		{ 0x34, 0x2c, 0x24 });

	/*
	deve ser revisado se nao me engano esse while tem que ser melhor definido
	*/
	int loop_count = 0;
	while (address_base_right_pane &&  loop_count < 10){
		int type = TibiaProcess::get_default()->read_int(address_base_right_pane + OFFSETS::offset_right_pane_child_type, false);
		if (id_type == type)
			return address_base_right_pane;
		address_base_right_pane = TibiaProcess::get_default()->read_int(address_base_right_pane + 0x10, false);
		loop_count++;
	}
	return -1;
}
/*
return std::shared_ptr<RightPaneChildWidget> with null reference if not found
*/
std::shared_ptr<RightPaneChildWidget> ClientGeneralInterface::findRightPaneClild(right_pane_child_t id_type){
	int address = get_right_pane_child_address(id_type);
	if (address != -1){
		std::shared_ptr<RightPaneChildWidget> retval(new RightPaneChildWidget);

		retval->x = 0; //TibiaProcess::get_default()->read_int(address + OFFSETS::offset_right_pane_child_position_x, false);
		retval->y = TibiaProcess::get_default()->read_int(address + OFFSETS::offset_right_pane_child_position_y, false);
		retval->width = 0; //TibiaProcess::get_default()->read_int(address + OFFSETS::offset_right_pane_child_width, false);
		retval->height = TibiaProcess::get_default()->read_int(address + OFFSETS::offset_right_pane_child_height, false);

		return retval;
	}
	return std::shared_ptr < RightPaneChildWidget >();
}

std::shared_ptr<RightPaneChildWidget> ClientGeneralInterface::getEquipmentChildMinimized(){
	return findRightPaneClild(right_pane_inventory_minimized);
}

std::shared_ptr<RightPaneChildWidget> ClientGeneralInterface::getEquipmentChildMaximized(){
	return findRightPaneClild(right_pane_inventory_maximized);
}

uint32_t ClientGeneralInterface::get_game_window_pos_x(){
	return get_game_window_pos().x;
}

uint32_t ClientGeneralInterface::get_game_window_pos_y(){
	return get_game_window_pos().y;
}

Point ClientGeneralInterface::get_game_window_pos(){
	Point point;
	uint32_t base =
		TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_OF_CONTAINER), { 0x30, 0x2c });

	if (!base)
		return point;


	point.x = TibiaProcess::get_default()->read_int(base + OFFSETS::offset_game_display_left, false);
	point.y = TibiaProcess::get_default()->read_int(base + OFFSETS::offset_game_display_top, false);
	return point;
}

Point ClientGeneralInterface::get_game_window_size(){
	Point point;
	uint32_t base =
		TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_OF_CONTAINER), { 0x30, 0x2c });

	if (!base)
		return point;

	point.x = TibiaProcess::get_default()->read_int(base + OFFSETS::offset_game_display_width, false);
	point.y = TibiaProcess::get_default()->read_int(base + OFFSETS::offset_game_display_height, false);
	return point;
}

Point ClientGeneralInterface::get_coordinate_as_pixel(Point point){
	Point game_window_size = get_game_window_size();
	Point game_window_position = get_game_window_pos();

	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();

	int value_of_each_sqm_in_pixel = 0;

	value_of_each_sqm_in_pixel = (game_window_size.x / SQM_COUNT_X);
	point.x = value_of_each_sqm_in_pixel / 2 + game_window_position.x + value_of_each_sqm_in_pixel * (point.x - (self_coordinate.x - 7));

	value_of_each_sqm_in_pixel = (game_window_size.y / SQM_COUNT_Y);
	int dif_our = self_coordinate.y - 5;
	int dif_coord = point.y - dif_our;

	point.y = value_of_each_sqm_in_pixel / 2 + game_window_position.y + value_of_each_sqm_in_pixel
		* dif_coord;

	return point;
}

void ClientGeneralInterface::set_maximized(){
	SetForegroundWindow((HWND)TibiaProcess::get_default()->get_window_handler());
	SetFocus((HWND)TibiaProcess::get_default()->get_window_handler());
	ShowWindow((HWND)TibiaProcess::get_default()->get_window_handler(), SW_SHOWMAXIMIZED);
}

void ClientGeneralInterface::set_minimized(){
	WINDOWPLACEMENT status;
	GetWindowPlacement((HWND)TibiaProcess::get_default()->get_window_handler(), &status);

	if (status.showCmd != SW_SHOWMINIMIZED)
		ShowWindow((HWND)TibiaProcess::get_default()->get_window_handler(), SW_SHOWMINIMIZED);
}

std::shared_ptr<RightPaneContainer> ClientGeneralInterface::get_container_by_type(right_pane_child_t type){
	uint32_t address =
		TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_OF_CONTAINER),
		{ 0x24, 0x24 });

	if (!address)
		return 0;

	std::shared_ptr<RightPaneContainer> info(new RightPaneContainer);
	ZeroMemory(info.get(), sizeof(RightPaneContainer));

	if (TibiaProcess::get_default()->
		read_memory_block(address, info.get(), sizeof(RightPaneContainer), false) != sizeof(RightPaneContainer))
		return 0;

	info->self_address = address;

	if (info->type == type)
		return info;


	while (true){
		Sleep(1);

		uint32_t next = info->address_next;

		if (!next)
			return 0;

		if (TibiaProcess::get_default()->
			read_memory_block(next, info.get(), sizeof(RightPaneContainer), false) != sizeof(RightPaneContainer))
			return 0;

		info->self_address = next;
		if (info->type == type)
			return info;
	}
	return info;
}

std::shared_ptr<RightPaneContainer> ClientGeneralInterface::get_container_at_index(uint32_t index){
	uint32_t address =
		TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_OF_CONTAINER), { 0x24, 0x24 });

	if (!address)
		return 0;

	std::shared_ptr<RightPaneContainer> info(new RightPaneContainer);
	ZeroMemory(info.get(), sizeof(RightPaneContainer));

	if (TibiaProcess::get_default()->
		read_memory_block(address, &info, sizeof(RightPaneContainer), false) != sizeof(RightPaneContainer))
		return 0;

	while (index > 0){
		if (!info->address_next)
			return 0;

		if (TibiaProcess::get_default()->
			read_memory_block(info->address_next, &info, sizeof(RightPaneContainer), false) != sizeof(RightPaneContainer))
			return 0;

		index--;
	}
	return info;
}

std::shared_ptr<RightPaneChildWidget> ClientGeneralInterface::getEquipmentChildOnRightPane(){
	auto it = getEquipmentChildMaximized();
	if (it)
		return it;
	return getEquipmentChildMinimized();
}

std::shared_ptr<RightPaneContainer> ClientGeneralInterface::get_trade_container(){
	return get_container_by_type(right_pane_trade);
}

std::shared_ptr<RightPaneContainer> ClientGeneralInterface::get_battle_container(){
	return get_container_by_type(right_pane_battle);
}

std::shared_ptr<RightPaneContainer> ClientGeneralInterface::get_vips_container(){
	//TODO
	return 0;
}

std::shared_ptr<RightPaneContainer> ClientGeneralInterface::get_skills_container(){
	//TODO
	return 0;
}

void ClientGeneralInterface::open_battle(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return;

	if (get_battle_container())
		return;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	input->send_ctrl_down();
	input->send_char(0x2, 0x300001);
	input->send_ctrl_up();
	
}

void ClientGeneralInterface::close_battle(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return;

	if (!get_battle_container())
		return;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	input->send_ctrl_down();
	input->send_char(0x2, 0x300001);
	input->send_ctrl_up();
}

void ClientGeneralInterface::open_vip(){
	//if (!TibiaProcess::get_default()->get_is_logged())
	//	return;
	if (!get_skills_container())
		return;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	input->send_ctrl_down();
	input->send_char(0x10/*, 0xc00190001*/);
	input->send_ctrl_up();
}

void ClientGeneralInterface::close_vip(){
	//if (!TibiaProcess::get_default()->get_is_logged())
	//	return;
	if (get_vips_container())
		return;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	input->send_ctrl_down();
	input->send_char(0x10/*, 0xc00190001*/);
	input->send_ctrl_up();
}

void ClientGeneralInterface::open_skills(){
	//if (!TibiaProcess::get_default()->get_is_logged())
	//	return;
	if (get_skills_container())
		return;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	input->send_ctrl_down();
	input->send_char(0x13/*, 0xc001f0001*/);
	input->send_ctrl_up();
}

void ClientGeneralInterface::close_skills(){
	//if (!TibiaProcess::get_default()->get_is_logged())
	//	return;
	if (!get_skills_container())
		return;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	input->send_ctrl_down();
	input->send_char(0x13/*, 0xc001f0001*/);
	input->send_ctrl_up();
}

Point ClientGeneralInterface::get_containers_pane_placement(){
	/*
	FIX the x coordinate is returning as zero.
	*/
	return (Point(
		get_client_dimension().x - CONTAINER_DEFAULT_WIDTH
		, TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_OF_CONTAINER), { 0x24, 0x18 })));
}

std::vector<std::shared_ptr<RightPaneContainer>> ClientGeneralInterface::get_containers(){
	std::vector<std::shared_ptr<RightPaneContainer>> retval;
	uint32_t address =
		TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_OF_CONTAINER), { 0x24, 0x24 });

	if (!address)
		return retval;

	std::shared_ptr<RightPaneContainer> info(new RightPaneContainer);

	if (TibiaProcess::get_default()->
		read_memory_block(address, info.get(), sizeof(RightPaneContainer), false) != sizeof(RightPaneContainer))
		return retval;
	retval.push_back(info);

	while (true){
		Sleep(1);

		uint32_t next = info->address_next;
		if (!next)
			break;

		info = std::shared_ptr<RightPaneContainer>(new RightPaneContainer);
		if (TibiaProcess::get_default()->
			read_memory_block(next, info.get(), sizeof(RightPaneContainer), false) != sizeof(RightPaneContainer))
			break;

		retval.push_back(info);
	}
	return retval;
}

uint32_t ClientGeneralInterface::get_dialog_address(){
	return TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_WINDOW));
}

uint32_t WindowDialog::get_move_scroll_count(){
	return TibiaProcess::get_default()->read_int(address + 0x34, { 0x34, 0x30 }, false);
}

std::shared_ptr<WindowDialog> ClientGeneralInterface::get_dialog(){
	std::shared_ptr<WindowDialog> retval;
	uint32_t address = Interface->get_dialog_address();
	if (address){
		retval = std::shared_ptr<WindowDialog>(new WindowDialog);
		retval->address = address;
	}
	return retval;
}

bool ClientGeneralInterface::close_dialog(int32_t timeout){
	TimeChronometer chronometer;
	while (chronometer.elapsed_milliseconds() < timeout){
		uint32_t window_address = get_dialog_address();
		if (!window_address)
			return true;

		auto input = HighLevelVirtualInput::get_default();
		TimeChronometer time;
		while (time.elapsed_milliseconds() < input->get_time_out()){
			if (input->can_use_input())
				break;
			else
				Sleep(1);
		}

		if (!input->can_use_input())
			continue;

		input->send_esc();
		Sleep(50);
	}
	return false;
}

bool WindowDialog::set_scroll_count(uint32_t count){
	uint32_t count_now = get_move_scroll_count();
	if (count_now == count)
		return true;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(10);
	}

	if (!input->can_use_input())
		return false;

	input->scroll_count(count_now, count);
	Sleep(20);

	count_now = get_move_scroll_count();
	if (count_now != count)
		return false;

	return true;
}

void ClientGeneralInterface::click_follow(){
	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	auto equipment = getEquipmentChildMinimized();
	auto client_dimension = get_client_dimension();

	if (equipment){
		Point point = Point(client_dimension.x - COORDINATES::COORDINATE_BUTTON_FOLLOW_MINIMIZED.X,
			equipment->y + COORDINATES::COORDINATE_BUTTON_FOLLOW_MINIMIZED.Y);

		input->click_left(point);
	}
	else {
		equipment = getEquipmentChildMaximized();
		if (equipment){
			Point point = Point(client_dimension.x - COORDINATES::COORDINATE_BUTTON_FOLLOW.X,
				equipment->y + COORDINATES::COORDINATE_BUTTON_FOLLOW.Y);

			input->click_left(point);
		}
	}
}

void ClientGeneralInterface::click_not_follow(){
	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;

	auto equipment = getEquipmentChildMinimized();
	auto client_dimension = get_client_dimension();
	if (equipment){
		int temp_x = client_dimension.x - COORDINATES::COORDINATE_BUTTON_NOT_FOLLOW_MINIMIZED.X;
		int temp_y = equipment->y + COORDINATES::COORDINATE_BUTTON_NOT_FOLLOW_MINIMIZED.Y;
		Point point = Point(temp_x,
			temp_y);

		input->click_left(point);
	}
	else{
		equipment = getEquipmentChildMaximized();
		if (equipment){
			int temp_x = client_dimension.x - COORDINATES::COORDINATE_BUTTON_NOT_FOLLOW.X;
			int temp_y = equipment->y + COORDINATES::COORDINATE_BUTTON_NOT_FOLLOW.Y;
			Point point = Point(temp_x,
				temp_y);

			input->click_left(point);
		}
	}
}

Rect RightPaneChildWidget::get_rect_on_client_view(){
	return	Rect(Interface->get_client_dimension().x - CONTAINER_DEFAULT_WIDTH, y, width, height);
}


#define offset_to_pointer_name 0x2c
#define offset_pos_x 0x14


uint32_t ChatChannels::get_channel_address(std::string& channel_name){
	uint32_t Address =
		_read_int(
		_read_int(
		_read_int(
		_read_int(_get_address(ADDRESS_TYPE::ADDRESS_BASE_GUI), true)
		+ 0x40, false)
		+ 0x30, false)
		+ 0x24, false);

	uint32_t index = 0;
	while (Address != 0){
		uint32_t temp_address = _read_int(Address + offset_to_pointer_name, false);
		if (temp_address != 0){
			std::string memory_channel_name = TibiaProcess::get_default()->read_string(temp_address, false);
			if (_stricmp(&memory_channel_name[0], &channel_name[0]) == 0){
				return Address;
			}
			index++;
			Address = _read_int(Address + 16, false);
		}
		else
			return UINT32_MAX;
	}
	return UINT32_MAX;
}

uint32_t ChatChannels::get_y(){
	uint32_t y = TibiaProcess::get_default()->read_int(_get_address(ADDRESS_TYPE::ADDRESS_BASE_GUI),
		std::vector < uint32_t > {0x34, 0x4, 0x64});

	y = Interface->get_client_dimension().y - (y + 10);
	return y;
}

uint32_t ChatChannels::get_channel_x(std::string& channel_name){
	uint32_t address = get_channel_address(channel_name);

	if (address != UINT32_MAX)
		return _read_int(address + offset_pos_x, false) + 60;

	return UINT32_MAX;
}

Point ChatChannels::get_channel_button_point(std::string& channel_name){
	uint32_t channel_info_address = get_channel_address(channel_name);
	if (channel_info_address == UINT32_MAX)
		return Point();

	uint32_t x = get_channel_x(channel_name);
	if (x == UINT32_MAX)
		return Point();

	uint32_t y = get_y();
	return Point(x, y);
}

bool ChatChannels::wait_channel_change(std::string channel_name, uint32_t timeout){
	TimeChronometer timer;
	while (timeout < (uint32_t)timer.elapsed_milliseconds()){
		if (_stricmp(&get_current_channel_name()[0], &channel_name[0]) == 0)
			return true;
		Sleep(10);
	}
	return false;
}

bool ChatChannels::set_channel(std::string& channel_name){
	std::string current_channel = get_current_channel_name();
	if (_stricmp(&current_channel[0], &channel_name[0]) == 0)
		return true;

	Point click_point = get_channel_button_point(channel_name);
	if (click_point.is_null())
		return false;
	HighLevelVirtualInput::get_default()->click_left(click_point, false, false);
	return wait_channel_change(channel_name);
}

uint32_t ChatChannels::get_message_address(){
	uint32_t y =
		_read_int(
		_read_int(
		_read_int(
		_read_int(_get_address(ADDRESS_TYPE::ADDRESS_BASE_GUI), true)
		+ 0x40, false)
		+ 0x40, false)
		+ 0x2c, false);
	return y;
}

void ChatChannels::set_text(std::string& text){
	uint32_t address = get_message_address();
	if (address == UINT32_MAX)
		return;
	TibiaProcess::get_default()->write_string(address, text);
}

std::string ChatChannels::get_current_channel_name(){
	return "";
}

void ChatChannels::send_message(std::string& message, std::string channel){
	set_channel(channel);
	set_text(message);
	HighLevelVirtualInput::get_default()->send_enter();
}