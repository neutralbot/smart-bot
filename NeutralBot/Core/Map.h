#pragma once
#include "Util.h"
#include "Tile.h"
#include "TibiaProcess.h"
#include "..\\MemoryCounter.h"

#define offsetMapX = 0x8;
#define offsetMapY = 0xc;
#define offsetMapZ = 0x10;
#define offsetBeginMapFront = 0x1C;
#define offsetBeginMapBack = 0x10000;
#define offsetBetweenMaps = 0x200A8;

#define MINIMAP_COUNT 12

#define WALKABILITY_MAP_X 256
#define WALKABILITY_MAP_Y 256

#define VISIBLE_TILE_X	15
#define VISIBLE_TILE_Y	11
#define WALKABLE 1
#define UNWALKABLE 0
#define UNDEFINED 255

enum tile_back_t: unsigned char{
	not_walkable = 0xff,//not walkable(players and walls)
	back_uptiles = 0x6a,//embaixo de arvores walkable
	not_explored = 0xfa,//nao explorado walkable
	walkable_flor = 0x0a//chao walkable
};

enum tile_front_t : unsigned char{
	snow_walkable_back_check = 0xd7,
	never_walkable2_back_check = 114,
	black_walkable_back_check = 0x0,
	tile_unwalkable = 0xba,
	black2_walkable_back_check = 0xa0,
	blue_water_walkable_back_check = 0x28,
	blue_water2_walkable_back_check = 0x33,
	lime_green_walkable = 0x18,
	dark_green_unwalkable = 0xc,
	grey_city_flor_walkable = 0x81,
	yellow = 0xd2,
	grown_weak_walkable = 0x79,
	grey_dark_not_walkable = 0x56,
	blue_weak_snow_walkable = 0xb3,
	black_different_walkable = 0x8c,
	black_differet_walkable_check_back = 0xcf
};

enum file_load_status_t{
	file_load_none,
	file_load_fail,
	file_load_success,
	file_save_fail,
	file_save_success
};

inline pathfinder_walkability_t get_walkability_map_file(unsigned char map_back_value, unsigned char map_front_value){
	return pathfinder_walkability_t::pathinder_walkability_walkable;
}

class MapTiles;
class MapMinimap;
class MiniMapFile;


#pragma pack(push,1)
struct PureMap{
	unsigned char raw_info[140];/*sei la que diabos sao esses outros dados*/

	uint32_t address_of_something;
	uint32_t idk;
	uint32_t x;
	uint32_t y;
	uint32_t z;
	uint32_t parece_posicao_do_mapax;
	uint32_t parece_posicao_do_mapay;
	
	unsigned char raw_map_front[256][256];
	unsigned char raw_map_back[256][256];

	bool get_walkability_at(uint32_t x, uint32_t y);

	int32_t get_real_base_x();
	int32_t get_real_base_y();
};

class MiniMapFile : public MemoryCounter{
	Coordinate central_coord;
public:
	MiniMapFile();
	~MiniMapFile();

	file_load_status_t state;
	uint32_t data_hash;
	unsigned char data_1[256][256];
	unsigned char data_2[256][256];

	void load(int z, int y, int x);

	bool loaded();

	void update_hash();

	bool hash_match();

	pathfinder_walkability_t get_walkability_at(uint32_t x, uint32_t y, map_type_t map_front_or_back);
};

class ChacheMiniMapFile : public MemoryCounter{
	Coordinate central_coord;
	void save();

public:
	ChacheMiniMapFile();
	
	~ChacheMiniMapFile();

	file_load_status_t state;
	uint32_t data_hash;
	unsigned char data_1[256][256];

	void load(int z, int y, int x);

	void save(int z, int y, int x);

	bool loaded();

	void update_hash();

	bool hash_match();

	static file_load_status_t save(int z, int y, int x, unsigned char* data);
};

struct quadrant_start_end{
	uint32_t start_x, end_x, start_y, end_y;
};
#pragma pack(pop)

class MapMinimap{	
	friend class MapTiles;
	friend class MapMinimap;
	friend class MiniMapFile;
	friend class MapCacheControl;

	neutral_mutex mtx_access;
	
	Coordinate center_now;
	Coordinate last_center;

	std::vector<Coordinate>* last_yellow_coords;
	std::vector<Coordinate>* last_yellow_coords_swap;

	uint32_t last_x;
	uint32_t last_y;
	uint32_t last_z;

	void update_changed_positions();

	void update_last_positions();
	bool has_some_custom_change = false;

	std::vector<uint32_t> block_ids;
	std::vector<uint32_t> release_ids;

	PureMap arround_maps[12];
	static PureMap* organized_arround_maps_ptr[3][3];

	static unsigned char walkability_temp[map_total_t][256][256];

	PureMap* get_pure_map_at(uint32_t x_id, uint32_t y_id, uint32_t z);

	uint32_t sqm_to_consider_near_yellow_points;
	
	void copy_special_areas_to_map_t(map_type_t mtype);

	void refresh();

	void update_map_special_areas();

	void read_all_arround_maps();

	void update_current_pos();

	void refresh_arround_maps();
	
	void refresh_back_front();

	void refresh_small_maps();

	static bool changed_last_center;
	static bool changed_last_x;
	static bool changed_last_y;
	static bool changed_last_z;

	static bool valid_arround_maps[4];
	static uint32_t quadrant_coords[4][2];
	
	static std::map<uint32_t, std::vector<std::pair<uint32_t, uint32_t>>> current_maps_in_memory;
	
	void swap_accesible_data();
	void lock();
	void unlock();
	static void flush_special_area_maps();

public:
	static unsigned char walkability[map_total_t][256][256];
	static char switch_walkability_back(unsigned char entrace);
	static unsigned char special_area_map[2][256][256];
	static Coordinate current_coord;
	static Rect current_rect;


	MapMinimap();

	static bool is_trapped_around(map_type_t map_type = map_front_cached_advanced_t);

	bool has_some_custom_change_in_walkability();

	bool is_coordinate_yellow(Coordinate coord);

	uint32_t get_sqm_to_consider_near_yellow_points(){
		return 	sqm_to_consider_near_yellow_points;
	}

	void set_sqm_to_consider_near_yellow_points(uint32_t value){
		sqm_to_consider_near_yellow_points = value;
	}

	uint32_t distance_near_yellow_coord();

	bool check_nearest_yellow_coordinate(Coordinate coord, uint32_t consider_nearest);

	bool get_walkability(Coordinate coord, map_type_t map_t);

	pathfinder_walkability_t get_walkability_value_at(Coordinate coord, map_type_t map_t);

	bool get_walkability_map(Coordinate coord, map_type_t map_t, Coordinate& current_coord);

	pathfinder_walkability_t get_walkability_value_at_map(Coordinate coord, map_type_t map_t, Coordinate& current_coord);
	
	void add_block_id(uint32_t id);

	void add_release_id(uint32_t id);

	void remove_block_id(uint32_t id);

	void remove_release_id(uint32_t id);

	static void GeneralRefresh();
};

class MapTiles{
	friend class MapTiles;
	friend class MapMinimap;
	friend class MiniMapFile;
	friend class MapCacheControl;

	std::vector<TilePtr> current_tiles;
	neutral_mutex mtx_access;
	uint32_t get_max_visible_level();
	void refresh();
	static uint64_t refreshed_count;
public:

	std::vector<CreatureOnBattlePtr> get_another_creatures_on_self_tile();

	std::vector<CreatureOnBattlePtr> get_monsters_on_self_tile();

	std::vector<CreatureOnBattlePtr> get_another_players_on_self_tile();
	
	uint64_t get_refreshed_count(){
		return refreshed_count;
	}

	void increase_refreshed_count(){
		refreshed_count++;
	}

	MapTiles();

	static void wait_refresh();

	uint32_t get_max_visible_level_up();

	uint32_t get_max_visible_level_down();
		
	tile_raw* MapTiles::get_raw_tile_at(uint32_t x, uint32_t y, int8_t z, bool fix_level_offset = true);

	TilePtr MapTiles::get_tile_from_raw(uint32_t x, uint32_t y, int8_t z, bool fix_level_offset = true);

	TilePtr get_tile_at(Coordinate coord, bool fix_z = true);

	std::vector<TilePtr> get_tiles_with_id(uint32_t id);

	std::vector<TilePtr> get_tiles_with_top_id(uint32_t id, bool ignore_player = false);

	bool contains_furniture_at(Coordinate coord);
	/*
		default coordinate is not specified it uses self character coordinate,
		default max_dist = 1
	*/
	std::vector<TilePtr> get_tiles_with_id_near_to(uint32_t item_id, Coordinate coord, uint32_t max_dist = 1);

	std::vector<TilePtr> get_depot_tiles();

	std::vector<TilePtr> get_depot_chest();

	TilePtr get_nearest_depot_tile(bool check_reachable = true, map_type_t map_type = map_front_mini_t);
	
	TilePtr get_tile_from_raw(uint32_t x, uint32_t y);

	uint32_t wait_delay_near_yellow_coord(uint32_t consider_nearest, uint32_t delay_start);

	std::vector<TilePtr> get_furniture_tiles();

	std::vector<std::pair<TilePtr, std::shared_ptr<ConfigPathProperty>>> get_custom_item_tiles();

	std::vector<TilePtr> get_rune_item_tiles();

	std::vector<TilePtr> get_creature_tiles();

	std::vector<TilePtr> get_player_tiles();

	std::vector<TilePtr> get_monster_tiles();

	std::vector<TilePtr> get_summon_tiles();

	std::vector<TilePtr> get_not_walkable_tiles();

	std::vector<TilePtr> get_not_pathable_tiles();

	std::vector<TilePtr> get_hole_or_step_tiles();

	std::vector<TilePtr> get_teleport_tiles();

	std::vector<TilePtr> get_tiles_with_item_attributes(std::vector<attr_item_t> attributes);

	bool have_id_near(int32_t x, int32_t y, uint32_t item_id);

	Coordinate find_coordinate_with_top_id(uint32_t item_id, map_type_t map_type = map_front_mini_t, bool ignore_player = false);

	Coordinate find_coordinate_contains_id(uint32_t item_id, map_type_t map_type = map_front_mini_t);

	static bool coordinate_is_traped(uint32_t x, uint32_t y, map_type_t map_type = map_front_mini_t, uint32_t free_slots = 1);

	static bool coordinate_is_traped(Coordinate coord, map_type_t map_type = map_front_t, uint32_t free_slots = 1);

	bool is_traped_on_screen(map_type_t map_type = map_front_t);

	std::vector<TilePtr> get_tiles_near_coordinate(Coordinate coordinate, bool check_reachable = false, map_type_t map_type = map_front_mini_t, uint32_t consider_near = 1);

	TilePtr get_nearest_tile(std::vector<TilePtr>& tiles_vector, bool check_reachable = false, map_type_t map_type = map_front_mini_t);

	TilePtr get_tile_nearest_coordinate(Coordinate coord, bool check_reachable = true, map_type_t map_type = map_front_mini_t, uint32_t consider_near = 1);

	static bool check_sight_line(Coordinate& fromPos, Coordinate& toPos, bool check_z = true);

	static bool is_cordinate_onscreen(Coordinate coord);

	static bool can_click_coord(Coordinate& toPos);
};

typedef unsigned char(*map_ptr_array)[256][256];

struct map_ptr_pair{
	map_ptr_pair(uint32_t _x, uint32_t _y, map_ptr_array _ptr) : x(_x), y(_y), ptr(_ptr){
	}
	uint32_t x, y;
	map_ptr_array ptr;
};

class MapCacheControl: public MemoryCounter{
	friend class MapTiles;
	friend class MapMinimap;
	friend class MiniMapFile;

	TimeChronometer last_change_z;

	std::map < uint32_t,
		std::map < uint32_t,
		std::map < uint32_t,
		MiniMapFile * > >> chache_tibia_maps;

	std::map < uint32_t,
		std::map < uint32_t,
		std::map < uint32_t,
		ChacheMiniMapFile * >>> chache_smart_maps;
	
	ChacheMiniMapFile* get_smart_map_cached(uint32_t x, uint32_t y, uint32_t z);

	MiniMapFile* get_tibia_map_cached(uint32_t x, uint32_t y, uint32_t z);

	uint32_t last_x_center = 0;
	uint32_t last_y_center = 0;
	uint32_t last_z_center = 0;

	uint32_t last_quadrant_x;
	uint32_t last_quadrant_y;

	void unload_not_arround_maps();

	void unload_not_arround_smart_bot_maps();

	void load_arround_maps();

	void load_cache_tibia_map(uint32_t x, uint32_t y, uint32_t z);

	void load_cache_smart_map(uint32_t x, uint32_t y, uint32_t z);

	void check_load_tibia_maps(uint32_t current_x, uint32_t current_y, uint32_t current_z);

	void update_cached_arround_maps();

	void unload_not_necessary_cached_maps();
	static MiniMapFile* organized_tibia_map[3][3];
	static ChacheMiniMapFile* organized_smart_map[3][3];

	static unsigned char organized_tibia_map_buffer[256][256];
	static unsigned char organized_smart_map_buffer[256][256];

	void notify_z_changed(){
		last_change_z.reset();
	}

	std::map < uint32_t/*z*/,
		std::map < uint32_t/*y*/,
		std::map < uint32_t/*x*/,
		unsigned char(*)[256][256] >> > cached_maps;

	std::map < uint32_t/*z*/,
		std::map < uint32_t/*y*/,
		std::map < uint32_t/*x*/,
		unsigned char(*)[256][256] >> > clean_cached_maps;

	unsigned char** get_map_ptr(uint32_t map_id_x, uint32_t map_id_y, uint32_t z, bool clean = false);

	Coordinate last_refresh_coord;
	Coordinate last_full_refresh_coord;

	bool create_map(uint32_t map_id_x, uint32_t map_id_y, uint32_t z, bool clean);
	bool create_map_from_file(uint32_t map_id_x, uint32_t map_id_y, uint32_t z, bool clean);

	
	void update_screen();

	void generate_map_type_cache();
public:
	void save_map_files();

	pathfinder_walkability_t get_walkability_at(uint32_t x, uint32_t y);

	MapCacheControl();

	void set_walkability(uint32_t x, uint32_t y, uint32_t z, unsigned char walkability, bool clean = false);
	void update_undef_walkability(uint32_t x, uint32_t y, uint32_t z, unsigned char walkability, bool clean = false);
};

extern MapTiles* gMapTilesPtr;
extern MapMinimap* gMapMinimapPtr;
extern MapCacheControl* gMapCacheControlPtr;

void init_maps();

