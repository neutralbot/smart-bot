#pragma once
#include "ContainerManager.h"
#include "Actions.h"
#include "Util.h"
#include <string>
#include <map>
#include <memory>
#include <vector>

#pragma pack(push,1)
struct AdvancedRepoterItens{
	std::string item;
	uint32_t min;
	uint32_t max;
	DEFAULT_GET_SET(std::string, item);
	DEFAULT_GET_SET(uint32_t, min);
	DEFAULT_GET_SET(uint32_t, max);
};
#pragma pack(pop)


#pragma pack(push,1)
class AdvancedRepoterId{
	std::map<std::string, std::shared_ptr<AdvancedRepoterItens>> mapRepotItens;
public:
	AdvancedRepoterId();
	std::string characterBackpack;
	std::string depotBoxName;
	uint32_t characterBackpackId;
	uint32_t depotBoxId;
	
	DEFAULT_GET_SET(std::string, characterBackpack);

	std::map<std::string, std::shared_ptr<AdvancedRepoterItens>> getMapRepoItens();
	
	void set_DepotBox(std::string itemName, std::string container_name);
	void set_Item(std::string id, std::string name);
	void set_Min(std::string id, uint32_t min);
	void set_Max(std::string id, uint32_t max);

	std::string get_Item(std::string id, std::string name);
	uint32_t get_Min(std::string id, uint32_t min);
	uint32_t get_Max(std::string id, uint32_t max);
	
	std::string requestNewitem_id();
	void removeRepotItem(std::string id);
	std::string addRepotItem(std::string item, int min, int max);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);
};
#pragma pack(pop)

#pragma pack(push,1)
class AdvancedRepoterManager{
	std::map<std::string, std::shared_ptr<AdvancedRepoterId>> mapRepotId;
	static std::shared_ptr<AdvancedRepoterManager> mAdvancedRepoterManager;
public:
	AdvancedRepoterManager();

	std::map<std::string, std::shared_ptr<AdvancedRepoterId>> getMapRepoId();
	std::shared_ptr<AdvancedRepoterId> getRepotIdRule(std::string id_);

	std::string requestNewRepotIdById(std::string current_id);
	std::string requestNewRepotId();
	void removeRepotId(std::string id);
	bool changeRepotId(std::string newId, std::string oldId);
	
	void request_all_repot_from_depot();
	bool move_items(ItemContainerPtr depotBoxContainer, std::shared_ptr<AdvancedRepoterId> RepotId);
	bool repot_from_depot(std::string id);
	bool try_to_move_item(ItemContainerPtr mainBackpack, int destinyBackpackId, std::shared_ptr<AdvancedRepoterItens> item);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);
	
	static void reset();
	void clear();
	static std::shared_ptr<AdvancedRepoterManager> get();
};
#pragma pack(pop)
