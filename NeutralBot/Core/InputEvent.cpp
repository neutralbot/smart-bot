#include "InputEvent.h"

std::string input_t_as_string(input_t t_val){
	switch (t_val){
	case input_type_none:
		return "input_type_none";
	case input_type_mouse:
		return "input_type_mouse";
	case input_type_keyboard:
		return "input_type_keyboard";
	}
	return "not_found";
}

std::string mouse_button_t_as_string(mouse_button_t t_val){
	switch (t_val){
	case mouse_button_none:
		return "mouse_button_none";
	case mouse_button_left:
		return "mouse_button_left";
	case mouse_button_rigth:
		return "mouse_button_rigth";
	case mouse_button_middle:
		return "mouse_button_middle";
	case mouse_button_additional_1:
		return "mouse_button_additional_1";
	case mouse_button_additional_2:
		return "mouse_button_additional_2";
	}
	return "not_found";
}

std::string mouse_event_t_as_string(mouse_event_t t_val){
	switch (t_val){
	case mouse_event_move:
		return "mouse_event_move";
	case mouse_event_button_down:
		return "mouse_event_button_down";
	case mouse_event_button_up:
		return "mouse_event_button_up";
	case mouse_event_double_click:
		return "mouse_event_double_click";
	case mouse_event_wheel:
		return "mouse_event_wheel";
	}
	return "mouse_event_none";
}

std::string keyboard_event_t_as_string(keyboard_event_t t_val){
	switch (t_val){
	case keyboard_event_none:
		return "keyboard_event_none";
	case keyboard_key_down:{
		return "keyboard_key_down";
	}
	case keyboard_key_up:{
		return "keyboard_key_up";
	}
	case keyboard_key_press:
		return "keyboard_key_press";
	}
	return "not_found";
}

InputBaseEvent::InputBaseEvent(input_t type){
	input_type = type;
}

bool InputBaseEvent::is_mouse_event(){
	return false;
}

bool InputBaseEvent::is_keyboard_event(){
	return false;
}

void InputBaseEvent::print(){
	if (input_type == input_t::input_type_keyboard){
		((KeyboardInput*)this)->print();
	}
	else{
		((MouseInput*)this)->print();
	}
}

std::string MouseInput::get_button_as_string(mouse_button_t button){
	switch (button){
	case mouse_button_none:{
		return "MOUSE NONE";
	}break;
	case mouse_button_left:{
		return "MOUSE LEFT";
	}break;
	case mouse_button_rigth:{
		return "MOUSE RIGTH";
	}break;
	case mouse_button_middle:{
		return "MOUSE MIDDLE";
	}break;
	case mouse_button_additional_1:{
		return "MOUSE ADD BUTTON 1";
	}break;
	case mouse_button_additional_2:{
		return "MOUSE ADD BUTTON 2";
	}break;
	}
	return "MOUSE NONE";
}

MouseInput::MouseInput() :InputBaseEvent(input_type_mouse){
}

std::string MouseInput::get_button_as_string(){
	return get_button_as_string((mouse_button_t)button_code);
}

bool MouseInput::is_mouse_event(){
	return true;
}

bool MouseInput::is_keyboard_event(){
	return false;
}

bool MouseInput::is_mouse_move_event(){
	return event_type == mouse_event_t::mouse_event_move;
}

void MouseInput::print() {
}

KeyboardInput::KeyboardInput() : InputBaseEvent(input_type_keyboard){
}

std::string KeyboardInput::get_button_as_string(){
	return key_string;
}

bool KeyboardInput::is_mouse_event(){
	return false;
}

bool KeyboardInput::is_keyboard_event(){
	return true;
}

void KeyboardInput::print() {
}

InputHolder::InputHolder() : input_ptr(0){
}

InputHolder::InputHolder(InputBaseEvent* input) : input_ptr(0){
	if (input->is_mouse_event()){
		input_ptr = new MouseInput();
		*(MouseInput*)input_ptr = *(MouseInput*)input;
	}
	else if (input->is_keyboard_event()){
		input_ptr = new KeyboardInput();
		*(KeyboardInput*)input_ptr = *(KeyboardInput*)input;
	}
}

InputHolder::~InputHolder(){
	if (input_ptr)
		if (input_ptr->is_mouse_event()){
		delete (MouseInput*)input_ptr;
		}
		else if (input_ptr->is_keyboard_event()){
			delete (KeyboardInput*)input_ptr;
		}
}

bool InputHolder::is_mouse(){
	if (!input_ptr)
		return false;
	return input_ptr->is_mouse_event();
}

bool InputHolder::is_keyboard(){
	if (!input_ptr)
		return false;
	return input_ptr->is_keyboard_event();
}
