#include "constants.h"

class PvpManager{
public:
	pvp_new_t get_skull_mode_new();
	pvp_old_t get_skull_mode_old();
	attack_mode_t get_attack_mode();

	bool set_skull_mode_old(pvp_old_t style);
	bool set_skull_mode_new(pvp_new_t mode);
	bool set_balanced_attack_mode();
	bool set_offensive_attack_mode();
	bool set_defensive_attack_mode();
	bool set_attack_mode(attack_mode_t mode);

	static PvpManager* get();
};
