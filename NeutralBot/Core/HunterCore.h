#pragma once
#include "../HunterManager.h"
#include <functional>
#include "../SpellManager.h"
#include "Time.h"



#pragma pack(push,1)
class CreatureOnBattle{
public:
	uint32_t id;//0
	char name[32];//4
	uint32_t pos_z;//36
	uint32_t pos_y;//40
	uint32_t pos_x;//44
	uint32_t dist_from_center_y;//48
	uint32_t dist_from_center_x;//52
	uint32_t direction;//56
	uint32_t idk2;//56//0
	uint32_t idk3;//60//1
	uint32_t idk4;
	uint32_t idk5;
	uint32_t idk6;
	uint32_t walk_state;
	uint32_t idk7;
	uint32_t before;
	uint32_t next;
	uint32_t visible;
	uint32_t idk8;//90
	uint32_t idk9;
	uint32_t idk10;
	uint32_t idk11;
	uint32_t idk12;
	uint32_t mount;
	uint32_t idk13;
	uint32_t light;
	uint32_t light_color;
	uint32_t idk14;
	uint32_t life;
	uint32_t idk15;
	uint32_t idk16;
	uint32_t skull;
	uint32_t idk17;
	uint32_t on_screenOld;
	uint32_t on_screenNew;
	uint32_t idk18;
	uint32_t idk19;
	uint32_t idk21;
	uint32_t idk22;
	uint32_t idk23;
	uint32_t type;
	uint32_t idk24;
	uint32_t idk28;
	uint32_t idk29;
	uint32_t idk30;
	uint32_t idk31;
	uint32_t idk32;
	uint32_t idk33;
	//164
	bool is_player();
	bool is_summon();
	bool is_npc();
	bool is_monster();

	Coordinate get_coordinate_monster();
	Coordinate get_axis_displacement();
	Coordinate get_displaced_coordinate();
	Coordinate get_mixed_coordinate();

	uint32_t get_distance();

	bool is_walking();
};
#pragma pack(pop)

typedef std::shared_ptr<CreatureOnBattle> CreatureOnBattlePtr;


enum hunter_event_t{
	hunter_event_creature_enter_screen,
	hunter_event_creature_out_screen,
	hunter_event_creature_die,
	hunter_event_target_changed
};


class BattleList{
	std::vector<std::string> creatures_appeared;
	std::vector<std::function<void(CreatureOnBattlePtr)>> on_creature_enter_screen;
	std::vector<std::function<void(CreatureOnBattlePtr)>> on_creature_out_screen;
	std::vector<std::function<void(CreatureOnBattlePtr)>> on_creature_die;
	std::vector<std::function<void(CreatureOnBattlePtr)>> on_target_changed;

public:

	std::vector<std::string> get_monsters_appeared();
	std::vector<std::string> get_npcs_appeared();
	std::vector<std::string> get_players_appeared();
	std::vector<std::string> get_creature_appeared();


	uint64_t refresh_count = 0;

	void wait_refresh();
	uint32_t current_target_id;
	uint32_t last_target_id;

	void notify_on_creature_enter_screen(CreatureOnBattlePtr creature);
	void notify_on_creature_out_screen(CreatureOnBattlePtr creature);
	void notify_on_creature_die(CreatureOnBattlePtr creature);
	void notify_on_target_changed(CreatureOnBattlePtr creature);

	void add_event(hunter_event_t event_type, std::function<void(CreatureOnBattlePtr)> callback);

	neutral_mutex mtx_access;
	std::vector<CreatureOnBattlePtr> all_creatures;
	std::map<int /*position_on_battle*/, CreatureOnBattlePtr>* creatures;
	std::map<int /*position_on_battle*/, CreatureOnBattlePtr>* creatures_swap;

	std::map<int /*position_on_battle*/, CreatureOnBattlePtr>* creatures_on_screen;
	std::map<int /*position_on_battle*/, CreatureOnBattlePtr>* creatures_on_screen_swap;


	BattleList();

	void swap_backbuffer();

	void update();

	void compare_buffers();

	std::map<int, CreatureOnBattlePtr> get_creatures_at_coordinate(Coordinate coord, bool ignoreZ = false, bool lock = true);

	std::vector<CreatureOnBattlePtr> get_creatures_near_coordinate
		(Coordinate coord, uint32_t sqm_to_consider_near = 1, bool ignoreZ = false, bool lock = true);

	std::vector<CreatureOnBattlePtr> get_creatures_on_area
		(Coordinate coord, uint32_t sqm_to_consider_near = 1, bool ignoreZ = false, bool lock = true);

	std::vector<CreatureOnBattlePtr> get_creatures_on_area_effect(Coordinate coord, area_type_t areaEffect);

	static std::vector<CreatureOnBattlePtr> get_creatures_on_area_effect(
		std::map<int, CreatureOnBattlePtr>& creatures_on_screen, Coordinate coord, area_type_t areaEffect);

	CreatureOnBattlePtr find_creature_on_screen(std::string name, bool ignoreZ = false, bool lock = true);

	CreatureOnBattlePtr find_creature_by_id(uint32_t id, bool ignoreZ = false, bool lock = true);

	bool is_traped_by_monster();

	bool have_player();

	bool have_other_player();

	bool have_creature_on_screen();

	std::map<int, CreatureOnBattlePtr> get_creatures_on_screen(bool ignoreZ = false, bool lock = true);

	uint32_t BattleList::wait_creature_out_of_range(uint32_t creature_id,
		int min_dist, int max_dist, uint32_t timeout = 2000,
		bool use_displaced_coords = true);

	uint32_t get_creature_count_in_radius(std::vector<std::string> creatures_name, uint32_t radius, bool must_be_reachable = false);

	CreatureOnBattlePtr get_creature_by_id(uint32_t id);

	std::vector<CreatureOnBattlePtr> get_creatures_in_radius(std::vector<std::string> creatures_name, uint32_t radius, bool must_be_reachable = false);

	std::vector<CreatureOnBattlePtr> refresh_battle_in_gui();

	uint32_t get_creature_index_in_battle_gui(uint32_t id);

	CreatureOnBattlePtr get_target(bool lock = true);

	CreatureOnBattlePtr get_self(bool lock = true);

	bool player_kill_on_screen(bool ignoreZ = false, bool lock = true);

	Json::Value parse_class_to_json(){
		Json::Value creatureInfo;

		creatureInfo["creatures_on_screen"] = this->get_creatures_on_screen(true, true).size();
		creatureInfo["have_player_on_screen"] = this->have_player();
		creatureInfo["have_creature_on_screen"] = this->have_creature_on_screen();
		creatureInfo["is_trapped_by_creatures"] = this->is_traped_by_monster();

		std::shared_ptr<CreatureOnBattle> current_creature = this->get_target();
		if (!current_creature){
			creatureInfo["current_creature_name"] = 0;
			creatureInfo["current_creature_is_monster"] = 0;
			creatureInfo["current_creature_is_npc"] = 0;
			creatureInfo["current_creature_is_player"] = 0;

			creatureInfo["current_creature_coordinate_x"] = 0;
			creatureInfo["current_creature_coordinate_y"] = 0;
			creatureInfo["current_creature_coordinate_z"] = 0;
		}
		else{
			creatureInfo["current_creature_name"] = current_creature->name;
			creatureInfo["current_creature_is_monster"] = current_creature->is_monster();
			creatureInfo["current_creature_is_npc"] = current_creature->is_npc();
			creatureInfo["current_creature_is_player"] = current_creature->is_player();
			
			creatureInfo["current_creature_coordinate_x"] = current_creature->get_coordinate_monster().x;
			creatureInfo["current_creature_coordinate_y"] = current_creature->get_coordinate_monster().y;
			creatureInfo["current_creature_coordinate_z"] = current_creature->get_coordinate_monster().z;
		}

		return creatureInfo;
	}

	static BattleList* get();
};


class TemporaryTarget{
public:
	TimeChronometer timer;
	uint32_t creature_id;
	uint32_t max_time;
	std::function<bool()> function_has_to_continue_attack;
};

class HunterActions{
	std::function<bool()> stop_attack_callback;
	std::vector<TemporaryTarget> temporary_tagets;
	neutral_mutex mtx_access;
	void erase_expired_temporary_targets();

	
public:

	void clear_temporary_targets();

	std::vector<TemporaryTarget> get_temporary_tagets();
	bool has_temporary;
	
	//internal
	bool attack_creature_on_coordinates(uint32_t id);
	bool attack_creature_on_battle(uint32_t id, int32_t timeout = 300);
	bool attack_creature_with_shared_memory(uint32_t id);
	//internal end

	bool attack_creature_by_name(std::string name, std::function<bool()> callback = nullptr);
	bool attack_creature_id(uint32_t id, std::function<bool()> callback = std::function<bool()>(), uint32_t timeout = 0);
	void add_temporary_target(uint32_t creature_id, uint32_t max_time = 0xffffffff, std::function<bool()> have_to_continue_attacking = std::function<bool()>());

	void stop_attack();
	uint32_t get_temporary_target_id();
	void set_follow_creature(uint32_t creature_id, uint32_t max_time = 0xffffffff, std::function<bool()> have_to_continue_attacking = std::function<bool()>()){
		//IMPLEMENT
	}


	static HunterActions* get();
};

struct hunter_state{
	int32_t time_changed_state;
	bool state;
};


enum distance_param_row_t{
	distance_param_creature,
	distance_param_coordinate,
	distance_param_none
};


class DistanceParamRowBase{
	distance_param_row_t type;
	uint32_t danger;
public:
	DistanceParamRowBase(distance_param_row_t t);

	distance_param_row_t get_type();

	void set_type(distance_param_row_t t);

	void set_danger(uint32_t d);

	uint32_t get_danger();
};

class DistanceParamRowCreature : public DistanceParamRowBase{
public:
	DistanceParamRowCreature(std::string creature_n, uint32_t d);
	std::string creature_name;
	uint32_t danger;
};

class DistanceParamRowCoordinate :public DistanceParamRowBase{
public:
	DistanceParamRowCoordinate(Coordinate c, uint32_t d);
	Coordinate coord;
};


enum lure_state_t{
	lure_state_none,
	lure_state_luring,
	lure_state_reached,
	lure_state_not_reachable
};


class HunterCore : public CoreBase{
	uint32_t last_luring_target = 0;
	uint64_t invoked_count = 0;
	static uint32_t last_coordinate_z;
	lure_state_t current_lure_state = lure_state_none;
	neutral_mutex mtx_access;
	bool m_is_pulsed = false;

	bool var_need_operate;

	TimeChronometer change_pulse_timer;
	TimeChronometer change_state_timer;
	TimeChronometer time_to_not_reachable;
	TimeChronometer timer_to_wait;
	TimeChronometer time_to_pause;
	int timeout_to_pause = 0;
	
	void set_need(bool state);
	
	bool has_creatures = false;

	bool check_creature_die(uint32_t creature_id, uint32_t timeout = 500);
public:

	void set_time_to_pause(int timeout){
		time_to_pause.reset();
		timeout_to_pause = timeout;
	}

	void wait_invoke_count();

	void unset_pulse();

	bool is_pulsed();

	void pulse();

	bool wait_kill_all_complete(uint32_t timeout = 10000);

	bool wait_pulse_out(uint32_t timeout = 10000);

	bool wait_pulse(uint32_t timeout = 10000);

	bool get_need(uint32_t delayed_interval = 500);
	static neutral_mutex mtx_runner_lock;
	static bool any_monster;
	static CreatureOnBattlePtr creature_to_attack;
	static TargetNonSharedPtr last_creature_group_info;
	static TargetPtr curret_target_type;

	static void set_pathrunner_params(
		CreatureOnBattlePtr _creature_to_attack,
		TargetNonSharedPtr _last_creature_group_info,
		TargetPtr _curret_target_type,
		bool any_monster);

	HunterCore();

	hunter_state get_state_info();

	void set_lure_state(lure_state_t in);

	lure_state_t get_lure_state();


	static void keep_distance(Coordinate avoid_coord = Coordinate(),
		std::vector<DistanceParamRowBase*>* distance_params = nullptr);

	std::pair<int32_t, int32_t> get_creature_points();
	boost::asio::io_service service;
	boost::asio::io_service service_refresher;
	boost::asio::deadline_timer timer;
	boost::asio::deadline_timer timer_refresher;

	bool to_lure_mage;
	bool info_lure_status;

	bool exist_creature_to_attack();
	bool need_operate();
	
	void run_async();
	bool can_execute();
	bool has_creature_to_attack();
	void path_runner_thread();

	void start_refresher();
	void invoke_refresher();
	void run_refresher();

	void start();
	void invoke();
	void run();
	
	action_retval_t hunter_by_path_points(bool check_looter);
	void change_attack_mode(uint32_t min_points, attack_mode_t new_mode, attack_mode_t old_mode);

	void pathrunner(CreatureOnBattlePtr current_target, std::shared_ptr<Target> current_target_confs, TargetNonSharedPtr group_info, bool any_monster = false);
	void lure_thread();
	static void init();
	static HunterCore* mHunterCore;
	static HunterCore* get();
};



