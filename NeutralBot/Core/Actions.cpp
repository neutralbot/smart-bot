#include "Actions.h"
#include "Inventory.h"
#include "InfoCore.h"
#include "Pathfinder.h"
#include "HunterCore.h"
#include "..\\ConfigPathManager.h"
#include "Time.h"
#include "ContainerManager.h"
#include "Hotkey.h"
#include "SpellcasterCore.h"
#include "Map.h"
#include "..\LooterManager.h"
#include "FloatingMiniMenu.h"
#include "ItemsManager.h"
#include "AdvancedRepoterCore.h"

neutral_mutex BaseActions::mtx_access;

action_retval_t Actions::change_weapon(uint32_t weapon){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	uint32_t main_bp_id = Inventory::get()->get_item_id_by_slot_type(inventory_slot_t::slot_bag);
	if (main_bp_id <= 0 || main_bp_id == UINT32_MAX)
		return action_retval_fail;

	ItemContainerPtr container = ContainerManager::get()->get_container_by_id(main_bp_id);
	if (!container)
		return action_retval_fail;

	if (weapon <= 0 || main_bp_id == UINT32_MAX)
		return action_retval_fail;

	uint32_t item_count = container->get_item_count_by_id(weapon);
	if (item_count <= 0)
		return action_retval_fail;

	if (LooterCore::get()->need_operate())
		return action_retval_fail;

	uint32_t item_equiped = Inventory::get()->body_weapon();
	if (item_equiped == weapon)
		return action_retval_success;

	Inventory::get()->put_item(weapon, inventory_slot_t::slot_weapon, 1);	
	return action_retval_success;
}
action_retval_t Actions::travel(std::string city, std::string npc_name){
	if (npc_name != "")
		goto_nearest_from_creature(npc_name);

	for (int trying_loop = 0; trying_loop <= 3; trying_loop++){
		Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();

		Actions::get()->says("hi", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(5);
		Actions::get()->says(city, "NPCs");		
		TibiaProcess::get_default()->wait_ping_delay(2);
		Actions::get()->says("yes", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);

		if (TibiaProcess::get_default()->character_info->get_self_coordinate().get_axis_max_dist(current_coordinate) > 3)
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::use_item(uint32_t item_id, bool resquest_lock){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	ContainerPosition position = ContainerManager::get()->find_item(item_id);
	if (position.is_null())
		return action_retval_t::action_retval_fail;

	auto container = ContainerManager::get()->get_container_by_index(position.container_index);
	if (!container)
		return action_retval_t::action_retval_fail;

	if (!container->set_slot_row(position.slot_index))
		return action_retval_t::action_retval_fail;

	auto area_to_use = container->get_slot_rect_client(position.slot_index);
	if (area_to_use.is_null())
		return action_retval_t::action_retval_fail;

	auto center = area_to_use.get_center();
	if (center.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();

	if (!resquest_lock){
		TimeChronometer time;
		while (time.elapsed_milliseconds() < input->get_time_out()){
			if (input->can_use_input())
				break;
			else
				Sleep(1);
		}

		if (!input->can_use_input())
			return action_retval_t::action_retval_fail;

		input->click_right(center);
		return action_retval_t::action_retval_success;
	}

	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->click_right(center);
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::move_item_container_to_coordinate(ContainerPosition container_location,
	Coordinate coord, uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	if (!MapTiles::is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	if (!gMapTilesPtr->is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	auto container = ContainerManager::get()->get_container_by_index(container_location.container_index);
	if (!container)
		return action_retval_t::action_retval_fail;

	if (container->get_max_slots() <= container_location.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container->set_slot_row(container_location.slot_index))
		return action_retval_t::action_retval_fail;

	Sleep(30);
	container = ContainerManager::get()->get_container_by_index(container_location.container_index);
	if (!container)
		return action_retval_t::action_retval_fail;

	Point slot_point = container->get_slot_rect_client(container_location.slot_index);
	if (slot_point.is_null())
		return action_retval_t::action_retval_fail;

	auto point1 = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord.to_point());
	if (point1.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->drag_item_on_screen(slot_point, point1, true, check_item_id, count);
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::move_item_container_to_container(ContainerPosition container_location_source,
	ContainerPosition container_location_dest, uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	//TODO FALTA FAZER SE FOR DA PROPRIA BP

	auto container_source = ContainerManager::get()->get_container_by_index(container_location_source.container_index);
	if (!container_source)
		return action_retval_t::action_retval_fail;

	container_source->maximize();

	if (container_source->get_max_slots() <= container_location_source.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container_source->set_slot_row(container_location_source.slot_index))
		return action_retval_t::action_retval_fail;
	//check container dest
	auto container_dest = ContainerManager::get()->get_container_by_index(container_location_dest.container_index);
	if (!container_dest)
		return action_retval_t::action_retval_fail;

	container_dest->maximize();

	if (container_dest->get_max_slots() <= container_location_dest.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container_dest->set_slot_row(container_location_dest.slot_index))
		return action_retval_t::action_retval_fail;

	Rect point1 = container_source->get_slot_rect_client(container_location_source.slot_index);
	Rect point2 = container_dest->get_slot_rect_client(container_location_dest.slot_index);
	if (point1.is_null())
		return action_retval_t::action_retval_fail;

	if (point2.is_null())
		return action_retval_t::action_retval_fail;

	auto slot = container_source->get_slot_at(container_location_source.slot_index);
	//if (slot.stack_count < count)
	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	auto temp_container_dest = ContainerManager::get()->get_container_by_index(container_location_dest.container_index);
	
	return input->drag_item_on_screen(point1.get_center(), point2.get_center(), false, check_item_id, count)
		? action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::move_item_container_to_slot(ContainerPosition container_location_source, inventory_slot_t slot,
	uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	Inventory::get()->maximize();

	auto container_source = ContainerManager::get()->get_container_by_index(container_location_source.container_index);
	if (!container_source)
		return action_retval_t::action_retval_fail;

	container_source->maximize();

	if (container_source->get_max_slots() <= container_location_source.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container_source->set_slot_row(container_location_source.slot_index))
		return action_retval_t::action_retval_fail;

	container_source = ContainerManager::get()->get_container_by_index(container_location_source.container_index);
	if (!container_source)
		return action_retval_t::action_retval_fail;
	
	Point point_from = container_source->get_slot_rect_client(container_location_source.slot_index).get_center();	
	Point point_to = Inventory::get()->get_slot_type_interface_coordinate(slot).to_point();

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	return input->drag_item_on_screen(point_from, point_to, true, check_item_id, count)
		? action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::move_item_coordinate_to_container(Coordinate coord, ContainerPosition container_location_dest,
	uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!MapTiles::is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	if (!gMapTilesPtr->is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	auto container = ContainerManager::get()->get_container_by_index(container_location_dest.container_index);
	if (!container)
		return action_retval_t::action_retval_fail;
	
	container->maximize();

	if (container->get_max_slots() <= container_location_dest.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container->set_slot_row(container_location_dest.slot_index))
		return action_retval_t::action_retval_fail;

	Sleep(30);
	container = ContainerManager::get()->get_container_by_index(container_location_dest.container_index);
	if (!container)
		return action_retval_t::action_retval_fail;

	Point slot_point = container->get_slot_rect_client(container_location_dest.slot_index);
	if (slot_point.is_null())
		return action_retval_t::action_retval_fail;

	auto point1 = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord.to_point());
	if (point1.is_null())
		return action_retval_t::action_retval_fail;
	
	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->drag_item_on_screen(point1, slot_point, true, check_item_id, count);
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::move_item_coordinate_to_slot(Coordinate coord, inventory_slot_t slot,
	uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!MapTiles::is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	auto point1 = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord.to_point());
	if (point1.is_null())
		return action_retval_t::action_retval_fail;


	/*if (!Inventory::get()->is_maximized()){
		if (!Inventory::get()->maximize())
			return action_retval_t::action_retval_fail;
	}*/
	Point point_to = Inventory::get()->get_slot_type_interface_coordinate(slot).to_point();
	if (point_to.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	return input->drag_item_on_screen(point1, point_to, true, check_item_id, count)
		? action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::move_item_coordinate_to_coordinate(Coordinate coord, Coordinate coord_target, uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!MapTiles::is_cordinate_onscreen(coord) || !MapTiles::is_cordinate_onscreen(coord_target))
		return action_retval_t::action_retval_fail;

	auto point1 = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord.to_point());
	if (point1.is_null())
		return action_retval_t::action_retval_fail;

	auto point2 = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord_target.to_point());
	if (point2.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	return input->drag_item_on_screen(point1, point2, true, check_item_id, count)
		? action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::move_item_slot_to_slot(inventory_slot_t slot, inventory_slot_t slot_dest, uint32_t count, uint32_t check_item_id){
	MessageBox(0, &(std::string("Method needs to be implemented ") + __FUNCTION__)[0], 0, MB_OK);
	return action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::move_item_slot_to_coordinate(inventory_slot_t slot, Coordinate coord, uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	if (!MapTiles::is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	auto point_to = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord.to_point());
	if (point_to.is_null())
		return action_retval_t::action_retval_fail;


	/*if (!Inventory::get()->is_maximized()){
		if (!Inventory::get()->maximize())
			return action_retval_t::action_retval_fail;
	}*/

	Point point1 = Inventory::get()->get_slot_type_interface_coordinate(slot).to_point();
	if (point_to.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	return input->drag_item_on_screen(point1, point_to, true, check_item_id, count)
		? action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::move_item_slot_to_container(inventory_slot_t slot, ContainerPosition container_location_dest, uint32_t count, uint32_t check_item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto container_source = ContainerManager::get()->get_container_by_index(container_location_dest.container_index);
	if (!container_source)
		return action_retval_t::action_retval_fail;

	container_source->maximize();

	if (container_source->get_max_slots() <= container_location_dest.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container_source->set_slot_row(container_location_dest.slot_index))
		return action_retval_t::action_retval_fail;

	Point point_to = container_source->get_slot_rect_client(container_location_dest.slot_index);
	Point point_from = Inventory::get()->get_slot_type_interface_coordinate(slot).to_point();

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	return input->drag_item_on_screen(point_from, point_to, true, check_item_id, count) ?
		action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::use_item_on_inventory(inventory_slot_t slot, uint32_t item_id/*ignore item id*/){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!Inventory::get()->is_maximized()){
		if (!Inventory::get()->maximize())
			return action_retval_t::action_retval_fail;
	}

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	Point item_location = Inventory::get()->get_slot_type_interface_coordinate(slot).to_point();
	input->click_right(item_location);
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::use_item_on_container(ContainerPosition container_location_dest, uint32_t item_id/*ignore item id*/){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	auto container_source = ContainerManager::get()->get_container_by_index(container_location_dest.container_index);
	if (!container_source)
		return action_retval_t::action_retval_fail;

	if (container_source->get_max_slots() <= container_location_dest.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container_source->set_slot_row(container_location_dest.slot_index))
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_trying;

	Rect point_to = container_source->get_slot_rect_client(container_location_dest.slot_index);
	if (point_to.is_null())
		return action_retval_t::action_retval_trying;

	input->click_right(point_to.get_center());
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::use_item_on_coordinate(Coordinate coord, uint32_t item_id/*ignore item id*/){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!gMapTilesPtr->is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	if (!gMapTilesPtr->get_tile_at(coord))
		return action_retval_t::action_retval_fail;

	if (item_id != UINT32_MAX){
		auto tile_item_top = gMapTilesPtr->get_tile_at(coord);
		if (!tile_item_top)
			return action_retval_t::action_retval_fail;

		int get_top_item_id = tile_item_top->get_top_item_id();
		if (item_id != UINT32_MAX && get_top_item_id != item_id)
			return action_retval_t::action_retval_fail;
	}

	auto point1 = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord.to_point());
	if (point1.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->click_right(point1);
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::use_crosshair_item_on_inventory(inventory_slot_t slot, uint32_t item_id/*ignore item id*/){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	if (!Inventory::get()->is_maximized()){
		if (!Inventory::get()->maximize())
			return action_retval_t::action_retval_fail;
	}

	if (!HotkeyManager::get()->useItemOnCrosshair(item_id))
	if (!Actions::get()->use_item(item_id, false))
		return action_retval_t::action_retval_fail;


	Point item_location = Inventory::get()->get_slot_type_interface_coordinate(slot).to_point();
	if (item_location.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->click_left(item_location);
	return action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::use_crosshair_item_on_coordinate(Coordinate coord, uint32_t item_id/*ignore item id*/){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!gMapTilesPtr->is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	if (!HotkeyManager::get()->useItemOnCrosshair(item_id))
		if (!Actions::get()->use_item(item_id, false))
			return action_retval_t::action_retval_fail;

	auto point1 = ClientGeneralInterface::get()->get_coordinate_as_pixel(coord.to_point());
	if (point1.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->click_left(point1);
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::use_crosshair_item_on_container(ContainerPosition container_location, uint32_t item_id/*ignore item id*/){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	auto container = ContainerManager::get()->get_container_by_index(container_location.container_index);
	if (!container)
		return action_retval_t::action_retval_fail;

	if (!HotkeyManager::get()->useItemOnCrosshair(item_id))
	if (!Actions::get()->use_item(item_id, false))
		return action_retval_t::action_retval_fail;

	auto slot_location = container->get_slot_rect_client(container_location.slot_index);
	if (!slot_location.is_null())
		return action_retval_t::action_retval_fail;

	auto center = slot_location.get_center();
	if (center.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;
	input->click_left(center);
	return action_retval_t::action_retval_fail;
}

action_retval_t BaseActions::use_item_on_to_coodinate(uint32_t item_id, Coordinate coord){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	if (!gMapTilesPtr->is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;
	return action_retval_t::action_retval_success;

	if (!HotkeyManager::get()->useItemOnCrosshair(item_id))
		if (!Actions::get()->use_item(item_id, false))
			return action_retval_t::action_retval_fail;

}

action_retval_t BaseActions::use_item_on_to_invetory(uint32_t item_id, inventory_slot_t slot_type){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!Inventory::get()->is_maximized()){
		if (!Inventory::get()->maximize())
			return action_retval_t::action_retval_fail;
	}

	if (!HotkeyManager::get()->useItemOnCrosshair(item_id))
	if (!Actions::get()->use_item(item_id, false))
			return action_retval_t::action_retval_fail;


	Point item_location = Inventory::get()->get_slot_type_interface_coordinate(slot_type).to_point();
	if (item_location.is_null())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->click_right(item_location);
	return action_retval_t::action_retval_success;
}

action_retval_t BaseActions::use_item_on_to_container(ContainerPosition container_location, uint32_t item_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto container_dest = ContainerManager::get()->get_container_by_index(container_location.container_index);
	if (!container_dest)
		return action_retval_t::action_retval_fail;

	container_dest->maximize();

	if (container_dest->get_max_slots() <= container_location.slot_index)
		return action_retval_t::action_retval_fail;

	if (!container_dest->set_slot_row(container_location.slot_index))
		return action_retval_t::action_retval_fail;

	Rect point1 = container_dest->get_slot_rect_client(container_location.slot_index);
	if (point1.is_null())
		return action_retval_t::action_retval_fail;
	
	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->click_right(point1.get_center());
	return action_retval_t::action_retval_success;
}

action_retval_t Actions::goto_coord(Coordinate coord, uint32_t consider_near, map_type_t map_type, pathfinder_state& state_out, bool consider_target_walkable, std::function<bool()> Stopfunction){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	pathfinder_state state = (*(Pathfinder::get())).set_to_go_while(coord, consider_near, map_type, pathfinder_walk_going, Stopfunction, consider_target_walkable);
	
	state_out = state;
	
	return state == pathfinder_state::pathfinder_walk_reached ? action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

std::shared_ptr<Actions> Actions::get(){
	static std::shared_ptr<Actions> m = nullptr;
	if (!m)
		m = std::shared_ptr<Actions>(new Actions);
	return m;
}

action_retval_t Actions::goto_coord_on_screen(Coordinate coord, uint32_t consider_near, bool map_front, pathfinder_state& state_out, bool consider_target_walkable){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (map_front)
		return goto_coord(coord, consider_near, map_front_mini_t, state_out, consider_target_walkable);
	else
		return goto_coord(coord, consider_near, map_back_mini_t, state_out, consider_target_walkable);
}

action_retval_t Actions::simple_go_coord_on_screen(Coordinate coord, uint32_t consider_near, bool map_front, pathfinder_state& state_out, bool consider_target_walkable){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	return goto_coord_on_screen(coord, consider_near, map_front, state_out, consider_target_walkable);
}

CoordinatePtr Actions::get_coord_near_other(Coordinate other, bool check_reachable, map_type_t map_type, uint32_t consider_near){
	if (!TibiaProcess::get_default()->get_is_logged())
		return CoordinatePtr();

	TilePtr tile = gMapTilesPtr->get_tile_nearest_coordinate(other, true, map_type, consider_near);
	if (!tile)
		return CoordinatePtr();

	return tile->coordinate.get_as_pointer();
}

action_retval_t Actions::goto_near_coord(Coordinate coord, bool go_under_sqm, uint32_t consider_near, map_type_t map_type, uint32_t wait_ms,
	bool monst_near_from_center, pathfinder_state& state_out, bool consider_target_walkable, std::function<bool()> Stopfunction){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	do {
		Sleep(5);
		Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

		if (coord.get_axis_max_dist(self_coord) <= consider_near)
			return action_retval_t::action_retval_success;

		coord = Pathfinder::get()->get_nearest_coordinate_from_another(coord, map_type, true, true, consider_near, false);

		if (coord.is_null())
			return action_retval_t::action_retval_fail;

		if (goto_coord(coord, 0, map_type, state_out, consider_target_walkable, Stopfunction)) {
			Sleep(wait_ms);
			if (coord.get_axis_max_dist(self_coord) <= consider_near)
				return action_retval_t::action_retval_success;
		}
		else 
			return action_retval_t::action_retval_fail;		
	} while (true);
}

action_retval_t Actions::clean_coordinate(Coordinate coord, int items_count, bool use_browse_field) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!use_browse_field)
		return move_creature(coord, false);
	
	ContainerManager::get()->close_all_browse_fields();

	if (!FloatingMiniMenu::open_browse_field_at_coordinate(coord))
		return action_retval_t::action_retval_fail;

	TibiaProcess::get_default()->wait_ping_delay(3);

	std::shared_ptr<ItemContainer> browse_container = ContainerManager::get()->get_browse_field();
	if (!browse_container)
		return action_retval_t::action_retval_fail;

	int slots_used_count = browse_container->get_used_slots();
	int moved_items_count = 0;
	for (int slot = (slots_used_count - 1); slot > -1; slot--) {
		browse_container = ContainerManager::get()->get_browse_field();
		if (!browse_container)
			return action_retval_t::action_retval_fail;

		if (browse_container->get_used_slots() <= 0)
			break;

		auto tile = gMapTilesPtr->get_tile_nearest_coordinate(coord, true, map_front_mini_t, 1);
		if (!tile)
			return action_retval_t::action_retval_fail;

		ContainerPosition slotPosition(browse_container->get_container_index(), slot);
		Coordinate coordinate_to_move_item = tile->coordinate;
		int current_item_id = browse_container->get_slot_at(slot).id;

		Actions::move_item_container_to_coordinate(slotPosition, coordinate_to_move_item, 100);
		moved_items_count++;
		TibiaProcess::get_default()->wait_ping_delay(3);

		if (items_count > 0 && items_count >= moved_items_count)
			break;
	}

	return ContainerManager::get()->close_all_browse_fields() ? action_retval_t::action_retval_success : action_retval_t::action_retval_fail;
}

action_retval_t Actions::open_thing_with_item_and_step(Coordinate coord, int item_id, int startId, int endId) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TilePtr tile = gMapTilesPtr->get_tile_at(coord);

	if (!tile)
		return action_retval_t::action_retval_fail;

	if (startId > 0 && tile->get_top_item_id() != startId)
		return action_retval_t::action_retval_fail;
	
	TibiaProcess::get_default()->wait_ping_delay(1.0);

	use_crosshair_item_on_coordinate(coord, item_id);

	TibiaProcess::get_default()->wait_ping_delay(1.0);

	tile = gMapTilesPtr->get_tile_at(coord);

	if (endId > 0 && tile->get_top_item_id() != endId)
		return action_retval_t::action_retval_fail;

	Pathfinder::get()->go_with_hotkey(coord,true,true);

	TibiaProcess::get_default()->wait_ping_delay(2.0);

	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();	
	if (self_coord.z == coord.z)
		return action_retval_t::action_retval_fail;
	
	return action_retval_t::action_retval_success;
}

action_retval_t Actions::use_rope_on_coordinate(Coordinate coord) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	reopen_containers();

	auto ropeList = ItemsManager::get()->get_items_as_rope();
	int item_id = ContainerManager::get()->find_item_id_from_vector(ropeList);
	if (!item_id)
		return action_retval_t::action_retval_fail;

	for (int count = 0; count < Max_Count; count++) {
		Actions::use_crosshair_item_on_coordinate(coord, item_id);

		TibiaProcess::get_default()->wait_ping_delay(2.5);

		Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		if (self_coord.z < coord.z)
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::open_hole_with_shovel(Coordinate coord, int start_item_id, int end_item_id, std::function<bool()> Stopfunction) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (TibiaProcess::get_default()->character_info->get_self_coordinate().z != coord.z)
		return action_retval_t::action_retval_success;
	
	auto item_id_list = ItemsManager::get()->get_items_as_shovel();
	int item_id = ContainerManager::get()->find_item_id_from_vector(item_id_list);
	if (!item_id)
		return action_retval_t::action_retval_fail;
	
	for (int count = 0; count < Max_Count; count++) {
		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;

		Coordinate coordTemp = Pathfinder::get()->get_nearest_coordinate_from_another(coord, map_front_mini_t, true, true, 1, false, true);

		goto_coord_on_screen(coordTemp, 0);

		TibiaProcess::get_default()->character_info->wait_stop_waking();

		clean_coordinate(coordTemp);

		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;

		if (open_thing_with_item_and_step(coord, item_id, start_item_id, end_item_id))
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::open_hole_with_pick(Coordinate coord, int start_item_id, int end_item_id, std::function<bool()> Stopfunction) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (TibiaProcess::get_default()->character_info->get_self_coordinate().z != coord.z)
		return action_retval_t::action_retval_success;

	auto item_id_list = ItemsManager::get()->get_items_as_pick();
	int item_id = ContainerManager::get()->find_item_id_from_vector(item_id_list);
	if (!item_id)
		return action_retval_t::action_retval_fail;
	
	for (int count = 0; count < Max_Count; count++) {
		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;

		Coordinate coordTemp = Pathfinder::get()->get_nearest_coordinate_from_another(coord, map_front_mini_t, true, true, 1, false, true);

		goto_coord_on_screen(coordTemp, 0);

		TibiaProcess::get_default()->character_info->wait_stop_waking();

		clean_coordinate(coord);

		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;

		if (open_thing_with_item_and_step(coord, item_id, start_item_id, end_item_id))
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::hole_or_step(Coordinate coord, side_t::side_t side){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_fail;

	if (TibiaProcess::get_default()->character_info->get_self_coordinate().z != coord.z)
		return action_retval_t::action_retval_success;

	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	std::function<bool()> clear_coord = [&]() -> bool{
		//already on door location
		if (TibiaProcess::get_default()->character_info->get_self_coordinate() == coord)
			return true;

		if (gMapMinimapPtr->get_walkability(coord, map_type_t::map_front_mini_t))
		if (Pathfinder::get()->go_with_hotkey(coord))
			return true;

		TilePtr door_tile = gMapTilesPtr->get_tile_at(coord);
		if (!door_tile)
			return false;

		//PLAYER
		if (door_tile->contains_player())
		if (move_creature(coord))
			return true;

		//MONSTER
		if (door_tile->contains_creature()){
			auto creatures = door_tile->get_creatures();
			if (!creatures.size())
				return true;

			auto creature_id = creatures.begin();
			std::function<bool()> f = std::bind([](Coordinate coord, uint32_t creature_id){
				Coordinate door_c = coord;
				auto creature = BattleList::get()->find_creature_by_id(creature_id);
				if (!creature)
					return false;
				if (creature->get_coordinate_monster() != door_c)
					return false;
				return true;
			}, coord, creature_id->second->id);

			HunterActions::get()->attack_creature_id(creature_id->second->id, f);

			return true;
		}
		return false;
	};

	int total_try_count = 0;
	while (total_try_count < Actions::Max_Count) {
		Sleep(1);

		Actions::goto_coord_on_screen(coord, 0, true, pathfinder_state_placeholder, true);

		if (TibiaProcess::get_default()->character_info->get_self_coordinate() != coord)
			clear_coord();

		Actions::step_side(side);

		TibiaProcess::get_default()->wait_ping_delay(2.0);

		Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		
		if (current_coord.z != coord.z){
			MapTiles::wait_refresh();
			return action_retval_t::action_retval_success;
		}

		total_try_count++;
	}

	return action_retval_fail;
}

action_retval_t Actions::open_thing_with_machet(Coordinate coord, std::function<bool()> Stopfunction) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto item_id_list = ItemsManager::get()->get_items_as_machete();
	int item_id = ContainerManager::get()->find_item_id_from_vector(item_id_list);
	if (!item_id)
		return action_retval_t::action_retval_fail;

	clean_coordinate(coord);
	for (int count = 0; count < Max_Count; count++) {
		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;

		clean_coordinate(coord);

		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;

		if (open_thing_with_item_and_step(coord, item_id))
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::open_hole_with_scythe(Coordinate coord, std::function<bool()> Stopfunction) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto item_id_list = ItemsManager::get()->get_items_as_scythe();
	int item_id = ContainerManager::get()->find_item_id_from_vector(item_id_list);
	if (!item_id)
		return action_retval_t::action_retval_fail;

	for (int count = 0; count < Max_Count; count++) {
		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;
		
		Coordinate coordTemp = Pathfinder::get()->get_nearest_coordinate_from_another(coord, map_front_mini_t, true, true, 1, false, true);

		goto_coord_on_screen(coordTemp, 0);

		TibiaProcess::get_default()->character_info->wait_stop_waking();
		clean_coordinate(coord);

		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;

		if (open_thing_with_item_and_step(coord, item_id))
			return action_retval_t::action_retval_success;
	}

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::open_hole_with_tool_id(Coordinate coord, uint32_t tool_id, std::function<bool()> Stopfunction){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	ContainerPosition item_on_backpack = ContainerManager::get()->find_item(tool_id);
	if (item_on_backpack.is_null())
		return action_retval_t::action_retval_fail;

	for (int count = 0; count < Max_Count; count++) {
		if (Stopfunction() && Stopfunction)
			return action_retval_t::action_retval_fail;
		
		Coordinate coordTemp = Pathfinder::get()->get_nearest_coordinate_from_another(coord, map_front_mini_t, true, true, 1, false, true);

		goto_coord_on_screen(coordTemp, 0);
		TibiaProcess::get_default()->character_info->wait_stop_waking();
		clean_coordinate(coord);

		if (Stopfunction()&& Stopfunction)
			return action_retval_t::action_retval_fail;

		if (open_thing_with_item_and_step(coord, tool_id))
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::goto_nearest_from_creature(std::string name){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (name == "")
		return action_retval_t::action_retval_fail;

	CreatureOnBattlePtr creature = BattleList::get()->find_creature_on_screen(name);
	if (!creature)
		return action_retval_t::action_retval_fail;

	Coordinate creature_coord = { (int32_t)creature->pos_x, (int32_t)creature->pos_y, (int8_t)creature->pos_z };

	for (int range = 0; range <= 4; range++){
		Coordinate coordo_to_go = Pathfinder::get()->get_nearest_coordinate_from_another(creature_coord, map_front_t, true, true, range);
	
		if (TibiaProcess::get_default()->character_info->get_self_coordinate() != coordo_to_go)
			return action_retval_t::action_retval_success;

		if (coordo_to_go.z == 0 || coordo_to_go.z != creature_coord.getZ())
			continue;

		goto_coord_on_screen(coordo_to_go, 0);

		TibiaProcess::get_default()->wait_ping_delay(2.0);

		TibiaProcess::get_default()->character_info->wait_stop_waking();

		if (TibiaProcess::get_default()->character_info->get_self_coordinate() != coordo_to_go)
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::up_or_down_on_thing_by_coord(Coordinate coord) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	FloatingMiniMenu::use(coord);

	TibiaProcess::get_default()->wait_ping_delay(1.5);

	MapTiles::wait_refresh();

	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	if (self_coord.z == coord.z){
		Actions::use_item_on_coordinate(coord);

		TibiaProcess::get_default()->wait_ping_delay(0.5);

		MapTiles::wait_refresh();

		if (self_coord.z == coord.z)
			return action_retval_t::action_retval_trying;
	}

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::up_ladder(Coordinate coord) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	
	TimeChronometer chronometer;
	int timeout = 2000;
	while (chronometer.elapsed_milliseconds() < timeout){
		action_retval_t up_or_down = up_or_down_on_thing_by_coord(coord);

		MapTiles::wait_refresh();

		if (up_or_down == action_retval_t::action_retval_success)
			return action_retval_t::action_retval_success;
	}

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::use_lever(Coordinate coord, Coordinate offset, int start_item_id, int end_item_id) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	for (int count = 0; count < 3; count++) {
		action_retval_t go_into = goto_coord_on_screen(coord, 0);

		if (go_into == action_retval_t::action_retval_fail)
			return action_retval_t::action_retval_fail;
		else if (go_into != action_retval_t::action_retval_success)
			continue;
		
		TilePtr tile = gMapTilesPtr->get_tile_at(coord + offset);
		if (!tile)
			return action_retval_t::action_retval_fail;

		Actions::clean_coordinate(coord + offset);

		TibiaProcess::get_default()->wait_ping_delay(1.0);

		int started_item_id = tile->get_top_item_id();
		if (start_item_id > 0 && started_item_id != start_item_id)
			continue;
		
		Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

		if (Actions::use_item_on_coordinate(coord + offset, started_item_id) != action_retval_t::action_retval_success)
			continue;

		TibiaProcess::get_default()->wait_ping_delay(2.5);

		TilePtr local_tile = gMapTilesPtr->get_tile_at(coord + offset);
		if (!local_tile)
			continue;

		Coordinate new_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		if (current_coord.get_axis_max_dist(new_coord) > 2 || current_coord.z != new_coord.z)
			return action_retval_t::action_retval_success;

		int current_item_id = local_tile->get_top_item_id();
		if (end_item_id > 0 && current_item_id == end_item_id)
			return action_retval_t::action_retval_success;
		else if (end_item_id <= 0 && current_item_id != started_item_id)
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::use_item_id_to_down_or_up(Coordinate coord, int item_id) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TilePtr tile = gMapTilesPtr->get_tile_at(coord);
	if (!tile)
		return action_retval_t::action_retval_fail;

	clean_coordinate(coord);

	for (int count = 0; count < Actions::Max_Count; count++) {
		int item_id_on_tile = tile->get_top_item_id();
		if (item_id > 0 && item_id_on_tile != item_id)
			break;

		action_retval_t up_or_down = up_or_down_on_thing_by_coord(coord);
		if (up_or_down == action_retval_t::action_retval_success)
			return action_retval_t::action_retval_success;
	}

	return action_retval_t::action_retval_trying;
}

action_retval_t Actions::join_in_telepot(Coordinate coord, int position_distance) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();

	for (int count = 0; count < Max_Count; count++) {
		goto_coord_on_screen(coord, 0, true, pathfinder_state_placeholder, true);
		TibiaProcess::get_default()->wait_ping_delay(2.5);

		int total_distance = TibiaProcess::get_default()->character_info->get_self_coordinate().get_axis_max_dist(coord);
		if (total_distance >= position_distance || !Pathfinder::get()->is_reachable(coord)) {
			return action_retval_t::action_retval_success;
		}

		goto_coord_on_screen(self_coordinate, 0, true, pathfinder_state_placeholder, true);
		TibiaProcess::get_default()->wait_ping_delay(2.5);
	}

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::clear_coord_to_loot(Coordinate coord){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto tile = gMapTilesPtr->get_tile_at(coord);
	if (!tile)
		return action_retval_t::action_retval_success;

	uint32_t top_id = tile->get_top_item_id();
	if (!top_id)
		return action_retval_t::action_retval_success;

	std::string item_name = ItemsManager::get()->getItemNameFromId(top_id);
	if (item_name == "not_found")
		return action_retval_t::action_retval_success;

	clean_coordinate(coord);
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::move_creature(Coordinate location, bool verify_creature){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	Coordinate final_ = Pathfinder::get()->get_nearest_coordinate_from_another(location);
	if (final_.is_null())
		return action_retval_t::action_retval_fail;

	for (int i = 0; i < 4; i++){
		move_item_coordinate_to_coordinate(location, final_, 100);

		TibiaProcess::get_default()->wait_ping_delay(2.0);

		TilePtr tile_player = gMapTilesPtr->get_tile_at(location);
		if (verify_creature){
			if (!tile_player->contains_creature())
				return action_retval_t::action_retval_success;
		}
		else
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::open_door(Coordinate door_location, uint32_t key_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	//already on door location

	if (gMapMinimapPtr->get_walkability(door_location, map_type_t::map_front_mini_t))
		if (Actions::goto_coord_on_screen(door_location))
			return action_retval_t::action_retval_success;

	if (TibiaProcess::get_default()->character_info->get_self_coordinate() == door_location)
		return action_retval_t::action_retval_success;

	if (key_id)
	if (ContainerManager::get()->get_item_count(key_id) <= 0)
		return action_retval_t::action_retval_fail;

	TilePtr door_tile = gMapTilesPtr->get_tile_at(door_location);
	if (!door_tile)
		return action_retval_t::action_retval_success;

	//SUMMON
	if (door_tile->contains_summon())
		if (move_creature(door_location))
			return action_retval_t::action_retval_trying;

	//PLAYER
	if (door_tile->contains_player())
		if (move_creature(door_location))
			return action_retval_t::action_retval_trying;

	//MONSTER
	if (door_tile->contains_creature()){
		auto creatures = door_tile->get_creatures();
		if (!creatures.size())
			return action_retval_t::action_retval_fail;

		auto creature_id = creatures.begin();

		std::function<bool()> f = std::bind([](Coordinate coord, uint32_t creature_id){
			Coordinate door_c = coord;
			auto creature = BattleList::get()->find_creature_by_id(creature_id);
			if (!creature)
				return false;
			if (creature->get_coordinate_monster() != door_c)
				return false;
			return true;
		}, door_location, creature_id->second->id);

		HunterActions::get()->attack_creature_id(creature_id->second->id, f);

		return action_retval_t::action_retval_fail;
	}

	if (TibiaProcess::get_default()->character_info->get_self_coordinate() == door_location)
		return action_retval_t::action_retval_success;

	for (int count = 0; count < Max_Count; count++){
		if (key_id)
			use_crosshair_item_on_coordinate(door_location, key_id);
		else
			use_item_on_coordinate(door_location);

		TibiaProcess::get_default()->wait_ping_delay(2.5);

		if (gMapMinimapPtr->get_walkability(door_location, map_type_t::map_front_mini_t))
			return action_retval_t::action_retval_success;
	}

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::dropitem(uint32_t item_id, Coordinate coord, uint32_t count, bool fast_drop){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!MapTiles::is_cordinate_onscreen(coord))
		return action_retval_t::action_retval_fail;

	if (coord.is_null())
		coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	ContainerPosition item_location = ContainerManager::get()->find_item(item_id);
	if (item_location.is_null())
		return action_retval_t::action_retval_fail;

	move_item_container_to_coordinate(item_location, coord, count);

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::destory_field(Coordinate coord){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto coord_tile = gMapTilesPtr->get_tile_at(coord);
	if (!coord_tile)
		return action_retval_t::action_retval_fail;

	if (!coord_tile->have_element_field())
		return action_retval_t::action_retval_fail;

	uint32_t item_id = ItemsManager::get()->getitem_idFromName("destroy filed rune");
	return use_item_on_coordinate(coord, item_id);
}

action_retval_t Actions::pick_up_item(uint32_t item_id, uint32_t count, uint32_t destination_container_id){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto container = ContainerManager::get()->get_container_by_id(destination_container_id);
	if (!container)
		return action_retval_t::action_retval_fail;

	if (container->is_full())
		return action_retval_t::action_retval_fail;

	auto coordinate = gMapTilesPtr->find_coordinate_with_top_id(item_id,map_front_mini_t,true);
	if (coordinate.is_null())
		return action_retval_t::action_retval_fail;

	if (!simple_go_coord_on_screen(coordinate,1))
		return action_retval_t::action_retval_fail;

	auto postion = container->get_container_position_to_add_item();
	if (!postion)
		return action_retval_t::action_retval_fail;

	move_item_coordinate_to_container(coordinate, *postion,	count, item_id);

	TibiaProcess::get_default()->wait_ping_delay(1.0);
	return action_retval_t::action_retval_success;
}

action_retval_t Actions::fish_process(){//TODO adicionar ids de canivete
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	static std::vector<int> fish_ids = { 4597, 4598, 4599, 4600, 4601, 4602 };
	
	//get worms count
	if (ContainerManager::get()->get_item_count(3492) <= 0)
		return action_retval_t::action_retval_fail;

	//find fishing rod
	ContainerPosition position = ContainerManager::get()->find_item(3483);
	if (position.is_null())
		return action_retval_t::action_retval_fail;

	for (auto fish_id : fish_ids){
		std::vector<TilePtr> tiles = gMapTilesPtr->get_tiles_with_id(fish_id);
		if (!tiles.size())
			continue;

		for (auto tile : tiles){
			if (!gMapTilesPtr->is_cordinate_onscreen(tile->coordinate))
				continue;

			if (use_crosshair_item_on_coordinate(tile->coordinate, 3483))
				return action_retval_t::action_retval_success;

			return action_retval_t::action_retval_trying;
		}
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::use_fishing_rod(Coordinate coord){//TODO adicionar ids de canivete
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	ContainerPosition position = ContainerManager::get()->find_item(3483);
	if (position.is_null())
		return action_retval_t::action_retval_fail;

	Coordinate our_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (gMapTilesPtr->check_sight_line(our_coord, coord)){
		if (use_crosshair_item_on_coordinate(coord, 3483))
			return action_retval_t::action_retval_success;
		else
			return action_retval_t::action_retval_trying;
		return action_retval_t::action_retval_fail;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::execute_dance(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	
	auto creature = BattleList::get()->find_creature_by_id(TibiaProcess::get_default()->character_info->character_id());
	if (!creature)
		return action_retval_t::action_retval_fail;
	side_nom_axis_t::side_nom_axis_t side = (side_nom_axis_t::side_nom_axis_t)creature->direction;
	side_nom_axis_t::side_nom_axis_t to_turn;

	do{
		Sleep(1);
		to_turn = (side_nom_axis_t::side_nom_axis_t)(rand() % (4 - 0) + 0);
	} while (to_turn == side);
	if (turn(to_turn, 100))
		return action_retval_t::action_retval_success;

	return action_retval_t::action_retval_trying;
}

action_retval_t Actions::eat_food(uint32_t container_index/*any*/,std::string nameFood){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	uint32_t food_id_temp = ItemsManager::get()->getitem_idFromName(nameFood);
	if (food_id_temp != UINT32_MAX && food_id_temp > 0){
		if (use_item(food_id_temp, true))
			return action_retval_t::action_retval_success;

		return action_retval_t::action_retval_fail;
	}
	
	auto food_ids = ItemsManager::get()->get_items_as_food();
	for (auto food_id : food_ids){
		if (use_item(food_id, true))
			return action_retval_t::action_retval_success;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::reopen_containers(bool closeAll, std::function<bool()> Stopfunction){
	//TODO NANDO - quando tiver a nova fun��o de open new window alterar o codigo...
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto BackpacksLooter = LooterManager::get()->containers;
	if (Inventory::get()->body_bag() <= 0)
		return action_retval_t::action_retval_fail;

	if (closeAll){
		if (Stopfunction && Stopfunction())
			return action_retval_t::action_retval_fail;

		ContainerManager::get()->close_all_containers();
	}

	for (auto backpacks : BackpacksLooter){
		uint32_t container_id = backpacks.second->get_container_id();
		if (container_id == 0)
			continue;

		if (ContainerManager::get()->get_container_by_id(container_id))
			continue;

		if (Stopfunction && Stopfunction())
			return action_retval_t::action_retval_fail;
		
		open_container_while_full(container_id);

		ContainerManager::get()->resize_all_containers();
	}
	
	if (!ContainerManager::get()->get_container_by_id(Inventory::get()->body_bag())){
		if (Stopfunction && Stopfunction())
			return action_retval_t::action_retval_fail;

		Actions::get()->open_main_bp();
		ContainerManager::get()->resize_all_containers();
	}
	
	Inventory::get()->minimize();
	return action_retval_t::action_retval_success;
}

action_retval_t Actions::set_talk_channel(std::string channel){
	MessageBox(0, &(std::string("Method needs to be implemented ") + __FUNCTION__)[0], 0, MB_OK);
	return action_retval_t::action_retval_success;
}

action_retval_t Actions::says(std::string text, std::string channel){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->release_basic_buttons();
	auto hotkey = HotkeyManager::get()->get_hotkey_match(text, 0, hotkey_cast_any, true);
	if (hotkey != hotkey_none){
		hotkey = HotkeyManager::get()->get_key_code_hotkey(hotkey);
		input->send_virtual_key((uint32_t)hotkey);
		return action_retval_t::action_retval_success;
	}

	hotkey = HotkeyManager::get()->get_hotkey_match(text, 0, hotkey_cast_any, false);
	if (hotkey != hotkey_none){
		hotkey = HotkeyManager::get()->get_key_code_hotkey(hotkey);

		input->send_virtual_key((uint32_t)hotkey);
		input->send_enter();
		return action_retval_t::action_retval_success;
	}

	ChatChannels::send_message(text , channel);

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::says_to_npc(std::string text, std::string npc_name) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (npc_name.length() > 0) {
		std::shared_ptr<CreatureOnBattle> creature = BattleList::get()->find_creature_on_screen(npc_name);
		if (!creature)
			return action_retval_t::action_retval_fail;

		if (goto_nearest_from_creature(npc_name) == action_retval_t::action_retval_fail)
			return action_retval_t::action_retval_fail;

		Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
		int total_distance = creature->get_coordinate_monster().get_axis_max_dist(self_coordinate);
		if (total_distance > 3)
			return action_retval_t::action_retval_fail;
	}
	return says(text, "NPCs");
}

action_retval_t Actions::loggout(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (TibiaProcess::get_default()->character_info->status_battle())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;

	input->send_ctrl_down();
	input->mSendMessage(WM_CHAR, 0x11, 0xc0100001);
	input->send_ctrl_up();
	return action_retval_t::action_retval_success;
};

action_retval_t Actions::turn(side_nom_axis_t::side_nom_axis_t side, int32_t timeout){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TimeChronometer chronometer;
	side_nom_axis_t::side_nom_axis_t current_side = side_nom_axis_t::side_north; //TODO GET CURRENT SIDE
	while (chronometer.elapsed_milliseconds() < timeout){
		Sleep(1);

		HighLevelVirtualInput::get_default()->turn(side);
		return action_retval_t::action_retval_success;//TODO - TIRAR ESSE RETURN DPS Q TIVER O GET CURRENT SIDE
		Sleep(200);
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::step_side(side_t::side_t side, int32_t timeout){
	if (!TibiaProcess::get_default()->get_is_logged()) 
		return action_retval_t::action_retval_fail;

	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	TimeChronometer chronometer;
	Coordinate offset;
	std::function <void()> move_function;
	switch (side){
	case side_t::side_east:
		move_function = std::bind(&HighLevelVirtualInput::arrow_right, HighLevelVirtualInput::get_default());
		offset = Coordinate(1, 0);
		break;
	case side_t::side_north:
		move_function = std::bind(&HighLevelVirtualInput::arrow_up, HighLevelVirtualInput::get_default());
		offset = Coordinate(0, -1);
		break;
	case side_t::side_north_east:
		move_function = std::bind(&HighLevelVirtualInput::arrow_up_rigth, HighLevelVirtualInput::get_default());
		offset = Coordinate(1, -1);
		break;
	case side_t::side_north_west:
		move_function = std::bind(&HighLevelVirtualInput::arrow_up_left, HighLevelVirtualInput::get_default());
		offset = Coordinate(-1, -1);
		break;
	case side_t::side_south:
		move_function = std::bind(&HighLevelVirtualInput::arrow_down, HighLevelVirtualInput::get_default());
		offset = Coordinate(0, 1);
		break;
	case side_t::side_south_east:
		move_function = std::bind(&HighLevelVirtualInput::arrow_down_right, HighLevelVirtualInput::get_default());
		offset = Coordinate(1, 1);
		break;
	case side_t::side_south_west:
		move_function = std::bind(&HighLevelVirtualInput::arrow_down_left, HighLevelVirtualInput::get_default());
		offset = Coordinate(-1, 1);
		break;
	case side_t::side_west:
		move_function = std::bind(&HighLevelVirtualInput::arrow_left, HighLevelVirtualInput::get_default());
		offset = Coordinate(-1, 0);
		break;
	default:
		return action_retval_t::action_retval_fail;
	}

	Coordinate final_coord = current_coordinate + offset;
	final_coord.z = current_coordinate.z;
	Coordinate going_step = TibiaProcess::get_default()->character_info->get_going_step();
	if (!timeout){

		if (final_coord == going_step)
			return action_retval_t::action_retval_success;

		move_function();
		return action_retval_t::action_retval_success;
	}


	if (final_coord != going_step)
		move_function();
		
	bool is_moving = false;
	uint32_t elapsed = 0;

	while ((elapsed = chronometer.elapsed_milliseconds()) < (uint32_t)timeout){
		Coordinate now_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		
		if (now_coord.x == final_coord.x && now_coord.y == final_coord.y){
			return action_retval_t::action_retval_success;
		}
		else if (now_coord.x != final_coord.x && now_coord.y != final_coord.y){
			if (now_coord.x != current_coordinate.x && now_coord.y != current_coordinate.y)
				return action_retval_t::action_retval_fail;
		}

		if (TibiaProcess::get_default()->character_info->get_going_step().is_null())
			return action_retval_t::action_retval_fail;			
		
		Sleep(10);
	}

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::refresh_profession_stuff(){
	//TODO
	/*if (time_to_refresh_profession > 0)return;
	time_to_refresh_profession = 1000;
	if (IsLogged() == false)
	{
	for (int i = 10; i < 0; i++)
	{
	_messages[i].message = "";
	}
	profession = 0;
	return;
	}
	else if (profession == 0)
	{
	if (!find_message_profession())
	{
	press_shift();//SendMessage(CurrentHWND,WM_KEYDOWN, 0x10,0x2a0001); // CTRL keydown
	ClickTelaTitleLeft(CordX(), CordY());
	Sleep(25);
	send_all_key_up();//SendMessage(CurrentHWND,WM_KEYUP, 0x10,0x2a0001); // CTRL keydown
	}
	else
	{
	for (int i = 0; i < 10; i++)
	{
	is_to_load_lua_setup[i] = true;
	}
	}
	}*/
	return action_retval_t::action_retval_success;
}

std::vector<uint32_t> get_openned_container_addresses(){
	auto& containers_copy = ContainerManager::get()->get_containers();
	std::vector<uint32_t> current_addresses;
	for (auto container : containers_copy){
		if (container.second->header_info)
			current_addresses.push_back(container.second->header_info->body_address);
	}
	return current_addresses;
}

action_retval_t Actions::open_container_at_coordinate(Coordinate coord, int32_t timeout, int32_t stack_pos, bool corpses, bool check_upstair, std::function<bool()> Stopfunction){
	auto auto_critical_handler = ContainerManager::get()->set_auto_critical_refresh();
	uint64_t not_owner_message_count = InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_you_are_not_the_owner)
		->count;
	uint64_t cannot_use_message_count = InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_you_cannot_use)
		->count;

	uint64_t first_go_downstairs_message_count_count = InfoCore::get()->get_warning_message_info(
		warning_message_type::warning_message_you_may_first_go_downstairs)
		->count;

	uint64_t first_go_upstairs_message_count = InfoCore::get()->get_warning_message_info(
		warning_message_type::warning_message_first_go_upstairs)
		->count;


	std::function<action_retval_t()> get_new_warning_message = [&](){
		if (InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_you_are_not_the_owner)
			->count
			> not_owner_message_count)
			return action_retval_warning_message_not_owner;

		if (InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_you_cannot_use)
			->count
			> cannot_use_message_count)
			return action_retval_warning_cannot_use;

		if (InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_you_may_first_go_downstairs)
			->count
			> first_go_downstairs_message_count_count)
			return action_retval_warning_message_first_go_downstairs;

		if (InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_first_go_upstairs)
			->count
			> first_go_upstairs_message_count)
			return action_retval_warning_message_first_go_upstairs;
		return action_retval_t::action_retval_none;
	};
	
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TimeChronometer chronometer;
	if (!TibiaProcess::get_default()->character_info->wait_stop_waking(timeout))
		return action_retval_t::action_retval_timeout;
			
	bool changed_last_container_address = false;
	std::vector<uint32_t> current_containers_address = get_openned_container_addresses();
	int current_container_address_count = current_containers_address.size();
	
	std::function<bool()> or_callback = [&]() -> bool {
		std::vector<uint32_t> local_current_containers_address = get_openned_container_addresses();
		int now_count = local_current_containers_address.size();
		bool new_is_open = current_container_address_count == now_count && 	Util::contains_vector<uint32_t>(current_containers_address, local_current_containers_address) == false;

		if (new_is_open || now_count > current_container_address_count){
			if (new_is_open){
			changed_last_container_address = true;
			return true;
		}
			else{
				TibiaProcess::get_default()->wait_ping_delay();
				now_count = get_openned_container_addresses().size();

				if (now_count > current_container_address_count){
					return true;
				}
			}
			
		}
		return false;
	};

	auto current_tile = gMapTilesPtr->get_tile_at(coord);
	if (!current_tile)
		return action_retval_t::action_retval_fail;

	uint32_t item_top = current_tile->get_top_item_id(true);
	auto creatures = BattleList::get()->get_creatures_at_coordinate(coord);
	if ((stack_pos < 0 || creatures.size()) || gMapMinimapPtr->is_coordinate_yellow(coord)
		|| ItemsManager::get()->is_item(item_top) || current_tile->have_element_field()){
		timeout = (int32_t)((float)timeout * 2.5f);
		auto temp = chronometer.elapsed_milliseconds();

		ContainerManager::get()->close_all_browse_fields();

		ContainerManager::get()->wait_browse_field_close(timeout - chronometer.elapsed_milliseconds());

		if (Stopfunction && Stopfunction())
			return action_retval_t::action_retval_trying;

		if (!FloatingMiniMenu::open_browse_field_at_coordinate(coord))
			return action_retval_t::action_retval_trying;

		bool container_open_res = ContainerManager::get()->wait_container_browse_field(timeout - chronometer.elapsed_milliseconds(), or_callback);

		if (get_new_warning_message() != action_retval_none)
			return get_new_warning_message();

		/*if (!container_open_res && chronometer.elapsed_milliseconds() < timeout){
			Sleep(50);
			container_open_res = ContainerManager::get()->wait_container_browse_field(timeout - chronometer.elapsed_milliseconds(), or_callback);
		}*/
		
		if (!container_open_res)
			if (!changed_last_container_address)
				return action_retval_t::action_retval_trying;
			
		changed_last_container_address = false;

		auto browse = ContainerManager::get()->get_browse_field();
		if (!browse)
			return action_retval_t::action_retval_trying;

		uint32_t order = (browse->get_used_slots() - 1) - (abs(stack_pos));
		if (order == UINT32_MAX)
			return action_retval_t::action_retval_fail;

		uint32_t item_id = browse->get_slot_at(order).id;
		if (ItemsManager::get()->is_item(item_id) && order > 0)
			order--;

		item_id = browse->get_slot_at(order).id;
		
		if (Stopfunction && Stopfunction())
			return action_retval_t::action_retval_trying;

		action_retval_t retval_use_item_on_container = use_item_on_container(ContainerPosition(browse->get_container_index(), order));
		if (retval_use_item_on_container == action_retval_t::action_retval_fail)
			return action_retval_t::action_retval_fail;
		
		else if (retval_use_item_on_container == action_retval_t::action_retval_trying)
			return action_retval_t::action_retval_trying;

		if (get_new_warning_message() != action_retval_none)
			return get_new_warning_message();

		container_open_res = ContainerManager::get()->wait_container_id_open(item_id, timeout - chronometer.elapsed_milliseconds(), or_callback);
		/*if (!container_open_res && chronometer.elapsed_milliseconds() < timeout){
			Sleep(50);
			container_open_res = ContainerManager::get()->wait_container_id_open(item_id, timeout - chronometer.elapsed_milliseconds(), or_callback);
		}*/
		if (get_new_warning_message() != action_retval_none)
			return get_new_warning_message();
		if (!container_open_res && chronometer.elapsed_milliseconds() < timeout)
			if (!changed_last_container_address)
				return action_retval_t::action_retval_timeout;

		TibiaProcess::get_default()->wait_ping_delay(0.5);
	}
	else{
		do{
			Sleep(1);
				if (chronometer.elapsed_milliseconds() > timeout)
					return action_retval_t::action_retval_timeout;
				
				if (Stopfunction && Stopfunction())
					return action_retval_t::action_retval_fail;

				action_retval_t openned = use_item_on_coordinate(coord);
				if (openned == action_retval_t::action_retval_fail){
					if (!InfoCore::get()->go_to_first_go_upstairs(coord))
						return action_retval_t::action_retval_fail;
				}

				if (get_new_warning_message() != action_retval_none)
					return get_new_warning_message();

				int temp_timeout = timeout - chronometer.elapsed_milliseconds();
				int32_t dif = ContainerManager::get()->wait_open_container_count_change(0, or_callback);
				if (dif < 0){
					if (chronometer.elapsed_milliseconds() < timeout){
						if (changed_last_container_address)
							break;			

						Sleep(50);

						dif = ContainerManager::get()->wait_open_container_count_change(0, or_callback);						
						if (dif > 0 || changed_last_container_address)
							break;						
					}
				}
				if (!dif){
					
					if (changed_last_container_address){
						
						break;
					}
					continue;
				}
				else if (dif > 0)
					break;				
					
			} while (true);
			if (chronometer.elapsed_milliseconds() > timeout)
				return action_retval_t::action_retval_timeout;			
			if (get_new_warning_message() != action_retval_none)
				return get_new_warning_message();
	}
	if (get_new_warning_message() != action_retval_none)
		return get_new_warning_message();
	return action_retval_t::action_retval_success;
}

action_retval_t Actions::open_main_bp(int32_t timeout){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TimeChronometer chronometer;
	auto auto_critical_handler = ContainerManager::get()->set_auto_critical_refresh();
	while (chronometer.elapsed_milliseconds() < timeout){
		uint32_t main_bp = Inventory::get()->body_bag();
		if (!main_bp)
			return action_retval_t::action_retval_fail;
		
		auto container = ContainerManager::get()->get_container_by_id(main_bp);
		if (container)
			return action_retval_t::action_retval_success;

		if (!Inventory::get()->use_at_inventory(inventory_slot_t::slot_bag))
			return action_retval_t::action_retval_fail;

		if (!ContainerManager::get()->wait_container_id_open(main_bp))
			return action_retval_t::action_retval_trying;

		Sleep(50);
	}
	return action_retval_t::action_retval_trying;
}

action_retval_t Actions::mantain_container_count(uint32_t item_id, uint32_t count){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;
	while (true){
		Sleep(50);

		auto containers = ContainerManager::get()->get_containers_by_id(item_id);
		if (containers.size() > count){
			uint32_t dif = containers.size() - count;
			for (auto container_it = containers.rbegin(); container_it != containers.rend(); container_it++){
				container_it->get()->close();
				Sleep(50);
				if (dif == 0)
					break;
			}
		}
		else{
			return action_retval_t::action_retval_success;
		}
	}
	return action_retval_t::action_retval_trying;
}

action_retval_t Actions::open_container_id(uint32_t item_id, bool unique, int32_t timeout){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	
	auto auto_critical_handler = ContainerManager::get()->set_auto_critical_refresh();
	auto containers = ContainerManager::get()->get_containers_by_id(item_id);
	TimeChronometer chronometer;
	while (chronometer.elapsed_milliseconds() < timeout){
		Sleep(10);
		if (unique){
			uint32_t current_count = containers.size();
			if (current_count == 1)
				return action_retval_t::action_retval_success;			
			else if (current_count > 1){
				mantain_container_count(item_id, 1);
				continue;
			}

			action_retval_t retval = open_main_bp();
			if (retval == action_retval_t::action_retval_fail)
				return retval;

			if (retval == action_retval_t::action_retval_trying)
				continue;

			auto main_container = ContainerManager::get()->get_container_by_id(Inventory::get()->body_bag());
			if (!main_container)
				continue;

			uint32_t slot_index = main_container->find_item_slot(item_id);
			if (slot_index == UINT32_MAX)
				return action_retval_t::action_retval_fail;

			if (BaseActions::use_item_on_to_container(
				ContainerPosition(main_container->get_container_index(), slot_index), item_id))
				Sleep(TibiaProcess::get_default()->get_action_wait_delay());

			containers = ContainerManager::get()->get_containers_by_id(item_id);

			LooterCore::get()->refresh_items_in_map_looted();
		}
		else{
			action_retval_t retval = open_main_bp();
			if (retval == action_retval_t::action_retval_fail)
				return retval;

			if (retval == action_retval_t::action_retval_trying)
				continue;

			auto main_container = ContainerManager::get()->get_container_by_id(Inventory::get()->body_bag());
			if (!main_container)
				continue;

			uint32_t slot_index = main_container->find_item_slot(item_id);

			if (slot_index == UINT32_MAX)
				return action_retval_t::action_retval_fail;

			if (BaseActions::use_item_on_to_container(ContainerPosition(main_container->get_container_index(), slot_index), item_id))
				Sleep(TibiaProcess::get_default()->get_action_wait_delay());

			LooterCore::get()->refresh_items_in_map_looted();

			if (ContainerManager::get()->get_container_by_id(Inventory::get()->body_bag()))	
				action_retval_t::action_retval_success;			
		}
	}
	return action_retval_t::action_retval_timeout;
}

action_retval_t Actions::set_follow(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!TibiaProcess::get_default()->character_info->is_follow_mode())
		Interface->click_follow();

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::unset_follow(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (TibiaProcess::get_default()->character_info->is_follow_mode())
		Interface->click_not_follow();

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::reach_creature(uint32_t cid) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	CreatureOnBattlePtr creature = BattleList::get()->find_creature_by_id(cid);

	if (!creature)
		return action_retval_t::action_retval_fail;

	return simple_go_coord_on_screen(creature->get_coordinate_monster(), 1);
}

action_retval_t Actions::reach_creature(std::string creatureName, int consider_near) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	CreatureOnBattlePtr creature = BattleList::get()->find_creature_on_screen(creatureName);
	if (!creature)
		return action_retval_t::action_retval_fail;	

	return goto_coord_on_screen(creature->get_coordinate_monster(), consider_near);
}

action_retval_t Actions::deposit_all(){
	int try_looping = 0;

	while (try_looping <= 3){
		Sleep(10);

		int count_money_begin = ContainerManager::get()->get_total_money();

		Actions::get()->says("hi", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(5);
		Actions::get()->says("deposit all", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);
		Actions::get()->says("yes", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2); 

		int count_money_end = ContainerManager::get()->get_total_money();
		if (count_money_begin != count_money_end || count_money_end == 0)
			return action_retval_t::action_retval_success;

		try_looping++;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::transfer_money(std::string NameChar, int value){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	Actions::get()->says("hi", "NPCs");
	TibiaProcess::get_default()->wait_ping_delay(5);
	Actions::get()->says("transfer " + std::to_string(value), "NPCs");
	TibiaProcess::get_default()->wait_ping_delay(2);
	Actions::get()->says(NameChar, "NPCs");
	TibiaProcess::get_default()->wait_ping_delay(2);
	Actions::get()->says("yes", "NPCs");
	TibiaProcess::get_default()->wait_ping_delay(2);
	Actions::get()->says("balance", "NPCs");

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::deposit_money(int money){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	int try_looping = 0;
	Actions::get()->reopen_containers();

	while (try_looping <= 3){
		Sleep(10);

		int count_money_begin = ContainerManager::get()->get_total_money();

		Actions::get()->says("hi", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(5);
		Actions::get()->says("deposit " + std::to_string(money), "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);
		Actions::get()->says("yes", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);

		int count_money_end = count_money_begin - money;
		if (ContainerManager::get()->get_total_money() == count_money_end || ContainerManager::get()->get_total_money() <= 0)
			return action_retval_t::action_retval_success;
		try_looping++;
	}

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::withdraw_value(int value, bool depositall, int delay){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (depositall)
		Actions::get()->deposit_all();

	int count_money_begin = ContainerManager::get()->get_total_money();
	Actions::get()->reopen_containers();

	int try_looping = 0;
	while (try_looping <= 2){
		Actions::get()->says("hi", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(5);
		Actions::get()->says("withdraw", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);
		Actions::get()->says(std::to_string(value), "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);
		Actions::get()->says("yes", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);
		Actions::get()->says("balance", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(2);

		int count_money_end = ContainerManager::get()->get_total_money();
		if (count_money_begin != count_money_end)
			return action_retval_t::action_retval_success;

		try_looping++;
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::check_move_item_container_to_container(ContainerPosition container_source,
	ContainerPosition container_location, uint32_t count, uint32_t check_item_id, bool wait){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	//TODO - melhorar fun��o, checka itens alterados.
	// FAZER CHECKAGEM DE BACKPACKS
	auto containerSource = ContainerManager::get()->get_container_by_index(container_source.container_index);
	auto containerLocation = ContainerManager::get()->get_container_by_index(container_location.container_index);

	auto current_container_state = ContainerManager::get()->get_container_by_index(container_source.container_index);
	if (containerSource && containerLocation)
		BaseActions::move_item_container_to_container(container_source, container_location, count, check_item_id);
	
	if (wait){
		if (!current_container_state)
			return action_retval_t::action_retval_fail;		
		if (current_container_state->wait_change_content())
			return action_retval_t::action_retval_success;		
	}
	else
		return action_retval_t::action_retval_success;

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::take_up_items_from_depot(int depotBackpackId, int destinyBackpackId, int item_id, int itemCount) {
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	ItemContainerPtr depot_chest = ContainerManager::get()->get_depot_chest_container();
	if (!depot_chest)
		return action_retval_t::action_retval_fail;

	ContainerPosition depot_chest_position = ContainerManager::get()->find_item(depotBackpackId, depot_chest->get_id());
	if (depot_chest_position.is_null())
		return action_retval_t::action_retval_fail;

	use_item_on_container(depot_chest_position, item_id);
	ContainerManager::get()->wait_container_id_open(depotBackpackId);

	ItemContainerPtr dpBackpack = ContainerManager::get()->get_container_by_id(depotBackpackId);
	if (!dpBackpack) 
		return action_retval_t::action_retval_fail;
	
	int current_value = 0;
	int total_try_count = 0;
	while (total_try_count < Actions::Max_Count) {
		Sleep(10);

		if (current_value >= itemCount)
			return action_retval_t::action_retval_success;

		int slot_index = dpBackpack->find_item_slot(item_id);
		tibia_slot slot_info = dpBackpack->get_slot_at(slot_index);

		int value_to_move = 0;
		int current_value_added = (itemCount - current_value);
		if (slot_info.stack_count <= (uint32_t)current_value_added) {
			value_to_move = slot_info.stack_count;
		}
		else if ((int)slot_info.stack_count > current_value_added) {
			value_to_move = current_value_added;
		}

		ContainerPosition item_position(dpBackpack->get_container_index(), slot_index);

		ItemContainerPtr destinyBP = ContainerManager::get()->get_container_by_id(destinyBackpackId);
		if (!destinyBP)
			break;

		Actions::move_item_container_to_container(item_position, *destinyBP->get_container_position_to_add_item(), value_to_move, item_id);

		if (!destinyBP->wait_change_content())
			total_try_count++;

		current_value = current_value + value_to_move;
	}

	return action_retval_t::action_retval_success;
}

action_retval_t Actions::use_exani_hur(exani_hur_t up_or_down, Coordinate location, side_nom_axis_t::side_nom_axis_t side,bool forcemove){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	std::string spell;
	switch (up_or_down){
	case exani_hur_t::exani_hur_up:
		spell = "exani hur up";
		break;
	case exani_hur_t::exani_hur_down:
		spell = "exani hur down";
		break;
	default:
		return action_retval_t::action_retval_fail;
	}

	if (forcemove){
		switch (side){
		case side_nom_axis_t::side_north:
			step_side(side_t::side_north, 1000);
			break;
		case side_nom_axis_t::side_south:
			step_side(side_t::side_south, 1000);
			break;
		case side_nom_axis_t::side_east:
			step_side(side_t::side_east, 1000);
			break;
		case side_nom_axis_t::side_west:
			step_side(side_t::side_west, 1000);
			break;
		default:
			return action_retval_t::action_retval_fail;
		}
		
		Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		if (self_coord.z != location.z)
			return action_retval_t::action_retval_success;
		
		goto_coord_on_screen(location, 0);	
		TibiaProcess::get_default()->character_info->wait_stop_waking();	

		TibiaProcess::get_default()->wait_ping_delay(2.0);

		Actions::turn(side);
	}


	SpellCasterCore::get()->can_cast_spell(spell);

	hotkey_t hotkeyNumber = HotkeyManager::get()->findHotkeyByText(spell);

	if (hotkeyNumber != hotkey_none)
		HotkeyManager::get()->useHotkey(hotkeyNumber);

	TibiaProcess::get_default()->wait_ping_delay(2.0);

	if (!Actions::says(spell))
		return action_retval_t::action_retval_fail;

	TibiaProcess::get_default()->wait_ping_delay(2.0);

	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (self_coord.z != location.z){
		MapTiles::wait_refresh();
		return action_retval_t::action_retval_success;
	}

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::refill_weap_amun_process(std::function<bool()> Stopfunction){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!NeutralManager::get()->get_bot_state())
		return action_retval_t::action_retval_fail;

	int temp_body_weapon = Inventory::get()->body_weapon();
	int temp_qty = ContainerManager::get()->get_item_count(temp_body_weapon);

	std::vector<uint32_t> containers_ids = LooterManager::get()->get_all_containers_id();
	std::vector<ItemContainerPtr> containers = ContainerManager::get()->get_containers_by_ids(containers_ids);
	
	if (temp_qty > 1){
		int qty = Inventory::get()->weapon_count();

		ContainerPosition container_position = ContainerPosition();
		for (auto container : containers){
			uint32_t item_slot = container->find_item_slot(temp_body_weapon);
			if (item_slot == UINT32_MAX)
				continue;

			ContainerPosition container_position_temp = ContainerPosition(container->get_container_index(), item_slot);
			if (container_position.is_null())
				continue;		

			container_position = container_position_temp;
		}

		if (container_position.is_null())
			return action_retval_t::action_retval_fail;
		
		if (qty < 90 && qty > 15 && Stopfunction == false){
			if (Inventory::get()->put_item(temp_body_weapon, inventory_slot_t::slot_weapon, 100, container_position))
				return action_retval_t::action_retval_fail;
		}
		else if (qty <= 15){
			if (Inventory::get()->put_item(temp_body_weapon, inventory_slot_t::slot_weapon, 100, container_position))
				return action_retval_t::action_retval_success;
		}
	}
	else{
		int body_rope = Inventory::get()->body_rope();
		if (body_rope == 0)
			return action_retval_t::action_retval_success;

		int qty = Inventory::get()->ammunation_count();
		ContainerPosition container_position = ContainerPosition();
		for (auto container : containers){
			uint32_t item_slot = container->find_item_slot(body_rope);
			if (item_slot == UINT32_MAX)
				continue;

			ContainerPosition container_position_temp = ContainerPosition(container->get_container_index(), item_slot);
			if (container_position.is_null())
				continue;

			container_position = container_position_temp;
		}

		if (container_position.is_null())
			return action_retval_t::action_retval_fail;

		if (qty < 90 && qty > 15 && Stopfunction == false){
			if (Inventory::get()->put_item(body_rope, inventory_slot_t::slot_rope, 100, container_position))
				return action_retval_t::action_retval_success;
		}
		else if (qty <= 15){
			if (Inventory::get()->put_item(body_rope, inventory_slot_t::slot_rope, 100, container_position))
				return action_retval_t::action_retval_success;
		}
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::refill_weap_amun_process_by_id(uint32_t temp_id, std::function<bool()> Stopfunction){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	if (!NeutralManager::get()->get_bot_state())
		return action_retval_t::action_retval_fail;

	if (temp_id == 0)
		return action_retval_t::action_retval_fail;

	if (Inventory::get()->body_shield() != 0){
		int qty = Inventory::get()->weapon_count();
		int qty_in_container = ContainerManager::get()->get_item_count(temp_id);

		if (qty_in_container == 0)
			return action_retval_t::action_retval_fail;

		if (qty < 90 && qty > 15 && Stopfunction == false){
			if (Inventory::get()->put_item(temp_id, inventory_slot_t::slot_weapon, 100))
				return action_retval_t::action_retval_success;
		}
		else if (qty <= 15){
			if (Inventory::get()->put_item(temp_id, inventory_slot_t::slot_weapon, 100))
				return action_retval_t::action_retval_success;
		}
	}
	else if (Inventory::get()->body_weapon() != 0){
		int qty = Inventory::get()->ammunation_count();
		int qty_in_container = ContainerManager::get()->get_item_count(temp_id);

		if (qty_in_container == 0)
			return action_retval_t::action_retval_fail;

		if (qty < 90 && qty > 15 && Stopfunction == false){
			if (Inventory::get()->put_item(temp_id, inventory_slot_t::slot_rope, 100))
				return action_retval_t::action_retval_success;
		}
		else if (qty <= 15){
			if (Inventory::get()->put_item(temp_id, inventory_slot_t::slot_rope, 100))
				return action_retval_t::action_retval_success;
		}
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::cancel_attack_chase(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return action_retval_t::action_retval_fail;
	input->send_esc();
	//TODO must verify if target was gonne
	return action_retval_t::action_retval_success;
}
action_retval_t Actions::open_container_while_hast_next(uint32_t item_id, int32_t timeout){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TimeChronometer timer;
	while (timer.elapsed_milliseconds() < timeout){
		Sleep(10);

		std::shared_ptr<ItemContainer> destination = ContainerManager::get()->get_container_by_id(item_id);
		if (!destination){
			action_retval_t open_container_retval = open_container_id(item_id, true, timeout - timer.elapsed_milliseconds());
			if (open_container_retval == action_retval_t::action_retval_fail)
				return action_retval_t::action_retval_fail;

			destination = ContainerManager::get()->get_container_by_id(item_id);
			if (!destination)
				return action_retval_t::action_retval_trying;
		}

		if (!destination->has_next()){
			TibiaProcess::get_default()->wait_ping_delay();

			destination = ContainerManager::get()->get_container_by_id(item_id);
			if (!destination || destination->is_full())
				continue;

			return action_retval_t::action_retval_success;
		}
		else{
			action_retval_t retval = destination->open_next(timeout - timer.elapsed_milliseconds());
			if (retval == action_retval_success || retval == action_retval_timeout)
				continue;
		}
		return action_retval_t::action_retval_fail;
	}

	return action_retval_t::action_retval_timeout;
}
action_retval_t Actions::open_container_while_full(uint32_t item_id, int32_t timeout){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TimeChronometer timer;
	while (timer.elapsed_milliseconds() < timeout){
		Sleep(10);
		std::shared_ptr<ItemContainer> destination = ContainerManager::get()->get_container_by_id(item_id);	
		if (!destination){
			action_retval_t open_container_retval = open_container_id(item_id, true, timeout - timer.elapsed_milliseconds());
			if (open_container_retval == action_retval_t::action_retval_fail)
				return action_retval_t::action_retval_fail;
			
			destination = ContainerManager::get()->get_container_by_id(item_id);
			if (!destination)
				return action_retval_t::action_retval_trying;			
		}
		
		if (!destination->is_full()){
			TibiaProcess::get_default()->wait_ping_delay();

			destination = ContainerManager::get()->get_container_by_id(item_id);
			if (!destination || destination->is_full())
				continue;
			
			return action_retval_t::action_retval_success;
		}
		else{
			action_retval_t retval = destination->open_next(timeout - timer.elapsed_milliseconds());			
			if (retval == action_retval_success || retval == action_retval_timeout)
				continue;
		}
		return action_retval_t::action_retval_fail;
	}

	return action_retval_t::action_retval_timeout;
}

action_retval_t Actions::sort_containers_content(std::map< uint32_t /*item_id*/, uint32_t/*container id*/>& containers_items_set, uint32_t timeout){
	return try_sort_containers_content(containers_items_set, timeout);
}

action_retval_t Actions::try_sort_containers_content(std::map< uint32_t /*item_id*/, uint32_t/*container id*/>& containers_items_set, uint32_t timeout){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	std::vector<uint32_t> our_containers_id;

	for (auto containers_param : containers_items_set)
		if (std::find(our_containers_id.begin(), our_containers_id.end(), containers_param.second) == our_containers_id.end())
			our_containers_id.push_back(containers_param.second);

	std::function<bool(uint32_t)> is_our_container =
		[&](uint32_t container_id) -> bool{
		return std::find(our_containers_id.begin(), our_containers_id.end(), container_id) != our_containers_id.end();
	};

	TimeChronometer timer;
	while ((uint32_t)timer.elapsed_milliseconds() < timeout){
		Sleep(10);

		bool has_some_job = false;
		auto containers = ContainerManager::get()->get_containers();
		for (auto container : containers){
			if (!is_our_container(container.second->get_id()))
				continue;
			if (!container.second->slots)
				continue;
			int32_t used_slots = container.second->get_used_slots();
			tibia_slot* slots_ptr = container.second->slots;
			for (int32_t i = 0; i < used_slots; i++){
				if (containers_items_set.find(slots_ptr[i].id) == containers_items_set.end())
					continue;
				uint32_t dest_container_id = containers_items_set[slots_ptr[i].id];
				if (dest_container_id == container.second->get_id())
					continue;
				auto destination = ContainerManager::get()->get_container_by_id(dest_container_id);
				if (!destination)
					continue;

				if (destination->is_full())
					continue;

				Actions::check_move_item_container_to_container(
					ContainerPosition(container.second->get_container_index(), i),
					destination->get_random_container_slot(),
					100,
					slots_ptr[i].id);
				has_some_job = true;
			}
		}

		if (!has_some_job)
			return action_retval_success;
	}
	return action_retval_timeout;
}

action_retval_t Actions::up_backpack(std::string backpack){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto is_open = ContainerManager::get()->get_container_by_caption(backpack);

	if (!is_open)
		return action_retval_t::action_retval_fail;

	uint32_t olde_body = is_open->header_info->body_address;

	is_open->up_container();

	TimeChronometer time;
	while (time.elapsed_milliseconds() < 2000) {
		TibiaProcess::get_default()->wait_ping_delay(1.0);

		auto current_container = ContainerManager::get()->get_container_by_caption(backpack);

		if (!current_container)
			return action_retval_t::action_retval_fail;

		uint32_t current_body = current_container->header_info->body_address;

		if (olde_body != current_body)
			return action_retval_t::action_retval_success;

	}

	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::up_backpack(int index){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	auto is_open = ContainerManager::get()->get_container_by_index(index);

	if (!is_open)
		return action_retval_t::action_retval_fail;

	uint32_t olde_body = is_open->header_info->body_address;	

	is_open->up_container();

	int try_count = 0;

	while (try_count < Actions::Max_Count) {
		TibiaProcess::get_default()->wait_ping_delay(1.0);

		auto current_container = ContainerManager::get()->get_container_by_index(index);

		if (!current_container)
			return action_retval_t::action_retval_fail;

		uint32_t current_body = current_container->header_info->body_address;

		if (olde_body != current_body)
			return action_retval_t::action_retval_success;

		try_count++;
	}

	return action_retval_t::action_retval_fail;	
}

action_retval_t Actions::reach_nearest_to_creature(uint32_t cid){
	MessageBox(0, &(std::string("Method needs to be implemented ") + __FUNCTION__)[0], 0, MB_OK);
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::drop_empty_potions(){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	bool logged = TibiaProcess::get_default()->get_is_logged();
	if (!logged)
		return action_retval_t::action_retval_fail;

	auto character_info = TibiaProcess::get_default()->character_info;
	if (!character_info)
		return action_retval_t::action_retval_fail;

	auto empty_potions_list = ItemsManager::get()->get_items_as_empty_potions();
	for (int item_id : empty_potions_list) {
		int item_count = ContainerManager::get()->get_item_count(item_id);

		if (item_count <= 5)
			continue;

		ContainerPosition position = ContainerManager::get()->find_item(item_id);
		return BaseActions::move_item_container_to_coordinate(position, character_info->get_self_coordinate(), 100, item_id);
	}
	return action_retval_t::action_retval_fail;
}

action_retval_t Actions::drop_item_from_containers(std::vector<uint32_t> containers_id, std::vector<uint32_t> item_ids,
	int32_t timeout, bool fast_drop){
	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TimeChronometer timer;
	action_retval_t retval;
	do{
		Sleep(10);

		retval = try_drop_item_from_containers(containers_id, item_ids, timeout - timer.elapsed_milliseconds(), fast_drop);
	} while (retval != action_retval_fail && retval != action_retval_has_not_more_work && timer.elapsed_milliseconds() < timeout);
	return retval;
}

action_retval_t Actions::try_drop_item_from_containers(std::vector<uint32_t> containers_id,
	std::vector<uint32_t> item_ids,
	int32_t timeout, bool fast_drop){

	if (!TibiaProcess::get_default()->get_is_logged())
		return action_retval_t::action_retval_fail;

	TimeChronometer timer;
	action_retval_t retval;

	std::function<bool(uint32_t)> is_in_container_vector = [&](uint32_t id){ 
		return Util::is_is_vector<uint32_t>(containers_id, id);
	};

	bool has_action;
	do{
		Sleep(10);

		has_action = false;
		std::vector<ItemContainerPtr> containers = ContainerManager::get()->get_containers_by_ids(containers_id);
		
		if (fast_drop){
			for (auto container = containers.rbegin(); container != containers.rend(); container++){
				uint32_t this_container_index = container->get()->get_container_index();
				uint32_t used_count = container->use_count();
				auto slots = container->get()->slots;
				if (slots){
					for (uint32_t i = 0; i < used_count; i++){
						if (Util::is_is_vector<uint32_t>(item_ids, slots[i].id)){
							has_action = true;
							Actions::move_item_container_to_coordinate(ContainerPosition(this_container_index, i),
								TibiaProcess::get_default()->character_info->get_self_coordinate(), 100, slots[i].id);
						}
					}
					retval = try_drop_item_from_containers(containers_id, item_ids, timeout);
				}
			}
			Sleep(TibiaProcess::get_default()->get_action_delay_promedy_time(1));
			
		}
		else{//TODO
			MessageBox(0,"function drop without fast option not implemented",0,0);
		}
	}
	while (has_action && retval != action_retval_fail && retval != action_retval_has_not_more_work && timer.elapsed_milliseconds() < timeout);
	if (!has_action)
		return action_retval_success;
	return action_retval_timeout;
}