#pragma once
#include "constants.h"
#include "Util.h"
#include "InputManager.h"
#include <lua.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include "ContainerManager.h"
#include <functional>

//#pragma comment(lib,"lua51.lib")
#pragma pack(push,1)
enum lua_core_ids{
	lua_core_waypoint_id,
	lua_core_hotkey_id,
	lua_core_hud_id,
	lua_core_hunter_id,
	lua_core_luabg_id,
	lua_core_special_areas_id
};

#define luaRegisterEnumTable(enumName)\
	{\
	std::string enumStr = enumName;\
	lua_newtable(m_luaState);\
	lua_setglobal(m_luaState, enumStr.c_str());\
	KeywordManager::get()->registerEnumTable(enumName);\
	}

#define registerEnum(value) \
{\
	std::string enumName = #value;\
	KeywordManager::get()->registerEnumTable(value);\
	registerGlobalVariable(enumName.substr(enumName.find_last_of(':') + 1), value);\
}

#define registerEnumIn(tableName, value)\
		{\
	std::string enumName = #value;\
	KeywordManager::get()->registerTableVariable(tableName, enumName);\
	registerVariable(tableName, enumName.substr(enumName.find_last_of(':') + 1), value);\
	}

#define registerEnumInNotAuto(tableName, ValueName, value)\
	{\
	std::string enumName = ValueName;\
	KeywordManager::get()->registerTableVariable(tableName, ValueName);\
	registerVariable(tableName, ValueName, value);\
	}

#define LUA_RET_INT(val)\
	LuaCore::pushNumber(state,val);\
	return 1;

#define LUA_RET_BOOL(val)\
	LuaCore::pushBoolean(state ,val);\
	return 1;

#define LUA_RET_STRING(val)\
	LuaCore::pushString(state,&val[0]);\
	return 1;

#define LUA_RET_COORDINATE(val)\
	LuaCore::pushPosition(state,val);\
	return 1;

#define LUA_RET_NULL()\
	LuaCore::pushNil(state);\
	return 1;


class LuaCore{	
	static std::map<uint32_t , std::shared_ptr<LuaCore>> cores;
	static std::map<std::string /*codes*/, std::shared_ptr<LuaCore>> lua_waypoint;
	static neutral_mutex static_mtx_access;
	neutral_mutex mtx_access;
	lua_State* m_luaState;	
	bool console_is_open;

public:

	std::vector<std::string > vectorCode;
	static std::shared_ptr<LuaCore> getState(uint32_t id);
	static std::shared_ptr<LuaCore> getState_waypoint(std::string id);

	static std::shared_ptr<LuaCore> getHudLuaCore(){
		return getState(lua_core_hud_id);
	}

	static std::shared_ptr<LuaCore> getBgLuaCore(){
		return getState(lua_core_luabg_id);
	}

	LuaCore();

	void refresh();
	
	void run_thread_scripts();

	uint32_t get_uin32_t(std::string var);

	bool execute(std::string script, lua_t lua_type);

	void simple_run_script(std::string& script){
		
		RunScript(script);
	}

	void RunScript(std::string& script, std::string name = "");

	void RunScript(char* script, char* name = "");

	bool LoadLib(std::string filePath);

	void AddErrorLog(char* error,std::string name = "");

	bool isNil(int32_t arg);

	bool isNumber(int32_t arg);

	bool isString(int32_t arg);

	bool isBoolean(int32_t arg);

	bool isTable(int32_t arg);

	bool isFunction(int32_t arg);

	bool isUserdata(int32_t arg);

	void pushBoolean(bool value);

	void pushNil();

	void pushString(std::string str);

	void pushPosition(Position& position);

	void registerTable(std::string tableName);

	void registerClass(std::string className, std::string baseClass, lua_CFunction newFunction = nullptr);

	void registerMethod(std::string globalName, std::string methodName, lua_CFunction func);

	void registerMetaMethod(std::string className, std::string methodName, lua_CFunction func);

	void registerGlobalMethod(std::string functionName, lua_CFunction func);

	void registerVariable(std::string tableName, std::string name, lua_Number value);

	void registerGlobalVariable(std::string name, lua_Number value);

	uint32_t getitem_id(int32_t arg);

	void pushNumber(lua_Number number);

	void setMetatable(int32_t index, std::string string);

	std::string getString(int32_t arg);

	bool getBoolean(int32_t arg);

	Position getPosition(int32_t arg);

	std::string getFieldString(int32_t arg, std::string key);

	void pushContainerSlot(const ContainerSlot& container_position);

	void pushContainerPosition(const ContainerPosition& container_position);

	static void pushContainerPosition(lua_State* state, ContainerPosition& container_position);

	void pushContainerSlot(lua_State* state, ContainerSlot& container_position);

	void pushContainer(const ItemContainer& container);

	void pushContainer(ItemContainer* container);

	ContainerSlot getContainerSlot(int32_t arg);

	ContainerPosition getContainerPosition(int32_t arg);

	void BindLua();

	template<typename T>
	T getField(int32_t arg, std::string key)
	{
		lua_getfield(m_luaState, arg, key.c_str());
		return getNumber<T>(m_luaState, -1);
	}
	template<typename T>
	T getNumber(int32_t arg)
	{
		return static_cast<T>(lua_tonumber(m_luaState, arg));
	}
	template<class T>
	T* getUserdata(int32_t arg)
	{
		T** userdata = getRawUserdata<T>(m_luaState, arg);
		if (!userdata) {
			return nullptr;
		}
		return *userdata;
	}
	template<class T>
	T** getRawUserdata(int32_t arg)
	{
		return static_cast<T**>(lua_touserdata(m_luaState, arg));
	}

	template<class T>
	void pushUserdata(T* value)
	{
		T** userdata = static_cast<T**>(lua_newuserdata(m_luaState, sizeof(T*)));
		*userdata = value;
	}
#pragma region STATIC

	template<typename T>
	static T getField(lua_State* L, int32_t arg, std::string key)
	{
		lua_getfield(L, arg, key.c_str());
		return getNumber<T>(L, -1);
	}
	template<typename T>
	static T getNumber(lua_State* L, int32_t arg)
	{
		return static_cast<T>(lua_tonumber(L, arg));
	}
	template<class T>
	static T* getUserdata(lua_State* L, int32_t arg)
	{
		T** userdata = getRawUserdata<T>(L, arg);
		if (!userdata) {
			return nullptr;
		}
		return *userdata;
	}
	template<class T>
	static T** getRawUserdata(lua_State* L, int32_t arg)
	{
		return static_cast<T**>(lua_touserdata(L, arg));
	}

	template<class T>
	static void pushUserdata(lua_State* L, T* value){
		T** userdata = static_cast<T**>(lua_newuserdata(L, sizeof(T*)));
		*userdata = value;
	}

	static void pushNumber(lua_State* state, int val);

	static void pushBoolean(lua_State* state, bool val);

	bool getGlobal(std::string& function, InputBaseEvent* input);
	
	std::string getGlobal(std::string global_name);

	void lua_push_table_input(InputBaseEvent* input);

	static void pushString(lua_State*state, std::string val);
	
	static void pushPosition(lua_State* state, Position& position);
	
	static void pushRect(lua_State* state, Rect rect);

	static void pushLine(lua_State* state, std::pair<Position, Position> line_pair);
	
	static void pushLine(lua_State* state, std::pair<Point, Point> line_pair);

	static void pushNil(lua_State* state);

	static uint32_t LuaCore::getitem_id(lua_State* state, int32_t arg);

	static std::string LuaCore::getString(lua_State* state, int32_t arg);

	static Position getPosition(lua_State* state, int32_t arg);

	static bool LuaCore::getBoolean(lua_State* state, int32_t arg);

	static void pushContainer(lua_State* state, ItemContainer* container);

	static ContainerPosition LuaCore::getContainerPosition(lua_State* state, int32_t arg);

	static void setMetatable(lua_State* state, int32_t index, std::string string);

	static std::string getFieldString(lua_State* state, int32_t arg, std::string key);
#pragma endregion
};

class LuaLibCache{
public:
	std::vector<std::string> libs_files;
	void loadLibs();

	LuaLibCache();
	static LuaLibCache* get();
};

#pragma pack(pop)