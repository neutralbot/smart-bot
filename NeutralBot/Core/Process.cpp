#pragma once
#include "Process.h"
#include "..\\PRODUCTVERSION.h"
#include <stdio.h>
#include <windows.h>
#include <SetupAPI.h>
#include <stdlib.h>
#include <Psapi.h>
#include <tlhelp32.h>
#include <vdmdbg.h>
#include <conio.h>
#include <winternl.h>
#include <vector>
#include <clocale>
#include <locale>
#include <codecvt>

#pragma comment(lib,"Psapi.lib")


// Application-defined callback for EnumWindows
BOOL CALLBACK EnumProc(HWND hWnd, LPARAM lParam) {
	char out[1024];
	int len = GetWindowText(hWnd, out, 1024);
	out[len] = 0;
	std::string t(out);

	if (t.find("Tibia") == std::string::npos)
		return TRUE;

	// Retrieve storage location for communication data
	EnumData& ed = *(EnumData*)lParam;
	DWORD dwProcessId = 0x0;
	// Query process ID for hWnd
	GetWindowThreadProcessId(hWnd, &dwProcessId);
	// Apply filter - if you want to implement additional restrictions,
	// this is the place to do so.
	if (ed.dwProcessId == dwProcessId) {
		// Found a window matching the process ID
		ed.hWnd = hWnd;
		// Report success
		SetLastError(ERROR_SUCCESS);
		// Stop enumeration
		return FALSE;
	}
	// Continue enumeration
	return TRUE;
}

HWND Process::FindWindowFromProcessId(DWORD dwProcessId) {
	EnumData ed = { dwProcessId };
	if (!EnumWindows(EnumProc, (LPARAM)&ed) && (GetLastError() == ERROR_SUCCESS)) {
		return ed.hWnd;
	}
	return NULL;
}

HWND Process::FindWindowFromProcess(HANDLE hProcess) {
	return FindWindowFromProcessId(GetProcessId(hProcess));
}

HWND Process::GetProcessIdMainThread(DWORD processId){
	return FindWindowFromProcessId(processId);
}

void Process::ShowWindow(HWND window){
	::ShowWindow(window, SW_SHOW);
}

void Process::HideWindow(HWND window){
	::ShowWindow(window, SW_HIDE);
}

int Process::GetForegroundPid(){
	int pid = 0;
	GetWindowThreadProcessId(GetForegroundWindow(), (LPDWORD)&pid);
	return pid;
}

void Process::RefresherNameChar(){

}
void Process::RefresherTibiaClients(){
	lock_guard _lock(mtx_access);
	
	tibia_processes.clear();

	if (!load_fundamental){
		AddressManager::get()->load_address("addressfundamental.lua");
		load_fundamental = true;
	}
	

	int CountEncountered = 0;
	PROCESSENTRY32 pe;
	HANDLE thSnapShot;
	BOOL retval, ProcFound = false;
	char TibiaName[] = "Tibia.exe";///"Tibia.exe"

	thSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	pe.dwSize = sizeof(PROCESSENTRY32);
	retval = Process32First(thSnapShot, &pe);
	int address_of = 0;
	do {
		if (!strstr(pe.szExeFile, "Tibia.exe"))
			continue;		

		DWORD pid = pe.th32ProcessID;//pid=pid
		uint32_t version = PRODUCT_VERSION::get()->GetTibiaVersion(pid);	
		uint32_t version_end = 0;
		for (auto it : version_suport){
			if (it == version){
				version_end = it;
				break;
			}
			else
				version_end = 0000;
		}

		tibia_processes[pid].pid = pid;
		if (!version_end){
			tibia_processes[pid].version = version;
			tibia_processes[pid].supported = false;
			tibia_processes[pid].player_name = "Not Supported Version";
			continue;
		}
		else{
			tibia_processes[pid].version = version_end;
			tibia_processes[pid].supported = true;
			tibia_processes[pid].player_name = "";
		}


		if (!version_end){
			continue;
		}
		HANDLE PROC_HANDLE;//declara o handle
		PROC_HANDLE = OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, false, pid);//coloca o valor no handle
		if (!PROC_HANDLE)
			continue;

		int BaseAddress = GetModuleBase("Tibia.exe", pe.th32ProcessID);//"Tibia"
		
		//int AddressIdChar = BaseAddress + AddressManager::get()->getAddress(ADDRESS_PLAYER_ID);//Valor	
		//int AddressPrimeiroIdBattle = BaseAddress + AddressManager::get()->getAddress(ADDRESS_BATTLE_FIRST_ID);//String
		//int AddressLogged = BaseAddress + AddressManager::get()->getAddress(ADDRESS_LOGGED);

		int AddressIdChar = BaseAddress + AddressManager::get()->get_address_by_var("lookAddressIdChar" +std::to_string(version));
		int AddressPrimeiroIdBattle = BaseAddress + AddressManager::get()->get_address_by_var("lookAddressPrimeiroIdBattle" +std::to_string(version));
		int AddressLogged = BaseAddress + AddressManager::get()->get_address_by_var("lookAddressLogged" + std::to_string(version));

		int is_logged = 10;

		if (!ReadProcessMemory(PROC_HANDLE, (LPCVOID)AddressLogged, &is_logged, sizeof(is_logged), NULL) || is_logged != 30){
			CloseHandle(PROC_HANDLE);
			continue;
		}

		int current_battle_address = AddressPrimeiroIdBattle;
		int current_player_id;

		if (!ReadProcessMemory(PROC_HANDLE, (LPCVOID)AddressIdChar, &current_player_id, sizeof(current_player_id), NULL)){
			CloseHandle(PROC_HANDLE);
			continue;
		}

		for (int i = 0; i < 20/*try 20 times*/; i++){
			int now_player_id;

			if (!ReadProcessMemory(PROC_HANDLE, (LPCVOID)(current_battle_address + (i * sizeof(CreatureOnBattle))), &now_player_id, sizeof(now_player_id), NULL))
				break;

			if (current_player_id != now_player_id)
				continue;

			char character_name[256];
			ZeroMemory(character_name, 256);

			if (!ReadProcessMemory(PROC_HANDLE, (LPCVOID)(current_battle_address + (i * sizeof(CreatureOnBattle)) + 4), &character_name, 255, NULL))
				break;
			
			tibia_processes[pid].player_name = character_name;
		}
	} while (Process32Next(thSnapShot, &pe));
}

DWORD Process::GetModuleBase(LPSTR lpModuleName, DWORD dwProcessId){
	MODULEENTRY32 lpModuleEntry = { 0 };
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessId);
	if (!hSnapShot)
		return NULL;
	lpModuleEntry.dwSize = sizeof(lpModuleEntry);
	BOOL bModule = Module32First(hSnapShot, &lpModuleEntry);
	while (bModule){
		if (!strcmp(lpModuleEntry.szModule, lpModuleName)){
			CloseHandle(hSnapShot);
			return (DWORD)lpModuleEntry.modBaseAddr;
		}
		bModule = Module32Next(hSnapShot, &lpModuleEntry);
	}
	CloseHandle(hSnapShot);

	return NULL;
}



BOOL WINAPI GetParentPID(PROCESSENTRY32& procentry)
{
	OSVERSIONINFO  osver;
	HINSTANCE      hInstLib;
	HANDLE         hSnapShot;
	BOOL           bContinue;

	// ToolHelp Function Pointers.
	HANDLE(WINAPI *lpfCreateToolhelp32Snapshot)(DWORD, DWORD);
	BOOL(WINAPI *lpfProcess32First)(HANDLE, LPPROCESSENTRY32);
	BOOL(WINAPI *lpfProcess32Next)(HANDLE, LPPROCESSENTRY32);

	// Check to see if were running under Windows95 or
	// Windows NT.
	osver.dwOSVersionInfoSize = sizeof(osver);
	if (!GetVersionEx(&osver))
	{
		return FALSE;
	}

	if (osver.dwPlatformId != VER_PLATFORM_WIN32_NT)
	{
		return FALSE;
	}




	hInstLib = LoadLibraryA("Kernel32.DLL");
	if (hInstLib == NULL)
	{
		return FALSE;
	}

	// Get procedure addresses.
	// We are linking to these functions of Kernel32
	// explicitly, because otherwise a module using
	// this code would fail to load under Windows NT,
	// which does not have the Toolhelp32
	// functions in the Kernel 32.
	lpfCreateToolhelp32Snapshot =
		(HANDLE(WINAPI *)(DWORD, DWORD))
		GetProcAddress(hInstLib,
		"CreateToolhelp32Snapshot");
	lpfProcess32First =
		(BOOL(WINAPI *)(HANDLE, LPPROCESSENTRY32))
		GetProcAddress(hInstLib, "Process32First");
	lpfProcess32Next =
		(BOOL(WINAPI *)(HANDLE, LPPROCESSENTRY32))
		GetProcAddress(hInstLib, "Process32Next");
	if (lpfProcess32Next == NULL ||
		lpfProcess32First == NULL ||
		lpfCreateToolhelp32Snapshot == NULL)
	{
		FreeLibrary(hInstLib);
		return FALSE;
	}

	// Get a handle to a Toolhelp snapshot of the systems
	// processes.
	hSnapShot = lpfCreateToolhelp32Snapshot(
		TH32CS_SNAPPROCESS, 0);
	if (hSnapShot == INVALID_HANDLE_VALUE)
	{
		FreeLibrary(hInstLib);
		return FALSE;
	}

	// Get the first process' information.
	memset((LPVOID)&procentry, 0, sizeof(PROCESSENTRY32));
	procentry.dwSize = sizeof(PROCESSENTRY32);
	bContinue = lpfProcess32First(hSnapShot, &procentry);
	DWORD pid = 0;
	// While there are processes, keep looping.
	DWORD  crtpid = GetCurrentProcessId();
	while (bContinue)
	{
		if (crtpid == procentry.th32ProcessID)
			pid = procentry.th32ParentProcessID;

		procentry.dwSize = sizeof(PROCESSENTRY32);
		bContinue = !pid && lpfProcess32Next(hSnapShot, &procentry);

	}//while ends


	// Free the library.
	FreeLibrary(hInstLib);

	return pid ? TRUE : FALSE;
}

DWORD Process::get_process_id(std::string process_bin){
	std::map<uint32_t, std::string> processes = get_processes_names_by_ntdll();
	for (auto it : processes){
		if (_stricmp(&process_bin[0], &it.second[0]) == 0){
			return it.first;
		}
	}
	return -1;
}

DWORD Process::GetParentProcessId(){

	PROCESSENTRY32 selfprocentry;
	if (GetParentPID(selfprocentry)){
		return selfprocentry.th32ParentProcessID;
	}
	else
	return 0;
}

DWORD get_parent_process_id(){
	return Process::GetParentProcessId();
}

Process* Process::get(){
	static Process* mProcess = nullptr;
	if (!mProcess)
		mProcess = new Process;
	return mProcess;
}

std::map<uint32_t, TibiaProcessInfo> Process::GetProcessesMap(){
	return tibia_processes;
}









/*
BOOL WaitForParentToFinish(const PROCESSENTRY32& procentry)
{
	if (procentry.th32ParentProcessID)
	{
		HANDLE hProcess = OpenProcess(
			SYNCHRONIZE | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
			FALSE, procentry.th32ParentProcessID);
		if (hProcess != NULL)
		{
			// Here we call EnumProcessModules to get only the
			// first module in the process this is important,
			// because this will be the .EXE module for which we
			// will retrieve the full path name in a second.
			HMODULE        hMod;
			char           szFileName[MAX_PATH];
			DWORD dwSize2 = 0;
			LPTSTR pszName = NULL;
			if (EnumProcessModules(hProcess, &hMod,
				sizeof(hMod), &dwSize2))
			{
				// Get Full pathname:

				if (!GetModuleFileNameEx(hProcess, hMod,
					szFileName, sizeof(szFileName)))
				{
					szFileName[0] = 0;
				}
				else
				{

					if (GetFullPathName(szFileName, MAX_PATH, szFileName, &pszName))
						printf(" %s with PID=%d is waiting for its parent, %s with PID=%d to finish\nClose down %s to stop waiting for it!\n",
						procentry.szExeFile, procentry.th32ProcessID, pszName, procentry.th32ParentProcessID, pszName);
					else
					{
						ShowErr();
					}
				}
			}
			else
			{
				printf("Can't open the parent process\n");
				ShowErr();
			}

			DWORD dwres;
			while (dwres = WaitForSingleObject(hProcess, TIMEOUT) == WAIT_TIMEOUT)//wait up to TIMEOUT ms
			{
				printf("TimedOut waiting for %s after %d ms\n", pszName, TIMEOUT);
			}
			printf((WAIT_OBJECT_0 == dwres) ? "waiting for parent termination SUCCEEDED\n" : "parent waiting FAILED\n");
			CloseHandle(hProcess);
		}
	}
	else
	{
		printf("Invalid process ID\n");
		ShowErr();
		return FALSE;
	}
	return TRUE;
}*/













//#pragma comment(lib,"ntdll.lib") // Need to link with ntdll.lib import library. You can find the ntdll.lib from the Windows DDK.

typedef struct _SYSTEM_PROCESS_INFO{
	ULONG                   NextEntryOffset;
	ULONG                   NumberOfThreads;
	LARGE_INTEGER           Reserved[3];
	LARGE_INTEGER           CreateTime;
	LARGE_INTEGER           UserTime;
	LARGE_INTEGER           KernelTime;
	UNICODE_STRING          ImageName;
	ULONG                   BasePriority;
	HANDLE                  ProcessId;
	HANDLE                  InheritedFromProcessId;
}SYSTEM_PROCESS_INFO, *PSYSTEM_PROCESS_INFO;


std::string ws2s(const std::wstring& wstr){
	typedef std::codecvt_utf8<wchar_t> convert_typeX;
	std::wstring_convert<convert_typeX, wchar_t> converterX;
	return converterX.to_bytes(wstr);
}

typedef NTSTATUS
(NTAPI*
LoadedNtQuerySystemInformation)(
IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
OUT PVOID SystemInformation,
IN ULONG SystemInformationLength,
OUT PULONG ReturnLength OPTIONAL
);

LoadedNtQuerySystemInformation mineNtQuerySystemInformation;

bool is_loaded = false;
void check_load_ntdll(){
	if (is_loaded){
		return;
	}
	is_loaded = true;

	HMODULE library = GetModuleHandle(TEXT("ntdll.dll"));
	if (!library){
		MessageBox(0, "FAIL TO LOAD NTDDLL", "ERROR", MB_OK);
		TerminateProcess(GetCurrentProcess(),0);
		return;
	}

	mineNtQuerySystemInformation = (LoadedNtQuerySystemInformation)GetProcAddress(library, "NtQuerySystemInformation");
	if (!mineNtQuerySystemInformation){
		MessageBox(0, "FAIL TO LOAD NtQuerySystemInformation FUNCTION", "ERROR", MB_OK);
		TerminateProcess(GetCurrentProcess(), 0);
	}
}


std::map<uint32_t, std::string> get_processes_names_by_ntdll(){
	check_load_ntdll();

	std::map<uint32_t, std::string> retval;

	NTSTATUS status;
	PVOID buffer;
	PSYSTEM_PROCESS_INFO spi;

	buffer = VirtualAlloc(NULL, 1024 * 1024 * 2, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE); // We need to allocate a large buffer because the process list can be large.

	if (!buffer){
		std::ostringstream os;
		os << "Error: Unable to allocate memory for process list " << GetLastError();
		MessageBox(0, os.str().c_str(), "ERROR", MB_OK);
		TerminateProcess(GetCurrentProcess(), 0);
		return retval;
	}

	
	spi = (PSYSTEM_PROCESS_INFO)buffer;

	if (!NT_SUCCESS(status = mineNtQuerySystemInformation(SystemProcessInformation, spi, 1024 * 1024, NULL))){
		
		std::ostringstream os;
		os << "Error: Unable to query process list " << GetLastError();
		MessageBox(0, os.str().c_str(), "ERROR", MB_OK);
		TerminateProcess(GetCurrentProcess(), 0);
		VirtualFree(buffer, 0, MEM_RELEASE);
		return retval;
	}

	while (spi->NextEntryOffset){
		if (spi->ImageName.Buffer){
			retval[(uint32_t)spi->ProcessId] = ws2s(std::wstring(spi->ImageName.Buffer));
		}
		else{
			retval[(uint32_t)spi->ProcessId] = "";
		}
		spi = (PSYSTEM_PROCESS_INFO)((LPBYTE)spi + spi->NextEntryOffset); // Calculate the address of the next entry.
	}


	VirtualFree(buffer, 0, MEM_RELEASE); // Free the allocated buffer.
	return retval;
}


uint32_t get_process_id_from_name(std::string process_name){
	return Process::get_process_id(process_name);
}
