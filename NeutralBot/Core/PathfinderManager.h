#include "Util.h"

enum wall_point_t{
	wall_lure_t,
	wall_battle_t,
	wall_solid_t
};

struct WallPoint{
	WallPoint(wall_point_t _type, Coordinate _coord){
		type = _type;
		coord = _coord;
	}
	wall_point_t type;
	Coordinate coord;
};

class PathfinderManager{
	neutral_mutex mtx_access;
public:
	bool walk_through_players;

	PathfinderManager(){
		walk_through_players = false;
	}

	std::vector<WallPoint> mapwalls_coordinates;


	void lock(){
		mtx_access.lock();
	}

	void unlock(){
		mtx_access.unlock();
	}

	static PathfinderManager* get(){
		static PathfinderManager* mPathfinderManager = nullptr;
		if (!mPathfinderManager)
			mPathfinderManager = new PathfinderManager;
		return mPathfinderManager;
	}
};