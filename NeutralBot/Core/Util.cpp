#include "Util.h"
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include "..\\unzip.h"


void remove_special(std::string& str){
	char special[8] = { '\n', ' ', '\n', '\b', '\v', '\t', '\r', '\0' };
	int special_size = 8;
	for (int i = 0; i < special_size; ++i){
		//tira do comeco;
		while (str.length() > 0){
			if (str[0] == special[i])
				str = str.substr(1, str.length() - 1);
			else
				break;
		}
		//tira do final
		while (str.length() > 0){
			if (str[str.length() - 1] == special[i])
				str = str.substr(0, str.length() - 2);
			else
				break;
		}
	}
}

void switch_num(int &x, int &y)
{
	int buff = x;
	x = y;
	y = buff;
}

bool is_int(std::string entrace)
{
	if (entrace.length() == 0)
	{
		return false;
	}
	int length = entrace.length();
	if (length > 7)
	{
		return false;
	}
	for (int i = 0; i < length; i++)
	{
		if (entrace[i] < 0x30 || entrace[i] > 0x30 + 9)
		{
			return false;

		}
	}
	return true;
}

std::string ms_to_hour(long long x){
	//TODO concertar essa meleca
	if (x <= 0)
		return "00:00:00";

	x = (x / 1000);

	int hours = (int)(x / 60) / 60;
	int minutes = (int)(x / 60) % 60;
	int seconds = (int)(x - (minutes * 60) - (hours * 60 * 60));

	std::string x_ = "";
	if (hours < 10)x_.append("0");
	x_.append(boost::lexical_cast<std::string>(hours));
	x_.append(":");

	if (minutes < 10)x_.append("0");
	x_.append(boost::lexical_cast<std::string>(minutes));
	x_.append(":");

	if (seconds < 10)x_.append("0");
	x_.append(boost::lexical_cast<std::string>(seconds));
	return x_;
}

bool Coordinate::is_null(){
	return x == INT32_MAX && y == INT32_MAX && z == INT8_MAX;
}

Coordinate::Coordinate(COORD coord){
	x = coord.X;
	y = coord.Y;
}

Coordinate::Coordinate() :x(INT32_MAX), y(INT32_MAX), z(INT8_MAX){}
Coordinate::Coordinate(int32_t _x, int32_t _y, int8_t _z) : x(_x), y(_y), z(_z){}
Coordinate::Coordinate(int32_t _x, int32_t _y) : x(_x), y(_y), z(INT8_MAX){}
Coordinate::Coordinate(Point p) : x(p.x), y(p.y), z(INT8_MAX){}
Coordinate::Coordinate(Point p, int32_t z) : x(p.x), y(p.y), z(z){}


Coordinate::Coordinate(std::string coord_as_string){
	std::vector<std::string> strs;
	boost::split(strs, coord_as_string, boost::is_any_of(","));
	int count = strs.size();
	try{
		if (count == 2){
			x = boost::lexical_cast<uint32_t>(strs[0]);
			y = boost::lexical_cast<uint32_t>(strs[1]);
			return;
		}
		else if (count == 3) {
			x = boost::lexical_cast<uint32_t>(strs[0]);
			y = boost::lexical_cast<uint32_t>(strs[1]);
			z = boost::lexical_cast<uint32_t>(strs[2]);
			return;
		}
	}
	catch (std::exception& ex){
		MessageBox(0,&("error on constructor of Coordinate error:" + (std::string)ex.what())[0],"",MB_OK);
	}
	Coordinate::Coordinate();
}

bool Coordinate::is_x_null(){
	return x == INT32_MAX;
}

bool Coordinate::is_y_null(){
	return y == INT32_MAX;
}

bool Coordinate::is_z_null(){
	return z == INT8_MAX;
}

int32_t Coordinate::get_abs_x(){
	if (x >= 0)
		return x;
	return -x;
}

int32_t Coordinate::get_abs_y(){
	if (y >= 0)
		return y;
	return -y;
}

int32_t Coordinate::get_abs_z(){
	if (z >= 0)
		return z;
	return -z;
}


void Coordinate::update_if_null_member(Coordinate _new){
	if (is_x_null())
		x = _new.x;
	if (is_y_null())
		y = _new.y;
	if (is_z_null())
		z = _new.z;
}

Coordinate Coordinate::get_abs_coord(){
	Coordinate retval = *this;
	if (x < 0)
		retval.x = get_abs_x();
	if (y < 0)
		retval.y = get_abs_y();
	if (z < 0)
		z = get_abs_z();
	return retval;
}

Coordinate Coordinate::operator -(Coordinate other){
	Coordinate retval = *this;
	retval.x -= other.x;
	retval.y -= other.y;
	retval.z -= other.z;
	return retval;
}

Coordinate Coordinate::operator +(Coordinate other){
	Coordinate retval = *this;
	retval.x += other.x;
	retval.y += other.y;
	retval.z += other.z;
	return retval;
}

CoordinatePtr Coordinate::get_as_pointer(){
	auto ptr = new Coordinate;
	*ptr = *this;
	return CoordinatePtr(ptr);
}

Coordinate* Coordinate::get(){
	static Coordinate* coordinate = nullptr;
	if (!coordinate)
		coordinate = new Coordinate;
	return coordinate;
}

bool Coordinate::operator !=(Coordinate other){
	return !(*this == other);
}

bool Coordinate::operator ==(Coordinate other){
	/*
	se usarmos pragma pack(push,1),
	poderiamos usar
	return memcmp(this,&other,sizeof(Coordinate)) == 0;
	*/
	if (x != other.x)
		return false;
	if (y != other.y)
		return false;
	if (z != other.z)
		return false;
	return true;
}

bool Coordinate::is_in_near_other(Coordinate other, int x_dist, int y_dist, int z_dist, bool ignore_z){
	if (!ignore_z && abs(other.z - this->z) > z_dist)
		return false;

	Coordinate dist = (*this - other).get_abs_coord();
	if (dist.z > z_dist)
		return false;

	if (dist.x > x_dist || dist.y > y_dist)
		return false;

	return true;
}

uint32_t Coordinate::get_axis_max_dist(Coordinate other){
	Coordinate result = *this - other;
	result = result.get_abs_coord();
	return std::max(result.x, result.y);
}

uint32_t Coordinate::get_axis_min_dist(Coordinate other){
	Coordinate result = *this - other;
	result = result.get_abs_coord();
	return std::min(result.x, result.y);
}

uint32_t Coordinate::get_total_distance(Coordinate other){
	return (get_axis_max_dist(other) + get_axis_min_dist(other));
}

Point Coordinate::to_point(){
	return Point(x, y);
}

Rect::Rect() :x(INT32_MAX), y(INT32_MAX), width(INT32_MAX), height(INT32_MAX){
	normalize();
}
Rect::Rect(int32_t _x, int32_t _y, int32_t _width, int32_t _height) :
x(_x), y(_y), width(_width), height(_height){
	normalize();
}


bool Rect::is_null(){
	return (x == INT32_MAX && y == INT32_MAX && width == INT32_MAX && height == INT32_MAX);
}

Point Rect::get_center(){
	return Point(x + (width ? (width / 2): 0), y + (height ? (height / 2) : 0));
}

Rect Rect::operator + (Point point){
	return Rect(x + point.x, y + point.y, width, height);
}

uint32_t Rect::get_x(){
	return x;
}

uint32_t Rect::get_y(){
	return y;
}

uint32_t Rect::get_width(){
	return width;
}

uint32_t Rect::get_height(){
	return height;
}

void Rect::set_x(uint32_t new_value){
	x = new_value;
}

void Rect::set_y(uint32_t new_value){
	y = new_value;
}

void Rect::set_width(uint32_t new_value){
	width = new_value;
	normalize();
}

void Rect::set_height(uint32_t new_value){
	height = new_value;
	normalize();
}

void Rect::normalize(){
	if (width < 0){
		width = (-width);
		x -= width;
	}

	if (height < 0){
		height = (-height);
		y -= height;
	}
}




Json::Value loadJsonFile(std::string file) {
	std::ifstream jsonGet;
	Json::Value root;
	Json::Reader reader;

	jsonGet.open(file);
	reader.parse(jsonGet, root);
	jsonGet.close();

	return root;
}

bool saveJsonFile(std::string file, Json::Value json) {
	std::ofstream jsonSet;

	jsonSet.open(file);
	if (!jsonSet.is_open())
		return false;

	jsonSet << json.toStyledString();
	std::string temp = json.toStyledString();
	jsonSet.close();
	return true;
}


void Range::update_is_nill(){
	_is_nill = (min_x == INT32_MAX && min_y == INT32_MAX && min_z == INT32_MAX)
		|| (max_x == INT32_MAX && max_x == INT32_MAX && max_z == INT32_MAX);
}

Range::Range(int32_t _min_x, int32_t _min_y, int32_t _min_z,
	int32_t _max_x, int32_t _max_y, int32_t _max_z) :
	min_x(_min_x), min_y(_min_y), min_z(_min_z),
	max_x(_max_x), max_y(_max_y), max_z(_max_z){
	update_is_nill();
}

Range::Range(Rect& rect) : min_x(rect.get_x()), min_y(rect.get_y()),
max_x(rect.get_x() + rect.get_width()), max_y(rect.get_y() + rect.get_height()),
max_z(INT32_MAX), min_z(INT32_MAX){
}

bool Range::is_null(){
	return _is_nill;
}

void Range::set_min_x(int32_t x){
	min_x = x;
	update_is_nill();
}

int32_t Range::get_min_x(){
	return min_x;
}

void Range::set_min_y(int32_t y){
	min_y = y;
	update_is_nill();
}

int32_t Range::get_min_y(){
	return min_y;
}

void Range::set_min_z(int32_t z){
	min_z = z;
	update_is_nill();
}

int32_t Range::get_min_z(){
	return min_z;
}

void Range::set_max_x(int32_t x){
	max_x = x;
	update_is_nill();
}

int32_t Range::get_max_x(){
	return max_x;
}

void Range::set_max_y(int32_t y){
	max_y = y;
	update_is_nill();
}

int32_t Range::get_max_y(){
	return max_y;
}

void Range::set_max_z(int32_t z){
	max_z = z;
	update_is_nill();
}

int32_t Range::get_max_z(){
	return max_z;
}

bool Range::is_null_range_z(){
	return (min_z == INT32_MAX || max_z == INT32_MAX);
}

bool Range::is_null_range_x(){
	return (min_x == INT32_MAX || max_x == INT32_MAX);
}

bool Range::is_null_range_y(){
	return (min_y == INT32_MAX || max_y == INT32_MAX);
}


bool Range::is_in_range(Point pt){
	if (!is_null_range_x())
		if (pt.x < min_x || pt.x >= max_x)
			return false;
	if (!is_null_range_y())
		if (pt.y < min_y || pt.y >= max_y)
			return false;
	return true;
}
bool Range::is_in_range(Coordinate coord){
	if (!is_null_range_x())
		if (coord.x < min_x || coord.x >= max_x)
			return false;
	if (!is_null_range_y())
		if (coord.y < min_y || coord.y >= max_y)
			return false;
	if (!is_null_range_z())
		if (coord.z < min_z || coord.z >= max_z)
			return false;
	return true;
}


std::pair<char*, uint32_t> load_file_data(std::string path){
	FILE *f = nullptr;
	errno_t error = fopen_s(&f, &path[0], "rb");
	if (!f || error)
		return std::pair<char*, uint32_t>(0, 0);

	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);
	if (!fsize){
		fclose(f);
		return std::pair<char*, uint32_t>(0, 0);
	}

	unsigned char* buffer = new unsigned char[fsize];

	int count = fread(buffer, 1, fsize, f);
	if (fsize != count){
		fclose(f);
		delete[]buffer;
		return std::pair<char*, uint32_t>(0, 0);
	}
	fclose(f);
	return std::pair<char*, uint32_t>((char*)buffer, fsize);
}

std::map<std::string, std::pair<char*, uint32_t>> get_unziped_files(std::string file){
	std::map<std::string, std::pair<char*, uint32_t>> retval;
	HZIP hz;
	hz = OpenZip(&file[0], 0);
	if (!hz)
		return retval;
	ZIPENTRY ze;
	GetZipItem(hz, -1, &ze);
	int numitems = ze.index;
	for (int zi = 0; zi < numitems; zi++){
		if (GetZipItem(hz, zi, &ze) != ZR_OK){
			
			continue;
		}


		if (ze.unc_size <= 0){
			
			continue;
		}


		char* data = new char[ze.unc_size];
		if (UnzipItem(hz, zi, (void *)data, ze.unc_size) != ZR_OK){
			
			delete[]data;
		}
		else{

			if (file.find("gifdat") != std::string::npos) {
				std::string temp_name = ze.name;
				std::replace(temp_name.begin(), temp_name.end(), '_', ' ');
				retval[temp_name] = std::pair<char*, uint32_t>(data, ze.unc_size);
			}
			else
				retval[ze.name] = std::pair<char*, uint32_t>(data, ze.unc_size);
		}
	}

	CloseZip(hz);
	return retval;
}

std::string GetDocummentsDir(){
	CHAR my_documents[MAX_PATH];
	ZeroMemory(my_documents, MAX_PATH);
	HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

	if (result != S_OK)
		return "";
	else
		return std::string(&my_documents[0]);
}

uint32_t Util::generateHash(void *data, size_t len){
	uint32_t hash = 0;
	for (size_t i = 0; i < len; ++i)
		hash = 65599 * hash + ((unsigned char*)data)[i];
	return hash ^ (hash >> 16);
}


bool Util::circle_contains_point(Rect& rect, Point& point){
	if (rect.Contains(point)){
		double dx = rect.get_x() + rect.get_width() / 2 - point.y;
		double dy = rect.get_y() + rect.get_height() / 2 - point.x;
		dx *= dx;
		dy *= dy;
		double distanceSquared = dx + dy;
		double radiusSquared = rect.get_width() / 2;
		return distanceSquared <= radiusSquared;
	}
	return false;
}

bool Util::rect_intersect_circle(Rect& rect, Point& circle, uint32_t radius){

	Point circleDistance;
	circleDistance.x = abs(circle.x - (int32_t)rect.get_x());
	circleDistance.y = abs(circle.y - (int32_t)rect.get_y());

	if ((uint32_t)circleDistance.x > (rect.get_width() / 2 + radius)) {
		return false; 
	}

	if ((uint32_t)circleDistance.y > (rect.get_height() / 2 + radius)) {
		return false; 
	}

	if ((uint32_t)circleDistance.x <= (rect.get_width() / 2)) {
		return true; 
	}
	if ((uint32_t)circleDistance.y <= (rect.get_height() / 2)) {
		return true; 
	}

	float cornerDistance_sq = (float)(((uint32_t)circleDistance.x - rect.get_width() / 2) ^ 2 +
		((uint32_t)circleDistance.y - rect.get_height() / 2) ^ 2);

	return (cornerDistance_sq <= (radius ^ 2));
}


bool Util::rect_contains_point(Rect& rect_parent, Point& point){
	return (uint32_t)point.x >= rect_parent.get_x() && (uint32_t)point.x <= rect_parent.get_x() + rect_parent.get_width() &&
		(uint32_t)point.y >= rect_parent.get_y() && (uint32_t)point.y <= rect_parent.get_y() + rect_parent.get_height();
}

bool Util::line_intersect_line(Point& l1p1, Point& l1p2, Point& l2p1, Point& l2p2){
	float q = (float)((l1p1.y - l2p1.y) * (l2p2.x - l2p1.x) - (l1p1.x - l2p1.x) * (l2p2.y - l2p1.y));
	float d = (float)((l1p2.x - l1p1.x) * (l2p2.y - l2p1.y) - (l1p2.y - l1p1.y) * (l2p2.x - l2p1.x));

	if (d == 0)
		return false;
	float r = q / d;

	q = (float)((l1p1.y - l2p1.y) * (l1p2.x - l1p1.x) - (l1p1.x - l2p1.x) * (l1p2.y - l1p1.y));
	float s = q / d;

	if (r < 0 || r > 1 || s < 0 || s > 1)
		return false;

	return true;
}

bool Util::line_intersect_rect(Point& p1, Point& p2, Rect& r){
	return line_intersect_line(p1, p2, Point(r.get_x(), r.get_y()), Point(r.get_x() + r.get_width(), r.get_y())) ||
		line_intersect_line(p1, p2, Point(r.get_x() + r.get_width(), r.get_y()), Point(r.get_x() + r.get_width(), r.get_y() + r.get_height())) ||
		line_intersect_line(p1, p2, Point(r.get_x() + r.get_width(), r.get_y() + r.get_height()), Point(r.get_x(), r.get_y() + r.get_height())) ||
		line_intersect_line(p1, p2, Point(r.get_x(), r.get_y() + r.get_height()), Point(r.get_x(), r.get_y())) ||
		(r.Contains(p1) && r.Contains(p2));
}

bool Util::rect_intersect_rect(Rect& rect, Rect& rectother){
	std::vector<std::pair<Point, Point>> rect_lines;
	std::vector<std::pair<Point, Point>> rect_other_lines;

	if (rect.Contains(rectother.get_center()))
		return true;

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x(), rect.get_y()),
		Point(rect.get_x() + rect.get_width(), rect.get_y())));

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x() + rect.get_width(), rect.get_y()),
		Point(rect.get_x() + rect.get_width(), rect.get_y() + rect.get_height())));

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x() + rect.get_width(), rect.get_y() + rect.get_height()),
		Point(rect.get_x(), rect.get_y() + rect.get_height())));

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x(), rect.get_y() + rect.get_height()),
		Point(rect.get_x(), rect.get_y())));


	rect_other_lines.push_back(std::pair<Point, Point>(
		Point(rectother.get_x(), rectother.get_y()),
		Point(rectother.get_x() + rectother.get_width(), rectother.get_y())));

	rect_other_lines.push_back(std::pair<Point, Point>(
		Point(rectother.get_x() + rectother.get_width(), rectother.get_y()),
		Point(rectother.get_x() + rectother.get_width(), rectother.get_y() + rectother.get_height())));

	rect_other_lines.push_back(std::pair<Point, Point>(
		Point(rectother.get_x() + rectother.get_width(), rectother.get_y() + rectother.get_height()),
		Point(rectother.get_x(), rectother.get_y() + rectother.get_height())));

	rect_other_lines.push_back(std::pair<Point, Point>(
		Point(rectother.get_x(), rectother.get_y() + rectother.get_height()),
		Point(rectother.get_x(), rectother.get_y())));

	for (auto lrect : rect_lines){
		for (auto lrectother : rect_other_lines){
			if (line_intersect_line(lrect.first, lrect.second, lrectother.first, lrectother.second))
				return true;
		}
	}
	return false;
}



bool Rect::is_valid(){
	if (width <= 0 || height <= 0)
		return false;
	return true;
}

bool Rect::Contains(Point& point){
	return Util::rect_contains_point(*this, point);
}

Point::Point(Rect rect){
	x = rect.get_x();
	y = rect.get_y();
}

bool Point::is_null(){
	return x == 0 && y == 0;
}

Point Point::operator+(Point other){
	return Point(x + other.x, y + other.y);
}