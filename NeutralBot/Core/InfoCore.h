#pragma once
#include "Messages.h"
#include <regex>
#include <iostream> 
#include "Inventory.h"
#include "ItemsManager.h"
#include "Time.h"
#include "LooterCore.h"

enum item_action_message_t{
	item_action_buy,
	item_action_sell,
	item_action_total
};

struct ItensUsed{
	std::string name;
	uint32_t item_used;
};

struct MonsterKilled{
	std::string name;
	uint32_t qty;
};

struct ItemsBuySellInfo{
	uint32_t count;
	uint32_t value;
	uint32_t name;
	item_action_message_t action;	

public:
	DEFAULT_GET_SET(uint32_t, count);
	DEFAULT_GET_SET(uint32_t, value);
	DEFAULT_GET_SET(uint32_t, name);
	DEFAULT_GET_SET(item_action_message_t, action);
};

enum warning_message_type{
	warning_message_you_cannot_use,
	warning_message_sorry_not_possible,
	warning_message_you_are_not_the_owner,
	warning_message_first_go_upstairs,
	warning_message_there_is_no_way,
	warning_message_you_may_first_go_downstairs,
	warning_message_you_may_no_attack_this_person,
	warning_message_you_cannot_move_this_object,
	warning_message_there_is_not_enough_room,
	warning_message_is_too_heavy_to_carry,
	warning_message_total_type
};

struct WarningMessage{
	neutral_mutex mtx_access;
	uint64_t count = 0;
	warning_message_type type;
	std::string text;
	TimeChronometer last_update;

	uint64_t get_last_refresh_timeout();

	void refresh_timeout();
	
	std::string get_text();

	void set_text(std::string new_text);

	static warning_message_type get_message_type_from_text(std::string& text);
};

#pragma pack(push,1)
class InfoCore{
	neutral_mutex mtx_access;
	uint32_t last_balance;
	uint32_t last_ammunation_ammount;
	uint32_t last_weapon_ammount;
	std::vector<WarningMessage> warnings_messages_information;
	std::map<std::string, TimeChronometer> players_attacking;
	std::map<uint32_t, std::shared_ptr<ItemsBuySellInfo>> map_items_buy_sell;
	uint32_t player_experience = 0;
	std::vector<std::string> tibia_messages_vector;
	uint32_t tibia_messages_vector_limit = 500;
	
	void parse_action_message(std::string& message_in);
	void parse_player_attack_string(std::string& str);
public:

	std::vector<std::string> get_tibia_messages_vector(){
		return tibia_messages_vector;
	}

	void add_tibia_messages_vector_message(std::string message){
		uint32_t vector_size = tibia_messages_vector.size();
		if (vector_size >= tibia_messages_vector_limit){			
			tibia_messages_vector.push_back(message);
			tibia_messages_vector.erase(tibia_messages_vector.begin());
		}
		else{
			tibia_messages_vector.push_back(message);
		}

	}

	void set_tibia_messages_vector_limit(uint32_t limit){
		mtx_access.lock();
		tibia_messages_vector_limit = limit;
		mtx_access.unlock();
	}
	
	uint32_t get_player_experience(){
		return player_experience;
	}
	void set_player_experience(uint32_t in){
		player_experience = in;
	}

	uint32_t expgained(){
		return TibiaProcess::get_default()->character_info->experience() - player_experience;
	}

	std::map<uint32_t,std::pair<uint32_t, uint32_t>> itensusedmap;
	std::map<std::string, uint32_t> monsterkilledmap;
	std::map<std::string, TimeChronometer> players_attack;
	TimeChronometer time;
	std::vector<uint32_t> ping_vector;


	std::map<uint32_t, std::pair<uint32_t, uint32_t>> get_itens_used_map();
	std::map <std::string, uint32_t> get_monster_killed_map();
	std::vector<int> vector_profit_hour;
	std::vector<int> vector_exp_hour;

	InfoCore();

	WarningMessage* get_warning_message_info(warning_message_type type);

	uint32_t get_item_used_count(uint32_t itemname);

	uint32_t get_last_balance();
	
	uint32_t exp_hour(){
		return TibiaProcess::get_default()->character_info->exp_hour();
	}

	uint32_t stamina(){
		return TibiaProcess::get_default()->character_info->stamina();
	}

	int profit_hour(){
		lock_guard _lock(mtx_access);
		int total_value_looted = 0;
		int total_value_used = 0;

		auto map_items_looted = LooterCore::get()->get_map_items_looted();
		if (!map_items_looted.size())
			return 0;

		for (auto item_looted : map_items_looted){
			if (!item_looted.second)
				continue;

			total_value_looted = total_value_looted + (item_looted.second->count * item_looted.second->sell_price);
		}

		auto map_items_used = InfoCore::get()->get_itens_used_map();
		for (auto item_used : map_items_used){
			auto item_info_rule = LooterCore::get()->getRuleItemsLooted(item_used.first);
			if (!item_info_rule)
				continue;

			total_value_used = total_value_used + (item_used.second.first * item_info_rule->buy_price);
		}

		if (InfoCore::get()->get_info_time_bot_running() <= 0)
			return 0;

		int total_value = total_value_looted - total_value_used;
		int total_value_end = 0;

		if (total_value > 0)
			total_value_end = (total_value / ((InfoCore::get()->get_info_time_bot_running() / 100) / 60)) * 60;

		if (total_value <= 0)
			total_value_end = 0;

		return total_value_end;
	}

	void run_att_info();

	void refresh_item_buy_and_sell(std::string& messagee);

	void refresh_player_attacking_you(std::string& messagee);

	bool go_to_first_go_upstairs(Coordinate coord);

	bool is_player_attack();

	void add_item_on_used(uint32_t itemname, uint32_t qty = 0);

	void add_item_on_used_bolt(uint32_t itemname, uint32_t qty);

	void update_last_balance(uint32_t value);

	int get_info_time_bot_running();

	void add_ping_in_vector(int ping);

	int get_last_medium_ping();

	int get_current_time_total_in_day();

	void add_monster_killed(std::string monstername);

	void refresh_items_used(std::string& messagee);

	void refresh_monster_killed(std::string& messagee);

	void refresh_last_balance_messages(std::string& messagee);

	void weapon_and_ammunation_wasted();

	std::vector<std::string> get_players_attacking();

	static InfoCore* get();

	void notify(std::string message_in, std::string title = "Notification", uint32_t timeout_seconds = 5);
};
#pragma pack(pop)