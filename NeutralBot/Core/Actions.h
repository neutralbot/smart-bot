#pragma once
#include "Input.h"
#include "ContainerManager.h"
#include "DepoterCore.h"
#include "Inventory.h"
#include "Map.h"
#include "Hotkey.h"

class BaseActions{
	static neutral_mutex mtx_access;
public:
	static action_retval_t move_item_container_to_coordinate(ContainerPosition container_location, Coordinate coord, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);
	static action_retval_t move_item_container_to_container(ContainerPosition container_source, ContainerPosition container_location, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);
	static action_retval_t move_item_container_to_slot(ContainerPosition container_source, inventory_slot_t slot, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);

	static action_retval_t move_item_coordinate_to_container(Coordinate coord, ContainerPosition container_location, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);
	static action_retval_t move_item_coordinate_to_slot(Coordinate coord, inventory_slot_t slot, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);
	static action_retval_t move_item_coordinate_to_coordinate(Coordinate coord, Coordinate coord_target, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);

	static action_retval_t move_item_slot_to_slot(inventory_slot_t slot, inventory_slot_t slot_dest, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);
	static action_retval_t move_item_slot_to_coordinate(inventory_slot_t slot, Coordinate coord, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);
	static action_retval_t move_item_slot_to_container(inventory_slot_t slot, ContainerPosition container_location, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX);

	static action_retval_t use_item_on_inventory(inventory_slot_t slot, uint32_t item_id = 0/*ignore item id*/);
	static action_retval_t use_item_on_container(ContainerPosition container_location, uint32_t item_id = UINT32_MAX/*ignore item id*/);
	static action_retval_t use_item_on_coordinate(Coordinate coordinate = Coordinate(), uint32_t item_id = UINT32_MAX/*ignore item id*/);

	static action_retval_t use_crosshair_item_on_inventory(inventory_slot_t slot, uint32_t item_id = UINT32_MAX/*ignore item id*/);
	static action_retval_t use_crosshair_item_on_coordinate(Coordinate coord, uint32_t item_id = UINT32_MAX/*ignore item id*/);
	static action_retval_t use_crosshair_item_on_container(ContainerPosition container_location, uint32_t item_id = UINT32_MAX/*ignore item id*/);

	static action_retval_t use_item_on_to_coodinate(uint32_t item_id, Coordinate dest);
	static action_retval_t use_item_on_to_invetory(uint32_t item_id, inventory_slot_t slot_type);
	static action_retval_t use_item_on_to_container(ContainerPosition container_location, uint32_t item_id = UINT32_MAX);
	static std::shared_ptr<lock_guard> lock_scope();
};

class Actions : public BaseActions{
public:
	static const int Max_Count = 3;

	static std::shared_ptr<Actions> get();

	static action_retval_t goto_coord(Coordinate coord, uint32_t consider_near = 1, map_type_t map_type = map_front_mini_t,
		pathfinder_state& state_out = pathfinder_state_placeholder, bool consider_target_walkable = false, std::function<bool()> Stopfunction = []() { return true; });
	static action_retval_t goto_coord_on_screen(Coordinate coord, uint32_t consider_near = 1,
		bool map_front = true, pathfinder_state& state_out = pathfinder_state_placeholder, bool consider_target_walkable = false);
	static action_retval_t goto_near_coord(Coordinate coord, bool go_under_sqm = false, uint32_t consider_near = 1,
		map_type_t map_type = map_front_mini_t, uint32_t wait_ms = 700,
		bool monst_near_from_center = true, pathfinder_state& state_out = pathfinder_state_placeholder, bool consider_target_walkable = false, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t simple_go_coord_on_screen(Coordinate coord, uint32_t consider_near = 1, bool map_front = true,
		pathfinder_state& state_out = pathfinder_state_placeholder, bool consider_target_walkable = false);

	static CoordinatePtr get_coord_near_other(Coordinate other, bool check_reachable = true, map_type_t map_type = map_front_mini_t,
		uint32_t consider_near = 1);
	
	static action_retval_t clean_coordinate(Coordinate coord, int items_count = 0, bool use_browse_field = true);

	static action_retval_t use_rope_on_coordinate(Coordinate coord);

	//static action_retval_t hole_or_step(Coordinate coord);

	static action_retval_t open_thing_with_item_and_step(Coordinate coord, int item_id, int startId = 0, int endId = 0);

	static action_retval_t open_hole_with_shovel(Coordinate coord, int start_item_id = 0, int end_item_id = 0, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t open_hole_with_pick(Coordinate coord, int start_item_id = 0, int end_item_id = 0, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t open_thing_with_machet(Coordinate coord, std::function<bool()> Stopfunction = []() { return false; });
		
	static action_retval_t open_hole_with_scythe(Coordinate coord, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t open_hole_with_tool_id(Coordinate coord, uint32_t tool_id, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t change_weapon(uint32_t weapon);
	
	static action_retval_t hole_or_step(Coordinate coord, side_t::side_t side);

	static action_retval_t goto_nearest_from_creature(std::string name);

	static action_retval_t up_or_down_on_thing_by_coord(Coordinate coord);

	static action_retval_t up_ladder(Coordinate coord);

	static action_retval_t use_lever(Coordinate coord, Coordinate offset, int start_item_id = 0, int end_item_id = 0);

	static action_retval_t use_item_id_to_down_or_up(Coordinate coord, int item_id = 0);

	static action_retval_t join_in_telepot(Coordinate coord, int position_distance = 5);

	static action_retval_t clear_coord_to_loot(Coordinate coord);

	static action_retval_t check_move_item_container_to_container(ContainerPosition container_source, ContainerPosition container_location, uint32_t count = 0, uint32_t check_item_id = UINT32_MAX, bool wait = true);

	static action_retval_t dropitem(uint32_t item_id, Coordinate coord = Coordinate(), uint32_t count = 100, bool fast_drop = true);

	static action_retval_t destory_field(Coordinate coord);

	static action_retval_t pick_up_item(uint32_t item_id, uint32_t count, uint32_t destination_container_id);

	static action_retval_t fish_process();

	static action_retval_t use_fishing_rod(Coordinate coord);

	static action_retval_t execute_dance();

	static action_retval_t eat_food(uint32_t container_index = UINT32_MAX/*any*/,std::string nameFood = "");

	static action_retval_t reopen_containers(bool closeAll = false, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t up_backpack(std::string backpack);

	static action_retval_t up_backpack(int index);
	
	static action_retval_t says(std::string text, std::string channel = "Default");

	static action_retval_t says_to_npc(std::string text, std::string npc_name = "");

	static action_retval_t loggout();

	static action_retval_t Actions::open_door(Coordinate door_location, uint32_t key_id);

	static action_retval_t turn(side_nom_axis_t::side_nom_axis_t side, int32_t timeout = 1000);

	static action_retval_t step_side(side_t::side_t side, int32_t timeout = 0);

	static action_retval_t refresh_profession_stuff();

	static action_retval_t open_container_at_coordinate(Coordinate coord, int32_t timeout = 1000, int32_t stack_pos = -1, bool corpses = false, bool check_upstair = false, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t open_main_bp(int32_t timeout = 1000);

	static action_retval_t open_container_id(uint32_t item_id, bool unique = true, int32_t timeout = 1000);
	
	static action_retval_t open_container_while_full(uint32_t item_id, int32_t timeout = 2000);

	static action_retval_t open_container_while_hast_next(uint32_t item_id, int32_t timeout = 2000);
	
	static action_retval_t mantain_container_count(uint32_t item_id, uint32_t count);

	static action_retval_t use_item(uint32_t item_id, bool resquest_lock = true);

	static action_retval_t set_follow();

	static action_retval_t unset_follow();

	static action_retval_t reach_creature(uint32_t cid);

	static action_retval_t reach_creature(std::string creatureName, int consider_near = 1);

	static action_retval_t use_exani_hur(exani_hur_t up_or_down, Coordinate location, side_nom_axis_t::side_nom_axis_t side, bool forcemove = false);

	static action_retval_t reach_nearest_to_creature(uint32_t cid);

	static action_retval_t drop_empty_potions();

	static action_retval_t reach_nearest_to_coordinate(Coordinate coord);

	static action_retval_t move_creature(Coordinate location, bool verify_creature = true);

	static action_retval_t deposit_all();

	static action_retval_t transfer_money(std::string NameChar, int value);

	static action_retval_t deposit_money(int money);

	static action_retval_t travel(std::string city, std::string npc_name = "");

	static action_retval_t refill_weap_amun_process(std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t refill_weap_amun_process_by_id(uint32_t temp_id, std::function<bool()> Stopfunction = []() { return false; });

	static action_retval_t withdraw_value(int value, bool depositall = false, int delay = 1000);

	static action_retval_t take_up_items_from_depot(int depotBackpackId, int destinyBackpackId, int item_id, int itemCount);

	static action_retval_t cancel_attack_chase();

	static action_retval_t sort_containers_content(std::map< uint32_t /*item_id*/, uint32_t/*container id*/>& containers_items_set, uint32_t timeout = 10000);

	static action_retval_t try_sort_containers_content(std::map< uint32_t /*item_id*/, uint32_t/*container id*/>& containers_items_set, uint32_t timeout);

	static action_retval_t drop_item_from_containers(std::vector<uint32_t> containers_id, std::vector<uint32_t> item_ids
		, int32_t timeout, bool fast_drop = true);

	static action_retval_t try_drop_item_from_containers(std::vector<uint32_t> containers_id, std::vector<uint32_t> item_ids,
		int32_t timeout, bool fast_drop = true);

	static action_retval_t set_talk_channel(std::string channel);
};

