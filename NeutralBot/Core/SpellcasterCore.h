#pragma once
#include "..\\XMLSpellManager.h"
#include "..\\SpellManager.h"
#include "constants.h"
#include "Time.h"

#define can_spell(type)\
	can_##type(condition_rule);

#define execute_action(type)\
	execute_##type(condition_rule);

class SpellCasterCore{
	TimeChronometer timer_waypoint;
	TimeChronometer timer_hunter;
	TimeChronometer timer_looter;
	TimeChronometer timer_spells;

	uint32_t current_creature_value = 0;
	bool fast_target_needs_work = false;
	bool fast_looter_needs_work = false;

	bool var_need_waypoint = false;
	bool var_need_looter = false;
	bool var_need_hunter = false;
	bool var_need_spells = false;
	
	uint32_t fast_hppc = 0;
	uint32_t fast_mppc = 0;
	uint32_t fast_hp = 0;
	uint32_t fast_mp = 0;
	std::shared_ptr<SpellBase> last_spell_base;
	uint32_t fast_target_id = 0;
	std::string fast_target_name = "";
	bool fast_player_on_screen = false;

	void update_fast_vars();

	uint32_t default_spell_spam = 20;

	static void init();
	static SpellCasterCore* mSpellCasterCore;

	int total_spell_in_use;

	TimeChronometer time_item;
	TimeChronometer time_attack;
	TimeChronometer time_healing;
	TimeChronometer time_healing_item;

	std::vector<int> spell_in_use;
	std::map<int, TimeChronometer> itens_in_use;
	std::map<std::string, TimeChronometer> custon_spell_in_use;
	std::map<int, bool> spell_types_in_use;
	bool use_spell_pc = false;
	neutral_mutex mtx_access;
	TimeChronometer last_try_cast_health_spell;
	TimeChronometer last_try_cast_attack_spell;

	void upate_last_try_to_use_health_spell();
	void upate_last_try_to_use_attack_spell();
	bool attack_spell_can_spam();
	bool health_spell_can_spam();


public:
	static SpellCasterCore* get() {
		if (!mSpellCasterCore)
			init();
		return mSpellCasterCore;
	}
	
	bool spell_match_conditions(SpellBase* spell_ptr);
	SpellCasterCore();
	void refresh_spell_cooldown();
	void refresh_basics_spell_cooldown();

	bool can_cast_spell(std::string words);
	bool can_cast_item(int item_id, int cooldownTime);
	bool can_cast_custom_spell_name(std::string spell_name, int cooldownTime);
	bool can_cast_spell_by_cooldown_id(int spell_id);
	
	bool cast_words(std::string words);
	
	bool execute_action_temp(std::shared_ptr<SpellCondition> condition,spell_actions_t type){
		bool temp = false;
		std::shared_ptr<SpellCondition>  condition_rule = condition;
		if (!condition_rule)
			return false;

		switch (type){
		case spell_actions_cast:{
									return true;
		}
			break;
		case spell_actions_no_cast:{
									return false;	
		}
			break;
		case spell_actions_do_waypoint_wait:{
												temp = execute_action(spell_actions_do_waypoint_wait); 
												return false;
		}
			break; 		
		case spell_actions_do_looter_wait:{
											  temp = execute_action(spell_actions_do_looter_wait);
											  return false;
		}
			break;
		case spell_actions_do_hunter_wait:{
											  temp = execute_action(spell_actions_do_hunter_wait); 
											  return false;
		}
			break;
		default:
			break;
		}
		return false;
	}

	bool can_spell_condition_none(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_trapped(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_not_looting(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_not_hunting(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_use_life_bellow_value(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_use_mana_bellow_value(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_use_life_percent_bellow_value(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_use_mana_percent_bellow_value(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_if_has_target(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_if_target_name(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_need_to_cast(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_not_if_target(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_not_if_target_name(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_force_if_player_on_screen(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_not_if_player_on_screen(std::shared_ptr<SpellCondition> condition);
	bool can_spell_condition_require_target_pulsed(std::shared_ptr<SpellCondition> condition);
	
	bool execute_spell_actions_none(std::shared_ptr<SpellCondition> condition);
	bool execute_spell_actions_cast(std::shared_ptr<SpellCondition> condition);
	bool execute_spell_actions_just_cast(std::shared_ptr<SpellCondition> condition);
	bool execute_spell_actions_do_waypoint_wait(std::shared_ptr<SpellCondition> condition);
	bool execute_spell_actions_do_looter_wait(std::shared_ptr<SpellCondition> condition);
	bool execute_spell_actions_do_hunter_wait(std::shared_ptr<SpellCondition> condition);

	bool check_spell_condition(int spell_condition);
	bool is_custom_spell(std::string words);

	void make_show_cooldown_bar();

	void cast_spell();

	std::vector<std::shared_ptr<SpellHealth>> get_health_spell_to_cast();
	void cast_health_spell(std::shared_ptr<SpellHealth> spell);
	std::shared_ptr<SpellAttack> get_attack_spell_to_cast(Coordinate& out_cast_coord);
	void cast_attack_spell(std::shared_ptr<SpellAttack> spell, Coordinate& coord);
	Coordinate get_best_pos_to_send_item(std::shared_ptr<SpellAttack> spellAttack);

	bool verify_shot_rune(Coordinate posToCast, int areaSize, int minMonsterInArea, int maxMonsterInArea, std::map<int, std::shared_ptr<MonsterInfo>>& monsterList);
	bool can_execute();

	bool need_operate_spells();
	bool need_operate_hunter();
	bool need_operate_looter();
	bool need_operate_waypoint();

	void executor();
};
