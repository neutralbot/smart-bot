#pragma once
#include "TibiaProcess.h"
#include "Interface.h"
#include "Process.h"
#include "Input.h"
#include "InfoCore.h"
#include <direct.h>
#include "..\\UtilityCore.h"
#include "HunterCore.h"
#include "..\\WindowsNetworkHelper.h"
#include "FloatingMiniMenu.h"
#include "..\\HUDManager.h"
#include "..\\MonsterManager.h"
#include "..\\DevelopmentManager.h"
#include "..\\GifManager.h"
#include <stdio.h>  /* defines FILENAME_MAX */
#include <boost\filesystem.hpp>
#include <thread>

TibiaProcess::TibiaProcess() {
	is_logged = false;
	attached = false;

	process_handler = 0;
	ping_promedy = 100;

	character_info = std::shared_ptr<CharacterInfo>(new CharacterInfo);
	character_info->set_owner(this);

	std::thread([&](){
		MONITOR_THREAD("TibiaProcess::refresh_name")
			Sleep(2000);
		refresh_name(); }).detach();
}

void TibiaProcess::update_base_address(){
	if (pid)
		base_address = Process::get()->GetModuleBase("Tibia.exe", pid);
}

std::string TibiaProcess::get_name_char(){
	if (!get_is_logged())
		return "Offline";

	uint32_t current_player_id = character_info->character_id();
	CreatureOnBattle curr_character;

	for (int i = 0; i < 100; i++){
		uint32_t address_battle = AddressManager::get()->getAddress(ADDRESS_BATTLE_FIRST_ID);
		TibiaProcess::get_default()->read_memory_block(address_battle
			+ (i * sizeof(CreatureOnBattle)), &curr_character, sizeof(CreatureOnBattle));// != sizeof(CreatureOnBattle);

		if (character_info->name == curr_character.name)
			return character_info->name;

		if (curr_character.id == current_player_id)
			break;
	}

	if (curr_character.id != current_player_id)
		return "Offline";

	return curr_character.name;
}

void TibiaProcess::kill_connect(){
	if (pid)
		killProcessConnections(pid);
}
void TibiaProcess::restore_window(){
	ShowWindow((HWND)window_handler, SW_RESTORE);
}

void TibiaProcess::minimize_window(){
	ShowWindow((HWND)window_handler, SW_MINIMIZE);
}

void TibiaProcess::hide_window(){
	ShowWindow((HWND)window_handler, SW_HIDE);
}

void TibiaProcess::show_window(){
	ShowWindow((HWND)window_handler, SW_SHOW);
}

void TibiaProcess::on_change_coordinate(Coordinate& old_coord, Coordinate& new_coord){
	for (auto it : on_change_coordinate_callbacks){
		if (!it)
			continue;

		it(old_coord, new_coord);
	}
}

void TibiaProcess::add_on_change_coordinate_callback(
	std::function < void(Coordinate&/*last_coord*/, Coordinate&/*new_coord*/) >
	callback_function){
	on_change_coordinate_callbacks.push_back(callback_function);
}

std::shared_ptr<TibiaProcess> TibiaProcess::default_TibiaProcess;

CharacterInfo::CharacterInfo(){
	
}

uint32_t CharacterInfo::get_id_to_move(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_ITEM_TO_MOVE));
}

std::string CharacterInfo::read_world(){
	/*int start_address = owner->read_int(AddressManager::get()->getAddress(ADDRESS_BASE_LOG_LIST)) + 4;
	std::string own_name = name;

	for (int i = 0; i < 20; i++){
	string tmp_name = ReadString(start_address);


	if (strcmp(&tmp_name[0], &own_name_[0]) == 0)
	{
	world = ReadString(start_address + 0x1C);
	break;
	}
	start_address += offset_name_name_list_login;
	}*/
	return "";
}

bool TibiaProcess::get_is_logged(){
	uint32_t address = AddressManager::get()->getAddress(ADDRESS_LOGGED);
	uint32_t AddressLogged = read_int(address);
	if (AddressLogged != 30)
		return is_logged = false;
	return is_logged = true;
}

void TibiaProcess::refresh_name(){
	while (true){
		Sleep(200);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		std::string name_char = get_name_char();
		if (!character_info)
			continue;
				
		mtx_ping_vector.lock();
		std::string name_char_in = character_info->name;
		mtx_ping_vector.unlock();

		if (name_char == name_char_in)
			continue;		

		mtx_ping_vector.lock();
		character_info->name = name_char;
		mtx_ping_vector.unlock();
	}
}

std::string get_pid_directory(uint32_t pid){
	TCHAR filename[MAX_PATH];
	HANDLE processHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);
	if (processHandle != NULL) {
		if (GetModuleFileNameEx(processHandle, NULL, filename, MAX_PATH) == 0){
			CloseHandle(processHandle);
			return "";
		}
		CloseHandle(processHandle);
		return boost::filesystem::path(filename).parent_path().string();
	}
	return "";
}

uint32_t TibiaProcess::get_current_tibia_version(){
	return current_tibia_version;
}

void TibiaProcess::set_current_tibia_version(uint32_t version){
	current_tibia_version = version;
}

void TibiaProcess::set_pid(uint32_t pid, int counts){
	auto process = Process::get()->GetProcessesMap();
	if (process[pid].version <= 0){
		MessageBox(0, "Fail to load tibia.dat", "Error!", MB_OK);
		return;
	}
	 
	std::string version_str = std::to_string(process[pid].version);
	current_tibia_version = process[pid].version;
	auto mgr = AddressManager::get();

	this->pid  = pid;
	update_process_handler();
	update_base_address();

	mgr->load_address("address" + version_str + ".lua");

	std::string process_bin_path = get_pid_directory(pid);

	/*if (!ItemsManager::get()->load(process_bin_path + "\\Tibia.dat")){
		MessageBox(0, "Fail to load tibia.dat", "Error!", MB_OK);
		return;
	}	
	*/
	auto input = HighLevelVirtualInput::get_default();
	/*
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return;
	*/
	input->set_pid(pid);
	
	UtilityCore::get()->set_current_level(character_info->level());

	HUDManager::get_default()->set_enabled(false);
	HUDManager::get_default()->set_windowshandle([](){ return (uint32_t)TibiaProcess::get_default()->get_window_handler(); });
	HUDManager::get_default()->init();



	/*ImageManager::get()->set_image([](std::string itemname){ return (std::pair<char*, uint32_t>)GifManager::get()->get_item_gif_buffer(itemname); });
	HUDManager::get_default()->init();
	HUDManager::get_default()->set_enabled(false);
	InfoCore::get()->set_player_experience(character_info->experience());
	UtilityCore::get()->set_current_level(character_info->level());
	HUDManager::get_default()->set_windowshandle([](){ return (uint32_t)TibiaProcess::get_default()->get_window_handler(); });
	HUDManager::get_default()->set_image([](std::string itemname){ return (std::pair<char*, uint32_t>)GifManager::get()->get_item_gif_buffer(itemname); });*/
/*	counts--;
	if (counts <= 0)
		return;
	set_pid(pid,counts);*/
}

void TibiaProcess::update_process_handler(){
	if (process_handler){
		CloseHandle(process_handler);
		process_handler = 0;
	}
	process_handler = OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, false, pid);
}

uint32_t TibiaProcess::get_pid(){
	return pid;
}

bool TibiaProcess::is_process_running(){
	uint32_t temp_pid = this->pid;
	auto myMap = Process::get()->GetProcessesMap();

	for (auto it : myMap)
		if (it.second.pid == temp_pid)
			return true;
	
	return false;
}

HWND TibiaProcess::get_window_handler(){
	return (HWND)character_info->get_window_handler();
}

std::shared_ptr<TibiaProcess> TibiaProcess::get_default(){
	static std::shared_ptr<TibiaProcess> m = nullptr;
	if (!m)
		m = std::shared_ptr<TibiaProcess>(new TibiaProcess);
	return m;
}
//Factory
std::shared_ptr<TibiaProcess> TibiaProcess::requestTibiaProcessByPid(uint32_t pid){
	static std::map<uint32_t/*pid*/, std::shared_ptr<TibiaProcess> > mClients;
	auto it = mClients.find(pid);
	if (it != mClients.end())
		return it->second;


	std::shared_ptr<TibiaProcess> retval(new TibiaProcess);
	retval->set_pid(pid);
	mClients[pid] = retval;
	return retval;
}

//client memory utilitty
uint32_t TibiaProcess::read_memory_block(uint32_t address, void * out_ptr, int length, std::vector<uint32_t>& offsets, bool use_base, bool read_last){
	if (use_base)
		address += base_address;

	SIZE_T bytes_read = 0;

	if (!offsets.size()){
		ReadProcessMemory(process_handler, (LPCVOID)address, out_ptr, length, &bytes_read);
		return bytes_read;
	}

	uint32_t final_address = 0;
	ReadProcessMemory(process_handler, (LPCVOID)address, &final_address, 4, &bytes_read);
	if (bytes_read != 4 || !final_address)
		return false;
	
	if (!read_last){
		*(uint32_t*)out_ptr = final_address + offsets[offsets.size() - 1];
		return bytes_read;
	}

	for (uint32_t i = 0; i < offsets.size() - 1/*the last should read real value*/; i++){
		ReadProcessMemory(process_handler, (LPCVOID)(final_address + offsets[i]), &final_address, 4, &bytes_read);
		if (bytes_read != 4 || !final_address)
			return 0;			
	}

	//read_last
	if (!read_last){
		return final_address + offsets[offsets.size() - 1];
	}

	ReadProcessMemory(process_handler, (LPCVOID)(final_address + offsets[offsets.size() - 1]), out_ptr, length, &bytes_read);
	return bytes_read;
}

uint32_t TibiaProcess::read_memory_block(uint32_t address, void * out_ptr, int length, bool use_base){
	if (use_base)
		address += base_address;

	SIZE_T bytes_read = 0;

	int retval = ReadProcessMemory(process_handler, (LPCVOID)address, out_ptr, length, &bytes_read);
	if (!retval && length){
		int last_error = GetLastError();
		while (last_error == 299){
			length = length / 2;
			if (!length)
				return 0;
			retval = ReadProcessMemory(process_handler, (LPCVOID)address, out_ptr, length, &bytes_read);
			if (retval)
				break;
			last_error = GetLastError();
		}
	}
	
	return bytes_read;
}

uint32_t TibiaProcess::read_int(uint32_t address, std::vector<uint32_t> offsets, bool use_base, bool read_last){
	uint32_t out;
	if (read_memory_block(address, &out, 4, offsets, use_base, read_last) != 4)
		return 0;

	return out;
}

uint32_t TibiaProcess::read_int(uint32_t address, bool use_base){
	uint32_t out;
	if (read_memory_block(address, &out, 4, use_base) != 4)
		return 0;

	return out;
}

unsigned char TibiaProcess::read_byte(uint32_t address, std::vector<uint32_t> offsets, bool use_base){
	unsigned char out;
	read_memory_block(address, &out, 1, offsets, use_base);
	return out;
}

unsigned char TibiaProcess::read_byte(uint32_t address, bool use_base){
	unsigned char out;
	read_memory_block(address, &out, 1, use_base);
	return out;
}

std::string TibiaProcess::read_string(uint32_t address, std::vector<uint32_t> offsets, bool use_base){
	char buff_great[1025];
	read_memory_block(address, &buff_great, 1024, offsets, use_base);
	buff_great[1024] = 0;
	return std::string(buff_great);
}

std::string TibiaProcess::read_string(uint32_t address, bool use_base){
	char buff_great[1025];
	read_memory_block(address, &buff_great, 1024, use_base);
	buff_great[1024] = 0;
	return std::string(buff_great);
}

void TibiaProcess::write_memory_block(uint32_t remote_address, void* in_ptr, uint32_t length){
	WriteProcessMemory((HANDLE)process_handler, (LPVOID)remote_address, in_ptr, length, NULL);
}

void TibiaProcess::write_byte(uint32_t address, unsigned char value){
	write_memory_block(address, &value, 1);
}

void TibiaProcess::write_string(uint32_t address, std::string value){
	write_memory_block(address, &value[0], value.length() + 1/*0 byte*/);
}

void TibiaProcess::write_int(uint32_t address, uint32_t value){
	write_memory_block(address, &value, 4);
}
//END CLIENT MEMORY UTLITTY

uint32_t TibiaProcess::get_id_to_move(){
	return read_int(AddressManager::get()->getAddress(ADDRESS_ITEM_TO_MOVE));
}

uint32_t TibiaProcess::get_id_to_use(){
	return read_int(AddressManager::get()->getAddress(ADDRESS_ITEM_USE));
}

uint32_t TibiaProcess::get_ping(){
	static TimeChronometer last_update;
	static neutral_mutex mtx_ping;
	static uint32_t last_ping = 0; 

	//TODO add o addres pro arquivo de addresses
	 uint32_t ping = read_int(AddressManager::get()->getAddress(ADDRESS_BASE_PING), false);
	if (!last_pings.size()){
		mtx_ping_vector.lock();
		for (int i = 0; i < PING_PROMEDY_COUNT; i++)
			last_pings.push_back(ping);
		last_ping = ping;
		mtx_ping_vector.unlock();
	}
	mtx_ping.lock();
	if (ping != last_ping || last_update.elapsed_milliseconds() >= 10000){
		mtx_ping.unlock();
		last_update.reset();
		mtx_ping_vector.lock();
			last_ping = ping;
		
			last_pings.push_back(ping);
			if (last_pings.size() > PING_PROMEDY_COUNT)
				last_pings.erase(last_pings.begin());
			uint32_t ping_promedy_temp = 0;
			for (int i = 0; i < PING_PROMEDY_COUNT; i++)
				ping_promedy_temp += last_pings[i];
		mtx_ping_vector.unlock();
		if (ping_promedy_temp)
			ping_promedy = (uint32_t)((float)ping_promedy_temp / 10.f);
	}
	else
		mtx_ping.unlock();
	return ping;
}

uint32_t TibiaProcess::get_ping_promedy(){
	return ping_promedy;
}

uint32_t TibiaProcess::get_action_delay_promedy_time(uint32_t action_count){
	uint32_t current_ping_promedy = get_ping_promedy();
	uint32_t ping_promedy_max = std::min((uint32_t)400, current_ping_promedy);

	return (uint32_t)((float)ping_promedy_max * (float)action_count * 1.5f);
}

uint32_t TibiaProcess::get_action_wait_delay(float multiplier){
	uint32_t self_ping = get_ping();
	return (uint32_t)((float)(self_ping > 400 ? 400 : self_ping) * multiplier);
}

void TibiaProcess::wait_ping_delay(float multiplier){
	Sleep(get_action_wait_delay(multiplier));
}

void TibiaProcess::kill(){
	if (!pid)
		return;

	HANDLE temp = OpenProcess(PROCESS_TERMINATE, 0, pid);
	TerminateProcess((HANDLE)temp, 0);
	CloseHandle(temp);
}

#pragma region STATUS

bool CharacterInfo::status_water(){
	return status(water, 2);
}

bool CharacterInfo::status_holly(){
	return status(holly, 2);
}

bool CharacterInfo::status_frozen(){
	return status(frozen, 2);
}

bool CharacterInfo::status_curse(){
	return status(curse, 2);
}

bool CharacterInfo::status_streng_up(){
	return status(streng_up, 2);
}

bool CharacterInfo::status_battle_red(){
	return status(battle_red, 2);
}

bool CharacterInfo::status_pz(){
	return status(pz, 2);
}

bool CharacterInfo::status_blood(){
	return status(blood, 2);
}

bool CharacterInfo::status_poison(){
	return status(poison, 1);
}

bool CharacterInfo::status_fire(){
	return status(fire, 1);
}

bool CharacterInfo::status_energy(){
	return status(energy, 1);
}

bool CharacterInfo::status_drunk(){
	return status(drunk, 1);
}

bool CharacterInfo::status_utamo(){
	return status(utamo, 1);
}

bool CharacterInfo::status_slow(){
	return status(slow, 1);
}

bool CharacterInfo::status_haste(){
	return status(haste, 1);
}

bool CharacterInfo::status_battle(){
	return status(battle, 1);
}

uint32_t CharacterInfo::self_ml(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_MAGIC_LEVEL));
}

uint32_t CharacterInfo::self_mlpc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_MAGIC_LEVEL_PERCENT));
}

uint32_t CharacterInfo::exp_to_lvl(uint32_t next_level){
	int temp = 50 * next_level * next_level * next_level / 3 - 100 * next_level *next_level + 850 * next_level / 3 - 200;
	return temp;
}

uint32_t CharacterInfo::exp_to_next_lvl(){
	int temp = exp_to_lvl(level() + 1) - experience();
	return  temp;
}

#pragma endregion

uint32_t CharacterInfo::time_to_next_lvl(){
	int exp_h = exp_hour();
	if (exp_h <= 0)
		return 0;
	int exp_nex = exp_to_next_lvl();
	if (!exp_nex)
		return 0;
	return (int)(((float)exp_nex / (float)exp_h) * 3600);
}

#pragma region SKILL_PERCENT
uint32_t CharacterInfo::first_pc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_FIRST_PERCENT));
}

uint32_t CharacterInfo::club_pc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_CLUB_PERCENT));
}

uint32_t CharacterInfo::sword_pc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_SWORD_PERCENT));
}

uint32_t CharacterInfo::axe_pc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_AXE_PERCENT));
}

uint32_t CharacterInfo::distance_pc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_DISTANCE_PERCENT));
}

uint32_t CharacterInfo::shielding_pc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_SHIELDING_PERCENT));
}

uint32_t CharacterInfo::fishing_pc(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_FISHING_PERCENT));
}

#pragma endregion

#pragma region SKILL_LEVEL
uint32_t CharacterInfo::first_lvl(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_FIRST_SKILL));
}

uint32_t CharacterInfo::club_lvl(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_CLUB));
}

uint32_t CharacterInfo::sword_lvl(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_SWORD));
}

uint32_t CharacterInfo::axe_lvl(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_AXE));
}

uint32_t CharacterInfo::distance_lvl(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_DISTANCE));
}

uint32_t CharacterInfo::shielding_lvl(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_SHIELDING));
}

uint32_t CharacterInfo::fishing_lvl(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_FISHING));
}

uint32_t CharacterInfo::exp_hour(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_EXPERIENCE_HOUR));
}

#pragma endregion



bool CharacterInfo::status(unsigned char entrace, int index){
	int temp_int = owner->read_int(AddressManager::get()->getAddress(ADDRESS_STATUS));
	unsigned char actual;
	int Address = (int)(&temp_int);
	if (index == 1)
		actual = *(unsigned char*)(Address);
	else
		actual = *(unsigned char*)(Address + 1);
	int i = entrace;
	int to_sum = i * 2;
	while (256 > i){
		if (actual >= i && actual <= i + entrace - 1)
			return true;
		i += to_sum;
	}
	return false;
}

uint32_t CharacterInfo::get_x(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_X));
}

uint32_t CharacterInfo::get_window_handler(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_WINDOW_HANDLER));
}

uint32_t CharacterInfo::get_y(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_Y));
}

int CharacterInfo::get_z(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_Z));
}

Coordinate CharacterInfo::get_self_coordinate(){
	static uint32_t last_x;
	static uint32_t last_y;
	static int last_z;
	static neutral_mutex mtx_event;

	uint32_t x = get_x();
	uint32_t y = get_y();
	int z = get_z();
	mtx_event.lock();
	if (x != last_x || y != last_y || z != last_z){
		TibiaProcess::get_default()->
		on_change_coordinate(Coordinate(last_x, last_y, last_z), Coordinate(x, y, z));
		last_x = x;
		last_y = y;
		last_z = z;
	}
	mtx_event.unlock();

	return Coordinate(get_x(), get_y(), get_z());
}

Coordinate CharacterInfo::get_mixed_coordinate(){
	Coordinate going_step_coord = this->get_going_step();
	if (going_step_coord.is_null())
		return get_self_coordinate();
	
	return going_step_coord;
}

uint32_t CharacterInfo::level(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_LEVEL));
}

uint32_t CharacterInfo::experience(){
	int temp = owner->read_int(AddressManager::get()->getAddress(ADDRESS_EXPERIENCE));
	return temp;
}

uint32_t CharacterInfo::max_hp(){
	uint32_t max_hp_address = AddressManager::get()->getAddress(ADDRESS_MAX_HP);
	return (int)(owner->read_int(AddressManager::get()->getAddress(ADDRESS_XOR)) ^
		owner->read_int(max_hp_address));
}

uint32_t CharacterInfo::max_mp(){
	return (int)(owner->read_int(AddressManager::get()->getAddress(ADDRESS_XOR)) ^
		owner->read_int(AddressManager::get()->getAddress(ADDRESS_MAX_MP)));
}

uint32_t CharacterInfo::hppc(){
	int hp_value = hp();
	int hp_max_value = max_hp();
	return static_cast<int>(static_cast<double>(hp_value) / (static_cast<double>(hp_max_value) / static_cast<double>(100)));
}

uint32_t CharacterInfo::mppc(){
	int32_t _mp = mp();
	int32_t _max_mp = max_mp();
	return static_cast<int>(static_cast<double>(_mp) / (static_cast<double>(_max_mp) / static_cast<double>(100)));
}

uint32_t CharacterInfo::hp(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_XOR)) ^ owner->read_int(AddressManager::get()->getAddress(ADDRESS_HP));
}

uint32_t CharacterInfo::mp(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_XOR)) ^
		owner->read_int(AddressManager::get()->getAddress(ADDRESS_MP));
}

uint32_t CharacterInfo::soul(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_SOUL));
}

uint32_t CharacterInfo::cap(){
	return (int)((owner->read_int(AddressManager::get()->getAddress(ADDRESS_XOR)) ^
		owner->read_int(AddressManager::get()->getAddress(ADDRESS_CAP))) / 100);
}

uint32_t CharacterInfo::stamina(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_STAMINA));
}

uint32_t CharacterInfo::offline_training(){
	return (int)((owner->read_int(AddressManager::get()->getAddress(ADDRESS_XOR)) ^
		owner->read_int(AddressManager::get()->getAddress(ADDRESS_OFFLINE_TRAINING))));
}

uint32_t CharacterInfo::character_id(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_PLAYER_ID));
}

//std::string CharacterInfo::get_name(){
//	return owner->get_name_char();
//}

uint32_t CharacterInfo::target_red(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_TARGET_RED));
}

bool CharacterInfo::is_follow_mode(){
	return owner->read_int(AddressManager::get()->getAddress(ADDRESS_FOLLOW)) != 0;
}

bool CharacterInfo::is_walking(){
	CreatureOnBattlePtr self = BattleList::get()->get_self();
	if (!self)
		return false;
	return self->dist_from_center_x != 0 || self->dist_from_center_y != 0;
}

Coordinate CharacterInfo::get_displacement(){
	CreatureOnBattlePtr self = BattleList::get()->get_self();
	if (!self)
		return Coordinate();
	return Coordinate(self->dist_from_center_x, self->dist_from_center_y, 0);
}

Coordinate CharacterInfo::get_going_step(){
	CreatureOnBattlePtr self = BattleList::get()->get_self();
	if (!self)
		return Coordinate();
	return self->get_displaced_coordinate();
	/*return Coordinate(self->dist_from_center_x, self->dist_from_center_y, 0);

	Coordinate displacement = get_displacement();
	if (displacement.is_null())
		return Coordinate();
	Coordinate self_coordinate = get_self_coordinate();
	Coordinate retval;
	if (displacement.x || displacement.y){
		if (displacement.x)
			retval.x = self_coordinate.x + ((displacement.x < 0) ? 1 : -1);
		else
			retval.x = self_coordinate.x;	
		
		if (displacement.y)
			retval.y = self_coordinate.y + ((displacement.y < 0) ? 1 : -1);
		else
			retval.y = self_coordinate.y;
		retval.z = self_coordinate.z;
	}

	return retval;*/
}

bool CharacterInfo::wait_stop_waking(uint32_t timeout){
	TimeChronometer timer;
	while ((uint32_t)timer.elapsed_milliseconds() < timeout){
		if (!is_walking())
			return true;

		Sleep(50);
	}
	return false;
}


void CharacterInfo::set_name_char(std::string in){
}

bool CharacterInfo::is_visible(){
	//todo
	return false;
}

uint32_t CharacterInfo::get_profession(){
	//TODO
	return 0;
}

uint32_t CharacterInfo::get_outfit(){
	//TODO
	return 0;
}

uint32_t CharacterInfo::get_direction(){
	//TODO
	return 0;
}

uint32_t CharacterInfo::get_level_pc(){
	//return owner->read_int(AddressManager::get()->getAddress(ADDRESS_LEVEL_PERCENT));
	return 0;
}

void CharacterInfo::mount(){
	if (status_pz())
		return;

	CreatureOnBattlePtr self = BattleList::get()->get_self();
	if (!self)
		return;

	if (self->mount > 0)
		return;

	FloatingMiniMenu::mount();
}

void CharacterInfo::unmount(){
	if (status_pz())
		return;

	CreatureOnBattlePtr self = BattleList::get()->get_self();
	if (self->mount <= 0)
		return;

	FloatingMiniMenu::dismount();
}

void CharacterInfo::tryloggout(){
	//IMPLEMENT

	if (status_pz() || !status_battle()){
		//press_control();//SendMessage(CurrentHWND,WM_KEYDOWN, 0x00000011,0x1d0001); // CTRL keydown
		//SendMessage(CurrentHWND, WM_CHAR, 0x11, 0xc0100001);
		//Sleep(25);
		//send_all_key_up();
	}
}

std::string CharacterInfo::get_name_char(){
	return owner->get_name_char();
}

void TibiaProcess::set_disable_hud(bool temp){
	disable_hud = temp;
}

bool TibiaProcess::get_disable_hud(){
	return disable_hud;
}