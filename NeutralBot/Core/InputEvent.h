#pragma once
#include "Util.h"

enum input_t{
	input_type_none,
	input_type_mouse,
	input_type_keyboard
};

std::string input_t_as_string(input_t t_val);

enum mouse_button_t : uint32_t{
	mouse_button_none,
	mouse_button_left,
	mouse_button_rigth,
	mouse_button_middle,
	mouse_button_additional_1,
	mouse_button_additional_2
};

std::string mouse_button_t_as_string(mouse_button_t t_val);

enum mouse_event_t{
	mouse_event_none,
	mouse_event_move,
	mouse_event_button_down,
	mouse_event_button_up,
	mouse_event_double_click,
	mouse_event_wheel
}; 

/*

function inputevents(e)
if (e.event_type == 2) then
Notify.message_box('seu codigo')
end
end
*/

std::string mouse_event_t_as_string(mouse_event_t t_val);

enum keyboard_event_t{
	keyboard_event_none,
	keyboard_key_down,
	keyboard_key_up,
	keyboard_key_press
};

std::string keyboard_event_t_as_string(keyboard_event_t t_val);
#pragma pack(push,1)
class InputBaseEvent{
public:
	
	uint32_t button_code;
	bool ctrl_pressed;
	bool alt_pressed;
	bool shift_pressed;
	bool scrolllock_state;
	bool numlock_state;
	bool capslock_state;
	bool insert_state;
	input_t input_type = input_type_none;

	InputBaseEvent(input_t type);
	virtual bool is_mouse_event();
	virtual bool is_keyboard_event();
	virtual void print();
};

class MouseInput : public InputBaseEvent{
public:
	int x, y;
	int wheel_delta;
	mouse_event_t event_type;

	static std::string get_button_as_string(mouse_button_t button);

	MouseInput();

	virtual std::string get_button_as_string();

	virtual bool is_mouse_event();

	virtual bool is_keyboard_event();

	bool is_mouse_move_event();

	virtual void print();
};

class KeyboardInput : public InputBaseEvent{
public:
	keyboard_event_t event_type;
	std::string key_string;
	char character;

	KeyboardInput();

	std::string get_button_as_string();

	bool is_mouse_event();

	bool is_keyboard_event();

	void print();
};

struct InputHolder{
	InputBaseEvent* input_ptr;
	InputHolder();
	InputHolder(InputBaseEvent* input);

	~InputHolder();

	bool is_mouse();

	bool is_keyboard();
};



#pragma pack(pop)

