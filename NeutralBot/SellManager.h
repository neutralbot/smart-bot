#pragma once
#include "Core\Util.h"
#include "Core\ItemsManager.h"

struct ItemStruct{
	std::string item_name;
	uint32_t item_id;
	uint32_t item_price;
	uint32_t item_qty;

public:
	DEFAULT_GET_SET(std::string, item_name);
	DEFAULT_GET_SET(uint32_t, item_id);
	DEFAULT_GET_SET(uint32_t, item_price);
	DEFAULT_GET_SET(uint32_t, item_qty);
};

class SellItems{
	std::map<std::string, std::shared_ptr<ItemStruct>> mapItemStruct;

	std::string backpack_loot = "";
	uint32_t backpack_loot_id = 0;
public:

	void set_backpack_loot(std::string name){
		this->backpack_loot = name;
		this->backpack_loot_id = ItemsManager::get()->getitem_idFromName(name);
	}
	std::string get_backpack_loot(){
		return backpack_loot;
	}
	int get_backpack_loot_id(){
		return backpack_loot_id;
	}
	std::map<std::string, std::shared_ptr<ItemStruct>> get_mapItemStruc(){
		return mapItemStruct;
	}
	std::string request_new_id(){
		uint32_t new_id = 0;

		while (mapItemStruct.find(std::to_string(new_id)) != mapItemStruct.end()){
			new_id++;
		}

		mapItemStruct[std::to_string(new_id)] = std::shared_ptr<ItemStruct>(new ItemStruct);
		mapItemStruct[std::to_string(new_id)]->item_id = 0;
		mapItemStruct[std::to_string(new_id)]->item_name = "none";
		mapItemStruct[std::to_string(new_id)]->item_price = 0;
		mapItemStruct[std::to_string(new_id)]->item_qty = 0;

		return std::to_string(new_id);
	}
	std::shared_ptr<ItemStruct> getItemStruct(std::string current_id){
		auto it = std::find_if(mapItemStruct.begin(), mapItemStruct.end(), [&](std::pair<std::string, std::shared_ptr<ItemStruct>> info_pair){
			return _stricmp(&current_id[0], &info_pair.first[0]) == 0; });
		if (it == mapItemStruct.end())
			return 0;

		return it->second;
	}
	void removeItemStruct(std::string current_id){
		auto it = std::find_if(mapItemStruct.begin(), mapItemStruct.end(), [&](std::pair<std::string, std::shared_ptr<ItemStruct>> info_pair){
			return _stricmp(&current_id[0], &info_pair.first[0]) == 0; });
		if (it != mapItemStruct.end())
			mapItemStruct.erase(current_id);
	}

	Json::Value parse_class_to_json() {
		Json::Value spellClasse;
		Json::Value repotItemList;

		for (auto it : mapItemStruct){
			Json::Value item;

			item["item_name"] = it.second->get_item_name();
			item["item_id"] = it.second->get_item_id();
			item["item_price"] = it.second->get_item_price();
			item["item_qty"] = it.second->get_item_qty();
			repotItemList[it.first] = item;
		}

		spellClasse["backpack_loot"] = this->backpack_loot;
		spellClasse["backpack_loot_id"] = this->backpack_loot_id;

		spellClasse["repotItemList"] = repotItemList;
		return spellClasse;
	}
	void parse_json_to_class(Json::Value jsonObject) {
		Json::Value repotItemList = jsonObject["repotItemList"];

		if (!jsonObject["backpack_loot"].isNull() || !jsonObject["backpack_loot"].empty())
			this->backpack_loot = jsonObject["backpack_loot"].asString();

		if (!jsonObject["backpack_loot_id"].isNull() || !jsonObject["backpack_loot_id"].empty())
			this->backpack_loot_id = jsonObject["backpack_loot_id"].asInt();

		Json::Value::Members members = repotItemList.getMemberNames();

		for (auto member : members){
			Json::Value item = repotItemList[member];
			std::shared_ptr<ItemStruct> newItem(new ItemStruct);

			std::string name = item["item_name"].asString();
			newItem->set_item_name(item["item_name"].asString());
			newItem->set_item_id(ItemsManager::get()->getitem_idFromName(name));
			newItem->set_item_price(item["item_price"].asInt());
			newItem->set_item_qty(item["item_qty"].asInt());
			mapItemStruct[member] = newItem;
		}
	}
};

class SellManager{
	std::map<std::string, std::shared_ptr<SellItems>> mapSellItems;

public:
	SellManager::SellManager(){
		request_new_id();
	}
	std::map<std::string, std::shared_ptr<SellItems>> get_mapSellItems(){
		return mapSellItems;
	}
	std::shared_ptr<SellItems> getSellItems(std::string current_id){
		auto it = std::find_if(mapSellItems.begin(), mapSellItems.end(), [&](std::pair<std::string, std::shared_ptr<SellItems>> info_pair){
			return _stricmp(&current_id[0], &info_pair.first[0]) == 0; });
		if (it == mapSellItems.end())
			return 0;

		return it->second;
	}
	void removeSellItems(std::string current_id){
		auto it = std::find_if(mapSellItems.begin(), mapSellItems.end(), [&](std::pair<std::string, std::shared_ptr<SellItems>> info_pair){
			return _stricmp(&current_id[0], &info_pair.first[0]) == 0; });
		if (it != mapSellItems.end())
			mapSellItems.erase(it);
	}
	bool changeSellItems(std::string newId, std::string oldId){
		if (newId == "" || oldId == "")
			return false;

		auto it = std::find_if(mapSellItems.begin(), mapSellItems.end(), [&](std::pair<std::string, std::shared_ptr<SellItems>> info_pair){
			return _stricmp(&oldId[0], &info_pair.first[0]) == 0; });
		if (it == mapSellItems.end())
			return false;

		string_util::lower(newId);
		mapSellItems[newId] = it->second;
		mapSellItems.erase(it);
		return true;
	}

	void clear(){
		mapSellItems.clear();
	}

	Json::Value parse_class_to_json() {
		Json::Value SellManager;
		Json::Value SellItemList;

		for (auto sellItems : mapSellItems)
			SellItemList[sellItems.first] = sellItems.second->parse_class_to_json();

		SellManager["SellItemList"] = SellItemList;
		return SellManager;
	}
	void parse_json_to_class(Json::Value jsonObject) {
		Json::Value SellItemList = jsonObject["SellItemList"];
		Json::Value::Members members = SellItemList.getMemberNames();

		for (auto member : members){
			std::shared_ptr<SellItems> sellItem(new SellItems);

			sellItem->parse_json_to_class(SellItemList[member]);
			mapSellItems[member] = sellItem;
		}
	}

	std::string request_new_id(){
		uint32_t current_id = 0;
		std::string temp_id = "sellitems" + std::to_string(current_id);
	

		while (mapSellItems.find(temp_id) != mapSellItems.end()){
			current_id++;
			temp_id = "sellitems" + std::to_string(current_id);
		}

		mapSellItems[temp_id] = std::shared_ptr<SellItems>(new SellItems);
		return temp_id;
	}

	static SellManager* get(){
		static SellManager* m = nullptr;
		if (!m){
			m = new SellManager();
		}
		return m;
	}
};


