#pragma once
#include "Core\Util.h"
#include "Core\constants.h"

enum waypoint_t:int32_t {
	waypoint_none,
	waypoint_walk_to,
	waypoint_walk_to_random,
	waypoint_message,
	waypoint_message_npc, 
	waypoint_rearch_creature,
	waypoint_auto_haste,
	waypoint_auto_utura,
	waypoint_cast_spell,
	waypoint_goto_label,
	waypoint_go_to_waypoint_path,
	waypoint_wait_milliseconds,
	waypoint_lua_script,	
	waypoint_hole_or_step,
	waypoint_up_ladder,
	waypoint_exani_hur_up,
	waypoint_exani_hur_down,
	waypoint_reopen_containers,
	waypoint_close_containers, 
	waypoint_repot_here_by_id,
	waypoint_deposit_items_by_depot_id,
	waypoint_deposit_items_all_depot,
	waypoint_repot_from_depot_by_id,
	waypoint_sell_items_by_sell_id,
	waypoint_sell_all_items,
	waypoint_deposit_money,
	waypoint_withdraw_all_repots_if_fail_goto_label,	
	waypoint_rope,
	waypoint_shovel,
	waypoint_pick,
	waypoint_machete,
	waypoint_scythe,
	waypoint_destroy_field,
	waypoint_tool_id, 
	waypoint_use,
	waypoint_use_item, 
	waypoint_use_lever,
	waypoint_open_door,
	waypoint_remove_item,
	waypoint_teleport_else_go_to_label,
	waypoint_use_item_id_to_down_or_up, 	
	waypoint_loggout_this_location, 
	waypoint_close_tibia_this_location, 
	waypoint_if_way_block_goto_label, 	
	waypoint_lure_here_distance,
	waypoint_lure_here_knight_mode,
	waypoint_pulse_target, 
	waypoint_pulse_looter,
	waypoint_check_necesary_deposit_go_to_label,
	waypoint_check_necesary_deposit_any_goto_label,
	waypoint_check_necesary_repot_go_to_label,
	waypoint_check_necesary_repot_any_go_to_label,
	waypoint_check_cap_less_than_goto_label,
	waypoint_withdraw_value, 
	waypoint_travel,
	waypoint_drop_item,
	waypoint_go_to_next_path,
	waypoint_t_last
};

enum waypoint_type :int32_t {
	type_none,
	type_walk,
	type_action,
	type_lure,
	type_last
};

#pragma pack(push,1)
class WaypointInfo{
	std::string label;
	std::string visual_name = "none";
	Coordinate position;
	waypoint_t action;
	side_t::side_t side_to_go;

	waypoint_type waypointType = waypoint_type::type_none;
	bool lure_state = false;

	std::map<std::string, std::string> additionalInfo;
public:


	WaypointInfo();
	WaypointInfo(std::string label, waypoint_t action, Coordinate position, side_t::side_t side = side_t::side_t::side_center);

	std::map<std::string, std::string> getadditionalInfoCopy();

	std::string get_visual_name();

	side_t::side_t get_side_to_go();

	waypoint_type get_waypointType();

	Coordinate get_position();

	void set_visual_name(std::string name);

	void set_side_to_go(side_t::side_t s);

	void set_waypointType(waypoint_type _type);

	void set_position(Coordinate coord);

	void set_label(std::string z);

	std::string get_label();

	Coordinate get_destination_coord();
	void set_position_x(int x);
	void set_position_y(int y);
	void set_position_z(int z);

	int get_position_x();
	int get_position_y();
	int get_position_z();

	void set_action(waypoint_t action);
	waypoint_t get_action();

	void set_additionalInfo(std::string key, std::string value);
	std::vector<std::string> get_additionalKeys();
	std::string get_additionalInfo(std::string key);
	int get_additional_info_as_int(std::string key);
	bool get_additional_info_as_bool(std::string key);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject, int version);

	std::pair<waypoint_t, side_t::side_t> migrate_to_new(uint32_t type);

};
#pragma pack(pop)
