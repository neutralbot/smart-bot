#pragma once 
#include "PlayerInfo.h"

struct ContainerInfo{
public:

	std::vector<std::pair<uint32_t/*item id*/, uint32_t/*item count*/ >> list_item_and_count;
	uint32_t slots_used;
	uint32_t slots_free;
	uint32_t slots_total;
	uint32_t container_id;
	std::string container_name;
	bool is_full;
	bool has_next;
	bool is_minimized;

};

class NaviPlayerContainers{
public:
	uint32_t* mtx_access;

	TimeChronometer time_last_update;

	uint32_t count_money_total;
	uint32_t count_container_open;
	std::vector<std::shared_ptr<ContainerInfo>> container_infos;

	NaviPlayerContainers();

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonValue);
};

typedef std::shared_ptr<NaviPlayerContainers> NaviPlayerContainersPtr;