#pragma once
#include "Repot.h"
#include "Core\RepoterCore.h"

using namespace Neutral;

void Repot::PageAddNewPage(std::string PageName) {
	std::string pageName = PageName;

	Telerik::WinControls::UI::RadPageViewPage^ radPageViewPage1 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
	Telerik::WinControls::UI::RadListView^ radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
	Telerik::WinControls::UI::RadDropDownList^ dropDownList = (gcnew Telerik::WinControls::UI::RadDropDownList());
	Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0", L"Item"));

	// radPageViewPage1
	radPageViewPage1->Controls->Add(radListView1);
	radPageViewPage1->ItemSize = System::Drawing::SizeF(39, 28);
	radPageViewPage1->Location = System::Drawing::Point(10, 37);
	radPageViewPage1->Name = "Page_" + gcnew String(pageName.c_str());
	radPageViewPage1->Size = System::Drawing::Size(773, 392);
	radPageViewPage1->Text = gcnew String(pageName.c_str());
	dropDownList->DropDownListElement->ItemHeight = 30;

	// radListView1
	radListView1->Dock = System::Windows::Forms::DockStyle::Fill;
	radListView1->AutoScroll = false;
	radListView1->ShowGridLines = true;
	listViewDetailColumn7->HeaderText = L"Item";
	listViewDetailColumn7->Width = 280;
	listViewDetailColumn7->MinWidth = 280;
	listViewDetailColumn7->MaxWidth = 280;	
	radListView1->AllowEdit = false;
	radListView1->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(1) { listViewDetailColumn7 });
	radListView1->ItemSpacing = -1;
	radListView1->Location = System::Drawing::Point(0, 30);
	radListView1->Name = "list_" + gcnew String(pageName.c_str());
	radListView1->Size = System::Drawing::Size(312, 222);
	radListView1->TabIndex = 17;
	radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
	radListView1->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Repot::ItemListViewItemRemoving);
	radListView1->SelectedItemChanged += gcnew System::EventHandler(this, &Repot::ItemListViewItemChanged);
	radListView1->VerticalScrollState = Telerik::WinControls::UI::ScrollState::AutoHide;
	radListView1->ListViewElement->ItemSize = System::Drawing::Size(radListView1->ListViewElement->ItemSize.Width, 25);
	PageRepot->Controls->Add(radPageViewPage1);

}

void Repot::addItemOnView(String^ idBp, String^ Item, int min, int max, int gpeach){
	array<Control^>^ getRadListView = this->PageRepot->SelectedPage->Controls->Find("list_" + idBp, true);
	std::shared_ptr<RepotId> Itemss = RepoterManager::get()->getRepoIdRule(managed_util::fromSS(idBp));
	std::string idd = Itemss->updateItem(managed_util::fromSS(Item), min, max, gpeach);

	add_in_setup(idd);

	if (getRadListView->Length < 0)
		return;

	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	array<String^>^ columnArrayItems = { Item, Convert::ToString(min), Convert::ToString(max), Convert::ToString(gpeach) };

	Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(idd.c_str()), columnArrayItems);
	newItem->Image = managed_util::get_item_image(Item, 20);
	radListView->Items->AddRange(gcnew array<Telerik::WinControls::UI::ListViewDataItem^>(1) { newItem });
	radListView->SelectedIndex = radListView->Items->Count - 1;
}

void Repot::loadItemOnView(std::string id_){
	array<Control^>^ getRadListView = Controls->Find("list_" + gcnew String(id_.c_str()), true);
	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];

	std::map<std::string, std::shared_ptr<RepotId>> mapRepoid = RepoterManager::get()->getMapRepotId();
	std::map<std::string, std::shared_ptr<RepotItens>> mapRepotItens = mapRepoid[id_]->getMapRepotItens();

	radListView->BeginUpdate();
	for (auto i = mapRepotItens.begin(); i != mapRepotItens.end(); i++){
		if (!i->second)
			continue;

		array<String^>^ columnArrayItems = { gcnew String(i->second->itemName.c_str()), Convert::ToString(i->second->min), Convert::ToString(i->second->max), Convert::ToString(i->second->gpeach) };
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(i->first.c_str()), columnArrayItems);
		newItem->Image = managed_util::get_item_image(gcnew String(i->second->itemName.c_str()), 20);
		radListView->Items->AddRange(gcnew array<Telerik::WinControls::UI::ListViewDataItem^>(1) { newItem });
	}
	radListView->EndUpdate();
}

void Repot::loadAll(){
	std::map<std::string, std::shared_ptr<RepotId>> mapRepoid = RepoterManager::get()->getMapRepotId();
	for (auto it = mapRepoid.begin(); it != mapRepoid.end(); it++){
		PageAddNewPage(it->first);
		loadItemOnView(it->first);
	}
}

void Repot::updateSelectedData(){
	if (!PageRepot->SelectedPage)
		return;

	Telerik::WinControls::UI::RadListView^ radlist = getItemListView();

	if (!radlist->SelectedItem)
		return;
	
	if (!radGroupBox1->Enabled)
		radGroupBox1->Enabled = true;

	std::shared_ptr<RepotId> mapId = RepoterManager::get()->getRepoIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
	std::string idMap = managed_util::fromSS(radlist->SelectedItem->Text);
	std::shared_ptr<RepotItens> mapItens = mapId->getRepotItensRule(idMap);

	

	disable_item_update = true;
	tx_nameitem->Text = gcnew String(mapItens->get_itemName().c_str());
	tx_min->Value = mapItens->get_min();
	tx_max->Value = mapItens->get_max();
	tx_gpeach->Value = mapItens->get_gpeach();


	disable_item_update = false;
}

System::Void Repot::ItemListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	if (!PageRepot->SelectedPage)
		return;

	array<Control^>^ getRadListView = this->PageRepot->SelectedPage->Controls->Find("list_" + PageRepot->SelectedPage->Text, true);
	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;
	auto mapRepoId = RepoterManager::get()->getMapRepotId();

	if (itemRow)
		mapRepoId[managed_util::fromSS(PageRepot->SelectedPage->Text)]->removeRepotItem(managed_util::fromSS(itemRow->Text));

	disable_item_update = true;
	if (radListView->Items->Count <= 1)
		radGroupBox1->Enabled = false;
	disable_item_update = false;

	auto control_rule = ControlManager::get()->getControlInfoRule("RepoterGroup");
	if (!control_rule)
		return;

	control_rule->removeControl(managed_util::fromSS(PageRepot->SelectedPage->Text + itemRow->Text) + "combo");
	control_rule->removeControl(managed_util::fromSS(PageRepot->SelectedPage->Text + itemRow->Text) + "spinmin");
	control_rule->removeControl(managed_util::fromSS(PageRepot->SelectedPage->Text + itemRow->Text) + "spinmax");
}

System::Void Repot::ItemListViewItemChanged(System::Object^  sender, System::EventArgs^  e) {
	Telerik::WinControls::UI::RadListViewElement^ list = (Telerik::WinControls::UI::RadListViewElement^)sender;
	updateSelectedData();
}

System::Void Repot::PageRepot_NewPageRequested(System::Object^  sender, System::EventArgs^  e) {
	PageAddNewPage(RepoterManager::get()->requestNewRepotId());
	close_button();
}

void Repot::add_in_setup(std::string id){
	String^ repoter_id = gcnew String(id.c_str());
	String^ container_name = "none";

	std::shared_ptr<ControlInfo> controlInfoRule = ControlManager::get()->getControlInfoRule("RepoterGroup");
	if (!controlInfoRule)
		return;
	std::string repot_id = managed_util::fromSS(PageRepot->SelectedPage->Text);
	String^ spinmin_control_id = gcnew String(controlInfoRule->requestNewById(repot_id + id + "spinmin").c_str());
	String^ spinmax_control_id = gcnew String(controlInfoRule->requestNewById(repot_id+id + "spinmax").c_str());
	String^ combo_control_id = gcnew String(controlInfoRule->requestNewById(repot_id + id + "combo").c_str());

	auto controlRule_ = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(combo_control_id));
	if (controlRule_){
		controlRule_->set_vectorItem(ItemsManager::get()->get_itens());
		controlRule_->set_property("name", managed_util::fromSS(repoter_id));
		controlRule_->set_property("text", managed_util::fromSS(container_name));

		controlRule_->set_property("type", "combobox");
		controlRule_->set_property("sizeW", std::to_string(140));
		controlRule_->set_property("sizeH", std::to_string(22));
		controlRule_->set_property("locationX", std::to_string(10));
		controlRule_->set_property("locationY", std::to_string(60));
	}

	auto controlRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(spinmin_control_id));
	if (controlRule){
		controlRule->set_property("text", managed_util::fromSS(repoter_id));
		controlRule->set_property("name", managed_util::fromSS(spinmin_control_id));
		controlRule->set_property("type", "spin");
		controlRule->set_property("maxvalue", "999999");
		controlRule->set_property("minvalue", "0");
		controlRule->set_property("value", "0");
		controlRule->set_property("sizeW", std::to_string(55));
		controlRule->set_property("sizeH", std::to_string(22));
		controlRule->set_property("locationX", std::to_string(10));
		controlRule->set_property("locationY", std::to_string(50));
	}
	auto controlRuleoi = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(spinmax_control_id));
	if (controlRuleoi){
		controlRuleoi->set_property("text", managed_util::fromSS(repoter_id));
		controlRuleoi->set_property("name", managed_util::fromSS(spinmax_control_id));
		controlRuleoi->set_property("type", "spin");
		controlRuleoi->set_property("maxvalue", "999999");
		controlRuleoi->set_property("minvalue", "0");
		controlRuleoi->set_property("value", "0");
		controlRuleoi->set_property("sizeW", std::to_string(55));
		controlRuleoi->set_property("sizeH", std::to_string(22));
		controlRuleoi->set_property("locationX", std::to_string(10));
		controlRuleoi->set_property("locationY", std::to_string(50));
	}
}

System::Void Repot::PageRepot_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e) {
	RepoterManager::get()->removeRepotId(managed_util::fromSS(e->Page->Text));
	close_button();
}

System::Void Repot::Repot_Load(System::Object^  sender, System::EventArgs^  e) {
	ContainerManager::get();

	std::vector<std::string> arrayitems = ItemsManager::get()->get_itens();
	tx_nameitem->BeginUpdate();
	for (auto item : arrayitems)
		tx_nameitem->Items->Add(gcnew String(item.c_str()));
	tx_nameitem->EndUpdate();

	loadAll();
	update_idiom();
			
	disable_item_update = true;
	DropDownListReorganize->Text = Convert::ToString(RepoterManager::get()->get_reorganize_containers());
	radDropDownList1->Text = Convert::ToString(RepoterManager::get()->get_minimize_all_containers());
	disable_item_update = false;

	radGroupBox1->Enabled = false;
	PageRepot->ViewElement->AllowEdit = true;
	PageRepot->ViewElement->EditorInitialized += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEditorEventArgs^>(this, &Repot::Page_EditorInitialized);

}

System::Void Repot::bt_delete_Click(System::Object^  sender, System::EventArgs^  e) {
if (!PageRepot->SelectedPage)
		return;

	array<Control^>^ getRadListView = this->PageRepot->SelectedPage->Controls->Find("list_" + PageRepot->SelectedPage->Text, true);
	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;

	auto mapRepotId = RepoterManager::get()->getMapRepotId();
	if (itemRow){
		radListView->Items->Remove(itemRow);
		mapRepotId[managed_util::fromSS(PageRepot->SelectedPage->Text)]->removeRepotItem(managed_util::fromSS(itemRow->Text));
	}

	disable_item_update = true;
	if (radListView->Items->Count <= 0)
		radGroupBox1->Enabled = false;
	disable_item_update = false;
}

System::Void Repot::bt_add_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!PageRepot->SelectedPage)
		return;

	String^ item = "new";
	int min = 0;
	int max = 0;
	int gpeach = 0;

	if (item == String::Empty)
		return;

	addItemOnView(PageRepot->SelectedPage->Text, item, min, max, gpeach);
}

System::Void Repot::Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e){
	this->PageRepot->SelectedPage->TextChanged += gcnew System::EventHandler(this, &Repot::PageRename_TextChanged);
	text = e->Value->ToString();
}

System::Void Repot::PageRename_TextChanged(System::Object^  sender, System::EventArgs^ e){
	String^ newValue = PageRepot->SelectedPage->Text;
	String^ oldValue = text;

	if (newValue == "" || oldValue == ""){
		PageRepot->SelectedPage->Text = oldValue;
		return;
	}

	if (newValue == oldValue){
		PageRepot->SelectedPage->Text = oldValue;
		return;
	}

	int count = 0;
	for each(auto items in PageRepot->Pages){
		if (items->Text == newValue){
			if (count == 0)
				count++;
			else{
				PageRepot->SelectedPage->Text = oldValue; 
				return;				
			}
		}
	}

	std::string newName = string_util::lower(managed_util::to_std_string(newValue));
	std::string oldName = string_util::lower(managed_util::to_std_string(oldValue));
	
	array<Control^>^ getRadListView = PageRepot->SelectedPage->Controls->Find("list_" + oldValue, true);

	if (getRadListView->Length == 0){
		PageRepot->SelectedPage->Text = oldValue;
		return;
	}

	if (!RepoterManager::get()->changeRepotId(newName, oldName)){
		PageRepot->SelectedPage->Text = oldValue;
		return;
	}

	text = newValue;

	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	radListView->Name = "list_" + newValue;

	ControlManager::get()->changePropertyRepoter(newName, oldName, "name");
}