#pragma once
#include "KeywordManager.h"
#include <string>
#include <vector>
#include <regex>
#include <iostream>
#include <string>
#include <regex>
#include <map>
#include <iostream>
#include "Core\\Util.h"
#include <algorithm>
#include <boost\algorithm\string.hpp>

LuaFunctionMehotdInfo::LuaFunctionMehotdInfo(std::string _method_string){
	method_string = _method_string;
}

bool LuaFunctionMehotdInfo::add_suggestion(std::string suggestion){
	//return nullptr;
	auto found_it = std::find(suggestions.begin(), suggestions.end(), suggestion);
	if (found_it == suggestions.end()){
		suggestions.push_back(suggestion);
		return true;
	}
	return false;
}

std::vector<std::string> LuaTableInfo::empty(){
	std::vector<std::string> rectval;
	for (auto description : descriptions){

		if (string_util::trim(description + " ") != "")
			continue;

		rectval.push_back(name + +"  || missing description");
	}
	return rectval;
}

std::vector<std::string> LuaFunctionMehotdInfo::empty(){
	std::vector<std::string> rectval;
	std::regex rgx("\\((.*\\))");
	std::smatch match;



	for (auto it : suggestions){

		const std::string suggestion = it;

		if (std::regex_search(suggestion.begin(), suggestion.end(), match, rgx)) {
			std::string temp = std::string(match[1]);
			std::replace(temp.begin(), temp.end(), ')', ' ');
			std::replace(temp.begin(), temp.end(), '(', ' ');

			if (string_util::trim(temp + " ") != "")
				continue;

			rectval.push_back(method_string + "  || missing suggestion");
		}
	}

	for (auto description : descriptions){

		if (string_util::trim(description + " ") != "")
			continue;

		rectval.push_back(method_string + "  || missing description");
	}
	return rectval;
}

bool LuaFunctionMehotdInfo::add_description(std::string description){
	string_util::trim(description);
	//return nullptr;
	auto found_it = std::find(descriptions.begin(), descriptions.end(), description);
	if (found_it == suggestions.end()){
		descriptions.push_back(description);
		return true;
	}
	return false;
}

LuaTableInfo::LuaTableInfo(std::string _name, lua_table_t _type){
	name = _name;
	type = _type;
}

bool LuaTableInfo::add_method(std::string method){
	//return nullptr;
	LuaFunctionMehotdInfo* method_ptr = get_method_by_name(method);
	if (!method_ptr){
		methods.push_back(new LuaFunctionMehotdInfo(method));
		return true;
	}
	return false;
}

bool LuaTableInfo::add_method_description(std::string method, std::string method_description){
	string_util::trim(method_description);
	//	return nullptr;
	LuaFunctionMehotdInfo* method_ptr = get_method_by_name(method);
	if (method_ptr){
		return method_ptr->add_description(method_description);
	}
	return false;
}

bool LuaTableInfo::add_method_suggestion(std::string method, std::string method_suggestion){
	//return nullptr;
	LuaFunctionMehotdInfo* method_ptr = get_method_by_name(method);
	if (method_ptr){
		return method_ptr->add_suggestion(method_suggestion);
	}
	return false;
}

LuaFunctionMehotdInfo* LuaTableInfo::get_method_by_name(std::string method){
	//	return nullptr;
	auto res = std::find_if(methods.begin(), methods.end(), [&](LuaFunctionMehotdInfo* method_ptr)->bool{
		return method_ptr->method_string == method; });
		if (res != methods.end())
			return *res;
		return nullptr;
}

LuaVarInfo* LuaTableInfo::get_var_by_name(std::string varname){
	auto var_it = std::find_if(variables.begin(), variables.end(), [&](LuaVarInfo* var){ return var->name == varname; });
	if (var_it == variables.end())
		return nullptr;
	return *var_it;
}

bool LuaTableInfo::add_description(std::string description){
	string_util::trim(description);
	//return nullptr;
	for (auto description_it = descriptions.begin(); description_it != descriptions.end(); description_it++){
		if ((*description_it) == description)
			return false;
	}
	descriptions.push_back(description);
	return true;
}

std::string KeywordManager::getLastError(){
	return last_error;
}

void KeywordManager::resetLastError(){
	last_error = "";
}

LuaTableInfo* KeywordManager::get_class_by_class_string(std::string lib_name, std::string class_string){
	//return nullptr;
	auto found_it = std::find_if(class_vector[lib_name].begin(), class_vector[lib_name].end(),
		[&](LuaTableInfo* class_info_ptr){
		return class_info_ptr->name == class_string;
	});
	if (found_it != class_vector[lib_name].end())
		return *found_it;
	return nullptr;
}

LuaFunctionMehotdInfo* KeywordManager::get_function_by_function_string(std::string libname, std::string function_string){

	//	return nullptr;
	auto found_it = std::find_if(function_vector[libname].begin(), function_vector[libname].end(),
		[&](LuaFunctionMehotdInfo* function_ptr){
		return function_ptr->method_string == function_string;
	});
	if (found_it != function_vector[libname].end())
		return *found_it;
	return nullptr;
}


/*
additional_info.libdat
*/


void KeywordManager::process_additional_info(std::vector<std::string>& additional_info_splited_vector, std::pair<char*, uint32_t>& file_pair){
	int arg_count = additional_info_splited_vector.size();

	if (arg_count < 4)
		return;
	//\Classes\Containers\Methods\find_item_slot
	if (additional_info_splited_vector[0] == "additional_info"){
		std::string lib = additional_info_splited_vector[1];
		std::string type = additional_info_splited_vector[2];
		std::string class_name = additional_info_splited_vector[3];
		string_util::trim(class_name);
		if (class_name.length() == 0)
			return;

		this->set_lib_parsing(lib);
		if (type == "Classes"){
			if (arg_count < 5)
				return;
			this->registerClass(class_name);
			std::string s_type = additional_info_splited_vector[4];
			auto class_ptr = this->get_class_by_class_string(lib, class_name);
			if (s_type == "description.txt"){
				class_ptr->add_description(std::string(file_pair.first, file_pair.first + file_pair.second));
			}
			else if (s_type == "Methods"){
				if (arg_count < 7)
					return;

				std::string method_name = additional_info_splited_vector[5];
				string_util::trim(method_name);
				if (method_name.length() == 0)
					return;

				std::string file_name = additional_info_splited_vector[6];
				class_ptr->add_method(method_name);
				if (file_name == "suggestions.txt"){
					std::vector<std::string> suggestions;
					boost::split(suggestions, std::string(file_pair.first, file_pair.first + file_pair.second), boost::is_any_of("\n"));
					for (auto suggestion : suggestions){
						string_util::trim(suggestion);
						if (suggestion.length() == 0){
							continue;
						}
						this->addMethodSuggestion(class_name, method_name, suggestion);
					}
				}
				else if (file_name == "description.txt"){
					this->addMethodDescription(class_name, method_name, std::string(file_pair.first, file_pair.first + file_pair.second));
				}
			}

		}
		else if (type == "Constants"){
		}
		else if (type == "Functions"){
		}
		else if (type == "Tables"){
			if (arg_count < 5)
				return;
			this->registerTable(class_name);
			std::string s_type = additional_info_splited_vector[4];
			auto class_ptr = this->get_class_by_class_string(lib, class_name);
			if (s_type == "description.txt"){

				class_ptr->add_description(std::string(file_pair.first, file_pair.first + file_pair.second));
			}
			else if (s_type == "Methods"){
				if (arg_count < 7)
					return;

				std::string method_name = additional_info_splited_vector[5];
				string_util::trim(method_name);
				if (method_name.length() == 0)
					return;

				std::string file_name = additional_info_splited_vector[6];
				class_ptr->add_method(method_name);
				if (file_name == "suggestions.txt"){
					std::vector<std::string> suggestions;

					boost::split(suggestions, std::string(file_pair.first, file_pair.first + file_pair.second), boost::is_any_of("\n"));
					for (auto suggestion : suggestions){
						string_util::trim(suggestion);

						if (suggestion.length() == 0)
							continue;

						this->addMethodSuggestion(class_name, method_name, suggestion);
					}
				}
				else if (file_name == "description.txt")
					this->addMethodDescription(class_name, method_name, std::string(file_pair.first, file_pair.first + file_pair.second));

			}
			else if (s_type == "Variables"){
				std::string var_name = additional_info_splited_vector[5];
				string_util::trim(var_name);
				if (var_name.length() == 0)
					return;

				std::string file_name = additional_info_splited_vector[6];
				class_ptr->add_variable(var_name);
				if (file_name == "description.txt"){
					this->addTableVariableDescription(class_name, var_name, std::string(file_pair.first, file_pair.first + file_pair.second));
				}
			}
		}
	}
}

KeywordManager::KeywordManager(){ 
	std::map<std::string, std::pair<char*, uint32_t>> files = get_unziped_files(".\\lua\\lualibs\\additional_info.libdat");
	for (auto file : files){
		std::vector<std::string> result;
		boost::split(result, file.first, boost::is_any_of("/"));
		process_additional_info(result, file.second);
	}
	
	//missing_sugestion = check_missing();
}

std::vector<std::string> KeywordManager::check_missing(bool classes, bool functions, bool tables, bool var){
	std::vector<std::string> new_missing;

	std::map<std::string/*lib*/, std::vector<LuaVarInfo*>> variables_vector;
	if (classes)
	for (auto lib : class_vector){
		for (auto _class : lib.second){
			for (auto variable : _class->variables){
				if (var && !variable->descriptions.size()){
					new_missing.push_back("var description:" + lib.first + "." + _class->name + "." + variable->name);
				}
			}

			for (auto method : _class->methods){
				if (!method->descriptions.size()){
					new_missing.push_back("class method description:" + lib.first + "." + _class->name + "." + method->method_string);
				}

				if (!method->suggestions.size()){
					new_missing.push_back("class method suggestion:" + lib.first + "." + _class->name + "." + method->method_string);
				}
			}
		}
	}

	if (functions)
	for (auto lib : function_vector){
		for (auto function : lib.second){

			if (!function->descriptions.size()){
				new_missing.push_back("function description:" + lib.first + "." + function->method_string);
			}

			if (!function->suggestions.size()){
				new_missing.push_back("function suggestion:" + lib.first + "." + function->method_string);
			}
			function->descriptions;
			function->suggestions;
		}
	}
	if (tables)
	for (auto lib : variables_vector){
		for (auto table : lib.second){
			if (!table->descriptions.size())
				new_missing.push_back("table description:" + lib.first + "." + table->name);
		}
	}
	return new_missing;
}

bool KeywordManager::registerClass(std::string class_name){
	//	return true;
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, class_name);
	if (class_info_ptr){
		last_error = "table name " + class_name + " already is registered";
		return false;
	}
	class_vector[current_lib_parsing].push_back(new LuaTableInfo(class_name, lua_table_t::lua_table_class));
	return true;
}

bool KeywordManager::registerTable(std::string table_name){
	//	return true;
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, table_name);
	if (class_info_ptr){
		last_error = "table name " + table_name + " already is registered";
		return false;
	}
	class_vector[current_lib_parsing].push_back(new LuaTableInfo(table_name, lua_table_t::lua_table_table));
	return true;
}

bool KeywordManager::registerMetatable(std::string metatable_name){
	//	return true;
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, metatable_name);
	if (class_info_ptr){
		last_error = "table name " + metatable_name + " already is registered";
		return false;
	}
	class_vector[current_lib_parsing].push_back(new LuaTableInfo(metatable_name, lua_table_t::lua_table_metatable));
	return true;
}

bool KeywordManager::registerEnumTable(std::string enum_table){
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, enum_table);
	if (class_info_ptr){
		last_error = "table name " + enum_table + " already is registered";
		return false;
	}
	class_vector[current_lib_parsing].push_back(new LuaTableInfo(enum_table, lua_table_t::lua_table_enum));
	return true;
}

bool KeywordManager::registerTableVariable(std::string table_name, std::string var_name){
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, table_name);
	if (!class_info_ptr){
		return false;
	}

	return class_info_ptr->add_variable(var_name);
}

void KeywordManager::addTableVariableDescription(std::string table_name, std::string variable, std::string description){
	string_util::trim(description);
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, table_name);
	if (!class_info_ptr){
		return;
	}
	class_info_ptr->add_variable_description(variable, description);
}

void KeywordManager::addVariableDescription(std::string variable, std::string description){
	string_util::trim(description);
	std::vector<LuaVarInfo*>& var_v = variables_vector[current_lib_parsing];
	auto found = std::find_if(var_v.begin(), var_v.end(), [&](LuaVarInfo* info){ return info->name == variable; });
	if (found == var_v.end()){
		return;
	}
	(*found)->add_description(description);
	return;
}

bool KeywordManager::addVariable(std::string variable){
	std::vector<LuaVarInfo*>& var_v = variables_vector[current_lib_parsing];
	auto found = std::find_if(var_v.begin(), var_v.end(), [&](LuaVarInfo* info){ return info->name == variable; });
	if (found != var_v.end())
		return false;
	
	LuaVarInfo* info = new LuaVarInfo(variable);
	var_v.push_back(info);
	return true;
}

bool LuaTableInfo::add_variable(std::string variable){
	auto found = std::find_if(variables.begin(), variables.end(), [&](LuaVarInfo* var) -> bool{ return var->name == variable; });
	if (found != variables.end()){
		return false;
	}
	variables.push_back(new LuaVarInfo(variable));
	return true;
}

bool LuaTableInfo::add_variable_description(std::string variable_name, std::string description){
	string_util::trim(description);
	auto found = std::find_if(variables.begin(), variables.end(), [&](LuaVarInfo* var) -> bool{ return var->name == variable_name; });
	if (found != variables.end()){
		(*found)->add_description(description);
	}
	return false;
}

bool KeywordManager::registerTableMethod(std::string class_name, std::string method){
	//return true;
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, class_name);
	if (!class_info_ptr){
		return false;
	}

	return class_info_ptr->add_method(method);
}

bool KeywordManager::addTableDescription(std::string class_name, std::string description){
	string_util::trim(description);
	return true;
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, class_name);
	if (!class_info_ptr){
		return false;
	}

	return class_info_ptr->add_description(description);
}

bool KeywordManager::addMethodDescription(std::string class_name, std::string method, std::string description){
	string_util::trim(description);
	//return true;
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, class_name);
	if (!class_info_ptr){
		return false;
	}

	class_info_ptr->add_method_description(method, description);
	return true;
}

bool KeywordManager::addMethodSuggestion(std::string class_name, std::string method, std::string autoCompleteText){
	//return true;
	resetLastError();
	LuaTableInfo* class_info_ptr = get_class_by_class_string(current_lib_parsing, class_name);
	if (!class_info_ptr){
		return false;
	}
	class_info_ptr->add_method_suggestion(method, autoCompleteText);
	return true;
}

bool KeywordManager::registerFunction(std::string function){
	//return true;
	LuaFunctionMehotdInfo* funtion_info_ptr = get_function_by_function_string(current_lib_parsing, function);
	if (!funtion_info_ptr){
		function_vector[current_lib_parsing].push_back(new LuaFunctionMehotdInfo(function));
		return true;
	}
	return false;
}

bool KeywordManager::addFunctionDescription(std::string function, std::string description){
	string_util::trim(description);
	//return true;
	LuaFunctionMehotdInfo* funtion_info_ptr = get_function_by_function_string(current_lib_parsing, function);
	if (funtion_info_ptr){
		return funtion_info_ptr->add_description(description);
	}
	return false;
}

bool KeywordManager::addFunctionSuggestion(std::string function, std::string autocompleteText){
	//return true;
	LuaFunctionMehotdInfo* funtion_info_ptr = get_function_by_function_string(current_lib_parsing, function);
	if (funtion_info_ptr){
		return funtion_info_ptr->add_suggestion(autocompleteText);
	}
	return false;
}

std::string KeywordManager::get_lib_description(std::string libname){
	return "";
}

std::string KeywordManager::get_table_description(std::string libname, std::string class_name){
	auto class_info_ptr = get_class_by_class_string(libname, class_name);
	if (!class_info_ptr)
		return "";
	std::string retval = "";
	for (auto description = class_info_ptr->descriptions.begin(); description != class_info_ptr->descriptions.end();
		description++){
		retval += *description + "\n";
	}
	return retval;
}

std::string KeywordManager::get_method_description(std::string libname, std::string class_name, std::string method_name){
	auto class_info_ptr = get_class_by_class_string(libname, class_name);
	if (!class_info_ptr)
		return "";
	auto method_info_ptr = class_info_ptr->get_method_by_name(method_name);
	if (!method_info_ptr)
		return "";

	std::string retval = "";
	for (auto description = method_info_ptr->descriptions.begin(); description != method_info_ptr->descriptions.end();
		description++){
		retval += *description + "\n";
	}
	return retval;
}

std::string KeywordManager::get_table_variable_description(std::string lib_name, std::string table_name, std::string variable_name){
	auto table_info_ptr = get_class_by_class_string(lib_name, table_name);
	if (!table_info_ptr)
		return "";

	auto var_ptr = table_info_ptr->get_var_by_name(variable_name);
	if (!var_ptr)
		return "";

	std::string retval = "";
	for (auto description = var_ptr->descriptions.begin(); description != var_ptr->descriptions.end();
		description++){
		retval += *description + "\n";
	}
	return retval;
}

std::string KeywordManager::get_function_description(std::string libname, std::string function_name){
	auto function_info_ptr = get_function_by_function_string(libname, function_name);
	if (!function_info_ptr)
		return "";
	std::string retval = "";
	for (auto description = function_info_ptr->descriptions.begin(); description != function_info_ptr->descriptions.end();
		description++){
		retval += *description + "\n";
	}
	return retval;
}




KeywordManager* KeywordManager::get(){
	static KeywordManager* m = nullptr;
	if (!m)
		m = new KeywordManager();
	return m;
}
