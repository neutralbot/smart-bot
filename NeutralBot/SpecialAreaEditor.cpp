#include "SpecialAreaEditor.h"
#include "ManagedUtil.h"
#include "LanguageManager.h"
#include "WaypointManager.h"

using namespace NeutralBot;
System::Void SpecialAreaEditor::LoadAllData(){
	disable_update = true;
	DropDownListCondition->BeginUpdate();
	for (int i = 0; i < special_area_activation_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("special_area_activation_t_") + std::to_string(i));
		item->Value = i;
		DropDownListCondition->Items->Add(item);
	}
	DropDownListCondition->SelectedIndex = 0;
	DropDownListCondition->EndUpdate();

	DropDownListEditEvent->BeginUpdate(); 
	for (int i = 0; i < special_area_event_t::special_area_event_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("special_area_event_t_") + std::to_string(i));
		item->Value = i;
		DropDownListEditEvent->Items->Add(item);
	}
	DropDownListEditEvent->SelectedIndex = 0;
	DropDownListEditEvent->EndUpdate();

	disable_update = false;

	SAArea* root = SpecialAreaManager::get()->get_root();
	std::vector<SABaseElement*> childrens = root->get_childrens();
	for (auto child = childrens.begin(); child != childrens.end(); child++){

		switch ((*child)->get_type()){
		case special_area_element_t::sa_element_area:{
														 minimap_set_area_pos(Convert::ToString((uint32_t)*child), (SAArea*)*child);
		}
			break;
		case special_area_element_t::sa_element_point:{
														  minimap_set_point_pos(Convert::ToString((uint32_t)*child), (SAPointElement*)*child);

		}
			break;
		case special_area_element_t::sa_element_wall:{
														 minimap_set_wall_pos(Convert::ToString((uint32_t)*child), (SAWallElement*)*child);
		}
			break;
		}
	}
}

System::Void SpecialAreaEditor::radTreeView1_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
}

void SpecialAreaEditor::DeleteSelected(){

}

void SpecialAreaEditor::AddNew(SABaseElement* add_new_element){
	special_area_element_t type = (special_area_element_t)add_new_element->get_type();

	//special_area_element_t::sa_element_area;
	//special_area_element_t::sa_element_point;
	//special_area_element_t::sa_element_wall;

}

void SpecialAreaEditor::AddPoint(SAPointElement* point){
	if (point->get_owner()){

	}
}

void SpecialAreaEditor::AddArea(SAArea* area){

}

void SpecialAreaEditor::AddWall(SAWallElement* wall){

}

void SpecialAreaEditor::ShowSelectedMenuOptions(){

}

void SpecialAreaEditor::ShowSelectedEditProperties(){

}

void SpecialAreaEditor::UpdateSelectedProperties(){

}
System::Void SpecialAreaEditor::timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	/*	auto coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	miniMapControl1->set_current_coordinate(coord.x, coord.y, coord.z);*/
}
System::Void SpecialAreaEditor::button1_Click(System::Object^  sender, System::EventArgs^  e) {
	/*auto coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	miniMapControl1->set_to_draw_effect_point("EFFECTTEST", 0xffff0000, gcnew MiniMap::Coordinate(
	System::Drawing::Point(coord.x, coord.y), coord.z), 0, 10);*/
}
System::Void SpecialAreaEditor::button2_Click(System::Object^  sender, System::EventArgs^  e) {
	/*auto current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	miniMapControl1->set_to_draw_line("LINETEST",
	System::Drawing::PointF(current_coord.x - 10, current_coord.y), System::Drawing::PointF(current_coord.x + 10, current_coord.y), 3, 0xff0000ff, 1);*/
}
System::Void SpecialAreaEditor::miniMapControl1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {

}

System::Void SpecialAreaEditor::minimapCreateNewLine(){

}
System::Void SpecialAreaEditor::radTreeView1_NodeMouseClick(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {

}
System::Void SpecialAreaEditor::menuNodeClick(System::Object^  sender, System::EventArgs^  e){
	/*if (lastMouseClickCoord)
	addCoordinate(lastMouseClickCoord, (waypoint_t)0);*/
}
bool editing = false;
System::Void SpecialAreaEditor::miniMapControl1_onCoordinateMouseDown(MiniMap::Coordinate^  __unnamed000, System::Windows::Forms::MouseEventArgs^  e) {

	if (e->Button == System::Windows::Forms::MouseButtons::Left){
		editing = true;
	}
	else if (e->Button == System::Windows::Forms::MouseButtons::Right){

	}
}
System::Void SpecialAreaEditor::miniMapControl1_onCoordinateMouseUp(MiniMap::Coordinate^  __unnamed000, System::Windows::Forms::MouseEventArgs^  e) {
	if (editingElement){
		editing = false;
		miniMapControl1->remove_named_object_by_name("EDITING_CONTROL");
		miniMapControl1->remove_named_object_by_name("EDITING_CONTROL_avoidance");
		miniMapControl1->remove_named_object_by_name("EDITING_CONTROL_walkability");


		special_area_element_t type = (special_area_element_t)editingElement->get_type();
		switch (type)
		{
		case special_area_element_t::sa_element_area:{
														 minimap_set_area_pos(Convert::ToString((uint32_t)editingElement), (SAArea*)editingElement);
		}
			break;
		case special_area_element_t::sa_element_point:{
														  minimap_set_point_pos(Convert::ToString((uint32_t)editingElement), (SAPointElement*)editingElement);
		}
			break;
		case special_area_element_t::sa_element_wall:{
														 minimap_set_wall_pos(Convert::ToString((uint32_t)editingElement), (SAWallElement*)editingElement);
		}
		}
		editingElement = nullptr;
	}
	else{
		if (radDropDownList1->SelectedIndex <= 0){
			editing = false;
			return;
		}

		if (!editing){
			return;
		}

		last_coordinate_mouse_down = __unnamed000;
		special_area_element_t type = (special_area_element_t)(radDropDownList1->SelectedIndex - 1);

		if (type == special_area_element_t::sa_element_point){
			MessageBox::Show("Sorry this option is under development please try other one!", "Warning!");
			return;
		}
		auto root = SpecialAreaManager::get()->get_root();
		switch (type)
		{
		case special_area_element_t::sa_element_area:{
														 SAArea* new_area = (SAArea*)root->create_child(special_area_element_t::sa_element_area);
														 new_area->start_x = last_coordinate_mouse_down->x;
														 new_area->start_y = last_coordinate_mouse_down->y;
														 new_area->end_x = __unnamed000->x;
														 new_area->end_y = __unnamed000->y;
														 new_area->z = __unnamed000->z;

														 minimap_set_area_pos("EDITING_CONTROL", new_area);

														 editingElement = new_area;
		}
			break;
		case special_area_element_t::sa_element_point:{
														  SAPointElement* new_point = (SAPointElement*)root->create_child(special_area_element_t::sa_element_point);

														  new_point->x = __unnamed000->x;
														  new_point->y = __unnamed000->y;
														  new_point->z = __unnamed000->z;

														  minimap_set_point_pos("EDITING_CONTROL", new_point);
														  editingElement = new_point;
		}
			break;
		case special_area_element_t::sa_element_wall:{
														 SAWallElement* new_wall = (SAWallElement*)root->create_child(special_area_element_t::sa_element_wall);

														 new_wall->start_x = last_coordinate_mouse_down->x;
														 new_wall->start_y = last_coordinate_mouse_down->y;
														 new_wall->end_x = __unnamed000->x;
														 new_wall->end_y = __unnamed000->y;
														 new_wall->z = __unnamed000->z;
														 minimap_set_wall_pos("EDITING_CONTROL", new_wall);

														 editingElement = new_wall;
		}
			break;
		}
	}
}

System::Void SpecialAreaEditor::miniMapControl1_onCoordinateMouseMove(MiniMap::Coordinate^  __unnamed000, System::Windows::Forms::MouseEventArgs^  e) {

	if (editingElement){
		special_area_element_t editing_type = editingElement->get_type();
		switch (editing_type)
		{
		case special_area_element_t::sa_element_area:{
														 SAArea* new_area = (SAArea*)editingElement;
														 new_area->start_x = last_coordinate_mouse_down->x;
														 new_area->start_y = last_coordinate_mouse_down->y;

														 new_area->end_x = __unnamed000->x;
														 new_area->end_y = __unnamed000->y;
														 new_area->z = __unnamed000->z;

														 minimap_set_area_pos("EDITING_CONTROL", new_area);

														 editingElement = new_area;
		}
			break;
		case special_area_element_t::sa_element_point:{

														  SAPointElement* new_point = (SAPointElement*)editingElement;

														  new_point->x = __unnamed000->x;
														  new_point->y = __unnamed000->y;
														  new_point->z = __unnamed000->z;

														  minimap_set_point_pos("EDITING_CONTROL", new_point);
														  editingElement = new_point;
		}
			break;
		case special_area_element_t::sa_element_wall:{
														 SAWallElement* new_wall = (SAWallElement*)editingElement;

														 new_wall->start_x = last_coordinate_mouse_down->x;
														 new_wall->start_y = last_coordinate_mouse_down->y;

														 new_wall->end_x = __unnamed000->x;
														 new_wall->end_y = __unnamed000->y;
														 new_wall->z = __unnamed000->z;

														 minimap_set_wall_pos("EDITING_CONTROL", new_wall);

														 editingElement = new_wall;
		}
			break;
		}
	}
	else{

	}
}

void SpecialAreaEditor::AddOnListView(uint32_t id, String^ text){
	for each(ListViewDataItem^ item in radListView1->Items){
		if ((uint32_t)item->Value == id)
			return;
	}
	int current_count = radListView1->Items->Count;
	Telerik::WinControls::UI::ListViewDataItem^ new_item
		= gcnew Telerik::WinControls::UI::ListViewDataItem((uint32_t)id);
	new_item->Value = id;
	radListView1->Items->Add(new_item);
	new_item["Elements"] = text;
	if (!current_count || radListView1->SelectedIndex == -1){
		LoadGrid();
	}
}

void SpecialAreaEditor::minimap_set_wall_pos(String^ identifier, SAWallElement* wall){
	if (identifier->IndexOf("EDITING_") == -1){
		AddOnListView((uint32_t)wall, gcnew String(&((SABaseElement*)wall)->get_name()[0]));
	}

	miniMapControl1->set_to_draw_effect_wall(identifier, 0xcc000000,
		gcnew MiniMap::Coordinate(wall->start_x, wall->start_y, wall->z),
		gcnew MiniMap::Coordinate(wall->end_x, wall->end_y, wall->z), wall->width);
}

void SpecialAreaEditor::minimap_set_point_pos(String^ identifier, SAPointElement* point){
	if (identifier->IndexOf("EDITING_") == -1){
		AddOnListView((uint32_t)point, gcnew String(&((SABaseElement*)point)->get_name()[0]));
	}

	miniMapControl1->set_to_draw_effect_point(identifier + "_avoidance", 0x90FF00FF,
		gcnew MiniMap::Coordinate(point->x, point->y, point->z), point->get_avoidance()->decay, point->get_avoidance()->range);
	miniMapControl1->set_to_draw_effect_point(identifier + "_walkability", 0x90FF0000,
		gcnew MiniMap::Coordinate(point->x, point->y, point->z), point->get_walk_dificult()->decay, point->get_walk_dificult()->range);
}

void SpecialAreaEditor::minimap_set_area_pos(String^ identifier, SAArea* area){
	if (identifier->IndexOf("EDITING_") == -1){
		AddOnListView((uint32_t)area, gcnew String(&((SABaseElement*)area)->get_name()[0]));
	}

	area->normalize_negative();
	miniMapControl1->set_do_draw_rectangle(identifier, area->start_x,
		area->start_y, area->z, area->end_x - area->start_x, area->end_y - area->start_y, 0xaa000000, true, area->width);
}

void SpecialAreaEditor::minimap_set_base_element(String^ identifier, SABaseElement* element){
	switch (element->get_type()){
	case special_area_element_t::sa_element_area:{
													 minimap_set_area_pos(identifier, (SAArea*)element);
	}
		break;
	case special_area_element_t::sa_element_point:{
													  minimap_set_point_pos(identifier, (SAPointElement*)element);
	}
		break;
	case special_area_element_t::sa_element_wall:{
													 minimap_set_wall_pos(identifier, (SAWallElement*)element);
	}
		break;
	}
	redraw();
}

System::Void SpecialAreaEditor::radDropDownList1_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {

}

SABaseElement* SpecialAreaEditor::getSelectedElement(){
	if (!radListView1->SelectedItem)
		return nullptr;

	ListViewDataItem^ data_item = radListView1->SelectedItem;
	auto root = SpecialAreaManager::get()->get_root();
	uint32_t id = (uint32_t)data_item->Value;
	return (SABaseElement*)root->get_childrens_by_raw_pointer((SABaseElement*)id, true);
}

Telerik::WinControls::UI::RadPropertyStore^ SpecialAreaEditor::getGrid(SABaseElement* element){

	special_area_element_t type = element->get_type();
	RadPropertyStore^ radPropertyStore = (gcnew RadPropertyStore());
	PropertyStoreItem^ elementType = (gcnew PropertyStoreItem(String::typeid, "Type", nullptr, "Element Type"));
	elementType->ReadOnly = true;

	radPropertyStore->Add(elementType);
	radPropertyStore->Add(gcnew PropertyStoreItem(System::String::typeid, "Name", nullptr, "Element Name"));
	radPropertyStore["Name"]->Value = gcnew String(&element->get_name()[0]);

	/*radPropertyStore->Add(gcnew PropertyStoreItem(managed_special_area_activation_t::typeid, "ActivationTrigger", nullptr, "Activation Modes."));
	radPropertyStore["ActivationTrigger"]->Value = (managed_special_area_activation_t)element->get_activation_trigger();*/

	radPropertyStore->Add(gcnew PropertyStoreItem(System::Boolean::typeid, "State", nullptr, "Element enabled/disabled."));
	radPropertyStore["State"]->Value = element->get_state();

	switch (element->get_type()){
	case special_area_element_t::sa_element_area:{
													 radPropertyStore["Type"]->Value = "Area Element";
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "X", nullptr, "Area Position X"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "Y", nullptr, "Area Position Y"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::Int32::typeid, "Z", nullptr, "Area Position Z"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "Width", nullptr, "Area Width"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "Height", nullptr, "Area Height"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "BorderWidth", nullptr, "The area border line width."));
													 /*
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "WalkDificultValue", nullptr, "Point region walkability dificult Y."));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::Double::typeid, "WalkDificultDecay", nullptr, "Point region wallkability decay per sqm."));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "WalkDificultRange", nullptr, "Point region wallkability range."));
													 */
													 SAArea* area = (SAArea*)element;
													 radPropertyStore["X"]->Value = (uint32_t)area->start_x;
													 radPropertyStore["Y"]->Value = (uint32_t)area->start_y;
													 radPropertyStore["Z"]->Value = area->z;
													 radPropertyStore["Width"]->Value = (uint32_t)(area->end_x - area->start_x);
													 radPropertyStore["Height"]->Value = (uint32_t)(area->end_y - area->start_y);

													 /*	auto walk_dif_ptr = area->get_walk_dificult();
														 radPropertyStore["WalkDificultDecay"]->Value = (float)walk_dif_ptr->decay;
														 radPropertyStore["WalkDificultValue"]->Value = (uint32_t)walk_dif_ptr->value;
														 radPropertyStore["WalkDificultRange"]->Value = (uint32_t)walk_dif_ptr->range;*/
													 radPropertyStore["BorderWidth"]->Value = (uint32_t)area->width;
	}
		break;
	case special_area_element_t::sa_element_point:{
													  radPropertyStore["Type"]->Value = "Point Element";
													  radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "X", nullptr, "Point Position X"));
													  radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "Y", nullptr, "Point Position Y"));
													  radPropertyStore->Add(gcnew PropertyStoreItem(System::Int32::typeid, "Z", nullptr, "Point Position Z"));

													  /*radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "WalkDificultValue", nullptr, "Point region walkability dificult Y."));
													  radPropertyStore->Add(gcnew PropertyStoreItem(System::Double::typeid, "WalkDificultDecay", nullptr, "Point region wallkability decay per sqm."));
													  radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "WalkDificultRange", nullptr, "Point region wallkability range."));

													  radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "AvoidanceValue", nullptr, "Point region avoidance value."));
													  radPropertyStore->Add(gcnew PropertyStoreItem(System::Double::typeid, "AvoidanceDecay", nullptr, "Point avoidance decay per sqm."));
													  radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "AvoidanceRange", nullptr, "Point avoidance range."));*/

													  SAPointElement* point = (SAPointElement*)element;

													  radPropertyStore["X"]->Value = (uint32_t)point->x;
													  radPropertyStore["Y"]->Value = (uint32_t)point->y;
													  radPropertyStore["Z"]->Value = point->z;

													  /*	auto walk_dif_ptr = point->get_walk_dificult();
														  radPropertyStore["WalkDificultDecay"]->Value = (double)walk_dif_ptr->decay;
														  radPropertyStore["WalkDificultValue"]->Value = (uint32_t)walk_dif_ptr->value;
														  radPropertyStore["WalkDificultRange"]->Value = (uint32_t)walk_dif_ptr->range;

														  auto avoidance = point->get_avoidance();
														  radPropertyStore["AvoidanceValue"]->Value = (uint32_t)avoidance->value;
														  radPropertyStore["AvoidanceDecay"]->Value = (double)avoidance->decay;
														  radPropertyStore["AvoidanceRange"]->Value = (uint32_t)avoidance->range;*/
	}
		break;
	case special_area_element_t::sa_element_wall:{
													 radPropertyStore["Type"]->Value = "Wall Element";
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "X", nullptr, "Point Position X"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "Y", nullptr, "Point Position Y"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::Int32::typeid, "Z", nullptr, "Point Position Z"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "EndX", nullptr, "Point Position X"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "EndY", nullptr, "Point Position Y"));
													 radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "Width", nullptr, "Wall width"));
													 //radPropertyStore->Add(gcnew PropertyStoreItem(System::Boolean::typeid, "Walkable", nullptr, "Can walk througth."));
													 //radPropertyStore->Add(gcnew PropertyStoreItem(System::UInt32::typeid, "WalkDificultValue", nullptr, "Can walk througth."));

													 SAWallElement* area = (SAWallElement*)element;
													 radPropertyStore["X"]->Value = (uint32_t)area->start_x;
													 radPropertyStore["Y"]->Value = (uint32_t)area->start_y;
													 radPropertyStore["Z"]->Value = area->z;
													 radPropertyStore["EndX"]->Value = (uint32_t)area->end_x;
													 radPropertyStore["EndY"]->Value = (uint32_t)area->end_y;
													 radPropertyStore["Width"]->Value = (uint32_t)area->get_width();

													 /*	auto walk_dif_ptr = area->get_walk_dificult();
														 radPropertyStore["WalkDificultValue"]->Value = (uint32_t)walk_dif_ptr->value;
														 radPropertyStore["Walkable"]->Value = area->get_block_walk() == true;*/
	}
		break;
	}

	return radPropertyStore;
}
	System::Void  SpecialAreaEditor::SpecialAreaEditor_Load(System::Object^  sender, System::EventArgs^  e) {
				 Coordinate coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
				 if (coord.is_null())
					 return;

				 miniMapControl1->set_current_coordinate(coord.x, coord.y, coord.z);

				 LoadAllData();

				 radDropDownList1->SelectedIndex = 0;

				 miniMapControl1->update(false);				
	}

	System::Void SpecialAreaEditor::radButtonPlus_Click(System::Object^  sender, System::EventArgs^  e) {
				 miniMapControl1->IncreaseZoom(1);
	}
void SpecialAreaEditor::LoadGrid(){
	ListViewTrigger->Items->Clear();

	if (!radListView1->SelectedItem){
		radPropertyGrid1->SelectedObject = nullptr;
		radPropertyGrid1->Enabled = false;
		return;
	}
	
	ListViewDataItem^ data_item = radListView1->SelectedItem;

	SAArea* root = SpecialAreaManager::get()->get_root();
	currentSABaseElement = root->get_childrens_by_raw_pointer((SABaseElement*)(uint32_t)data_item->Value, true);
	if (currentSABaseElement)
		radPropertyGrid1->SelectedObject = getGrid(currentSABaseElement);
	
	disable_update = true;
	std::vector<std::shared_ptr<SAActivationTrigger>> myVector = currentSABaseElement->get_vector_activation_trigger();
	for (auto trigger : myVector)
		addItemOnTrigger(GET_MANAGED_TR(std::string("special_area_activation_t_") + std::to_string((int)trigger->get_activation_trigger())));
	disable_update = false;

	radPropertyGrid1->Enabled = true;
	
	miniMapControl1->set_do_draw_rectangle("EDITING_", ((SAArea*)currentSABaseElement)->start_x,
		((SAArea*)currentSABaseElement)->start_y, ((SAArea*)currentSABaseElement)->z,
		((SAArea*)currentSABaseElement)->end_x - ((SAArea*)currentSABaseElement)->start_x,
		((SAArea*)currentSABaseElement)->end_y - ((SAArea*)currentSABaseElement)->start_y, 0x55ff0000, true, ((SAArea*)currentSABaseElement)->width);
	
	miniMapControl1->update(true);
}

System::Void SpecialAreaEditor::radListView1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	LoadGrid();
}

System::Void SpecialAreaEditor::radPropertyGrid1_Edited(System::Object^  sender, Telerik::WinControls::UI::PropertyGridItemEditedEventArgs^  e) {
	try{


		SABaseElement* base_el = getSelectedElement();
		if (!base_el || !e->Item)
			return;

		PropertyGridItem^ item = (PropertyGridItem^)e->Item;
		String^ name = item->Name;

		if (name == "Name"){
			String^ str = (String^)item->Value;
			base_el->set_name(managed_util::fromSS(str));
			radListView1->SelectedItem["Elements"] = str;
			return;
		}
		else if (name == "ActivationTrigger"){
			managed_special_area_activation_t temp = (managed_special_area_activation_t)item->Value;
			special_area_activation_t temp2 = (special_area_activation_t)temp;

			base_el->set_activation_trigger(temp2);
			return;
		}
		else if (name == "State"){
			base_el->set_state((bool)item->Value);
			return;
		}

		switch (base_el->get_type()){
		case special_area_element_t::sa_element_area:{
														 SAArea* area = (SAArea*)base_el;
														 if (name == "X"){
															 area->start_x = (uint32_t)item->Value;
														 }
														 else if (name == "Y"){
															 area->start_y = (uint32_t)item->Value;
														 }
														 else if (name == "Z"){
															 area->z = (int32_t)item->Value;
														 }
														 else if (name == "Width"){
															 area->end_x = area->start_x + (uint32_t)item->Value;
														 }
														 else if (name == "Height"){
															 area->end_y = area->start_y + (uint32_t)item->Value;
														 }
														 else if (name == "WalkDificultDecay"){
															 area->get_walk_dificult()->decay = (float)item->Value;
														 }
														 else if (name == "WalkDificultValue"){
															 area->get_walk_dificult()->value = (uint32_t)item->Value;
														 }
														 else if (name == "WalkDificultRange"){
															 area->get_walk_dificult()->range = (uint32_t)item->Value;
														 }
														 else if (name == "BorderWidth"){
															 area->width = ((uint32_t)item->Value);
														 }

		}
			break;
		case special_area_element_t::sa_element_point:{
														  SAPointElement* point = (SAPointElement*)base_el;
														  if (name == "X"){
															  point->x = (uint32_t)item->Value;
														  }
														  else if (name == "Y"){
															  point->y = (uint32_t)item->Value;
														  }
														  else if (name == "Z"){
															  point->z = (int32_t)item->Value;
														  }
														  else if (name == "WalkDificultDecay"){
															  point->get_walk_dificult()->decay = (float)(Double)item->Value;
														  }
														  else if (name == "WalkDificultValue"){
															  point->get_walk_dificult()->value = (uint32_t)item->Value;
														  }
														  else if (name == "WalkDificultRange"){
															  point->get_walk_dificult()->range = (uint32_t)item->Value;
														  }
														  else if (name == "AvoidanceDecay"){
															  point->get_avoidance()->decay = (float)(Double)item->Value;
														  }
														  else if (name == "AvoidanceValue"){
															  point->get_avoidance()->value = (uint32_t)item->Value;
														  }
														  else if (name == "AvoidanceRange"){
															  point->get_avoidance()->range = (uint32_t)item->Value;
														  }
		}
			break;
		case special_area_element_t::sa_element_wall:{
														 SAWallElement* wall = (SAWallElement*)base_el;
														 if (name == "X"){
															 wall->start_x = (uint32_t)item->Value;
														 }
														 else if (name == "Y"){
															 wall->start_y = (uint32_t)item->Value;
														 }
														 else if (name == "Z"){
															 wall->z = (int32_t)item->Value;
														 }
														 else if (name == "EndX"){
															 wall->end_x = (uint32_t)item->Value;
														 }
														 else if (name == "EndY"){
															 wall->end_y = (uint32_t)item->Value;
														 }
														 else if (name == "Width"){
															 wall->width = (uint32_t)item->Value;
														 }
														 else if (name == "Walkable"){
															 wall->set_block_walk((System::Boolean)item->Value);
														 }
														 else if (name == "WalkDificultValue"){
															 wall->get_walk_dificult()->value = (uint32_t)item->Value;
														 }
		}
			break;
		}
		minimap_set_base_element(Convert::ToString((uint32_t)base_el), base_el);
		redraw();
	}
	catch (Exception^ ex){
		MessageBox::Show(ex->Message, "Error!");
	}


}
System::Void SpecialAreaEditor::radListView1_ItemRemoved(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
	miniMapControl1->remove_named_object_by_name(Convert::ToString((uint32_t)e->Item->Value));
	miniMapControl1->remove_named_object_by_name(Convert::ToString((uint32_t)e->Item->Value) + "_avoidance");
	miniMapControl1->remove_named_object_by_name(Convert::ToString((uint32_t)e->Item->Value) + "_walkability");
	SpecialAreaManager::get()->get_root()->remove_element((SABaseElement*)(uint32_t)e->Item->Value, true);
	redraw();
	miniMapControl1->update(true);
}

System::Void SpecialAreaEditor::radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
}


System::Void SpecialAreaEditor::redraw(){
	miniMapControl1->request_redraw(true);
}

void SpecialAreaEditor::ShowUnique(){
	System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;

	if (unique == nullptr)
		unique = gcnew SpecialAreaEditor();

	System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
	unique->BringToFront();
	unique->Show();
}

SpecialAreaEditor::SpecialAreaEditor(void){
	editingElement = nullptr;
	this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
	InitializeComponent();

	NeutralBot::FastUIPanelController::get()->install_controller(this);

	MenuAdvanced_Click(nullptr, nullptr);

	std::vector<Coordinate> lurePoints = WaypointManager::get()->get_all_coordinate_lure();
	if (!lurePoints.size())
		return;
	
	int i = 0;
	for (Coordinate coord : lurePoints){
		if (coord.is_null())
			continue;

		//miniMapControl1->set_to_draw_point("character_position" + i, gcnew System::Drawing::Point((float)coord.x, (float)coord.y));

		i++;
	}

	miniMapControl1->update(false);
}

void SpecialAreaEditor::CloseUnique(){
	if (unique)if (unique)unique->Close();
}

SpecialAreaEditor::~SpecialAreaEditor(){
	if (components)
	{
		delete components;
	}
	unique = nullptr;
}