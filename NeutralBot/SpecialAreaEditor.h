#pragma once
#include "SpecialAreas.h"
#include "Core\\TibiaProcess.h"
#include "FastUIPanelController.h"
#include "LuaBackground.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections; 
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Telerik::WinControls::UI;

	public enum class managed_special_area_activation_t{
		InsideArea = 0,
		WithTarget,
		OnHunterListCreature,
		OnCreature, 
		OnPlayer,
		AfterLured,
		Luring,
		LurePointNotReachable,
		ManualActivationLua
	};


	public ref class SpecialAreaEditor : public Telerik::WinControls::UI::RadForm{
	public:
		SpecialAreaEditor(void);
		static void CloseUnique();
	protected:
		~SpecialAreaEditor();


	public: static void HideUnique(){
				if (unique)unique->Hide();
	}

			MiniMap::Coordinate^ last_coordinate_mouse_down;
			SABaseElement* editingElement;
			System::Windows::Forms::Timer^  timer1;
			Telerik::WinControls::UI::RadContextMenu^  miniMapMenu;
			Telerik::WinControls::UI::RadPanel^  radPanel1;
			Telerik::WinControls::UI::RadDropDownList^  radDropDownList1;
			Telerik::WinControls::UI::RadListView^  radListView1;
			Telerik::WinControls::UI::RadPropertyGrid^  radPropertyGrid1;
	protected: Telerik::WinControls::UI::RadRepeatButton^  radRepeatButton2;

	private: Telerik::WinControls::UI::RadButton^  radButtonDown;
	protected:
	private: Telerik::WinControls::UI::RadButton^  radButtonUp;
	private: Telerik::WinControls::UI::RadButton^  radButtonMinus;
	private: Telerik::WinControls::UI::RadButton^  radButtonPlus;
	private: Telerik::WinControls::UI::RadMenu^  radMenu1;
	private: Telerik::WinControls::UI::RadListView^  ListViewTrigger;
	private: Telerik::WinControls::UI::RadMenuItem^  MenuAdvanced;
	private: Telerik::WinControls::UI::RadButton^  ButtonAdd;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListCondition;
	private: Telerik::WinControls::UI::RadButton^  ButtonRemove;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
	private: Telerik::WinControls::UI::RadButton^  ButtonEditEvent;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListEditEvent;
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadPanel^  radPanel2;
	protected:
	protected:
	protected: static SpecialAreaEditor^ unique;
	public: static void ShowUnique();
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew SpecialAreaEditor();
	}
	public: static void ReloadForm(){
				unique->SpecialAreaEditor_Load(nullptr, nullptr);
	}

			MiniMap::MiniMapControl^  miniMapControl1;
	private: System::ComponentModel::IContainer^  components;
	public:
	protected:

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"ID",
				L"ID"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Elements",
				L"Elements"));
			Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"ActivationTrigger",
				L"ActivationTrigger"));
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(SpecialAreaEditor::typeid));
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->miniMapMenu = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radButtonDown = (gcnew Telerik::WinControls::UI::RadButton());
			this->radButtonUp = (gcnew Telerik::WinControls::UI::RadButton());
			this->radButtonMinus = (gcnew Telerik::WinControls::UI::RadButton());
			this->radButtonPlus = (gcnew Telerik::WinControls::UI::RadButton());
			this->radRepeatButton2 = (gcnew Telerik::WinControls::UI::RadRepeatButton());
			this->radDropDownList1 = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->miniMapControl1 = (gcnew MiniMap::MiniMapControl());
			this->ButtonRemove = (gcnew Telerik::WinControls::UI::RadButton());
			this->ButtonAdd = (gcnew Telerik::WinControls::UI::RadButton());
			this->radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
			this->radPropertyGrid1 = (gcnew Telerik::WinControls::UI::RadPropertyGrid());
			this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
			this->MenuAdvanced = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->ListViewTrigger = (gcnew Telerik::WinControls::UI::RadListView());
			this->DropDownListCondition = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->ButtonEditEvent = (gcnew Telerik::WinControls::UI::RadButton());
			this->DropDownListEditEvent = (gcnew Telerik::WinControls::UI::RadDropDownList());
			this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonDown))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonUp))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonMinus))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonPlus))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radRepeatButton2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownList1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemove))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonAdd))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPropertyGrid1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewTrigger))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListCondition))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
			this->radGroupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
			this->radGroupBox2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonEditEvent))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListEditEvent))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &SpecialAreaEditor::timer1_Tick);
			// 
			// radPanel1
			// 
			this->radPanel1->AutoSize = true;
			this->radPanel1->Controls->Add(this->radButtonDown);
			this->radPanel1->Controls->Add(this->radButtonUp);
			this->radPanel1->Controls->Add(this->radButtonMinus);
			this->radPanel1->Controls->Add(this->radButtonPlus);
			this->radPanel1->Controls->Add(this->radRepeatButton2);
			this->radPanel1->Controls->Add(this->radDropDownList1);
			this->radPanel1->Controls->Add(this->miniMapControl1);
			this->radPanel1->Location = System::Drawing::Point(0, 26);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(418, 420);
			this->radPanel1->TabIndex = 12;
			this->radPanel1->Text = L"radPanel1";
			// 
			// radButtonDown
			// 
			this->radButtonDown->Location = System::Drawing::Point(38, 315);
			this->radButtonDown->Name = L"radButtonDown";
			this->radButtonDown->Size = System::Drawing::Size(26, 28);
			this->radButtonDown->TabIndex = 23;
			this->radButtonDown->Text = L"v";
			this->radButtonDown->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::radButtonDown_Click);
			// 
			// radButtonUp
			// 
			this->radButtonUp->Location = System::Drawing::Point(38, 286);
			this->radButtonUp->Name = L"radButtonUp";
			this->radButtonUp->Size = System::Drawing::Size(26, 28);
			this->radButtonUp->TabIndex = 23;
			this->radButtonUp->Text = L"^";
			this->radButtonUp->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::radButtonUp_Click);
			// 
			// radButtonMinus
			// 
			this->radButtonMinus->Location = System::Drawing::Point(10, 315);
			this->radButtonMinus->Name = L"radButtonMinus";
			this->radButtonMinus->Size = System::Drawing::Size(26, 28);
			this->radButtonMinus->TabIndex = 23;
			this->radButtonMinus->Text = L"-";
			this->radButtonMinus->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::radButtonMinus_Click);
			// 
			// radButtonPlus
			// 
			this->radButtonPlus->Location = System::Drawing::Point(10, 286);
			this->radButtonPlus->Name = L"radButtonPlus";
			this->radButtonPlus->Size = System::Drawing::Size(26, 28);
			this->radButtonPlus->TabIndex = 22;
			this->radButtonPlus->Text = L"+";
			this->radButtonPlus->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::radButtonPlus_Click);
			// 
			// radRepeatButton2
			// 
			this->radRepeatButton2->Location = System::Drawing::Point(253, 0);
			this->radRepeatButton2->Name = L"radRepeatButton2";
			// 
			// 
			// 
			this->radRepeatButton2->RootElement->Opacity = 0.8;
			this->radRepeatButton2->Size = System::Drawing::Size(144, 24);
			this->radRepeatButton2->TabIndex = 21;
			this->radRepeatButton2->Text = L"Show Current Coordinate";
			this->radRepeatButton2->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::radRepeatButton2_Click);
			(cli::safe_cast<Telerik::WinControls::UI::RadRepeatButtonElement^>(this->radRepeatButton2->GetChildAt(0)))->Text = L"Show Current Coordinate";
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->radRepeatButton2->GetChildAt(0)->GetChildAt(0)))->Opacity = 1;
			// 
			// radDropDownList1
			// 
			this->radDropDownList1->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			radListDataItem1->Text = L"Move Map";
			radListDataItem2->Text = L"Add Area";
			radListDataItem3->Text = L"Add Point";
			radListDataItem4->Text = L"Add Wall";
			this->radDropDownList1->Items->Add(radListDataItem1);
			this->radDropDownList1->Items->Add(radListDataItem2);
			this->radDropDownList1->Items->Add(radListDataItem3);
			this->radDropDownList1->Items->Add(radListDataItem4);
			this->radDropDownList1->Location = System::Drawing::Point(13, 3);
			this->radDropDownList1->Name = L"radDropDownList1";
			this->radDropDownList1->NullText = L"Edition Mode";
			this->radDropDownList1->Size = System::Drawing::Size(128, 20);
			this->radDropDownList1->TabIndex = 0;
			this->radDropDownList1->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &SpecialAreaEditor::radDropDownList1_SelectedIndexChanged);
			// 
			// miniMapControl1
			// 
			this->miniMapControl1->cursor_image = nullptr;
			this->miniMapControl1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->miniMapControl1->dragingWaypoint = nullptr;
			this->miniMapControl1->hoveredWaypoint = nullptr;
			this->miniMapControl1->Location = System::Drawing::Point(0, 0);
			this->miniMapControl1->Name = L"miniMapControl1";
			this->miniMapControl1->selected_waypoint_image = nullptr;
			this->miniMapControl1->selectedWaypoint = nullptr;
			this->miniMapControl1->Size = System::Drawing::Size(418, 420);
			this->miniMapControl1->TabIndex = 1;
			this->miniMapControl1->waypoint_image = nullptr;
			this->miniMapControl1->onCoordinateMouseMove += gcnew MiniMap::delegateCoordinateMouseEvent(this, &SpecialAreaEditor::miniMapControl1_onCoordinateMouseMove);
			this->miniMapControl1->onCoordinateMouseUp += gcnew MiniMap::delegateCoordinateMouseEvent(this, &SpecialAreaEditor::miniMapControl1_onCoordinateMouseUp);
			this->miniMapControl1->onCoordinateMouseDown += gcnew MiniMap::delegateCoordinateMouseEvent(this, &SpecialAreaEditor::miniMapControl1_onCoordinateMouseDown);
			this->miniMapControl1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &SpecialAreaEditor::miniMapControl1_MouseClick);
			// 
			// ButtonRemove
			// 
			this->ButtonRemove->Location = System::Drawing::Point(513, 164);
			this->ButtonRemove->Name = L"ButtonRemove";
			this->ButtonRemove->Size = System::Drawing::Size(118, 24);
			this->ButtonRemove->TabIndex = 17;
			this->ButtonRemove->Text = L"Remove";
			this->ButtonRemove->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::ButtonRemove_Click);
			// 
			// ButtonAdd
			// 
			this->ButtonAdd->Location = System::Drawing::Point(382, 164);
			this->ButtonAdd->Name = L"ButtonAdd";
			this->ButtonAdd->Size = System::Drawing::Size(118, 24);
			this->ButtonAdd->TabIndex = 15;
			this->ButtonAdd->Text = L"Add";
			this->ButtonAdd->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::ButtonAdd_Click);
			// 
			// radListView1
			// 
			this->radListView1->AllowEdit = false;
			listViewDetailColumn1->HeaderText = L"ID";
			listViewDetailColumn1->Visible = false;
			listViewDetailColumn2->HeaderText = L"Elements";
			listViewDetailColumn2->MaxWidth = 150;
			listViewDetailColumn2->MinWidth = 150;
			listViewDetailColumn2->Width = 150;
			this->radListView1->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(2) {
				listViewDetailColumn1,
					listViewDetailColumn2
			});
			this->radListView1->ItemSpacing = -1;
			this->radListView1->Location = System::Drawing::Point(227, 3);
			this->radListView1->Name = L"radListView1";
			this->radListView1->Size = System::Drawing::Size(151, 346);
			this->radListView1->TabIndex = 11;
			this->radListView1->Text = L"radListView1";
			this->radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			this->radListView1->SelectedIndexChanged += gcnew System::EventHandler(this, &SpecialAreaEditor::radListView1_SelectedIndexChanged);
			this->radListView1->ItemRemoved += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &SpecialAreaEditor::radListView1_ItemRemoved);
			// 
			// radPropertyGrid1
			// 
			this->radPropertyGrid1->Enabled = false;
			this->radPropertyGrid1->Location = System::Drawing::Point(3, 3);
			this->radPropertyGrid1->Name = L"radPropertyGrid1";
			this->radPropertyGrid1->Size = System::Drawing::Size(225, 346);
			this->radPropertyGrid1->TabIndex = 0;
			this->radPropertyGrid1->Text = L"radPropertyGrid1";
			this->radPropertyGrid1->ToolbarVisible = true;
			this->radPropertyGrid1->Edited += gcnew Telerik::WinControls::UI::PropertyGridItemEditedEventHandler(this, &SpecialAreaEditor::radPropertyGrid1_Edited);
			// 
			// radMenu1
			// 
			this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->MenuAdvanced });
			this->radMenu1->Location = System::Drawing::Point(0, 0);
			this->radMenu1->Name = L"radMenu1";
			this->radMenu1->Size = System::Drawing::Size(1042, 20);
			this->radMenu1->TabIndex = 13;
			this->radMenu1->Text = L"radMenu1";
			// 
			// MenuAdvanced
			// 
			this->MenuAdvanced->AccessibleDescription = L"MenuAdvanced";
			this->MenuAdvanced->AccessibleName = L"MenuAdvanced";
			this->MenuAdvanced->Name = L"MenuAdvanced";
			this->MenuAdvanced->Text = L"Advanced";
			this->MenuAdvanced->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::MenuAdvanced_Click);
			// 
			// ListViewTrigger
			// 
			this->ListViewTrigger->AllowEdit = false;
			listViewDetailColumn3->HeaderText = L"ActivationTrigger";
			listViewDetailColumn3->MaxWidth = 248;
			listViewDetailColumn3->MinWidth = 248;
			listViewDetailColumn3->Width = 248;
			this->ListViewTrigger->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(1) { listViewDetailColumn3 });
			this->ListViewTrigger->ItemSpacing = -1;
			this->ListViewTrigger->Location = System::Drawing::Point(382, 194);
			this->ListViewTrigger->Name = L"ListViewTrigger";
			this->ListViewTrigger->Size = System::Drawing::Size(249, 155);
			this->ListViewTrigger->TabIndex = 14;
			this->ListViewTrigger->Text = L"radListView2";
			this->ListViewTrigger->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
			this->ListViewTrigger->SelectedItemChanged += gcnew System::EventHandler(this, &SpecialAreaEditor::ListViewTrigger_SelectedItemChanged);
			this->ListViewTrigger->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &SpecialAreaEditor::ListViewTrigger_ItemRemoving);
			// 
			// DropDownListCondition
			// 
			this->DropDownListCondition->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
			this->DropDownListCondition->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			this->DropDownListCondition->Location = System::Drawing::Point(5, 30);
			this->DropDownListCondition->Name = L"DropDownListCondition";
			this->DropDownListCondition->Size = System::Drawing::Size(235, 20);
			this->DropDownListCondition->TabIndex = 16;
			this->DropDownListCondition->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &SpecialAreaEditor::DropDownListCondition_SelectedIndexChanged);
			// 
			// radLabel1
			// 
			this->radLabel1->Location = System::Drawing::Point(5, 6);
			this->radLabel1->Name = L"radLabel1";
			this->radLabel1->Size = System::Drawing::Size(92, 18);
			this->radLabel1->TabIndex = 18;
			this->radLabel1->Text = L"ActivationTrigger";
			// 
			// radGroupBox1
			// 
			this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox1->Controls->Add(this->radLabel1);
			this->radGroupBox1->Controls->Add(this->DropDownListCondition);
			this->radGroupBox1->HeaderText = L"";
			this->radGroupBox1->Location = System::Drawing::Point(382, 96);
			this->radGroupBox1->Name = L"radGroupBox1";
			this->radGroupBox1->Size = System::Drawing::Size(249, 62);
			this->radGroupBox1->TabIndex = 19;
			// 
			// radGroupBox2
			// 
			this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox2->Controls->Add(this->ButtonEditEvent);
			this->radGroupBox2->Controls->Add(this->DropDownListEditEvent);
			this->radGroupBox2->Controls->Add(this->radLabel2);
			this->radGroupBox2->HeaderText = L"";
			this->radGroupBox2->Location = System::Drawing::Point(382, 3);
			this->radGroupBox2->Name = L"radGroupBox2";
			this->radGroupBox2->Size = System::Drawing::Size(249, 87);
			this->radGroupBox2->TabIndex = 20;
			// 
			// ButtonEditEvent
			// 
			this->ButtonEditEvent->Location = System::Drawing::Point(5, 54);
			this->ButtonEditEvent->Name = L"ButtonEditEvent";
			this->ButtonEditEvent->Size = System::Drawing::Size(239, 24);
			this->ButtonEditEvent->TabIndex = 18;
			this->ButtonEditEvent->Text = L"Edit Event";
			this->ButtonEditEvent->Click += gcnew System::EventHandler(this, &SpecialAreaEditor::ButtonEditEvent_Click);
			// 
			// DropDownListEditEvent
			// 
			this->DropDownListEditEvent->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			this->DropDownListEditEvent->Location = System::Drawing::Point(5, 28);
			this->DropDownListEditEvent->Name = L"DropDownListEditEvent";
			this->DropDownListEditEvent->Size = System::Drawing::Size(239, 20);
			this->DropDownListEditEvent->TabIndex = 1;
			this->DropDownListEditEvent->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &SpecialAreaEditor::DropDownListEditEvent_SelectedIndexChanged);
			// 
			// radLabel2
			// 
			this->radLabel2->Location = System::Drawing::Point(5, 6);
			this->radLabel2->Name = L"radLabel2";
			this->radLabel2->Size = System::Drawing::Size(56, 18);
			this->radLabel2->TabIndex = 0;
			this->radLabel2->Text = L"Edit Event";
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radPropertyGrid1);
			this->radPanel2->Controls->Add(this->radGroupBox2);
			this->radPanel2->Controls->Add(this->ListViewTrigger);
			this->radPanel2->Controls->Add(this->ButtonAdd);
			this->radPanel2->Controls->Add(this->ButtonRemove);
			this->radPanel2->Controls->Add(this->radGroupBox1);
			this->radPanel2->Controls->Add(this->radListView1);
			this->radPanel2->Location = System::Drawing::Point(403, 26);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(634, 352);
			this->radPanel2->TabIndex = 21;
			// 
			// SpecialAreaEditor
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1042, 384);
			this->Controls->Add(this->radPanel2);
			this->Controls->Add(this->radMenu1);
			this->Controls->Add(this->radPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"SpecialAreaEditor";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"SpecialAreaEditor";
			this->Load += gcnew System::EventHandler(this, &SpecialAreaEditor::SpecialAreaEditor_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			this->radPanel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonDown))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonUp))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonMinus))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButtonPlus))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radRepeatButton2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radDropDownList1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemove))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonAdd))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPropertyGrid1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewTrigger))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListCondition))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
			this->radGroupBox1->ResumeLayout(false);
			this->radGroupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
			this->radGroupBox2->ResumeLayout(false);
			this->radGroupBox2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonEditEvent))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListEditEvent))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		bool thisHide = false;

		System::Void LoadAllData();

		System::Void radTreeView1_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);

		void DeleteSelected();

		void AddNew(SABaseElement* add_new_element);

		void AddPoint(SAPointElement* point);

		void AddArea(SAArea* area);

		void AddWall(SAWallElement* wall);

		void ShowSelectedMenuOptions();

		void ShowSelectedEditProperties();

		void UpdateSelectedProperties();
		System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e);
		System::Void button1_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void button2_Click(System::Object^  sender, System::EventArgs^  e);
		System::Void miniMapControl1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);

		System::Void minimapCreateNewLine();
		System::Void radTreeView1_NodeMouseClick(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e);
		System::Void menuNodeClick(System::Object^  sender, System::EventArgs^  e);
		System::Void miniMapControl1_onCoordinateMouseDown(MiniMap::Coordinate^  __unnamed000, System::Windows::Forms::MouseEventArgs^  e);
		System::Void miniMapControl1_onCoordinateMouseUp(MiniMap::Coordinate^  __unnamed000, System::Windows::Forms::MouseEventArgs^  e);

		System::Void miniMapControl1_onCoordinateMouseMove(MiniMap::Coordinate^  __unnamed000, System::Windows::Forms::MouseEventArgs^  e);

		void AddOnListView(uint32_t id, String^ text);

		void minimap_set_wall_pos(String^ identifier, SAWallElement* wall);

		void minimap_set_point_pos(String^ identifier, SAPointElement* point);

		void minimap_set_area_pos(String^ identifier, SAArea* area);
		System::Void radDropDownList1_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
		SABaseElement* getSelectedElement();

		Telerik::WinControls::UI::RadPropertyStore^ getGrid(SABaseElement* element);

		SABaseElement* currentSABaseElement;

		System::Void radListView1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
		System::Void LoadGrid();
		System::Void radPropertyGrid1_Edited(System::Object^  sender, Telerik::WinControls::UI::PropertyGridItemEditedEventArgs^  e);
		System::Void radListView1_ItemRemoved(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e);
		void SpecialAreaEditor::minimap_set_base_element(String^ identifier, SABaseElement* element);
		System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e);

		System::Void SpecialAreaEditor::redraw();

		void update_idiom_listbox(){
			 DropDownListCondition->BeginUpdate();
			 for (int i = 0; i < special_area_activation_total; i++){
				 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
				 item->Text = GET_MANAGED_TR(std::string("special_area_activation_t_") + std::to_string(i));
				 item->Value = i;
				 DropDownListCondition->Items->Add(item);
			 }
			 DropDownListCondition->SelectedIndex = 0;
			 DropDownListCondition->EndUpdate();

			 DropDownListEditEvent->BeginUpdate();
			 for (int i = 0; i < special_area_event_t::special_area_event_total; i++){
				 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
				 item->Text = GET_MANAGED_TR(std::string("special_area_event_t_") + std::to_string(i));
				 item->Value = i;
				 DropDownListEditEvent->Items->Add(item);
			 }
			 DropDownListEditEvent->SelectedIndex = 0;
			 DropDownListEditEvent->EndUpdate();
		}

		void update_idiom(){
			radRepeatButton2->Text = gcnew String(&GET_TR(managed_util::fromSS(radRepeatButton2->Text))[0]);
			radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
			radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
			ButtonAdd->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonAdd->Text))[0]);
			ButtonRemove->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonRemove->Text))[0]);

			for each(auto colum in radListView1->Columns)
				colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
			
			for each(auto colum in ListViewTrigger->Columns)
				colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
			
		}

	private: System::Void radRepeatButton1_Click(System::Object^  sender, System::EventArgs^  e) {
				 SABaseElement* base_el = getSelectedElement();
				 if (!base_el)
					 return;
				 SAArea* area = (SAArea*)base_el;

				 miniMapControl1->set_current_coordinate(area->start_x, area->start_y, area->z);
	}
	private: System::Void radRepeatButton2_Click(System::Object^  sender, System::EventArgs^  e) {
				 Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
				 miniMapControl1->set_current_coordinate(current_coord.x, current_coord.y, current_coord.z);
	}
	private: System::Void SpecialAreaEditor_Load(System::Object^  sender, System::EventArgs^  e);

	private: System::Void radButtonUp_Click(System::Object^  sender, System::EventArgs^  e) {
				 miniMapControl1->decrease_z(1);
	}
	private: System::Void radButtonDown_Click(System::Object^  sender, System::EventArgs^  e) {
				 miniMapControl1->increase_z(1);
	}
	private: System::Void radButtonPlus_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radButtonMinus_Click(System::Object^  sender, System::EventArgs^  e) {
				 miniMapControl1->DescreaseZoom(1);
	}

	private: System::Void MenuAdvanced_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (!thisHide){
					 this->Size = System::Drawing::Size(794, 414);
					 thisHide = true;
				 }
				 else{
					 this->Size = System::Drawing::Size(1048, 414);
					 thisHide = false;
				 }
	}

			 bool disable_update = false;

			 void addItemOnTrigger(String^ text){
				 if (!currentSABaseElement || !radListView1->SelectedItem)
					 return;

				 disable_update = true;
				 array<String^>^ columnArrayItems = { text  };
				 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(
					 Convert::ToString(currentSABaseElement->get_vector_activation_trigger().size() - 1), columnArrayItems);

				 ListViewTrigger->Items->Add(newItem);
				 ListViewTrigger->SelectedItem = newItem;
				 disable_update = false;
			 }

private: System::Void DropDownListCondition_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
			 if (!currentSABaseElement || disable_update || !ListViewTrigger->SelectedItem || !radListView1->SelectedItem)
				 return;

			 int current_index = ListViewTrigger->Items->IndexOf(ListViewTrigger->SelectedItem);
			 std::shared_ptr<SAActivationTrigger> triggerRule = currentSABaseElement->get_rule_activation_trigger(current_index);
			 if (!triggerRule)
				 return;
			 
			 disable_update = true;
			 int indexDropDownList = DropDownListCondition->SelectedIndex;
			 triggerRule->set_activation_trigger((special_area_activation_t)indexDropDownList);
			 ListViewTrigger->SelectedItem["ActivationTrigger"] = DropDownListCondition->Text;
			 disable_update = false;
}
private: System::Void ButtonAdd_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!currentSABaseElement || !radListView1->SelectedItem)
				 return;

			 currentSABaseElement->requset_new_activation_trigger();

			 addItemOnTrigger(DropDownListCondition->Text);
}
private: System::Void ListViewTrigger_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
			 if (disable_update)
				 return;

			 if (!currentSABaseElement)
				 return;

			 int vector_index = ListViewTrigger->Items->IndexOf(ListViewTrigger->SelectedItem);
			 if (vector_index < 0)
				 return;

			 std::shared_ptr<SAActivationTrigger> triggerRule = currentSABaseElement->get_rule_activation_trigger(vector_index);
			 if (!triggerRule)
				 return;			 

			 disable_update = true;
			 DropDownListCondition->SelectedIndex = (int)triggerRule->get_activation_trigger();
			 disable_update = false;
}
		 
private: System::Void DropDownListEditEvent_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
			 if (disable_update)
				 return;

}
private: System::Void ButtonEditEvent_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (disable_update)
				 return;

			 if (!radListView1->SelectedItem)
				 return;

			 int current_index = DropDownListEditEvent->SelectedIndex;
			 if (current_index < 0)
				 return;

			 std::shared_ptr<SAEvent> eventRule = currentSABaseElement->get_rule_event(current_index);
			 if (!eventRule)
				 return;

			 std::string current_script = eventRule->get_event_code();
			 NeutralBot::LuaBackground^ mluaEditor = gcnew NeutralBot::LuaBackground(gcnew String(&current_script[0]));
			 mluaEditor->hide_left_pane();
			 mluaEditor->ShowDialog();
			 eventRule->set_event_code(managed_util::fromSS(mluaEditor->fastColoredTextBox1->Text));
}
private: System::Void ButtonRemove_Click(System::Object^  sender, System::EventArgs^  e) {
			 ListViewTrigger_ItemRemoving(nullptr, nullptr);
}
private: System::Void ListViewTrigger_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
			 if (disable_update)
				 return;

			 if (!currentSABaseElement)
				 return;

			 int vector_index = ListViewTrigger->Items->IndexOf(ListViewTrigger->SelectedItem);
			 if (vector_index < 0)
				 return;

			 std::shared_ptr<SAActivationTrigger> triggerRule = currentSABaseElement->get_rule_activation_trigger(vector_index);
			 if (!triggerRule)
				 return;
			 
			 disable_update = true;
			 currentSABaseElement->remove_activation_trigger(vector_index);
			 ListViewTrigger->Items->Remove(ListViewTrigger->SelectedItem);
			 disable_update = false;
}
};
}




