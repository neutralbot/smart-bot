#include "HotkeysManager.h"
#include "Core\InputManager.h"
#include "Core\Util.h"
#include <boost\filesystem.hpp>
#include <thread>
#include "DevelopmentManager.h"

Hotkey::Hotkey(){
}

Hotkey::Hotkey(uint32_t _id, uint32_t _button_code, bool _keyboard) : keyboard(_keyboard){
	ctrl = hotkey_value_t::hotkey_value_any;
	shift = hotkey_value_t::hotkey_value_any;
	alt = hotkey_value_t::hotkey_value_any;
	numlock = hotkey_value_t::hotkey_value_any;
	scrolllock = hotkey_value_t::hotkey_value_any;
	capslock = hotkey_value_t::hotkey_value_any;
	insert = hotkey_value_t::hotkey_value_any;
	button_code = _button_code;
	id = _id;
}

Json::Value Hotkey::ClassToJson(){
	Json::Value retval;
	Json::Value& _events = retval["events"];


	retval["name"] = name;
	retval["state"] = state;
	retval["ctrl"] = ctrl;
	retval["shift"] = shift;
	retval["alt"] = alt;
	retval["numlock"] = numlock;
	retval["scrolllock"] = scrolllock;
	retval["capslock"] = capslock;
	retval["insert"] = insert;
	retval["button_code"] = button_code;
	retval["keyboard"] = keyboard;
	retval["button_as_string"] = button_as_string;

	for (auto _event_it : events){
		_events.append(_event_it);
	}
	return retval;
}

void Hotkey::JsonToClass(Json::Value& json_in){
	Json::Value& _events = json_in["events"];
	name = json_in["name"].asString();
	state = json_in["state"].asBool();
	id = HotkeysManager::get()->generate_new_id();
	ctrl = (hotkey_value_t)json_in["ctrl"].asInt();
	shift = (hotkey_value_t)json_in["shift"].asInt();
	alt = (hotkey_value_t)json_in["alt"].asInt();
	numlock = (hotkey_value_t)json_in["numlock"].asInt();
	scrolllock = (hotkey_value_t)json_in["scrolllock"].asInt();
	capslock = (hotkey_value_t)json_in["capslock"].asInt();
	insert = (hotkey_value_t)json_in["insert"].asInt();
	button_code = (hotkey_value_t)json_in["button_code"].asInt();
	keyboard = (hotkey_value_t)json_in["keyboard"].asBool();
	button_as_string = json_in["button_as_string"].asString();

	for (auto it : _events){
		events.push_back(it.asString());
	}
}

bool Hotkey::match(InputBaseEvent* input_event){
	if (input_event->is_mouse_event()){
		MouseInput* mouse_evt = (MouseInput*)input_event;
		if (mouse_evt->event_type != mouse_event_t::mouse_event_button_down)
			return false;
	}
	else{
		KeyboardInput* keyboard_evt = (KeyboardInput*)input_event;
		if(keyboard_evt->event_type != keyboard_event_t::keyboard_key_down)
			return false;
	}
	if (!this->state)
		return false;
	//input_event->print();
	
	if (ctrl != hotkey_value_t::hotkey_value_any && (ctrl != hotkey_value_t::hotkey_value_unpressed) != input_event->ctrl_pressed)
		return false;

	if (shift != hotkey_value_t::hotkey_value_any && (shift != hotkey_value_t::hotkey_value_unpressed) != input_event->shift_pressed)
		return false;

	if (alt != hotkey_value_t::hotkey_value_any && (alt != hotkey_value_t::hotkey_value_unpressed) != input_event->alt_pressed)
		return false;

	if (insert != hotkey_value_t::hotkey_value_any && (insert != hotkey_value_t::hotkey_value_unpressed) != input_event->insert_state)
		return false;

	if (numlock != hotkey_value_t::hotkey_value_any && (numlock != hotkey_value_t::hotkey_value_unpressed) != input_event->numlock_state)
		return false;

	if (scrolllock != hotkey_value_t::hotkey_value_any && (scrolllock != hotkey_value_t::hotkey_value_unpressed) != input_event->scrolllock_state)
		return false;

	return button_code == input_event->button_code;
}

void Hotkey::assign(InputBaseEvent* input_event){

	if (input_event->is_mouse_event()){
		MouseInput* mouse_input = (MouseInput*)input_event;
		if (mouse_input->is_mouse_move_event())
			return;
		button_as_string = ((MouseInput*)input_event)->get_button_as_string();
		keyboard = false;
	}
	else{
		button_as_string = ((KeyboardInput*)input_event)->get_button_as_string();
		keyboard = true;
	}
		
	ctrl = (hotkey_value_t)input_event->ctrl_pressed;
	shift = (hotkey_value_t)input_event->shift_pressed;
	alt = (hotkey_value_t)input_event->alt_pressed;
	insert = (hotkey_value_t)input_event->insert_state;
	numlock = (hotkey_value_t)input_event->numlock_state;
	scrolllock = (hotkey_value_t)input_event->scrolllock_state;
	capslock = (hotkey_value_t)input_event->capslock_state;
	
	button_code = input_event->button_code;
}

HotkeysManager::HotkeysManager(){

}

void HotkeysManager::remove_expired_events(){
	auto first = event_queue.begin();
	while (first != event_queue.end() && first->get()->has_expired()){
		first = event_queue.erase(first);
	}
}


std::shared_ptr<HotkeyScriptEvent> HotkeysManager::get_event_from_queue(){
	lock_guard _lock(event_mtx);
	
	remove_expired_events();

	auto first = event_queue.begin();
	if(first != event_queue.end()){
		std::shared_ptr<HotkeyScriptEvent> retval = *first;
		event_queue.erase(first);
		return retval;;
	}
	has_event = false;
	return nullptr;
}

void HotkeysManager::process_events(){
	MONITOR_THREAD(__FUNCTION__);
	while (true){
		Sleep(5);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;
		
		if (has_event&& !edit_script){
			std::shared_ptr<HotkeyScriptEvent> current = get_event_from_queue();
			if (current){
				for (auto& event_script : current->owner->events){
					mLuaCore->RunScript(event_script, std::string("Hotkey"));
				}
			}
		}
		else
			Sleep(1);
	}
}

bool HotkeysManager::has_recent_event(std::shared_ptr<Hotkey>& _event_hk){
	for (auto it : event_queue){
		if (it.get()->owner == _event_hk && it.get()->start_time.elapsed_milliseconds() < 200)
			return true;
	}
	return false;
}

void HotkeysManager::add_event(std::shared_ptr<Hotkey>& _event_hk){
	lock_guard _lock(event_mtx);
	if (has_recent_event(_event_hk))
		return;

	event_queue.push_back(std::shared_ptr<HotkeyScriptEvent>(new HotkeyScriptEvent(_event_hk)));
	has_event = true;
}

void HotkeysManager::process_inputs(InputBaseEvent* input_event){
	int sz = hotkeys.size();
	for (int i = 0; i < sz; i++){
		if (hotkeys[i]->match(input_event)){
			add_event(hotkeys[i]);
		}
	}
}

uint32_t HotkeysManager::generate_new_id(){
	uint32_t new_id = 0;

	while (exist_id(new_id)){
		Sleep(5);
		new_id++;
	}
	
	return new_id;
}

void HotkeysManager::set_edit_script(bool in){
	edit_script = in;
}

HotkeyScriptEvent::HotkeyScriptEvent(std::shared_ptr<Hotkey> _owner) : owner(_owner){
}

bool HotkeyScriptEvent::has_expired(){
	return start_time.elapsed_milliseconds() > MAX_HOTKEY_TIMEOUT;
}


bool HotkeysManager::exist_id(uint32_t id){
	auto hotkey_iterator = std::find_if(hotkeys.begin(), hotkeys.end(), [&](std::shared_ptr<Hotkey>& hk){
		return hk.get()->id == id;
	});
	return hotkey_iterator != hotkeys.end();
}

std::shared_ptr<Hotkey> HotkeysManager::find_hotkey_by_id(uint32_t id){
	auto hotkey_iterator = std::find_if(hotkeys.begin(), hotkeys.end(), [&](std::shared_ptr<Hotkey>& hk){
		return hk.get()->id == id;
	});
	if (hotkey_iterator != hotkeys.end()){
		return *hotkey_iterator;
	}
	return nullptr;
}

std::shared_ptr<Hotkey> HotkeysManager::new_hotkey(){
	uint32_t id = generate_new_id();
	auto retval = std::shared_ptr<Hotkey>(new Hotkey(id, 0, false));
	retval->state = false;
	retval->name = "Edit Name";
	hotkeys.push_back(retval);
	return retval;
}

std::shared_ptr<Hotkey> HotkeysManager::get_hotkey_by_id(uint32_t id){
	return find_hotkey_by_id(id);
}

HotkeysManager* HotkeysManager::get(){
	static neutral_mutex mtx_access;
	
	static HotkeysManager* mHotkeysManager = nullptr;
	if (!mHotkeysManager){
		mtx_access.lock();
		if (!mHotkeysManager){
			mHotkeysManager = new HotkeysManager();
			mtx_access.unlock();
			mHotkeysManager->init();
		}
		else
			mtx_access.unlock();
	}
	
	return mHotkeysManager;
}

void HotkeysManager::remove_hotkey(uint32_t id){
	auto hotkey_iterator = std::find_if(hotkeys.begin(), hotkeys.end(), [&](std::shared_ptr<Hotkey>& hk){
		return hk.get()->id == id;
	});
	if (hotkey_iterator != hotkeys.end()){
		hotkeys.erase(hotkey_iterator);
	}
}

void HotkeysManager::reset_default(){
	hotkeys.clear();
	Load("default_hotkeys.json");
}

std::vector<std::shared_ptr<Hotkey>> HotkeysManager::get_hotkeys(){
	return hotkeys;
}

bool HotkeysManager::Load(std::string path){
	if (!boost::filesystem::exists(path)){
		if (hotkeys.size() == 0 && path != "default_hotkeys.json"){
			reset_default();
		}
		return false;
	}
		
	Json::Value hotkeys_as_json = loadJsonFile(path);
	if (hotkeys_as_json.empty() || hotkeys_as_json.isNull()){
		if (hotkeys.size() == 0 && path != "default_hotkeys.json"){
			reset_default();
		}
		return false;
	}

	return JsonToClass(hotkeys_as_json);
}

void HotkeysManager::init(){
	Load();
	mLuaCore = LuaCore::getState(lua_core_ids::lua_core_hotkey_id);
	std::thread(&HotkeysManager::process_events, this).detach(); 
	mListenerId = InputManager::get()->add_callback(0,
		(std::function<void(InputBaseEvent* input_event)>)std::bind(&HotkeysManager::process_inputs, this, std::placeholders::_1));
}

bool HotkeysManager::Save(std::string path){
	Json::Value json_file = ClassToJson();

	if (json_file.empty() || json_file.isNull()){
		if (hotkeys.size() == 0 && path != "default_hotkeys.json"){
			reset_default();
		}
		return false;
	}

	boost::filesystem::path hotkey_file_path(path);
	if (!boost::filesystem::exists(hotkey_file_path.parent_path())){
		boost::filesystem::create_directories(hotkey_file_path.parent_path());
	}

	return saveJsonFile(path, json_file);
}

Json::Value HotkeysManager::ClassToJson(){
	Json::Value retval;

	Json::Value& hotkeys_vector = retval["hotkeys_vector"];
	for (auto hotkey_it : hotkeys){
		hotkeys_vector.append(hotkey_it->ClassToJson());
	}

	return retval;
}

bool HotkeysManager::JsonToClass(Json::Value& json_in){
	Json::Value& hotkeys_vector = json_in["hotkeys_vector"];
	if (hotkeys_vector.empty() || hotkeys_vector.isNull()){
		return false;
		//MessageBox(0, "HANDLE ERROR MISSING hotkeys_vector", "ERROR", MB_OK);
	}

	for (auto hotkey_it : hotkeys_vector){
		std::shared_ptr<Hotkey> temp(new Hotkey);
		temp->JsonToClass(hotkey_it);
		hotkeys.push_back(temp);
	}
	return true;
}