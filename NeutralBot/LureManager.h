#pragma once
#include "WaypointManager.h"

struct LureInfos{
	bool knightMode = false;
	Coordinate coord = Coordinate{ 0, 0, 0 };
	std::string visualName = "none";
};

class LureManager{
	std::vector<std::shared_ptr<LureInfos>> listLureInfo;

public: static LureInfos* get(){
		static LureInfos* m = nullptr;
		if (!m)
			m = new LureInfos();
		return m;
	}
};