#include <stdint.h>
#include <string>
#include <vector>
#include <mutex>
#include <boost\asio.hpp>
 
enum session_status : uint32_t {
	state_none,
	state_disconnected,
	state_connecting,
	state_reconnecting,
	state_connection_lost
};

enum session_property : uint32_t{
	property_ping,
	property_reconnect
};

class ClientSession{
	ClientSession() : current_status(state_none){
	}


	void set_user();
	void set_password();
	void set_room_name();
	
	uint64_t get_room_id(){
		return room_id;
	}


	bool has_property(session_property _property){
		//TODO
		return false;
	}



	std::shared_ptr<boost::asio::ip::tcp::socket> mSocket;
	std::mutex mtx_access;
	session_status current_status;
	uint64_t room_id;

	void set_room_id(uint64_t id){
		room_id = id;
	}

	void check_reconnect(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		if (current_status == state_connection_lost){

		}
	}

	void handle_connected();
	void handle_reconnect();
	void handle_connection_closed();
	void handle_send_packet();
	void handle_recv_packet();
	void set_listen();
	void check_send();

	std::vector<session_property> properties;
	uint64_t internal_id;
};

typedef std::shared_ptr<ClientSession> ClientSessionPtr;

class ClientManager{
	std::string user, password, room_name;
	uint32_t room_id;//gererated by server
	std::vector<ClientSessionPtr> sessions;
public:
	void close_all();
	ClientSessionPtr get_connection(uint64_t connection_id = 0);
};