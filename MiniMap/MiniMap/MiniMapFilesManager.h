#pragma once

#include <map>
#include <memory>

enum file_load_status_t{
	file_load_none,
	file_load_fail,
	file_load_success
}; 

class MiniMapFile{
public:
	MiniMapFile();
	file_load_status_t state;
	unsigned char data_1[256][256];
	unsigned char data_2[256][256];
	void load(int z, int y, int x);
};

class MiniMapFilesManager{
	uint32_t TIBIA_ARGB_TABLE[256];


	int display_x;
	int display_y;
	int display_z;

	int current_x;
	int current_y;
	int current_z;

	int current_map_x;
	int current_map_y;
	int current_map_z;
	void on_change_z(int new_z);

	void on_change_current_map_x(int x);

	void on_change_current_map_y(int y);

	void on_change_current_map_z(int z);

	void clear_unecesary_map();

	void check_maps();

public:
	MiniMapFilesManager();
	void update_view(int x, int y, int z);
	void get_argb_map(uint32_t* out_ptr, int initial_x, int initial_y, int width, int height, int z, int z_dif = 1);

	uint32_t get_rgb_at(int x, int y, int z);

	std::map<int /*z*/,
		std::map<int /*y*/,
		std::map<int /*x*/,
		std::shared_ptr<MiniMapFile >> >> maps;

	void update_current_pos(int x, int y, int z);
	void on_change_x(int new_x);
	void on_change_y(int new_y);
};

