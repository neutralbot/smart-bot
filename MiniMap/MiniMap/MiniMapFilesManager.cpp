#pragma once
#include "stdafx.h"

#include "MiniMapFilesManager.h"

#include <map>
#include <vector>
#include <string>
#include <stdint.h>
#include <fstream>
#include <iostream>
#include <memory> 
#include <chrono>
#include <Windows.h>
#include <shlobj.h>

#pragma comment(lib,"shell32.lib")

static bool is_windows_vista_or_later(){
	OSVERSIONINFO	vi;

	memset(&vi, 0, sizeof vi);
	vi.dwOSVersionInfoSize = sizeof vi;
	GetVersionEx(&vi);
	return (vi.dwPlatformId == VER_PLATFORM_WIN32_NT  &&  vi.dwMajorVersion >= 6);
}

std::string get_app_data_dir(){
	CHAR szPath[MAX_PATH];
	ZeroMemory(szPath, MAX_PATH);
	SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, szPath);
	return szPath;
}

#define MAPDIR_W_VISTA "C:\\Users\\neutral\\AppData\\Roaming\\Tibia\\Automap"
#define MAPDIR_W_XP ""

/*C:\Users\[USERNAME]\AppData\Roaming\Tibia

	Under Windows 2000 and Windows XP :
C : \Documents and Settings\[USERNAME]\Application Data\Tibia*/
std::string files_path = get_app_data_dir() + "\\Tibia\\Automap";// is_windows_vista_or_later() ? MAPDIR_W_VISTA : MAPDIR_W_XP;

class TimeChronometer{
	std::chrono::system_clock::time_point start_time;

public:
	TimeChronometer::TimeChronometer(){
		reset();
	}

	void TimeChronometer::reset(){
		start_time = std::chrono::high_resolution_clock::now();
	}

	int32_t TimeChronometer::elapsed_milliseconds(){
		return (int32_t)(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
	}


	static TimeChronometer* get(){
		static TimeChronometer* mTimeChronometer = nullptr;
		if (!mTimeChronometer)
			mTimeChronometer = new TimeChronometer;
		return mTimeChronometer;
	}
};

inline uint32_t to_grayscale(uint32_t value, float factor){
	if (factor == 1.0f)
		return value;
	unsigned char value_gray = (unsigned char)( 0.2126 * (value & 0x00ff0000) + 0.7152 * (value & 0x0000ff00) + 0.0722 * (value & 0x000000ff));
	value_gray = (unsigned char)((float)value_gray * factor);
	unsigned char retval[] = { value_gray, value_gray, value_gray, 0xff };
	return *(int*)retval;
}

MiniMapFile::MiniMapFile(){
	state = file_load_none;
}
void MiniMapFile::load(int z, int y, int x){
	std::string x_str = std::to_string(x);
	while (x_str.length() < 3){
		x_str += "0" + x_str;
	}

	std::string y_str = std::to_string(y);
	while (y_str.length() < 3){
		y_str += "0" + y_str;
	}

	std::string z_str = std::to_string(z);
	while (z_str.length() < 2){
		z_str = "0" + z_str;
	}

	std::string file_name = x_str + y_str + z_str + ".map";

	FILE *f = nullptr;
	errno_t error = fopen_s(&f, &(files_path + std::string("\\") + file_name)[0], "rb");
	if (!f || error){
		
		state = file_load_fail;
		return;
	}
	

	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);

	/*if (fsize != (256 * 256 * 2)){
		state = file_load_fail;
		fclose(f);
		return;
	}*/

	int count = fread(&data_1[0][0], 1, (256 * 256 * 2), f);
	state = count == (256 * 256 * 2) ? file_load_success : file_load_fail;
	
	fclose(f);
}


MiniMapFilesManager::MiniMapFilesManager(){
	memset(TIBIA_ARGB_TABLE, 256, 0);

	TIBIA_ARGB_TABLE[0x0] = 0xFF000000;
	TIBIA_ARGB_TABLE[0xba] = 0xFFFF3300;
	TIBIA_ARGB_TABLE[0xa0] = 0xFF000000;
	TIBIA_ARGB_TABLE[0x28] = 0xFF3300CC;
	TIBIA_ARGB_TABLE[0x18] = 0xFF00CC00;
	TIBIA_ARGB_TABLE[0x0c] = 0xFF006600;
	TIBIA_ARGB_TABLE[0x81] = 0xFF999999;
	TIBIA_ARGB_TABLE[0xd2] = 0xFFFFFF00;
	TIBIA_ARGB_TABLE[0x79] = 0xFF996633;
	TIBIA_ARGB_TABLE[0x56] = 0xFF666666;
	TIBIA_ARGB_TABLE[0x72] = 0xFF993300;
	TIBIA_ARGB_TABLE[0xb3] = 0xFFCCFFFF;
	TIBIA_ARGB_TABLE[0x8c] = 0xFF000000;
	TIBIA_ARGB_TABLE[0xcf] = 0xFF000000;


	current_x = 0;
	current_y = 0;
	current_z = 0;

	current_map_x = 0;
	current_map_y = 0;
	current_map_z = 0;

	display_x = 1;
	display_y = 1;
	display_z = 1;
}



void MiniMapFilesManager::on_change_current_map_x(int x){
	if (!x)
		return;
	current_map_x = x / 256;
	check_maps();
}

void MiniMapFilesManager::on_change_current_map_y(int y){
	if (!y)
		return;
	current_map_y = y / 256;
	check_maps();
}

void MiniMapFilesManager::on_change_current_map_z(int z){
	current_map_z = z;
	check_maps();
}

void MiniMapFilesManager::clear_unecesary_map(){
	for (auto item_map_z = maps.begin(); item_map_z != maps.end();){
		if (abs(item_map_z->first - current_map_z) > display_z){
			maps.erase(item_map_z++);
			continue;
		}
		for (auto item_map_y = item_map_z->second.begin(); item_map_y != item_map_z->second.end();){
			if (abs(item_map_y->first - current_map_y) > display_y){
				item_map_z->second.erase(item_map_y++);
				continue;
			}
			for (auto item_map_x = item_map_y->second.begin(); item_map_x != item_map_y->second.end();){
				if (abs(item_map_x->first - current_map_x) > display_x){
					item_map_y->second.erase(item_map_x++);
					continue;
				}
				item_map_x++;
			}
			item_map_y++;
		}
		item_map_z++;
	}
}


void MiniMapFilesManager::check_maps(){
	clear_unecesary_map();
		
	for (int _z_i = current_map_z - 1; _z_i <= current_map_z + display_z; _z_i++){
		auto& map_z = maps[_z_i];
		for (int _y_i = current_map_y - display_y; _y_i <= current_map_y + display_y; _y_i++){
			auto& map_y = map_z[_y_i];
			for (int _x_i = current_map_x - display_x; _x_i <= current_map_x + display_x; _x_i++){
				auto& map = map_y[_x_i];
				if (!map){
					auto temp_map = std::shared_ptr<MiniMapFile>(new MiniMapFile);
					map_y[_x_i] = temp_map;
					temp_map->load(_z_i, _y_i, _x_i);
				}
			}
		}
	}
}

void MiniMapFilesManager::update_view(int x, int y, int z){
	display_x =
		x / 256 + 1;
	display_y =
		y / 256 + 1;
	this->display_z = z;
	check_maps();
}

void MiniMapFilesManager::get_argb_map(uint32_t* out_ptr, int initial_x, int initial_y, int width, int height, int z, int z_dif){
	int init_x = initial_x / 256;
	int init_y = initial_y / 256;

	int init_end_x = initial_x + width;
	int init_end_y = initial_y + height;

	int out_col = 0;
	int out_row = 0;
	int total_col = width / 256;
	if (width % 256 > 0)
		total_col++;

	int total_row = height / 256;
	if (height % 256 > 0)
		total_row++;

	int first_map_use_width = 0;
	int first_map_use_height = 0;

	for (int x_r = 0; x_r < total_col; x_r++){
		for (int y_r = 0; y_r < total_row; y_r++){
			int start_x = (init_x + x_r) * 256;
			int end_x = (init_x + x_r + 1) * 256;

			if (end_x > init_end_x)
				end_x = 256 - (end_x - init_end_x);
			else
				end_x = 256;

			if (start_x < initial_x)
				start_x = initial_x - start_x;
			else
				start_x = 0;

			int start_y = (init_y + y_r) * 256;
			int end_y = (init_y + y_r + 1) * 256;

			int conserver_y_end = end_y;
			int cnsever_y_start = start_y;
			if (end_y > init_end_y)
				end_y = 256 - (end_y - init_end_y);
			else
				end_y = 256;

			if (start_y < initial_y)
				start_y = initial_y - start_y;
			else
				start_y = 0;

			if (x_r == 0 && y_r == 0){
				first_map_use_width = abs(end_x - start_x);
				first_map_use_height = abs(end_y - start_y);

				if (first_map_use_width)
					total_col += 1;

				if (first_map_use_height)
					total_row += 1;
			}

			int now_index_x = 0;
			if (x_r)
				now_index_x = first_map_use_width + ((x_r - 1) * 256);

			int now_index_y = 0;
			if (y_r)
				now_index_y = ((y_r - 1) * 256) * width + first_map_use_height * width;
			
			for (int i = z + z_dif; i >= z; i--){
				auto map_it_z = maps.find(i);
				if (map_it_z != maps.end()){
					auto& z_map = map_it_z->second;
					auto map_it_y = z_map.find(init_y + y_r);
					if (map_it_y != z_map.end()){
						auto map_it_x = map_it_y->second.find(init_x + x_r);
						if (map_it_x != map_it_y->second.end()){
							auto map = map_it_x->second;
							if (map && map->state == file_load_success){
								unsigned char(*data)[256] = map->data_1;
								float gray_scale = (1.0f - 0.1f * abs(i - z));
								for (int index_y = 0, y_i = start_y; y_i < end_y; y_i++, index_y++){
									int row_index = now_index_y + (index_y * width);
									for (int index_x = 0, x_i = start_x; x_i < end_x; x_i++, index_x++){
										uint32_t argb = TIBIA_ARGB_TABLE[data[x_i][y_i]];
										if (argb && argb != 0xFF000000)
											out_ptr[row_index + now_index_x + index_x] = to_grayscale(
											TIBIA_ARGB_TABLE[data[x_i][y_i]], gray_scale);

									}
								}
							}
						}
					}
				}
			}
		}
	}
}

uint32_t MiniMapFilesManager::get_rgb_at(int x, int y, int z){
	int x_rest = x % 256;
	int y_rest = y % 256;
	x = x / 256;
	y = y / 256;
	auto map = maps[z][y][x];

	auto map_it_z = maps.find(z);
	if (map_it_z == maps.end())
		return 0;
	auto map_it_y = map_it_z->second.find(y);
	if (map_it_y == map_it_z->second.end())
		return 0;
	auto map_it_x = map_it_y->second.find(x);
	if (map_it_x == map_it_y->second.end())
		return 0;

	if (!map /*|| map->state != file_load_success*/)
		return 0;


	int table_index = map->data_1[x_rest][y_rest];
	if (TIBIA_ARGB_TABLE[table_index])
		return TIBIA_ARGB_TABLE[table_index];
	else
		return TIBIA_ARGB_TABLE[table_index];

}

void MiniMapFilesManager::update_current_pos(int x, int y, int z){
	if (current_x != x)
		on_change_x(x);
	if (current_y != y)
		on_change_y(y);
	if (current_z != z)
		on_change_z(z);
}

void MiniMapFilesManager::on_change_x(int new_x){
	current_x = new_x;
	on_change_current_map_x(new_x);
}

void MiniMapFilesManager::on_change_y(int new_y){
	current_y = new_y;
	on_change_current_map_y(new_y);
}

void MiniMapFilesManager::on_change_z(int new_z){
	current_z = new_z;
	on_change_current_map_z(new_z);
}
