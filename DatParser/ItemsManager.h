#pragma once 

#include "FileHelper.h"
#include <memory>
#include <vector>
#include <map>
#include <string>
#include <cctype>
#include <clocale>
#pragma pack(push, 1)
template <typename valueT>
class stringmap :public std::enable_shared_from_this<stringmap<valueT>> {
public:
	void set_value(valueT _value){
		is_null = false;
		value = _value;

	}
	valueT get_value(){
		return value;
	}

	stringmap(std::shared_ptr<stringmap<valueT>> parent = nullptr){
		this->parent = parent;
		is_null = true;
	}

	~stringmap(){
	}

	void set(std::string key, valueT value){
		set((unsigned char*)&key[0], key.length(), value);
	}

	valueT find(std::string key, bool ignore_case = true){
		int len = key.length();
		return find((unsigned char*)&key[0], len, ignore_case);
	}

	void clear(){
		for (auto it : childs)
			it.second->clear();
		childs.clear();
		parent = std::shared_ptr<stringmap<valueT>>();
	}
	void erase(std::string key){
		erase((unsigned char*)&key[0], key.length());
	}
private:

	std::map <unsigned char, std::shared_ptr<stringmap<valueT>> > childs;
	valueT value;
	bool is_null;
	void set(unsigned char* str, int key_len, valueT value){
		if (key_len > 0){
			std::shared_ptr<stringmap<valueT>> next
				= childs[*str];
			if (!next){
				next = std::shared_ptr < stringmap >(new stringmap<valueT>(shared_from_this()));
				childs[*str] = next;
			}

			if (key_len == 1){
				next->set_value(value);
			}
			else{
				next->set(str + 1, key_len - 1, value);
			}
		}
	}

	valueT find(unsigned char* str, int key_len, bool ignore_case){
		if (key_len > 1){

			if (ignore_case){
				valueT retval;

				auto next
					= childs.find((unsigned char)std::tolower((char)*str));
				if (next != childs.end())
					retval = next->second->find(str + 1, key_len - 1, ignore_case);
				if (retval)
					return retval;

				next = childs.find((unsigned char)std::toupper((char)*str));
				if (next != childs.end())
					retval = next->second->find(str + 1, key_len - 1, ignore_case);
				return retval;
			}
			else{
				auto next
					= childs.find(*str);
				if (next == childs.end())
					return valueT();
				return (valueT)next->second->find(str + 1, key_len - 1, ignore_case);
			}
		}
		else if (key_len == 1){

			if (ignore_case){
				valueT retval;

				auto next
					= childs.find((unsigned char)std::tolower((char)*str));
				if (next != childs.end())
					return next->second->get_value();
				next = childs.find((unsigned char)std::toupper((char)*str));
				if (next != childs.end())
					retval = next->second->get_value();
				return retval;
			}
			else
			{
				auto next
					= childs.find(*str);
				if (next == childs.end())
					return valueT();
				return (valueT)next->second->get_value();
			}
		}
		else
			return valueT();

		if (key_len >= 0){
			std::shared_ptr<stringmap<valueT>> next
				= childs[*str];
			if (!next){
				next = std::shared_ptr < stringmap >(new stringmap<valueT>(shared_from_this()));
				childs[*str] = next;
			}

			if (key_len == 0){
				next->set_value(value);
			}
			else{
				next->set(str + 1, key_len - 1, value);
			}
		}
	}

	void erase(unsigned char* str, int key_len){
		if (key_len > 1){
			auto next
				= childs.find(*str);
			if (next == childs.end())
				return;
			next->second->erase(str + 1, key_len - 1);
		}
		else if (key_len == 1){
			if (!childs.size()){
				if (parent)
					parent->erase_chield(shared_from_this());
			}
			if (!is_null){
				value = valueT();
				is_null = true;
			}

		}
		else
			return;
	}

protected:


	void erase_chield(std::shared_ptr<stringmap<valueT>> chield){
		std::map <unsigned char, std::shared_ptr<stringmap<valueT>>>::iterator it = childs.begin();
		for (; it != childs.end(); it++)
			if (it->second == chield)
				break;

		if (it != childs.end())
			childs.erase(it);

		if (!parent)
			return;

		if (!childs.size() && is_null)
			parent->erase_chield(shared_from_this());
	}
	std::shared_ptr<stringmap<valueT>> parent;


};

#pragma pack(pop)

enum FrameGroup : uint8_t {
	FrameGroupIdle = 0,
	FrameGroupMoving,
	FrameGroupDefault = FrameGroupIdle
};

enum ThingCategory : uint8_t {
	ThingCategoryItem = 0,
	ThingCategoryCreature,
	ThingCategoryEffect,
	ThingCategoryMissile,
	ThingInvalidCategory,
	ThingLastCategory = ThingInvalidCategory
};

enum ThingAttr : uint8_t {
	ThingAttrGround = 0,
	ThingAttrGroundBorder = 1,
	ThingAttrOnBottom = 2,
	ThingAttrOnTop = 3,
	ThingAttrContainer = 4,
	ThingAttrStackable = 5,
	ThingAttrForceUse = 6,
	ThingAttrMultiUse = 7,
	ThingAttrWritable = 8,
	ThingAttrWritableOnce = 9,
	ThingAttrFluidContainer = 10,
	ThingAttrSplash = 11,
	ThingAttrNotWalkable = 12,
	ThingAttrNotMoveable = 13,
	ThingAttrBlockProjectile = 14,
	ThingAttrNotPathable = 15,
	ThingAttrPickupable = 16,
	ThingAttrHangable = 17,
	ThingAttrHookSouth = 18,
	ThingAttrHookEast = 19,
	ThingAttrRotateable = 20,
	ThingAttrLight = 21,
	ThingAttrDontHide = 22,
	ThingAttrTranslucent = 23,
	ThingAttrDisplacement = 24,
	ThingAttrElevation = 25,
	ThingAttrLyingCorpse = 26,
	ThingAttrAnimateAlways = 27,
	ThingAttrMinimapColor = 28,
	ThingAttrLensHelp = 29,
	ThingAttrFullGround = 30,
	ThingAttrLook = 31,
	ThingAttrCloth = 32,
	ThingAttrMarket = 33,
	ThingAttrUsable = 34,

	// additional
	ThingAttrOpacity = 100,
	ThingAttrNotPreWalkable = 101,

	ThingAttrFloorChange = 252,
	ThingAttrNoMoveAnimation = 253, // 10.10: real value is 16, but we need to do this for backwards compatibility
	ThingAttrChargeable = 254, // deprecated
	ThingLastAttr = 255
};

enum SpriteMask {
	SpriteMaskRed = 1,
	SpriteMaskGreen,
	SpriteMaskBlue,
	SpriteMaskYellow
};

class Size{
public:
	Size(){
		width = 0;
		height = 0;
	}
	Size(int in_width, int in_height){
		width = in_width;
		height = in_height;
	}
	int width, height;

	int area(){
		return width * height;
	}
};

class any {
public:
	struct placeholder {
		virtual ~placeholder()  { }
		virtual const std::type_info& type() const = 0;
		virtual placeholder* clone() const = 0;
	};

	template<typename T>
	struct holder : public placeholder {
		holder(const T& value) : held(value) { }
		const std::type_info& type() const { return typeid(T); }
		placeholder* clone() const { return new holder(held); }
		T held;

		holder& operator=(const holder &);
	};

	placeholder* content;

	any() : content(nullptr) { }
	any(const any& other) : content(other.content ? other.content->clone() : nullptr) { }
	template<typename T> any(const T& value) : content(new holder<T>(value)) { }
	~any() { if (content) delete content; }

	any& swap(any& rhs) { std::swap(content, rhs.content); return *this; }

	template<typename T> any& operator=(const T& rhs) { any(rhs).swap(*this); return *this; }
	any& operator=(any rhs) { rhs.swap(*this); return *this; }

	bool empty() const { return !content; }
	template<typename T> const T& cast() const;
	const std::type_info & type() const { return content ? content->type() : typeid(void); }
};

template<typename T>
const T& any_cast(const any& operand) {
	//assert(operand.type() == typeid(T));
	return static_cast<any::holder<T>*>(operand.content)->held;
}
template<typename T> const T& any::cast() const { return any_cast<T>(*this); }

template<typename Key>
class dynamic_storage {
public:
	template<typename T> void set(const Key& k, const T& value) {
		if (m_data.size() <= k)
			m_data.resize(k + 1);
		m_data[k] = value;
	}
	bool remove(const Key& k) {
		if (m_data.size() < k)
			return false;
		if (m_data[k].empty())
			return false;
		m_data[k] = any();
		return true;
	}
	template<typename T> T get(const Key& k) const { return has(k) ? any_cast<T>(m_data[k]) : T(); }
	bool has(Key& k) { return k < m_data.size() && !m_data[k].empty(); }
	bool has(const Key& k)const { return k < m_data.size() && !m_data[k].empty(); }


	std::size_t size() const {
		std::size_t count = 0;
		for (std::size_t i = 0; i<m_data.size(); ++i)
			if (!m_data[i].empty())
				count++;
		return count;
	}

	void clear() { m_data.clear(); }


	std::vector<any> m_data;
};

struct MarketData {
	std::string name;
	int category;
	uint16_t requiredLevel;
	uint16_t restrictVocation;
	uint16_t showAs;
	uint16_t tradeAs;
};

struct Light {
	Light() { intensity = 0; color = 215; }
	uint8_t intensity;
	uint8_t color;
};

class ThingType{
public:

	bool m_null;
	uint32_t m_id;
	ThingCategory m_category;
	dynamic_storage<uint8_t>  m_attribs;
	uint8_t width;
	uint8_t height;
	void ThingType::unserialize(uint16_t clientId, ThingCategory category, std::shared_ptr< FileReader> fin);

	std::string get_item_name(){
		if (m_attribs.has(ThingAttrMarket))
			return m_attribs.get<MarketData>(ThingAttrMarket).name;
		return "";
	}
};

typedef std::shared_ptr<ThingType> ThingTypePtr;

class TibiaFileParser{
public:
	ThingTypePtr m_nullThingType;
	std::vector<ThingTypePtr> m_thingTypes[ThingLastCategory];
	std::vector<ThingTypePtr> m_thingTypesCustom;
	std::shared_ptr<stringmap<ThingTypePtr>> m_thingTypesStringKey;
	TibiaFileParser();

	void updateStringMap();
	void sort();
	bool LoadDat(std::string file);

	static TibiaFileParser* get();
};

#pragma pack(push,1)
class ItemsManager{
public:
	TibiaFileParser* dat_parser;

	ItemsManager();
	bool is_food_item_id(uint32_t id){
		return std::find(items_as_food.begin(), items_as_food.end(), id) != items_as_food.end();
	}


	void load_custom();
	bool load(std::string file_path);

	ThingTypePtr getItemTypeById(uint32_t id);

	ThingTypePtr getItemTypeByName(std::string name);

	std::vector<ThingTypePtr> getItemsByAttributes(std::vector<ThingAttr> types);

	std::vector<ThingTypePtr> getItemsByAttribute(ThingAttr types);

	bool isitem_id(uint32_t id);
	bool is_item(uint32_t id);
	bool isRuneFieldItem(uint32_t id);

	int getItemPrice(uint32_t id);

	int getitem_idFromName(std::string name);

	std::string getItemNameFromId(uint32_t id);

	void addItem(int id, std::string name, std::vector<ThingAttr> attributes = std::vector<ThingAttr>());

	std::vector<uint32_t> get_depot_ids();

	std::vector<uint32_t> get_depot_box_ids();

	std::vector<uint32_t> get_depot_tiles_ids();

	std::vector<std::string> get_containers();

	std::vector<std::string> get_itens();

	std::vector<std::string> get_potions();

	std::vector<std::string> get_runes();

	std::vector<std::string> get_ammunitions();

	bool is_furniture_id(uint32_t item_id);

	bool is_pick_item(uint32_t item_id);

	bool is_rope_item(uint32_t item_id);

	bool is_shovel_item(uint32_t item_id);

	bool is_machete_item(uint32_t item_id);

	bool is_kitchen_item(uint32_t item_id);

	bool is_knife_item(uint32_t item_id);

	bool is_spoon_item(uint32_t item_id);

	bool is_croowbar_item(uint32_t item_id);

	bool is_scythe_item(uint32_t item_id);

	bool is_sickle_item(uint32_t item_id);

	std::vector<uint32_t> items_as_pick;
	std::vector<uint32_t> items_as_rope;
	std::vector<uint32_t> items_as_shovel;
	std::vector<uint32_t> items_as_machete;
	std::vector<uint32_t> items_as_container;
	std::vector<uint32_t> items_as_kitchen;
	std::vector<uint32_t> items_as_knife;
	std::vector<uint32_t> items_as_spoon;
	std::vector<uint32_t> items_as_crowbar;
	std::vector<uint32_t> items_as_scythe;
	std::vector<uint32_t> items_as_sickle;
	std::vector<uint32_t> items_as_take_skin;
	std::vector<uint32_t> items_as_furniture;
	std::vector<uint32_t> items_as_food;
	std::vector<uint32_t> items_as_empty_potions;
	std::vector<uint32_t> items_as_fields_runes;

	std::vector<uint32_t>& get_items_as_pick(){ return items_as_pick; };
	std::vector<uint32_t>& get_items_as_rope(){ return items_as_rope; };
	std::vector<uint32_t>& get_items_as_shovel(){ return items_as_shovel; };
	std::vector<uint32_t>& get_items_as_take_skin(){ return items_as_take_skin; };
	std::vector<uint32_t>& get_items_as_machete(){ return items_as_machete; };
	std::vector<uint32_t>& get_items_as_kitchen(){ return items_as_kitchen; };
	std::vector<uint32_t>& get_items_as_knife(){ return items_as_knife; };
	std::vector<uint32_t>& get_items_as_spoon(){ return items_as_spoon; };
	std::vector<uint32_t>& get_items_as_crowbar(){ return items_as_crowbar; };
	std::vector<uint32_t>& get_items_as_scythe(){ return items_as_scythe; };
	std::vector<uint32_t>& get_items_as_sickle(){ return items_as_sickle; };
	std::vector<uint32_t>& get_items_as_food(){ return items_as_food; };
	std::vector<uint32_t>& get_items_as_empty_potions(){ return items_as_empty_potions; };
	std::vector<uint32_t>& get_items_as_fields_runes(){ return items_as_fields_runes; };

	bool is_lying_corpse(uint32_t item_id);

	static ItemsManager* get();
};
#pragma pack(pop)

