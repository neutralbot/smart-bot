#include <Packet.h>

TimeChronometer::TimeChronometer(){
	reset();
}

void TimeChronometer::reset(){
	start_time = std::chrono::high_resolution_clock::now();
}

int32_t TimeChronometer::elapsed_milliseconds(){
	return (int32_t)(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
}