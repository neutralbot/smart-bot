#pragma once
#include <algorithm>
#include <vector>
#include <map>
#include <memory>

#include <chrono>

class TimeChronometer{
	std::chrono::system_clock::time_point start_time;

public:
	TimeChronometer();
	void reset();
	int32_t elapsed_milliseconds();

	static TimeChronometer* get(){
		static TimeChronometer* mTimeChronometer = nullptr;
		if (!mTimeChronometer)
			mTimeChronometer = new TimeChronometer;
		return mTimeChronometer;
	}
};


class PacketBase{
	TimeChronometer time;
public:
	std::vector<unsigned char> data;
};

class ClientPacket : public PacketBase{
};

typedef std::shared_ptr<ClientPacket> ClientPacketPtr;

class ServerPacket : public PacketBase{
};

typedef std::shared_ptr<ServerPacket> ServerPacketPtr;



