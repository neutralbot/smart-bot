#pragma once

#include <string>
#include <map>
#include <iostream>
#include <sstream>



std::string serialize_map(std::map<std::string, std::string>& mMap){
	std::ostringstream os;
	const int version = 0;
	os << version;
	for (auto it = mMap.begin(); it != mMap.end(); it++)
		os << " " << it->first.length() << " " << it->first << " " << it->second.length() << " " << it->second;
	return os.str();
}


std::map<std::string, std::string> deserialize_map(std::string str){
	const int max_string_len = 1024;
	std::map<std::string, std::string> retval;
	int len = str.length();
	int carret = 0;
	std::stringstream ss(str);
	int version;
	if (ss)
		ss >> version;

	std::string key;
	std::string value;
	char buff[max_string_len];

	while (ss){
		ss >> len;
		if (!len || !ss || len >= max_string_len || len <= 0)
			break;
		ss.ignore(256, ' ');
		//read key
		ss.read(buff, len);
		buff[len] = 0;
		key = buff;

		if (ss){
			ss >> len;
			if (!len || !ss || len >= max_string_len || len <= 0)
				break;
			ss.ignore(256, ' ');
			//read value
			ss.read(buff, len);
			buff[len] = 0;

			retval[key] = buff;
		}
	}
	return retval;
}
