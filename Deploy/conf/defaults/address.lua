

function dectohex(num)
	return tonumber(num, 16);
end


lookAddressIdChar					= dectohex("0x6D3034")
lookAddressPrimeiroIdBattle 		= dectohex("0x72F210")
AddressSkullModeNew					= dectohex("0x5468F4")
AddressSkullModeOld					= dectohex("0x54691A")
lookAddressLogged					= dectohex("0x53570C")

AddressBasePing						= dectohex("0x1A14C");
AddressActualHotkeySchema 			= dectohex("0x51FC08")
AddressHWND							= dectohex("0x535654")
AddressTreino						= dectohex("0x535664")
XorAddress							= dectohex("0x5356A0")
AddressExp							= dectohex("0x5356A8")
AddressLvl							= dectohex("0x5356B8")
AddressSoul							= dectohex("0x5356BC")
AddressMagicLvl						= dectohex("0x5356C0")
AddressMagicLvlPc					= dectohex("0x5356C8")
AddressTargetRed					= dectohex("0x5356CC")
AddressMp							= dectohex("0x5356D0")
AddressFirstPc						= dectohex("0x5356D8")
AddressWhiteTarget					= dectohex("0x5356F4")
AddressStamina						= dectohex("0x535704")
AddressStatus						= dectohex("0x53565C")
AddressOfWindow						= dectohex("0x535970")
AddressOfBpPointer					= dectohex("0x53596C")
AdressEquip							= dectohex("0x53596C")
AddressReceivBuffer					= dectohex("0x535394")
AddressMouseX1						= dectohex("0x5466D4")
AddressFollow						= dectohex("0x5458D0")
AddressCooldownBar					= dectohex("0x5467EA")
AddressTelaY						= dectohex("0x5471C8")
address_mouse						= dectohex("0x54715C")
address_mouse_2						= address_mouse;
AddressTelaX						= dectohex("0x547168")
AddressServerMessage				= dectohex("0x588FE0")
AddressMessagePlayer				= AddressServerMessage
AddressMessageWarning				= dectohex("0x588DD4")
AddressMessageNotPossible			= AddressMessageWarning 
address_pointer_spells_basic		= dectohex("0x58AA58")
address_pointer_spells				= address_pointer_spells_basic 	+ 8
address_total_spells				= address_pointer_spells 		+ 4
AddressExpHour						= dectohex("0x58AAEC")
addressOfFirstMap					= dectohex("0x58EA70")
pointer_vip_players					= dectohex("0x6CEFEC")
AddressBaseLogList					= dectohex("0x6AD8D0")
AddressCtrl							= dectohex("0x6CF1E8")
AddressSchemaHotkeyReference		= dectohex("0x6CF29C")
address_item_to_be_used				= dectohex("0x6CF4D8")
AddressMouseX2						= dectohex("0x6CF4F0")
AddressMouseY2						= AddressMouseX2 + 4;
address_item_to_be_moved			= dectohex("0x6CF500")
AddressCap							= dectohex("0x6D3024")
AddressHp							= dectohex("0x6D3000")
AddressZGO							= dectohex("0x6D3004")
AddressFirst						= dectohex("0x6D3008")
AddressYGO							= dectohex("0x6D3028")
AddressMaxHp						= dectohex("0x6D302C")
AddressXGO							= dectohex("0x6D3030")
AddressX							= dectohex("0x6D3038")
AddressY							= AddressX + 4;
AddressZ							= dectohex("0x6D3040")
base_address_in_gui					= dectohex("0x775214")
AddressHelmet						= dectohex("0x7753C0")
PointerInicioMap					= dectohex("0x775404")
PointerInicioOffsetMap				= dectohex("0x7779F34")
address_bp_base_new					= dectohex("0x77B694")
address_tibia_time					= dectohex("0x77C1E8")


AddressMouseY1						= AddressMouseX1 + 4
AddressMaxMp						= XorAddress + 4;


AddressClubPc					= AddressFirstPc + 4;
AddressSwordPc					= AddressFirstPc + 4 * 2;
AddressAxePc					= AddressFirstPc + 4 * 3;
AddressDistancePc				= AddressFirstPc + 4 * 4;
AddressShieldingPc				= AddressFirstPc + 4 * 5;
AddressFishingPc				= AddressFirstPc + 4 * 6;

AddressMouse_fix_x				= AddressMouseX1;
AddressMouse_fix_y				= AddressMouse_fix_x + 4;

AddressClub						= AddressFirst		+ 4;
AddressSword					= AddressFirst		+ 4 *2;
AddressAxe						= AddressFirst		+ 4 *3;
AddressDistance					= AddressFirst		+ 4 *4;
AddressShielding				= AddressFirst		+ 4 *5;
AddressFishing					= AddressFirst		+ 4 *6;

offset_beetwen_body_item		= dectohex("0x20");	

AddressAmulet					= AddressHelmet		- offset_beetwen_body_item * 1;
AddressBag						= AddressHelmet		- offset_beetwen_body_item * 2;
AddressMainBp					= AddressBag;
AddressArmor					= AddressHelmet		- offset_beetwen_body_item * 3;
AddressShield					= AddressHelmet		- offset_beetwen_body_item * 4;
AddressWeapon					= AddressHelmet		- offset_beetwen_body_item * 5;
AddressLeg						= AddressHelmet		- offset_beetwen_body_item * 6;
AddressBoot						= AddressHelmet		- offset_beetwen_body_item * 7;
AddressRing						= AddressHelmet		- offset_beetwen_body_item * 8;
AddressRope						= AddressHelmet		- offset_beetwen_body_item * 9;
AddressLogged					= lookAddressLogged;	
AddressIdChar					= lookAddressIdChar;
AddressPrimeiroNomeBattle		= lookAddressPrimeiroIdBattle + 4;




