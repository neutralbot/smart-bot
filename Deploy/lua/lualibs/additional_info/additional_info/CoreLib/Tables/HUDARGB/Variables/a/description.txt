Type: Integer range 0 - 255.
Alpha value of color.
Alpha is the opacity of the color.
Alpha is the transparent value regulator.
255 equals to 100% visible.
