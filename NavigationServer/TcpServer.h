#include <vector>
#include <map>
#include <string>
#include <stdint.h>
#include <memory>
#include <chrono>

#include <boost\asio.hpp>
#include <boost\bind.hpp>

#include <Packet.h>
typedef std::shared_ptr<boost::asio::ip::tcp::socket> TcpSocketPtr;
typedef boost::asio::ip::tcp::socket TcpSocket;
class Room;

class ClientSession{
	ClientSession(std::shared_ptr<boost::asio::ip::tcp::socket> _socket){
		mSocketPtr = _socket;
		listening = false;
	}

	TimeChronometer last_ping;
	char token[20];
	TcpSocketPtr mSocketPtr;

	bool listening;
	void set_listen();
	void parsePacket();
	void assignSocket(TcpSocketPtr socket);
	void setProperty();
	//getProperty
	void sendProperties();
	uint64_t internalServerToken;
	std::shared_ptr<Room> owner;
public:
	void setOwner(std::shared_ptr<Room> _owner){
		owner = _owner;
	}

	std::shared_ptr<Room> getOwner(){
		return owner;
	}

	void addToSendQueue(ServerPacketPtr packetPtr){

	}
};

typedef std::shared_ptr<ClientSession> ClientSessionPtr;

class Room{
	std::string name;
	std::string password;

	
	TimeChronometer start_time;
	std::vector<ServerPacketPtr> packets;
	std::vector<std::shared_ptr<ClientSession>> clients;
	uint64_t internal_uid;
	
	uint64_t current_packet_index = 0;
	uint64_t last_sent_packet_index = 0;

	void check_send_messages();
public:
	std::string get_name();
	void set_name(std::string name);

	std::string get_password();
	void set_password(std::string password);
	
	void set_internal_uid(uint64_t id);
	uint64_t get_internal_uid();

	void broadCastPacket(ServerPacketPtr packet, ClientSessionPtr sender){

	}
};


class TcpServer{
	TimeChronometer start_time;
	std::vector<std::shared_ptr<Room>> rooms;

	std::shared_ptr<Room> get_room_by_name(std::string name);
	std::shared_ptr<Room> get_room_by_id(uint64_t room_id);
	std::shared_ptr<Room> create_room(std::string name);




	void handle_receive_data(boost::system::error_code& error, size_t bytes_transferred);
	void handle_send_data(boost::system::error_code& error, size_t bytes_transferred);

	void handle_first_packet(boost::system::error_code& error, size_t bytes_transferred);
	void handle_accept(boost::system::error_code& error, size_t bytes_transferred);


	void start();
};