#include "Dialog.h"
#include "browser.h"
#include "resource.h"
#include "hashgen.h"
#include "Downloader.h"
 
BEGIN_MESSAGE_MAP(MyBtn, CButton)
	ON_WM_MOUSEHOVER()
	ON_WM_MOUSELEAVE()
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()

void MyBtn::OnPaint(){
	CButton::OnPaint();
}

Gdiplus::Image *  MyBtn::Load(LPCTSTR pName, LPCTSTR pType,
	HMODULE hInst){
	Gdiplus::Image * m_pBitmap = nullptr;
	void * m_hBuffer;

	HRSRC hResource = ::FindResource(hInst, pName, pType);
	if (!hResource)
		return false;

	DWORD imageSize = ::SizeofResource(hInst, hResource);
	if (!imageSize)
		return false;

	const void* pResourceData = ::LockResource(::LoadResource(hInst,
		hResource));

	if (!pResourceData)
		return false;

	m_hBuffer = ::GlobalAlloc(GMEM_MOVEABLE, imageSize);
	if (m_hBuffer){
		void* pBuffer = ::GlobalLock(m_hBuffer);
		if (pBuffer){
			CopyMemory(pBuffer, pResourceData, imageSize);
			IStream* pStream = NULL;
			if (::CreateStreamOnHGlobal(m_hBuffer, FALSE, &pStream) == S_OK){
				m_pBitmap = Gdiplus::Image::FromStream(pStream);
				pStream->Release();
				if (m_pBitmap){
					int status = m_pBitmap->GetLastStatus();
					WCHAR tem[256];
					_itow_s(status, &tem[0], 256, 10);
					if (status == Gdiplus::Ok)
						return m_pBitmap;
					delete m_pBitmap;
					m_pBitmap = NULL;
				}
			}
			m_pBitmap = NULL;
			::GlobalUnlock(m_hBuffer);
		}
		::GlobalFree(m_hBuffer);
		m_hBuffer = NULL;
	}
	return m_pBitmap;
}

CDC bufferDC;
void MyBtn::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct){
	extern MyDlg dlg;
	CRect rect;
	GetWindowRect(rect);

	CRect rt2;
	GetParent()->GetWindowRect(rt2);
	rect.right = rect.right - rt2.right;
	rect.bottom = rect.bottom - rt2.bottom;
	rect.top = rect.top - rt2.top;
	rect.left = rect.left - rt2.left;

	RECT trect = lpDrawItemStruct->rcItem;

	if (!::BitBlt(lpDrawItemStruct->hDC, 0, 0, trect.right - trect.left,
		trect.bottom - trect.top, bufferDC.m_hDC, rect.left, rect.top, SRCCOPY)){
	}

	CRect rcClient;
	GetClientRect(&rcClient);
	if (lpDrawItemStruct->itemState & ODS_SELECTED){
		// If you want to do anything special when the button is pressed, do it here
		// Maybe offset the rect to give the impression of the button being pressed?
		rcClient.OffsetRect(1, 1);
	}

	extern MyDlg dlg;
	Gdiplus::Graphics gr(lpDrawItemStruct->hDC);
	RECT ts;
	RECT par;
	GetParent()->GetWindowRect(&par);
	GetWindowRect(&ts);
	gr.DrawImage(dlg.bkimagegdi, rcClient.left, rcClient.top, par.left - ts.left, par.top - ts.top, par.right - ts.right, par.bottom - ts.bottom, Gdiplus::UnitPixel);

	Gdiplus::Graphics graphic(lpDrawItemStruct->hDC);

	graphic.SetCompositingQuality(Gdiplus::CompositingQualityHighQuality);
	graphic.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);
	graphic.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);
	Gdiplus::Pen blackPen(Gdiplus::Color(255, 0, 0, 0), 3);

	// Initialize the coordinates of the points that define the line.

	int x1 = lpDrawItemStruct->rcItem.left;
	int y1 = lpDrawItemStruct->rcItem.top;
	int x2 = lpDrawItemStruct->rcItem.right;
	int y2 = lpDrawItemStruct->rcItem.bottom;

	Gdiplus::Color color(192, 255, 0, 0);
	Gdiplus::Rect rectangle(100, 100, 400, 400);
	Gdiplus::SolidBrush solidBrush(color);

	if ((int)img_on_push > 1 && (int)img_on_hover > 1 && (int)img_on_default > 1)
		if (mouse_is_hover)
			graphic.DrawImage(img_on_push, Gdiplus::Rect(x1, y1, (x2 - x1), y2 - y1));
		else if ((lpDrawItemStruct->itemState & ODS_FOCUS))
			graphic.DrawImage(img_on_hover, Gdiplus::Rect(x1, y1, (x2 - x1), y2 - y1));
		else
			graphic.DrawImage(img_on_default, Gdiplus::Rect(x1, y1, (x2 - x1), y2 - y1));
	

	CString string2;
	GetWindowText(string2);;

	int fsize = 12;
	Gdiplus::Font myFont(L"Arial", (Gdiplus::REAL)fsize);
	Gdiplus::RectF layoutRect((Gdiplus::REAL)x1, (Gdiplus::REAL)y1,
		(Gdiplus::REAL)(x2 - x1), (Gdiplus::REAL)(y2 - y1));
	Gdiplus::RectF boundRect;

	char * zd = string2.LockBuffer();
	WCHAR temp[1024];
	wsprintfW(temp, L"%S", zd);
	string2.UnlockBuffer();
	graphic.MeasureString(temp, string2.GetLength(), &myFont, layoutRect, &boundRect);

	float difx = abs(layoutRect.Width - boundRect.Width) / 2;
	float dify = abs(layoutRect.Height - boundRect.Height) / 2;
	Gdiplus::RectF c(boundRect.X + difx, boundRect.Y + dify, boundRect.Width, boundRect.Height);

	Gdiplus::StringFormat format;
	format.SetAlignment(Gdiplus::StringAlignmentCenter);

	Gdiplus::Color temclr;
	if (mouse_is_hover)
		temclr = Gdiplus::Color(255, 255, 0, 0);
	else
		temclr = Gdiplus::Color(255, 255, 255, 255);

	Gdiplus::SolidBrush blackBrush(temclr);

	graphic.DrawString(temp,string2.GetLength(),&myFont,c,&format,&blackBrush);
}


Gdiplus::Image * Load(LPCTSTR pName, LPCTSTR pType,
	HMODULE hInst);

MyDlg::MyDlg(CWnd* pParent)
	: CDialog(MyDlg::IDD, pParent){

	maximized = true;

}

void MyDlg::DoDataExchange(CDataExchange* pDX){
	CDialog::DoDataExchange(pDX);

	//DDX_Control(pDX, IDC_BUTTON5, progressb);
	//DDX_Control(pDX, 1008, btntest);
	//DDX_Control(pDX, 1001, btntest2);
	//DDX_Control(pDX, IDC_BUTTON4, btntest3);
	//DDX_Control(pDX, IDC_BUTTON2, mybtnclose);
	//DDX_Control(pDX, IDC_BUTTON101, info);

	DDX_Control(pDX, IDC_BUTTON6, close);
	DDX_Control(pDX, IDC_BUTTON7, btntest3);

}

BEGIN_MESSAGE_MAP(MyDlg, CDialog)
	ON_WM_MOUSEHOVER()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSELEAVE()
	ON_WM_SYSCOMMAND()
	ON_WM_NCHITTEST()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_MOUSEHOVER, OnMouseHover_)
	
	ON_WM_MOUSELEAVE()
	ON_NOTIFY(BCN_HOTITEMCHANGE, IDC_BUTTON3, &MyDlg::OnBnHotItemChangeButton3)
	ON_WM_TIMER()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON3, &MyDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &MyDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON100, &MyDlg::OnBnClickedButton100)
	ON_BN_CLICKED(IDC_BUTTON101, &MyDlg::OnBnClickedButton101)
	ON_BN_CLICKED(IDC_BUTTON1, &MyDlg::OnBnStartNeutral)
	ON_BN_CLICKED(IDC_BUTTON6, &MyDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &MyDlg::OnBnClickedButton7)
END_MESSAGE_MAP()

BEGIN_MESSAGE_MAP(MyProgess, CButton)
END_MESSAGE_MAP()

void MyProgess::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct){

	Gdiplus::Graphics graphic(lpDrawItemStruct->hDC);
	graphic.SetCompositingQuality(Gdiplus::CompositingQualityHighQuality);
	graphic.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);
	graphic.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);

	int x1 = lpDrawItemStruct->rcItem.left;
	int y1 = lpDrawItemStruct->rcItem.top;
	int x2 = lpDrawItemStruct->rcItem.right;
	int y2 = lpDrawItemStruct->rcItem.bottom;

	if (barbk)
		graphic.DrawImage(barbk, Gdiplus::Rect(x1, y1, (x2 - x1), y2 - y1));
	if (barfront)
		graphic.DrawImage(barfront, Gdiplus::Rect(x1, y1, (x2 - x1)*percent / 100, y2 - y1));

	extern std::string bar_text;
	char buff[10];
	extern int percent;
	_itoa((int)percent, &buff[0], 10);
	std::string mystr = bar_text + " " + string(&buff[0]) + "%";
	Gdiplus::Font font(L"Comic Sans Ms", 8, Gdiplus::FontStyleBold);
	Gdiplus::RectF layoutRect((Gdiplus::REAL)0.0,
		(Gdiplus::REAL)0, (Gdiplus::REAL)(x2 - x1), (Gdiplus::REAL)(y2 - y1));
	Gdiplus::StringFormat format;
	format.SetAlignment(Gdiplus::StringAlignmentCenter);
	Gdiplus::SolidBrush blackBrush(Gdiplus::Color(255, 255, 0, 0));
	CString string2(&mystr[0]);
	char * zd = string2.LockBuffer();
	WCHAR temp[512];
	wsprintfW(temp, L"%S", zd);
	string2.UnlockBuffer();

	graphic.DrawString(temp, string2.GetLength(), &font, layoutRect, &format, &blackBrush);
}
BOOL MyDlg::OnInitDialog()
{
	extern HWND main_hwnd;
	main_hwnd = this->m_hWnd;
	PlaySound(MAKEINTRESOURCE(IDR_WAVE2), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);
	HICON hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON2));
	SetIcon(hIcon, FALSE);
	/*
		CComboBox * combo = (CComboBox*)GetDlgItem(IDC_COMBO1);
		combo->AddString("Client DirectX.");
		combo->AddString("Client OpenGL.");
		combo->AddString("Old Client.");
		combo->SetCurSel(0);
	*/

	SetTimer(1, 200, 0);

	status = Gdiplus::GdiplusStartup(&m_gdiplusToken, &gdiplusStartupInput, MyGdi);

	bkimagegdi = Load(MAKEINTRESOURCE(IDB_PNG9), CString("PNG"), GetModuleHandle(0));
	//logo = Load(MAKEINTRESOURCE(IDB_PNG8), CString("PNG"), GetModuleHandle(0));
	//progressb.barbk = Load(MAKEINTRESOURCE(IDB_PNG6), "PNG", GetModuleHandle(0));
	//progressb.barfront = Load(MAKEINTRESOURCE(IDB_PNG5), "PNG", GetModuleHandle(0));

	//progressb.percent = 0;
	CDialog::OnInitDialog();





	CRect rect;
	//CWnd *pWnd = GetDlgItem(ID_BROWSER_LOC);

	//pWnd->GetWindowRect(&rect);
	this->ScreenToClient(&rect);

	RECT x = { rect.left, rect.top, rect.Width(), rect.Height() };
	x.bottom = rect.bottom;
	x.left = rect.left;
	x.right = rect.right;
	x.top = rect.top;

	//browser.Create(WS_CHILD | WS_VISIBLE, x, this, 01);
	//browser.Navigate("http://spo.besaba.com/patch/news/new.html");

	extern void start_update_thread();
	start_update_thread();

	mybtnclose.inita((int)IDB_PNG16, (int)IDB_PNG17);
	close.inita((int)IDB_PNG2, (int)IDB_PNG4);
	btntest3.inita((int)IDB_PNG7, (int)IDB_PNG8);

	btntest.inita((int)IDB_PNG18, (int)IDB_PNG19);

	info.inita((int)IDB_PNG12, (int)IDB_PNG13);
	return TRUE;



}

void MyDlg::OnSysCommand(UINT nID, LPARAM lParam){
	CDialog::OnSysCommand(nID, lParam);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
bool teste = false;
void MyDlg::OnPaint()
{
	if (!maximized)
		return;
	dc = new CPaintDC(this);
	Gdiplus::Graphics MyInterface(*dc);
	MyInterface.SetCompositingQuality(Gdiplus::CompositingQualityHighQuality);
	MyInterface.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);
	MyInterface.SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);
	CRect rect;
	Gdiplus::Rect rec;
	this->GetClientRect(&rect);
	rec.Width = rect.Width();
	rec.Height = rect.Height();
	rec.X = 0;
	rec.Y = 0;

	MyInterface.DrawImage(bkimagegdi, 0, 0, rect.Width(), rect.Height());
	//MyInterface->DrawImage(logo, 205, 50, rect.Width() - 420, 50);

	if (!teste){
		teste = true;
		//	if(!bufferDC.CreateCompatibleDC(this->GetDC()))MessageBox("1","");
		HBITMAP map = CreateCompatibleBitmap(this->GetDC()->m_hDC, rect.Width(), rect.Height());
		//if(!bufferDC.SelectObject(map))MessageBox("2","");
	}
	CRect recta;
	GetWindowRect(recta);

	Gdiplus::Graphics msec(bufferDC);
	msec.DrawImage(bkimagegdi, 0, 0, rect.Width(), rect.Height());
	/*bufferDC.BitBlt( 0, 0, recta.Width(),
		recta.Height(), dc,0, 0, SRCCOPY);
		*/

	//Gdiplus::Graphics mSecInterface(bufferDC);
	//mSecInterface.DrawImage(bkimagegdi, 0, 0, rect.Width(), rect.Height());

	delete(dc);

	CDialog::OnPaint();
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR MyDlg::OnQueryDragIcon(){
	return static_cast<HCURSOR>(m_hIcon);
}

void MyDlg::OnSize(UINT nType, int cx, int cy){

	switch (nType){

	case SIZE_MAXIMIZED:
		maximized = true;
		break;

	case SIZE_MINIMIZED:
		maximized = false;
		break;

	case SIZE_RESTORED:
		maximized = true;
		break;

	default:
		// you could also deal with SIZE_MAXHIDE and SIZE_MAXSHOW
		// but rarely need to
		break;
	}
}
Gdiplus::Image * Load(LPCTSTR pName, LPCTSTR pType,
	HMODULE hInst){
	Gdiplus::Image * m_pBitmap = nullptr;
	void * m_hBuffer;

	HRSRC hResource = ::FindResource(hInst, pName, pType);
	if (!hResource)
		return false;
	DWORD imageSize = ::SizeofResource(hInst, hResource);
	if (!imageSize)
		return false;
	const void* pResourceData = ::LockResource(::LoadResource(hInst,
		hResource));
	if (!pResourceData)
		return false;
	m_hBuffer = ::GlobalAlloc(GMEM_MOVEABLE, imageSize);
	if (m_hBuffer){
		void* pBuffer = ::GlobalLock(m_hBuffer);
		if (pBuffer){
			CopyMemory(pBuffer, pResourceData, imageSize);
			IStream* pStream = NULL;
			if (::CreateStreamOnHGlobal(m_hBuffer, FALSE, &pStream) == S_OK){
				m_pBitmap = Gdiplus::Image::FromStream(pStream);
				pStream->Release();
				if (m_pBitmap){
					int status = m_pBitmap->GetLastStatus();
					WCHAR tem[256];
					_itow_s(status, &tem[0], 256, 10);
					if (status == Gdiplus::Ok)
						return m_pBitmap;
					delete m_pBitmap;
					m_pBitmap = NULL;
				}
			}
			m_pBitmap = NULL;
			::GlobalUnlock(m_hBuffer);
		}
		::GlobalFree(m_hBuffer);
		m_hBuffer = NULL;
	}
	return m_pBitmap;
}

void MyDlg::OnMouseHover(UINT nFlags, CPoint point){
	CDialog::OnMouseHover(nFlags, point);
}


void MyDlg::OnMouseLeave(){
	CDialog::OnMouseLeave();
}


void MyDlg::OnBnHotItemChangeButton3(NMHDR *pNMHDR, LRESULT *pResult){
	LPNMBCHOTITEM pHotItem = reinterpret_cast<LPNMBCHOTITEM>(pNMHDR);
	*pResult = 0;
}

void MyDlg::OnTimer(UINT_PTR nIDEvent){
	extern std::string bar_text;
	extern int percent;

	//progressb.Invalidate();

	extern bool can_login;
	if (can_login)
		percent = 100;
	

	CProgressCtrl* ctrl = (CProgressCtrl*)GetDlgItem(IDC_PROGRESS2);
	ctrl->SetPos(percent);
	updateProgress();
	//progressb.percent = percent;
}
void start_app(std::string in){
	STARTUPINFO startupInfo = { 0 };
	startupInfo.cb = sizeof(startupInfo);

	PROCESS_INFORMATION processInformation;
	BOOL result = ::CreateProcess(
		&in[0],
		NULL,
		NULL,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS,
		NULL,
		NULL,
		&startupInfo,
		&processInformation
		);

	/*if(result == 0)
	 MessageBox(NULL,"Could not create process","Warning!",MB_OK);*/
}
void MyDlg::updateProgress(){
	if (!maximized)
		return;

	extern std::string bar_text;
	extern int percent;
	extern bool can_login;

	if (can_login)
		percent = 100;

	CProgressCtrl* progressBar = (CProgressCtrl*)GetDlgItem(IDC_PROGRESS2);
	if (progressBar){
		progressBar->SetPos(percent);
		CDC * dc_ = progressBar->GetDC();
		Gdiplus::Graphics GHP(*dc_);
		RECT progressWnd;
		progressBar->GetWindowRect(&progressWnd);

		int fsize = 14;
		Gdiplus::Font myFont(L"Arial", (Gdiplus::REAL)fsize, Gdiplus::FontStyleBold, Gdiplus::UnitPixel);
		Gdiplus::RectF layoutRect((Gdiplus::REAL)0, (Gdiplus::REAL)0,
			(Gdiplus::REAL)(progressWnd.right - progressWnd.left), (Gdiplus::REAL) (progressWnd.bottom - progressWnd.top));

		Gdiplus::RectF boundRect;

		WCHAR temp[1024];
		wsprintfW(temp, L"%S", &bar_text[0]);

		GHP.MeasureString(temp, bar_text.length(), &myFont, layoutRect, &boundRect);

		float difx = abs(layoutRect.Width - boundRect.Width) / 2;
		float dify = abs(layoutRect.Height - boundRect.Height) / 2;
		Gdiplus::RectF c(boundRect.X + difx, boundRect.Y + dify, boundRect.Width, boundRect.Height);

		Gdiplus::StringFormat format;
		format.SetAlignment(Gdiplus::StringAlignmentCenter);

		Gdiplus::Color temclr;
		temclr = Gdiplus::Color(185, 0, 0, 0);

		Gdiplus::SolidBrush blackBrush(temclr);

		GHP.DrawString(
			temp,
			lstrlenW(&temp[0]),
			&myFont,
			c,
			&format,
			&blackBrush);
		Gdiplus::Point pt1;
		pt1.X = 0;
		pt1.Y = 0;

		/*
			Gdiplus::Point pt2;
			pt2.X = 10;
			pt2.Y = 10;
			Gdiplus::Pen blackPen(Gdiplus::Color(255, 0, 0, 0), 3);
			GHP.DrawLine(&blackPen,pt1,pt2);
		*/
	}
}

void minimize(){
	extern MyDlg dlg;
	dlg.ShowWindow(SW_MINIMIZE);
}

void close(){
	TerminateProcess(GetCurrentProcess(), NULL);
}

void MyDlg::OnBnClickedButton3(){
	ShellExecute(NULL, "open", "http://oturox.com", NULL, NULL, SW_SHOWNORMAL);
	//jogar
	/*extern bool can_login;
	if (!can_login){
	MessageBox("Please wait the complete update checks.", "Atention!");
	return;
	}
	/*CComboBox * combo = (CComboBox*)GetDlgItem(IDC_COMBO1);
	/*
	0 dx
	1 ogl
	3 oldcli
	*/
	/*switch(combo->GetCurSel())
	{
	case 0:
	start_app("Client_DxP.exe");
	TerminateProcess(GetCurrentProcess(),NULL);
	break;
	case 1:
	start_app("DxP_OpenGL.exe");
	TerminateProcess(GetCurrentProcess(),NULL);
	break;
	case 2:
	{
	STARTUPINFO startupInfo = {0};
	startupInfo.cb = sizeof(startupInfo);

	PROCESS_INFORMATION processInformation;

	// Try to start the process

	BOOL result = ::CreateProcess(
	".\\data\\things\\854\\DxP_OLD_Client.exe",
	NULL,
	NULL,
	NULL,
	FALSE,
	NORMAL_PRIORITY_CLASS,
	NULL,
	".\\data\\things\\854\\",
	&startupInfo,
	&processInformation
	);
	TerminateProcess(GetCurrentProcess(),NULL);

	}
	break;
	default:
	MessageBox("Por favor selecione um item da lista.","Error!");
	}*/
	/*STARTUPINFO startupInfo = {0};
	startupInfo.cb = sizeof(startupInfo);

	PROCESS_INFORMATION processInformation;
	BOOL result = ::CreateProcess(
	"Tibia.exe",
	NULL,
	NULL,
	NULL,
	FALSE,
	NORMAL_PRIORITY_CLASS,
	NULL,
	".\\",
	&startupInfo,
	&processInformation
	);
	TerminateProcess(GetCurrentProcess(),NULL);
	*/
}

void MyDlg::OnBnClickedButton4(){
	ShellExecute(NULL, "open", "http://oturox.com/?subtopic=forum", NULL, NULL, SW_SHOWNORMAL);
}

void MyDlg::OnBnClickedButton1(){
	ShellExecute(NULL, "open", "darkxpoke.zapto.org", NULL, NULL, SW_SHOWNORMAL);
}

void MyDlg::OnBnClickedButton100(){

	extern bool can_login;
	if (!can_login){
		MessageBox("Please wait the complete update checks.", "Atention!", MB_TOPMOST);
		return;
	}
	/*CComboBox * combo = (CComboBox*)GetDlgItem(IDC_COMBO1);
	/*
	0 dx
	1 ogl
	3 oldcli
	*/
	/*switch(combo->GetCurSel())
	{
	case 0:
	start_app("Client_DxP.exe");
	TerminateProcess(GetCurrentProcess(),NULL);
	break;
	case 1:
	start_app("DxP_OpenGL.exe");
	TerminateProcess(GetCurrentProcess(),NULL);
	break;
	case 2:
	{
	STARTUPINFO startupInfo = {0};
	startupInfo.cb = sizeof(startupInfo);

	PROCESS_INFORMATION processInformation;

	// Try to start the process

	BOOL result = ::CreateProcess(
	".\\data\\things\\854\\DxP_OLD_Client.exe",
	NULL,
	NULL,
	NULL,
	FALSE,
	NORMAL_PRIORITY_CLASS,
	NULL,
	".\\data\\things\\854\\",
	&startupInfo,
	&processInformation
	);
	TerminateProcess(GetCurrentProcess(),NULL);

	}
	break;
	default:
	MessageBox("Por favor selecione um item da lista.","Error!");
	}*/
	STARTUPINFO startupInfo = { 0 };
	startupInfo.cb = sizeof(startupInfo);

	PROCESS_INFORMATION processInformation;
	BOOL result = ::CreateProcess(
		"CheckProcess.exe",
		NULL,
		NULL,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS,
		NULL,
		".\\",
		&startupInfo,
		&processInformation
	);

	TerminateProcess(GetCurrentProcess(), NULL);
	//close
	/*HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, false, GetCurrentProcessId());
	BOOL result = TerminateProcess(hProcess, 0);
	// TODO: Add your control notification handler code here*/
}



class about : public CDialog{
public:
	about(){
		/*if(!Create(IDD_DIALOG2,NULL))
			MessageBox("s");*/
		ShowWindow(SW_SHOW);
	}
	DECLARE_MESSAGE_MAP()
};

void MyDlg::OnBnClickedButton101(){
	ShellExecute(NULL, "open", "http://oturox.com/?subtopic=buypoints", NULL, NULL, SW_SHOWNORMAL);
}

void MyDlg::OnBnStartNeutral(){
	extern std::string version_to_update;
	short cIndex;
	extern bool all_is_okay;
/*	cIndex = ((CComboBox*)GetDlgItem(IDC_COMBO2))->GetCurSel();
	if (cIndex == -1)
	{
		::MessageBox(0, "Please select a version.", "Launcher", MB_OK);
		return;
	}

	((CComboBox*)GetDlgItem(IDC_COMBO2))->GetLBText(cIndex, version);*/
	CString version;
	((CComboBox*)GetDlgItem(IDC_COMBO2))->GetWindowTextA(version);

	extern bool is_hidden;
	is_hidden = true;
	

	version_to_update =
		version.GetString();
	if (all_is_okay){
		
		start_version(version_to_update);
		TerminateProcess(GetCurrentProcess(), NULL);
	}
	else
		MessageBox("Please wait updates!","Warning!");
	bar_text = "";
}

BEGIN_MESSAGE_MAP(about, CDialog)
END_MESSAGE_MAP()

void MyDlg::OnBnClickedButton6(){
	TerminateProcess(GetCurrentProcess(), NULL);
	// TODO: Add your control notification handler code here
}


void MyDlg::OnBnClickedButton7(){
	minimize();
	// TODO: Add your control notification handler code here
}
