                                           set_hud_delay(100)    

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          profit_total = 0
total = 0                  
waste_total=0

                                   
info_pos_x =  TelaBordaX()-170
info_pos_y =  40
info_offset_x_value = 120
offset_y = 15;
info_R = 0
info_G =255
info_B =0
mlpc= 100-Self.mlevelpc()
swpc= 100 - Self.swordpc()
shpc=100 - Self.shieldingpc()
time_running = info_bot_time_running()
time_h = math.floor(time_running/3600000)
time_min = math.fmod(math.floor(time_running/60000),60)
time_sec =math.fmod(math.floor(time_running/1000),60)
qty_exp_h= exp_hour()
exp_pro_lvl = exp_to_next_lvl()
exp_h= math.floor ((exp_to_next_lvl() / exp_hour()))
exp_m= math.fmod(math.floor(exp_to_next_lvl() / (exp_hour() / 60)),60)


exp_s=0

info_list = {
	{name = "Level", func = Self.level()},
    {name = "Exp/h",func = qty_exp_h},
	{name = "Exp next lvl", func =exp_pro_lvl },
    {name =  "Time Next Lvl", func = "" ..exp_h.. ":" .. exp_m},
    {name="Tempo de bot:  ", func ="" .. time_h .. ":" .. time_min .. ":" .. time_sec},
	{name = "Cap", func = Self.cap()},
    {name= "Ml                             ", func = ""..Self.mlevel().. "(" .. mlpc .."%)"},
    {name= "Sword                    ", func = "" .. Self.sword().. "(".. swpc .. "%)"},
    {name= "Shield                     ", func="" .. Self.shielding().. "(".. shpc .. "%)"},

}
info_pos_x = 15

-- IMPRIMIR CONTORNO

hudbar(100,10, info_pos_y,180,TelaSizeY(),20,60,120,0,0,0,false,false)
hudbar(100,tibiascreensizex() - 350, info_pos_y,183,TelaSizeY(),20,60,120,0,0,0,false,false)
hudsquare(10,info_pos_y-3,info_pos_x+55+info_offset_x_value,TelaSizeY()+40,255,180,20,3)
hudsquare(tibiascreensizex()-350,info_pos_y-3,tibiascreensizex()-350+183,TelaSizeY()+40,255,180,20,3)

-- FIM CONTORNO

hudfont("comic san ms",1,1, 1, 20)
hudtext(newstring("Self Info"),255,255,255,info_pos_x,info_pos_y)
info_pos_y = (info_pos_y + offset_y + 5)
hudfont("comic san ms",0,0, 1, 14)
for i=1,table.getn(info_list),1 do  -- IMPRIMIR ARRAY INF_LIST

	hudtext(newstring(info_list[i].name),info_R,info_G,info_B,info_pos_x,info_pos_y)
	hudtext(newstring(info_list[i].func),info_R+255,info_G -75,info_B+20,info_pos_x+info_offset_x_value,info_pos_y)
	info_pos_y=info_pos_y+offset_y
end
hudfont("comic san ms",1,0,1,25)
hudtext(newstring("--------------------------"),255,180,0,info_pos_x-5,info_pos_y+10)
info_pos_y=info_pos_y+offset_y


-- SELF INFO PART END -- 

-- BOT INFO PART --

info_pos_y = (info_pos_y + 50)
offset_y = 15;
offset_x_on = 120
info_on_R = 0;
info_on_G = 255;
info_on_B = 0;
hudfont("comic san ms",1,1, 1, 20)
hudtext("Bot Status",255,255,255,info_pos_x,info_pos_y)
info_pos_y = (info_pos_y + offset_y+10)
hudfont("comic san ms",0,0, 1, 14)
status_on_of = "OFF"
if status_waypointer() then status_on_of = "ON" else status_on_of = "OFF" end  -- COLOCAR ON VERDE  OFF VERMELHO
if status_on_of == "OFF" then 
info_on_R = 255
info_on_G = 0
info_on_B = 0
else
info_on_R = 0
info_on_G = 255
info_on_B = 0
end 
hudtext("Waypointer ",info_R,info_G,info_B,info_pos_x,info_pos_y)  
hudtext(status_on_of,info_on_R,info_on_G,info_on_B,info_pos_x+offset_x_on,info_pos_y)  
info_pos_y = (info_pos_y + offset_y)

if status_hunter() then status_on_of = "ON" else status_on_of = "OFF" end -- COLOCAR ON VERDE  OFF VERMELHO

if status_on_of == "OFF" then -- OFF VERMELHO

info_on_R = 255
info_on_G = 0
info_on_B = 0

else                         -- ON VERDE

info_on_R = 0
info_on_G = 255
info_on_B = 0
end 
hudtext("Hunter ",info_R,info_G,info_B,info_pos_x,info_pos_y)  
hudtext(status_on_of,info_on_R,info_on_G,info_on_B,info_pos_x+offset_x_on,info_pos_y)  
info_pos_y = (info_pos_y + offset_y)

if status_looter() then status_on_of = "ON" else status_on_of = "OFF" end -- COLOCAR ON VERDE  OFF VERMELHO

if status_on_of == "OFF" then  --  OFF VERMELHO

info_on_R = 255
info_on_G = 0
info_on_B = 0

else  							-- ON VERDE

info_on_R = 0
info_on_G = 255
info_on_B = 0
end 
hudtext("Looter ",info_R,info_G,info_B,info_pos_x,info_pos_y)  
hudtext(status_on_of,info_on_R,info_on_G,info_on_B,info_pos_x+offset_x_on,info_pos_y)  
info_pos_y = (info_pos_y + offset_y)

if status_waypointer() then status_on_of = "ON" else status_on_of = "OFF" end -- COLOCAR ON VERDE  OFF VERMELHO

if status_on_of == "OFF" then   -- OFF VERMELHO

info_on_R = 255
info_on_G = 0
info_on_B = 0

else                        -- ON VERDE

info_on_R = 0
info_on_G = 255
info_on_B = 0
end 
hudtext("Atack Spell ",info_R,info_G,info_B,info_pos_x,info_pos_y)  
hudtext(status_on_of,info_on_R,info_on_G,info_on_B,info_pos_x+offset_x_on,info_pos_y)  
info_pos_y = (info_pos_y + offset_y)

if status_lua_background() then status_on_of = "ON" else status_on_of = "OFF" end -- COLOCAR ON VERDE  OFF VERMELHO
if status_on_of == "OFF" then 	--OFF VERMELHO
info_on_R = 255
info_on_G = 0
info_on_B = 0

else                  -- ON VERDE

info_on_R = 0
info_on_G = 255
info_on_B = 0
end 
hudtext("Lua Background ",info_R,info_G,info_B,info_pos_x,info_pos_y)  
hudtext(status_on_of,info_on_R,info_on_G,info_on_B,info_pos_x+offset_x_on,info_pos_y)  
info_pos_y = (info_pos_y + offset_y)

if status_auto_mount() then status_on_of = "ON" else status_on_of = "OFF" end     -- COLOCAR ON VERDE  OFF VERMELHO

if status_on_of == "OFF" then -- OFF VERMELHO
info_on_R = 255
info_on_G = 0
info_on_B = 0

else                           -- ON VERDE

info_on_R = 0
info_on_G = 255
info_on_B = 0
end 
hudtext("Auto Mount ",info_R,info_G,info_B,info_pos_x,info_pos_y)  
hudtext(status_on_of,info_on_R,info_on_G,info_on_B,info_pos_x+offset_x_on,info_pos_y)  
info_pos_y = (info_pos_y + offset_y)

if status_recover() then status_on_of = "ON" else status_on_of = "OFF" end    -- COLOCAR ON VERDE  OFF VERMELHO

if status_on_of == "OFF" then  -- OFF VERMELHO

info_on_R = 255
info_on_G = 0
info_on_B = 0

else           -- ON VERDE

info_on_R = 0
info_on_G = 255
info_on_B = 0
end 
hudtext("Recover ",info_R,info_G,info_B,info_pos_x,info_pos_y)  
hudtext(status_on_of,info_on_R,info_on_G,info_on_B,info_pos_x+offset_x_on,info_pos_y)  
hudfont("comic san ms",1,0,1,25)
hudtext(newstring("--------------------------"),255,180,0,info_pos_x-5,info_pos_y+20)
info_pos_y = (info_pos_y + offset_y)
                                                                                                                                                                            
-- BOT INFO PART END --     
-- BOT ANIMATED LIFE BAR --

place_x= TelaBordaX()+20
place_y= TelaBordaY()+ 40
size_x = TelaSizeX()-20
size_y = 15
red_variation = 0
blue_bar = 255
hp = Self.hppc()

if hp <= 40 and hp >=25 then 
	red_variation = 7
	if temp_life == nil then
		temp_life = 255
	else
		if temp_life >= 200 then
			temp_life = (temp_life - 10)
		else
			temp_life = 255
		end
	end
elseif hp <= 25 and hp >=0 then 
	red_variation = 4
	if temp_life == nil then
		temp_life = 255
	else
		if temp_life >= 200 then
			temp_life = (temp_life - 10)
		else
			temp_life = 255
		end
	end
else

	temp_life = 200
	red_variation = 5
end
hudbar(100,place_x+10,place_y,size_x-20,size_y,255,0,0,1,0,0,true,true)
hudbar(hp,place_x+10,place_y,size_x-20,size_y,temp_life,0,0,red_variation,0,0,true,true)

-- BOT ANIMATED LIFE BAR END--

--STAMINA 

temp_r=0
temp_g=0
temp_b=0

if Self.stamina() >=  2400 then  -- STAMINA COR VERDE SE 40HRS+

	temp_r=0
	temp_g=255
	temp_b=0
elseif (Self.stamina()>=900) and (Self.stamina()<2400) then   -- STAMINA COR LARANJA SE 15HRS<STAMINA<40
	temp_r=240
	temp_g=120
	temp_b=0
else       								-- STAMINA COR VERMELHA SE 15HRS
	temp_r=255
	temp_g=0
	temp_b=0
end
min = math.fmod(Self.stamina(),60)
stamina_percent = Self.stamina() / (2520 / 100)

hudfont("comic san ms",1,0,1,20)

if  min < 10 then -- IMPRIMIR A STAMINA CORRETA EX: 35:08   // SE RETIRAR ESSE IF ELE IRA IMRPIMIR  35:8

	hudtext(newstring("Stamina -> ", Self.stamina()/60,":0",min),temp_r,temp_g,temp_b,TelaBordaX()+((TelaSizeX())/2)-20,TelaBordaY() +TelaSizeY()+35)
	
else -- IMPRIMIR A STAMINA NORMAL

	hudtext(newstring("Stamina ->  ", Self.stamina()/60,":",min),temp_r,temp_g,temp_b,TelaBordaX()+((TelaSizeX())/2)-20,TelaBordaY() +TelaSizeY()+35)
	
end
hudbar(stamina_percent,place_x+10,TelaBordaY()+TelaSizeY()+55,size_x-20,size_y-5,temp_r,temp_g,temp_b,0,8,0,true,true)

-- BOOT LOOTED ITEM LIST --

item_list = {
	{name = "Gold coin", price = 1},
	{name = "Marsh Stalker Beak", price = 65},
	{name = "Marsh Stalker Feather", price= 50},
}

-- POSITION OF THE ITEM LIST AND SPACE BETWEEN ITEMS -- 

loot_list_x =tibiascreensizex() -345;
loot_list_y  =  40
off_set_between_item = 15
offset_between_value = 130
loot_R = 0
loot_G=255
loot_B=0
loot_valor_R =255
loot_valor_G =180
loot_valor_B =20



hudfont("comic san ms",1,1, 1, 20)
hudtext("Loot list by Neutral",255,255,255,loot_list_x,loot_list_y)
hudfont("arial",0,1, 1, 15)
loot_list_y = (loot_list_y + off_set_between_item+10)
for i = 1,table.getn(item_list),1 do 						 -- IMPRIMIR O ARRAY ITEM_LIST
	this_total = (qtylooteditem(item_list[i].name) * item_list[i].price)
	total = (total +this_total)
	temp_str = item_list[i].name;
	
	if (string.len(temp_str) > 11) then						 -- DIMINUIR O NOME DO LOOT EX: MARSH STALK..
		temp_str = newstring(string.sub(temp_str,1,11),"...")
	end
	
	if i==1  then 							 -- VERIFICAR SE O LOOT � GOLD COIN, PARA IMPRIMIR APENAS A QUANTIDADE! GOLD COIN TEM QUE SER O PRIMEIRO DO ARRAY ITEM_LIST
	
		hudtext(newstring(temp_str),loot_R,loot_G,loot_B,loot_list_x,loot_list_y)
		hudtext(newstring(qtylooteditem(item_list[i].name)),loot_valor_R,loot_valor_G,loot_valor_B,loot_list_x+120,loot_list_y)
		
	elseif this_total < 1000  then   				-- IMPRIMIR O RESTANTE DOS LOOTS E FAZER ELE VIRAR GPS CASO SEJA MENOR QUE 1K
	
		hudtext(newstring(temp_str),loot_R,loot_G,loot_B,loot_list_x,loot_list_y)
		hudtext(newstring(qtylooteditem(item_list[i].name)),loot_valor_R,loot_valor_G,loot_valor_B,loot_list_x+100,loot_list_y)
        hudtext(newstring("(",(this_total),"gps)"),loot_valor_R,loot_valor_G,loot_valor_B,(loot_list_x+offset_between_value-8),loot_list_y)
        
	else  							-- IMPRIMIR EM K
	
		hudtext(newstring(temp_str),loot_R,loot_G,loot_B,loot_list_x,loot_list_y)
		hudtext(newstring(qtylooteditem(item_list[i].name)),loot_valor_R,loot_valor_G,loot_valor_B,loot_list_x+100,loot_list_y)
        hudtext(newstring("(",(this_total/1000),".",math.floor(math.fmod(this_total,1000)/100),"k)"),loot_valor_R,loot_valor_G,loot_valor_B,(loot_list_x+offset_between_value),loot_list_y)
	end
	loot_list_y = loot_list_y + off_set_between_item
end
loot_list_y = loot_list_y +5
hudfont("arial",0,1,1,19)
hudtext(newstring("Total: "),loot_R,loot_G,loot_B,loot_list_x,loot_list_y)
hudtext(newstring(total),loot_R+255,loot_G-75,loot_B+20,loot_list_x+100,loot_list_y)

	loot_list_y = (loot_list_y + off_set_between_item)
                                                      

-- BOOT LOOTED ITEM LIST END --

hudfont("comic san ms",1,0,1,25)
hudtext(newstring("--------------------------"),255,180,0,loot_list_x-5,loot_list_y+20)
loot_list_y = (loot_list_y + off_set_between_item)

-- PROFIT

waste_list = {
                         {name= "Mana Potion", price=50},
                         {name= "Strong Health Potion",  price= 100},
              }

-- POSITION OF PROFIT

profit_x= tibiascreensizex() - 345
profit_y= loot_list_y + 50
off_set_item=15
off_set_value=130
profit_R=0
profit_G=255
profit_B=0
profit_valor_R =255
profit_valor_G =180
profit_valor_B =20

hudfont("comic san ms",1,1,1,20)
hudtext("Profit",255,255,255,profit_x,profit_y)
hudfont("comic san ms",0,1,1,14)
profit_y= profit_y+off_set_item+10

for x=1,table.getn(waste_list),1 do  -- IMPRIMIR O ARRAY WASTE_LIST

	waste_local=count_used_items(waste_list[x].name)*waste_list[x].price
	waste_total=waste_total+waste_local
	temp_str=waste_list[x].name

	if (string.len(temp_str) > 11) then   -- REDUZIR O NOME DO LOOT EX: MARSH STALK...

		temp_str = (string.sub(temp_str,1,11).."...")
	end
	
	if waste_local < 1000 then -- IMPRIMIR EM GPS
	
		hudtext(newstring(temp_str),profit_R,profit_G,profit_B,profit_x,profit_y)
		hudtext(newstring(count_used_items(waste_list[x].name)),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+100,profit_y)
		hudtext(newstring(waste_local,"gps"),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+off_set_value,profit_y)
		
	else -- IMPRIMIR EM K
	
		hudtext(newstring(temp_str),profit_R,profit_G,profit_B,profit_x,profit_y)
		hudtext(newstring(count_used_items(waste_list[x].name)),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+100,profit_y)
		hudtext(newstring(waste_local/1000,".",math.floor(math.fmod(waste_local/1000)/100),"k"),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+off_set_value,profit_y)
	end
	profit_y = (profit_y + off_set_item)

end
profit_y = profit_y +5
profit_total=total-waste_total
profit_hora=(profit_total/time_h)
hudfont("comic san ms",0,0,1,20)
hudtext(newstring( "Waste Total:"),profit_R,profit_G,profit_B,profit_x,profit_y)

if waste_total < 1000 then  -- IMPRIMIR EM GPS

	hudtext(newstring(waste_total,"gps"),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+off_set_value-8,profit_y)

else -- IMPRIMIR EM K

	hudtext(newstring(waste_total/1000,"k"),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+off_set_value,profit_y)

end
profit_y = profit_y + off_set_item+10
hudfont("comic san ms",0,1,1,25)

if time_h < 1 then  -- n�o ficar profit negativo!
	hudtext(newstring("Profit/H:"),profit_R,profit_G,profit_B,profit_x,profit_y)
	hudtext(newstring("0.0"),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+85,profit_y)
	profit_y=profit_y+off_set_item+15
	
else                      -- IMPRIMIR PROFIT/H DEPOIS DE 1 HORA

	hudtext(newstring("Profit/H:"),profit_R,profit_G,profit_B,profit_x,profit_y)
	hudtext(newstring(profit_hora/1000,".", math.floor(math.fmod(profit_hora,10)),"k"),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+85,profit_y)
	profit_y=profit_y+off_set_item+15
end
hudtext(newstring("Profit:"),profit_R,profit_G,profit_B,profit_x,profit_y)
hudtext(newstring(profit_total/1000,".",math.floor(math.fmod(profit_total,1000)/100),"k"),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+85,profit_y)
profit_y=profit_y+off_set_item+15
hudtext(newstring("Balance:"),profit_R,profit_G,profit_B,profit_x,profit_y)
hudtext(newstring(last_balance()/10),profit_valor_R,profit_valor_G,profit_valor_B,profit_x+100,profit_y)
profit_y=profit_y+off_set_item+50

-- PROFIT END !!!!!

hudfont("comic san ms",1,0,1,25)
hudtext(newstring("--------------------------"),255,180,0,profit_x-5,profit_y-25)
profit_y = (profit_y + off_set_item)

-- TASK SYSTEM

monster=250-qtymonsterkilled("Killer Caiman")

if monster <=0  then  -- resetar quando matar 250 criaturas

	resetmonstercount()
end
hudfont("comic san ms",1,1,1,25)
hudtext(newstring("Task Infos: "),profit_R+255,profit_G,profit_B+255,profit_x,profit_y) 
profit_y=profit_y+off_set_item+15
hudfont("comic san ms",0,1,1,15)
hudtext(newstring("Killer Caiman Restantes: "), profit_R,profit_G,profit_B,profit_x,profit_y)
hudtext(newstring(monster), profit_R+255,profit_G-75,profit_B+20,profit_x+150,profit_y)
hudfont("comic san ms",1,0,1,25)
hudtext(newstring("--------------------------"),255,180,0,profit_x-5,profit_y+25)
hudfont("comic sans ms",1,1,1,30)
hudtext(newstring("HUD by Peedroh"),255,155,50,profit_x-5,profit_y+80)
hudfont("verdana",1,0,0,15)
pos_Y_msg=TelaSizeY()
cont = 0
cont_local = 0

while cont < 5 do   -- imprimir 5 loot of 

	if cont_local == 200 then    -- m�ximo de msgs, resete.
	
		break
	else

		temporary_buffer = get_message_from_stack(cont_local)
		if string.find(temporary_buffer,"Loot of")  ~=  nil then   -- verificar se a mensagem possui 'loot of'

			hudtext(temporary_buffer, 255,255,0, place_x-10,pos_Y_msg+15)
			cont=cont+1

			pos_Y_msg = pos_Y_msg -20
		end   
	cont_local=cont_local+1
	end
end
