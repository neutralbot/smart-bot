function dectohex(num)
	return tonumber(num, 16);
end

lookAddressIdChar					= dectohex("0x6D202C")
lookAddressPrimeiroIdBattle 		= dectohex("0x72DE20")
AddressSkullModeNew					= dectohex("0x5458F4")
AddressSkullModeOld					= dectohex("0x54591A")
lookAddressLogged					= dectohex("0x53470C")

--ok							--byte 1=not skull. byte 0=skull -- 41336 -- ok

AddressActualHotkeySchema 			= dectohex("0x51EC08")
AddressTreino						= dectohex("0x53461C")
XorAddress							= dectohex("0x534658")
AddressExp							= dectohex("0x534660")
AddressLvl							= dectohex("0x534670")
AddressMagicLvl						= dectohex("0x534678")
AddressMagicLvlPc					= dectohex("0x534680")
AddressTargetRed					= dectohex("0x534684")
AddressMp							= dectohex("0x534688")
AddressFirstPc						= dectohex("0x534690")
AddressHWND							= dectohex("0x534654")
AddressWhiteTarget					= dectohex("0x5346AC")
AddressStamina						= dectohex("0x5346BC")
AddressStatus						= dectohex("0x5346C4")

AddressOfBpPointer					= dectohex("0x534970")
AddressOfWindow						= dectohex("0x53495C")
AdressEquip							= dectohex("0x534970")
AddressSoul							= dectohex("0x534674")
AddressReceivBuffer					= dectohex("0x534394")
--53497C
AddressMouseX1						= dectohex("0x5456D4")
AddressFollow						= dectohex("0x5448D0")
AddressCooldownBar					= dectohex("0x5457EA")
AddressTelaY						= dectohex("0x5461AC")
address_mouse						= dectohex("0x5461CC")
address_mouse_2						= address_mouse;

AddressTelaX						= dectohex("0x5461D8")

AddressServerMessage				= dectohex("0x587FF0")
AddressMessagePlayer				= AddressServerMessage
AddressMessageWarning				= dectohex("0x587DD0")
AddressMessageNotPossible			= AddressMessageWarning 
address_pointer_spells_basic		= dectohex("0x589A54")+8
address_pointer_spells				= address_pointer_spells_basic 	+ 8
address_total_spells				= address_pointer_spells 		+ 4
AddressExpHour						= dectohex("0x589AFC")

addressOfFirstMap					= dectohex("0x58DA08")+8
pointer_vip_players					= dectohex("0x6CDF78")
AddressBaseLogList					= dectohex("0x6AC85C")
AddressCtrl							= dectohex("0x6CE188")
AddressSchemaHotkeyReference		= dectohex("0x6CE23C")
address_item_to_be_used				= dectohex("0x6CE464")
AddressMouseX2						= dectohex("0x6CE47C")
AddressMouseY2						= AddressMouseX2 + 4;
address_item_to_be_moved			= dectohex("0x6CE48C")

AddressCap							= dectohex("0x6D201C")
AddressHp							= dectohex("0x6D2030")
AddressZGO							= dectohex("0x6D2034")
AddressFirst						= dectohex("0x6D2000")

AddressYGO							= dectohex("0x6D2020")
AddressMaxHp						= dectohex("0x6D2024")
AddressXGO							= dectohex("0x6D2028")

AddressX							= dectohex("0x6D2038")
AddressY							= AddressX + 4;
AddressZ							= dectohex("0x6D2040")

base_address_in_gui					= dectohex("0x773E24")
AddressHelmet						= dectohex("0x773FB0")
PointerInicioMap					= dectohex("0x773FF4")
PointerInicioOffsetMap				= dectohex("0x778B24")

address_bp_base_new					= dectohex("0x77A2A4")
address_tibia_time					= dectohex("0x77ADF8")

AddressMouseY1						= AddressMouseX1 + 4
AddressMaxMp						= XorAddress + 4;


AddressClubPc					= AddressFirstPc + 4;
AddressSwordPc					= AddressFirstPc + 4 * 2;
AddressAxePc					= AddressFirstPc + 4 * 3;
AddressDistancePc				= AddressFirstPc + 4 * 4;
AddressShieldingPc				= AddressFirstPc + 4 * 5;
AddressFishingPc				= AddressFirstPc + 4 * 6;

AddressMouse_fix_x				= AddressMouseX1;
AddressMouse_fix_y				= AddressMouse_fix_x + 4;

AddressClub						= AddressFirst		+ 4;
AddressSword					= AddressFirst		+ 4 *2;
AddressAxe						= AddressFirst		+ 4 *3;
AddressDistance					= AddressFirst		+ 4 *4;
AddressShielding				= AddressFirst		+ 4 *5;
AddressFishing					= AddressFirst		+ 4 *6;

offset_beetwen_body_item		= dectohex("0x20");	

AddressAmulet					= AddressHelmet		- offset_beetwen_body_item * 1;
AddressBag						= AddressHelmet		- offset_beetwen_body_item * 2;
AddressMainBp					= AddressBag;
AddressArmor					= AddressHelmet		- offset_beetwen_body_item * 3;
AddressShield					= AddressHelmet		- offset_beetwen_body_item * 4;
AddressWeapon					= AddressHelmet		- offset_beetwen_body_item * 5;
AddressLeg						= AddressHelmet		- offset_beetwen_body_item * 6;
AddressBoot						= AddressHelmet		- offset_beetwen_body_item * 7;
AddressRing						= AddressHelmet		- offset_beetwen_body_item * 8;
AddressRope						= AddressHelmet		- offset_beetwen_body_item * 9;
AddressLogged					= lookAddressLogged;	
AddressIdChar					= lookAddressIdChar;
AddressPrimeiroNomeBattle		= lookAddressPrimeiroIdBattle + 4;




AddressAcc							= dectohex("0x5D1380")--ignore deprecated
AddressPass							= dectohex("0x5D138C")--ignore deprecated
AddressIndexChar					= dectohex("0x5D13BC")--ignore deprecated

set_global_lua_var("lookAddressIdChar",	lookAddressIdChar);
set_global_lua_var("lookAddressIdChar",	lookAddressIdChar);
set_global_lua_var("lookAddressPrimeiroIdBattle",lookAddressPrimeiroIdBattle);
set_global_lua_var("lookAddressLogged",lookAddressLogged);
set_global_lua_var("AddressReceivBuffer",AddressReceivBuffer);
set_global_lua_var("AddressStatus",AddressStatus);
set_global_lua_var("AddressTreino",AddressTreino);
set_global_lua_var("XorAddress",XorAddress);
set_global_lua_var("AddressExp",AddressExp);
set_global_lua_var("AdressEquip",AdressEquip);
set_global_lua_var("AddressOfWindow",AddressOfWindow);
set_global_lua_var("AddressMaxMp",	AddressMaxMp);
set_global_lua_var("AddressLvl",AddressLvl);
set_global_lua_var("AddressSoul",AddressSoul);
set_global_lua_var("AddressMagicLvl",AddressMagicLvl);
set_global_lua_var("AddressMagicLvlPc",AddressMagicLvlPc);
set_global_lua_var("AddressTargetRed",AddressTargetRed);
set_global_lua_var("AddressWhiteTarget",AddressWhiteTarget);
set_global_lua_var("AddressMp",AddressMp);
set_global_lua_var("AddressFirstPc",AddressFirstPc);
set_global_lua_var("AddressStamina",AddressStamina);
set_global_lua_var("AddressClubPc",AddressClubPc);
set_global_lua_var("AddressSwordPc",AddressSwordPc);
set_global_lua_var("AddressAxePc",AddressAxePc);
set_global_lua_var("AddressDistancePc",AddressDistancePc);
set_global_lua_var("AddressShieldingPc",AddressShieldingPc);
set_global_lua_var("AddressFishingPc",AddressFishingPc);
set_global_lua_var("AddressOfBpPointer",AddressOfBpPointer);
set_global_lua_var("AddressCtrl",AddressCtrl);
set_global_lua_var("AddressMouseX1",AddressMouseX1);
set_global_lua_var("AddressMouseY1",AddressMouseY1);
set_global_lua_var("AddressFollow",AddressFollow);
set_global_lua_var("AddressMouse_fix_x",AddressMouse_fix_x);
set_global_lua_var("AddressMouse_fix_y",AddressMouse_fix_y);
set_global_lua_var("address_hotkey_message",address_hotkey_message);
set_global_lua_var("AddressSkullModeNew",AddressSkullModeNew);
set_global_lua_var("AddressCooldownBar",AddressCooldownBar);
set_global_lua_var("AddressSkullModeOld",AddressSkullModeOld);
set_global_lua_var("address_hotkey_type",address_hotkey_type);
set_global_lua_var("address_hotkey_auto",address_hotkey_auto);
set_global_lua_var("address_hotkey_id",address_hotkey_id);
set_global_lua_var("address_mouse",address_mouse);
set_global_lua_var("AddressTelaX",AddressTelaX);
set_global_lua_var("address_mouse_2",address_mouse_2);
set_global_lua_var("AddressTelaY",AddressTelaY);
set_global_lua_var("AddressHWND",AddressHWND);
set_global_lua_var("AddressServerMessage",AddressServerMessage);
set_global_lua_var("AddressMessageWarning",AddressMessageWarning);
set_global_lua_var("AddressMessageNotPossible",AddressMessageNotPossible);
set_global_lua_var("AddressMessagePlayer",AddressMessagePlayer);
set_global_lua_var("address_pointer_spells",address_pointer_spells);
set_global_lua_var("address_pointer_spells_basic",address_pointer_spells_basic);
set_global_lua_var("address_total_spells",address_total_spells);
set_global_lua_var("AddressExpHour",AddressExpHour);
set_global_lua_var("addressOfFirstMap",addressOfFirstMap);
set_global_lua_var("AddressMouseX2",AddressMouseX2);
set_global_lua_var("AddressMouseY2",AddressMouseY2);
set_global_lua_var("AddressAcc",AddressAcc);
set_global_lua_var("AddressPass",AddressPass);
set_global_lua_var("AddressIndexChar",AddressIndexChar);
set_global_lua_var("address_item_to_be_used",address_item_to_be_used);
set_global_lua_var("address_item_to_be_moved",address_item_to_be_moved);
set_global_lua_var("AddressBaseLogList",AddressBaseLogList);
set_global_lua_var("pointer_vip_players",pointer_vip_players);
set_global_lua_var("AddressHp",AddressHp);
set_global_lua_var("AddressZGO",AddressZGO);
set_global_lua_var("AddressFirst",AddressFirst);
set_global_lua_var("AddressCap",AddressCap);
set_global_lua_var("AddressYGO",AddressYGO);
set_global_lua_var("AddressMaxHp",AddressMaxHp);
set_global_lua_var("AddressXGO",AddressXGO);
set_global_lua_var("AddressX",AddressX);
set_global_lua_var("AddressY",AddressY);
set_global_lua_var("AddressZ",AddressZ);
set_global_lua_var("base_address_in_gui",base_address_in_gui);
set_global_lua_var("AddressHelmet",AddressHelmet);
set_global_lua_var("PointerInicioMap",PointerInicioMap);
set_global_lua_var("PointerInicioOffsetMap",PointerInicioOffsetMap);
set_global_lua_var("address_bp_base_new",address_bp_base_new);
set_global_lua_var("address_tibia_time",address_tibia_time);
set_global_lua_var("AddressClub",AddressClub);
set_global_lua_var("AddressSword",AddressSword);
set_global_lua_var("AddressAxe",AddressAxe);
set_global_lua_var("AddressDistance",AddressDistance);
set_global_lua_var("AddressShielding",AddressShielding);
set_global_lua_var("AddressFishing",AddressFishing);
set_global_lua_var("AddressAmulet",AddressAmulet);
set_global_lua_var("AddressBag",AddressBag);
set_global_lua_var("AddressMainBp",AddressMainBp);
set_global_lua_var("AddressArmor",AddressArmor);
set_global_lua_var("AddressShield",AddressShield);
set_global_lua_var("AddressWeapon",AddressWeapon);
set_global_lua_var("AddressLeg",AddressLeg); 
set_global_lua_var("AddressBoot",AddressBoot);
set_global_lua_var("AddressRing",AddressRing);
set_global_lua_var("AddressRope",AddressRope);
set_global_lua_var("AddressLogged",AddressLogged);
set_global_lua_var("AddressIdChar",AddressIdChar);
set_global_lua_var("AddressPrimeiroNomeBattle",AddressPrimeiroNomeBattle);
set_global_lua_var("AddressActualHotkeySchema",AddressActualHotkeySchema);
set_global_lua_var("AddressSchemaHotkeyReference",AddressSchemaHotkeyReference);

UpdateAddresses();



