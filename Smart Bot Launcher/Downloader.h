#pragma once
#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include "stdafx.h"

#include "ApiFiles.h"
#include "unzip.h"
#include "hashgen.h"
#include "xml.h"

#include <vector>
#include <fstream>
#include <wininet.h>
#include <stdio.h> 
#include <stdlib.h>
#include <iostream>
#include <tchar.h>
#include <stdio.h>
#include <psapi.h>
#include <ostream>
#include <sstream>
#include <iostream>
#include <stdlib.h>
#include <algorithm>

#include "Dialog.h"

#include <NoPingShared\defines_all.h>
#include <boost\filesystem.hpp>

#include "resource.h"




#pragma comment(lib,"psapi.lib")
#pragma comment(lib,"Advapi32.lib")
#pragma comment(lib,"Wininet.lib")

extern bool can_login;
extern std::string bar_text;
extern int percent;

using namespace std;
bool curl_download_string(char* url, std::string& out, bool use_ftp = false);
bool curl_download(char * url, bool reload/*ignore*/, void(*update)(unsigned long, unsigned long), char* out_filename, bool use_ftp = false);
#define MAX_ERRMSG_SIZE 80
#define MAX_FILENAME_SIZE 512
#define BUF_SIZE 10240           // 10 KB
#define HTTP_USER_AGENT "HTTPRIP"
#define BUFFER_SIZE 10240
bool download_to_directory(std::string directory, std::string & url, std::string hash, std::string finalname, int trytimes);
void unzip(std::string zipfile, std::string outname);

std::string StringToLower(char * entrace);

std::string StringToLower(std::string & entrace);

int download_string(char * entrace_url, string & out);


class DLExc
{
private:
	char err[MAX_ERRMSG_SIZE];
public:
	DLExc(char *exc){
		if (strlen(exc) < MAX_ERRMSG_SIZE)
			strcpy(err, exc);
	}

	const char *geterr(){
		return err;
	}
};


class Download{
public:
	static bool ishttp(char *url);
	static bool httpverOK(HINTERNET hIurl);
	static bool getfname(char *url, char *fname);
	static unsigned long openfile(char *url, bool reload, ofstream &fout);
	static bool download(char *url, bool reload = false, void(*update)(unsigned long, unsigned long) = NULL);
};


#endif


std::string get_this_client_hash();

struct file{
	std::string name;
	std::string path;
	std::string hash;
	std::string link;
};

struct info{
	std::vector<file> files;// name  --  filehash
	std::string message;
	std::string hash;
};

void replaceAll(std::string& str, const std::string& from, const std::string& to);

void create_shortcut(std::string version);

void start_version(std::string version);

bool ExistModuleNameInProcess(DWORD processID, char* file);

void TerminateProcessWithModuleName(char* file);

bool ExistProcessUsingFile(char* str);


std::string string_to_lower(std::string str);

extern HWND main_hwnd;
extern bool can_login;
extern std::string bar_text;
extern int percent;

void parse_all(std::string url);

bool create_directory(std::string path);

bool is_zip(std::string & entrace);

struct version_node{
	std::string before;
	std::string next;
	std::string current;
};

struct version_file{
	std::string current_version;
	std::vector<version_node> versions;
	bool valid;
};

version_file get_version_file_from_string(std::string xml_string);
version_file get_version_file(std::string url);

DWORD update_thread();

void start_update_thread();

void update_percent(unsigned long total, unsigned long now);

inline bool exists_test0(const std::string& name);

std::string GetOwnFileName();

void update_percent_curl(unsigned long total, unsigned long now);

std::string get_disponible_name();

void remove_reserved_files();

void check_create_dir(std::string dir_full_path);

bool download_to_directory(std::string directory, std::string & url, std::string hash, std::string finalname, int trytimes);

void unzip(std::string zipfile, std::string outname);


std::string StringToLower(char * entrace);

std::string StringToLower(std::string & entrace);