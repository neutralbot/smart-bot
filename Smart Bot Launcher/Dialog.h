#pragma once
#include "stdafx.h"
#include<mmsystem.h>
#pragma comment(lib,"Winmm.lib")
void myprogess_set(int pos);
class MyProgress : public CProgressCtrl{
	//todo yet
};
class MyProgess :public CButton{
public:
	MyProgess(){
		percent = 0;
		barfront = 0;
		barbk = 0;
	};
	void init(CDialog * father, int id){
		Create(_T("My button"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
			CRect(0, 0, 100, 100), father, id);
	}
	virtual ~MyProgess(){  }

public:
	Gdiplus::Image * barfront;
	Gdiplus::Image * barbk;
	int percent;
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	DECLARE_MESSAGE_MAP()
};
class MyBtn : public CButton{
public:

	MyBtn(){ loaded_images = false; };
	void inita(int id, int id2){

		img_on_hover = Load(MAKEINTRESOURCE(id), "PNG", GetModuleHandle(0));;
		img_on_push = Load(MAKEINTRESOURCE(id2), "PNG", GetModuleHandle(0));
		img_on_default = Load(MAKEINTRESOURCE(id), "PNG", GetModuleHandle(0));
		loaded_images = true;
		initialized = true;
	}
	void init(CDialog * father, int id){
		Create(_T("My button"), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | BS_OWNERDRAW,
			CRect(0, 0, 100, 100), father, id);
		initialized = true;
	}
	virtual ~MyBtn(){}
public:
	bool initialized;
	Gdiplus::Image * img_on_hover;
	Gdiplus::Image * img_on_push;
	Gdiplus::Image * img_on_default;
	bool loaded_images;
	Gdiplus::Image * Load(LPCTSTR pName, LPCTSTR pType, HMODULE hInst);
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	bool mouse_is_hover;
	virtual void OnMouseMove(UINT nFlags, CPoint point){
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE | TME_HOVER;
		tme.dwHoverTime = 1;
		TrackMouseEvent(&tme);
	}
	afx_msg void OnMouseHover(UINT, CPoint){
		if (!mouse_is_hover){

			//PlaySound(MAKEINTRESOURCE(IDR_WAVE1), GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC);
			mouse_is_hover = true;
			Invalidate();
		}
	}
	afx_msg void OnMouseLeave(){
		if (mouse_is_hover){
			mouse_is_hover = false;
			Invalidate();
		}
	}

	afx_msg void OnPaint();
	DECLARE_MESSAGE_MAP()
	int my_id;
};

/*
*/

class MyDlg :public CDialog{


	// Construction
public:
	bool maximized;
	MyDlg(CWnd* pParent = NULL);
	enum { IDD = IDD_DIALOG1 };
protected:
	virtual void DoDataExchange(CDataExchange* pDX);

public:
	
	void updateProgress();
	ULONG_PTR m_gdiplusToken;
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	CImage bkimage;
	Gdiplus::Image * logo;
	Gdiplus::Image * bkimagegdi;
	//Gdiplus::Graphics * MyInterface;
	Gdiplus::GdiplusStartupOutput * MyGdi;
	int status;
	CPaintDC * dc;
	CProgressCtrl * myprogress;
public:
	HICON m_hIcon;

	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	MyBtn btntest;
	MyBtn btntest2;
	MyBtn btntest3;
	MyProgess  progressb;
	MyBtn mybtnclose; MyBtn close;
	MyBtn info;

	DECLARE_MESSAGE_MAP()
public:
	virtual void OnMouseMove(UINT nFlags, CPoint point){
		TRACKMOUSEEVENT tme;
		tme.cbSize = sizeof(tme);
		tme.hwndTrack = m_hWnd;
		tme.dwFlags = TME_LEAVE | TME_HOVER;
		tme.dwHoverTime = 1;
		TrackMouseEvent(&tme);
	}
	virtual void OnMouseHover(UINT nFlags, CPoint point);
	virtual LRESULT OnMouseHover_(WPARAM wparam, LPARAM lparam){/* MessageBox(L"hove"); */return 0; }
	afx_msg void OnMouseLeave();
	afx_msg void OnBnHotItemChangeButton3(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton100();
	afx_msg void OnBnClickedButton101();
	afx_msg void OnBnStartNeutral();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();

	

	LRESULT OnNcHitTest(CPoint point)
	{
		UINT hit = CDialog::OnNcHitTest(point);
		int vKey = GetSystemMetrics(SM_SWAPBUTTON) ? VK_RBUTTON : VK_LBUTTON;
		if (hit == HTCLIENT && (GetAsyncKeyState(vKey) & 0x8000))
		{
			hit = HTCAPTION;
		}
		return hit;
	}
};

//