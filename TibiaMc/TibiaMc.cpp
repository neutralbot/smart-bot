#pragma once
#include <Windows.h>
#pragma comment(linker, "/opt:nowin98")
#include "TibiaMC.h"
#include <ntstatus.h>

#include <stdio.h>
#include <string>
#include <thread>

using namespace std;

// -------------------------------------------------------------------------
typedef LONG NTSTATUS;

typedef struct _IO_STATUS_BLOCK {
	union {
		NTSTATUS Status;
		PVOID Pointer;
	};
	ULONG_PTR Information;
} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

typedef void (WINAPI * PIO_APC_ROUTINE)(PVOID, PIO_STATUS_BLOCK, DWORD);

typedef LONG TDI_STATUS;
typedef PVOID CONNECTION_CONTEXT;       // connection context



#define TDI_QUERY_ADDRESS_INFO			0x00000003
#define IOCTL_TDI_QUERY_INFORMATION		CTL_CODE(FILE_DEVICE_TRANSPORT, 4, METHOD_OUT_DIRECT, FILE_ANY_ACCESS)

typedef VOID *POBJECT;

typedef struct _SYSTEM_HANDLE {
	ULONG		uIdProcess;
	UCHAR		ObjectType;    // OB_TYPE_* (OB_TYPE_TYPE, etc.)
	UCHAR		Flags;         // HANDLE_FLAG_* (HANDLE_FLAG_INHERIT, etc.)
	USHORT		Handle;
	POBJECT		pObject;
	ACCESS_MASK	GrantedAccess;
} SYSTEM_HANDLE, *PSYSTEM_HANDLE;

typedef struct _SYSTEM_HANDLE_INFORMATION {
	ULONG			uCount;
	SYSTEM_HANDLE	Handles[1];
} SYSTEM_HANDLE_INFORMATION, *PSYSTEM_HANDLE_INFORMATION;

typedef struct _UNICODE_STRING {
	USHORT Length;
	USHORT MaximumLength;
	PWSTR  Buffer;
} UNICODE_STRING;
typedef UNICODE_STRING *PUNICODE_STRING;
typedef const UNICODE_STRING *PCUNICODE_STRING;

typedef UNICODE_STRING OBJECT_NAME_INFORMATION;
typedef UNICODE_STRING *POBJECT_NAME_INFORMATION;

#define SystemHandleInformation			16

typedef enum _OBJECT_INFORMATION_CLASS{
	ObjectBasicInformation,
	ObjectNameInformation,
	ObjectTypeInformation,
	ObjectAllTypesInformation,
	ObjectHandleInformation
} OBJECT_INFORMATION_CLASS;

#define STATUS_SUCCESS					((NTSTATUS)0x00000000L)
#define STATUS_INFO_LENGTH_MISMATCH		((NTSTATUS)0xC0000004L)
#define STATUS_BUFFER_OVERFLOW			((NTSTATUS)0x80000005L)
//-------------------------------------------------------------------------


typedef NTSTATUS(WINAPI *tNTQSI)(DWORD SystemInformationClass, PVOID SystemInformation,
	DWORD SystemInformationLength, PDWORD ReturnLength);
typedef NTSTATUS(WINAPI *tNTQO)(HANDLE ObjectHandle, OBJECT_INFORMATION_CLASS ObjectInformationClass, PVOID ObjectInformation,
	DWORD Length, PDWORD ResultLength);
typedef NTSTATUS(WINAPI *tNTDIOCF)(HANDLE FileHandle, HANDLE Event, PIO_APC_ROUTINE ApcRoutine, PVOID ApcContext,
	PIO_STATUS_BLOCK IoStatusBlock, DWORD IoControlCode,
	PVOID InputBuffer, DWORD InputBufferLength,
	PVOID OutputBuffer, DWORD OutputBufferLength);

void EnableDebugPrivilege()
{
	HANDLE hToken;
	TOKEN_PRIVILEGES tokenPriv;
	LUID luidDebug;
	if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES, &hToken) != FALSE) {
		if (LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &luidDebug) != FALSE)
		{
			tokenPriv.PrivilegeCount = 1;
			tokenPriv.Privileges[0].Luid = luidDebug;
			tokenPriv.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
			AdjustTokenPrivileges(hToken, FALSE, &tokenPriv, sizeof(tokenPriv), NULL, NULL);
		}
	}
}

LPWSTR GetObjectInfo(HANDLE hObject, OBJECT_INFORMATION_CLASS objInfoClass)
{
	LPWSTR lpwsReturn = NULL;
	tNTQO pNTQO = (tNTQO)GetProcAddress(GetModuleHandle("NTDLL.DLL"), "NtQueryObject");
	if (pNTQO != NULL){
		DWORD dwSize = sizeof(OBJECT_NAME_INFORMATION);
		POBJECT_NAME_INFORMATION pObjectInfo = (POBJECT_NAME_INFORMATION) new BYTE[dwSize];
		NTSTATUS ntReturn = pNTQO(hObject, objInfoClass, pObjectInfo, dwSize, &dwSize);
		if ((ntReturn == STATUS_BUFFER_OVERFLOW) || (ntReturn == STATUS_INFO_LENGTH_MISMATCH)){
			delete pObjectInfo;
			pObjectInfo = (POBJECT_NAME_INFORMATION) new BYTE[dwSize];
			ntReturn = pNTQO(hObject, objInfoClass, pObjectInfo, dwSize, &dwSize);
		}
		if ((ntReturn >= STATUS_SUCCESS) && (pObjectInfo->Buffer != NULL))
		{
			lpwsReturn = (LPWSTR) new BYTE[pObjectInfo->Length + sizeof(WCHAR)];
			ZeroMemory(lpwsReturn, pObjectInfo->Length + sizeof(WCHAR));
			CopyMemory(lpwsReturn, pObjectInfo->Buffer, pObjectInfo->Length);
		}
		delete pObjectInfo;
	}
	return lpwsReturn;
}
typedef struct _OBJECT_TYPE_INFORMATION
{
	UNICODE_STRING TypeName;
	ULONG TotalNumberOfObjects;
	ULONG TotalNumberOfHandles;
	ULONG TotalPagedPoolUsage;
	ULONG TotalNonPagedPoolUsage;
	ULONG TotalNamePoolUsage;
	ULONG TotalHandleTableUsage;
	ULONG HighWaterNumberOfObjects;
	ULONG HighWaterNumberOfHandles;
	ULONG HighWaterPagedPoolUsage;
	ULONG HighWaterNonPagedPoolUsage;
	ULONG HighWaterNamePoolUsage;
	ULONG HighWaterHandleTableUsage;
	ULONG InvalidAttributes;
	GENERIC_MAPPING GenericMapping;
	ULONG ValidAccessMask;
	BOOLEAN SecurityRequired;
	BOOLEAN MaintainHandleCount;
	ULONG PoolType;
	ULONG DefaultPagedPoolCharge;
	ULONG DefaultNonPagedPoolCharge;
} OBJECT_TYPE_INFORMATION, *POBJECT_TYPE_INFORMATION;


tNTQSI pNTQSI = (tNTQSI)GetProcAddress(GetModuleHandle("NTDLL.DLL"), "NtQuerySystemInformation");
void another_one();
int mc_process(){
	EnableDebugPrivilege();

	if (pNTQSI == NULL)
		return 0;

	another_one();
	Sleep(700);

	return 0;
}


void MultiClient::disable_mc(){
	state = false;
}

void MultiClient::enable_mc(){
	state = true;
}

bool kill_mutex(int PID, HANDLE mutex);
bool is_did_this_proc_id(int id);
void add_to_did_tibia(int id);

void another_one(){
	bool result = false;


	DWORD dwSize = sizeof(SYSTEM_HANDLE_INFORMATION);
	PSYSTEM_HANDLE_INFORMATION pHandleInfo = (PSYSTEM_HANDLE_INFORMATION) new BYTE[dwSize];
	NTSTATUS ntReturn = pNTQSI(SystemHandleInformation, pHandleInfo, dwSize, &dwSize);
	if (ntReturn == STATUS_INFO_LENGTH_MISMATCH)
	{
		delete pHandleInfo;
		pHandleInfo = (PSYSTEM_HANDLE_INFORMATION) new BYTE[dwSize];
		ntReturn = pNTQSI(SystemHandleInformation, pHandleInfo, dwSize, &dwSize);
	}

	if (ntReturn == STATUS_SUCCESS)
	{
		for (DWORD dwIdx = 0; dwIdx < pHandleInfo->uCount; dwIdx++)
		{

			{
				HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, pHandleInfo->Handles[dwIdx].uIdProcess);
				if (hProcess)
				{
					HANDLE hObject = NULL;
					if (DuplicateHandle(hProcess, (HANDLE)pHandleInfo->Handles[dwIdx].Handle, GetCurrentProcess(), &hObject, STANDARD_RIGHTS_ALL, FALSE, 0))
					{

						char buffer[500];
						ZeroMemory(buffer, 500);
						LPWSTR lpwsName = GetObjectInfo(hObject, ObjectNameInformation);
						sprintf_s(buffer, "%ws \n", lpwsName);
						if (string(buffer).find("TibiaPlayerMutex") != string::npos)
						{
							kill_mutex(pHandleInfo->Handles[dwIdx].uIdProcess, (HANDLE)pHandleInfo->Handles[dwIdx].Handle);
						}
						delete lpwsName;
						CloseHandle(hObject);
						CloseHandle(hProcess);
					}
					else
					{
						CloseHandle(hProcess);
					}
				}
				else
				{

				}
			}
		}

	}
	if (pHandleInfo)
		delete pHandleInfo;
}



bool kill_mutex(int PID, HANDLE mutex)
{
	HANDLE out;
	HANDLE handle;
	if (!PID)return true;
	handle = OpenProcess(PROCESS_ALL_ACCESS, 0, PID);
	if (!handle)return true;
	if (!DuplicateHandle(handle, mutex, GetCurrentProcess(), &out, 0, FALSE, DUPLICATE_CLOSE_SOURCE))return false;

	if (CloseHandle(out) == 0)
	{
		CloseHandle(handle);
		return true;
	}
	CloseHandle(handle);
	return true;
}

int MultiClient::MC_THREAD(){

	for (;;){
		if (state){
			mc_process();
			Sleep(500);
		}
		else{
			Sleep(100);
		}
	}
	return 0;
}

MultiClient::MultiClient() {
	thread(&MultiClient::MC_THREAD, this).detach();
}