#pragma once

namespace MiniMapTest {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	 MiniMap::MiniMapControl^  miniMapControl1;
	protected:

	
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->miniMapControl1 = (gcnew MiniMap::MiniMapControl());
			this->SuspendLayout();
			// 
			// miniMapControl1
			// 
			this->miniMapControl1->cursor_image = nullptr;
			this->miniMapControl1->Location = System::Drawing::Point(12, 12);
			this->miniMapControl1->Name = L"miniMapControl1";
			this->miniMapControl1->selected_waypoint_image = nullptr;
			this->miniMapControl1->selectedWaypoint = nullptr;
			this->miniMapControl1->Size = System::Drawing::Size(380, 377);
			this->miniMapControl1->TabIndex = 0;
			this->miniMapControl1->waypoint_image = nullptr;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(816, 443);
			this->Controls->Add(this->miniMapControl1);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
