

function dectohex(num)
	return tonumber(num, 16);
end

lookAddressIdChar					= dectohex("0x6D202C")
lookAddressPrimeiroIdBattle 		= dectohex("0x72DE20")
AddressSkullModeNew					= dectohex("0x5458F4")
AddressSkullModeOld					= dectohex("0x54591A")
AddressBasePing						= {dectohex("0x1A16C"), dectohex("0x30") }
lookAddressLogged					= dectohex("0x53470C")

--ok							--byte 1=not skull. byte 0=skull -- 41336 -- ok
AddressBasePing						= {dectohex("0x1A16C"), dectohex("0x30")}
AddressActualHotkeySchema 			= dectohex("0x51EC08")
AddressTreino						= dectohex("0x53461C")
XorAddress							= dectohex("0x534658")
AddressExp							= dectohex("0x534660") 
AddressLvl							= dectohex("0x534670")
AddressMagicLvl						= dectohex("0x534678")
AddressMagicLvlPc					= dectohex("0x534680")
AddressTargetRed					= dectohex("0x534684")
AddressMp							= dectohex("0x534688")
AddressFirstPc						= dectohex("0x534690")
AddressHWND							= dectohex("0x534654")
AddressWhiteTarget					= dectohex("0x5346AC")
AddressStamina						= dectohex("0x5346BC")
AddressStatus						= dectohex("0x5346C4")

AddressOfBpPointer					= dectohex("0x534970")
AddressOfWindow						= dectohex("0x53495C")
AdressEquip							= dectohex("0x534970")
AddressSoul							= dectohex("0x534674")
AddressReceivBuffer					= dectohex("0x534394")
--53497C
AddressMouseX1						= dectohex("0x5456D4")
AddressFollow						= dectohex("0x5448D0")
AddressCooldownBar					= dectohex("0x5457EA")
AddressTelaY						= dectohex("0x5461AC")
address_mouse						= dectohex("0x5461CC")
address_mouse_2						= address_mouse;

AddressTelaX						= dectohex("0x5461D8")

AddressServerMessage				= dectohex("0x6CED18")
AddressMessagePlayer				= dectohex("0x587FF0")
AddressMessageNotPossible			= dectohex("0x587DD0") 


address_pointer_spells_basic		= dectohex("0x589A54")+8
address_pointer_spells				= address_pointer_spells_basic 	+ 8
address_total_spells				= address_pointer_spells 		+ 4
AddressExpHour						= dectohex("0x589AFC")

addressOfFirstMap					= dectohex("0x58DA08")+8
pointer_vip_players					= dectohex("0x6CEF20")
AddressBaseLogList					= dectohex("0x6AC85C")
AddressCtrl							= dectohex("0x6CE188")
AddressSchemaHotkeyReference		= dectohex("0x6CE23C")
address_item_to_be_used				= dectohex("0x6CE464")
AddressMouseX2						= dectohex("0x6CE47C")
AddressMouseY2						= AddressMouseX2 + 4;
address_item_to_be_moved			= dectohex("0x6CE48C")

AddressCap							= dectohex("0x6D201C")
AddressHp							= dectohex("0x6D2030")
AddressZGO							= dectohex("0x6D2034")
AddressFirst						= dectohex("0x6D2000")

AddressYGO							= dectohex("0x6D2020")
AddressMaxHp						= dectohex("0x6D2024")
AddressXGO							= dectohex("0x6D2028")

AddressX							= dectohex("0x6D2038")
AddressY							= AddressX + 4;
AddressZ							= dectohex("0x6D2040")

base_address_in_gui					= dectohex("0x773E24")
AddressHelmet						= dectohex("0x773FB0")
PointerInicioMap					= dectohex("0x773FF4")
PointerInicioOffsetMap				= dectohex("0x778B24")

address_bp_base_new					= dectohex("0x77A2A4")
address_tibia_time					= dectohex("0x77ADF8")

AddressMouseY1						= AddressMouseX1 + 4
AddressMaxMp						= XorAddress + 4;


AddressClubPc					= AddressFirstPc + 4;
AddressSwordPc					= AddressFirstPc + 4 * 2;
AddressAxePc					= AddressFirstPc + 4 * 3;
AddressDistancePc				= AddressFirstPc + 4 * 4;
AddressShieldingPc				= AddressFirstPc + 4 * 5;
AddressFishingPc				= AddressFirstPc + 4 * 6;

AddressMouse_fix_x				= AddressMouseX1;
AddressMouse_fix_y				= AddressMouse_fix_x + 4;

AddressClub						= AddressFirst		+ 4;
AddressSword					= AddressFirst		+ 4 *2;
AddressAxe						= AddressFirst		+ 4 *3;
AddressDistance					= AddressFirst		+ 4 *4;
AddressShielding				= AddressFirst		+ 4 *5;
AddressFishing					= AddressFirst		+ 4 *6;

offset_beetwen_body_item		= dectohex("0x20");	

AddressAmulet					= AddressHelmet		- offset_beetwen_body_item * 1;
AddressBag						= AddressHelmet		- offset_beetwen_body_item * 2;
AddressMainBp					= AddressBag;
AddressArmor					= AddressHelmet		- offset_beetwen_body_item * 3;
AddressShield					= AddressHelmet		- offset_beetwen_body_item * 4;
AddressWeapon					= AddressHelmet		- offset_beetwen_body_item * 5;
AddressLeg						= AddressHelmet		- offset_beetwen_body_item * 6;
AddressBoot						= AddressHelmet		- offset_beetwen_body_item * 7;
AddressRing						= AddressHelmet		- offset_beetwen_body_item * 8;
AddressRope						= AddressHelmet		- offset_beetwen_body_item * 9;
AddressLogged					= lookAddressLogged;	
AddressIdChar					= lookAddressIdChar;
AddressPrimeiroNomeBattle		= lookAddressPrimeiroIdBattle + 4;




AddressAcc							= dectohex("0x5D1380")--ignore deprecated
AddressPass							= dectohex("0x5D138C")--ignore deprecated
AddressIndexChar					= dectohex("0x5D13BC")--ignore deprecated


