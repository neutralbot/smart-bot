#pragma once
#include "Updater.h"
#include <string>
#include <stdint.h>

namespace Launcher {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	 
	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public Telerik::WinControls::UI::RadForm
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: Telerik::WinControls::UI::RadProgressBar^  radProgressBar1;
	private: Telerik::WinControls::RadThemeManager^  radThemeManager1;
	private: Telerik::WinControls::Themes::Office2013DarkTheme^  office2013DarkTheme1;
	private: Telerik::WinControls::Themes::Office2010BlackTheme^  office2010BlackTheme1;
	private: Telerik::WinControls::Themes::Office2013LightTheme^  office2013LightTheme1;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->radProgressBar1 = (gcnew Telerik::WinControls::UI::RadProgressBar());
			this->radThemeManager1 = (gcnew Telerik::WinControls::RadThemeManager());
			this->office2013DarkTheme1 = (gcnew Telerik::WinControls::Themes::Office2013DarkTheme());
			this->office2010BlackTheme1 = (gcnew Telerik::WinControls::Themes::Office2010BlackTheme());
			this->office2013LightTheme1 = (gcnew Telerik::WinControls::Themes::Office2013LightTheme());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radProgressBar1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// radLabel1
			// 
			this->radLabel1->Location = System::Drawing::Point(103, 159);
			this->radLabel1->Name = L"radLabel1";
			this->radLabel1->Size = System::Drawing::Size(55, 18);
			this->radLabel1->TabIndex = 0;
			this->radLabel1->Text = L"radLabel1";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(-9, -49);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(400, 399);
			this->pictureBox1->TabIndex = 1;
			this->pictureBox1->TabStop = false;
			// 
			// radProgressBar1
			// 
			this->radProgressBar1->Location = System::Drawing::Point(12, 143);
			this->radProgressBar1->Name = L"radProgressBar1";
			this->radProgressBar1->Size = System::Drawing::Size(242, 17);
			this->radProgressBar1->TabIndex = 2;
			this->radProgressBar1->Text = L"smartbot.exe | 90%";
			this->radProgressBar1->ThemeName = L"Office2010Black";
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(266, 177);
			this->Controls->Add(this->radProgressBar1);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->radLabel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"MyForm";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Launcher";
			this->ThemeName = L"Office2010Black";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radProgressBar1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
		Updater::get()->start();
		if (Updater::get()->has_finished()){
			MessageBox::Show("Finished");
		}
		std::string state;
		uint32_t percent;
		if (Updater::get()->get_current_state(state, percent)){
			radProgressBar1->Text =gcnew String(state.c_str());
			radProgressBar1->Value1 = percent;
		}
		else{

		}
	}
};
}
