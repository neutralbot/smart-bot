#include <string>
#include <stdint.h>

class Updater{
	bool _has_finished;
	bool _has_started;
public:
	void start();

	bool has_finished();

	Updater();
	
	bool get_current_state(std::string& file_name, uint32_t& percent);

	static Updater* get();
};